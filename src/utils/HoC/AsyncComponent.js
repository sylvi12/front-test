import React, { Component } from 'react';
import { SProvider } from 'utils/context/SContext'

export default function asyncComponent(getComponent, menuPath) {

    class AsyncComponent extends Component {

        static Componenet = null;

        state = { Component: AsyncComponent.Component };

        constructor(props) {
            super(props);

            if (AsyncComponent.Component) return;

            getComponent().then(({ default: Component }) => {
                AsyncComponent.Component = Component;
                this.setState({ Component });
            })
        }

        render() {
            const { Component } = this.state;

            if (Component) {
                return (
                    <SProvider menuPath={menuPath}>
                        <Component {...this.props} />
                    </SProvider>
                )
            }

            return null;

        }
    }

    // 서버사이드 렌더링/코드 스플리팅 충돌을 해결하는 함수
    AsyncComponent.getComponent = () => {
        return getComponent.then(({ default: Component }) => {
            AsyncComponenet.Component = Component;
        })
    }

    return AsyncComponent;
}