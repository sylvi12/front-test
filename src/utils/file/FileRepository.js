import API from "components/override/api/API";
import ObjectUtility from 'utils/object/ObjectUtility';
import Alert from 'components/override/alert/Alert';

class FileRepository {

    URL = "/api/file";

    // 파일 업로드
    fileUpload(formData) {
        const config = {
            headers: {
                'Content-Type': 'multipart/form-data',
                Pragma: 'no-cache'
            }
        }
        return API.file.post(`${this.URL}/uploads`, formData, config);
    }

    // 파일 다운로드
    fileDownload(fileId) {
        const config = {
            headers: {
                Pragma: 'no-cache'
            },
            responseType: 'blob',
        }

        API.file.get(`${this.URL}/download?fileId=${fileId}`, config).then((res) => {

            let disposition = res.headers['content-disposition']
            let filename = '';

            try {
                filename = ObjectUtility.replaceAll(decodeURI(disposition.match(/fileName="(.*)"/)[1]), "+", " ");
            } catch (error) {
                Alert.meg('다운로드 중 문제가 발생 하였습니다.');
                return ;
            }

            if (navigator.appVersion.toString().indexOf('.NET') > 0){
                window.navigator.msSaveBlob(new Blob([res.data]), filename);
            }else {
                    
                const url = window.URL.createObjectURL(new Blob([res.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', filename);
                document.body.appendChild(link);
                link.click();

            }
        });
    }

    // 엑셀 다운로드
    excelDownload(downUrl) {
        const config = {
            headers: {
                Pragma: 'no-cache'
            },
            responseType: 'blob',
            //responseType: 'arraybuffer',
            
        }

        API.file.get(`${downUrl}`, config).then((res) => {

            let disposition = res.headers['content-disposition']
            let filename = '';

            try {
                filename = ObjectUtility.replaceAll(decodeURI(disposition.match(/fileName="(.*)"/)[1]), "+", " ");
            } catch (error) {
                console.log(error);
                Alert.meg('다운로드 중 문제가 발생 하였습니다.');
                return ;
            }
            if (navigator.appVersion.toString().indexOf('.NET') > 0){
                window.navigator.msSaveBlob(new Blob([res.data]), filename);
            } else {
                const url = window.URL.createObjectURL(new Blob([res.data], {type:'application/vnd.ms-excel'}));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', filename);
                document.body.appendChild(link);
                link.click();
            }
        });
    }

}
export default new FileRepository();