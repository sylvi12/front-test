import React, { Component } from 'react';
import { inject } from 'mobx-react';
import { NavLink, withRouter } from 'react-router-dom'

import logo from 'style/img/logo.png'


@inject(stores => ({
    isMenuExpand: stores.mainStore.isMenuExpand,
    isMenuOverlap: stores.mainStore.isMenuOverlap,
    onMenuExpandClick: stores.mainStore.onMenuExpandClick,
    logout: stores.loginStore.logout,
    loginNm: stores.commonStore.loginNm,
    loginId: stores.commonStore.loginId,
}))
class HeaderLayout extends Component {

    // logout = async () => {
    //     const { logout, history } = this.props;

    //     await logout();
    //     window.location.href = '/auth/login';
    // }

    render() {

        const { isMenuExpand, isMenuOverlap, onMenuExpandClick, loginNm, loginId } = this.props;

        let topLogoClassName = (isMenuExpand ? 'top_logo' : 'top_logo_close');
        const title = (isMenuExpand ? '삼양사 식품' : '식품');

        if (isMenuOverlap) {
            topLogoClassName = 'top_logo overlap'
        }

        return (
            <div id='header'>
                <div className={topLogoClassName}>
                    <NavLink to={"/app"}><img src={logo} style={{ textAlign: "center" }} />
                        <div className='top_text'>
                            {title}
                            <span className="eng">BU WMS</span>
                        </div>
                    </NavLink>
                </div>

                <div className='top_left'>
                    <a>
                        <i className='icon-menu' onClick={onMenuExpandClick} />
                    </a>
                </div>

                <div className='top_right'>
                    <li>
                        <a>
                            <div className='user_name'>{'작업자'}</div>
                        </a>
                    </li>
                    <li>
                        <a>
                            <i className='icon-logout' onClick={this.logout} />
                        </a>
                    </li>
                </div>
            </div>
        )
    }
}

export default HeaderLayout;