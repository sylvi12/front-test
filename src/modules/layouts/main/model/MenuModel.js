import { Component } from 'react';
import { extendObservable } from 'mobx'

class MenuModel extends Component{

    // 생성자로 전달되는 항목을 @observable 가능항 property로 추가해줌
    constructor(data) {
        super();
        extendObservable(this, data);
    }
}
export default MenuModel;