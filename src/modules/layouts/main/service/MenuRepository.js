import API from "components/override/api/API";

class MenuRepository {

    URL = "/api/menu";

    getSideMenu() {
        return API.request.get(`${this.URL}/side?type=WEB`);
    }

    getSideAuthMenu() {
        return API.request.get(`${this.URL}/sidewithauth?type=WEB`);
    }

    getSite() {
        return API.request.get(`${this.URL}/site`);
    }

    getFamilySite() {
        return API.request.get(`${this.URL}/familysite`);
    }

    getProfileImage() {
        return API.request.get(`${this.URL}/getprofileimage`);
    }

}
export default new MenuRepository();