import React from 'react';
import 'modules/layouts/match/style.css'
import error_tit from 'style/img/error_tit.png';

const NoMatch = () => {
    return (
        <div className="Wrap_error">
            <div className="WrapsetMain">
                <div className="error" style={{ overflow: 'auto' }}>
                    <div className="fl">
                        <h3><img src={error_tit} /></h3>
                        <h3>
                            <div className="error_con_text">
                                요청하신 페이지를 찾을 수 없습니다.<br />
                                해당 페이지는 경로가 변경되었거나 서버에 존재하지 않습니다.
                </div>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default NoMatch;