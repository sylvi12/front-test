import React, { Component } from 'react'
import { NavLink } from 'react-router-dom';
import { inject } from 'mobx-react'

@inject(stores => ({
    onMenuOpen: stores.mainStore.onMenuOpen,
    isMenuExpand: stores.mainStore.isMenuExpand,
    isMenuOverlap: stores.mainStore.isMenuOverlap,
    onOneDepthMenuClick: stores.mainStore.onOneDepthMenuClick,
}))
class SideNavMenu extends Component {

    componentWillReceiveProps(nextProps) {
        if (this.props.isMenuExpand != nextProps.isMenuExpand) {
            this.twoDepthAllVisible(nextProps.isMenuExpand);
        }
    }

    // 소메뉴 전체 Visible 처리
    twoDepthAllVisible = (value) => {
        const activeAll = document.querySelectorAll('.active');

        for (let i = 0; i < activeAll.length; i++) {
            const classList = activeAll[i].classList;

            if (value) {
                classList.remove('allOff');
            } else {
                classList.add('allOff');
            }
        }
    }

    oneDepthMenuClick = (event) => {

        const { isMenuExpand, isMenuOverlap, onOneDepthMenuClick } = this.props;

        const targetClassList = event.currentTarget.parentNode.classList;
        const onCheck = targetClassList.contains('off'); // onCheck true: 현재 닫힌 상태

        // 닫힌 상태에서 누를 경우
        if (!isMenuExpand && !isMenuOverlap) {
            this.twoDepthAllVisible(true);
            onOneDepthMenuClick();
            targetClassList.remove('off');
        } else {
            if (onCheck) {
                // 소메뉴 열기
                targetClassList.remove('off');
            } else {
                // 소메뉴 닫기
                targetClassList.add('off');
            }
        }
    }

    twoDepthMenuClick = (subMenu) => {

        const { isMenuExpand, onMenuOpen } = this.props;

        if (!isMenuExpand) {
            this.twoDepthAllVisible(false);
        }

        onMenuOpen(subMenu);
    }

    render() {

        const { sideMenu } = this.props;

        let subMenu = [
            {
                menuNm: '입고관리'
                , formClass: 'Workorder'
                , menuId: "8010"
                , url: '/order/workorder/Workorder'
                , formId: 'order_workorder_f'
               
            },
            {
                menuNm: '출고관리'
                , formClass: 'output'
                , menuId: "8020"
                , url: '/order/output/Output'
                , formId: 'order_output_f'
               
            },
            {
                menuNm: '창고현황'
                , formClass: 'outputList'
                , menuId: "8030"
                , url: '/order/outputList/OutputList'
                , formId: 'order_outputList_f'
               
            },
            // {
            //     menuNm: '출고관리'
            //     , formClass: 'Workorder'
            //     , menuId: "8020"
            //     , contentUrl: '/order/workorder'
            //     , formId: 'order_workorder_f'
             
            // },
            // {
            //     menuNm: '창고현황'
            //     , formClass: 'Workorder'
            //     , menuId: "8030"
            //     , contentUrl: '/order/workorder'
            //     , formId: 'order_workorder_f'
            // },
        ]
            


        let body = [];
        for (let i = 0; i < subMenu.length; i++) {
            let content = subMenu[i];

            let menu = <div key = {i}>
            {
                <li key={content.menuId} onClick={() => this.twoDepthMenuClick(content)}>
                    <a>
                        {content.menuNm}
                    </a>
                </li>
            }
            </div>
            body.push(menu)
        }
        console.log('사이드메뉴', subMenu)
        return (
            <React.Fragment>
                <ul className='left_menu'>
                        <li className='active'>
                            <a className='has_ul' onClick={this.oneDepthMenuClick}>
                                <i className={'icon-clock'} style={{fontSize:'20px'}}/>
                                <span>{'입출고 MSA'}</span>
                            </a>
                            <ul className='two_depth_menu'>
                                {body}
                            </ul>
                        </li>
                </ul>
            </React.Fragment>
        );
    }
}

export default SideNavMenu;