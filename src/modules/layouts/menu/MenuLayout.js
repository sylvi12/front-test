import React from 'react'
import { NavLink, withRouter } from 'react-router-dom'
import { inject } from 'mobx-react'

import SideNavMenu from 'modules/layouts/menu/SideNavMenu';
import FamilySite from 'modules/layouts/menu/FamilySite';
import placeholder from 'style/img/placeholder.jpg'
import logo_ep_w from 'style/img/logo.png';

@inject(stores => ({
    mainStore: stores.mainStore,
    getSideMenu: stores.mainStore.getSideMenu,
    sideMenuList: stores.mainStore.sideMenuList,
    isMenuExpand: stores.mainStore.isMenuExpand,
    isMenuOverlap: stores.mainStore.isMenuOverlap,
    logout: stores.loginStore.logout,
    loginNm: stores.commonStore.loginNm,
    loginId: stores.commonStore.loginId,
    profileImage: stores.mainStore.profileImage
}))
class MenuLayout extends React.Component {

    constructor(props) {
        super(props);
    }

    logout = async () => {
        const { logout, history } = this.props;

        await logout();
        window.location.href = '/auth/login';
    }

    render() {
        const { isMenuExpand, isMenuOverlap, loginNm, loginId, profileImage } = this.props;

        let toolbarClassName = (isMenuExpand ? 'left_toolbar' : 'left_toolbar_close');

        if (isMenuOverlap) {
            toolbarClassName = 'left_toolbar overlap'
        }
        let image = '';
        if (profileImage == undefined) {
            image = placeholder;
        } else {
            image = profileImage;
        }
        
        return (
            <React.Fragment>

                <div className={toolbarClassName}>

                    <div className='profile'>
                        <p className='pic'>
                            {/* <img src={placeholder}/> */}
                            <img src={image}/>
                        </p>
                        <ul className='txtlist'>
                            <li className='name'>{'작업자'}</li>
                            <li className='bnum'>
                                {'System'}
                            <span className='logout' onClick={this.logout}>로그아웃</span>
                            </li>
                        </ul>
                    </div>

                    <SideNavMenu sideMenu={this.props.sideMenuList} />
                </div>
            </React.Fragment>
        );
    }
}
export default withRouter(MenuLayout)