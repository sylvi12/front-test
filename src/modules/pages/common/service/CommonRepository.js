import API from "components/override/api/API";

class CommonRepository {

    URL = "/api";

    getCodes(params) {
        return API.request.get(`${this.URL}/codes/map?cdGubn=${params.cdGubn}&useFlag=${params.useFlag}`);
    }

    getBu(params) {
        return API.request.get(`${this.URL}/bu?buId=${params.buId}&useYn=${params.useYn}`);
    }

    // checkToken() {
    //     return API.request.get(`/token/validate`);
    // }
}
export default new CommonRepository();