import { observable, action, reaction } from 'mobx';
import CommonRepository from "modules/pages/common/service/CommonRepository"
import LoginRepository from "modules/pages/login/service/LoginRepository"
import MainStore from "modules/layouts/main/service/MainStore"

class CommonStore {

    @observable token = window.localStorage.getItem('jwt');
    @observable loginId = window.localStorage.getItem('loginId');
    @observable loginNm = window.localStorage.getItem('loginNm');
    @observable buId = window.localStorage.getItem('buId');
    @observable userInfo = JSON.parse(window.sessionStorage.getItem('userInfo'));

    codeObject;
    codeHelper = JSON.parse(window.localStorage.getItem('codeHelper'));
    buList = JSON.parse(window.localStorage.getItem('buList'));

    constructor() {

        reaction(() => this.token, token => {
            if (token) {
                window.localStorage.setItem('jwt', token);
            } else {
                window.localStorage.removeItem('jwt');
            }
        });

        reaction(() => this.loginId, loginId => {
            if (loginId) {
                window.localStorage.setItem('loginId', loginId);
            } else {
                window.localStorage.removeItem('loginId');
            }
        });

        reaction(() => this.loginNm, loginNm => {
            if (loginNm) {
                window.localStorage.setItem('loginNm', loginNm);
            } else {
                window.localStorage.removeItem('loginNm');
            }
        });

        reaction(() => this.buId, buId => {
            if (buId) {
                window.localStorage.setItem('buId', buId);
            } else {
                window.localStorage.removeItem('buId');
            }
        });

        reaction(() => this.codeObject, codeObject => {
            if (codeObject) {
                window.localStorage.setItem('codeObject', codeObject);
            } else {
                window.localStorage.removeItem('codeObject');
            }
        });

        reaction(() => this.codeHelper, codeHelper => {
            if (codeHelper) {
                window.localStorage.setItem('codeHelper', codeHelper);
            } else {
                window.localStorage.removeItem('codeHelper');
            }
        });

        reaction(() => this.buList, buList => {
            if (buList) {
                window.localStorage.setItem('buList', buList);
            } else {
                window.localStorage.removeItem('buList');
            }
        });
    }

    @action.bound
    async checkToken() {
        const { data, status } = await CommonRepository.checkToken();

        if (status == 200) {
            return data;
        } else {
            return null;
        }
    }

    @action
    setUserInfo(userInfo) {
        this.userInfo = userInfo;
        window.sessionStorage.setItem('userInfo', JSON.stringify(userInfo));
    }

    @action
    setCodeObject(codeObject) {

        this.codeObject = codeObject;
        window.localStorage.setItem('codeObject', codeObject);
    }

    @action
    setToken(token) {
        this.token = token;
        window.localStorage.setItem('jwt', token);
    }

    @action
    setLoginId(loginId) {
        this.loginId = loginId;
        window.localStorage.setItem('loginId', loginId);
    }

    @action
    setLoginNm(loginNm) {
        this.loginNm = loginNm;
        window.localStorage.setItem('loginNm', loginNm);
    }

    @action
    setBuId(buId) {
        this.buId = buId;
        window.localStorage.setItem('buId', buId);
    }

    @action
    setCodeHelper(codeHelper) {
        this.codeHelper = codeHelper;
        window.localStorage.setItem('codeHelper', codeHelper);
    }

    @action
    setbuList(buList) {
        this.buList = buList;
        window.localStorage.setItem('buList', buList);
    }

    @action clearLocalStorage() {
        this.loginId = undefined;
        this.loginNm = undefined;
        this.buId = undefined;
        this.token = undefined;
        this.codeObject = undefined;
        window.localStorage.removeItem('loginId');
        window.localStorage.removeItem('loginNm');
        window.localStorage.removeItem('buId');
        window.localStorage.removeItem('jwt');
        window.localStorage.removeItem('codeObject');
        window.localStorage.removeItem('buList');
        window.localStorage.clear();

        MainStore.clearMenu();
        window.sessionStorage.clear();
    }

    @action
    async getCodeObject() {
        if (!this.codeObject) {
            await this.requestGetCodes();
        }

        const arrayData = this.codeObject;
        return arrayData;
    }

    @action
    async getCodeObjectSelectBox(codeNameGroup) {
        if (!this.codeObject) {
            await this.requestGetCodes();
        }

        let resultArr = {};

        for (let i = 0; i < codeNameGroup.length; i++) {
            const element = codeNameGroup[i];
            let codeArr = this.codeObject[element.code].concat();
            if (element.allAdd) {
                codeArr.unshift({ cd: '', cdNm: "ALL" });
            }
            resultArr[element.code] = codeArr;
        }

        return resultArr;
    }

    @action
    async getCodeObjectMultiSelect(code, addAll) {
        if (!this.codeObject) {
            await this.requestGetCodes();
        }

        const options = this.codeObject[code];
        const resultArr = [];
        for (let i = 0; i < options.length; i++) {
            const element = options[i];
            resultArr.push({ value: element.cd, label: element.cdNm });
        }

        // if(addAll) {
        //   resultArr.unshift({value:'', label:"ALL"});
        // }

        return resultArr;
    }

    @action
    async requestGetCodes() {
        const params = {
            cdGubn: '',
            useFlag: 'Y'
        };

        const { data, status } = await CommonRepository.getCodes(params);

        if (status < 300) {
            this.codeObject = data.data;
            window.localStorage.setItem('codeObject', JSON.stringify(data.data));

        } else {
            //alert('공통 코드를 가져오지 못하였습니다.');
        }
    }

    @action
    async commonUserInfo() {

        const { data, status } = await LoginRepository.getLoginUserInfo(this.loginId);

        if (status == 200) {
            this.setUserInfo(data.data);
        }
    }

    @action.bound
    async getBuList() {

        const params = {
            buId: '',
            useYn: 'Y',
        }

        const { data, status } = await CommonRepository.getBu(params);

        console.log('getBu', data);
        if (status == 200) {
            this.buList = data.data;
            window.localStorage.setItem('buList', JSON.stringify(data.data));
        }
    }
}
export default new CommonStore();