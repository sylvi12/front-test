import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { action } from 'mobx';

import ico_id from 'style/img/ico_id.png';
import ico_pw from 'style/img/ico_pw.png';


@inject(stores => ({
    userLogin: stores.loginStore.userLogin,
    handleChange: stores.loginStore.handleChange,
    loginId: stores.loginStore.loginId,
    loginPass: stores.loginStore.loginPass,
    userLogin: stores.loginStore.userLogin
}))
@observer
class Login extends Component {

    constructor(props) {
        super(props);
        this.onKeyDown = this.onKeyDown.bind(this);
    }

    componentDidMount() {
        this.refs.input1.addEventListener('keydown', this.onKeyDown);
        this.refs.input2.addEventListener('keydown', this.onKeyDown);
    }

    componentWillUnmount() {
        this.refs.input1.addEventListener('keydown', this.onKeyDown);
        this.refs.input2.addEventListener('keydown', this.onKeyDown);
    }

    onKeyDown(event) {
        if (event.keyCode == 13) {
            event.stopPropagation();

            const { userLogin } = this.props;
            userLogin(this);
        }
    }

    render() {
        const { userLogin, handleChange, loginId, loginPass } = this.props;

        return (
            <div className="WrapLogin">
                <div className="WrapsetLogin">
                    <div className="LoginSet">
                        <div className="Login">
                            <div className="titbx">
                                <p className="tit"></p>
                            </div>
                            <div className="loginbx">
                                <form className="form-signin">
                                    <label className="IdText" >
                                        <input id="loginId" ref="input1" onChange={handleChange} type="text" placeholder="아이디" value={loginId} />
                                    </label>
                                    <label className="PassText">
                                        <input id="loginPass" ref="input2" onChange={handleChange} type="password" placeholder="비밀번호" value={loginPass} />
                                    </label>
                                    <p className="loginbtn" onClick={() => userLogin(this)}><a>로그인</a></p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;