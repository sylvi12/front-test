import { withRouter } from 'react-router-dom';

class goLogin {
    
    goLogin() {
        this.props.history.push("/auth/login");    
    }
}

export default withRouter(goLogin);
