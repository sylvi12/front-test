import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton';
import SBuSelectBox from 'components/override/business/SBuSelectBox';


import CategoryStore from 'modules/pages/standard/category/service/CategoryStore';

@inject(stores => ({
    
      buId: stores.categoryStore.buId
    , handleChange: stores.categoryStore.handleChange
    , handleSearchClick: stores.categoryStore.handleSearchClick
    , buSelectionChanged: stores.categoryStore.buSelectionChanged
    
}))

class CategorySearchItem extends Component {

    constructor(props) {
        super(props);
        //categoryStore.getBu();
    }

    render() {

        const {   buId
                , handleChange
                , handleSearchClick
                , buSelectionChanged } = this.props;

        return (
            <React.Fragment>
                <div>
                    <ul style={{ float: 'right' }}>
                        <SButton 
                            buttonName={'조회'}
                            onClick={handleSearchClick}
                            type={'btnSearch'} 
                        />
                    </ul>
                    <ul style={{ float: 'right' }}>

                        <SBuSelectBox
                            title={"공장"}
                            id={"buId"}
                            value={buId}
                            onChange={handleChange}
                            //onSelectionChange={buSelectionChanged}
                        />
                    </ul>


                </div>
                    
                 
            </React.Fragment>
        );
    }
}

export default CategorySearchItem;