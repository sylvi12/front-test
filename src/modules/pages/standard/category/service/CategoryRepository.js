import API from 'components/override/api/API';

class CategoryRepository {
    
  
    URL = "/api/standard/category";
    

    //저장
    saveCategory(params) {
        console.log(params);
        return API.request.post(encodeURI(`${this.URL}/savecategory`), params);
    }
    
    //삭제
    deleteCategory(params) {
        return API.request.delete(encodeURI(`${this.URL}/deletecategory?categoryCd=${params.categoryCd}`))
    }
}

export default new CategoryRepository();