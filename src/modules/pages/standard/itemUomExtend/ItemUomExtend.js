import React, { Component } from "react";
import { inject } from "mobx-react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";
import STab from 'components/override/tab/STab';

import ItemUomExtendSearchItem from "modules/pages/standard/itemUomExtend/ItemUomExtendSearchItem";
import ItemUomExtendContents from "modules/pages/standard/itemUomExtend/ItemUomExtendContents";

@inject(stores => ({

}))

//제품 UOM 구분별 조회
class ItemUomExtend extends Component {

	constructor(props) {
		super(props);
	}

	render() {
		const { } = this.props;

		return (
			<React.Fragment>
				<div style={{ width: '100%', height: '100%' }}>
					<SearchTemplate searchItem={<ItemUomExtendSearchItem />} />
					<ContentsMiddleTemplate contentSubTitle={'제품 UOM 관리'} />
					<ContentsTemplate id='itemUomExtend_contents' contentItem={<ItemUomExtendContents />} />
				</div>
			</React.Fragment>
		)
	}
}

export default ItemUomExtend;