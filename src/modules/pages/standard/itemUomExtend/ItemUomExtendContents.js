import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid';

@inject(stores => ({
	setItemUomExtendContentsGridApi: stores.itemUomExtendStore.setItemUomExtendContentsGridApi
	, itemUomExtendContentsList: stores.itemUomExtendStore.itemUomExtendContentsList
	, rowDataHandleChange: stores.itemUomExtendStore.rowDataHandleChange
}))

class ItemUomExtendContents extends Component {

	constructor(props) {
		super(props);
	}

	render() {

		const {
			setItemUomExtendContentsGridApi
			, itemUomExtendContentsList
			, rowDataHandleChange
		} = this.props;

		return (
			<SGrid
				grid={'itemUomExtendGrid'}
				gridApiCallBack={setItemUomExtendContentsGridApi}
				rowData={itemUomExtendContentsList}
				onSelectionChanged={rowDataHandleChange}
				cellReadOnlyColor={true}
				editable={false}
			//onCellValueChanged={setChangeYn}
			/>
		);
	}
}
export default ItemUomExtendContents;