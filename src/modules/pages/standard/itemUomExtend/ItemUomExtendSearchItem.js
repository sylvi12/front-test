import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SButton from 'components/atoms/button/SButton';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';

@inject(stores => ({
	itemUomExtendSearchItemItemCd: stores.itemUomExtendStore.itemUomExtendSearchItemItemCd
	, handleChange: stores.itemUomExtendStore.handleChange
	, handleSearchClick: stores.itemUomExtendStore.handleSearchClick
}))

class ItemUomExtendSearchItem extends Component {

	constructor(props) {
		super(props);
	}

	render() {

		const {
			itemUomExtendSearchItemItemCd
			, handleChange
			, handleSearchClick
		} = this.props;

		return (

			<React.Fragment>
				{/* <SSelectBox
                    title={"GLCLASS"}
                    id={"glclass"}
                    value={glclass}
                    codeGroup={"MT002"}
                    onChange={handleChange}
                /> */}
				<SInput
					title={"제품코드"}
					id={"itemUomExtendSearchItemItemCd"}
					value={itemUomExtendSearchItemItemCd}
					onChange={handleChange}
					onEnterKeyDown={handleSearchClick}
				/>
				<div className='search_item_btn'>
					<SButton
						className="btn_red"
						buttonName={'조회'}
						type={'btnSearch'}
						onClick={handleSearchClick} />
				</div>
			</React.Fragment>
		)
	}
}

export default ItemUomExtendSearchItem;