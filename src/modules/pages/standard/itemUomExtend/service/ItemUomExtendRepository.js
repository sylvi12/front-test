import API from "components/override/api/API";

class ItemUomExtendRepository {

    URL = "api/standard/itemUomExtend";

    getItemUomExtendContents(param) {
        return API.request.get(encodeURI(`${this.URL}/getitemuomextendcontents?itemCd=${param.itemCd}`))
    }

}
export default new ItemUomExtendRepository();