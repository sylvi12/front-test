import { observable, action } from 'mobx';

import Alert from 'components/override/alert/Alert';
import GridUtil from 'components/override/grid/utils/GridUtil';

import ItemChangeMappingRepository from 'modules/pages/standard/itemChangeMapping/service/ItemChangeMappingRepository';

import CommonStore from 'modules/pages/common/service/CommonStore';
import BaseStore from 'utils/base/BaseStore';

class ItemChangeMappingStore extends BaseStore{
	@observable itemChangeMappingSearchItemBuIdCheck = false;
	@observable itemChangeMappingSearchItemBuId = '';
	@observable itemChangeMappingSearchItemLocalOrDomestic = 1; //로컬 or 내수 구분 flag
	@observable itemChangeMappingSearchItemItemCdNm = ''; //제품코드 or 제품명

	@observable itemChangeMappingContentsGridApi = undefined;
	@observable itemChangeMappingContentsList = [];

	@observable itemChangeMappingSelectItemModal = false;
	@observable localItemId = 0;
	@observable localItemCd = '';
	@observable localItemNm = '';
	@observable dcItemId = 0;
	@observable dcItemCd = '';
	@observable dcItemNm = '';

	// 로컬/내수 Modal 관련 변수
	@observable itemChangeMappingDcItemCdNm = '';
	@observable itemChangeMappingLocalItemCdNm = '';
	@observable itemChangeMappingModalDcItemGridApi = undefined;
	@observable itemChangeMappingModalLocalItemGridApi = undefined;
	@observable itemChangeMappingModalDcItemList = [];
	@observable itemChangeMappingModalLocalItemList = [];

	// 로컬/내수 SubModal 관련 변수
	@observable itemChangeMappingInsertItemModal = false;
	@observable itemChangeMappingSubModalGridApi = undefined;
	@observable itemChangeMappingSubModalList = [];

	constructor() {
        super();
        // this.validator.valid();
        // this.itemHelperOption.cdNmIsFocus = true;
        this.setInitialState();
    }


	@action.bound
	getBuId = async () => {
		this.itemChangeMappingSearchItemBuId = CommonStore.buId;
		this.itemChangeMappingSearchItemBuIdCheck = true;
	}

	@action.bound
	handleChange = (data) => {
		console.log(data)
		this[data.id] = data.value;
	}

	@action.bound
	rowDataHandleChange = (data) => {
		console.log('내수/로컬 그리드 관리', data)
	}

	//조회
	@action.bound
	handleSearchClick = async () => {

		const searchData = {
			"buId" : this.itemChangeMappingSearchItemBuId
			, "lodGubn" : this.itemChangeMappingSearchItemLocalOrDomestic
			, "itemCdNm" : this.itemChangeMappingSearchItemItemCdNm
		}

		const { data, status } = await ItemChangeMappingRepository.getItemChangeMappingContents(searchData);
		if (data.success && status == 200) {
			this.itemChangeMappingContentsList = data.data;
			this.itemChangeMappingContentsGridApi.setRowData(this.itemChangeMappingContentsList);
		} else {
			Alert.meg(data.msg);
		}
	}

	// 내수/로컬 제품 그리드
	@action.bound
	setItemChangeMappingContentsGridApi = async (gridApi) => {
		this.itemChangeMappingContentsGridApi = gridApi;
		const columnDefs = [
			// {
			// 	headerName: "", field: ""
			// 	, width: 30
			// 	, checkboxSelection: true
			// 	, headerCheckboxSelection: true
			// 	// , editable: function (params) { }
			// },
			{
				headerName : "",
				children : [
					{ headerName: "공장", field: "buNm", width: 80, cellClass: "cell_align_center",editable: false },
				]
			},
			{
				headerName : "내수당",
				children : [
					{ headerName: "제품ID", field: "dcItemId", width: 70, cellClass: "cell_align_center" },
					{ headerName: "제품코드", field: "dcItemCd", width: 80, cellClass: "cell_align_center" },
					{ headerName: "제품명", field: "dcItemNm", width: 160, cellClass: "cell_align_left" },

				]
			},
			{
				headerName : "로컬",
				children : [
					{ headerName: "제품ID", field: "localItemId", width: 70, cellClass: "cell_align_center" },
					{ headerName: "제품코드", field: "localItemCd", width: 80, cellClass: "cell_align_center" },
					{ headerName: "제품명", field: "localItemNm", width: 160, cellClass: "cell_align_left" },

				]
			}
		];
		this.itemChangeMappingContentsGridApi.setColumnDefs(columnDefs);
	}
	
	// 내수/로컬 제품 선택 Modal 열기
	@action.bound
	itemChangeMappingSelectItemModalIsOpen = () => {
		this.itemChangeMappingSelectItemModal = true;
	}

	// 내수/로컬 제품 선택 Modal 닫기
	@action.bound
	itemChangeMappingSelectItemModalIsClose = () => {
		this.localItemId = 0;
		this.localItemCd = '';
		this.localItemNm = '';
		this.dcItemId = 0;
		this.dcItemCd = '';
		this.dcItemNm = '';
		this.itemChangeMappingDcItemCdNm = '';
		this.itemChangeMappingLocalItemCdNm = '';
		this.itemChangeMappingModalDcItemList = [];
		this.itemChangeMappingModalLocalItemList = [];
		this.itemChangeMappingSelectItemModal = false;
	}

	// 내수/로컬 제품 맵핑 선택
	@action.bound
	btnItemChangeMappingSelect = async () => {
		const selectedDcItemRow = GridUtil.getSelectedRowData(this.itemChangeMappingModalDcItemGridApi);
		const selectedLocalItemRow = GridUtil.getSelectedRowData(this.itemChangeMappingModalLocalItemGridApi);

		if(!selectedDcItemRow) {
			Alert.meg('내수 제품을 선택 해주세요.');
			return;
		} else if(!selectedLocalItemRow) {
			Alert.meg('로컬 제품을 선택 해주세요.');
			return;
		} else {
			this.itemChangeMappingInsertItemModal = true;
			const selectRowData = [{
				buId: this.itemChangeMappingSearchItemBuId
				, dcItemId: selectedDcItemRow.itemId
				, dcItemCd: selectedDcItemRow.itemCd
				, dcItemNm: selectedDcItemRow.itemNm
				, localItemId: selectedLocalItemRow.itemId
				, localItemCd: selectedLocalItemRow.itemCd
				, localItemNm: selectedLocalItemRow.itemNm
			}];
			this.itemChangeMappingSubModalList = selectRowData;
		}
	}

	// 내수/로컬 제품 선택 SubModal 닫기
	@action.bound
	itemChangeMappingInsertItemModalIsClose = () => {
		this.itemChangeMappingSubModalList = [];
		this.itemChangeMappingInsertItemModal = false;
	}

	// 내수/로컬 제품 선택 SubModal 그리드
	@action.bound
	setItemChangeMappingSubModalGridApi = async (gridApi) => {
		this.itemChangeMappingSubModalGridApi = gridApi;
		const columnDefs = [
			{
				headerName : "내수당",
				children : [
					{ headerName: "제품ID", field: "dcItemId", width: 70, cellClass: "cell_align_center" },
					{ headerName: "제품코드", field: "dcItemCd", width: 80, cellClass: "cell_align_center" },
					{ headerName: "제품명", field: "dcItemNm", width: 160, cellClass: "cell_align_left" }
				]
			},
			{
				headerName : "로컬",
				children : [
					{ headerName: "제품ID", field: "localItemId", width: 70, cellClass: "cell_align_center" },
					{ headerName: "제품코드", field: "localItemCd", width: 80, cellClass: "cell_align_center" },
					{ headerName: "제품명", field: "localItemNm", width: 160, cellClass: "cell_align_left" }
				]
			}
		];
		this.itemChangeMappingSubModalGridApi.setColumnDefs(columnDefs);
	}


	// 내수/로컬 제품 맵핑 추가
	@action.bound
	btnItemChangeMappingInsert = async () => {
		if (!this.itemChangeMappingSubModalList) {
			return;
		} else {
			// 추가할 제품 RowData
			const addData = this.itemChangeMappingSubModalList[0];

			// 변경 저장 후 Select 변수
			const searchData = {
				"buId": this.itemChangeMappingSearchItemBuId
				, "lodGubn": this.itemChangeMappingSearchItemLocalOrDomestic
				, "itemCdNm": this.itemChangeMappingSearchItemItemCdNm
			};
			const param = JSON.stringify({ param1: addData, param2: searchData });
			const value = await Alert.confirm("추가 하시겠습니까?").then(result => { return result; });
			if (value) {
				const { data, status } = await ItemChangeMappingRepository.insertItemChangeMappingContents(param);
				if (data.success == false) {
					Alert.meg(data.msg);
				} else {
					this.itemChangeMappingContentsList = data.data;
					this.itemChangeMappingContentsGridApi.setRowData(this.itemChangeMappingContentsList);
					this.itemChangeMappingSelectItemModal = false;
					this.itemChangeMappingInsertItemModal = false;
					//GridUtil.setFocus(this.itemChangeMappingContentsGridApi);
					Alert.meg('저장 되었습니다.');
				}
			} else {
				return;
			}
		}
	}

	// 내수제품 모달 그리드
	// @action.bound
	// setItemChangeMappingModalDcItemGridApi = async (gridApi) => {
	// 	this.itemChangeMappingModalDcItemGridApi = gridApi;
	// 	const columnDefs = [
	// 				{ headerName: "제품ID", field: "dcItemId", width: 70, cellClass: "cell_align_center" },
	// 				{ headerName: "제품코드", field: "dcItemCd", width: 80, cellClass: "cell_align_center" },
	// 				{ headerName: "제품명", field: "dcItemNm", width: 160, cellClass: "cell_align_left" }
	// 	];
	// 	this.itemChangeMappingModalDcItemGridApi.setColumnDefs(columnDefs);
	// }

	// 로컬제품 모달 그리드
	// @action.bound
	// setItemChangeMappingModalLocalItemGridApi = async (gridApi) => {
	// 	this.itemChangeMappingModalLocalItemGridApi = gridApi;
	// 	const columnDefs = [
	// 				{ headerName: "제품ID", field: "localItemId", width: 70, cellClass: "cell_align_center" },
	// 				{ headerName: "제품코드", field: "localItemCd", width: 80, cellClass: "cell_align_center" },
	// 				{ headerName: "제품명", field: "localItemNm", width: 160, cellClass: "cell_align_left" }
	// 	];
	// 	this.itemChangeMappingModalLocalItemGridApi.setColumnDefs(columnDefs);
	// }

	///////////////////// 삭제필요 ////////////////////////////

	@action.bound
	setItemChangeMappingModalDcItemGridApi = async (gridApi) => {
		this.itemChangeMappingModalDcItemGridApi = gridApi;
		const columnDefs = [
			{ headerName: "제품ID", field: "itemId", width: 70, cellClass: "cell_align_center" },
			{ headerName: "제품코드", field: "itemCd", width: 80, cellClass: "cell_align_center" },
			{ headerName: "제품명", field: "itemNm", width: 200, cellClass: "cell_align_left" }
		];
		this.itemChangeMappingModalDcItemGridApi.setColumnDefs(columnDefs);
	}

	@action.bound
	setItemChangeMappingModalLocalItemGridApi = async (gridApi) => {
		this.itemChangeMappingModalLocalItemGridApi = gridApi;
		const columnDefs = [
			{ headerName: "제품ID", field: "itemId", width: 70, cellClass: "cell_align_center" },
			{ headerName: "제품코드", field: "itemCd", width: 80, cellClass: "cell_align_center" },
			{ headerName: "제품명", field: "itemNm", width: 200, cellClass: "cell_align_left" }
		];
		this.itemChangeMappingModalLocalItemGridApi.setColumnDefs(columnDefs);
	}
	//////////////////////////////////////////////////////////

	@action.bound
	btnItemChangeMappingDcItemSearch = async () => {
		//내수당 제품 찾기
		const searchData = {
			"itemId" : Number(this.itemChangeMappingDcItemCdNm)
		}
		const { data, status } = await ItemChangeMappingRepository.getItemChangeMappingDcItemContents(searchData);
		if (status == 200) {
			this.itemChangeMappingModalDcItemList = data.data;
			this.itemChangeMappingModalDcItemGridApi.setRowData(this.itemChangeMappingModalDcItemList);
		} else {
			return;
		}
	}

	@action.bound
	btnItemChangeMappingLocalItemSearch = async () => {
		//로컬 제품 찾기
		const searchData = {
			"itemId" : Number(this.itemChangeMappingLocalItemCdNm)
		}
		const { data, status } = await ItemChangeMappingRepository.getItemChangeMappingLocalItemContents(searchData);
		if (status == 200) {
			this.itemChangeMappingModalLocalItemList = data.data;
			this.itemChangeMappingModalLocalItemGridApi.setRowData(this.itemChangeMappingModalLocalItemList);
		} else {
			return;
		}
	}
	
	// 내수/로컬 제품 관리 삭제
	@action.bound
	itemChangeMappingMiddleItemDelete = async () => {
		//const selectedRowData = this.itemChangeMappingContentsGridApi.getSelectedRows();
		const selectedRowData = GridUtil.getSelectedRowData(this.itemChangeMappingContentsGridApi);
		if (!selectedRowData) {
			Alert.meg('선택된 대상이 없습니다.');
			return;
		} else {
			const searchData = {
				"buId": this.itemChangeMappingSearchItemBuId
				, "lodGubn": this.itemChangeMappingSearchItemLocalOrDomestic
				, "itemCdNm": this.itemChangeMappingSearchItemItemCdNm
			};

			const param = JSON.stringify({ param1: selectedRowData, param2: searchData });
			const value = await Alert.confirm("삭제 하시겠습니까?").then(result => { return result; });
			if (value) {
				const { data, status } = await ItemChangeMappingRepository.deleteItemChangeMappingContents(param);
				if (data.success == false) {
					Alert.meg(data.msg);
				} else {
					this.itemChangeMappingContentsList = data.data;
					this.itemChangeMappingContentsGridApi.setRowData(this.itemChangeMappingContentsList);
					Alert.meg('삭제 되었습니다.');
				}
			} else {
				return;
			}
		}
	}

}

export default new ItemChangeMappingStore();