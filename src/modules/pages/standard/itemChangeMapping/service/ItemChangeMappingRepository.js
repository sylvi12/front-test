import API from "components/override/api/API";

class ItemChangeMappingRepository {

    URL = "api/standard/itemChangeMapping";

    // 조회
    getItemChangeMappingContents(param) {
        return API.request.get(encodeURI(`${this.URL}/getitemchangemappingcontents?buId=${param.buId}&lodGubn=${param.lodGubn}&itemCdNm=${param.itemCdNm}`))
    }

    // 내수당 ITEM 조회
    getItemChangeMappingDcItemContents(param) {
        return API.request.get(encodeURI(`${this.URL}/getitemchangemappingitemcontents?itemId=${param.itemId}`));
    }

    // 로컬 ITEM 조회
    getItemChangeMappingLocalItemContents(param) {
        return API.request.get(encodeURI(`${this.URL}/getitemchangemappingitemcontents?itemId=${param.itemId}`));
    }

    // 추가
    insertItemChangeMappingContents(params) {
        return API.request.post(encodeURI(`${this.URL}/insertitemchangemappingcontents`),params);
    }

    // 삭제
    deleteItemChangeMappingContents(param) {
        return API.request.post(encodeURI(`${this.URL}/deleteitemchangemappingcontents`), param);
    }



}
export default new ItemChangeMappingRepository();