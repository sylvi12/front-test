import React, { Component } from "react";
import { inject } from "mobx-react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import ItemChangeMappingStore from "modules/pages/standard/itemChangeMapping/service/ItemChangeMappingStore";
import SButton from 'components/atoms/button/SButton';
import SGrid from 'components/override/grid/SGrid';


@inject(stores => ({
	btnItemChangeMappingInsert: stores.itemChangeMappingStore.btnItemChangeMappingInsert
	, setItemChangeMappingSubModalGridApi: stores.itemChangeMappingStore.setItemChangeMappingSubModalGridApi
	, itemChangeMappingSubModalList: stores.itemChangeMappingStore.itemChangeMappingSubModalList
}))

//제품 UOM 구분별 조회
export default class ItemChangeMappingSubModal extends Component {

	constructor(props) {
		super(props);
	}

	render() {
		const {
			btnItemChangeMappingInsert
			, setItemChangeMappingSubModalGridApi
			, itemChangeMappingSubModalList
		} = this.props;

		return (
			<React.Fragment>
				<div style={{ width: '100%', height: '100%' }}>
					{/* contentSubTitle={'내수당'}  */}
					<ContentsMiddleTemplate middleItem={
						<ItemChangeMappingSubModalMiddleItem
							btnItemChangeMappingInsert={btnItemChangeMappingInsert}
						/>} />
					<ContentsTemplate id='itemChangeMappingSubModal_contents' contentItem={
						<ItemChangeMappingSubModalContentItem
							setItemChangeMappingSubModalGridApi={setItemChangeMappingSubModalGridApi}
							itemChangeMappingSubModalList={itemChangeMappingSubModalList}
						/>} />
				</div>
			</React.Fragment>
		);
	}
}
const ItemChangeMappingSubModalMiddleItem = ({
	btnItemChangeMappingInsert
}) => {
	return (
		<SButton
			buttonName={'저장'}
			type={'btnItemChangeMappingInsert'}
			onClick={btnItemChangeMappingInsert}
		/>
	)
}

const ItemChangeMappingSubModalContentItem = ({
	setItemChangeMappingSubModalGridApi
	, itemChangeMappingSubModalList
}) => {
	return (
		<SGrid
			grid={'itemChangeMappingSubModalGridApi'}
			gridApiCallBack={setItemChangeMappingSubModalGridApi}
			rowData={itemChangeMappingSubModalList}
			cellReadOnlyColor={true}
			editable={false}
		/>
	)
}