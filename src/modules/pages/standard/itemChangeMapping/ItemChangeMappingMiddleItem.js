import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SButton from 'components/atoms/button/SButton';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';

@inject(stores => ({
	itemChangeMappingSelectItemModalIsOpen: stores.itemChangeMappingStore.itemChangeMappingSelectItemModalIsOpen
	, itemChangeMappingMiddleItemDelete: stores.itemChangeMappingStore.itemChangeMappingMiddleItemDelete
}))

class ItemChangeMappingMiddleItem extends Component {

	constructor(props) {
		super(props);
	}

	render() {
		const {
			itemChangeMappingSelectItemModalIsOpen
			, itemChangeMappingMiddleItemDelete
		} = this.props;

		return (
			<React.Fragment>
				<SButton
					className="btn_red"
					buttonName={'추가'}
					type={'btnAdd'}
					onClick={itemChangeMappingSelectItemModalIsOpen}
				/>
				<SButton
					className="btn_red"
					buttonName={'삭제'}
					type={'btnDelete'}
					onClick={itemChangeMappingMiddleItemDelete}
				/>
			</React.Fragment >
		)
	}
}

export default ItemChangeMappingMiddleItem;