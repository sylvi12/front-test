import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SButton from 'components/atoms/button/SButton';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SBuSelectBox from 'components/override/business/SBuSelectBox';

@inject(stores => ({
	itemChangeMappingSearchItemBuId: stores.itemChangeMappingStore.itemChangeMappingSearchItemBuId
	, itemChangeMappingSearchItemLocalOrDomestic: stores.itemChangeMappingStore.itemChangeMappingSearchItemLocalOrDomestic
	, itemChangeMappingSearchItemItemCdNm: stores.itemChangeMappingStore.itemChangeMappingSearchItemItemCdNm
	, handleChange: stores.itemChangeMappingStore.handleChange
	, handleSearchClick: stores.itemChangeMappingStore.handleSearchClick
}))

class ItemChangeMappingSearchItem extends Component {

	constructor(props) {
		super(props);
	}

	render() {

		const {
			itemChangeMappingSearchItemBuId
			, itemChangeMappingSearchItemLocalOrDomestic
			, itemChangeMappingSearchItemItemCdNm
			, handleChange
			, handleSearchClick
		} = this.props;

		const ldType= [
			{	
				cd: 1
				,cdNm : '내수'
			},
			{
				cd: 2
				,cdNm : '로컬'
			}
		];
		return (

			<React.Fragment>
				<SBuSelectBox
					title={"공장"}
					id={"itemChangeMappingSearchItemBuId"}
					value={itemChangeMappingSearchItemBuId}
					isAddAll={false}
					onChange={handleChange}
					//onSelectionChange={buSelectionChanged}
				/>

				<SSelectBox
                    title={"내수/로컬"}
					id={"itemChangeMappingSearchItemLocalOrDomestic"}
					value={itemChangeMappingSearchItemLocalOrDomestic}
					optionGroup={ldType}
					valueMember={'cd'}
					displayMember={'cdNm'}
					onChange={handleChange}
					//onSelectionChange={}
					// contentWidth={70}
					// marginLeft={10}
                />

				<SInput
					title={"제품명/코드"}
					id={"itemChangeMappingSearchItemItemCdNm"}
					value={itemChangeMappingSearchItemItemCdNm}
					onChange={handleChange}
					onEnterKeyDown={handleSearchClick}
				/>

				<div className='search_item_btn'>
					<SButton
						className="btn_red"
						buttonName={'조회'}
						type={'btnSearch'}
						onClick={handleSearchClick} />
				</div>
			</React.Fragment>
		)
	}
}

export default ItemChangeMappingSearchItem;