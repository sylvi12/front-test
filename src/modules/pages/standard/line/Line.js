import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SearchTemplate from 'components/template/SearchTemplate';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate'
import ContentsTemplate from 'components/template/ContentsTemplate';
import SModal from "components/override/modal/SModal";

import LineEmployeeModal from "modules/pages/standard/line/linemodal/LineEmployeeModal";

import LineSearchItem from 'modules/pages/standard/line/LineSearchItem';
import LineMiddleItem from 'modules/pages/standard/line/LineMiddleItem';
import LineContents from 'modules/pages/standard/line/LineContents';


import LocationHelper from "components/codehelper/LocationHelper";
import EmployeeMultiHelper from 'components/codehelper/EmployeeMultiHelper';

@inject(stores => ({
    

     isLocationOpen: stores.lineStore.isLocationOpen
    , locationBuId: stores.lineStore.locationBuId
    , handleChange: stores.lineStore.handleChange
    , locationHelperResult: stores.lineStore.locationHelperResult
    , getEmpModalIsClose: stores.lineStore.getEmpModalIsClose
    
    , isEmpOpen: stores.lineStore.isEmpOpen
    , lotHelperResult: stores.lineStore.lotHelperResult
    , temp: stores.lineStore.temp

    
}))




class Line extends Component {

    constructor(props) {
        super(props);

       
    }

    render() {

        const { 
                isEmpOpen
                , lotHelperResult
                , temp
                , getEmpModalIsClose

                , isLocationOpen
                , locationBuId
                , handleChange
                , locationHelperResult
                
            } = this.props;

        return (
            <React.Fragment>
                <SearchTemplate searchItem={<LineSearchItem />} />
                <ContentsMiddleTemplate contentSubTitle={'라인 관리'} middleItem={<LineMiddleItem />} />
                <ContentsTemplate id='line' contentItem={<LineContents />} />
                
                <LocationHelper
                    id={'isLocationOpen'}
                    isLocationOpen={isLocationOpen}
                    buId={locationBuId}
                    buReadOnly={true}
                    onChange={handleChange}
                    onHelperResult={locationHelperResult}
                />

                {/* <EmployeeMultiHelper
                    id={'isEmpOpen'}
                    isEmpOpen={isEmpOpen}
                    handleChange={handleChange}
                    onHelperResult={lotHelperResult}
                    selectedEmployeeList={temp}
                    outsourcingYn={'Y'}
                /> */}

                <SModal
                    formId={'standard_line_employee_p'}
                    isOpen={isEmpOpen}
                    onRequestClose={getEmpModalIsClose}
                    contentLabel={'사용자 추가'}
                    width={1000}
                    height={450}
                    contents={<LineEmployeeModal />}
                />

            </React.Fragment>
        )
    }
}

export default Line;