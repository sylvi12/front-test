import API from 'components/override/api/API';

class LineRepository {
    
  
    URL = "/api/standard/line";
    
    //조회
    getLine(param) {
        return API.request.get(encodeURI(`${this.URL}/getline?buId=${param.buId}`))
    }

    //저장 및 수정
    saveLine(params) {
        return API.request.post(encodeURI(`${this.URL}/saveline`), params);
    }

    //삭제
    deleteLine(param) {
        return API.request.post(encodeURI(`${this.URL}/deleteline`), param);
    }

    //외주인력 조회
    getEmployee(param) {
        return API.request.post(encodeURI(`${this.URL}/getemployee`), param);
    }
    
}

export default new LineRepository();