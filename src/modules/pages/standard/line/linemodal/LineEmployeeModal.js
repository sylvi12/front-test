import React, { Component } from "react";
import { inject } from "mobx-react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SModal from "components/override/modal/SModal";
import SInput from 'components/atoms/input/SInput';
import SButton from 'components/atoms/button/SButton';
import SGrid from 'components/override/grid/SGrid';


@inject(stores => ({
buId: stores.lineStore.buId
, buList: stores.lineStore.buList
, handleChange: stores.lineStore.handleChange
, employee: stores.lineStore.employee
, modalEmployeeSearch: stores.lineStore.modalEmployeeSearch
, modalEmployeeSelect: stores.lineStore.modalEmployeeSelect
, setEmpGridApi: stores.lineStore.setEmpGridApi
, employeeList: stores.lineStore.employeeList
}))

//제품 UOM 구분별 조회
export default class LineEmployeeModal extends Component {

	constructor(props) {
		super(props);
	}

	render() {
		const {
			buId
			, handleChange
			, buList
			, employee
			, employeeList
			, modalEmployeeSearch
			, modalEmployeeSelect
			, setEmpGridApi
		} = this.props;

		return (
			<React.Fragment>
				<div style={{ width: '100%', height: '100%' }}>
					<SearchTemplate searchItem={
					<LineEmployeeModalSearchItem
						buId = {buId}
						buList = {buList}
						handleChange = {handleChange}
						employee = {employee}
						modalEmployeeSearch = {modalEmployeeSearch}
						/>} />
					<ContentsMiddleTemplate contentSubTitle={'외주인력'} middleItem={
					<LineEmployeeModalMiddleItem
						modalEmployeeSelect = {modalEmployeeSelect}
					/>} />
					<ContentsTemplate id='lineEmployeeModal_contents' contentItem={
						<LineEmployeeModalContentItem
						setEmpGridApi = {setEmpGridApi}
						employeeList = {employeeList}
						/>} />
				</div>
			</React.Fragment>
		);
	}
}
const LineEmployeeModalSearchItem = ({
	buId
	, buList
	, handleChange
	, employee
	, modalEmployeeSearch
}) => {
	return (
		<React.Fragment>
			<div className='search_item_btn'>
				<SButton
					buttonName={"조회"}
					type={"default"}
					onClick={modalEmployeeSearch}
				/>
			</div>
			<SSelectBox // 공장이 없는 사용자도 있기때문에 권한 없이 드롭다운 *수정자: 박성현  일시:2019.10.23
				title={"공장"}
				id={"buId"}
				value={buId}
				optionGroup={buList}
				valueMember={"buId"}
				displayMember={"buNm"}
				onChange={handleChange}
				addOption={"전체"}
			/>

			<SInput
				title={"사용자"}
				id={"employee"}
				value={employee}
				onChange={handleChange}
				onEnterKeyDown={modalEmployeeSearch}
				isFocus={true}
			/>
		</React.Fragment>
	)
}


const LineEmployeeModalMiddleItem = ({
	modalEmployeeSelect
}) => {
	return (
		<React.Fragment>
			<SButton
				buttonName={"선택"}
				type={"default"}
				onClick={modalEmployeeSelect}
			/>
		</React.Fragment>
	)
}

const LineEmployeeModalContentItem = ({
	setEmpGridApi
	, employeeList
 }) => {
	return (
		<SGrid
			grid={"lineEmployeeModalGrid"}
			gridApiCallBack={setEmpGridApi}
			rowData={employeeList}
			editable={false}
		/>
	)
}