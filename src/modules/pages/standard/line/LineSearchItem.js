import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton';
import SBuSelectBox from 'components/override/business/SBuSelectBox';

import LineStore from 'modules/pages/standard/line/service/LineStore';


@inject(stores => ({
    
      buId: stores.lineStore.buId
    , handleChange: stores.lineStore.handleChange
    , handleSearchClick: stores.lineStore.handleSearchClick
    , buSelectionChanged: stores.lineStore.buSelectionChanged
    
}))

class LineSearchItem extends Component {

    constructor(props) {
        super(props);
        LineStore.getBu();
    }

    render() {

        const {   buId
                , handleChange
                , handleSearchClick
                , buSelectionChanged } = this.props;

        return (
            <React.Fragment>
                <div>
                    <SButton 
                        buttonName={'조회'}
                        onClick={handleSearchClick}
                        type={'btnSearch'} 
                    />
                </div>
                    

                <SBuSelectBox
                    title={"공장"}
                    id={"buId"}
                    value={buId}
                    onChange={handleChange}
                    //onSelectionChange={buSelectionChanged}
                />
                   


                    
                 
            </React.Fragment>
        );
    }
}

export default LineSearchItem;