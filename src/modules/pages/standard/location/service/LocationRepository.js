import API from "components/override/api/API";

class LocationRepository {

    URL = "api/standard/location";

    getLocation(params) {
        return API.request.get(encodeURI(`${this.URL}/getLocation?buId=${params.buId}`))
    }

    saveLocation(params) {
        return API.request.post(encodeURI(`${this.URL}/savelocation`), params);
    }
}
export default new LocationRepository();