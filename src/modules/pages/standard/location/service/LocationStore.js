import { observable, action} from 'mobx';
import BaseStore from 'utils/base/BaseStore';

import GridUtil from 'components/override/grid/utils/GridUtil';

import Alert from 'components/override/alert/Alert';

import CommonStore from 'modules/pages/common/service/CommonStore';
import LocationRepository from 'modules/pages/standard/location/service/LocationRepository';


class LocationStore extends BaseStore {

    constructor() {
        super()
        this.setInitialState();
    }

    @observable buId = CommonStore.buId;
    
    //트리 변수
    @observable treeData;
    locationList = [];

    //제품군 부모키 변수
    pKey = '';

    locationGridApi = undefined;

    //공장 조회
    @action.bound
    getBu = async () => {
        this.buId = CommonStore,buId;
    }

    //위치정보 조회
    @action.bound
    handleSearchClick = () => {
        this.setLocation();
    }
    
    //데이터변화 확인
    @action.bound
    handleChange = (data) => {
        console.log(data)
        this[data.id] = data.value;
    }

    //제품군 자식 조회
    @action.bound
    handleSelect = async (selectedKey, info) => {
        this.locationGridApi.setRowData();
        let arr = info.node.children;
        this.pKey = info.node.key;

        const locList = this.locationList;
        
        let newArr = [];

        if(arr){
            locList.forEach(loc => {
                arr.map(item => {
                    if(item.key == loc.locId){
    
                       return newArr.push(({...item, loc}))
                    }
                })
            })
        }
        
        if(newArr) {
            newArr.forEach( e => {
                
                //자식 코드가 없을때
                if(!e.children) {
                    const item = {
                        locId: e.key,
                        whId: e.loc.whId,
                        locCd: e.loc.locCd,
                        locNm: e.title,
                        position: e.loc.position,
                        whYn: e.loc.whYn,
                        useYn: e.loc.useYn,
                        plocId: this.pKey,
                        child: ''
                    }
                    this.locationGridApi.updateRowData({ add: [item] });
                } else {
                    //자식 코드가 있을때
                    const item = {
                        locId: e.key,
                        whId: e.loc.whId,
                        locCd: e.loc.locCd,
                        locNm: e.title,
                        position: e.loc.position,
                        whYn: e.loc.whYn,
                        useYn: e.loc.useYn,
                        plocId: this.pKey,
                        child: 'Y'
                    }
                    this.locationGridApi.updateRowData({ add: [item] });
                }
            });
        } else {
            this.locationGridApi.setRowData();
        }
        

    }


    //위치 추가
    @action.bound
    addClick = () => {
        
        if(this.pKey.length == 0){
            Alert.meg("선택된 제품군이 없습니다.");
            return;
        }

        let count = this.locationGridApi.getDisplayedRowCount();

        const codeHdmt = {
            whId: '',
            locCd: '',
            locNm: '',
            plocId: this.pKey,
            position: '',
            whYn: 'Y',
            useYn: 'Y',
            newItem: 'Y'
        };
        const result = this.locationGridApi.updateRowData({ add: [codeHdmt], addIndex: count });
        GridUtil.setFocus(this.locationGridApi, result.add[0]);
        
    }

    //위치 저장
    @action.bound
    saveClick = async () => {

        //중복체크 변수
        let checkList = [];
        let checkCnt = 0;

        //필수입력 체크 변수
        let cntLocCd = 0;
        let cntLocNm = 0;

        const saveArr = GridUtil.getGridSaveArray(this.locationGridApi);
        if (saveArr.length == 0) {
            Alert.meg('저장할 데이터가 없습니다.');
            return;
        }

        this.locationGridApi.forEachNode(rowNode => {
            checkList.push(rowNode.data.locCd);
            if(rowNode.data.locCd.length < 1) {
                cntLocCd++
                return;
            } else if (rowNode.data.locNm.length < 1) {
                cntLocNm++
                return;
            }
        })

        if(cntLocCd > 0) {
            Alert.meg("위치코드가 입력되지 않았습니다.");
            return;
        } else if (cntLocNm > 0) {
            Alert.meg("위치명이 입력되지 않았습니다.");
            return;
        }

        for(let i = 0; i < checkList.length; i++) {
            for(let j = 0; j < i; j++) {
                if(checkList[i] == checkList[j]) {
                    checkCnt++
                    break;
                }
            }
        }

        if(checkCnt > 0) {
            Alert.meg("위치코드가 중복됬습니다.")
            return;
        }

        const params = JSON.stringify({ param: saveArr });
        
        const { data, status } = await LocationRepository.saveLocation(params);
        if (status == 200) {
            
            this.setLocation();
            // Alert.meg(data.msg);
            Alert.meg("저장되었습니다.");
        } else {
            Alert.meg(data.msg);
        }
        
        
    }

    /*위치 삭제
      - 데이터를 삭제하는게 아니라 추가로 된 row만 삭제한다.  */
      @action.bound
      deleteClick = async () => {
  
          const selectedRow = GridUtil.getSelectedRowData(this.locationGridApi);
          
          if(selectedRow) {
              if(selectedRow.newItem == 'Y') {
                  this.locationGridApi.updateRowData({ remove: [selectedRow] });
                  return;
              }
          } else return;
      }

    //위치 목록
    @action.bound
    setLocation = async () => {
        
        const params = {
            buId: this.buId
        }
        const { data, status } = await LocationRepository.getLocation(params);

        if (data.success) {
            
            this.treeData = data.data;

            //데이터 확인을 위해 갖고옴
            this.locationList = data.data;
            
        }
    }

    // 제품군 그리드
    @action.bound
    setLocationGridApi = async (gridApi) => {

        this.locationGridApi = gridApi;

        const columnDefs = [
            {
                headerName: "위치ID", field: "locId", width: 80
                , editable: function (params) {
                    if (params.node.data.newItem == 'Y') {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                , customCellClass: 'cell_align_center'
                , hide: true
            },
            { 
                headerName: "위치코드"
                , field: "locCd"
                , width: 100
                , cellClass: 'cell_align_left'
                , editable: function (params) {
                    if (params.node.data.newItem == 'Y') {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            },
            { 
                headerName: "창고ID"
                , field: "whId"
                , width: 80
                , cellClass: 'cell_align_left'
                , hide: true
            },
            { 
                headerName: "위치명"
                , field: "locNm"
                , width: 120
                , cellClass: 'cell_align_left'
            },
            { 
                headerName: "위치"
                , field: "position"
                , width: 120
                , cellClass: 'cell_align_left'
            },
            { 
                headerName: "창고여부"
                , field: "whYn"
                , width: 80
                , cellClass: 'cell_align_left'
            },
            { 
                headerName: "사용여부"
                , field: "useYn"
                , width: 80
                , cellClass: 'cell_align_left'
            }
            
            
        ];
        GridUtil.setSelectCellByCode(columnDefs[5], "SYS001");
        GridUtil.setSelectCellByCode(columnDefs[6], "SYS001");
        this.locationGridApi.setColumnDefs(columnDefs);
    }

}

export default new LocationStore();