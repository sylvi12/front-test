import React, { Component } from 'react';
import { inject } from 'mobx-react';
import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setLocationGridApi: stores.locationStore.setLocationGridApi
    , locationList: stores.locationStore.locationList
    
}))
class LocationContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setLocationGridApi, locationList } = this.props;

        return (
            <div className="panel_body" style={{ height: '100%' }}>
                <SGrid
                    grid={'locationGrid'}
                    gridApiCallBack={setLocationGridApi}
                    rowData={locationList}
                    cellReadOnlyColor={true}
                />
            </div>
        );
    }
}
export default LocationContents;