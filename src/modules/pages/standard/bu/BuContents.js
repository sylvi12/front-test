import React, { Component } from 'react';
import { inject } from 'mobx-react';
import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
      setBuGridApi: stores.buStore.setBuGridApi
    , buList: stores.buStore.buList
    
}))
class BuContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setBuGridApi, buList } = this.props;

        return (
            <div className="panel_body" style={{ height: '100%' }}>
                <SGrid
                    grid={'buGrid'}
                    gridApiCallBack={setBuGridApi}
                    // onSelectionChanged={handleMasterSelectionChanged}
                    // onCellClicked={buOnClick}
                    rowData={buList}
                    cellReadOnlyColor={true}
                />
            </div>
        );
    }
}
export default BuContents;