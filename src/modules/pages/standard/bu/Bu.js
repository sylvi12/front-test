import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate'
import ContentsTemplate from 'components/template/ContentsTemplate';

import BuMiddleItem from 'modules/pages/standard/bu/BuMiddleItem';
import BuContents from 'modules/pages/standard/bu/BuContents';

import BuStore from 'modules/pages/standard/bu/service/BuStore';

class Bu extends Component {

    constructor(props) {
        super(props);

        BuStore.getBu();
    }

    render() {

        return (
            <React.Fragment>

                <ContentsMiddleTemplate contentSubTitle={'공장 관리'} middleItem={<BuMiddleItem />} />
                <ContentsTemplate id='bu' contentItem={<BuContents />} />
                
            </React.Fragment>
        )
    }
}

export default Bu;