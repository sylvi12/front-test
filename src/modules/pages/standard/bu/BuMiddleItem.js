import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton'

@inject(stores => ({
      saveClick: stores.buStore.saveClick
    , addClick: stores.buStore.addClick
    , deleteClick: stores.buStore.deleteClick,
}))
class BuMiddleItem extends Component {

    render() {

        const { saveClick, addClick, deleteClick } = this.props;
        return (
            <React.Fragment>
                <SButton
                    buttonName={'저장'}
                    type={'btnSave'}
                    onClick={saveClick}
                />
                <SButton
                    buttonName={'추가'}
                    type={'btnAdd'}
                    onClick={addClick}
                />
                <SButton
                    buttonName={'삭제'}
                    type={'btnDelete'}
                    onClick={deleteClick}
                />
            </React.Fragment>
        )
    }
}

export default BuMiddleItem;