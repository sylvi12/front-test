import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid';

@inject(stores => ({
    setBarcodeTrackingGridApi: stores.barcodeTrackingStore.setBarcodeTrackingGridApi
   ,barcodeTrackingList: stores.barcodeTrackingStore.barcodeTrackingList
   ,selectBarcodeTrackingSearch: stores.barcodeTrackingStore.selectBarcodeTrackingSearch
}))

class BarcodeTrackingSearchContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setBarcodeTrackingGridApi, barcodeTrackingList, selectBarcodeTrackingSearch} = this.props;

        return (

            <SGrid
                grid={'barcodeTrackingGrid'}
                gridApiCallBack={setBarcodeTrackingGridApi}
                rowData={barcodeTrackingList}
                onSelectionChanged={selectBarcodeTrackingSearch}
                cellReadOnlyColor={false}
            />
        );
    }
}

export default BarcodeTrackingSearchContents;