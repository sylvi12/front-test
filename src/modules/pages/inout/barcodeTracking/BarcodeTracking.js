import React, { Component } from "react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import BarcodeTrackingSearchItem from "modules/pages/inout/barcodeTracking/BarcodeTrackingSearchItem";
import BarcodeTrackingContents from "modules/pages/inout/barcodeTracking/BarcodeTrackingContents";
import BarcodeTrackingRestContents from "modules/pages/inout/barcodeTracking/BarcodeTrackingRestContents";

import BarcodeTrackingSearchContents from "modules/pages/inout/barcodeTracking/BarcodeTrackingSearchContents";


class BarcodeTracking extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <React.Fragment>
                <SearchTemplate searchItem={<BarcodeTrackingSearchItem />} />

                <ContentsTemplate id='barcodeTracking' contentItem={

                    <div className='basic'>
                        <div style={{ width: '50%', height: '100%', float: 'left' }}>
                            <ContentsMiddleTemplate contentSubTitle={'출고이력'} />
                            <ContentsTemplate id='form' contentItem={<BarcodeTrackingSearchContents />} />
                        </div>
                        <div style={{ width: '50%', height: '100%', float: 'right' }}>
                            <div style={{height: '60%'}}>
                                <ContentsMiddleTemplate contentSubTitle={'배송처 목록'} />
                                <ContentsTemplate id='btn' contentItem={<BarcodeTrackingContents />} />
                            </div>
                            <div style={{height: '40%'}}>
                                <ContentsMiddleTemplate contentSubTitle={'창고 목록'} />
                                <ContentsTemplate id='btn2' contentItem={<BarcodeTrackingRestContents />} />
                            </div>
                        </div>
                    </div>
                } />
            </React.Fragment>
        )
    }
}

export default BarcodeTracking;