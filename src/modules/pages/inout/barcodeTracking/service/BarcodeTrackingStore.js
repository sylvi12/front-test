import { observable, action } from 'mobx';
import moment from 'moment';
import 'moment/locale/ko';

import Alert from 'components/override/alert/Alert';
import GridUtil from 'components/override/grid/utils/GridUtil';

import CommonStore from 'modules/pages/common/service/CommonStore';
import BarcodeTrackingRepository from 'modules/pages/inout/barcodeTracking/service/BarcodeTrackingRepository';

class BarcodeTrackingStore {

    @observable buId = CommonStore.buId;
    @observable item_search = '';
    @observable destination_search = '';

    @observable inoutFromDt = moment().format('YYYYMMDD');
    @observable inoutToDt = moment().format('YYYYMMDD');
    @observable prodDt = '';

    @observable checkTitle = '바코드모드'
    @observable isBarcodeGroup = 'Y'
    @observable woOrderNo_search = ''
    @observable orderLine = ''
    @observable orderLine = ''

    isBarcodeGroupList = [
        { cd: "Y", cdNm: "바코드" },
        { cd: "N", cdNm: "수량" },
    ];

    // 출고 조회 목록
    barcodeTrackingList = [];
    BarcodeTrackingGridApi = undefined;

    // 배송처 바코드 목록
    barcodeTrackingResultList = [];
    BarcodeTrackingResultGridApi = undefined;

    // 창고 바코드 목록
    barcodeTrackingRestList = [];
    BarcodeTrackingRestGridApi = undefined;

    @action.bound
    handleChange = (data) => {
        this[data.id] = data.value;

        if (data.id == 'isBarcodeGroup') {
            switch (data.value) {
                case 'Y':
                    this.checkTitle = '바코드모드'
                    break
                case 'N':
                    this.checkTitle = '수량모드'
                    break
            }
            this.clickBarcodeTrackingSearch()
        }
    }

    @action.bound
    setBarcodeTrackingGridApi = async (gridApi) => {

        this.BarcodeTrackingGridApi = gridApi;

        var columnDefs = [];
        if (this.isBarcodeGroup == 'Y') {
            columnDefs = [
                {
                    headerName: "주문번호"
                    , field: "orderLine"
                    , width: 77
                    , editable: false
                    , customCellClass: 'cell_align_center'
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params)
                    }
                },
                {
                    headerName: "배송처"
                    , field: "targetSNm"
                    , width: 150
                    , editable: false
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params, false, ['orderLine', 'targetSNm'])
                    }
                },
                {
                    headerName: "출고일"
                    , field: "confirmDtm"
                    , width: 75
                    , editable: false
                    , customCellClass: 'cell_align_center'
                    , valueFormatter: GridUtil.dateFormatter
                },
                {
                    headerName: "바코드"
                    , field: "barcode"
                    , width: 88
                    , editable: false
                    , customCellClass: 'cell_align_center'
                },
                {
                    headerName: "제품코드"
                    , field: "itemCd"
                    , width: 80
                    , editable: false
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params, false, ['orderLine', 'itemCd'])
                    }
                },
                {
                    headerName: "제품명"
                    , field: "itemNm"
                    , width: 110
                    , editable: false
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params, false, ['orderLine', 'itemNm'])
                    }
                },
                {
                    headerName: "배차수량"
                    , field: "totalQty"
                    , width: 80
                    , editable: false
                },
                {
                    headerName: "제조일자"
                    , field: "prodDt"
                    , width: 80
                    , editable: false
                    , customCellClass: 'cell_align_center'
                    , valueFormatter: GridUtil.dateFormatter
                },
                {
                    headerName: "지시번호"
                    , field: "woOrderNo"
                    , width: 60
                    , editable: false
                    , customCellClass: 'cell_align_center'
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params, false, ['orderLine', 'woOrderNo'])
                    }
                }
            ];
        }
        else {
            columnDefs = [
                {
                    headerName: "주문번호"
                    , field: "orderLine"
                    , width: 80
                    , editable: false
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params)
                    }
                },
                {
                    headerName: "배송처"
                    , field: "targetSNm"
                    , width: 220
                    , editable: false
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params, false, ['orderLine', 'targetSNm'])
                    }
                },
                {
                    headerName: "제품코드"
                    , field: "itemCd"
                    , width: 80
                    , editable: false
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params, false, ['orderLine', 'itemCd'])
                    }
                },
                {
                    headerName: "제품명"
                    , field: "itemNm"
                    , width: 150
                    , editable: false
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params, false, ['orderLine', 'itemNm'])
                    }
                },
                {
                    headerName: "배차수량"
                    , field: "totalQty"
                    , width: 80
                    , editable: false
                },
                {
                    headerName: "제조일자"
                    , field: "prodDt"
                    , width: 80
                    , editable: false
                    , customCellClass: 'cell_align_center'
                    , valueFormatter: GridUtil.dateFormatter
                },
                {
                    headerName: "지시번호"
                    , field: "woOrderNo"
                    , width: 60
                    , editable: false
                    , customCellClass: 'cell_align_center'
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params, false, ['orderLine', 'woOrderNo'])
                    }
                }
            ];
        }


        this.BarcodeTrackingGridApi.setColumnDefs(columnDefs);
    }

    @action.bound
    setBarcodeTrackingResultGridApi = async (gridApi) => {

        this.BarcodeTrackingResultGridApi = gridApi;

        var columnDefs = [];
        if (this.isBarcodeGroup == 'Y') {
            columnDefs = [
                {
                    headerName: "지시번호"
                    , field: "woOrderNo"
                    , width: 60
                    , editable: false
                    , customCellClass: 'cell_align_center'
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params)
                    }
                },
                {
                    headerName: "바코드"
                    , field: "barcode"
                    , width: 90
                    , editable: false
                },
                {
                    headerName: "제품코드"
                    , field: "itemCd"
                    , width: 80
                    , editable: false
                },
                {
                    headerName: "제품명"
                    , field: "itemNm"
                    , width: 200
                    , editable: false
                },
                {
                    headerName: "제조일자"
                    , field: "prodDt"
                    , width: 80
                    , editable: false
                    , customCellClass: 'cell_align_center'
                    , valueFormatter: GridUtil.dateFormatter
                },
                {
                    headerName: "배송처"
                    , field: "targetSNm"
                    , width: 220
                    , editable: false
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params, false, ['woOrderNo', 'targetSNm'])
                    }
                },
                {
                    headerName: "배차수량"
                    , field: "totalQty"
                    , width: 80
                    , editable: false
                },
            ];
        }
        else {
            columnDefs = [
                {
                    headerName: "지시번호"
                    , field: "woOrderNo"
                    , width: 60
                    , editable: false
                    , customCellClass: 'cell_align_center'
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params)
                    }
                },
                {
                    headerName: "제품코드"
                    , field: "itemCd"
                    , width: 80
                    , editable: false
                },
                {
                    headerName: "제품명"
                    , field: "itemNm"
                    , width: 200
                    , editable: false
                },
                {
                    headerName: "제조일자"
                    , field: "prodDt"
                    , width: 80
                    , editable: false
                    , customCellClass: 'cell_align_center'
                    , valueFormatter: GridUtil.dateFormatter
                },
                {
                    headerName: "배송처"
                    , field: "targetSNm"
                    , width: 220
                    , editable: false
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params, false, ['woOrderNo', 'targetSNm'])
                    }
                },
                {
                    headerName: "배차수량"
                    , field: "totalQty"
                    , width: 80
                    , editable: false
                },
            ];
        }

        this.BarcodeTrackingResultGridApi.setColumnDefs(columnDefs);
    }

    @action.bound
    setBarcodeTrackingRestGridApi = async (gridApi) => {

        this.BarcodeTrackingRestGridApi = gridApi;

        var columnDefs = [];
        if (this.isBarcodeGroup == 'Y') {
            columnDefs = [
                {
                    headerName: "지시번호"
                    , field: "woOrderNo"
                    , width: 60
                    , editable: false
                    , customCellClass: 'cell_align_center'
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params)
                    }
                },
                {
                    headerName: "바코드"
                    , field: "barcode"
                    , width: 90
                    , editable: false
                },
                {
                    headerName: "제품코드"
                    , field: "itemCd"
                    , width: 80
                    , editable: false
                },
                {
                    headerName: "제품명"
                    , field: "itemNm"
                    , width: 200
                    , editable: false
                },
                {
                    headerName: "총재고"
                    , field: "totalQty"
                    , width: 80
                    , editable: false
                },
                {
                    headerName: "제조일자"
                    , field: "prodDt"
                    , width: 80
                    , editable: false
                    , customCellClass: 'cell_align_center'
                    , valueFormatter: GridUtil.dateFormatter
                },
                {
                    headerName: "창고"
                    , field: "locNm"
                    , width: 220
                    , editable: false
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params, false, ['woOrderNo', 'locNm'])
                    }
                },
            ];
        }
        else {
            columnDefs = [
                {
                    headerName: "지시번호"
                    , field: "woOrderNo"
                    , width: 60
                    , editable: false
                    , customCellClass: 'cell_align_center'
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params)
                    }
                },
                {
                    headerName: "제품코드"
                    , field: "itemCd"
                    , width: 80
                    , editable: false
                },
                {
                    headerName: "제품명"
                    , field: "itemNm"
                    , width: 200
                    , editable: false
                },
                {
                    headerName: "총재고"
                    , field: "totalQty"
                    , width: 80
                    , editable: false
                },
                {
                    headerName: "제조일자"
                    , field: "prodDt"
                    , width: 80
                    , editable: false
                    , customCellClass: 'cell_align_center'
                    , valueFormatter: GridUtil.dateFormatter
                },
                {
                    headerName: "창고"
                    , field: "locNm"
                    , width: 220
                    , editable: false
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params, false, ['woOrderNo', 'locNm'])
                    }
                },
            ];
        }

        this.BarcodeTrackingRestGridApi.setColumnDefs(columnDefs);
    }

    // 조회
    @action.bound
    clickBarcodeTrackingSearch = async () => {

        if (this.prodDt == null) {
            this.prodDt = ''
        }
        this.prodDt = this.prodDt.replace(/-/gi, '')

        if (isNaN(this.woOrderNo_search)) {
            Alert.meg("지시번호가 잘못되었습니다. 다시 입력해 주세요.");
            this.woOrderNo_search = ''
            return;
        }

        const submitdata = {
            "item_search": this.item_search
            , "destination_search": this.destination_search
            , "inoutFromDt": this.inoutFromDt
            , "inoutToDt": this.inoutToDt
            , "prodDtm": this.prodDt
            , "buId": this.buId
            , "isBarcodeGroup": this.isBarcodeGroup
            , "woOrderNo": this.woOrderNo_search
            , "orderLine": this.orderLine
        }
        this.prodDt = ''
        const { data, status } = await BarcodeTrackingRepository.getBarcodeTracking(submitdata);

        if (status == 200) {
            data.data.forEach(item => {
                if (item.woOrderNo == null) {
                    item.woOrderNo = '기타입고'
                }
            })
            this.barcodeTrackingList = data.data;
            this.BarcodeTrackingGridApi.setRowData(this.barcodeTrackingList);
            this.setBarcodeTrackingGridApi(this.BarcodeTrackingGridApi);

            if (this.woOrderNo_search != '') {
                const searchdata = {
                    "isBarcodeGroup": this.isBarcodeGroup
                    , "woOrderNo": this.woOrderNo_search
                }

                const { data, status } = await BarcodeTrackingRepository.getBarcodeTrackingResult(searchdata);
                if (status == 200) {
                    this.barcodeTrackingResultList = data.data;
                    this.BarcodeTrackingResultGridApi.setRowData(this.barcodeTrackingResultList);
                    this.setBarcodeTrackingResultGridApi(this.BarcodeTrackingResultGridApi)
                    const result = await BarcodeTrackingRepository.getBarcodeTrackingRest(searchdata);
                    if (result.status == 200) {
                        this.barcodeTrackingRestList = result.data.data;
                        this.BarcodeTrackingRestGridApi.setRowData(this.barcodeTrackingRestList);
                        this.setBarcodeTrackingRestGridApi(this.BarcodeTrackingRestGridApi)
                    }
                }
            }
            else {
                this.BarcodeTrackingRestGridApi.setRowData([]);
                this.BarcodeTrackingResultGridApi.setRowData([]);
            }
        }


    }

    // 조회
    @action.bound
    selectBarcodeTrackingSearch = async () => {

        const selectedRows = GridUtil.getSelectedRowData(this.BarcodeTrackingGridApi);

        if (isNaN(selectedRows.woOrderNo)) {
            this.BarcodeTrackingRestGridApi.setRowData([]);
            this.BarcodeTrackingResultGridApi.setRowData([]);
            return;
        }

        const submitdata = {
            "isBarcodeGroup": this.isBarcodeGroup
            , "woOrderNo": selectedRows.woOrderNo
        }

        this.woOrderNo_search = selectedRows.woOrderNo
        const { data, status } = await BarcodeTrackingRepository.getBarcodeTrackingResult(submitdata);

        if (status == 200) {
            this.barcodeTrackingResultList = data.data;
            this.BarcodeTrackingResultGridApi.setRowData(this.barcodeTrackingResultList);
            this.setBarcodeTrackingResultGridApi(this.BarcodeTrackingResultGridApi)
            const result = await BarcodeTrackingRepository.getBarcodeTrackingRest(submitdata);
            if (result.status == 200) {
                this.barcodeTrackingRestList = result.data.data;
                this.BarcodeTrackingRestGridApi.setRowData(this.barcodeTrackingRestList);
                this.setBarcodeTrackingRestGridApi(this.BarcodeTrackingRestGridApi)
            }
        }
    }
}

export default new BarcodeTrackingStore();