import API from "components/override/api/API";

class BarcodeTrackingRepository {

    URL = "/api/inout";
    // 조회
    getBarcodeTracking(params) {
        return API.request.get(encodeURI(`${this.URL}/getbarcodetracking?searchItem=${params.item_search}&prodDtm=${params.prodDtm}&startDtm=${params.inoutFromDt}&endDtm=${params.inoutToDt}&targetSNm=${params.destination_search}&buId=${params.buId}&isBarcodeGroup=${params.isBarcodeGroup}&orderLine=${params.orderLine}&woOrderNo=${params.woOrderNo}`))
    }

    getBarcodeTrackingResult(params) {
        return API.request.get(encodeURI(`${this.URL}/getbarcodetrackingresult?woOrderNo=${params.woOrderNo}&isBarcodeGroup=${params.isBarcodeGroup}`))
    }

    getBarcodeTrackingRest(params) {
        return API.request.get(encodeURI(`${this.URL}/getbarcodetrackingrest?woOrderNo=${params.woOrderNo}&isBarcodeGroup=${params.isBarcodeGroup}`))
    }
}
export default new BarcodeTrackingRepository();