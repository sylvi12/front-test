import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SButton from 'components/atoms/button/SButton';
import SCheckBox from 'components/atoms/checkbox/SCheckBox';
import SBuSelectBox from 'components/override/business/SBuSelectBox';
import SRadio from 'components/atoms/radio/SRadio'
import SDatePicker from 'components/override/datepicker/SDatePicker'

@inject(stores => ({
    item_search: stores.barcodeTrackingStore.item_search
    , handleChange: stores.barcodeTrackingStore.handleChange
    , clickBarcodeTrackingSearch: stores.barcodeTrackingStore.clickBarcodeTrackingSearch
    , destination_search: stores.barcodeTrackingStore.destination_search
    , inoutFromDt: stores.barcodeTrackingStore.inoutFromDt
    , inoutToDt: stores.barcodeTrackingStore.inoutToDt
    , prodDt: stores.barcodeTrackingStore.prodDt
    , buId: stores.barcodeTrackingStore.buId
    , woOrderNo_search: stores.barcodeTrackingStore.woOrderNo_search
    , isBarcodeGroup: stores.barcodeTrackingStore.isBarcodeGroup
    , isBarcodeGroupList: stores.barcodeTrackingStore.isBarcodeGroupList
    , checkTitle: stores.barcodeTrackingStore.checkTitle
    , orderLine: stores.barcodeTrackingStore.orderLine
}))

class BarcodeTrackingSearchItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { orderLine, isBarcodeGroup, isBarcodeGroupList, woOrderNo_search, item_search, handleChange, clickBarcodeTrackingSearch, destination_search, buId, inoutFromDt, inoutToDt, prodDt, checkTitle } = this.props;

        return (

            <React.Fragment>
                <SBuSelectBox
                    title={"공장"}
                    id={"buId"}
                    value={buId}
                    isAddAll={false}
                    onChange={handleChange}
                // onSelectionChange={buSelectionChanged}
                />

                <SInput
                    title={"지시번호"}
                    id={"woOrderNo_search"}
                    value={woOrderNo_search}
                    // contentWidth={100}
                    onChange={handleChange}
                    onEnterKeyDown={clickBarcodeTrackingSearch}
                />

                <SInput
                    title={"주문번호"}
                    id={"orderLine"}
                    value={orderLine}
                    // contentWidth={100}
                    onChange={handleChange}
                    onEnterKeyDown={clickBarcodeTrackingSearch}
                />

                <SDatePicker
                    title={"출고일자"}
                    id={"inoutFromDt"}
                    value={inoutFromDt}
                    onChange={handleChange}

                />
                <SDatePicker
                    title={"~"}
                    id={"inoutToDt"}
                    value={inoutToDt}
                    onChange={handleChange}
                />

                <SDatePicker
                    title={"제조일자"}
                    id={"prodDt"}
                    value={prodDt}
                    onChange={handleChange}
                />
                <SInput
                    title={"제품명/코드"}
                    id={"item_search"}
                    value={item_search}
                    onChange={handleChange}
                    onEnterKeyDown={clickBarcodeTrackingSearch}
                />
                <SInput
                    title={"배송처"}
                    id={"destination_search"}
                    value={destination_search}
                    onChange={handleChange}
                    onEnterKeyDown={clickBarcodeTrackingSearch}
                />

                <SRadio title={"기준"}
                    id={"isBarcodeGroup"}
                    value={isBarcodeGroup}
                    optionGroup={isBarcodeGroupList}
                    onChange={handleChange} />

                <div className='search_item_btn'>
                    <SButton
                        buttonName={'조회'}
                        type={'btnSearch'}
                        onClick={clickBarcodeTrackingSearch} />
                </div>
            </React.Fragment>
        )
    }
}

export default BarcodeTrackingSearchItem;