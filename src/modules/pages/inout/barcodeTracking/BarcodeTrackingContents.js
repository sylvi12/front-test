import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setBarcodeTrackingResultGridApi: stores.barcodeTrackingStore.setBarcodeTrackingResultGridApi,
    barcodeTrackingResultList: stores.barcodeTrackingStore.barcodeTrackingResultList,
}))

class BarcodeTrackingContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setBarcodeTrackingResultGridApi, barcodeTrackingResultList} = this.props;

        return (
            <SGrid
                grid={'barcodeTrackingResultGrid'}
                gridApiCallBack={setBarcodeTrackingResultGridApi}
                rowData={barcodeTrackingResultList}
                cellReadOnlyColor={false}
            />
        );
    }
}
export default BarcodeTrackingContents;