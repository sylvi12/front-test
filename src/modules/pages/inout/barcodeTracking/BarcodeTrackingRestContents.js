import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setBarcodeTrackingRestGridApi: stores.barcodeTrackingStore.setBarcodeTrackingRestGridApi,
    barcodeTrackingRestList: stores.barcodeTrackingStore.barcodeTrackingRestList,
}))

class BarcodeTrackingRestContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setBarcodeTrackingRestGridApi, barcodeTrackingRestList} = this.props;

        return (
            <SGrid
                grid={'barcodeTrackingRestGridApi'}
                gridApiCallBack={setBarcodeTrackingRestGridApi}
                rowData={barcodeTrackingRestList}
                cellReadOnlyColor={false}
            />
        );
    }
}
export default BarcodeTrackingRestContents;