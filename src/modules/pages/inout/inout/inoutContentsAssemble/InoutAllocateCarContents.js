import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setAllocateCarGridApi: stores.inoutStore.setAllocateCarGridApi
    , allocateCarList: stores.inoutStore.allocateCarList
    , getTheOthers: stores.inoutStore.getTheOthers
}))

class InoutAllocateCarContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setAllocateCarGridApi, allocateCarList, getTheOthers } = this.props;

        return (
            <SGrid
                grid={'allocateCarGrid'}
                gridApiCallBack={setAllocateCarGridApi}
                rowData={allocateCarList}
                onSelectionChanged={getTheOthers}
                editable={false}
            />
        );
    }
}
export default InoutAllocateCarContents;