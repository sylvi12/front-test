import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setPalletGridApi: stores.inoutStore.setPalletGridApi
    , palletList: stores.inoutStore.palletList
    , pinnedBottomRowDataAtPalletGrid: stores.inoutStore.pinnedBottomRowDataAtPalletGrid
    , editPalletGrid: stores.inoutStore.editPalletGrid
}))

class InoutPalletContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setPalletGridApi, palletList, pinnedBottomRowDataAtPalletGrid, editPalletGrid } = this.props;

        return (
            <SGrid
                grid={'palletGrid'}
                gridApiCallBack={setPalletGridApi}
                rowData={palletList}
                pinnedBottomRowData={pinnedBottomRowDataAtPalletGrid}
                suppressRowClickSelection={true}
                headerHeight={0}
            />
        );
    }
}
export default InoutPalletContents;