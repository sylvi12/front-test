import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    locationMove: stores.inoutStore.locationMove
    , locationButtonAvailableToggle: stores.inoutStore.locationButtonAvailableToggle

    , addPallet: stores.inoutStore.addPallet
    , addButtonAvailableToggle: stores.inoutStore.addButtonAvailableToggle

    , savePallet: stores.inoutStore.savePallet
    , saveButtonAvailableToggle: stores.inoutStore.saveButtonAvailableToggle

    , deletePallet: stores.inoutStore.deletePallet
    , deleteButtonAvailableToggle: stores.inoutStore.deleteButtonAvailableToggle
}))

class InoutPalletMiddleItem extends Component {

    render() {

        const { locationMove, locationButtonAvailableToggle
            , addPallet, addButtonAvailableToggle
            , savePallet, saveButtonAvailableToggle
            , deletePallet, deleteButtonAvailableToggle
        } = this.props;
        return (
            <React.Fragment>
                <div style={{ display: "flex" }}>

                    <SButton
                        className="btn_red"
                        buttonName={'위치변경'}
                        type={'btnLocationMove'}
                        onClick={locationMove}
                        visible={locationButtonAvailableToggle}
                    />
                    <SButton
                        className="btn_red"
                        buttonName={'추가'}
                        type={'btnAddBarcode'}
                        onClick={addPallet}
                        visible={addButtonAvailableToggle}
                    />
                    <SButton
                        className="btn_red"
                        buttonName={'수정'}
                        type={'btnSaveBarcode'}
                        onClick={savePallet}
                        visible={addButtonAvailableToggle}
                    />
                    <SButton
                        className="btn_red"
                        buttonName={'삭제'}
                        type={'btnDeleteBarcode'}
                        onClick={deletePallet}
                        visible={deleteButtonAvailableToggle}
                    />
                </div>
            </React.Fragment>
        )
    }
}

export default InoutPalletMiddleItem;