import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setOrderGridApi: stores.inoutStore.setOrderGridApi
    , orderList: stores.inoutStore.orderList
    , pinnedBottomRowDataAtOrderGrid: stores.inoutStore.pinnedBottomRowDataAtOrderGrid
    , getBarcode: stores.inoutStore.getBarcode
}))

class InoutOrderContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setOrderGridApi, orderList, pinnedBottomRowDataAtOrderGrid, getBarcode } = this.props;

        return (
            <SGrid
                grid={'orderGrid'}
                gridApiCallBack={setOrderGridApi}
                rowData={orderList}
                editable={false}
                onSelectionChanged={getBarcode}
                pinnedBottomRowData={pinnedBottomRowDataAtOrderGrid}
                headerHeight={0}
            />
        );
    }
}
export default InoutOrderContents;