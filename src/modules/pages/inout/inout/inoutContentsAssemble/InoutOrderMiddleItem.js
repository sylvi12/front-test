import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    orderConfirm: stores.inoutStore.orderConfirm
    , confirmButtonAvailableToggle: stores.inoutStore.confirmButtonAvailableToggle
}))

class InoutOrderMiddleItem extends Component {

    render() {

        const { orderConfirm, confirmButtonAvailableToggle } = this.props;
        return (
            <React.Fragment>
                <SButton
                    className="btn_red"
                    buttonName={'확정'}
                    type={'btnConfirm'}
                    onClick={orderConfirm}
                    visible={confirmButtonAvailableToggle}
                />
            </React.Fragment>
        )
    }
}

export default InoutOrderMiddleItem;