import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SButton from 'components/atoms/button/SButton';
import SInput from 'components/atoms/input/SInput';
import SRadio from 'components/atoms/radio/SRadio'
import SBuSelectBox from 'components/override/business/SBuSelectBox';

import InoutStore from "modules/pages/inout/inout/service/InoutStore";

@inject(stores => ({
    buId: stores.inoutStore.buId
    , handleChange: stores.inoutStore.handleChange
    , startDate: stores.inoutStore.startDate
    , endDate: stores.inoutStore.endDate
    , tmddocno: stores.inoutStore.tmddocno

    , inoutType: stores.inoutStore.inoutType
    , inoutTypeSelectionChanged: stores.inoutStore.inoutTypeSelectionChanged
    , ledgerType: stores.inoutStore.ledgerType
    , inoutStatus: stores.inoutStore.inoutStatus
    , inoutStep: stores.inoutStore.inoutStep

    , itemNmOrItemCd: stores.inoutStore.itemNmOrItemCd
    , targetNm: stores.inoutStore.targetNm
    , carNmOrCarNo: stores.inoutStore.carNmOrCarNo
    , inoutNum: stores.inoutStore.inoutNum
    , inoutList: stores.inoutStore.inoutList    
    , handleSearchClick: stores.inoutStore.handleSearchClick
    , getBu: stores.inoutStore.getBu
}))

class InoutSearchItem extends Component {

    constructor(props) {
        super(props);
    }

    async componentWillMount() {
        if (InoutStore.alphaCheck == false) {
            await this.props.getBu();
        }
    }

    render() {

        const { buId, handleChange
            , startDate, endDate
            , tmddocno

            , inoutType, inoutTypeSelectionChanged
            , ledgerType
            , inoutStatus
            , inoutStep
            , inoutList
            , itemNmOrItemCd
            , targetNm
            , carNmOrCarNo
            , inoutNum

            , handleSearchClick
        } = this.props;

        console.log(inoutStatus, inoutList , '???테스트')
        return (
            <React.Fragment>
                <div className='search_item_btn'>
                    <SButton
                        buttonName={'조회'}
                        type={'btnSearch'}
                        onClick={handleSearchClick}
                    />
                </div>
                <div>
                    <div className='search_line'>
                        <SBuSelectBox
                            title={"공장"}
                            id={"buId"}
                            value={buId}
                            onChange={handleChange}
                        />
                        <SDatePicker
                            title={"입출고 일자"}
                            id={"startDate"}
                            value={startDate}
                            onChange={handleChange}
                        />
                        {/* <SDatePicker
                            title={"~"}
                            id={"endDate"}
                            value={endDate}
                            onChange={handleChange}
                        /> */}
                        <SInput
                            title={"배차 문서번호"}
                            id={"tmddocno"}
                            value={tmddocno}
                            onChange={handleChange}
                            onEnterKeyDown={handleSearchClick}
                        />
                    </div>
                    <div className='search_line'>
                        <SRadio
                            title={'입출고 구분'}
                            id={"inoutType"}
                            value={inoutType}
                            codeGroup={"MFD010"}
                            onChange={handleChange}
                            onSelectionChange={inoutTypeSelectionChanged}
                        />
                        <SSelectBox
                            title={'수불타입'}
                            id={"ledgerType"}
                            value={ledgerType}
                            codeGroup={"MFD002"}
                            onChange={handleChange}
                            addOption={"전체"}
                        />
                        <SSelectBox
                            title={'SLS 상태'}
                            id={"inoutStatus"}
                            value={inoutStatus}
                            onChange={handleChange}
                            optionGroup={inoutList}
                            valueMember={"cd"}
                            displayMember={"cdNm"}
                            addOption={"전체"}
                        />
                        <SSelectBox
                            title={'WMS 상태'}
                            id={'inoutStep'}
                            value={inoutStep}
                            codeGroup={"MFD012"}
                            onChange={handleChange}
                            addOption={"전체"}
                        />
                    </div>
                    <div className='search_line'>
                        <SInput
                            title={"제품"}
                            id={"itemNmOrItemCd"}
                            value={itemNmOrItemCd}
                            onChange={handleChange}
                            onEnterKeyDown={handleSearchClick}
                        />
                        <SInput
                            title={"거래처"}
                            id={"targetNm"}
                            value={targetNm}
                            onChange={handleChange}
                            onEnterKeyDown={handleSearchClick}
                        />
                        <SInput
                            title={"차량"}
                            id={"carNmOrCarNo"}
                            value={carNmOrCarNo}
                            onChange={handleChange}
                            onEnterKeyDown={handleSearchClick}
                        />
                        <SInput
                            title={"차수"}
                            id={"inoutNum"}
                            value={inoutNum}
                            onChange={handleChange}
                            onEnterKeyDown={handleSearchClick}
                        />
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default InoutSearchItem;