import React, { Component } from "react";
import { inject } from "mobx-react";

import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import InoutAllocateCarMiddleItem from "modules/pages/inout/inout/inoutContentsAssemble/InoutAllocateCarMiddleItem";
import InoutAllocateCarContents from "modules/pages/inout/inout/inoutContentsAssemble/InoutAllocateCarContents";
import InoutOrderMiddleItem from "modules/pages/inout/inout/inoutContentsAssemble/InoutOrderMiddleItem";
import InoutOrderContents from "modules/pages/inout/inout/inoutContentsAssemble/InoutOrderContents";
import InoutPalletMiddleItem from "modules/pages/inout/inout/inoutContentsAssemble/InoutPalletMiddleItem";
import InoutPalletContents from "modules/pages/inout/inout/inoutContentsAssemble/InoutPalletContents";


@inject(stores => ({
}))

class InoutContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { } = this.props

        return (
            <React.Fragment>
                <div style={{ width: '504px', height: '100%', float: 'left'}}>
                    <ContentsMiddleTemplate contentSubTitle={'배차정보'} middleItem={<InoutAllocateCarMiddleItem />} />
                    <ContentsTemplate id='allocateCar' contentItem={<InoutAllocateCarContents />} />
                </div>
                <div style={{ width: 'calc(100% - 504px)', height: '100%', float: 'left'}}>
                    <div style={{ height: '34%' }}>
                        <ContentsMiddleTemplate contentSubTitle={'주문/품목'} middleItem={<InoutOrderMiddleItem />} />
                        <ContentsTemplate id='order' contentItem={<InoutOrderContents />} />
                    </div>
                    <div style={{ height: '66%' }}>
                        <ContentsMiddleTemplate contentSubTitle={'바코드 내역'} middleItem={<InoutPalletMiddleItem />} />
                        <ContentsTemplate id='pallet' contentItem={<InoutPalletContents />} />
                    </div>
                </div>

            </React.Fragment>
        )
    }
}

export default InoutContents;