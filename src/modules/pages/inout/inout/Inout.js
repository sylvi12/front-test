import React, { Component } from "react";
import { inject } from "mobx-react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import LocationHelper from "components/codehelper/LocationHelper";
import LotHelper from "components/codehelper/LotHelper";

import InoutSearchItem from "modules/pages/inout/inout/InoutSearchItem";
import InoutAllocateCarMiddleItem from "modules/pages/inout/inout/inoutContentsAssemble/InoutAllocateCarMiddleItem";
import InoutAllocateCarContents from "modules/pages/inout/inout/inoutContentsAssemble/InoutAllocateCarContents";
import InoutOrderMiddleItem from "modules/pages/inout/inout/inoutContentsAssemble/InoutOrderMiddleItem";
import InoutOrderContents from "modules/pages/inout/inout/inoutContentsAssemble/InoutOrderContents";
import InoutPalletMiddleItem from "modules/pages/inout/inout/inoutContentsAssemble/InoutPalletMiddleItem";
import InoutPalletContents from "modules/pages/inout/inout/inoutContentsAssemble/InoutPalletContents";
import InoutContents from "modules/pages/inout/inout/InoutContents";

@inject(stores => ({
    isLocationOpen: stores.inoutStore.isLocationOpen
    , locationBuId: stores.inoutStore.locationBuId
    , handleChange: stores.inoutStore.handleChange
    , locationHelperResult: stores.inoutStore.locationHelperResult

    , isLotOpen: stores.inoutStore.isLotOpen
    , lotHelperinoutDetailNo: stores.inoutStore.lotHelperinoutDetailNo
    , lotHelperResult: stores.inoutStore.lotHelperResult
    , palletList: stores.inoutStore.palletList
}))

class Inout extends Component {

    constructor(props) {
        super(props);
    }
    
    render() {

        const { isLocationOpen, locationBuId, handleChange, locationHelperResult
            , isLotOpen, lotHelperinoutDetailNo, lotHelperResult, palletList } = this.props

        return (
            <React.Fragment>
                <SearchTemplate searchItem={<InoutSearchItem />} />                
                {/* <div style={{ width: '35%', height: '100%', float: 'left' }}>
                    <ContentsMiddleTemplate contentSubTitle={'배차정보'} middleItem={<InoutAllocateCarMiddleItem />} />
                    <ContentsTemplate id='allocateCar' contentItem={<InoutAllocateCarContents />} />
                </div>
                <div style={{ width: '65%', height: '100%', float: 'left' }}>
                    <div style={{ height: '34%' }}>
                        <ContentsMiddleTemplate contentSubTitle={'주문/품목'} middleItem={<InoutOrderMiddleItem />} />
                        <ContentsTemplate id='order' contentItem={<InoutOrderContents />} />
                    </div>
                    <div style={{ height: '66%' }}>
                        <ContentsMiddleTemplate contentSubTitle={'바코드 내역'} middleItem={<InoutPalletMiddleItem />} />
                        <ContentsTemplate id='pallet' contentItem={<InoutPalletContents />} />
                    </div>
                </div> */}
                <ContentsTemplate id='inout' contentItem={<InoutContents />} />

                <LocationHelper
                    id={'isLocationOpen'}
                    isLocationOpen={isLocationOpen}
                    buId={locationBuId}
                    buReadOnly={true}
                    onChange={handleChange}
                    onHelperResult={locationHelperResult}
                />
                <LotHelper
                    id={'isLotOpen'}
                    isLotOpen={isLotOpen}
                    inoutDetailNo={lotHelperinoutDetailNo}
                    selectedBarcodeList={palletList}
                    handleChange={handleChange}
                    onHelperResult={lotHelperResult}
                />
            </React.Fragment>
        )
    }
}

export default Inout;