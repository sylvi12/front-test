import API from "components/override/api/API";

class OutputRepository {

    URL = "/api/inout";
    /* 조회 */
    // 배차 그리드 조회
    getAllocate(param) {
        return API.request.post(encodeURI(`${this.URL}/getallocate`), param)
    }
    // 주문 그리드 조회
    getOrder(param) {
        return API.request.post(encodeURI(`${this.URL}/getorder`), param)
    }
    // 팔레트 그리드 조회
    getPallet(param) {
        return API.request.get(encodeURI(`${this.URL}/getpallet?orderNo=${param.orderNo}`))
    }
    getPallet2(param) {
        return API.request.post(encodeURI(`${this.URL}/getpallet2`), param)
    }

    /* 등록 */
    // 주문/품목 확정 클릭 시
    inoutConfirm(param) {
        return API.request.post(encodeURI(`${this.URL}/confirm`), param)
    }
    // 바코드 내역 추가 시
    saveBarcode(params) {
        return API.request.post(encodeURI(`${this.URL}/savebarcode`), params)
    }

    /* 삭제 */
    deleteBarcode(params) {
        return API.request.post(encodeURI(`${this.URL}/deletebarcode`), params)
    }
}
export default new OutputRepository();