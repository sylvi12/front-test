import { observable, action } from 'mobx';
import moment from 'moment';
import 'moment/locale/ko';

import Alert from 'components/override/alert/Alert';
import GridUtil from 'components/override/grid/utils/GridUtil';
import Validator from 'components/override/utils/Validator';

import InoutRepository from 'modules/pages/inout/inout/service/InoutRepository';
import OutputRepository from 'modules/pages/order/output/service/OutputRepository';
import CommonStore from 'modules/pages/common/service/CommonStore';
import BaseStore from 'utils/base/BaseStore';

class InoutStore extends BaseStore {
    // InoutSearchItem
    @observable alphaCheck = false;
    @observable buId = "";
    @observable startDate = moment().format('YYYYMMDD');
    @observable endDate = moment().format('YYYYMMDD');
    @observable tmddocno = "";

    @observable inoutType = "O";
    @observable ledgerType = "";
    @observable inoutStatus = "";
    @observable inoutStep = "";

    @observable itemNmOrItemCd = "";
    @observable targetNm = "";
    @observable carNmOrCarNo = "";
    @observable inoutNum = "";

    // InoutAllocateCarContents
    allocateCarGridApi = undefined;
    @observable allocateCarList = [];

    // InoutOrderMiddleItem
    @observable confirmButtonAvailableToggle = true;

    // InoutOrderContents
    orderGridApi = undefined;
    @observable orderList = [];
    @observable pinnedBottomRowDataAtOrderGrid = [];
    orderSearchTmddocno = "";
    orderSearchOrderNo = 0;

    // InoutPallteMiddleItem
    @observable locationButtonAvailableToggle = true;
    @observable addButtonAvailableToggle = true;
    @observable deleteButtonAvailableToggle = true;

    // InoutPalletContents
    palletGridApi = undefined;
    @observable palletList = [];
    @observable pinnedBottomRowDataAtPalletGrid = [];
    barcodeSearchOrderNo = 0;

    /* LocationHelpler variables */
    @observable isLocationOpen = false;
    @observable locationBuId = '';

    /* LotHelpler variables */
    @observable isLotOpen = false;
    @observable lotHelperinoutDetailNo = '';
    
    slsStatusList  = []

    @observable inList = []
    @observable outList = []

    @observable inoutList = []

    constructor() {
        super();
        this.setInitialState();
    }

    @action.bound
    handleChange = (data) => {
        console.log(data);
        this[data.id] = data.value;
    }

    // InoutSearchItem 공장
    @action.bound
    getBu = async () => {
        this.buId = CommonStore.buId;
        this.alphaCheck = true;
        this.slsStatusList = await CommonStore.codeObject["MFD011"];
        
        await this.slsStatusList.forEach(element => {
            
            if(element.desc1 != '') {
                var param = {
                    'cd' : element.cd
                   ,'cdNm' : element.desc1
                }
                this.inList.push(param)
            }
            
            if(element.desc2 != '') {
                var param = {
                    'cd' : element.cd
                   ,'cdNm' : element.desc2
                }
                this.outList.push(param)
            }
        });

        this.inoutList = this.outList; //JSON.parse(JSON.stringify(this.outList));
        console.log(this.alphaCheck, this.inoutList, this.slsStatusList, this.outList.length, this.inList.length);
    }

    // InoutSearchItem 구분 선택사항 변경 시
    @action.bound
    inoutTypeSelectionChanged = async (selectedItem) => {
        this.generateGrid();
         if(selectedItem == 'I') {
            this.inoutList = this.inList;
        } 
        else {
            this.inoutList = this.outList;
        }
        await this.handleSearchClick();
    }

    // InoutSearchItem 조회 클릭 시
    @action.bound
    handleSearchClick = async () => {

        this.allocateCarGridApi.setRowData([]);
        const searchData = {
            "buId": this.buId
            , "startDate": this.startDate
            , "endDate": this.endDate
            , "tmddocno": this.tmddocno

            , "inoutType": this.inoutType
            , "ledgerType": this.ledgerType
            , "inoutStatus": this.inoutStatus
            , "inoutStep": this.inoutStep

            , "itemNmOrItemCd": this.itemNmOrItemCd
            , "targetNm": this.targetNm
            , "carNmOrCarNo": this.carNmOrCarNo
            , "inoutNum": this.inoutNum
        }
        const param = JSON.stringify({ param: searchData });
        const { data, status } = await InoutRepository.getAllocate(param);
        if (this.inoutType == "I") {
            this.locationButtonAvailableToggle = true;
        } else if (this.inoutType == "O") {
            this.locationButtonAvailableToggle = false;
        }
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            this.allocateCarList = data.data;
            this.allocateCarGridApi.setRowData(this.allocateCarList);
            if (data.data[0] != undefined) {
                this.orderSearchOrderNo = data.data[0].orderNo;
                GridUtil.setFocusByRowIndex(this.allocateCarGridApi, 0);
                if (data.data[0].inoutStatus != "10") {
                    this.locationButtonAvailableToggle = false;
                    this.confirmButtonAvailableToggle = false;
                    this.addButtonAvailableToggle = false;
                    this.deleteButtonAvailableToggle = false;
                }
            } else {
                this.orderGridApi.setRowData([]);
                this.palletGridApi.setRowData([]);
                this.confirmButtonAvailableToggle = true;
                this.addButtonAvailableToggle = true;
                this.deleteButtonAvailableToggle = true;
            }
        }
    }
    // InoutAllocateCarContents grid
    @action.bound
    setAllocateCarGridApi = async (gridApi) => {
        this.allocateCarGridApi = gridApi;
        this.generateGrid();
    }
    // 그리드 만들기
    @action.bound
    generateGrid = async () => {
        let hide = false;
        if (this.inoutType == 'O') {
            hide = false;
        } else {
            hide = true;
        }
        const columnDefs = [
            {
                headerName: "문서번호"
                , field: "tmddocno"
                , width: 80
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "지시번호"
                , field: "orderNo"
                , hide: true
            },
            {
                headerName: "차량명"
                , field: "carNm"
                , width: 110
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "차수"
                , field: "inoutNum"
                , width: 50
                , cellClass: 'cell_align_center'
                , hide: hide
            },
            {
                headerName: "SLS 상태"
                , field: "inoutStatusNm"
                , width: 95
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "WMS 상태"
                , field: "inoutStepNm"
                , width: 65
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "I/F 상태"
                , field: "ifYN"
                , width: 65
                , cellClass: 'cell_align_center'
            }
        ];
        this.allocateCarGridApi.setColumnDefs(columnDefs);
    }
    // InoutAllocateCarContents grid row 클릭 시
    @action.bound
    getTheOthers = async () => {
        const selectedRow = this.allocateCarGridApi.getSelectedRows()[0];
        if (!selectedRow || selectedRow.length == 0) {
            return;
        }
        this.orderSearchTmddocno = selectedRow.tmddocno;
        this.orderSearchOrderNo = selectedRow.orderNo;
        this.searchOrder();
        if (this.inoutType == "I") {
            this.locationButtonAvailableToggle = true;
        } else if (this.inoutType == "O") {
            this.locationButtonAvailableToggle = false;
        }
        if (selectedRow.inoutStatus != "10") {
            this.locationButtonAvailableToggle = false;
            this.confirmButtonAvailableToggle = false;
            this.addButtonAvailableToggle = false;
            this.deleteButtonAvailableToggle = false;
        } else {
            this.confirmButtonAvailableToggle = true;
            this.addButtonAvailableToggle = true;
            this.deleteButtonAvailableToggle = true;
        }
    }

    // InoutOrderMiddleItem 확정 클릭 시
    @action.bound
    orderConfirm = async () => {

        const selectedRow = this.allocateCarGridApi.getSelectedRows()[0];

        if(selectedRow == undefined) {
            Alert.meg("배차를 선택해 주세요.");
            return
        }

        const saveData = {
            "orderNo": this.barcodeSearchOrderNo
            , "checkPass": 'N'
        }

        const { data, status } = await InoutRepository.inoutConfirm(saveData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            Alert.meg("확정되었습니다.");
            this.handleSearchClick();
            this.orderGridApi.setRowData([]);
            this.palletGridApi.setRowData([]);
        }
    }

    // InoutOrderContents grid
    @action.bound
    setOrderGridApi = async (gridApi) => {
        this.orderGridApi = gridApi

        this.pinnedBottomRowDataAtOrderGrid = [
            {
                orderLine: '합계',
                inoutQty: 0,
                qtyWei: 0
            }
        ];
        const columnDefs = [
            {
                headerName: "주문번호"
                , children: [
                    {
                        headerName: "주문번호"
                        , field: "orderLine"
                        , width: 85
                        , cellClass: 'cell_align_center'
                    }
                ]
            },
            {
                headerName: "라인번호"
                , children: [
                    {
                        headerName: "라인번호"
                        , field: "orderLineNo"
                        , width: 60
                        , cellClass: 'cell_align_center'
                    }
                ]
            },
            {
                headerName: "수불타입"
                , children: [
                    {
                        headerName: "수불타입"
                        , field: "ledgerTypeNm"
                        , width: 100
                        , cellClass: 'cell_align_center'
                    }
                ]
            },
            {
                headerName: "제품"
                , children: [
                    {
                        headerName: "제품코드"
                        , field: "itemCd"
                        , width: 90
                        , cellClass: 'cell_align_center'
                    },
                    {
                        headerName: "제품명"
                        , field: "itemNm"
                        , width: 160
                    }
                ]
            },
            {
                headerName: "입출고량",
                children: [
                    {
                        headerName: "주문량"
                        , field: "inoutQty"
                        , width: 60
                        , type: "numericColumn"
                        , valueFormatter: (params) => GridUtil.numberFormatter(params, 2) //GridUtil.numberFormatter
                        , valueParser: GridUtil.numberParser
                        , pinnedRowCellRenderer: "customPinnedRowRenderer"
                        , pinnedRowCellRendererParams: { style: { "fontWeight": "bold" }, decimalScale: 2 }
                    },
                    {
                        headerName: "주문단위"
                        , field: "qtyUom"
                        , width: 40
                        , cellClass: 'cell_align_center'
                    }
                ]
            },
            {
                headerName: "주문중량",
                children: [
                    {
                        headerName: "주문중량"
                        , field: "qtyWei"
                        , width: 60
                        , type: "numericColumn"
                        , valueFormatter: (params) => GridUtil.numberFormatter(params, 3) //GridUtil.numberFormatter
                        , valueParser: GridUtil.numberParser
                        , pinnedRowCellRenderer: "customPinnedRowRenderer"
                        , pinnedRowCellRendererParams: { style: { "fontWeight": "bold" }, decimalScale: 3 }
                    },
                    {
                        headerName: "주문단위"
                        , field: "wgTunt"
                        , width: 40
                        , cellClass: 'cell_align_center'
                    }
                ]
            },
            {
                headerName: "배송처",
                children: [
                    {
                        headerName: "번호"
                        , field: "targetSCd"
                        , width: 60
                        , cellClass: 'cell_align_center'
                    },
                    {
                        headerName: "명"
                        , field: "targetSNm"
                        , width: 140
                    }
                ]
            },
            {
                headerName: "거래처",
                children: [
                    {
                        headerName: "번호"
                        , field: "targetCd"
                        , width: 60
                        , cellClass: 'cell_align_center'
                    },
                    {
                        headerName: "명"
                        , field: "targetNm"
                        , width: 140
                    }
                ]
            },
            {
                headerName: "비고",
                children: [
                    {
                        headerName: "비고"
                        , field: "remark"
                        , width: 150
                    }
                ]

            }

        ];
        this.orderGridApi.setColumnDefs(columnDefs);
    }

    // InoutOrderContents 조회
    @action.bound
    searchOrder = async () => {
        const searchData = {
            "tmddocno": this.orderSearchTmddocno
            , "orderNo": this.orderSearchOrderNo
            , "ledgerType": this.ledgerType
            , "targetNm": this.targetNm
            , "itemNmOrItemCd": this.itemNmOrItemCd
        }
        const { data, status } = await InoutRepository.getOrder(searchData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            this.orderList = data.data;
            this.orderGridApi.setRowData(this.orderList);
            if (this.orderGridApi.getRowNode(0) != undefined) {
                // this.orderGridApi.getRowNode(0).setSelected(true);
                GridUtil.setFocusByRowIndex(this.orderGridApi, 0);
            }
        }
    }

    // InoutOrderContents 그리드 로우 클릭 시
    @action.bound
    getBarcode = async () => {
        const selectedRow = this.orderGridApi.getSelectedRows()[0];
        console.log(selectedRow);
        this.barcodeSearchOrderNo = selectedRow.orderNo;
        this.barcodeSearchItemId = selectedRow.itemId;
        this.barcodeSearchtmddocno = selectedRow.tmddocno;
        this.barcodeSearchtmdlvi = selectedRow.tmdlvi;
        this.barcodeInoutDetailNo = selectedRow.inoutDetailNo;
        this.barcodeOrderDetailNo = selectedRow.orderDetailNo;
        this.barcodeOrderQty = selectedRow.inoutQty;
        this.barcodeDetailLedgerType = selectedRow.detailLedgerType;
        if (selectedRow.barcodeYn != 'Y' && selectedRow.barcodeYn != null && selectedRow.barcodeYn != undefined) {
            this.barcodeGridEditable = true;
        } else {
            this.barcodeGridEditable = false;
        }
        this.searchPallet();
    }

    // InoutPalletMiddleItem 위치변경 클릭 시
    @action.bound
    locationMove = async () => {
        const selectedRow = this.palletGridApi.getSelectedRows()[0];
        if (!selectedRow || selectedRow.length == 0) {
            Alert.meg("생산정보를 선택해 주세요.");
            return;
        }
        this.locationBuId = this.buId;
        this.locationBarcode = selectedRow.barcode;
        this.isLocationOpen = true;
    }
    // LocationModal MiddleItem 선택 클릭 시
    @action.bound
    locationHelperResult = async (result) => {
        if (result != null) {
            const saveData = {
                "barcode": this.locationBarcode
                , "locId": result.locId
                , "orderNo": this.barcodeSearchOrderNo
            }
            const { data, status } = await OutputRepository.locationMoveInsert(saveData);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                this.searchPallet();
            }
        }
    }

    // InoutPalletMiddleItem 추가 클릭 시
    @action.bound
    addPallet = async () => {
        const selectedRow = this.orderGridApi.getSelectedRows()[0];
        if (!selectedRow || selectedRow.length == 0) {
            Alert.meg("바코드를 추가할 주문/품목을 선택해 주세요");
            return;
        }
        console.log(selectedRow);
        this.barcodeSearchOrderNo = selectedRow.orderNo;
        this.barcodeInoutDetailNo = selectedRow.inoutDetailNo;
        this.barcodeSearchItemId = selectedRow.itemId;
        this.barcodeLedgerType = selectedRow.ledgerType;
        this.lotHelperinoutDetailNo = selectedRow.inoutDetailNo;
        if (selectedRow.barcodeYn != 'Y' && selectedRow.barcodeYn != null && selectedRow.barcodeYn != undefined) {
            this.barcodeGridEditable = true;
        } else {
            this.barcodeGridEditable = false;
        }
        this.isLotOpen = true;
    }

    // LotModal MiddleItem 저장 클릭 시
    @action.bound
    lotHelperResult = async (result) => {

        await this.searchPallet();
    }
    // InoutPalletMiddleItem 저장 클릭 시
    @action.bound
    savePallet = async () => {
        const selectedRows = GridUtil.getGridSaveArray(this.palletGridApi);

        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg("저장할 항목이 없습니다.");
            return;
        }
        // console.log(this.pinnedBottomRowDataAtPalletGrid);
        // console.log(this.barcodeOrderQty);
        if (this.pinnedBottomRowDataAtPalletGrid[0].processQty > this.barcodeOrderQty) {
            Alert.meg("상차수량은 입출고량보다 클 수 없습니다.");
            return;
        }
        for (let i = 0; i < selectedRows.length; i++) {
            if (this.barcodeGridEditable && selectedRows[i].prodDt == '') {
                Alert.meg("수량관리대상은 생산일자가 입력되어야 합니다.");
                return;
            }
            if (selectedRows[i].processQty > selectedRows[i].availableQty) {
                Alert.meg(`재고량보다 클 수 없습니다.<br/>${selectedRows[i].barcode} 가용재고: ${selectedRows[i].availableQty} ${selectedRows[i].processUom}`);
                return;
            }
            selectedRows[i].ledgerType = this.barcodeLedgerType;
        }
        const searchData = {
            "orderNo": this.barcodeSearchOrderNo
            , "inoutDetailNo": this.barcodeInoutDetailNo
            , "itemId": this.barcodeSearchItemId
        }
        const params = JSON.stringify({ param1: selectedRows, param2: searchData });
        const { data, status } = await InoutRepository.saveBarcode(params);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            Alert.meg("저장되었습니다.");
            this.palletList = data.data;
            this.palletGridApi.setRowData(this.palletList);
        }
    }

    // InoutPalletMiddleItem 삭제 클릭 시
    @action.bound
    deletePallet = async () => {
        const selectedRows = this.palletGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg("삭제할 바코드를 선택해 주세요.");
            return;
        }

        const searchData = {
            "inoutDetailNo": this.barcodeInoutDetailNo
            , "orderNo": this.barcodeSearchOrderNo
            , "itemId": this.barcodeSearchItemId
        }
        const params = JSON.stringify({ param1: selectedRows, param2: searchData });
        const value = await Alert.confirm("저장되지 않은 바코드는 리스트에서 제거됩니다.<br>삭제하시겠습니까?").then(result => { return result; });
        if (value) {
            let count = 0;
            for (let i = 0; i < selectedRows.length; i++) {
                if (selectedRows[i].isNew == 'Y') {
                    selectedRows[i].willRemove = 'Y';
                    this.palletList = this.palletList.filter(x => x.willRemove != 'Y');
                    count++;
                } else {
                    break;
                }
            }
            if (count == selectedRows.length) {
                this.palletGridApi.setRowData(this.palletList);
                return;
            }

            console.log(params);

            const { data, status } = await InoutRepository.deleteBarcode(params);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                this.palletList = data.data;
                this.palletGridApi.setRowData(this.palletList);
            }
        }
        else {
            return;
        }
    }

    // InoutPalletContents grid
    @action.bound
    setPalletGridApi = async (gridApi) => {
        this.palletGridApi = gridApi;
        this.generatePalletGrid();
    }
    @action.bound
    generatePalletGrid = async () => {

        const isEditableProcessQty = this.barcodeDetailLedgerType != "ST" ? true : false;

        this.pinnedBottomRowDataAtPalletGrid = [
            {
                seq: '합계',
                processQty: 0
            }
        ];
        const columnDefs = [
            {
                headerName: ""
                , children: [
                    {
                        headerName: ""
                        , field: "check"
                        , width: 29
                        , checkboxSelection: true
                        , headerCheckboxSelection: true
                        , cellClass: 'cell_align_center'
                        , editable: false
                    }
                ]
            },
            {
                headerName: "순번"
                , children: [
                    {
                        headerName: "순번"
                        , field: "seq"
                        , width: 40
                        , editable: false
                        , cellClass: 'cell_align_center'
                    }
                ]
            },
            {
                headerName: "바코드번호"
                , children: [
                    {
                        headerName: "바코드번호"
                        , field: "barcode"
                        , width: 100
                        , cellClass: 'cell_align_center'
                        , editable: false
                    }
                ]
            },
            {
                headerName: "상태"
                , children: [
                    {
                        headerName: "상태"
                        , field: "lotQualityNm"
                        , width: 50
                        , cellClass: 'cell_align_center'
                        , editable: false
                    }
                ]
            },
            {
                headerName: "제품"
                , children: [
                    {
                        headerName: "제품코드"
                        , field: "itemCd"
                        , width: 90
                        , editable: false
                        , cellClass: 'cell_align_center'
                    },
                    {
                        headerName: "제품명"
                        , field: "itemNm"
                        , width: 160
                        , editable: false
                    }
                ]
            },
            {
                headerName: "LOT NO"
                , children: [
                    {
                        headerName: "LOT NO"
                        , field: "lotNo"
                        , width: 80
                        , cellClass: 'cell_align_center'
                        , editable: false
                    }
                ]

            },
            {
                headerName: "생산일자"
                , children: [
                    {
                        headerName: "생산일자"
                        , field: "prodDt"
                        , width: 90
                        , cellClass: 'cell_align_center'
                        , valueFormatter: GridUtil.dateFormatter
                        , editable: this.barcodeGridEditable
                    }
                ]
            },
            {
                headerName: "유통기한"
                , children: [
                    {
                        headerName: "유통기한"
                        , field: "expiryDt"
                        , width: 90
                        , cellClass: 'cell_align_center'
                        , valueFormatter: GridUtil.dateFormatter
                        , editable: false
                    }
                ]
            },
            {
                headerName: "상차수량",
                children: [
                    {
                        headerName: "주문중량"
                        , field: "processQty"
                        , width: 50
                        , type: "numericColumn"
                        , editable: isEditableProcessQty
                        , valueFormatter: (params) => GridUtil.numberFormatter(params, 2)
                        , valueParser: GridUtil.numberParser
                        , pinnedRowCellRenderer: "customPinnedRowRenderer"
                        , pinnedRowCellRendererParams: { style: { "fontWeight": "bold" }, decimalScale: 2 }
                    },
                    {
                        headerName: "재고량"
                        , field: "avilableQty"
                        , width: 50
                        , type: "numericColumn"
                        , valueFormatter: GridUtil.numberFormatter
                        , valueParser: GridUtil.numberParser
                        , pinnedRowCellRenderer: "customPinnedRowRenderer"
                        , pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
                        , hide: true
                    },
                    {
                        headerName: "단위"
                        , field: "processUom"
                        , width: 50
                        , cellClass: 'cell_align_center'
                        , editable: false
                    }
                ]
            },
            {
                headerName: "처리시간"
                , children: [
                    {
                        headerName: "처리시간"
                        , field: "inputDtm"
                        , width: 110
                        , cellClass: 'cell_align_center'
                        , valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm')
                        , editable: false
                    }
                ]
            },
            {
                headerName: "작업자"
                , children: [
                    {
                        headerName: "처리자"
                        , field: "inputEmpNm"
                        , width: 90
                        , cellClass: 'cell_align_center'
                        , editable: false
                    }
                ]
            },
            {
                headerName: "위치"
                , children: [
                    {
                        headerName: "위치"
                        , field: "locNm"
                        , width: 100
                        , cellClass: 'cell_align_center'
                        , editable: false
                    }
                ]
            }
        ];
        this.palletGridApi.setColumnDefs(columnDefs);
    }

    // InoutPalletContents 조회
    @action.bound
    searchPallet = async () => {
        const searchData = {
            "inoutDetailNo": this.barcodeInoutDetailNo
        }
        const { data, status } = await InoutRepository.getPallet2(searchData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            this.palletList = data.data;
            this.generatePalletGrid();
            this.palletGridApi.setRowData(this.palletList);
        }
    }
}



export default new InoutStore();