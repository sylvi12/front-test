import React, { Component } from "react";
import { inject } from "mobx-react";

import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import EtcEtcMiddleItem from "modules/pages/inout/etc/etcContentsAssemble/EtcEtcMiddleItem";
import EtcEtcContents from "modules/pages/inout/etc/etcContentsAssemble/EtcEtcContents";
import EtcBarcodeMiddleItem from "modules/pages/inout/etc/EtcBarcodeMiddleItem";
import EtcBarcodeContents from "modules/pages/inout/etc/etcContentsAssemble/EtcBarcodeContents";


@inject(stores => ({
}))

class EtcContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { } = this.props

        return (
            <React.Fragment>
                <div style={{ width: '100%', height: '50%' }}>
                    <ContentsMiddleTemplate contentSubTitle={'기타출고'} middleItem={<EtcEtcMiddleItem />} />
                    <ContentsTemplate id='etcEtc' contentItem={<EtcEtcContents />} />
                </div>
                <div style={{ width: '100%', height: '50%' }}>
                    <ContentsMiddleTemplate contentSubTitle={'바코드 내역'} middleItem={<EtcBarcodeMiddleItem />} />
                    <ContentsTemplate id='etcBarcode' contentItem={<EtcBarcodeContents />} />
                </div>
            </React.Fragment>
        )
    }
}

export default EtcContents;