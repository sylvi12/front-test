import React, { Component } from 'react';
import { inject } from 'mobx-react';

import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import EmployeeHelper from 'components/codehelper/EmployeeHelper';
import STextArea from 'components/atoms/input/STextArea';
import SButton from 'components/atoms/button/SButton';



@inject(stores => ({
      ledgerType_U: stores.etcStore.ledgerType_U
    , ledgerTypeList_U: stores.etcStore.ledgerTypeList_U
    , inoutEtcDt_U: stores.etcStore.inoutEtcDt_U
    , remark_U: stores.etcStore.remark_U
    , handleChange: stores.etcStore.handleChange
    //, etcInModalClose: stores.etcStore.etcInModalClose
    , etcSave_U: stores.etcStore.etcSave_U
    
}))

class EtcUpdateModal extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        const {   ledgerType_U
                , ledgerTypeList_U
                , inoutEtcDt_U
                , remark_U
                , handleChange
                , etcSave_U
                , etcInModalClose } = this.props;
        return(
            <React.Fragment>
                
                
                    <div className='btn_wrap'>
                        <SButton buttonName={"저장"} onClick={etcSave_U} type={"etcInSave"} />
                        {/* <SButton buttonName={"닫기"} onClick={etcInModalClose} type={"delete"} /> */}
                    </div>
                    
                    <table style={{ width: "100%" }}>
                        <tbody>
                            <tr>
                                <th>출고유형</th>
                                <td>
                                    <SSelectBox
                                        id="ledgerType_U"
                                        value={ledgerType_U}
                                        // codeGroup={"MFD002"}
                                        optionGroup={ledgerTypeList_U}
                                        onChange={handleChange}
                                    />
                                </td>

                                <th>출고일자</th>
                                <td>
                                    <SDatePicker
                                        id="inoutEtcDt_U"
                                        value={inoutEtcDt_U}
                                        onChange={handleChange}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <th>비고</th>
                                <td colSpan="3">
                                    <STextArea
                                        id="remark_U"
                                        value={remark_U}
                                        onChange={handleChange}
                                        contentWidth={390}
                                    />
                                </td>
                            </tr>

                        </tbody>
                    </table>
                
                
                
            </React.Fragment>
        );
    }
}

export default EtcUpdateModal;