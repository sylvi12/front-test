import React, { Component } from "react";
import { inject } from "mobx-react";

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setEtcBarcodeUpsertModalGridApi: stores.etcStore.setEtcBarcodeUpsertModalGridApi
    , etcBarcodeUpsertModalList: stores.etcStore.etcBarcodeUpsertModalList
    , etcBarcodeUpsertModalSave: stores.etcStore.etcBarcodeUpsertModalSave
}))

class EtcBarcodeUpsertModalContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setEtcBarcodeUpsertModalGridApi, etcBarcodeUpsertModalList, etcBarcodeUpsertModalSave } = this.props;

        return (
            <SGrid
                grid={'etcBarcodeUpsertModalGrid'}
                gridApiCallBack={setEtcBarcodeUpsertModalGridApi}
                rowData={etcBarcodeUpsertModalList}
                rowDoubleClick={etcBarcodeUpsertModalSave}
                editable={false}
                suppressRowClickSelection={true}
            />
        );
    }
}

export default EtcBarcodeUpsertModalContents;