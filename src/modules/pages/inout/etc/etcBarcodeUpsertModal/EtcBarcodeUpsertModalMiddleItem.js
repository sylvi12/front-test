import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    closeEtcBarcodeUpsertModal: stores.etcStore.closeEtcBarcodeUpsertModal
    , etcBarcodeUpsertModalSave: stores.etcStore.etcBarcodeUpsertModalSave
}))

class EtcBarcodeUpsertModalMiddleItem extends Component {

    render() {

        const { closeEtcBarcodeUpsertModal, etcBarcodeUpsertModalSave } = this.props;
        return (
            <React.Fragment>
                <SButton buttonName={"저장"} onClick={etcBarcodeUpsertModalSave} type={"btnSave"} />
                {/* <SButton buttonName={"닫기"} onClick={closeEtcBarcodeUpsertModal} type={"default"} /> */}
            </React.Fragment>
        )
    }
}

export default EtcBarcodeUpsertModalMiddleItem;