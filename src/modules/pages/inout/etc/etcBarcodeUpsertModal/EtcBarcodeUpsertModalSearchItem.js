import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SDatePicker from 'components/override/datepicker/SDatePicker';
import SInput from 'components/atoms/input/SInput'
import SButton from 'components/atoms/button/SButton'

@inject(stores => ({
    etcBarcodeUpsertModalBarcode: stores.etcStore.etcBarcodeUpsertModalBarcode
    , handleChange: stores.etcStore.handleChange
    , etcBarcodeUpsertModalProdDt: stores.etcStore.etcBarcodeUpsertModalProdDt
    , etcBarcodeUpsertModalItemNmOrItemCd: stores.etcStore.etcBarcodeUpsertModalItemNmOrItemCd
    , etcBarcodeUpsertModalSearch: stores.etcStore.etcBarcodeUpsertModalSearch
}))
class EtcBarcodeUpsertModalSearchItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { etcBarcodeUpsertModalBarcode, handleChange
            , etcBarcodeUpsertModalProdDt
            , etcBarcodeUpsertModalItemNmOrItemCd
            , etcBarcodeUpsertModalSearch
        } = this.props;

        return (
            <React.Fragment>

                <SInput
                    title={"바코드"}
                    id={"etcBarcodeUpsertModalBarcode"}
                    value={etcBarcodeUpsertModalBarcode}
                    onChange={handleChange}
                    onEnterKeyDown={etcBarcodeUpsertModalSearch}
                />

                <SDatePicker
                    title={"생산일자"}
                    id="etcBarcodeUpsertModalProdDt"
                    value={etcBarcodeUpsertModalProdDt}
                    onChange={handleChange}
                />

                <SInput
                    title={"제품명/코드"}
                    id={"etcBarcodeUpsertModalItemNmOrItemCd"}
                    value={etcBarcodeUpsertModalItemNmOrItemCd}
                    onChange={handleChange}
                    onEnterKeyDown={etcBarcodeUpsertModalSearch}
                />

                <div className='search_item_btn' style={{ right: "" }}>
                    <SButton buttonName={'조회'} onClick={etcBarcodeUpsertModalSearch} type={'btnSearch'} />
                </div>

            </React.Fragment>
        )
    }
}

export default EtcBarcodeUpsertModalSearchItem;