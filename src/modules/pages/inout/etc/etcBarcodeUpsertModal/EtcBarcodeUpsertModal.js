import React, { Component } from 'react';

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import EtcBarcodeUpsertModalSearchItem from "modules/pages/inout/etc/etcBarcodeUpsertModal/EtcBarcodeUpsertModalSearchItem";
import EtcBarcodeUpsertModalMiddleItem from "modules/pages/inout/etc/etcBarcodeUpsertModal/EtcBarcodeUpsertModalMiddleItem";
import EtcBarcodeUpsertModalContents from "modules/pages/inout/etc/etcBarcodeUpsertModal/EtcBarcodeUpsertModalContents";

class EtcBarcodeUpsertModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <React.Fragment>
                <SearchTemplate searchItem={<EtcBarcodeUpsertModalSearchItem />} />
                <ContentsMiddleTemplate contentSubTitle={'LOT 정보'} middleItem={<EtcBarcodeUpsertModalMiddleItem />} />
                <ContentsTemplate id='etcBarcodeUpsertModal' contentItem={<EtcBarcodeUpsertModalContents />} />
            </React.Fragment>
        );
    }
}

export default EtcBarcodeUpsertModal;