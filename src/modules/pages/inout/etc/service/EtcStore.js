import { observable, action } from 'mobx';
import moment, { relativeTimeRounding } from 'moment';
import 'moment/locale/ko';

import Alert from 'components/override/alert/Alert';
import GridUtil from 'components/override/grid/utils/GridUtil';
import Validator from 'components/override/utils/Validator';
import ItemOptionModel from 'components/codehelper/model/ItemOptionModel';

import EtcRepository from 'modules/pages/inout/etc/service/EtcRepository';
import OutputRepository from 'modules/pages/order/output/service/OutputRepository';
import InoutRepository from 'modules/pages/inout/inout/service/InoutRepository';
import CommonStore from 'modules/pages/common/service/CommonStore';

import QualityStatusStore from 'modules/pages/inventory/qualityStatus/service/QualityStatusStore';
import BaseStore from 'utils/base/BaseStore';

class EtcStore extends BaseStore {

    // EtcSearchItem 변수
    @observable alphaCheck = false;
    @observable buId = "";
    @observable buGroup = [];
    @observable startDt = moment().add(-7, 'days').format('YYYYMMDD');
    @observable endDt = moment().format('YYYYMMDD');
    @observable itemNmOrItemCd = '';
    @observable changeItem = '';

    // 원료 재투입 리스트
    @observable rawList = [];

    // EtcEtcContents 변수
    etcEtcGridApi = undefined;
    @observable etcEtcList = [];

    // EtcBarcodeContents 변수
    etcBarcodeGridApi = undefined;
    @observable etcBarcodeList = [];
    @observable pinnedBottomRowDataAtEtcBarcodeGrid = [];
    barcodeOrderNo = 0;

    // EtcUpsertModal 변수
    @observable etcBarcodeUpsertModalIsOpen = false;
    etcUpsertModalOrderNo = 0;
    @observable etcUpsertModalInoutEtcDt = "";
    @observable etcUpsertModalLedgerType = "";
    @observable ledgerTypeGroup = [];
    @observable etcUpsertModalRemark = "";

    /* EtcBarcodeUpsertModal 변수 */
    @observable etcUpsertModalIsOpen = false;
    // EtcBarcodeUpsertModal SearchItem 변수
    etcBarcodeUpsertModalOrderNo = 0;
    ledgerType = "";
    @observable etcBarcodeUpsertModalBarcode = "";
    @observable etcBarcodeUpsertModalProdDt = "";
    @observable etcBarcodeUpsertModalItemNmOrItemCd = "";
    // EtcBarcodeUpsertModal Contents 변수
    etcBarcodeUpsertModalGridApi = undefined;
    @observable etcBarcodeUpsertModalList = [];

    /* BarcodeHelpler variables */
    @observable isBarcodeHelperOpen = false;
    @observable barcodeHelplerOrderNo = 0;
    @observable barcodeHelplerledgerType = '';

    //수정 모달 변수
    @observable udateModalOpen = false;
    @observable ledgerType_U = '';
    @observable inoutEtcDt_U = '';
    @observable remark_U = '';
    @observable ledgerTypeList_U = [];

    @observable itemHelperOption = new ItemOptionModel().optionModel; //ItemHelper Option
    @observable selectedItem = {};

    //바코드내역 추가 키 변수
    key = '';

    //등록에서 바코드 추가 변수
    add = [];
    @observable itemId = ''
    @observable itemCd = ''
    @observable itemNm = ''

    constructor() {
        super();
        this.etcQtyItemHelperOption.barcodeYn = 'N';
        this.etcQtyItemHelperOption.cdWidth = 70;
        this.etcQtyItemHelperOption.cdNmWidth = 120;
        this.etcQtyItemHelperOption.cdNmIsFocus = true;
        this.etcQtyItemHelperOption_u.cdWidth = 70;
        this.etcQtyItemHelperOption_u.cdNmWidth = 120;
        this.etcQtyItemHelperOption_u.cdNmReadOnly = true;
        this.etcQtyItemHelperOption_u.searchVisible = false;
        this.setInitialState();
    }

    etnQtyInsertValidator = new Validator([
        {
            field: 'itemCd_q',
            method: 'isEmpty',
            message: '제품',
        }
    ])

    @action.bound
    handleChange = (data) => {

        if (data.id == 'selectedEtcQtyItem') {
            if (data.value.itemNm == '') {
                this.itemCd_q = '';
            }
        }
        this[data.id] = data.value;

        console.log(data, 'DATA')
    }

    // EtcSearchItem 공장
    @action.bound
    getBu = async () => {
        this.buId = CommonStore.buId;
        this.alphaCheck = true;

    }
    // EtcSearchItem 조회 클릭 시
    @action.bound
    handleSearchClick = async () => {
        const searchData = {
            "buId": this.buId
            , "startDt": this.startDt
            , "endDt": this.endDt
            , "itemNmOrItemCd": this.itemNmOrItemCd
        }
        const { data, status } = await EtcRepository.getEtcs(searchData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            this.etcEtcList = data.data;
            this.etcEtcGridApi.setRowData(this.etcEtcList);
            //조회시 바코드 내역은 초기화
            this.etcBarcodeGridApi.setRowData([]);
        }
    }
    // EtcEtcMiddleItem 등록 클릭 시
    @action.bound
    etcMiddleRegister = async () => {

        let allBu = this.buId.split(',');

        if (allBu.length > 1) {
            Alert.meg("선택된 공장이 없습니다.");
            return;
        }

        const all = CommonStore.codeObject["MFD002"]
        this.rawList = CommonStore.codeObject["MFD023"];
        await this.rawList.forEach(element => {
            if(element.desc1 == this.buId) {
                this.changeItem  = element.cd
                return
            }
        });
        this.ledgerTypeGroup = all.filter(x => x.desc2 == "Y");
        this.etcUpsertModalOrderNo = 0;
        this.etcUpsertModalInoutEtcDt = moment().format('YYYYMMDD');
        this.etcUpsertModalLedgerType = "LZ";
        this.etcUpsertModalRemark = "";
        this.etcUpsertModalIsOpen = true;

        //바코드내역 - 추가버튼 다르게 사용하기 위해 넣음
        this.key = 'Y';
    }
    // EtcEtcMiddleItem 삭제 클릭 시
    @action.bound
    etcMiddleDelete = async () => {

        const selectedRows = this.etcEtcGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg("선택된 항목이 없습니다.");
            return;
        }
        for (let i = 0; i < selectedRows.length; i++) {
            if (selectedRows[i].inoutEtcStatus == '20') {
                Alert.meg("이미 확정된 기타출고 입니다.");
                return;
            }
        }
        const searchData = {
            "buId": this.buId
            , "startDt": this.startDt
            , "endDt": this.endDt
            , "itemNmOrItemCd": this.itemNmOrItemCd
        }
        const value = await Alert.confirm("삭제하시겠습니까?").then(result => { return result; });
        const params = JSON.stringify({ param1: selectedRows, param2: searchData });
        if (value) {
            const { data, status } = await EtcRepository.delete(params);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                this.etcEtcList = data.data;
                this.etcEtcGridApi.setRowData(this.etcEtcList);
                this.etcBarcodeGridApi.setRowData([]);
                Alert.meg("완료되었습니다.");
            }
        }
        else {
            return;
        }
    }
    // EtcEtcMiddleItem 확정 클릭 시
    @action.bound
    etcMiddleConfirm = async () => {
        const selectedRow = this.etcEtcGridApi.getSelectedRows()[0];
        console.log(selectedRow, 'selectedRow')
        if (!selectedRow || selectedRow.length == 0) {
            Alert.meg("선택된 항목이 없습니다.");
            return;
        }
        if (selectedRow.inoutEtcStatus == '20') {
            Alert.meg("이미 확정된 기타출고 입니다.");
            return;
        }
        if (this.etcBarcodeGridApi.getDisplayedRowCount() <= 0) {
            Alert.meg("바코드가 없습니다.");
            return;
        }
        const confirmData = {
            "orderNo": selectedRow.orderNo
        }
        const { data, status } = await EtcRepository.confirm(confirmData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            Alert.meg("확정되었습니다.");
            this.handleSearchClick();
            this.getBarcode();
        }
    }
    // EtcEtcMiddleItem 확정취소 클릭 시
    @action.bound
    etcMiddleCancel = async () => {
        const selectedRow = this.etcEtcGridApi.getSelectedRows()[0];
        if (!selectedRow || selectedRow.length == 0) {
            Alert.meg("선택된 항목이 없습니다.");
            return;
        }
        if (selectedRow.inoutEtcStatus != '20') {
            Alert.meg("확정된 기타출고가  아닙니다.");
            return;
        }
        const deleteData = {
            "orderNo": selectedRow.orderNo
        }
        const value = await Alert.confirm("취소하시겠습니까?").then(result => { return result; });
        if (value) {
            const { data, status } = await EtcRepository.cancel(deleteData);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                this.handleSearchClick();
                this.getBarcode();
            }
        }
        else {
            return;
        }
    }

    //기타출고 ROW 더블 클릭
    @action.bound
    openModalEdit = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.etcEtcGridApi);

        if (selectedRow.itemId != 0 && selectedRow.inoutEtcStatus != '20') {
            const submitData = {
                "orderNo": selectedRow.orderNo
            };

            const { data, status } = await EtcRepository.getEtcChangeData(submitData);
            if (data.success && status == 200) {
                const successData = data.data;
                const all = CommonStore.codeObject["MFD002"];
                this.rawList = CommonStore.codeObject["MFD023"];
                await this.rawList.forEach(element => {
                    if(element.desc1 == this.buId) {
                        this.changeItem  = element.cd
                        return
                    }
                });
                this.ledgerTypeGroup = all.filter(x => x.desc2 == "Y");
                this.ledgerType_u = successData.ledgerType;
                this.etcQtyOrderDt_u = moment(successData.inoutEtcDt).format('YYYYMMDD');
                this.etcQtyUpsertModalOrderNo_u = successData.orderNo;
                this.etcQtyBarcode_u = successData.barcode;
                this.selectedEtcQtyItem_u = { "itemCd": successData.itemCd, "itemNm": successData.itemNm };
                this.etcQtyProcessQty_u = successData.processQty;
                this.etcQtyProcessUom_u = successData.processUom;
                this.remark_u = successData.remark;
                this.etcQtyUpdateModalIsOpen = true;
            }
        } else {
            const all = CommonStore.codeObject["MFD002"];
            this.rawList = CommonStore.codeObject["MFD023"];
            await this.rawList.forEach(element => {
                if(element.desc1 == this.buId) {
                    this.changeItem  = element.cd
                    return
                }
            });
            this.ledgerTypeList_U = all.filter(x => x.cd.indexOf('L') == 0);

            if (selectedRow.inoutEtcStatus != '20') {
                this.etcUpsertModalOrderNo = selectedRow.orderNo;
                this.ledgerType_U = selectedRow.ledgerType;
                this.inoutEtcDt_U = moment(selectedRow.inoutEtcDt).format('YYYYMMDD');
                this.remark_U = selectedRow.remark;
                this.udateModalOpen = true;
            }
        }
    }

    //기타입고 수정 모달 닫기
    @action.bound
    closeUpdateModal = () => {
        this.udateModalOpen = false;
    }

    //기타입고 수정 저장
    @action.bound
    etcSave_U = async () => {
        const saveData = {
            "orderNo": this.etcUpsertModalOrderNo
            , "inoutEtcDt": this.inoutEtcDt_U
            , "ledgerType": this.ledgerType_U
            , "remark": this.remark_U

            , "buId": this.buId
            , "startDt": this.startDt
            , "endDt": this.endDt
            , "itemNmOrItemCd": this.itemNmOrItemCd
        }

        const params = JSON.stringify({ param: saveData });

        const value = await Alert.confirm("저장하시겠습니까?").then(result => { return result; });
        if (value) {
            const { data, status } = await EtcRepository.updateEtc(params);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {

                this.startDt = this.startDt <= this.inoutEtcDt_U ? this.startDt : this.inoutEtcDt_U;
                this.endDt = this.inoutEtcDt_U; //this.endDt >= this.inoutEtcDt_U ? this.endDt : this.inoutEtcDt_U;

                const searchData = {
                    "buId": this.buId
                    , "startDt": this.startDt
                    , "endDt": this.endDt
                    , "itemNmOrItemCd": ''
                }

                this.insertSearch(searchData, this.etcUpsertModalOrderNo);

            }
            this.closeUpdateModal();
        } else {
            return;
        }
    }

    // EtcEtcContents
    @action.bound
    setEtcEtcGridApi = async (gridApi) => {
        this.etcEtcGridApi = gridApi;
        const columnDefs = [
            {
                headerName: "지시번호"
                , field: "orderNo"
                , width: 70
                , customCellClass: 'cell_align_center'
            },
            {
                headerName: "출고일자"
                , field: "inoutEtcDt"
                , width: 90
                , customCellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "출고상태"
                , field: "inoutEtcStatusNm"
                , width: 90
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "출고유형"
                , field: "ledgerTypeNm"
                , width: 90
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "비고"
                , field: "remark"
                , width: 115
            },
            {
                headerName: "등록자"
                , field: "inputEmpNm"
                , width: 75
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "등록일"
                , field: "inputDtm"
                , width: 105
                , cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm')
            },
            {
                headerName: "확정자"
                , field: "confirmUserNm"
                , width: 75
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "확정일"
                , field: "confirmDtm"
                , width: 105
                , cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm')
            },
            {
                headerName: "비고"
                , field: "remark"
                , width: 200
            },
        ];
        this.etcEtcGridApi.setColumnDefs(columnDefs);
    }

    // EtcEtcContents 그리드 로우 클릭 시
    @action.bound
    getBarcode = async () => {
        const selectedRow = this.etcEtcGridApi.getSelectedRows()[0];
        if (!selectedRow || selectedRow.length == 0) {
            return;
        }
        if (selectedRow.itemId != 0) {
            this.etcBarcodeRegisterAvailableToggle = false;
            this.etcBarcodeDeleteAvailableToggle = false;
        } else {
            this.etcBarcodeRegisterAvailableToggle = true;
            this.etcBarcodeDeleteAvailableToggle = true;
        }
        this.barcodeOrderNo = selectedRow.orderNo;
        this.ledgerType = selectedRow.ledgerType;
        this.etcBarcodeUpsertModalOrderNo = selectedRow.orderNo;
        this.searchBarcode();
    }

    // EtcBarcodeMiddleItem 추가 클릭 시
    @action.bound
    etcBarcodeRegister = async () => {
        
        if (this.key != 'Y') {
            const selectedRow = this.etcEtcGridApi.getSelectedRows()[0];
            if (!selectedRow || selectedRow.length == 0) {
                Alert.meg("기타출고를 선택해 주세요.");
                return;
            }
            if (selectedRow.inoutEtcStatus == '20') {
                Alert.meg("이미 확정된 기타출고 입니다.");
                return;
            }
            if (selectedRow.inoutEtcStatus == '30') {
                Alert.meg("이미 ERP 전송이 완료된 지시입니다.");
                return;
            }

            this.etcBarcodeUpsertModalBarcode = "";
            this.etcBarcodeUpsertModalProdDt = moment().format('YYYYMMDD');
            this.etcBarcodeUpsertModalItemNmOrItemCd = "";
            this.etcBarcodeUpsertModalList = [];
            this.etcBarcodeUpsertModalIsOpen = true;
            this.etcUpsertModalLedgerType = selectedRow.ledgerType;



            //공통 바코드 추가 모델(조건부 때문에 주석처리)
            // this.barcodeHelplerOrderNo = selectedRow.orderNo;
            // this.barcodeHelplerledgerType = selectedRow.ledgerType;
            // this.isBarcodeHelperOpen = true;
        } else {
            this.etcBarcodeUpsertModalBarcode = "";
            this.etcBarcodeUpsertModalProdDt = moment().format('YYYYMMDD');
            this.etcBarcodeUpsertModalItemNmOrItemCd = "";
            this.etcBarcodeUpsertModalList = [];
            this.etcBarcodeUpsertModalIsOpen = true;
        }
    }
    // barcodeHelper 반환
    @action.bound
    barocdeHelperResult = async (result) => {
        if (result) {
            this.key = '';
            this.searchBarcode();
        }
    }

    // EtcBarcodeMiddleItem 삭제 클릭 시
    @action.bound
    etcBarcodeDelete = async () => {

        if (this.key == 'Y') { //등록에서 제거 클릭시
            const selectedRows = QualityStatusStore.holdBarcodeGridApi.getSelectedRows();

            selectedRows.forEach(selectedRow => {
                QualityStatusStore.holdBarcodeGridApi.updateRowData({ remove: [selectedRow] });
                const index = this.add.findIndex(x => x.barcode == selectedRow.barcode);

                this.add.splice(index, 1);
            })

        } else {
            const selectedRow = this.etcEtcGridApi.getSelectedRows()[0];
            if (selectedRow) {
                if (selectedRow.inoutEtcStatus == '20') {
                    Alert.meg("이미 확정된 기타출고 입니다.");
                    return;
                }
            } else {
                Alert.meg("기타출고를 선택해 주세요.");
                return;
            }
            const selectedRows = this.etcBarcodeGridApi.getSelectedRows();
            if (!selectedRows || selectedRows.length == 0) {
                Alert.meg("삭제할 바코드를 선택해 주세요.");
                return;
            }
            const searchData = {
                "orderNo": this.barcodeOrderNo
            }
            const params = JSON.stringify({ param1: selectedRows, param2: searchData });
            const value = await Alert.confirm("삭제하시겠습니까?").then(result => { return result; });
            if (value) {
                const { data, status } = await InoutRepository.deleteBarcode(params);
                if (data.success == false) {
                    Alert.meg(data.msg);
                } else {
                    this.etcBarcodeList = data.data;
                    this.etcBarcodeGridApi.setRowData(this.etcBarcodeList);
                }
            }
            else {
                return;
            }

        }
    }

    // EtcBarcodeContents
    @action.bound
    setEtcBarcodeGridApi = async (gridApi) => {
        this.etcBarcodeGridApi = gridApi;
        this.pinnedBottomRowDataAtEtcBarcodeGrid = [
            {
                seq: '합계',
                processQty: 0
            }
        ];
        const columnDefs = [
            {
                headerName: "",
                children: [
                    {
                        headerName: ""
                        , field: "check"
                        , width: 29
                        , checkboxSelection: true
                        , headerCheckboxSelection: true
                    },
                    {
                        headerName: "순번"
                        , field: "seq"
                        , width: 60
                        , type: "numericColumn"
                        , cellClass: 'cell_align_center'
                        , valueFormatter: GridUtil.numberFormatter
                        , valueParser: GridUtil.numberParser
                    },
                    {
                        headerName: "바코드"
                        , field: "barcode"
                        , width: 100
                        , cellClass: 'cell_align_center'
                    },
                    {
                        headerName: "상태"
                        , field: "lotQualityNm"
                        , width: 50
                        , cellClass: 'cell_align_center'
                    },
                    {
                        headerName: "제품코드"
                        , field: "itemCd"
                        , width: 80
                        , cellClass: 'cell_align_center'
                    },
                    {
                        headerName: "LOT"
                        , field: "lotNo"
                        , width: 90
                        , cellClass: 'cell_align_center'
                    },
                    {
                        headerName: "생산일자"
                        , field: "prodDt"
                        , width: 90
                        , cellClass: 'cell_align_center'
                        , valueFormatter: GridUtil.dateFormatter
                    },
                    {
                        headerName: "유통기한"
                        , field: "expiryDt"
                        , width: 90
                        , cellClass: 'cell_align_center'
                        , valueFormatter: GridUtil.dateFormatter
                    }
                ]
            },
            {
                headerName: "수량",
                children: [
                    {
                        headerName: "투입량"
                        , field: "processQty"
                        , width: 80
                        , type: "numericColumn"
                        , valueFormatter: (params) => GridUtil.numberFormatter(params, 2)
                        , valueParser: GridUtil.numberParser
                        , pinnedRowCellRenderer: "customPinnedRowRenderer"
                        , pinnedRowCellRendererParams: { style: { "fontWeight": "bold" }, decimalScale: 2 }
                    },
                    {
                        headerName: "단위"
                        , field: "processUom"
                        , width: 65
                        , cellClass: 'cell_align_center'
                    }
                ]
            },
            // {
            //     headerName: "",
            //     children: [
            //         {
            //             headerName: "위치"
            //             , field: "locNm"
            //             , width: 80
            //             , cellClass: 'cell_align_center'
            //         },
            //         {
            //             headerName: "처리시간"
            //             , field: "inputDtm"
            //             , width: 120
            //             , cellClass: 'cell_align_center'
            //             , valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm')
            //         },
            //         {
            //             headerName: "처리자"
            //             , field: "inputEmpNm"
            //             , width: 90
            //             , cellClass: 'cell_align_center'
            //         }
            //     ]
            // }
        ];
        this.etcBarcodeGridApi.setColumnDefs(columnDefs);
    }

    // EtcBarcodeContents 조회
    @action.bound
    searchBarcode = async () => {
        const searchData = {
            "orderNo": this.barcodeOrderNo
        }
        const { data, status } = await InoutRepository.getPallet(searchData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            this.etcBarcodeList = data.data;
            this.etcBarcodeGridApi.setRowData(this.etcBarcodeList);
        }
    }

    // EtcUpsertModal 닫기
    @action.bound
    closeEtcUpsertModal = async () => {
        this.etcUpsertModalIsOpen = false;
        this.add = [];
        this.key = '';
    }

    // EtcUpsertModal 저장 클릭 시
    @action.bound
    etcUpsertModalSave = async () => {

        const saveData = {
            "orderNo": this.etcUpsertModalOrderNo
            , "inoutEtcDt": this.etcUpsertModalInoutEtcDt
            , "ledgerType": this.etcUpsertModalLedgerType
            , "remark": this.etcUpsertModalRemark
            , "changeItemId": this.etcUpsertModalLedgerType == 'LZ' ?  this.changeItem : this.itemId
            , "buId": this.buId
            , "startDt": this.startDt
            , "endDt": this.endDt
            , "itemNmOrItemCd": this.itemNmOrItemCd
        }

        const value = await Alert.confirm("저장하시겠습니까?").then(result => { return result; });
        if (value) {
            QualityStatusStore.holdBarcodeGridApi.selectAll();
            const selectedRows = QualityStatusStore.holdBarcodeGridApi.getSelectedRows();
            const params = JSON.stringify({ param1: selectedRows, param2: saveData });
            const { data, status } = await EtcRepository.saveEtc(params);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {

                //key값 초기화
                this.key = '';

                //add값 초기화
                this.add = [];

                this.startDt = this.startDt <= this.etcUpsertModalInoutEtcDt ? this.startDt : this.etcUpsertModalInoutEtcDt;
                this.endDt = this.etcUpsertModalInoutEtcDt; //this.endDt >= this.etcUpsertModalInoutEtcDt ? this.endDt : this.etcUpsertModalInoutEtcDt;

                const searchData = {
                    "buId": this.buId
                    , "startDt": this.startDt
                    , "endDt": this.endDt
                    , "itemNmOrItemCd": ''
                }

                this.insertSearch(searchData);

            }
            this.closeEtcUpsertModal();
        } else {
            QualityStatusStore.holdBarcodeGridApi.deselectAll();
            return;
        }
    }

    // EtcBarcodeUpsertModal 조회 클릭 시
    @action.bound
    etcBarcodeUpsertModalSearch = async () => {
        const searchData = {
            "buId": this.buId
            , "barcode": this.etcBarcodeUpsertModalBarcode
            , "prodDt": this.etcBarcodeUpsertModalProdDt
            , "itemNmOrItemCd": this.etcBarcodeUpsertModalItemNmOrItemCd
        }
        const { data, status } = await EtcRepository.getBarcode(searchData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            this.etcBarcodeUpsertModalList = data.data;

            if (this.etcUpsertModalLedgerType == 'LZ') {

                let etcList = [];
                this.etcBarcodeUpsertModalList.forEach(item => {
                    if (item.lotQuality == "30") {
                        etcList.push(item);
                    }

                })

                this.etcBarcodeUpsertModalGridApi.setRowData(etcList);
            } else {

                let etcList = [];
                this.etcBarcodeUpsertModalList.forEach(item => {
                    if (item.lotQuality == "10") {
                        etcList.push(item);
                    }
                })

                this.etcBarcodeUpsertModalGridApi.setRowData(etcList);

            }
        }
    }

    // EtcBarcodeUpsertModal 닫기
    @action.bound
    closeEtcBarcodeUpsertModal = async () => {
        this.etcBarcodeUpsertModalIsOpen = false;
    }



    // EtcBarcodeUpsertModal 저장 클릭 시
    @action.bound
    etcBarcodeUpsertModalSave = async () => {

        if (this.key == 'Y') {
            //기타출고에서 바코드를 추가 저장 할 때
            const selectedRows = this.etcBarcodeUpsertModalGridApi.getSelectedRows();
            if (!selectedRows || selectedRows.length == 0) {
                Alert.meg("바코드를 선택해 주세요.");
                return;
            }

            selectedRows.forEach(item => {
                if (!this.add.some(x => x.barcode == item.barcode)) {
                    this.add.push(item);
                }
            })

            QualityStatusStore.holdBarcodeGridApi.setRowData(this.add);
            this.etcBarcodeUpsertModalIsOpen = false;

        } else {
            //지시등록이 아닌 곳에서 바코드 추가 저장 할 때
            const selectedRows = this.etcBarcodeUpsertModalGridApi.getSelectedRows();
            if (!selectedRows || selectedRows.length == 0) {
                Alert.meg("바코드를 선택해 주세요.");
                return;
            }
            const searchData = {
                "orderNo": this.etcBarcodeUpsertModalOrderNo
                , "ledgerType": this.ledgerType
            }
            const params = JSON.stringify({ param1: selectedRows, param2: searchData });
            const { data, status } = await EtcRepository.saveBarcode(params);
            if (data.success == false) {
                Alert.meg(data.msg);
                return;
            } else {
                this.getBarcode();
                // this.etcBarcodeList = data.data;
                // this.etcBarcodeGridApi.setRowData(this.etcBarcodeList);
                this.etcBarcodeUpsertModalIsOpen = false;
            }
        }
    }

    // EtcBarcodeUpsertModal 그리드
    @action.bound
    setEtcBarcodeUpsertModalGridApi = async (gridApi) => {
        this.etcBarcodeUpsertModalGridApi = gridApi;
        const columnDefs = [
            {
                headerName: ""
                , field: "check"
                , width: 29
                , checkboxSelection: true
                , headerCheckboxSelection: true
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "순번"
                , field: "seq"
                , width: 40
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
            },
            {
                headerName: "바코드"
                , field: "barcode"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "상태"
                , field: "lotQualityNm"
                , width: 50
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "제품코드"
                , field: "itemCd"
                , width: 90
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "LOT"
                , field: "lotNo"
                , width: 80
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "생산일자"
                , field: "prodDt"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "유통기한"
                , field: "expiryDt"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "재고량"
                , field: "processQty"
                , width: 60
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
            },
            {
                headerName: "위치"
                , field: "locNm"
                , width: 120
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "처리시간"
                , field: "inputDtm"
                , width: 110
                , cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm')
            },
            {
                headerName: "처리자"
                , field: "inputEmpNm"
                , width: 90
                , cellClass: 'cell_align_center'
            }
        ];
        this.etcBarcodeUpsertModalGridApi.setColumnDefs(columnDefs);
    }












































































































































































































    insertSearch = async (search, orderNo) => {

        const { data, status } = await EtcRepository.getEtcs(search);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            this.etcEtcList = data.data;
            this.etcEtcGridApi.setRowData(this.etcEtcList);

            if (orderNo == undefined) {
                GridUtil.setFocusByRowIndex(this.etcEtcGridApi, this.etcEtcList.length - 1);
            } else {
                const index = this.etcEtcList.findIndex(x => x.orderNo == orderNo);
                GridUtil.setFocusByRowIndex(this.etcEtcGridApi, index);
            }
        }
    }


























    // 기타출고 등록(수량) 관련 변수
    @observable etcQtyInsertModalIsOpen = false;
    @observable etcQtyUpsertModalOrderNo = 0;
    @observable etcQtyOrderDt = moment().format('YYYYMMDD');
    @observable selectedEtcQtyItem = {};
    @observable etcQtyItemHelperOption = new ItemOptionModel().optionModel;
    @observable etcQtyProcessQty = 0;
    @observable etcQtyText = '';
    @observable itemId_q = 0;
    @observable itemCd_q = '';
    @observable itemNm_q = '';

    // 기타출고 수정(수량) 관련 변수
    @observable etcQtyUpdateModalIsOpen = false;
    @observable etcQtyOrderDt_u = moment().format('YYYYMMDD');
    @observable etcQtyItemHelperOption_u = new ItemOptionModel().optionModel;
    @observable selectedEtcQtyItem_u = {};
    @observable etcQtyBarcode_u = '';
    @observable etcQtyUpsertModalOrderNo_u = 0;
    @observable etcQtyProcessQty_u = 0;
    @observable etcQtyProcessUom_u = '';
    @observable ledgerType_u = '';
    @observable remark_u = '';

    // 기타출고 DoubleClick시 Btn visible Event
    @observable etcBarcodeRegisterAvailableToggle = true;
    @observable etcBarcodeDeleteAvailableToggle = true;

    // 등록(수량) 모달Btn
    @action.bound
    etcQtyInsertModal = async () => {

        let allBu = this.buId.split(',');

        if (allBu.length > 1) {
            Alert.meg("선택된 공장이 없습니다.");
            return;
        }

        const all = CommonStore.codeObject["MFD002"]
        this.rawList = CommonStore.codeObject["MFD023"];
        await this.rawList.forEach(element => {
            if(element.desc1 == this.buId) {
                this.changeItem  = element.cd
                return
            }
        });
        this.ledgerTypeGroup = all.filter(x => x.desc2 == "Y");
        this.etcUpsertModalLedgerType = "LB";
        this.key = 'Y';  //바코드내역 - 추가버튼 다르게 사용하기 위해 넣음
        this.etcQtyInsertModalIsOpen = true;
    }

    // 기타출고 등록 Modal 닫기
    @action.bound
    closeEtcQtyInsertModal = () => {
        this.etcQtyOrderDt = moment().format('YYYYMMDD');
        this.selectedEtcQtyItem = {};
        this.etcQtyProcessQty = 0;
        this.ledgerTypeGroup = [];
        this.etcUpsertModalLedgerType = "";
        this.etcQtyText = '';
        this.etcQtyInsertModalIsOpen = false;
    }

    // ItemHelper 설정
    @action.bound
    etcQtyItemHelperResult = (result) => {
        if (result) {
            this.itemId_q = this.selectedEtcQtyItem.itemId;
            this.itemCd_q = this.selectedEtcQtyItem.itemCd;
            this.itemNm_q = this.selectedEtcQtyItem.itemNm;
        }
    }

    @action.bound
    itemHelperResult = (result) => {
        if (result) {
            this.itemId = this.selectedItem.itemId;
            this.itemCd = this.selectedItem.itemCd;
            this.itemNm = this.selectedItem.itemNm;
        }
    }
    
    @action.bound
    handleHelperOpen = () => {

        this.etcQtyItemHelperOption.buId = this.buId;
        this.itemHelperOption.buId = this.buId;
    }

    // 기타출고 등록(수량) '저장' Btn
    //etcQtyUpsertModalSave
    @action.bound
    btnEtcQtySave = async () => {

        const validation = this.etnQtyInsertValidator.validate(this);
        if (!validation.isValid) {
            return;
        }

        if (this.etcQtyProcessQty == 0 || this.itemId_q == null) {
            Alert.meg('등록할 제품/수량을 확인 해주세요.');
        }

        const submitData = {
            "inoutEtcDt": this.etcQtyOrderDt
            , "ledgerType": this.etcUpsertModalLedgerType
            , "remark": this.etcQtyText
            , "orderNo": this.etcQtyUpsertModalOrderNo
            , "itemId": this.itemId_q
            , "itemCd": this.itemCd_q
            , "itemNm": this.itemNm_q
            , "processQty": this.etcQtyProcessQty
            , "changeItemId": this.etcUpsertModalLedgerType == 'LZ' ?  this.changeItem : null
            , "buId": this.buId
            , "startDt": this.startDt
            , "endDt": this.endDt
            , "itemNmOrItemCd": this.itemNmOrItemCd
        }
        
        console.log(this.changeItem, 'this.changeItem')
        const value = await Alert.confirm("저장하시겠습니까?").then(result => { return result; });
        if (value) {
            const { data, status } = await EtcRepository.saveQtyEtc(submitData);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                if (data.data.sqlMessage != null) {
                    Alert.meg(data.data.sqlMessage);
                    return;
                }

                //key값 초기화
                this.key = '';

                this.startDt = this.startDt <= this.etcQtyOrderDt ? this.startDt : this.etcQtyOrderDt;
                this.endDt = this.etcQtyOrderDt; //this.endDt >= this.etcQtyOrderDt ? this.endDt : this.etcQtyOrderDt;

                const searchData = {
                    "buId": this.buId
                    , "startDt": this.startDt
                    , "endDt": this.endDt
                    , "itemNmOrItemCd": this.itemNmOrItemCd
                }

                this.insertSearch(searchData);
            }
            this.closeEtcQtyInsertModal();
        } else {
            // QualityStatusStore.holdBarcodeGridApi.deselectAll();
            return;
        }
    }

    //기타출고 수정(수량) 변경 Btn
    @action.bound
    btnEtcQtyUpdateModalSave = async () => {

        const selectedRow = GridUtil.getSelectedRowData(this.etcEtcGridApi);

        const submitData = {
            "orderNo": this.etcQtyUpsertModalOrderNo_u
            , "barcode": this.etcQtyBarcode_u
            , "ledgerType": this.ledgerType_u
            , "remark": this.remark_u
            , "processQty": this.etcQtyProcessQty_u
            , "buId": this.buId
            , "startDt": this.startDt
            , "endDt": this.endDt
            , "itemNmOrItemCd": this.itemNmOrItemCd
        }

        const value = await Alert.confirm("변경하시겠습니까?").then(result => { return result; });
        if (value) {
            const { data, status } = await EtcRepository.etcQtyUpdateModalSave(submitData);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {

                if (data.data.sqlMessage != null) {
                    Alert.meg(data.data.sqlMessage);
                    return;
                }
                //key값 초기화
                this.key = '';

                this.startDt = this.startDt < selectedRow.inoutEtcDt ? this.startDt : selectedRow.inoutEtcDt;
                this.endDt = selectedRow.inoutEtcDt; //this.endDt > selectedRow.inoutEtcDt ? this.endDt : selectedRow.inoutEtcDt;

                const searchData = {
                    "buId": this.buId
                    , "startDt": this.startDt
                    , "endDt": this.endDt
                    , "itemNmOrItemCd": this.itemNmOrItemCd
                }

                this.insertSearch(searchData, selectedRow.orderNo);


            }
            this.closeEtcQtyUpdateModal();
        } else {
            // QualityStatusStore.holdBarcodeGridApi.deselectAll();
            return;
        }
    }

    @action.bound
    closeEtcQtyUpdateModal = () => {
        this.ledgerType_u = '';
        this.etcQtyOrderDt_u = moment().format('YYYYMMDD');
        this.etcQtyUpsertModalOrderNo_u = 0;
        this.etcQtyBarcode_u = '';
        this.selectedEtcQtyItem_u = {};
        this.etcQtyProcessQty_u = 0;
        this.etcQtyProcessUom_u = '';
        this.remark_u = '';
        this.etcQtyUpdateModalIsOpen = false;
    }

}
export default new EtcStore();