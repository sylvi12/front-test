import API from "components/override/api/API";

class EtcRepository {

    URL = "/api/inout/etc";
    /* 조회 */
    // 배차 그리드 조회
    getEtcs(param) {
        return API.request.get(encodeURI(`${this.URL}/getetcs?buId=${param.buId}&startDt=${param.startDt}&endDt=${param.endDt}&itemNmOrItemCd=${param.itemNmOrItemCd}`))
    }
    // 바코드 등록 모달 그리드 조회
    getBarcode(param) {

        return API.request.get(encodeURI(`${this.URL}/getbarcode?buId=${param.buId}&barcode=${param.barcode}&prodDt=${param.prodDt}&itemNmOrItemCd=${param.itemNmOrItemCd}`))

    }

    /* 등록 */
    // 기타출고 등록
    saveEtc(params) {
        return API.request.post(encodeURI(`${this.URL}/saveetc`), params)
    }
    // 기타출고 수량 등록
    saveQtyEtc(param) {
        return API.request.post(encodeURI(`${this.URL}/saveqtyetc`), param)
    }
    // 기타출고 수정 정보
    getEtcChangeData(param) {
        return API.request.get(encodeURI(`${this.URL}/getetcchangedata?orderNo=${param.orderNo}`))
    }

    // 기타출고 수정 변경Btn
    etcQtyUpdateModalSave(param) {
        return API.request.post(encodeURI(`${this.URL}/etcqtyupdatemodalsave`), param)
    }

    // 바코드 등록
    saveBarcode(params) {
        return API.request.post(encodeURI(`${this.URL}/savebarcode`), params)
    }

    // 기타출고 수정
    updateEtc(params) {
        return API.request.post(encodeURI(`${this.URL}/updateetc`), params)
    }

    /* 삭제 */
    delete(params) {
        return API.request.post(encodeURI(`${this.URL}/deleteetc`), params)
    }
    /* 확정 */
    confirm(param) {
        return API.request.post(encodeURI(`${this.URL}/confirm`), param)
    }
    /* 확정 취소 */
    cancel(param) {
        return API.request.post(encodeURI(`${this.URL}/cancel`), param)
    }
}
export default new EtcRepository();