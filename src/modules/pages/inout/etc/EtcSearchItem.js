import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SInput from 'components/atoms/input/SInput'
import SButton from 'components/atoms/button/SButton'
import EtcStore from 'modules/pages/inout/etc/service/EtcStore';
import SBuSelectBox from 'components/override/business/SBuSelectBox';

@inject(stores => ({
    buId: stores.etcStore.buId,
    buGroup: stores.etcStore.buGroup,
    handleChange: stores.etcStore.handleChange,
    startDt: stores.etcStore.startDt,
    endDt: stores.etcStore.endDt,
    itemNmOrItemCd: stores.etcStore.itemNmOrItemCd,
    handleSearchClick: stores.etcStore.handleSearchClick,
}))
class EtcSearchItem extends Component {

    constructor(props) {
        super(props);
    }

    async componentWillMount() {
        if (EtcStore.alphaCheck == false) {
            await EtcStore.getBu();
        }
    }

    render() {

        const { buId, buGroup, handleChange
            , startDt, endDt
            , itemNmOrItemCd, handleSearchClick
        } = this.props;

        return (
            <React.Fragment>
                {/* <SSelectBox
                    title={"공장"}
                    id={"buId"}
                    value={buId}
                    optionGroup={buGroup}
                    valueMember={'buId'}
                    displayMember={'buNm'}
                    onChange={handleChange}
                    addOption={"전체"}
                /> */}
                <SBuSelectBox
                    title={"공장"}
                    id={"buId"}
                    value={buId}
                    onChange={handleChange}
                // onSelectionChange={buSelectionChanged}
                />

                <SDatePicker
                    title={"출고일자"}
                    id="startDt"
                    value={startDt}
                    onChange={handleChange}
                />

                <SDatePicker
                    title={"~"}
                    id="endDt"
                    value={endDt}
                    onChange={handleChange}
                />

                <SInput
                    title={"제품"}
                    id={"itemNmOrItemCd"}
                    value={itemNmOrItemCd}
                    onChange={handleChange}
                    onEnterKeyDown={handleSearchClick}
                />

                <div className='search_item_btn'>
                    <SButton buttonName={'조회'} onClick={handleSearchClick} type={'btnSearch'} />
                </div>

            </React.Fragment>
        )
    }
}

export default EtcSearchItem;