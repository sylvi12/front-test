import React, { Component } from "react";
import { inject } from "mobx-react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";
import SModal from "components/override/modal/SModal";
import BarcodeAddHelper from "components/codehelper/BarcodeAddHelper";

import EtcSearchItem from "modules/pages/inout/etc/EtcSearchItem";
import EtcEtcMiddleItem from "modules/pages/inout/etc/etcContentsAssemble/EtcEtcMiddleItem";
import EtcEtcContents from "modules/pages/inout/etc/etcContentsAssemble/EtcEtcContents";
import EtcBarcodeMiddleItem from "modules/pages/inout/etc/EtcBarcodeMiddleItem";
import EtcBarcodeContents from "modules/pages/inout/etc/etcContentsAssemble/EtcBarcodeContents";
import EtcContents from "modules/pages/inout/etc/EtcContents";
import EtcUpsertModal from "modules/pages/inout/etc/etcContentsAssemble/EtcUpsertModal";
import EtcBarcodeUpsertModal from "modules/pages/inout/etc/etcBarcodeUpsertModal/EtcBarcodeUpsertModal";
import EtcUpdateModal from "modules/pages/inout/etc/etcUpdateModal/EtcUpdateModal";

import EtcQtyInsertModal from "modules/pages/inout/etc/etcContentsAssemble/EtcQtyInsertModal";
import EtcQtyUpdateModal from "modules/pages/inout/etc/etcContentsAssemble/EtcQtyUpdateModal";

@inject(stores => ({
    etcUpsertModalIsOpen: stores.etcStore.etcUpsertModalIsOpen
    , closeEtcUpsertModal: stores.etcStore.closeEtcUpsertModal
    , etcQtyInsertModalIsOpen: stores.etcStore.etcQtyInsertModalIsOpen
    , closeEtcQtyInsertModal: stores.etcStore.closeEtcQtyInsertModal
    , etcQtyUpdateModalIsOpen: stores.etcStore.etcQtyUpdateModalIsOpen
    , closeEtcQtyUpdateModal: stores.etcStore.closeEtcQtyUpdateModal

    , etcBarcodeUpsertModalIsOpen: stores.etcStore.etcBarcodeUpsertModalIsOpen
    , closeEtcBarcodeUpsertModal: stores.etcStore.closeEtcBarcodeUpsertModal

    , isBarcodeHelperOpen: stores.etcStore.isBarcodeHelperOpen
    , barocdeHelperResult: stores.etcStore.barocdeHelperResult
    , handleChange: stores.etcStore.handleChange
    , barcodeHelplerOrderNo: stores.etcStore.barcodeHelplerOrderNo
    , barcodeHelplerledgerType: stores.etcStore.barcodeHelplerledgerType
    , udateModalOpen: stores.etcStore.udateModalOpen
    , closeUpdateModal: stores.etcStore.closeUpdateModal
}))

class Etc extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { etcUpsertModalIsOpen, closeEtcUpsertModal, etcQtyInsertModalIsOpen, closeEtcQtyInsertModal
            , etcQtyUpdateModalIsOpen, closeEtcQtyUpdateModal
            , etcBarcodeUpsertModalIsOpen, closeEtcBarcodeUpsertModal
            , isBarcodeHelperOpen, barocdeHelperResult, handleChange, barcodeHelplerOrderNo, barcodeHelplerledgerType
            , udateModalOpen, closeUpdateModal } = this.props

        return (
            <React.Fragment>
                <div style={{ width: '100%', height: '60%' }}>
                    <SearchTemplate searchItem={<EtcSearchItem />} />                
                    <ContentsMiddleTemplate contentSubTitle={'기타출고 내역'} middleItem={<EtcEtcMiddleItem />} />
                    <ContentsTemplate id='etcEtc' contentItem={<EtcEtcContents />} />
                </div>
                <div style={{ width: '100%', height: '40%' }}>
                    <ContentsMiddleTemplate contentSubTitle={'바코드 내역'} middleItem={<EtcBarcodeMiddleItem />} />
                    <ContentsTemplate id='etcBarcode' contentItem={<EtcBarcodeContents />} />
                </div>
                {/* <ContentsTemplate id='etc' contentItem={<EtcContents />} /> */}

                {/* 수량Base 등록 Modal */}
                <SModal
                    formId={"inout_etcqty_p"}
                    isOpen={etcQtyInsertModalIsOpen}
                    onRequestClose={closeEtcQtyInsertModal}
                    contentLabel="기타출고 등록(수량)"
                    width={650}
                    height={265}
                    contents={<EtcQtyInsertModal />}
                />

                <SModal
                    formId={"inout_etcqty_update_p"}
                    isOpen={etcQtyUpdateModalIsOpen}
                    onRequestClose={closeEtcQtyUpdateModal}
                    contentLabel="기타출고 수정(수량)"
                    width={800}
                    height={250}
                    contents={<EtcQtyUpdateModal />}
                />

                <SModal
                    formId={"inout_etc_p"}
                    isOpen={etcUpsertModalIsOpen}
                    onRequestClose={closeEtcUpsertModal}
                    contentLabel="기타출고 등록(바코드)"
                    width={800}
                    height={500}
                    contents={<EtcUpsertModal />}
                />

                <SModal
                    formId={"inout_etc_update_p"}
                    isOpen={udateModalOpen}
                    onRequestClose={closeUpdateModal}
                    contentLabel="기타출고 수정(바코드)"
                    width={500}
                    height={400}
                    contents={<EtcUpdateModal />}
                />

                <SModal
                    formId={"inout_barcode_add_p"}
                    isOpen={etcBarcodeUpsertModalIsOpen}
                    onRequestClose={closeEtcBarcodeUpsertModal}
                    contentLabel="바코드 추가"
                    width={1220}
                    height={530}
                    contents={<EtcBarcodeUpsertModal />}
                />

                <BarcodeAddHelper
                    id={'isBarcodeHelperOpen'}
                    isBarcodeOpen={isBarcodeHelperOpen}
                    barcodeHelplerOrderNo={barcodeHelplerOrderNo}
                    barcodeHelplerledgerType={barcodeHelplerledgerType}
                    handleChange={handleChange}
                    onHelperResult={barocdeHelperResult}
                />
            </React.Fragment>
        )
    }
}

export default Etc;