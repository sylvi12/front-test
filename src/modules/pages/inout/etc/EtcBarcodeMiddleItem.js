import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    etcBarcodeRegisterAvailableToggle: stores.etcStore.etcBarcodeRegisterAvailableToggle
    , etcBarcodeDeleteAvailableToggle: stores.etcStore.etcBarcodeDeleteAvailableToggle
    , etcBarcodeRegister: stores.etcStore.etcBarcodeRegister
    , etcBarcodeDelete: stores.etcStore.etcBarcodeDelete
}))

class EtcBarcodeMiddleItem extends Component {

    render() {

        const { 
            etcBarcodeRegisterAvailableToggle
            , etcBarcodeDeleteAvailableToggle
            , etcBarcodeRegister
            , etcBarcodeDelete } = this.props;
        return (
            <React.Fragment>
                <SButton
                    className="btn_red"
                    buttonName={'추가'}
                    type={'btnBarcodeAdd'}
                    onClick={etcBarcodeRegister}
                    visible={etcBarcodeRegisterAvailableToggle}
                />
                <SButton
                    className="btn_red"
                    buttonName={'삭제'}
                    type={'btnBarcodeDelete'}
                    onClick={etcBarcodeDelete}
                    visible={etcBarcodeDeleteAvailableToggle}
                />
            </React.Fragment>
        )
    }
}

export default EtcBarcodeMiddleItem;