import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setEtcEtcGridApi: stores.etcStore.setEtcEtcGridApi
    , etcEtcList: stores.etcStore.etcEtcList
    , getBarcode: stores.etcStore.getBarcode
    , openModalEdit: stores.etcStore.openModalEdit
}))

class EtcEtcContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setEtcEtcGridApi, etcEtcList, getBarcode, openModalEdit } = this.props;

        return (
            <SGrid
                grid={'etcEtcGrid'}
                gridApiCallBack={setEtcEtcGridApi}
                rowData={etcEtcList}
                onSelectionChanged={getBarcode}
                rowDoubleClick={openModalEdit}
                editable={false}
            />
        );
    }
}
export default EtcEtcContents;