import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import STextArea from 'components/atoms/input/STextArea';
import SButton from 'components/atoms/button/SButton';
import ItemHelper from 'components/codehelper/ItemHelper';
import SNumericInput from 'components/override/numericinput/SNumericInput';

@inject(stores => ({
	btnEtcQtyUpdateModalSave: stores.etcStore.btnEtcQtyUpdateModalSave
	, etcQtyOrderDt_u: stores.etcStore.etcQtyOrderDt_u
	, selectedEtcQtyItem_u: stores.etcStore.selectedEtcQtyItem_u
	, etcQtyItemHelperOption_u: stores.etcStore.etcQtyItemHelperOption_u
	, ledgerType_u: stores.etcStore.ledgerType_u
	, ledgerTypeGroup: stores.etcStore.ledgerTypeGroup
	, etcQtyProcessQty_u: stores.etcStore.etcQtyProcessQty_u
	, etcQtyProcessUom_u: stores.etcStore.etcQtyProcessUom_u
	, remark_u: stores.etcStore.remark_u
	, handleChange: stores.etcStore.handleChange
}))

class EtcQtyUpdateModal extends Component {

	constructor(props) {
		super(props);
	}

	render() {

		const {
			btnEtcQtyUpdateModalSave
			, etcQtyOrderDt_u
			, selectedEtcQtyItem_u
			, etcQtyItemHelperOption_u
			, etcQtyProcessQty_u
			, etcQtyProcessUom_u
			, ledgerType_u
			, ledgerTypeGroup
			, remark_u
			, handleChange
		} = this.props;

		return (
			<React.Fragment>
				<div className='search_item_btn'>
					<SButton 
					buttonName={"변경"} 
					onClick={btnEtcQtyUpdateModalSave} 
					type={"btnEtcQtyUpdateModalSave"} />
				</div>
				<table style={{ width: "100%" }}>
					<tbody>
						<tr>
							<th>출고일자</th>
							<td>
								<SDatePicker
									id="etcQtyOrderDt_u"
									value={etcQtyOrderDt_u}
									onChange={handleChange}
									readOnly={true}
								/>
							</td>
							<th>출고제품</th>
							<td>
								<ItemHelper
									selectedItemId={"selectedEtcQtyItem_u"}
									defaultValue={selectedEtcQtyItem_u} //기본값 변수
									onChange={handleChange}
									helperOption={etcQtyItemHelperOption_u} // 인스턴스 생성 및 세팅 변수
									cdName={'itemCd'}
									cdNmName={'itemNm'}
									readOnly={true}
								/>
							</td>
						</tr>
						<tr>
						<th>출고유형</th>
							<td>
								<SSelectBox
									id="ledgerType_u"
									value={ledgerType_u}
									// codeGroup={"MFD002"}
									optionGroup={ledgerTypeGroup}
									onChange={handleChange}
								/>
							</td>
							<th>출고량</th>
							<td>
								<SNumericInput
									id={"etcQtyProcessQty_u"}
									value={etcQtyProcessQty_u}
									onChange={handleChange}
								/>
								{etcQtyProcessUom_u}
							</td>
						</tr>
						<tr>
						<th>비고</th>
							<td colSpan="3">
								<STextArea
									id={"remark_u"}
									value={remark_u}
									onChange={handleChange}
									contentWidth={690}
								/>
							</td>
						</tr>
					</tbody>
				</table>
			</React.Fragment>
		);
	}
}

export default EtcQtyUpdateModal;