import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import STextArea from 'components/atoms/input/STextArea';
import SButton from 'components/atoms/button/SButton';

import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';
import ItemHelper from 'components/codehelper/ItemHelper';

import EtcBarcodeMiddleItem from "modules/pages/inout/etc/EtcBarcodeMiddleItem";
import HoldInsertModalContents from "modules/pages/inventory/qualityStatus/qualityStatusModal/HoldInsertModalContents";
import EtcBarcodeAddMiddleItem from 'modules/pages/inout/etc/etcContentsAssemble/EtcBarcodeAddMiddleItem';
import { ColumnGroup } from 'ag-grid-community';


@inject(stores => ({
    etcUpsertModalInoutEtcDt: stores.etcStore.etcUpsertModalInoutEtcDt
    , handleChange: stores.etcStore.handleChange

    , etcUpsertModalLedgerType: stores.etcStore.etcUpsertModalLedgerType
    , ledgerTypeGroup: stores.etcStore.ledgerTypeGroup
    , etcUpsertModalRemark: stores.etcStore.etcUpsertModalRemark

    , closeEtcUpsertModal: stores.etcStore.closeEtcUpsertModal
    , etcUpsertModalSave: stores.etcStore.etcUpsertModalSave
    , selectedItem: stores.etcStore.selectedItem
    , itemHelperOption: stores.etcStore.itemHelperOption
    , itemHelperResult: stores.etcStore.itemHelperResult
    , handleHelperOpen: stores.etcStore.handleHelperOpen
    , rawList: stores.etcStore.rawList
    , buId: stores.etcStore.buId
    , changeItem : stores.etcStore.changeItem
}))
@observer
class EtcUpsertModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { etcUpsertModalInoutEtcDt, handleChange
            , etcUpsertModalLedgerType, ledgerTypeGroup
            , etcUpsertModalRemark
            , closeEtcUpsertModal
            , etcUpsertModalSave
            , itemHelperOption
            , selectedItem
            , itemHelperResult
            , handleHelperOpen
            , rawList
            , buId
            , changeItem
        } = this.props;

        var selectboxList = []
		rawList.forEach(element => {
			if(element.desc1 == buId) {
				selectboxList.push(element)
			}
		});

        console.log(etcUpsertModalLedgerType , 'etcUpsertModalLedgerType')
        return (
            <React.Fragment>
                <div className='btn_wrap'>
                    <SButton buttonName={"저장"} onClick={etcUpsertModalSave} type={"btnEtcSave"} />
                    {/* <SButton buttonName={"닫기"} onClick={closeEtcUpsertModal} type={"default"} /> */}
                </div>

                <table style={{ width: "100%" }}>
                    <tbody>
                        <tr>
                            <th>출고일자</th>
                            <td>
                                <SDatePicker
                                    id="etcUpsertModalInoutEtcDt"
                                    value={etcUpsertModalInoutEtcDt}
                                    onChange={handleChange}
                                />
                            </td>
                        </tr>
                        <tr>
                                <th>출고유형</th>
                                <td>
                                    <SSelectBox
                                        id="etcUpsertModalLedgerType"
                                        value={etcUpsertModalLedgerType}
                                        // codeGroup={"MFD002"}
                                        optionGroup={ledgerTypeGroup}
                                        onChange={handleChange}
                                    />
                                </td>
                            </tr>
                        {etcUpsertModalLedgerType == "LZ" ?
                            <tr>
                                <th>품변선택</th>
                                <td>
                                    <SSelectBox
                                        id="changeItem"
                                        value={changeItem}
                                        // codeGroup={"MFD002"}
                                        optionGroup={selectboxList}
                                        onChange={handleChange}
                                    />
                                </td>
                            </tr>
                            : null
                        }
                        <tr>
                            <th>비고</th>
                            <td colSpan="3">
                                <STextArea
                                    id="etcUpsertModalRemark"
                                    value={etcUpsertModalRemark}
                                    onChange={handleChange}
                                    contentWidth={690}
                                />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <ContentsMiddleTemplate contentSubTitle={'바코드 내역'} middleItem={<EtcBarcodeAddMiddleItem />} />
                <ContentsTemplate id='holdInsertModal' contentItem={<HoldInsertModalContents />} />

                {/* <ContentsTemplate id='locationBarcode' contentItem={<EtcBarcodeContents />} /> */}
                {/* HOLD관리Component (같은스토어에 있는 Grid를 불러오면 더이상 작동하지 않음)*/}


            </React.Fragment>
        );
    }
}

export default EtcUpsertModal;