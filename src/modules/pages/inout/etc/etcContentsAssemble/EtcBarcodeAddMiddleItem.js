import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    etcBarcodeRegister: stores.etcStore.etcBarcodeRegister
    , etcBarcodeDelete: stores.etcStore.etcBarcodeDelete
}))

class EtcBarcodeAddMiddleItem extends Component {

    render() {

        const { etcBarcodeRegister, etcBarcodeDelete } = this.props;
        return (
            <React.Fragment>
                <SButton
                    className="btn_red"
                    buttonName={'추가'}
                    type={'btnBarcodeAdd'}
                    onClick={etcBarcodeRegister}
                />
                <SButton
                    className="btn_red"
                    buttonName={'제거'}
                    type={'btnBarcodeDelete'}
                    onClick={etcBarcodeDelete}
                />
            </React.Fragment>
        )
    }
}

export default EtcBarcodeAddMiddleItem;