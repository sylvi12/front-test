import React, { Component } from 'react';
import { inject } from 'mobx-react';

import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import STextArea from 'components/atoms/input/STextArea';
import SButton from 'components/atoms/button/SButton';

import EtcInInsertMiddleItem from 'modules/pages/inout/etcIn/etcInModal/EtcInInsertMiddleItem';
import HoldInsertModalContents from "modules/pages/inventory/qualityStatus/qualityStatusModal/HoldInsertModalContents";
import BarcodeAddHelper from 'components/codehelper/BarcodeAddHelper';

import ItemHelper from 'components/codehelper/ItemHelper';
import EmployeeHelper from 'components/codehelper/EmployeeHelper';
import SNumericInput from 'components/override/numericinput/SNumericInput';

@inject(stores => ({
    btnEtcQtySave: stores.etcStore.btnEtcQtySave
    , etcUpsertModalLedgerType: stores.etcStore.etcUpsertModalLedgerType
    , ledgerTypeGroup: stores.etcStore.ledgerTypeGroup
    , etcQtyOrderDt: stores.etcStore.etcQtyOrderDt
    , etcQtyText: stores.etcStore.etcQtyText
    , handleChange: stores.etcStore.handleChange
    , etcQtyProcessQty: stores.etcStore.etcQtyProcessQty
    , selectedEtcQtyItem: stores.etcStore.selectedEtcQtyItem
    , etcQtyItemHelperResult: stores.etcStore.etcQtyItemHelperResult
    , etcQtyItemHelperOption: stores.etcStore.etcQtyItemHelperOption
    , handleHelperOpen: stores.etcStore.handleHelperOpen
    , selectedItem: stores.etcStore.selectedItem
    , itemHelperOption: stores.etcStore.itemHelperOption
    , itemHelperResult: stores.etcStore.itemHelperResult
    , rawList: stores.etcStore.rawList
    , buId: stores.etcStore.buId
    , changeItem : stores.etcStore.changeItem

}))

class EtcInQtyInsertModal extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        const { btnEtcQtySave
            , etcUpsertModalLedgerType
            , ledgerTypeGroup
            , etcQtyOrderDt
            , etcQtyText
            , handleChange
            , etcQtyProcessQty
            , selectedEtcQtyItem
            , etcQtyItemHelperResult
            , etcQtyItemHelperOption
            , handleHelperOpen
            , itemHelperOption
            , selectedItem
            , itemHelperResult
            , rawList
            , buId
            , changeItem
        } = this.props;

        var selectboxList = []
		rawList.forEach(element => {
			if(element.desc1 == buId) {
				selectboxList.push(element)
			}
        });
        
        return (
            <React.Fragment>


                <div className='btn_wrap'>
                    <SButton buttonName={"저장"} onClick={btnEtcQtySave} type={"btnEtcQtySave"} />
                    {/* <SButton buttonName={"닫기"} onClick={etcInModalClose} type={"delete"} /> */}
                </div>

                <table style={{ width: "100%" }}>
                    <tbody>
                        <tr>
                            <th>출고유형</th>
                            <td>
                                <SSelectBox
                                    id="etcUpsertModalLedgerType"
                                    value={etcUpsertModalLedgerType}
                                    // codeGroup={"MFD002"}
                                    optionGroup={ledgerTypeGroup}
                                    onChange={handleChange}
                                />
                            </td>

                            <th>출고일자</th>
                            <td>
                                <SDatePicker
                                    id="etcQtyOrderDt"
                                    value={etcQtyOrderDt}
                                    onChange={handleChange}
                                />
                            </td>
                        </tr>
                        <tr>
                            <th>제품</th>
                            <td>
                                <ItemHelper
                                    selectedItemId={"selectedEtcQtyItem"}
                                    defaultValue={selectedEtcQtyItem}
                                    onChange={handleChange}
                                    onHelperResult={etcQtyItemHelperResult}
                                    helperOption={etcQtyItemHelperOption}
                                    onOpen={handleHelperOpen}
                                    cdName={'itemId'}
                                    cdNmName={'itemNm'}
                                />

                            </td>

                            <th>수량</th>
                            <td>
                                <SNumericInput
                                    id={"etcQtyProcessQty"}
                                    value={etcQtyProcessQty}
                                    onChange={handleChange}
                                />
                                {"KG"}
                            </td>
                        </tr>
                        {etcUpsertModalLedgerType == "LZ" ?
                            <tr>
                                <th>품변선택</th>
                                <td colSpan="3">
                                    <SSelectBox
                                            id="changeItem"
                                            value={changeItem}
                                            // codeGroup={"MFD002"}
                                            optionGroup={selectboxList}
                                            onChange={handleChange}
                                        />
                                </td>
                            </tr>
                            : null
                        }
                        <tr>
                            <th>비고</th>
                            <td colSpan="3">
                                <STextArea
                                    id="etcQtyText"
                                    value={etcQtyText}
                                    onChange={handleChange}
                                    contentWidth={485}
                                />
                            </td>
                        </tr>

                    </tbody>
                </table>

            </React.Fragment>
        );
    }
}

export default EtcInQtyInsertModal;