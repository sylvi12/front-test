import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    etcQtyInsertModal: stores.etcStore.etcQtyInsertModal
    , etcMiddleRegister: stores.etcStore.etcMiddleRegister
    , etcMiddleDelete: stores.etcStore.etcMiddleDelete
    , etcMiddleConfirm: stores.etcStore.etcMiddleConfirm
    , etcMiddleCancel: stores.etcStore.etcMiddleCancel
}))

class EtcEtcMiddleItem extends Component {

    render() {

        const { etcQtyInsertModal, etcMiddleRegister, etcMiddleDelete, etcMiddleConfirm, etcMiddleCancel } = this.props;
        return (
            <React.Fragment>
                <SButton
                    className="btn_red"
                    buttonName={'등록(수량)'}
                    type={'btnEtcQtyInsert'}
                    onClick={etcQtyInsertModal}
                />
                <SButton
                    className="btn_red"
                    buttonName={'등록(바코드)'}
                    type={'btnEtcInsert'}
                    onClick={etcMiddleRegister}
                />
                <SButton
                    className="btn_red"
                    buttonName={'삭제'}
                    type={'btnEtcDelete'}
                    onClick={etcMiddleDelete}
                />
                <SButton
                    className="btn_red"
                    buttonName={'확정'}
                    type={'btnEtcConfirm'}
                    onClick={etcMiddleConfirm}
                />
                <SButton
                    className="btn_red"
                    buttonName={'확정취소'}
                    type={'btnEtcCancel'}
                    onClick={etcMiddleCancel}
                />
            </React.Fragment>
        )
    }
}

export default EtcEtcMiddleItem;