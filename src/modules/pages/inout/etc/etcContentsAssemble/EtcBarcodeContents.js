import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setEtcBarcodeGridApi: stores.etcStore.setEtcBarcodeGridApi
    , etcBarcodeList: stores.etcStore.etcBarcodeList
    , pinnedBottomRowDataAtEtcBarcodeGrid: stores.etcStore.pinnedBottomRowDataAtEtcBarcodeGrid
}))

class EtcBarcodeContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setEtcBarcodeGridApi, etcBarcodeList, pinnedBottomRowDataAtEtcBarcodeGrid } = this.props;

        return (
            <SGrid
                grid={'etcBarcodeGrid'}
                gridApiCallBack={setEtcBarcodeGridApi}
                rowData={etcBarcodeList}
                editable={false}
                pinnedBottomRowData={pinnedBottomRowDataAtEtcBarcodeGrid}
                suppressRowClickSelection={true} 
            />
        );
    }
}
export default EtcBarcodeContents;