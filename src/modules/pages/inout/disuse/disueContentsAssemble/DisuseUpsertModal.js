import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import STextArea from 'components/atoms/input/STextArea';
import SButton from 'components/atoms/button/SButton';

import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';

import DisuseBarcodeAddMiddleItem from "modules/pages/inout/disuse/disueContentsAssemble/DisuseBarcodeAddMiddleItem";
import HoldInsertModalContents from "modules/pages/inventory/qualityStatus/qualityStatusModal/HoldInsertModalContents";

@inject(stores => ({
    disuseUpsertModalOutDisuseDt: stores.disuseStore.disuseUpsertModalOutDisuseDt
    , handleChange: stores.disuseStore.handleChange

    , disuseUpsertModalRemark: stores.disuseStore.disuseUpsertModalRemark

    , closeDisuseUpsertModal: stores.disuseStore.closeDisuseUpsertModal
    , disuseUpsertModalSave: stores.disuseStore.disuseUpsertModalSave
}))
@observer
class DisuseUpsertModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { disuseUpsertModalOutDisuseDt, handleChange
            , disuseUpsertModalRemark
            , closeDisuseUpsertModal
            , disuseUpsertModalSave
        } = this.props;

        return (
            <React.Fragment>
                <div className='btn_wrap'>
                    <SButton buttonName={"저장"} onClick={disuseUpsertModalSave} type={"btnDisuseSave"} />
                    {/* <SButton buttonName={"닫기"} onClick={closeDisuseUpsertModal} type={"default"} /> */}
                </div>
                <table style={{ width: "100%" }}>
                    <tbody>
                        <tr>
                            <th>폐기일자</th>
                            <td>
                                <SDatePicker
                                    id="disuseUpsertModalOutDisuseDt"
                                    value={disuseUpsertModalOutDisuseDt}
                                    onChange={handleChange}
                                />
                            </td>
                        </tr>
                        <tr>
                            <th>비고</th>
                            <td>
                                <STextArea
                                    id="disuseUpsertModalRemark"
                                    value={disuseUpsertModalRemark}
                                    onChange={handleChange}
                                    contentWidth={690}
                                />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <ContentsMiddleTemplate contentSubTitle={'바코드 내역'} middleItem={<DisuseBarcodeAddMiddleItem />} />
                <ContentsTemplate id='holdInsertModal' contentItem={<HoldInsertModalContents />} />

                {/* <ContentsTemplate id='disuseBarcode' contentItem={<DisuseBarcodeContents />} /> */}
                {/* HOLD관리Component (같은스토어에 있는 Grid를 불러오면 더이상 작동하지 않음)*/}


            </React.Fragment>
        );
    }
}

export default DisuseUpsertModal;