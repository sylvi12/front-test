import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setDisuseDisuseGridApi: stores.disuseStore.setDisuseDisuseGridApi
    , disuseDisuseList: stores.disuseStore.disuseDisuseList
    , getBarcode: stores.disuseStore.getBarcode
    , openModalEdit: stores.disuseStore.openModalEdit
}))

class DisuseDisuseContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setDisuseDisuseGridApi, disuseDisuseList, getBarcode, openModalEdit } = this.props;

        return (
            <SGrid
                grid={'disuseDisuseGrid'}
                gridApiCallBack={setDisuseDisuseGridApi}
                rowData={disuseDisuseList}
                onSelectionChanged={getBarcode}
                rowDoubleClick={openModalEdit}
                editable={false}
            />
        );
    }
}
export default DisuseDisuseContents;