import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    disuseQtyMiddleInsert: stores.disuseStore.disuseQtyMiddleInsert
    , disuseMiddleInsert: stores.disuseStore.disuseMiddleInsert
    , disuseMiddleDelete: stores.disuseStore.disuseMiddleDelete
    , disuseMiddleConfirm: stores.disuseStore.disuseMiddleConfirm
    , disuseMiddleCancel: stores.disuseStore.disuseMiddleCancel
}))

class DisuseDisuseMiddleItem extends Component {

    render() {

        const { disuseQtyMiddleInsert, disuseMiddleInsert, disuseMiddleDelete, disuseMiddleConfirm, disuseMiddleCancel } = this.props;
        return (
            <React.Fragment>
                <SButton
                    className="btn_red"
                    buttonName={'등록(수량)'}
                    type={'btnDisuseQtyInsert'}
                    onClick={disuseQtyMiddleInsert}
                />
                <SButton
                    className="btn_red"
                    buttonName={'등록(바코드)'}
                    type={'btnDisuseInsert'}
                    onClick={disuseMiddleInsert}
                />
                <SButton
                    className="btn_red"
                    buttonName={'삭제'}
                    type={'btnDisuseDelete'}
                    onClick={disuseMiddleDelete}
                />
                <SButton
                    className="btn_red"
                    buttonName={'확정'}
                    type={'btnDisuseConfirm'}
                    onClick={disuseMiddleConfirm}
                />
                <SButton
                    className="btn_red"
                    buttonName={'확정취소'}
                    type={'btnDisuseCancel'}
                    onClick={disuseMiddleCancel}
                />
            </React.Fragment>
        )
    }
}

export default DisuseDisuseMiddleItem;