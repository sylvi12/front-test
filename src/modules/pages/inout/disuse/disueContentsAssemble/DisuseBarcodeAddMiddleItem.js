import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    disuseBarcodeRegister: stores.disuseStore.disuseBarcodeRegister
    , disuseBarcodeDelete: stores.disuseStore.disuseBarcodeDelete
}))

class DisuseBarcodeAddMiddleItem extends Component {

    render() {

        const { disuseBarcodeRegister, disuseBarcodeDelete } = this.props;
        return (
            <React.Fragment>
                <SButton
                    className="btn_red"
                    buttonName={'추가'}
                    type={'btnBarcodeAdd'}
                    onClick={disuseBarcodeRegister}
                />
                <SButton
                    className="btn_red"
                    buttonName={'제거'}
                    type={'btnBarcodeAddDelete'}
                    onClick={disuseBarcodeDelete}
                />
            </React.Fragment>
        )
    }
}

export default DisuseBarcodeAddMiddleItem;