import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setDisuseBarcodeGridApi: stores.disuseStore.setDisuseBarcodeGridApi
    , disuseBarcodeList: stores.disuseStore.disuseBarcodeList
    , pinnedBottomRowDataAtDisuseBarcodeGrid: stores.disuseStore.pinnedBottomRowDataAtDisuseBarcodeGrid
}))

class DisuseBarcodeContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setDisuseBarcodeGridApi, disuseBarcodeList, pinnedBottomRowDataAtDisuseBarcodeGrid } = this.props;

        return (
            <SGrid
                grid={'disuseBarcodeGrid'}
                gridApiCallBack={setDisuseBarcodeGridApi}
                rowData={disuseBarcodeList}
                editable={false}
                pinnedBottomRowData={pinnedBottomRowDataAtDisuseBarcodeGrid}
            />
        );
    }
}
export default DisuseBarcodeContents;