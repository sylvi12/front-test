import React, { Component } from 'react';

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import DisuseBarcodeUpsertModalSearchItem from "modules/pages/inout/disuse/disuseBarcodeUpsertModal/DisuseBarcodeUpsertModalSearchItem";
import DisuseBarcodeUpsertModalMiddleItem from "modules/pages/inout/disuse/disuseBarcodeUpsertModal/DisuseBarcodeUpsertModalMiddleItem";
import DisuseBarcodeUpsertModalContents from "modules/pages/inout/disuse/disuseBarcodeUpsertModal/DisuseBarcodeUpsertModalContents";

class DisuseBarcodeUpsertModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <React.Fragment>
                <SearchTemplate searchItem={<DisuseBarcodeUpsertModalSearchItem />} />
                <ContentsMiddleTemplate contentSubTitle={'LOT 정보'} middleItem={<DisuseBarcodeUpsertModalMiddleItem />} />
                <ContentsTemplate id='disuseBarcodeUpsertModal' contentItem={<DisuseBarcodeUpsertModalContents />} />
            </React.Fragment>
        );
    }
}

export default DisuseBarcodeUpsertModal;