import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    closeDisuseBarcodeUpsertModal: stores.disuseStore.closeDisuseBarcodeUpsertModal
    , disuseBarcodeUpsertModalSave: stores.disuseStore.disuseBarcodeUpsertModalSave
}))

class DisuseBarcodeUpsertModalMiddleItem extends Component {

    render() {

        const { closeDisuseBarcodeUpsertModal, disuseBarcodeUpsertModalSave } = this.props;
        return (
            <React.Fragment>
                <SButton buttonName={"저장"} onClick={disuseBarcodeUpsertModalSave} type={"btnSave"} />
                {/* <SButton buttonName={"닫기"} onClick={closeDisuseBarcodeUpsertModal} type={"default"} /> */}
            </React.Fragment>
        )
    }
}

export default DisuseBarcodeUpsertModalMiddleItem;