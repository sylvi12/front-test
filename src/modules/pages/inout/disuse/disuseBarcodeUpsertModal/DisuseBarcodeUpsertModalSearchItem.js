import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SDatePicker from 'components/override/datepicker/SDatePicker';
import SInput from 'components/atoms/input/SInput'
import SButton from 'components/atoms/button/SButton'

@inject(stores => ({
    disuseBarcodeUpsertModalBarcode: stores.disuseStore.disuseBarcodeUpsertModalBarcode
    , handleChange: stores.disuseStore.handleChange
    , disuseBarcodeUpsertModalProdDt: stores.disuseStore.disuseBarcodeUpsertModalProdDt
    , disuseBarcodeUpsertModalItemNmOrItemCd: stores.disuseStore.disuseBarcodeUpsertModalItemNmOrItemCd
    , disuseBarcodeUpsertModalSearch: stores.disuseStore.disuseBarcodeUpsertModalSearch
}))
class DisuseBarcodeUpsertModalSearchItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { disuseBarcodeUpsertModalBarcode, handleChange
            , disuseBarcodeUpsertModalProdDt
            , disuseBarcodeUpsertModalItemNmOrItemCd
            , disuseBarcodeUpsertModalSearch
        } = this.props;

        return (
            <React.Fragment>

                <SInput
                    title={"바코드"}
                    id={"disuseBarcodeUpsertModalBarcode"}
                    value={disuseBarcodeUpsertModalBarcode}
                    onChange={handleChange}
                    onEnterKeyDown={disuseBarcodeUpsertModalSearch}
                />

                <SDatePicker
                    title={"생산일자"}
                    id="disuseBarcodeUpsertModalProdDt"
                    value={disuseBarcodeUpsertModalProdDt}
                    onChange={handleChange}
                />

                <SInput
                    title={"제품명/코드"}
                    id={"disuseBarcodeUpsertModalItemNmOrItemCd"}
                    value={disuseBarcodeUpsertModalItemNmOrItemCd}
                    onChange={handleChange}
                    onEnterKeyDown={disuseBarcodeUpsertModalSearch}
                />

                <div className='search_item_btn' style={{ right: "" }}>
                    <SButton buttonName={'조회'} onClick={disuseBarcodeUpsertModalSearch} type={'btnSearch'} />
                </div>

            </React.Fragment>
        )
    }
}

export default DisuseBarcodeUpsertModalSearchItem;