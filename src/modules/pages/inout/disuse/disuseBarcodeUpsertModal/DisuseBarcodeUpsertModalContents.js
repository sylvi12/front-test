import React, { Component } from "react";
import { inject } from "mobx-react";

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setDisuseBarcodeUpsertModalGridApi: stores.disuseStore.setDisuseBarcodeUpsertModalGridApi
    , disuseBarcodeUpsertModalList: stores.disuseStore.disuseBarcodeUpsertModalList
    , disuseBarcodeUpsertModalSave: stores.disuseStore.disuseBarcodeUpsertModalSave
}))

class DisuseBarcodeUpsertModalContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setDisuseBarcodeUpsertModalGridApi, disuseBarcodeUpsertModalList, disuseBarcodeUpsertModalSave } = this.props;

        return (
            <SGrid
                grid={'disuseBarcodeUpsertModalGrid'}
                gridApiCallBack={setDisuseBarcodeUpsertModalGridApi}
                rowData={disuseBarcodeUpsertModalList}
                rowDoubleClick={disuseBarcodeUpsertModalSave}
                editable={false}
            />
        );
    }
}

export default DisuseBarcodeUpsertModalContents;