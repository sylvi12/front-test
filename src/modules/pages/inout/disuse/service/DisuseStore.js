import { observable, action } from 'mobx';
import moment from 'moment';
import 'moment/locale/ko';

import Alert from 'components/override/alert/Alert';
import GridUtil from 'components/override/grid/utils/GridUtil';
import Validator from 'components/override/utils/Validator';
import ItemOptionModel from 'components/codehelper/model/ItemOptionModel';

import DisuseRepository from 'modules/pages/inout/disuse/service/DisuseRepository';
import OutputRepository from 'modules/pages/order/output/service/OutputRepository';
import InoutRepository from 'modules/pages/inout/inout/service/InoutRepository';
import EtcRepository from 'modules/pages/inout/etc/service/EtcRepository';
import CommonStore from 'modules/pages/common/service/CommonStore';

import QualityStatusStore from 'modules/pages/inventory/qualityStatus/service/QualityStatusStore';
import BaseStore from 'utils/base/BaseStore';

class DisuseStore extends BaseStore {

    // DisuseSearchItem 변수
    @observable alphaCheck = false;
    @observable buId = "";
    @observable buGroup = [];
    @observable startDt = moment().add(-7, 'days').format('YYYYMMDD');
    @observable endDt = moment().format('YYYYMMDD');
    @observable itemNmOrItemCd = '';

    // DisuseDisuseContents 변수
    disuseDisuseGridApi = undefined;
    @observable disuseDisuseList = [];

    // DisuseBarcodeContents 변수
    disuseBarcodeGridApi = undefined;
    @observable disuseBarcodeList = [];
    @observable pinnedBottomRowDataAtDisuseBarcodeGrid = [];
    barcodeOrderNo = 0;

    // DisuseUpsertModal 변수
    @observable disuseUpsertModalIsOpen = false;
    disuseUpsertModalOrderNo = 0;
    @observable disuseUpsertModalOutDisuseDt = "";
    @observable disuseUpsertModalRemark = "";

    /* DisuseBarcodeUpsertModal 변수 */
    @observable disuseBarcodeUpsertModalIsOpen = false;
    // DisuseBarcodeUpsertModal SearchItem 변수
    disuseBarcodeUpsertModalOrderNo = 0;
    @observable disuseBarcodeUpsertModalBarcode = "";
    @observable disuseBarcodeUpsertModalProdDt = "";
    @observable disuseBarcodeUpsertModalItemNmOrItemCd = "";
    // DisuseBarcodeUpsertModal Contents 변수
    disuseBarcodeUpsertModalGridApi = undefined;
    @observable disuseBarcodeUpsertModalList = [];

    /* BarcodeHelpler variables */
    @observable isBarcodeHelperOpen = false;
    @observable barcodeHelplerOrderNo = 0;
    @observable barcodeHelplerledgerType = '';

    //수정 모달 변수
    @observable udateModalOpen = false;
    @observable disuseDt_U = '';
    @observable remark_U = '';
    orderNo_U = '';

    ledgerTypeList = [
        { cd: "20", cdNm: "홀드" },
        { cd: "30", cdNm: "불용" },
    ];
    @observable qtyLedgerType = '20'


    //바코드내역 추가 키 변수
    key = '';

    //등록에서 바코드 추가 변수
    add = [];

    constructor() {
        super();
        this.disuseQtyItemHelperOption.barcodeYn = 'N';
        this.disuseQtyItemHelperOption.cdWidth = 70;
        this.disuseQtyItemHelperOption.cdNmWidth = 120;
        this.disuseQtyItemHelperOption.cdNmIsFocus = true;
        this.disuseQtyItemHelperOption_u.cdWidth = 70;
        this.disuseQtyItemHelperOption_u.cdNmWidth = 120;
        this.disuseQtyItemHelperOption_u.cdNmReadOnly = true;
        this.disuseQtyItemHelperOption_u.searchVisible = false;
        this.setInitialState();
    }


    disuseQtyInsertValidator = new Validator([
        {
            field: 'itemCd_d',
            method: 'isEmpty',
            message: '제품',
        }
    ])


    @action.bound
    getBu = async () => {
        this.buId = CommonStore.buId;
        this.alphaCheck = true;
    }

    @action.bound
    handleChange = (data) => {

        if (data.id == 'selectedDisuseQtyItem') {
            if (data.value.itemNm == '') {
                this.itemCd_d = '';
            }
        }
        this[data.id] = data.value;

        console.log(data)
    }

    // DisuseSearchItem 조회 클릭 시
    @action.bound
    handleSearchClick = async () => {
        const searchData = {
            "buId": this.buId
            , "startDt": this.startDt
            , "endDt": this.endDt
            , "itemNmOrItemCd": this.itemNmOrItemCd
        }
        const { data, status } = await DisuseRepository.getDisuses(searchData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            this.disuseDisuseList = data.data;
            this.disuseDisuseGridApi.setRowData(this.disuseDisuseList);

            //조회시 바코드 내역은 초기화
            this.disuseBarcodeGridApi.setRowData([]);
        }
    }
    // DisuseDisuseMiddleItem 등록 클릭 시
    @action.bound
    disuseMiddleInsert = async () => {

        let allBu = this.buId.split(',');

        if (allBu.length > 1) {
            Alert.meg("선택된 공장이 없습니다.");
            return;
        }

        this.disuseUpsertModalOrderNo = 0;
        this.disuseUpsertModalOutDisuseDt = moment().format('YYYYMMDD');
        this.disuseUpsertModalRemark = "";
        this.disuseUpsertModalIsOpen = true;

        //바코드내역 - 추가버튼 다르게 사용하기 위해 넣음
        this.key = 'Y';
    }
    // MiddleItem 삭제 클릭 시
    @action.bound
    disuseMiddleDelete = async () => {
        const selectedRows = this.disuseDisuseGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg("선택된 항목이 없습니다.");
            return;
        }
        for (let i = 0; i < selectedRows.length; i++) {
            if (selectedRows[i].outDisuseStatus == '20' || selectedRows[i].outDisuseStatus == '60') {
                Alert.meg("이미 확정된 폐기출고 입니다.");
                return;
            }
        }
        const searchData = {
            "buId": this.buId
            , "startDt": this.startDt
            , "endDt": this.endDt
            , "itemNmOrItemCd": this.itemNmOrItemCd
        }
        const value = await Alert.confirm("삭제하시겠습니까?").then(result => { return result; });
        const params = JSON.stringify({ param1: selectedRows, param2: searchData });
        if (value) {
            const { data, status } = await DisuseRepository.delete(params);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                this.disuseDisuseList = data.data;
                this.disuseDisuseGridApi.setRowData(this.disuseDisuseList);
                this.disuseBarcodeGridApi.setRowData([]);
                Alert.meg("완료되었습니다.");
            }
        }
        else {
            return;
        }
    }
    // DisuseDisuseMiddleItem 확정 클릭 시
    @action.bound
    disuseMiddleConfirm = async () => {
        const selectedRow = this.disuseDisuseGridApi.getSelectedRows()[0];
        if (!selectedRow || selectedRow.length == 0) {
            Alert.meg("선택된 항목이 없습니다.");
            return;
        }
        if (selectedRow.outDisuseStatus == '20') {
            Alert.meg("이미 확정된 폐기출고 입니다.");
            return;
        }
        if (this.disuseBarcodeGridApi.getDisplayedRowCount() <= 0) {
            Alert.meg("바코드가 없습니다.");
            return;
        }
        const confirmData = {
            "orderNo": selectedRow.orderNo
        }
        const { data, status } = await EtcRepository.confirm(confirmData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            Alert.meg("확정되었습니다.");
            this.handleSearchClick();
            this.getBarcode();
        }
    }
    // DisuseDisuseMiddleItem 확정취소 클릭 시
    @action.bound
    disuseMiddleCancel = async () => {
        const selectedRow = this.disuseDisuseGridApi.getSelectedRows()[0];
        if (!selectedRow || selectedRow.length == 0) {
            Alert.meg("선택된 항목이 없습니다.");
            return;
        }
        if (selectedRow.outDisuseStatus != '20') {
            Alert.meg("확정된 폐기출고가  아닙니다.");
            return;
        }
        const deleteData = {
            "orderNo": selectedRow.orderNo
        }
        const value = await Alert.confirm("취소하시겠습니까?").then(result => { return result; });
        if (value) {
            const { data, status } = await EtcRepository.cancel(deleteData);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                this.handleSearchClick();
                this.getBarcode();
            }
        }
        else {
            return;
        }
    }

    //폐기출고 ROW 더블 클릭
    @action.bound
    openModalEdit = async () => {
        let confBarcodeYn = 'Y';
        const selectedRow = GridUtil.getSelectedRowData(this.disuseDisuseGridApi);
        // func searchBarcode의 바코드 내역으로 바코드 관리대상 여부(Y or N) 확인
        for (let i = 0; i < this.disuseBarcodeList.length; i++) {

            if (this.disuseBarcodeList[i].barcodeYn == 'N') {
                confBarcodeYn = 'N';
            }
        }

        if (confBarcodeYn == 'N' && selectedRow.outDisuseStatus != '20') {
            const submitData = {
                "orderNo": selectedRow.orderNo
            }

            const { data, status } = await DisuseRepository.getDisuseQtyInfo(submitData);
            if (data.success && status == 200) {
                const successData = data.data;

                this.disuseQtyDt_u = moment(successData.outDisuseDt).format('YYYYMMDD');
                this.disuseQtyUpdateModalOrderNo = successData.orderNo;
                this.disuseQtyBarcode_u = successData.barcode;
                this.selectedDisuseQtyItem_u = { "itemCd": successData.itemCd, "itemNm": successData.itemNm };
                this.disuseQtyProcessQty_u = successData.processQty;
                this.disuseQtyProcessUom_u = successData.processUom;
                this.disuseQtyText_u = successData.remark;
                this.disuseQtyUpdateModalIsOpen = true;
            }
        } else {
            if (selectedRow.outDisuseStatus != '20') {
                this.orderNo_U = selectedRow.orderNo;
                this.disuseDt_U = moment(selectedRow.outDisuseDt).format('YYYYMMDD');
                this.remark_U = selectedRow.remark;
                this.udateModalOpen = true;
            }
        }
    }

    //폐기출고 수정 모달 닫기
    @action.bound
    closeUpdateModal = () => {
        this.udateModalOpen = false;
    }

    //폐기 수정 저장
    @action.bound
    disuseSave_U = async () => {

        this.startDt = this.startDt <= this.disuseDt_U ? this.startDt : this.disuseDt_U;
        this.endDt = this.disuseDt_U;

        const saveData = {
            "orderNo": this.orderNo_U
            , "outDisuseDt": this.disuseDt_U
            , "remark": this.remark_U
            , "buId": this.buId
            , "startDt": this.startDt
            , "endDt": this.endDt
            , "itemNmOrItemCd": ''
        }

        const params = JSON.stringify({ param: saveData });

        const value = await Alert.confirm("저장하시겠습니까?").then(result => { return result; });
        if (value) {
            const { data, status } = await DisuseRepository.updateDisuse(params);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                this.disuseDisuseList = data.data;
                this.disuseDisuseGridApi.setRowData(this.disuseDisuseList);

                this.getBarcode();
            }
            this.closeUpdateModal();
        } else {
            return;
        }
    }

    // DisuseDisuseContents
    @action.bound
    setDisuseDisuseGridApi = async (gridApi) => {
        this.disuseDisuseGridApi = gridApi;
        const columnDefs = [
            {
                headerName: "지시번호"
                , field: "orderNo"
                , width: 70
                , customCellClass: 'cell_align_center'
            },
            {
                headerName: "폐기일자"
                , field: "outDisuseDt"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "폐기상태"
                , field: "outDisuseStatusNm"
                , width: 90
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "등록자"
                , field: "inputEmpNm"
                , width: 75
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "등록일"
                , field: "inputDtm"
                , width: 105
                , cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm')
            },
            {
                headerName: "확정자"
                , field: "confirmUserNm"
                , width: 75
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "확정일"
                , field: "confirmDtm"
                , width: 105
                , cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm')
            },
            {
                headerName: "비고"
                , field: "remark"
                , width: 200
            },
        ];
        this.disuseDisuseGridApi.setColumnDefs(columnDefs);
    }

    // DisuseDisuseContents 그리드 로우 클릭 시
    @action.bound
    getBarcode = async () => {
        const selectedRow = this.disuseDisuseGridApi.getSelectedRows()[0];
        if (!selectedRow || selectedRow.length == 0) {
            return;
        }
        this.barcodeOrderNo = selectedRow.orderNo;
        this.disuseBarcodeUpsertModalOrderNo = selectedRow.orderNo;
        this.searchBarcode();
    }

    // DisuseBarcodeMiddleItem 추가 클릭 시
    @action.bound
    disuseBarcodeRegister = async () => {

        if (this.key != 'Y') {
            const selectedRow = this.disuseDisuseGridApi.getSelectedRows()[0];
            if (!selectedRow || selectedRow.length == 0) {
                Alert.meg("폐기출고를 선택해 주세요.");
                return;
            }
            if (selectedRow.outDisuseStatus == '20' || selectedRow.outDisuseStatus == '60') {
                Alert.meg("이미 확정된 폐기출고 입니다.");
                return;
            }


            this.disuseBarcodeUpsertModalBarcode = "";
            this.disuseBarcodeUpsertModalProdDt = moment().format('YYYYMMDD');
            this.disuseBarcodeUpsertModalItemNmOrItemCd = "";
            this.disuseBarcodeUpsertModalList = [];
            this.disuseBarcodeUpsertModalIsOpen = true;


            //공통 바코드 팝업 
            // this.barcodeHelplerOrderNo = selectedRow.orderNo;
            // this.barcodeHelplerledgerType = selectedRow.ledgerType;
            // this.isBarcodeHelperOpen = true;

        } else {
            this.disuseBarcodeUpsertModalBarcode = "";
            this.disuseBarcodeUpsertModalProdDt = moment().format('YYYYMMDD');
            this.disuseBarcodeUpsertModalItemNmOrItemCd = "";
            this.disuseBarcodeUpsertModalList = [];
            this.disuseBarcodeUpsertModalIsOpen = true;
        }

    }
    // barcodeHelper 반환
    @action.bound
    barocdeHelperResult = async (result) => {
        if (result) {
            this.key = '';
            this.searchBarcode();
        }
    }
    // DisuseBarcodeMiddleItem 삭제 클릭 시
    @action.bound
    disuseBarcodeDelete = async () => {

        if (this.key == 'Y') { //등록에서 제거 클릭시
            const selectedRows = QualityStatusStore.holdBarcodeGridApi.getSelectedRows();

            selectedRows.forEach(selectedRow => {
                QualityStatusStore.holdBarcodeGridApi.updateRowData({ remove: [selectedRow] });
                const index = this.add.findIndex(x => x.barcode == selectedRow.barcode);
                this.add.splice(index, 1);
            })

        } else {
            const selectedRow = this.disuseDisuseGridApi.getSelectedRows()[0];
            if (selectedRow) {
                if (selectedRow.outDisuseStatus == '20') {
                    Alert.meg("이미 확정된 폐기출고 입니다.");
                    return;
                }
            } else {
                Alert.meg("폐기출고를 선택해 주세요.");
                return;
            }
            const selectedRows = this.disuseBarcodeGridApi.getSelectedRows();
            if (!selectedRows || selectedRows.length == 0) {
                Alert.meg("삭제할 바코드를 선택해 주세요.");
                return;
            }
            const searchData = {
                "orderNo": this.barcodeOrderNo
            }
            const params = JSON.stringify({ param1: selectedRows, param2: searchData });
            const value = await Alert.confirm("삭제하시겠습니까?").then(result => { return result; });
            if (value) {
                const { data, status } = await InoutRepository.deleteBarcode(params);
                if (data.success == false) {
                    Alert.meg(data.msg);
                } else {
                    this.disuseBarcodeList = data.data;
                    this.disuseBarcodeGridApi.setRowData(this.disuseBarcodeList);
                }
            }
            else {
                return;
            }
        }
    }

    // DisuseBarcodeContents
    @action.bound
    setDisuseBarcodeGridApi = async (gridApi) => {
        this.disuseBarcodeGridApi = gridApi;
        this.pinnedBottomRowDataAtDisuseBarcodeGrid = [
            {
                seq: '합계',
                processQty: 0
            }
        ];
        const columnDefs = [
            {
                headerName: "",
                children: [
                    {
                        headerName: ""
                        , field: "check"
                        , width: 29
                        , checkboxSelection: true
                        , headerCheckboxSelection: true
                    },
                    {
                        headerName: "순번"
                        , field: "seq"
                        , width: 70
                        , type: "numericColumn"
                        , valueFormatter: GridUtil.numberFormatter
                        , valueParser: GridUtil.numberParser
                    },
                    {
                        headerName: "바코드"
                        , field: "barcode"
                        , width: 100
                        , cellClass: 'cell_align_center'
                    },
                    {
                        headerName: "상태"
                        , field: "lotQualityNm"
                        , width: 50
                        , cellClass: 'cell_align_center'
                    },
                    {
                        headerName: "제품코드"
                        , field: "itemCd"
                        , width: 90
                        , cellClass: 'cell_align_center'
                    },
                    {
                        headerName: "LOT"
                        , field: "lotNo"
                        , width: 80
                        , cellClass: 'cell_align_center'
                    },
                    {
                        headerName: "생산일자"
                        , field: "prodDt"
                        , width: 90
                        , cellClass: 'cell_align_center'
                        , valueFormatter: GridUtil.dateFormatter
                    },
                    {
                        headerName: "유통기한"
                        , field: "expiryDt"
                        , width: 90
                        , cellClass: 'cell_align_center'
                        , valueFormatter: GridUtil.dateFormatter
                    }
                ]
            },
            {
                headerName: "수량",
                children: [
                    {
                        headerName: "재고량"
                        , field: "processQty"
                        , width: 100
                        , type: "numericColumn"
                        , valueFormatter: (params) => GridUtil.numberFormatter(params, 2)
                        , valueParser: GridUtil.numberParser
                        , pinnedRowCellRenderer: "customPinnedRowRenderer"
                        , pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
                    },
                    {
                        headerName: "단위"
                        , field: "processUom"
                        , width: 65
                        , cellClass: 'cell_align_center'
                    }
                ]
            },
            // {
            //     headerName: "",
            //     children: [
            //         {
            //             headerName: "위치"
            //             , field: "locNm"
            //             , width: 120
            //             , cellClass: 'cell_align_center'
            //         },
            //         {
            //             headerName: "처리시간"
            //             , field: "inputDtm"
            //             , width: 105
            //             , cellClass: 'cell_align_center'
            //             , valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm')
            //         },
            //         {
            //             headerName: "처리자"
            //             , field: "inputEmpNm"
            //             , width: 90
            //             , cellClass: 'cell_align_center'
            //         }
            //     ]
            // }
        ];
        this.disuseBarcodeGridApi.setColumnDefs(columnDefs);
    }

    // DisuseBarcodeContents 조회
    @action.bound
    searchBarcode = async () => {
        const searchData = {
            "orderNo": this.barcodeOrderNo
        }
        const { data, status } = await InoutRepository.getPallet(searchData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            this.disuseBarcodeList = data.data;
            this.disuseBarcodeGridApi.setRowData(this.disuseBarcodeList);
            for (let i = 0; i < this.disuseBarcodeList.length; i++) {
                if (this.disuseBarcodeList[i].barcodeYn == 'N') {
                    this.disuseQtyBarcodeRegisterAvailableToggle = false;
                    this.disuseQtyBarcodeDeleteAvailableToggle = false;
                    return;
                }
            }
            this.disuseQtyBarcodeRegisterAvailableToggle = true;
            this.disuseQtyBarcodeDeleteAvailableToggle = true;
        }
    }

    // DisuseUpsertModal 닫기
    @action.bound
    closeDisuseUpsertModal = async () => {
        this.disuseUpsertModalIsOpen = false;
        this.add = [];
        this.key = '';
    }

    // DisuseUpsertModal 저장 클릭 시
    @action.bound
    disuseUpsertModalSave = async () => {

        this.startDt = this.startDt <= this.disuseUpsertModalOutDisuseDt ? this.startDt : this.disuseUpsertModalOutDisuseDt;
        this.endDt = this.disuseUpsertModalOutDisuseDt;

        const saveData = {
            "orderNo": ''
            , "outDisuseDt": this.disuseUpsertModalOutDisuseDt
            , "remark": this.disuseUpsertModalRemark

            , "startDt": this.startDt
            , "endDt": this.endDt
            , "itemNmOrItemCd": ''

            , "buId": this.buId
            , "itemId": ''
            , "processQty": 0
            , "processUom": ''
        }

        const value = await Alert.confirm("저장하시겠습니까?").then(result => { return result; });
        if (value) {
            QualityStatusStore.holdBarcodeGridApi.selectAll();
            const selectedRows = QualityStatusStore.holdBarcodeGridApi.getSelectedRows();
            const params = JSON.stringify({ param1: selectedRows, param2: saveData });
            const { data, status } = await DisuseRepository.saveDisuse(params);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                //key값 초기화
                this.key = '';

                //add값 초기화
                this.add = [];

                this.itemNmOrItemCd = this.itemNmOrItemCd

                this.disuseDisuseList = data.data;
                this.disuseDisuseGridApi.setRowData(this.disuseDisuseList);
                //this.disuseBarcodeGridApi.setRowData([]);

                GridUtil.setFocusByRowIndex(this.disuseDisuseGridApi, this.disuseDisuseList.length - 1);
            }
            this.closeDisuseUpsertModal();
        } else {
            QualityStatusStore.holdBarcodeGridApi.deselectAll();
            return;
        }

    }

    // DisuseBarcodeUpsertModal 조회 클릭 시
    @action.bound
    disuseBarcodeUpsertModalSearch = async () => {
        const searchData = {
            "buId": this.buId
            , "barcode": this.disuseBarcodeUpsertModalBarcode
            , "prodDt": this.disuseBarcodeUpsertModalProdDt
            , "itemNmOrItemCd": this.disuseBarcodeUpsertModalItemNmOrItemCd
        }
        const { data, status } = await EtcRepository.getBarcode(searchData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            this.disuseBarcodeUpsertModalList = data.data;

            let holdList = [];
            this.disuseBarcodeUpsertModalList.forEach(item => {

                if (item.lotQuality == "20" || item.lotQuality == "30") {
                    holdList.push(item);
                }
            })

            this.disuseBarcodeUpsertModalGridApi.setRowData(holdList);
            // this.disuseBarcodeUpsertModalGridApi.setRowData(this.disuseBarcodeUpsertModalList);
        }
    }

    // DisuseBarcodeUpsertModal 닫기
    @action.bound
    closeDisuseBarcodeUpsertModal = async () => {
        this.disuseBarcodeUpsertModalIsOpen = false;
    }

    // DisuseBarcodeUpsertModal 저장 클릭 시
    @action.bound
    disuseBarcodeUpsertModalSave = async () => {

        if (this.key == 'Y') {
            //폐기출고에서 바코드를 추가 저장 할 때
            const selectedRows = this.disuseBarcodeUpsertModalGridApi.getSelectedRows();
            if (!selectedRows || selectedRows.length == 0) {
                Alert.meg("바코드를 선택해 주세요.");
                return;
            }

            selectedRows.forEach(item => {
                if (!this.add.some(x => x.barcode == item.barcode)) {
                    this.add.push(item);
                }
            })

            QualityStatusStore.holdBarcodeGridApi.setRowData(this.add);
            this.disuseBarcodeUpsertModalIsOpen = false;

        } else {
            //지시등록이 아닌 곳에서 바코드 추가 저장 할 때
            const selectedRows = this.disuseBarcodeUpsertModalGridApi.getSelectedRows();
            if (!selectedRows || selectedRows.length == 0) {
                Alert.meg("바코드를 선택해 주세요.");
                return;
            }
            const searchData = {
                "orderNo": this.disuseBarcodeUpsertModalOrderNo
            }

            const params = JSON.stringify({ param1: selectedRows, param2: searchData });
            const { data, status } = await EtcRepository.saveBarcode(params);
            if (data.success == false) {
                Alert.meg(data.msg);
                return;
            } else {
                this.disuseBarcodeList = data.data;
                this.disuseBarcodeGridApi.setRowData(this.disuseBarcodeList);
                this.disuseBarcodeUpsertModalIsOpen = false;
            }
        }
    }

    // DisuseBarcodeUpsertModal 그리드
    @action.bound
    setDisuseBarcodeUpsertModalGridApi = async (gridApi) => {
        this.disuseBarcodeUpsertModalGridApi = gridApi;
        const columnDefs = [
            {
                headerName: ""
                , field: "check"
                , width: 29
                , checkboxSelection: true
                , headerCheckboxSelection: true
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "순번"
                , field: "seq"
                , width: 40
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
            },
            {
                headerName: "바코드"
                , field: "barcode"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "상태"
                , field: "lotQualityNm"
                , width: 50
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "제품코드"
                , field: "itemCd"
                , width: 90
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "LOT"
                , field: "lotNo"
                , width: 80
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "생산일자"
                , field: "prodDt"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "유통기한"
                , field: "expiryDt"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "재고량"
                , field: "processQty"
                , width: 60
                , type: "numericColumn"
                , valueFormatter: (params) => GridUtil.numberFormatter(params, 2)
                , valueParser: GridUtil.numberParser
            },
            {
                headerName: "위치"
                , field: "locNm"
                , width: 120
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "처리시간"
                , field: "inputDtm"
                , width: 110
                , cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm')
            },
            {
                headerName: "처리자"
                , field: "inputEmpNm"
                , width: 90
                , cellClass: 'cell_align_center'
            }
        ];
        this.disuseBarcodeUpsertModalGridApi.setColumnDefs(columnDefs);
    }
































































































































































































































































    // 폐기출고 등록(수량) 관련 변수
    @observable disuseQtyInsertModalIsOpen = false;
    @observable disuseQtyInsertModalOrderNo = 0;
    @observable disuseQtyDt = moment().format('YYYYMMDD');
    @observable selectedDisuseQtyItem = {};
    @observable disuseQtyItemHelperOption = new ItemOptionModel().optionModel;
    @observable disuseQtyProcessQty = 0;
    @observable disuseQtyText = '';
    @observable itemId_d = 0;
    @observable itemCd_d = '';
    // 단위 변수
    itemUom = "KG"

    // 폐기출고 수정(수량) 관련 변수
    @observable disuseQtyUpdateModalIsOpen = false;
    @observable disuseQtyUpdateModalOrderNo = 0;
    @observable disuseQtyDt_u = moment().format('YYYYMMDD');
    @observable disuseQtyItemHelperOption_u = new ItemOptionModel().optionModel;
    @observable selectedDisuseQtyItem_u = {};
    @observable disuseQtyBarcode_u = '';
    @observable disuseQtyProcessQty_u = 0;
    @observable disuseQtyProcessUom_u = '';
    @observable disuseQtyText_u = '';

    // 폐기출고 DoubleClick시 Btn visible Event
    @observable disuseQtyBarcodeRegisterAvailableToggle = true;
    @observable disuseQtyBarcodeDeleteAvailableToggle = true;

    // 등록(수량) 모달Btn
    @action.bound
    disuseQtyMiddleInsert = async () => {

        let allBu = this.buId.split(',');

        if (allBu.length > 1) {
            Alert.meg("선택된 공장이 없습니다.");
            return;
        }

        const all = CommonStore.codeObject["MFD002"]
        this.key = 'Y';
        this.disuseQtyInsertModalIsOpen = true;
    }

    // 폐기출고 등록 Modal 닫기
    @action.bound
    closeDisuseQtyInsertModal = () => {
        this.disuseQtyDt = moment().format('YYYYMMDD');
        this.selectedDisuseQtyItem = {};
        this.disuseQtyProcessQty = 0;
        this.disuseQtyText = '';
        this.disuseQtyInsertModalIsOpen = false;
    }

    // ItemHelper 설정
    @action.bound
    disuseQtyItemHelperResult = (result) => {
        if (result) {
            this.itemId_d = this.selectedDisuseQtyItem.itemId;
            this.itemCd_d = this.selectedDisuseQtyItem.itemCd;
            this.itemNm_d = this.selectedDisuseQtyItem.itemNm;
        }
    }

    @action.bound
    handleHelperOpen = () => {
        this.disuseQtyItemHelperOption.buId = this.buId;
    }

    // 폐기출고 등록(수량) '저장' Btn
    @action.bound
    disuseQtySave = async () => {

        const validation = this.disuseQtyInsertValidator.validate(this);
        if (!validation.isValid) {
            return;
        }


        if (this.disuseQtyProcessQty == 0 || this.itemId_d == null) {
            Alert.meg('폐기할 제품/수량을 확인 해주세요.');
        }

        this.startDt = this.startDt <= this.disuseQtyDt ? this.startDt : this.disuseQtyDt;
        this.endDt = this.disuseQtyDt;

        const submitData = {
            "orderNo": this.disuseQtyInsertModalOrderNo
            , "outDisuseDt": this.disuseQtyDt
            , "processQty": this.disuseQtyProcessQty
            , "remark": this.disuseQtyText
            , "itemId": this.itemId_d
            , "itemCd": this.itemCd_d
            , "processUom": this.itemUom
            , "buId": this.buId
            , "startDt": this.startDt
            , "endDt": this.endDt
            , "itemNmOrItemCd": ''
            , "qtyLedgerType" : this.qtyLedgerType
        }

        const value = await Alert.confirm("저장하시겠습니까?").then(result => { return result; });
        if (value) {
            const { data, status } = await DisuseRepository.saveDisuseQty(submitData);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                
                console.log(data.data, '?ㅋㅋㅋ');
                //key값 초기화
                this.key = '';

                this.disuseDisuseList = data.data;
                this.disuseDisuseGridApi.setRowData(this.disuseDisuseList);

                GridUtil.setFocusByRowIndex(this.disuseDisuseGridApi, this.disuseDisuseList.length - 1);
            }
            this.closeDisuseQtyInsertModal();
        } else {
            return;
        }
    }

    // 폐기출고 수정(수량) 변경 Btn
    @action.bound
    btnDisuseQtyUpdateModalSave = async () => {
        const submitData = {
            "orderNo": this.disuseQtyUpdateModalOrderNo
            , "barcode": this.disuseQtyBarcode_u
            , "remark": this.disuseQtyText_u
            , "processQty": this.disuseQtyProcessQty_u

            , "buId": this.buId
            , "startDt": this.startDt
            , "endDt": this.endDt
            , "itemNmOrItemCd": this.itemNmOrItemCd
        }

        const value = await Alert.confirm("변경하시겠습니까?").then(result => { return result; });
        if (value) {
            const { data, status } = await DisuseRepository.disuseQtyUpdateModalSave(submitData);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                //key값 초기화
                this.key = '';

                this.disuseDisuseList = data.data;
                this.disuseDisuseGridApi.setRowData(this.disuseDisuseList);

                // this.getBarcode();
                this.searchBarcode();
            }
            this.closeDisuseQtyUpdateModal();
        } else {
            QualityStatusStore.holdBarcodeGridApi.deselectAll();
            return;
        }
    }

    //폐기출고 수량Base Modal 닫기
    @action.bound
    closeDisuseQtyUpdateModal = () => {
        this.disuseQtyUpdateModalOrderNo = 0;
        this.disuseQtyDt_u = moment().format('YYYYMMDD');
        this.selectedDisuseQtyItem_u = {};
        this.disuseQtyBarcode_u = '';
        this.disuseQtyProcessQty_u = 0;
        this.disuseQtyProcessUom_u = '';
        this.disuseQtyText_u = '';
        this.disuseQtyUpdateModalIsOpen = false;
    }
}
export default new DisuseStore();