import API from "components/override/api/API";

class DisuseRepository {

    URL = "/api/inout/disuse";
    /* 조회 */
    // 배차 그리드 조회
    getDisuses(param) {
        return API.request.get(encodeURI(`${this.URL}/getdisuses?buId=${param.buId}&startDt=${param.startDt}&endDt=${param.endDt}&itemNmOrItemCd=${param.itemNmOrItemCd}`))
    }

    /* 등록 */
    // 폐기등록
    saveDisuse(params) {
        return API.request.post(encodeURI(`${this.URL}/savedisuse`), params)
    }

    saveDisuseQty(param) {
        return API.request.post(encodeURI(`${this.URL}/savedisuseqty`), param)
    }

    // 폐기수정(수량Base) 관련 Info
    getDisuseQtyInfo(param) {
        return API.request.get(encodeURI(`${this.URL}/getdisuseqtyinfo?orderNo=${param.orderNo}`))
    }

    disuseQtyUpdateModalSave(param) {
        return API.request.post(encodeURI(`${this.URL}/updatedisuseqty`), param)
    }

    //수정
    updateDisuse(params) {
        return API.request.post(encodeURI(`${this.URL}/updatedisuse`), params)
    }

    /* 삭제 */
    delete(params) {
        return API.request.post(encodeURI(`${this.URL}/deletedisuse`), params)
    }
}
export default new DisuseRepository();