import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SDatePicker from 'components/override/datepicker/SDatePicker';

import STextArea from 'components/atoms/input/STextArea';
import SButton from 'components/atoms/button/SButton';



@inject(stores => ({
     
      disuseDt_U: stores.disuseStore.disuseDt_U
    , remark_U: stores.disuseStore.remark_U
    , handleChange: stores.disuseStore.handleChange
    , disuseSave_U: stores.disuseStore.disuseSave_U
    
}))

class DisuseUpdateModal extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        const {   disuseDt_U
                , remark_U
                , handleChange
                , disuseSave_U } = this.props;
        return(
            <React.Fragment>
                
                
                    <div className='btn_wrap'>
                        <SButton buttonName={"저장"} onClick={disuseSave_U} type={"disuseSave_U"} />
                        {/* <SButton buttonName={"닫기"} onClick={etcInModalClose} type={"delete"} /> */}
                    </div>
                    
                    <table style={{ width: "100%" }}>
                        <tbody>
                            <tr>
                                <th>출고일자</th>
                                <td colSpan="3">
                                    <SDatePicker
                                        id="disuseDt_U"
                                        value={disuseDt_U}
                                        onChange={handleChange}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <th>비고</th>
                                <td colSpan="3">
                                    <STextArea
                                        id="remark_U"
                                        value={remark_U}
                                        onChange={handleChange}
                                        contentWidth={390}
                                    />
                                </td>
                            </tr>

                        </tbody>
                    </table>
            </React.Fragment>
        );
    }
}

export default DisuseUpdateModal;