import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SButton from 'components/atoms/button/SButton';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import ItemHelper from 'components/codehelper/ItemHelper';
import SNumericInput from 'components/override/numericinput/SNumericInput';
import STextArea from 'components/atoms/input/STextArea';
import SRadio from 'components/atoms/radio/SRadio';


@inject(stores => ({
	disuseQtySave: stores.disuseStore.disuseQtySave
	, disuseQtyDt: stores.disuseStore.disuseQtyDt
	, selectedDisuseQtyItem: stores.disuseStore.selectedDisuseQtyItem
	, disuseQtyItemHelperResult: stores.disuseStore.disuseQtyItemHelperResult
	, disuseQtyItemHelperOption: stores.disuseStore.disuseQtyItemHelperOption
	, disuseQtyProcessQty: stores.disuseStore.disuseQtyProcessQty
	, disuseQtyText: stores.disuseStore.disuseQtyText
	, handleChange: stores.disuseStore.handleChange
	, handleHelperOpen: stores.disuseStore.handleHelperOpen
	, qtyLedgerType: stores.disuseStore.qtyLedgerType
	, ledgerTypeList: stores.disuseStore.ledgerTypeList
}))

class DisuseQtyInsertModal extends Component {

	constructor(props) {
		super(props);
	}

	render() {

		const {
			disuseQtySave
			, disuseQtyDt
			, selectedDisuseQtyItem
			, disuseQtyItemHelperResult
			, disuseQtyItemHelperOption
			, disuseQtyProcessQty
			, disuseQtyText
			, handleChange
			, handleHelperOpen
			, qtyLedgerType
			, ledgerTypeList
		} = this.props;

		return (
			<React.Fragment>
				<div className='search_item_btn'>
					<SButton
						buttonName={"저장"}
						onClick={disuseQtySave}
						type={"btndDisuseQtySave"} />
				</div>
				<table style={{ width: "100%" }}>
					<tbody>
						<tr>
							<th>폐기일자</th>
							<td>
								<SDatePicker
									id="disuseQtyDt"
									value={disuseQtyDt}
									onChange={handleChange}
								/>
							</td>
							<th>폐기량</th>
							<td>
								<SNumericInput
									id={"disuseQtyProcessQty"}
									value={disuseQtyProcessQty}
									onChange={handleChange}
								/>
								{"KG"}
							</td>
						</tr>
						<tr>
							<th>폐기제품</th>
							<td >
								<ItemHelper
									selectedItemId={"selectedDisuseQtyItem"}
									defaultValue={selectedDisuseQtyItem} //기본값 변수
									onChange={handleChange}
									onHelperResult={disuseQtyItemHelperResult} //제품 선택후 결과 반환 func
									helperOption={disuseQtyItemHelperOption} // 인스턴스 생성 및 세팅 변수
									onOpen={handleHelperOpen}
									cdName={'itemCd'}
									cdNmName={'itemNm'}
								/>
							</td>
							<th>폐기타입</th>
							<td>
								<SRadio 
                				    id={"qtyLedgerType"}
                				    value={qtyLedgerType}
                				    optionGroup={ledgerTypeList}
                				    onChange={handleChange}
                				/>     
							</td>
						</tr>
						<tr>
							<th>비고</th>
							<td colSpan="3">
								<STextArea
									id={"disuseQtyText"}
									value={disuseQtyText}
									onChange={handleChange}
									contentWidth={690}
								/>
							</td>
						</tr>
					</tbody>
				</table>
				{/* <ContentsMiddleTemplate contentSubTitle={'바코드 내역'} middleItem={<EtcBarcodeAddMiddleItem />} />
                <ContentsTemplate id='holdInsertModal' contentItem={<HoldInsertModalContents />} /> */}
			</React.Fragment>
		);
	}
}

export default DisuseQtyInsertModal;