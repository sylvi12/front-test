import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SButton from 'components/atoms/button/SButton';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import ItemHelper from 'components/codehelper/ItemHelper';
import SNumericInput from 'components/override/numericinput/SNumericInput';
import STextArea from 'components/atoms/input/STextArea';


@inject(stores => ({
	btnDisuseQtyUpdateModalSave: stores.disuseStore.btnDisuseQtyUpdateModalSave
	, disuseQtyDt_u: stores.disuseStore.disuseQtyDt_u
	, selectedDisuseQtyItem_u: stores.disuseStore.selectedDisuseQtyItem_u
	, disuseQtyItemHelperOption_u: stores.disuseStore.disuseQtyItemHelperOption_u
	, disuseQtyProcessQty_u: stores.disuseStore.disuseQtyProcessQty_u
	, disuseQtyProcessUom_u: stores.disuseStore.disuseQtyProcessUom_u
	, disuseQtyText_u: stores.disuseStore.disuseQtyText_u
	, handleChange: stores.disuseStore.handleChange
}))

class DisuseQtyUpdateModal extends Component {

	constructor(props) {
		super(props);
	}

	render() {

		const {
			btnDisuseQtyUpdateModalSave
			, disuseQtyDt_u
			, disuseQtyProcessQty_u
			, disuseQtyProcessUom_u
			, selectedDisuseQtyItem_u
			, disuseQtyItemHelperOption_u
			, disuseQtyText_u
			, handleChange
		} = this.props;

		return (
			<React.Fragment>
				<div className='search_item_btn'>
					<SButton 
					buttonName={"변경"} 
					onClick={btnDisuseQtyUpdateModalSave}
					type={"btnDisuseQtyUpdateModalSave"} />
				</div>
				<table style={{ width: "100%" }}>
					<tbody>
						<tr>
							<th>폐기일자</th>
							<td>
								<SDatePicker
									id="disuseQtyDt_u"
									value={disuseQtyDt_u}
									onChange={handleChange}
									readOnly={true}
								/>
							</td>
							<th>폐기량</th>
							<td>
								<SNumericInput
									id={"disuseQtyProcessQty_u"}
									value={disuseQtyProcessQty_u}
									onChange={handleChange}
								/>
								{disuseQtyProcessUom_u}
							</td>
						</tr>
						<tr>
							<th>폐기제품</th>
							<td>
								<ItemHelper
									selectedItemId={"selectedDisuseQtyItem_u"}
									defaultValue={selectedDisuseQtyItem_u} //기본값 변수
									onChange={handleChange}
									helperOption={disuseQtyItemHelperOption_u} // 인스턴스 생성 및 세팅 변수
									cdName={'itemCd'}
									cdNmName={'itemNm'}
									readOnly={true}
								/>
							</td>
						</tr>
						<tr>
							<th>비고</th>
							<td colSpan="3">
								<STextArea
									id={"disuseQtyText_u"}
									value={disuseQtyText_u}
									onChange={handleChange}
									contentWidth={690}
								/>
							</td>
						</tr>
					</tbody>
				</table>
			</React.Fragment>
		);
	}
}

export default DisuseQtyUpdateModal;