import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    disuseBarcodeRegister: stores.disuseStore.disuseBarcodeRegister
    , disuseBarcodeDelete: stores.disuseStore.disuseBarcodeDelete
    , disuseQtyBarcodeRegisterAvailableToggle: stores.disuseStore.disuseQtyBarcodeRegisterAvailableToggle
    , disuseQtyBarcodeDeleteAvailableToggle: stores.disuseStore.disuseQtyBarcodeDeleteAvailableToggle
}))

class DisuseBarcodeMiddleItem extends Component {

    render() {

        const { 
            disuseBarcodeRegister
            , disuseBarcodeDelete
            , disuseQtyBarcodeRegisterAvailableToggle
            , disuseQtyBarcodeDeleteAvailableToggle
         } = this.props;
        return (
            <React.Fragment>
                <SButton
                    className="btn_red"
                    buttonName={'추가'}
                    type={'btnBarcodeAdd'}
                    onClick={disuseBarcodeRegister}
                    visible={disuseQtyBarcodeRegisterAvailableToggle}
                />
                <SButton
                    className="btn_red"
                    buttonName={'삭제'}
                    type={'btnBarcodeDelete'}
                    onClick={disuseBarcodeDelete}
                    visible={disuseQtyBarcodeDeleteAvailableToggle}
                />
            </React.Fragment>
        )
    }
}

export default DisuseBarcodeMiddleItem;