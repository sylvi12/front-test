import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SInput from 'components/atoms/input/SInput'
import SButton from 'components/atoms/button/SButton'
import DisuseStore from 'modules/pages/inout/disuse/service/DisuseStore';
import SBuSelectBox from 'components/override/business/SBuSelectBox';

@inject(stores => ({
    buId: stores.disuseStore.buId,
    buGroup: stores.disuseStore.buGroup,
    handleChange: stores.disuseStore.handleChange,
    startDt: stores.disuseStore.startDt,
    endDt: stores.disuseStore.endDt,
    itemNmOrItemCd: stores.disuseStore.itemNmOrItemCd,
    handleSearchClick: stores.disuseStore.handleSearchClick,
}))
class DisuseSearchItem extends Component {

    constructor(props) {
        super(props);
    }

    async componentWillMount() {
        if (DisuseStore.alphaCheck == false) {
            await DisuseStore.getBu();
        }
    }

    render() {

        const { buId, buGroup, handleChange
            , startDt, endDt
            , itemNmOrItemCd, handleSearchClick
        } = this.props;

        return (
            <React.Fragment>
                {/* <SSelectBox
                    title={"공장"}
                    id={"buId"}
                    value={buId}
                    optionGroup={buGroup}
                    valueMember={'buId'}
                    displayMember={'buNm'}
                    onChange={handleChange}
                    addOption={"전체"}
                /> */}

                <SBuSelectBox
                    title={"공장"}
                    id={"buId"}
                    value={buId}
                    onChange={handleChange}
                // onSelectionChange={buSelectionChanged}
                />

                <SDatePicker
                    title={"폐기일자"}
                    id="startDt"
                    value={startDt}
                    onChange={handleChange}
                />

                <SDatePicker
                    title={"~"}
                    id="endDt"
                    value={endDt}
                    onChange={handleChange}
                />

                <SInput
                    title={"제품"}
                    id={"itemNmOrItemCd"}
                    value={itemNmOrItemCd}
                    onChange={handleChange}
                    onEnterKeyDown={handleSearchClick}
                />

                <div className='search_item_btn'>
                    <SButton buttonName={'조회'} onClick={handleSearchClick} type={'btnSearch'} />
                </div>

            </React.Fragment>
        )
    }
}

export default DisuseSearchItem;