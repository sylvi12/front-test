import React, { Component } from "react";
import { inject } from "mobx-react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";
import SModal from "components/override/modal/SModal";
import BarcodeAddHelper from "components/codehelper/BarcodeAddHelper";

import DisuseSearchItem from "modules/pages/inout/disuse/DisuseSearchItem";
import DisuseDisuseMiddleItem from "modules/pages/inout/disuse/disueContentsAssemble/DisuseDisuseMiddleItem";
import DisuseDisuseContents from "modules/pages/inout/disuse/disueContentsAssemble/DisuseDisuseContents";
import DisuseBarcodeMiddleItem from "modules/pages/inout/disuse/DisuseBarcodeMiddleItem";
import DisuseBarcodeContents from "modules/pages/inout/disuse/disueContentsAssemble/DisuseBarcodeContents";
import DisuseContents from "modules/pages/inout/disuse/DisuseContents";
import DisuseUpsertModal from "modules/pages/inout/disuse/disueContentsAssemble/DisuseUpsertModal";
import DisuseBarcodeUpsertModal from "modules/pages/inout/disuse/disuseBarcodeUpsertModal/DisuseBarcodeUpsertModal";
import DisuseUpdateModal from "modules/pages/inout/disuse/disuseUpdateModal/DisuseUpdateModal";
import DisuseQtyInsertModal from "modules/pages/inout/disuse/disuseUpdateModal/DisuseQtyInsertModal";
import DisuseQtyUpdateModal from "modules/pages/inout/disuse/disuseUpdateModal/DisuseQtyUpdateModal";

@inject(stores => ({
    disuseQtyInsertModalIsOpen: stores.disuseStore.disuseQtyInsertModalIsOpen
    , closeDisuseQtyInsertModal: stores.disuseStore.closeDisuseQtyInsertModal
    , disuseUpsertModalIsOpen: stores.disuseStore.disuseUpsertModalIsOpen
    , closeDisuseUpsertModal: stores.disuseStore.closeDisuseUpsertModal
    , disuseQtyUpdateModalIsOpen: stores.disuseStore.disuseQtyUpdateModalIsOpen
    , closeDisuseQtyUpdateModal: stores.disuseStore.closeDisuseQtyUpdateModal

    , disuseBarcodeUpsertModalIsOpen: stores.disuseStore.disuseBarcodeUpsertModalIsOpen
    , closeDisuseBarcodeUpsertModal: stores.disuseStore.closeDisuseBarcodeUpsertModal

    , isBarcodeHelperOpen: stores.disuseStore.isBarcodeHelperOpen
    , barocdeHelperResult: stores.disuseStore.barocdeHelperResult
    , handleChange: stores.disuseStore.handleChange
    , barcodeHelplerOrderNo: stores.disuseStore.barcodeHelplerOrderNo
    , barcodeHelplerledgerType: stores.disuseStore.barcodeHelplerledgerType

    , udateModalOpen: stores.disuseStore.udateModalOpen
    , closeUpdateModal: stores.disuseStore.closeUpdateModal
}))

class Disuse extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { disuseQtyInsertModalIsOpen, closeDisuseQtyInsertModal, disuseUpsertModalIsOpen, closeDisuseUpsertModal
            , disuseBarcodeUpsertModalIsOpen, closeDisuseBarcodeUpsertModal
            , disuseQtyUpdateModalIsOpen, closeDisuseQtyUpdateModal
            , isBarcodeHelperOpen, barocdeHelperResult, handleChange, barcodeHelplerOrderNo, barcodeHelplerledgerType
            , udateModalOpen, closeUpdateModal } = this.props

        return (
            <React.Fragment>
                <div style={{ width: '100%', height: '60%' }}>
                    <SearchTemplate searchItem={<DisuseSearchItem />} />
                    <ContentsMiddleTemplate contentSubTitle={'폐기출고 내역'} middleItem={<DisuseDisuseMiddleItem />} />
                    <ContentsTemplate id='disuseDisuse' contentItem={<DisuseDisuseContents />} />
                </div>
                <div style={{ width: '100%', height: '40%' }}>
                    <ContentsMiddleTemplate contentSubTitle={'바코드 내역'} middleItem={<DisuseBarcodeMiddleItem />} />
                    <ContentsTemplate id='disuseBarcode' contentItem={<DisuseBarcodeContents />} />
                </div>
                {/* <ContentsTemplate id='disuse' contentItem={<DisuseContents />} /> */}


                {/* 수량Base 등록 Modal */}
                <SModal
                    formId={"inout_disuseqty_p"}
                    isOpen={disuseQtyInsertModalIsOpen}
                    onRequestClose={closeDisuseQtyInsertModal}
                    contentLabel="폐기출고 등록(수량)"
                    width={800}
                    height={250}
                    contents={<DisuseQtyInsertModal />}
                />

                <SModal
                    formId={"inout_disuseqty_update_p"}
                    isOpen={disuseQtyUpdateModalIsOpen}
                    onRequestClose={closeDisuseQtyUpdateModal}
                    contentLabel="폐기출고 수정(수량)"
                    width={800}
                    height={250}
                    contents={<DisuseQtyUpdateModal />}
                />


                <SModal
                    formId={"inout_disuse_p"}
                    isOpen={disuseUpsertModalIsOpen}
                    onRequestClose={closeDisuseUpsertModal}
                    contentLabel="폐기출고 등록(바코드)"
                    width={800}
                    height={500}
                    contents={<DisuseUpsertModal />}
                />

                <SModal
                    formId={"inout_disuse_update_p"}
                    isOpen={udateModalOpen}
                    onRequestClose={closeUpdateModal}
                    contentLabel="폐기출고 수정(바코드)"
                    width={500}
                    height={300}
                    contents={<DisuseUpdateModal />}
                />

                <SModal
                    formId={"inout_barcode_add_p"}
                    isOpen={disuseBarcodeUpsertModalIsOpen}
                    onRequestClose={closeDisuseBarcodeUpsertModal}
                    contentLabel="바코드 추가"
                    width={1220}
                    height={530}
                    contents={<DisuseBarcodeUpsertModal />}
                />
                <BarcodeAddHelper
                    id={'isBarcodeHelperOpen'}
                    isBarcodeOpen={isBarcodeHelperOpen}
                    barcodeHelplerOrderNo={barcodeHelplerOrderNo}
                    barcodeHelplerledgerType={barcodeHelplerledgerType}
                    handleChange={handleChange}
                    onHelperResult={barocdeHelperResult}
                />
            </React.Fragment>
        )
    }
}

export default Disuse;