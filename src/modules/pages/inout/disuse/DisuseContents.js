import React, { Component } from "react";

import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import DisuseDisuseMiddleItem from "modules/pages/inout/disuse/disueContentsAssemble/DisuseDisuseMiddleItem";
import DisuseDisuseContents from "modules/pages/inout/disuse/disueContentsAssemble/DisuseDisuseContents";
import DisuseBarcodeMiddleItem from "modules/pages/inout/disuse/DisuseBarcodeMiddleItem";
import DisuseBarcodeContents from "modules/pages/inout/disuse/disueContentsAssemble/DisuseBarcodeContents";

class DisuseContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { } = this.props

        return (
            <React.Fragment>
                <div style={{ width: '100%', height: '50%' }}>
                    <ContentsMiddleTemplate contentSubTitle={'폐기출고'} middleItem={<DisuseDisuseMiddleItem />} />
                    <ContentsTemplate id='disuseDisuse' contentItem={<DisuseDisuseContents />} />
                </div>
                <div style={{ width: '100%', height: '50%' }}>
                    <ContentsMiddleTemplate contentSubTitle={'바코드 내역'} middleItem={<DisuseBarcodeMiddleItem />} />
                    <ContentsTemplate id='disuseBarcode' contentItem={<DisuseBarcodeContents />} />
                </div>
            </React.Fragment>
        )
    }
}

export default DisuseContents;