import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SButton from 'components/atoms/button/SButton';
import SBuSelectBox from 'components/override/business/SBuSelectBox';
import SInput from 'components/atoms/input/SInput';

import EtcInStore from "modules/pages/inout/etcIn/service/EtcInStore";

@inject(stores => ({

    buId: stores.etcInStore.buId
    , buList: stores.etcInStore.buList
    , handleChange: stores.etcInStore.handleChange
    , startDt: stores.etcInStore.startDt
    , endDt: stores.etcInStore.endDt
    , handleSearchClick: stores.etcInStore.handleSearchClick
    , itemCdNm: stores.etcInStore.itemCdNm
    , buSelectionChanged: stores.etcInStore.buSelectionChanged

}))

class EtcInSearchItem extends Component {

    constructor(props) {
        super(props);
        // EtcInStore.getBu();
    }

    async componentWillMount() {
        if (EtcInStore.alphaCheck == false) {
            await EtcInStore.getBu();
        }
    }

    render() {

        const { buId
            , buList
            , handleChange
            , startDt, endDt
            , handleSearchClick
            , itemCdNm
            , buSelectionChanged } = this.props;

        return (
            <React.Fragment>
                <div className='search_item_btn'>
                    <SButton
                        buttonName={'조회'}
                        onClick={handleSearchClick}
                        type={'btnSearch'}
                    />
                </div>


                {/* <SSelectBox
                        title={"공장"}
                        id={"buId"}
                        value={buId}
                        optionGroup={buList}
                        valueMember={'buId'}
                        displayMember={'buNm'}
                        onChange={handleChange}
                        onSelectionChange={buSelectionChanged}
                        addOption={"전체"}
                    /> */}

                <SBuSelectBox
                    title={"공장"}
                    id={"buId"}
                    value={buId}
                    onChange={handleChange}
                // onSelectionChange={buSelectionChanged}
                />

                <SDatePicker
                    title={"입고일자"}
                    id="startDt"
                    value={startDt}
                    onChange={handleChange}
                />

                <SDatePicker
                    title={"~"}
                    id="endDt"
                    value={endDt}
                    onChange={handleChange}
                />

                <SInput
                    title={"제품"}
                    id={"itemCdNm"}
                    value={itemCdNm}
                    onChange={handleChange}
                    onEnterKeyDown={handleSearchClick}
                />

            </React.Fragment>
        );
    }
}

export default EtcInSearchItem;