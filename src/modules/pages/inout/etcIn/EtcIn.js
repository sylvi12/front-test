import React, { Component } from "react";
import { inject } from "mobx-react";

import SModal from "components/override/modal/SModal";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import EtcInSearchItem from "modules/pages/inout/etcIn/EtcInSearchItem";
import EtcInMiddleItem from "modules/pages/inout/etcIn/EtcInMiddleItem";
import EtcInContents from "modules/pages/inout/etcIn/EtcInContents";

import EtcInBarcodeMiddleItem from "modules/pages/inout/etcIn/EtcInBarcodeMiddleItem";
import EtcInBarcodeContents from "modules/pages/inout/etcIn/EtcInBarcodeContents";

import EtcInInsertModal from "modules/pages/inout/etcIn/etcInModal/EtcInInsertModal";

import LocationHelper from "components/codehelper/LocationHelper";

import BarcodeCreateModal from "modules/pages/inout/etcIn/barcodeCreateModal/BarcodeCreateModal";
import EtcInQtyInsertModal from "modules/pages/inout/etcIn/etcInModal/EtcInQtyInsertModal";
import EtcInQtyUpdateModal from "modules/pages/inout/etcIn/etcInModal/EtcInQtyUpdateModal";
import EtcInUpdateModal from "modules/pages/inout/etcIn/etcInModal/EtcInUpdateModal";

import EtcInStore from "modules/pages/inout/etcIn/service/EtcInStore";

@inject(stores => ({
      etcInModalOpen: stores.etcInStore.etcInModalOpen
    , etcInModalClose: stores.etcInStore.etcInModalClose

    , barcodeModalOpen: stores.etcInStore.barcodeModalOpen
    , barcodeModalClose: stores.etcInStore.barcodeModalClose

    , etcInQtyModalOpen: stores.etcInStore.etcInQtyModalOpen
    , etcInQtyModalClose: stores.etcInStore.etcInQtyModalClose

    , isLocationOpen: stores.etcInStore.isLocationOpen
    , locationBuId: stores.etcInStore.locationBuId
    , locationHelperResult: stores.etcInStore.locationHelperResult

    , etcInQtyUpdateModalOpen: stores.etcInStore.etcInQtyUpdateModalOpen
    , etcInQtyUpdateModalClose: stores.etcInStore.etcInQtyUpdateModalClose
    , etcInUpdateModalOpen: stores.etcInStore.etcInUpdateModalOpen
    , etcInUpdateModalClose: stores.etcInStore.etcInUpdateModalClose

    , barcodeAddModalOpen: stores.etcInStore.barcodeAddModalOpen
    , barcodeAddModalClose: stores.etcInStore.barcodeAddModalClose
    , isBarcodeHelperOpen: stores.etcInStore.isBarcodeHelperOpen
    , barcodeHelplerOrderNo: stores.etcInStore.barcodeHelplerOrderNo
    , barcodeHelplerledgerType: stores.etcInStore.barcodeHelplerledgerType
    , barocdeHelperResult: stores.etcInStore.barocdeHelperResult
    , handleChange: stores.etcInStore.handleChange
}))

class EtcIn extends Component {

    constructor(props) {
        super(props);
        
        
    }

    render() {

        const {   etcInModalOpen
                , etcInModalClose

                , barcodeModalOpen
                , barcodeModalClose

                , etcInQtyModalOpen
                , etcInQtyModalClose

                , isLocationOpen
                , locationBuId
                , locationHelperResult
                , etcInQtyUpdateModalOpen
                , etcInQtyUpdateModalClose
                , etcInUpdateModalOpen
                , etcInUpdateModalClose

                , barcodeAddModalOpen
                , barcodeAddModalClose
                , isBarcodeHelperOpen
                , barcodeHelplerOrderNo
                , barcodeHelplerledgerType
                , barocdeHelperResult
                , handleChange
                } = this.props;

        return (
            <React.Fragment>
                <div style={{ width: '100%', height: '50%' }}>
                    <SearchTemplate searchItem={<EtcInSearchItem />} />
                    <ContentsMiddleTemplate contentSubTitle={'기타입고 내역'} middleItem={<EtcInMiddleItem />} />
                    <ContentsTemplate id='etcIn' contentItem={<EtcInContents />} />
                </div>
                <div style={{ width: '100%', height: '50%' }}>
                    <ContentsMiddleTemplate contentSubTitle={'바코드내역'} middleItem={<EtcInBarcodeMiddleItem />} />
                    <ContentsTemplate id='etcInBarcode' contentItem={<EtcInBarcodeContents />} />
                </div>

                <SModal
                    formId={"inout_etcIn_insert_barcode_p"}
                    id="etcInInsert"
                    isOpen={etcInModalOpen}
                    onRequestClose={etcInModalClose}
                    contentLabel="기타입고(바코드)등록"
                    width={600}
                    height={250}
                    contents={<EtcInInsertModal />}
                />

                <SModal
                    formId={"inout_etcIn_insert_qty_p"}
                    id="etcInInsert"
                    isOpen={etcInQtyModalOpen}
                    onRequestClose={etcInQtyModalClose}
                    contentLabel="기타입고(수량)등록"
                    width={600}
                    height={250}
                    contents={<EtcInQtyInsertModal />}
                />

                <SModal
                    formId={"inout_etcIn_update_qty_p"}
                    id="etcInUpdateQty"
                    isOpen={etcInQtyUpdateModalOpen}
                    onRequestClose={etcInQtyUpdateModalClose}
                    contentLabel="기타입고(수량) 수정"
                    width={600}
                    height={250}
                    contents={<EtcInQtyUpdateModal />}
                />

                <SModal
                    formId={"inout_etcIn_update_p"}
                    id="etcInUpdate"
                    isOpen={etcInUpdateModalOpen}
                    onRequestClose={etcInUpdateModalClose}
                    contentLabel="기타입고(바코드) 수정"
                    width={600}
                    height={250}
                    contents={<EtcInUpdateModal />}
                />

               <SModal
                    formId={"inout_etcIn_barcode_create_p"}
                    id="barcodeCreate"
                    isOpen={barcodeModalOpen}
                    onRequestClose={barcodeModalClose}
                    contentLabel="바코드 발행"
                    width={600}
                    height={310}
                    minWidth={600}
                    minHeight={310}
                    contents={<BarcodeCreateModal />}
                />

                <LocationHelper
                    id={'isLocationOpen'}
                    isLocationOpen={isLocationOpen}
                    buId={locationBuId}
                    buReadOnly={true}
                    onChange={handleChange}
                    onHelperResult={locationHelperResult}
                />
            </React.Fragment>
        );
    }
}

export default EtcIn;