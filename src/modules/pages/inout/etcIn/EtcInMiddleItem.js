import React, { Component } from "react";
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
      openEtcInQtyModal: stores.etcInStore.openEtcInQtyModal
    , openEtcInBarcodeModal: stores.etcInStore.openEtcInBarcodeModal
    , confirm: stores.etcInStore.confirm
    , confirmCancel: stores.etcInStore.confirmCancel
    , etcInDelete: stores.etcInStore.etcInDelete
    , createBarcode: stores.etcInStore.createBarcode
}))

class EtcInMiddleItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {   openEtcInQtyModal
                , openEtcInBarcodeModal
                , confirm
                , confirmCancel
                , etcInDelete
                , createBarcode } =this.props;

        return (

            <React.Fragment>
                <SButton 
                    className="btn_red"
                    buttonName={'등록(수량)'}
                    type={'btnEtcQtyInsert'}
                    onClick={openEtcInQtyModal}
                />
                <SButton 
                    className="btn_red"
                    buttonName={'등록(바코드)'}
                    type={'btnEtcBarcodeInsert'}
                    onClick={openEtcInBarcodeModal}
                />

                <SButton 
                    className="btn_red"
                    buttonName={'삭제'}
                    type={'btnEtcDelete'}
                    onClick={etcInDelete}
                />
                
                {/* <SButton 
                    className="btn_red"
                    buttonName={'확정'}
                    type={'btnEtcConfirm'}
                    onClick={confirm}
                />

                <SButton 
                    className="btn_red"
                    buttonName={'확정취소'}
                    type={'btnEtcCancel'}
                    onClick={confirmCancel}
                /> */}

                <SButton 
                    className="btn_red"
                    buttonName={'바코드 발행'}
                    type={'btnBarcodeCreate'}
                    onClick={createBarcode}
                />
                
            </React.Fragment>
        );
    }
}

export default EtcInMiddleItem;