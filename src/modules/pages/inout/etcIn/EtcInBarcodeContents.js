import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid';

@inject(stores => ({
      setBarcodeGridApi: stores.etcInStore.setBarcodeGridApi
    , barcodeList: stores.etcInStore.barcodeList
    , pinnedBottomRowDataAtEtcBarcodeGrid: stores.etcInStore.pinnedBottomRowDataAtEtcBarcodeGrid //이름수정할거임
}))

class EtcInBarcodeContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setBarcodeGridApi, barcodeList, pinnedBottomRowDataAtEtcBarcodeGrid} = this.props;

        return (
            <SGrid 
                grid={'barcodeGrid'}
                gridApiCallBack={setBarcodeGridApi}
                rowData={barcodeList}
                editable={false}
                pinnedBottomRowData={pinnedBottomRowDataAtEtcBarcodeGrid}
                suppressRowClickSelection = {true}
            />
        );
    }
}

export default EtcInBarcodeContents;