import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid';

@inject(stores => ({
    setEtcInGridApi: stores.etcInStore.setEtcInGridApi
    , etcInList: stores.etcInStore.etcInList
    , getBarcode: stores.etcInStore.getBarcode
    , openModalEdit: stores.etcInStore.openModalEdit
}))

class EtcInContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setEtcInGridApi, etcInList, getBarcode, openModalEdit } = this.props;

        return(
            <SGrid 
                grid={'etcInGrid'}
                gridApiCallBack={setEtcInGridApi}
                rowData={etcInList}
                onSelectionChanged={getBarcode}
                // onCellClicked={getBarcode}
                rowDoubleClick={openModalEdit}
                suppressRowClickSelection={false}
                //cellReadOnlyColor={true}
                editable={false}
            />
        );
    }
}

export default EtcInContents;