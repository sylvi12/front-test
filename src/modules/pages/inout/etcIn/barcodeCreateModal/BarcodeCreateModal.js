import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SButton from 'components/atoms/button/SButton';
import SNumericInput from 'components/override/numericinput/SNumericInput';
import EmployeeHelper from 'components/codehelper/EmployeeHelper';

@inject(stores => ({
    handleChange: stores.etcInStore.handleChange

    , barcodeModalOrderNo: stores.etcInStore.barcodeModalOrderNo
    , barcodeModalLotNo: stores.etcInStore.barcodeModalLotNo

    , barcodeModalItemCd: stores.etcInStore.barcodeModalItemCd
    , barcodeModalItemNm: stores.etcInStore.barcodeModalItemNm

    , barcodeModalProdDt: stores.etcInStore.barcodeModalProdDt
    , makeExpireDate: stores.etcInStore.makeExpireDate
    , barcodeModalExpiryDt: stores.etcInStore.barcodeModalExpiryDt

    , barcodeModalProdUomQty: stores.etcInStore.barcodeModalProdUomQty
    , barcodeModalProdUom: stores.etcInStore.barcodeModalProdUom

    , barcodeModalPackQty: stores.etcInStore.barcodeModalPackQty
    , calculateProdQty: stores.etcInStore.calculateProdQty
    , barcodeModalPackUom: stores.etcInStore.barcodeModalPackUom

    , barcodeModalProdQty: stores.etcInStore.barcodeModalProdQty

    , rackTypeHide: stores.etcInStore.rackTypeHide
    , barcodeModalRackType: stores.etcInStore.barcodeModalRackType

    // , prodEmp: stores.workorderStore.prodEmp
    // , employeeOptionModel: stores.workorderStore.employeeOptionModel
    // , prodEmployeeReturn: stores.workorderStore.prodEmployeeReturn

    // , packEmp: stores.workorderStore.packEmp
    // , packEmployeeReturn: stores.workorderStore.packEmployeeReturn

    // , prodEmpNo: stores.workorderStore.prodEmpNo
    , prodEmpNm: stores.etcInStore.prodEmpNm
    // , packerEmpNo: stores.workorderStore.packerEmpNo
    , packerEmpNm: stores.etcInStore.packerEmpNm

    , minSeq: stores.etcInStore.minSeq
    , maxSeq: stores.etcInStore.maxSeq

    , barcodeModalMemo: stores.etcInStore.barcodeModalMemo

    // , printPage: stores.workorderStore.printPage    

    , barcodeCreate: stores.etcInStore.barcodeCreate
    , closeBarcodeModal: stores.etcInStore.closeBarcodeModal
}))
@observer
class BarcodeCreateModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { handleChange
            , barcodeModalOrderNo, barcodeModalLotNo
            , barcodeModalItemCd, barcodeModalItemNm
            , barcodeModalProdDt, makeExpireDate, barcodeModalExpiryDt
            , barcodeModalProdUomQty, barcodeModalProdUom
            , barcodeModalPackQty, calculateProdQty, barcodeModalPackUom
            , barcodeModalProdQty
            , rackTypeHide, barcodeModalRackType
            , prodEmp, employeeOptionModel, prodEmployeeReturn
            , packEmp, packEmployeeReturn
            , prodEmpNo, prodEmpNm
            , packerEmpNo, packerEmpNm
            , minSeq, maxSeq
            , barcodeModalMemo
            , printPage
            , barcodeCreate, closeBarcodeModal
        } = this.props;

        if (rackTypeHide == true) {
            return (
                <React.Fragment>                
                    <table style={{ width: "100%" }}>
                        <tbody>
                            <tr>
                                <td className="th">지시번호</td>
                                <td>
                                    <SInput
                                        id="barcodeModalOrderNo"
                                        value={barcodeModalOrderNo}
                                        onChange={handleChange}
                                        readOnly={true}
                                    />
                                </td>
                                <td className="th">LOT NO</td>
                                <td>
                                    <SInput
                                        id={"barcodeModalLotNo"}
                                        value={barcodeModalLotNo}
                                        onChange={handleChange}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td className="th">제품코드</td>
                                <td>
                                    <SInput
                                        id="barcodeModalItemCd"
                                        value={barcodeModalItemCd}
                                        onChange={handleChange}
                                        readOnly={true}
                                    />
                                </td>
                                <td className="th">제품명</td>
                                <td>
                                    <SInput
                                        id="barcodeModalItemNm"
                                        value={barcodeModalItemNm}
                                        onChange={handleChange}
                                        readOnly={true}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td className="th">생산일자</td>
                                <td>
                                    <SDatePicker
                                        id="barcodeModalProdDt"
                                        value={barcodeModalProdDt}
                                        onChange={handleChange}
                                        onValueChange={makeExpireDate}
                                    />
                                </td>
                                <td className="th">유통기한</td>
                                <td>
                                    <SDatePicker
                                        id="barcodeModalExpiryDt"
                                        value={barcodeModalExpiryDt}
                                        onChange={handleChange}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td className="th">제품규격</td>
                                <td>
                                    <SNumericInput
                                        id="barcodeModalProdUomQty"
                                        value={barcodeModalProdUomQty}
                                        onChange={handleChange}
                                        readOnly={true}
                                    />
                                    {barcodeModalProdUom}
                                </td>
                                <td className="th">포장수량</td>
                                <td>
                                    <SNumericInput
                                        id="barcodeModalPackQty"
                                        value={barcodeModalPackQty}
                                        onChange={handleChange}
                                        onValueChange={calculateProdQty}
                                    />
                                    {barcodeModalPackUom}
                                </td>
                            </tr>
                            <tr>
                                <td className="th">포장량</td>
                                <td>
                                    <SNumericInput
                                        id="barcodeModalProdQty"
                                        value={barcodeModalProdQty}
                                        onChange={handleChange}
                                        readOnly={true}
                                    />
                                    {barcodeModalProdUom}
                                </td>
                                <td className="th"></td>
                                <td></td>                              
                            </tr>
                            {/* <tr>
                                
                                <td className="th">생산자</td>
                                <td>
                                    <SInput
                                        id="prodEmpNm"
                                        value={prodEmpNm}
                                        onChange={handleChange}
                                    />
                                </td>
                                <td className="th">포장자</td>
                                <td>
                                    <SInput
                                        id="packerEmpNm"
                                        value={packerEmpNm}
                                        onChange={handleChange}
                                    />
                                </td>
                            </tr> */}
                            <tr>                                
                            </tr>
                            <tr>
                                <td className="th">일련번호</td>
                                <td>
                                    <SNumericInput
                                        id="minSeq"
                                        value={minSeq}
                                        onChange={handleChange}
                                        contentWidth={50}
                                    />
                                    <div style={{ marginLeft: "1px", marginRight: "1px", display: "inline" }}>~</div>
                                    <SNumericInput
                                        id="maxSeq"
                                        value={maxSeq}
                                        onChange={handleChange}
                                        contentWidth={50}
                                    />
                                </td>
                                <td className="th">비고</td>
                                <td>
                                    <SInput
                                        id="barcodeModalMemo"
                                        value={barcodeModalMemo}
                                        onChange={handleChange}
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <div>
                        <SButton buttonName={"바코드 생성"} onClick={barcodeCreate} type={"btnCreateBarcode"} />
                    </div>                
                </React.Fragment>
            );
        } else {
            return (
                <React.Fragment>                
                    <table style={{ width: "100%" }}>
                        <tbody>
                            <tr>
                                <td className="th">지시번호</td>
                                <td>
                                    <SInput
                                        id="barcodeModalOrderNo"
                                        value={barcodeModalOrderNo}
                                        onChange={handleChange}
                                        readOnly={true}
                                    />
                                </td>
                                <td className="th">LOT NO</td>
                                <td>
                                    <SInput
                                        id={"barcodeModalLotNo"}
                                        value={barcodeModalLotNo}
                                        onChange={handleChange}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td className="th">제품코드</td>
                                <td>
                                    <SInput
                                        id="barcodeModalItemCd"
                                        value={barcodeModalItemCd}
                                        onChange={handleChange}
                                        readOnly={true}
                                    />
                                </td>
                                <td className="th">제품명</td>
                                <td>
                                    <SInput
                                        id="barcodeModalItemNm"
                                        value={barcodeModalItemNm}
                                        onChange={handleChange}
                                        readOnly={true}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td className="th">생산일자</td>
                                <td>
                                    <SDatePicker
                                        id="barcodeModalProdDt"
                                        value={barcodeModalProdDt}
                                        onChange={handleChange}
                                        onValueChange={makeExpireDate}
                                    />
                                </td>
                                <td className="th">유통기한</td>
                                <td>
                                    <SDatePicker
                                        id="barcodeModalExpiryDt"
                                        value={barcodeModalExpiryDt}
                                        onChange={handleChange}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td className="th">제품규격</td>
                                <td>
                                    <SNumericInput
                                        id="barcodeModalProdUomQty"
                                        value={barcodeModalProdUomQty}
                                        onChange={handleChange}
                                        readOnly={true}
                                    />
                                    {barcodeModalProdUom}
                                </td>
                                <td className="th">포장수량</td>
                                <td>
                                    <SNumericInput
                                        id="barcodeModalPackQty"
                                        value={barcodeModalPackQty}
                                        onChange={handleChange}
                                        onValueChange={calculateProdQty}
                                    />
                                    {barcodeModalPackUom}
                                </td>
                            </tr>
                            <tr>
                                <td className="th">포장량</td>
                                <td>
                                    <SNumericInput
                                        id="barcodeModalProdQty"
                                        value={barcodeModalProdQty}
                                        onChange={handleChange}
                                        readOnly={true}
                                    />
                                    {barcodeModalProdUom}
                                </td>
                                <td className="th">적재높이(PL)</td>
                                <td>
                                    <SSelectBox
                                        id={"barcodeModalRackType"}
                                        value={barcodeModalRackType}
                                        codeGroup={"MFD009"}
                                        onChange={handleChange}
                                    />
                                </td>                                
                            </tr>
                            <tr>
                                <td className="th">일련번호</td>
                                <td>
                                    <SNumericInput
                                        id="minSeq"
                                        value={minSeq}
                                        onChange={handleChange}
                                        contentWidth={50}
                                    />
                                    <div style={{ marginLeft: "1px", marginRight: "1px", display: "inline" }}>~</div>
                                    <SNumericInput
                                        id="maxSeq"
                                        value={maxSeq}
                                        onChange={handleChange}
                                        contentWidth={50}
                                    />
                                </td>
                                <td className="th">비고</td>
                                <td>
                                    <SInput
                                        id="barcodeModalMemo"
                                        value={barcodeModalMemo}
                                        onChange={handleChange}
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <div>
                        <SButton buttonName={"바코드 생성"} onClick={barcodeCreate} type={"btnCreateBarcode"} />
                    </div>                
                </React.Fragment>
            );
        }
        
        
    }
}

export default BarcodeCreateModal;