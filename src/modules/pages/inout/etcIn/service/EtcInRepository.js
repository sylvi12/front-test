import API from "components/override/api/API";

class EtcInRepository {
    URL = "/api/inout/etcIn";

    /* 조회 */
    // 기타입고 조회
    getEtcIn(param) {
        return API.request.get(encodeURI(`${this.URL}/getEtcIn?buId=${param.buId}&startDt=${param.startDt}&endDt=${param.endDt}&itemCdNm=${param.itemCdNm}`))
    }
    //바코드 내역 조회
    getBarcodeHis(param) {
        return API.request.get(encodeURI(`${this.URL}/getBarcodeHis?orderNo=${param.orderNo}&barcodeYn=${param.barcodeYn}&itemId=${param.itemId}&itemCd=${param.itemCd}`));
    }
  
    /* 등록 */
    //기타입고(바코드) 등록
    saveEtcIn(params) {
        return API.request.post(encodeURI(`${this.URL}/saveEtcIn`), params);
    }

    //기타입고(수량) 등록
    saveEtcInQty(params) {
        return API.request.post(encodeURI(`${this.URL}/saveEtcInQty`), params);
    }

    /* 수정 */
    //기타입고(바코드) 수정
    updateEtcIn(params) {
        return API.request.post(encodeURI(`${this.URL}/updateetcin`), params);
    }
    
    //기타입고(수량) 수정
    updateEtcInQty(params) {
        return API.request.post(encodeURI(`${this.URL}/updateetcinqty`), params);
    }
    

    /* 삭제 */
    //기타입고 삭제
    deleteEtcIn(params) {
        return API.request.post(encodeURI(`${this.URL}/deleteEtcIn`), params);
    }
    
    //바코드 발행
    barcodeCreate(param) {
        return API.request.post(`${this.URL}/barcodecreate`, param);
    }

    //입고등록
    inInsert(params) {
        return API.request.post(`${this.URL}/ininsert`, params);
    }

    //입고취소
    inCancle(params) {
        return API.request.post(`${this.URL}/incancle`, params);
    }

    //위치변경
    locationMoveChange(params) {
        return API.request.post(`${this.URL}/locationmovechange`, params);
    }

}

export default new EtcInRepository();