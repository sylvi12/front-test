import { observable, action } from 'mobx';
import moment from 'moment';
import 'moment/locale/ko';

import Alert from 'components/override/alert/Alert';
import GridUtil from 'components/override/grid/utils/GridUtil';
import ObjectUtility from 'utils/object/ObjectUtility';

import CommonStore from 'modules/pages/common/service/CommonStore';
import BaseStore from 'utils/base/BaseStore';
import Validator from 'components/override/utils/Validator';

import ItemOptionModel from 'components/codehelper/model/ItemOptionModel';
import EmployeeOptionModel from 'components/codehelper/model/EmployeeOptionModel';

import EtcInRepository from 'modules/pages/inout/etcIn/service/EtcInRepository';


import WorkorderRepository from 'modules/pages/order/workorder/service/WorkorderRepository';
import BarcodeHistoryRepository from "modules/pages/inventory/barcodeHistory/service/BarcodeHistoryRepository";



class EtcInStore extends BaseStore {

    constructor() {
        super()
        this.itemHelperOption_Qty.barcodeYn = 'N';
        this.itemHelperOption.barcodeYn = 'Y';

        this.setInitialState();
    }

    etnInInsertValidator = new Validator([
        {
            field: 'itemCd',
            method: 'isEmpty',
            message: '제품',
        },
        {
            field: 'inConfirmUserNo',
            method: 'isEmpty',
            message: '입고자',
        }
    ])

    etnInQtyInsertValidator = new Validator([
        {
            field: 'itemCd',
            method: 'isEmpty',
            message: '제품',
        }
    ])

    barcodeValidator = new Validator([
        {
            field: 'barcodeModalLotNo',
            method: 'isEmpty',
            message: 'LOT NO',
        },
        {
            field: 'barcodeModalPackQty',
            method: 'isEmpty',
            message: '포장수량',
        },
        {
            field: 'minSeq',
            method: 'isEmpty',
            message: '일련번호',
        },
        {
            field: 'maxSeq',
            method: 'isEmpty',
            message: '일련번호',
        }
    ]);

    //ItemHelper(바코드) 변수
    @observable itemHelperOption = new ItemOptionModel().optionModel; //ItemHelper Option
    @observable selectedItem = {}; //ItemHelper ReturnValue

    //ItemHelper(수량) 변수
    @observable itemHelperOption_Qty = new ItemOptionModel().optionModel; //ItemHelper Option
    @observable selectedItem_Qty = {}; //ItemHelper ReturnValue

    //EmpHelper 변수
    @observable empHelperOption = new EmployeeOptionModel().optionModel; //ItemHelper Option
    @observable selectedEmp = {}; //ItemHelper ReturnValue

    // LocationHelpler 변수
    @observable isLocationOpen = false;
    @observable locationBuId = '';

    //검색조건 변수
    @observable buId = '';//CommonStore.buId;
    // @observable buList = CommonStore.buList;
    // @observable etcDt = moment().format('YYYYMMDD');
    @observable startDt = moment().add(-7, 'days').format('YYYYMMDD');
    @observable endDt = moment().format('YYYYMMDD');
    @observable itemCdNm = '';
    @observable alphaCheck = false;

    // BarcodeModal 변수
    @observable barcodeModalIsOpen = false;
    @observable barcodeModalOrderNo = 0;
    @observable barcodeModalLotNo = "";
    barcodeModalItemId = 0;
    @observable barcodeModalItemCd = '';
    @observable barcodeModalItemNm = '';
    addDate = 0;
    @observable barcodeModalProdDt = '';
    @observable barcodeModalExpiryDt = '';
    @observable barcodeModalProdUomQty = 0;
    @observable barcodeModalProdUom = '';
    @observable barcodeModalPackQty = 0;
    @observable barcodeModalPackUom = '';
    @observable barcodeModalProdQty = 0;
    @observable rackTypeHide = true;
    @observable barcodeModalRackType = "";
    prodEmpNo = '';
    @observable prodEmpNm = '';
    packerEmpNo = '';
    @observable packerEmpNm = '';
    @observable minSeq = 1;
    @observable maxSeq = 1;
    @observable barcodeModalMemo = '';

    //기타입고(바코드) 등록 변수
    @observable inoutEtcDt_b = moment().format('YYYYMMDD');
    @observable ledgerTypeList = [];
    @observable ledgerType = 'C11';
    @observable remark = '';
    itemId;
    itemCd;
    itemNm;
    inConfirmUserNo;
    empNo;

    //기타입고(수량) 등록 변수
    @observable inoutEtcDt_q = moment().format('YYYYMMDD');
    @observable etcInQtyModalOpen = false;
    @observable prodQty_q = 0;
    // @observable itemUom= '';
    itemUom = 'KG';

    //기타입고(수량) 수정 변수
    @observable etcInQtyUpdateModalOpen = false;

    //기타입고(바코드) 수정 변수
    @observable etcInUpdateModalOpen = false;
    //바코드 추가 변수
    @observable barcodeAddModalOpen = false;

    //기타입고 Modal변수
    @observable etcInModalOpen = false;

    //기타입고 그리드 변수
    etcInGridApi = undefined;
    etcInList = [];

    //바코드발행 Modal변수
    @observable barcodeModalOpen = false;

    //바코드내역 그리드 변수
    barcodeGridApi = undefined;
    @observable barcodeList = [];
    @observable pinnedBottomRowDataAtEtcBarcodeGrid = [];

    //바코드 추가 검색조건 변수
    @observable prodDt = '';
    @observable itemCd = '';
    @observable barcode = '';

    //바코드추가 그리드 변수
    barcodeModalGridApi = undefined;
    @observable barcodeModalList = [];

    //로그인 유저 정보
    @observable userInfo = {};

    //기타 변수
    key = '';
    add = [];
    barcodeYn = 'Y';

    //공장 불러오기
    @action.bound
    getBu = async () => {
        this.buId = CommonStore.buId;
        this.alphaCheck = true;
    }

    //ItemHelper 항목 선택후
    @action.bound
    itemHelperResult = (result) => {

        if (result) {
            this.itemId = this.selectedItem.itemId;
            this.itemCd = this.selectedItem.itemCd;
            this.itemNm = this.selectedItem.itemNm;
        }
    }

    //ItemHelper 항목 선택전
    @action.bound
    handleHelperOpen = () => {
        this.itemHelperOption.buId = this.buId;
        this.itemHelperOption_Qty.buId = this.buId;
    }

    //ItemHelper(수량)) 항목 선택후
    @action.bound
    itemHelperResult_Qty = (result) => {

        if (result) {
            this.itemId = this.selectedItem_Qty.itemId;
            this.itemCd = this.selectedItem_Qty.itemCd;
            this.itemNm = this.selectedItem_Qty.itemNm;
            // this.itemUom = this.selectedItem_Qty.itemUom;
        }
    }

    //EmpHelper 항목 선택후
    empHelperResult = (result) => {

        if (result) {
            this.inConfirmUserNo = this.selectedEmp.empNo;

        }
    }

    // 유통기한 계산하기
    @action.bound
    makeExpireDate = () => {

        if (this.addDate != '') {
            this.addDate = Number(this.addDate);
            this.barcodeModalExpiryDt = this.barcodeModalProdDt.getAddDays(this.addDate - 1).format("yyyy-MM-dd");
        }
        this.barcodeModalLotNo = this.barcodeModalProdDt.replace(/-/gi, '');
    }

    // 생산량 계산하기
    @action.bound
    calculateProdQty = () => {
        if (this.barcodeModalProdQty) {

        }
        this.barcodeModalProdQty = this.barcodeModalProdUomQty * this.barcodeModalPackQty;
    }

    //데이터 체인지
    @action.bound
    handleChange = (data) => {
        console.log(data)
        if (data.id == 'selectedItem_Qty') {
            if (data.value.itemNm == '') {
                this.itemCd = '';
            }
        }

        if (data.id == 'selectedItem') {
            if (data.value.itemNm == '') {
                this.itemCd = '';
            }
        }

        if (data.id == 'selectedEmp') {
            if (data.value.loginNm == '') {
                this.inConfirmUserNo = '';
            }
        }


        if (data.id == 'remark') {
            if (this.remark.length > 499) {
                data.value = data.value.substring(0, 500);
            }
        }
        this[data.id] = data.value;
    }

    //검색조건 - 조회 클릭
    handleSearchClick = async () => {

        const searchData = {
            "buId": this.buId
            , "startDt": this.startDt
            , "endDt": this.endDt
            , "itemCdNm": this.itemCdNm
        }
        const { data, status } = await EtcInRepository.getEtcIn(searchData);

        if (status == 200) {

            this.etcInList = data.data;
            this.etcInGridApi.setRowData(this.etcInList);
            this.barcodeGridApi.setRowData([]);

        } else {
            Alert.meg(data.msg);
        }
    }

    // 검색조건 공장 선택 - 데이터 변경
    @action.bound
    buSelectionChanged = async () => {
        //this.handleSearchClick();
    }

    //기타입고 - 등록(바코드) 클릭
    @action.bound
    openEtcInBarcodeModal = async () => {

        let allBu = this.buId.split(',');

        if (allBu.length > 1) {
            Alert.meg("선택된 공장이 없습니다.");
            return;
        }

        this.userInfo = CommonStore.userInfo;
        this.itemHelperOption.cdNmReadOnly = false;
        this.itemHelperOption.searchVisible = true;
        const all = CommonStore.codeObject["MFD002"]
        this.ledgerTypeList = all.filter(x => x.desc5 == 'Y');
        this.ledgerType = 'C11';
        this.inoutEtcDt_b = moment().format('YYYYMMDD');
        this.selectedEmp.loginNm = this.userInfo.loginNm;

        this.selectedEmp.empNo = this.userInfo.loginId;
        this.itemId = 0;
        this.itemCd = '';
        this.itemNm = '';
        this.remark = '';
        this.selectedItem = {}
        this.inConfirmUserNo = this.userInfo.loginId;

        this.etcInModalOpen = true;
    }

    //기타입고 - 등록(수량) 클릭
    @action.bound
    openEtcInQtyModal = async () => {

        let allBu = this.buId.split(',');

        if (allBu.length > 1) {
            Alert.meg("선택된 공장이 없습니다.");
            return;
        }

        this.userInfo = CommonStore.userInfo;

        this.itemHelperOption_Qty.cdNmReadOnly = false;
        this.itemHelperOption_Qty.searchVisible = true;

        const all = CommonStore.codeObject["MFD002"]
        this.ledgerTypeList = all.filter(x => x.desc5 == 'Y');
        this.ledgerType = 'C11';
        this.inoutEtcDt_q = moment().format('YYYYMMDD');
        // this.selectedEmp.loginNm = this.userInfo.loginNm;
        // this.selectedEmp.empNo = this.userInfo.loginId;
        this.itemId = 0;
        this.itemCd = '';
        this.itemNm = '';
        this.remark = '';
        this.selectedItem_Qty = {};
        this.prodQty_q = 0;
        // this.itemUom = '';

        this.etcInQtyModalOpen = true;
    }

    //기타입고(바코드) - 저장 클릭
    @action.bound
    etcInSave = async () => {

        const validation = this.etnInInsertValidator.validate(this);

        if (!validation.isValid) {
            return;
        }

        this.startDt = this.startDt <= this.inoutEtcDt_b ? this.startDt : this.inoutEtcDt_b;
        this.endDt = this.inoutEtcDt_b;

        const saveData = {
            "inoutEtcDt": this.inoutEtcDt_b
            , "ledgerType": this.ledgerType
            , "itemId": this.itemId
            , "processQty": 0
            , "processUom": ''
            , "buId": this.buId
            , "inConfirmUserNo": this.inConfirmUserNo
            , "remark": this.remark
            , "startDt": this.startDt
            , "endDt": this.endDt
        }

        const params = JSON.stringify({ param: saveData });
        const { data, status } = await EtcInRepository.saveEtcIn(params);

        if (status == 200) {
            this.etcInList = data.data;
            this.etcInGridApi.setRowData(this.etcInList);

            Alert.meg(data.msg);

            GridUtil.setFocusByRowIndex(this.etcInGridApi, this.etcInList.length - 1);
            this.getBarcode();
        } else {
            Alert.meg(data.msg);
        }
        this.etcInModalClose();
    }

    //기타입고(바코드)수정 - 저장 클릭
    @action.bound
    etcInUpdate = async () => {

        const selectedRow = GridUtil.getSelectedRowData(this.etcInGridApi);

        this.startDt = this.startDt <= this.inoutEtcDt_b ? this.startDt : this.inoutEtcDt_b;
        this.endDt = this.inoutEtcDt_b;

        const saveData = {
            "buId": selectedRow.buId
            , "orderNo": selectedRow.orderNo
            , "inoutEtcDt": this.inoutEtcDt_b
            , "ledgerType": this.ledgerType
            , "remark": this.remark
            , "itemId": this.itemId
            , "processQty": 0
            , "inConfirmUserNo": this.inConfirmUserNo
            , "startDt": this.startDt
            , "endDt": this.endDt
        }

        const params = JSON.stringify({ param: saveData });
        const { data, status } = await EtcInRepository.updateEtcIn(params);

        if (status == 200) {
            this.etcInList = data.data;
            this.etcInGridApi.setRowData(this.etcInList);

            const index = this.etcInList.findIndex(x => x.orderNo == selectedRow.orderNo);
            GridUtil.setFocusByRowIndex(this.etcInGridApi, index);

            Alert.meg(data.msg);

        } else {
            Alert.meg(data.msg);
        }
        this.etcInUpdateModalClose();

    }

    //기타입고(수량) - 저장 클릭
    @action.bound
    etcInSave_Qty = async () => {

        const validation = this.etnInQtyInsertValidator.validate(this);
        if (!validation.isValid) {
            return;
        }

        this.startDt = this.startDt <= this.inoutEtcDt_q ? this.startDt : this.inoutEtcDt_q;
        this.endDt = this.inoutEtcDt_q;

        const saveData = {
            "inoutEtcDt": this.inoutEtcDt_q
            , "ledgerType": this.ledgerType
            , "itemId": this.itemId
            , "processQty": this.prodQty_q
            , "processUom": this.itemUom
            , "buId": this.buId
            , "inConfirmUserNo": ''
            , "remark": this.remark
            , "startDt": this.startDt
            , "endDt": this.endDt
        }

        const params = JSON.stringify({ param: saveData });

        const { data, status } = await EtcInRepository.saveEtcInQty(params);
        if (status == 200) {
            this.etcInList = data.data;
            this.etcInGridApi.setRowData(this.etcInList);
            // this.barcodeGridApi.setRowData();
            Alert.meg(data.msg);

            GridUtil.setFocusByRowIndex(this.etcInGridApi, this.etcInList.length - 1);
            this.getBarcode();
        } else {
            Alert.meg(data.data.sqlMessage);
        }
        this.etcInQtyModalClose();
    }

    //기타입고(수량) 수정 - 저장 클릭
    @action.bound
    etcInUpdate_Qty = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.etcInGridApi);

        this.startDt = this.startDt <= this.inoutEtcDt_q ? this.startDt : this.inoutEtcDt_q;
        this.endDt = this.inoutEtcDt_q;

        const saveData = {
            "buId": selectedRow.buId
            , "orderNo": selectedRow.orderNo
            , "inoutEtcDt": this.inoutEtcDt_q
            , "ledgerType": this.ledgerType
            , "itemId": this.itemId
            , "processQty": this.prodQty_q
            , "inConfirmUserNo": ''
            , "remark": this.remark
            , "startDt": this.startDt
            , "endDt": this.endDt
        }

        const params = JSON.stringify({ param: saveData });

        const { data, status } = await EtcInRepository.updateEtcInQty(params);

        if (status == 200) {
            this.etcInList = data.data;

            this.etcInGridApi.setRowData(this.etcInList);

            Alert.meg(data.msg);

            const index = this.etcInList.findIndex(x => x.orderNo == selectedRow.orderNo);
            GridUtil.setFocusByRowIndex(this.etcInGridApi, index);

        } else {
            Alert.meg(data.msg);
        }

        this.etcInQtyUpdateModalClose();
        this.getBarcode();
    }

    //기타입고 등록(수량) - 닫기 클릭
    @action.bound
    etcInQtyModalClose = () => {
        this.etcInQtyModalOpen = false;
    }

    //기타입고 등록(바코드) - 닫기 클릭
    @action.bound
    etcInModalClose = async () => {
        this.etcInModalOpen = false;
    }

    //기타입고 ROW 더블 클릭 - 수정 모달창 열기
    @action.bound
    openModalEdit = () => {

        const all = CommonStore.codeObject["MFD002"]
        this.ledgerTypeList = all.filter(x => x.desc5 == 'Y');

        const selectedRow = GridUtil.getSelectedRowData(this.etcInGridApi);

        if (selectedRow.barcodeYn == 'Y') {

            for (let i = 0; i < this.barcodeList.length; i++) {

                if (this.barcodeList[i].invenQty > 0) {
                    Alert.meg("입고된 바코드가 있습니다.");
                    return;
                }
            }

            this.itemHelperOption.cdNmReadOnly = true;
            this.itemHelperOption.searchVisible = false;

            this.ledgerType = selectedRow.ledgerType;
            this.selectedItem.itemCd = selectedRow.itemCd;
            this.selectedItem.itemNm = selectedRow.itemNm;

            this.selectedEmp.empNo = selectedRow.inConfirmUserNo;
            this.selectedEmp.loginNm = selectedRow.inConfirmUserNm;

            this.inoutEtcDt_b = moment(selectedRow.inoutEtcDt).format('YYYYMMDD');
            this.remark = selectedRow.remark;

            this.itemCd = selectedRow.itemCd;
            this.itemNm = selectedRow.itemNm;
            this.itemId = selectedRow.itemId;

            this.inConfirmUserNo = selectedRow.inConfirmUserNo;

            this.etcInUpdateModalOpen = true;

        } else {

            this.itemHelperOption_Qty.cdNmReadOnly = true;
            this.itemHelperOption_Qty.searchVisible = false;

            this.ledgerType = selectedRow.ledgerType;
            // this.prodQty_q = selectedRow.invenQty
            this.selectedItem_Qty.itemCd = selectedRow.itemCd;
            this.selectedItem_Qty.itemNm = selectedRow.itemNm;
            this.itemCd = selectedRow.itemCd;
            this.itemNm = selectedRow.itemNm;
            this.itemId = selectedRow.itemId;
            // this.itemUom = selectedRow.productionUom;

            this.prodQty_q = this.barcodeList[0].processQty;
            this.inoutEtcDt_q = moment(selectedRow.inoutEtcDt).format('YYYYMMDD');
            this.remark = selectedRow.remark;
            this.etcInQtyUpdateModalOpen = true;
        }
    }

    //기타입고(수량) 수정 닫기
    @action.bound
    etcInQtyUpdateModalClose = () => {
        this.etcInQtyUpdateModalOpen = false;
    }

    //기타입고(바코드) 수정 닫기
    @action.bound
    etcInUpdateModalClose = () => {
        this.etcInUpdateModalOpen = false;
    }

    //기타입고 그리드 ROW 클릭 
    @action.bound
    getBarcode = async () => {

        const selectedRow = GridUtil.getSelectedRowData(this.etcInGridApi);

        if (selectedRow) {
            this.orderNo = selectedRow.orderNo;
            this.ledgerType = selectedRow.ledgerType;
            this.barcodeYn = selectedRow.barcodeYn;

            const searchData = {
                "orderNo": selectedRow.orderNo
                , "barcodeYn": selectedRow.barcodeYn
                , "itemId": selectedRow.itemId
                , "itemCd": selectedRow.itemCd
            }

            let { data, status } = await EtcInRepository.getBarcodeHis(searchData);

            if (status == 200) {
                this.barcodeList = data.data;
                this.barcodeGridApi.setRowData(this.barcodeList);
                this.barcodeGridApi.setColumnDefs(this.createBarcodeColumnDefs());
                if (this.barcodeList.length != 0) {
                    this.minSeq = this.barcodeList.length + 1;
                    this.maxSeq = this.barcodeList.length + 1;
                } else {
                    this.minSeq = 1;
                    this.maxSeq = 1;
                }
            }
        }

    }

    //기타입고 삭제
    @action.bound
    etcInDelete = async () => {
        const selectedRows = this.etcInGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg("선택된 항목이 없습니다.");
            return;
        }

        for (let i = 0; i < this.barcodeList.length; i++) {
            if (this.barcodeList[i].invenQty > 0) {
                Alert.meg("입고된 바코드가 존재합니다");
                return;
            }
        }

        const searchData = {
            "buId": this.buId
            , "startDt": this.startDt
            , "endDt": this.endDt
        }

        const value = await Alert.confirm("삭제하시겠습니까?").then(result => { return result; });

        if (value) {

            const params = JSON.stringify({ param1: selectedRows, param2: searchData });

            const { data, status } = await EtcInRepository.deleteEtcIn(params);

            if (status == 200) {
                this.etcInList = data.data;
                this.etcInGridApi.setRowData(this.etcInList);
                this.barcodeGridApi.setRowData([]);
                Alert.meg(data.msg);

            } else {
                Alert.meg(data.msg);
            }
        }
        else {
            return;
        }
    }

    //기타입고 - 바코드발행 클릭 
    @action.bound
    createBarcode = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.etcInGridApi);

        if (!selectedRow || selectedRow.length == 0) {
            Alert.meg("선택된 항목이 없습니다.");
            return;
        }
        if (selectedRow.barcodeYn == 'Y') {

            this.barcodeModalOrderNo = selectedRow.orderNo;
            this.barcodeModalLotNo = moment().format('YYYYMMDD').replace(/-/gi, '');
            this.barcodeModalItemId = selectedRow.itemId;
            this.barcodeModalItemCd = selectedRow.itemCd;
            this.barcodeModalItemNm = selectedRow.itemNm;
            this.barcodeModalProdDt = moment().format('YYYYMMDD');
            this.addDate = selectedRow.expiryDay;
            this.makeExpireDate();

            this.barcodeModalProdUomQty = selectedRow.productionConvtQty;
            // this.barcodeModalProdUom = selectedRow.itemUom;
            this.barcodeModalProdUom = selectedRow.productionUom;
            this.barcodeModalPackQty = selectedRow.palletConvtQty;
            this.barcodeModalPackUom = selectedRow.packUom;
            this.barcodeModalProdQty = selectedRow.palletConvtQty * selectedRow.productionConvtQty;
            this.rackTypeHide = true;
            this.prodEmpNo = '';
            this.prodEmpNm = '';
            this.packerEmpNo = '';
            this.packerEmpNm = '';
            this.barcodeModalMemo = "";

            // this.printPage = 1;
            if (this.buId == "13FA") {
                this.rackTypeHide = false;
                this.barcodeModalRackType = "1";
            }
            this.barcodeModalOpen = true;
        } else {
            Alert.meg('바코드 발행 할 수 없는 제품입니다.');
        }

    }

    //바코드 발행
    @action.bound
    barcodeCreate = async () => {


        const validation = this.barcodeValidator.validate(this);
        if (!validation.isValid) {
            return;
        }

        if (this.barcodeModalPackQty <= 0 || Number.isInteger(this.barcodeModalPackQty) == false) {
            Alert.meg("유효하지 않은 포장수량입니다.");
            return;
        }
        if (this.maxSeq < this.minSeq) {
            Alert.meg("일련번호 범위를 확인해주세요.");
            return;
        }

        if ((this.minSeq == null || this.maxSeq == null)
            || (this.minSeq == 0 || this.maxSeq == 0)) {
            Alert.meg("일련번호는 0이 될 수 없습니다.");
            return;
        }

        if (ObjectUtility.convertToDateFromYYYYMMDD(this.barcodeModalExpiryDt) <= ObjectUtility.convertToDateFromYYYYMMDD(this.barcodeModalProdDt)) {
            Alert.meg("유통기한이 생산일자보다 빠릅니다.");
            return;
        }

        const checkData = {
            "orderNo": this.barcodeModalOrderNo
            , "minSeq": this.minSeq
            , "maxSeq": this.maxSeq
        }



        const { data, status } = await WorkorderRepository.barcodeCheck(checkData);
        if (data.data.length > 0) {
            const value = await Alert.confirm('이미 재고로 잡힌<br> 바코드가 있습니다.<br>그래도 재발행 하시겠습니까?').then(result => { return result; });
            if (value) {
                const saveData = {
                    "orderNo": this.barcodeModalOrderNo
                    , "lotNo": this.barcodeModalLotNo
                    , "itemId": this.barcodeModalItemId
                    , "prodDt": this.barcodeModalProdDt
                    , "expiryDt": this.barcodeModalExpiryDt
                    , "packUom": this.barcodeModalPackUom
                    , "packQty": this.barcodeModalPackQty
                    , "prodUom": this.barcodeModalProdUom
                    , "prodUomQty": this.barcodeModalProdUomQty
                    , "prodQty": this.barcodeModalProdQty
                    , "rackType": this.barcodeModalRackType
                    , "minSeq": this.minSeq
                    , "maxSeq": this.maxSeq
                    , "memo": this.barcodeModalMemo
                    , "prodEmpNo": this.prodEmpNo
                    , "prodEmpNm": this.prodEmpNm
                    , "packerEmpNo": this.packerEmpNo
                    , "packerEmpNm": this.packerEmpNm
                }
                const { data, status } = await EtcInRepository.barcodeCreate(saveData);
                if (data.success == false) {
                    Alert.meg(data.msg);
                } else {
                    Alert.meg("발행되었습니다.");
                    this.barcodeModalClose();
                    this.getBarcode();
                }
            } else {
                return;
            }
        } else {
            const saveData = {
                "orderNo": this.barcodeModalOrderNo
                , "lotNo": this.barcodeModalLotNo
                , "itemId": this.barcodeModalItemId
                , "prodDt": this.barcodeModalProdDt
                , "expiryDt": this.barcodeModalExpiryDt
                , "packUom": this.barcodeModalPackUom
                , "packQty": this.barcodeModalPackQty
                , "prodUom": this.barcodeModalProdUom
                , "prodUomQty": this.barcodeModalProdUomQty
                , "prodQty": this.barcodeModalProdQty
                , "rackType": this.barcodeModalRackType
                , "minSeq": this.minSeq
                , "maxSeq": this.maxSeq
                , "memo": this.barcodeModalMemo
                , "prodEmpNo": this.prodEmpNo
                , "prodEmpNm": this.prodEmpNm
                , "packerEmpNo": this.packerEmpNo
                , "packerEmpNm": this.packerEmpNm
            }
            const { data, status } = await EtcInRepository.barcodeCreate(saveData);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                Alert.meg("발행되었습니다.");
                this.barcodeModalClose();
                this.getBarcode();
            }
        }

    }

    //바코드 발행 닫기
    @action.bound
    barcodeModalClose = () => {
        this.barcodeModalOpen = false;
    }


    //위치변경
    @action.bound
    locationChange = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.etcInGridApi);

        this.locationSaveData = {};

        if (!selectedRow || selectedRow.length == 0) {
            Alert.meg("선택된 항목이 없습니다.");
            return;
        }
        if (selectedRow.barcodeYn == 'Y') {
            const selectedRows = GridUtil.getGridCheckArray(this.barcodeGridApi);

            if (!selectedRows || selectedRows.length == 0) {
                Alert.meg("바코드를 선택 해주세요.");
                return;
            }

            let cnt = 0;
            for (let i = 0; i < selectedRows.length; i++) {
                if (selectedRows[i].locNm == null) {
                    cnt++;
                }
            }
            if (cnt > 0) {
                Alert.meg("재고로 등록되지 않은 바코드가 있습니다.");
                return;
            }

            this.locationBuId = this.buId;
            this.locationSaveData = selectedRows;
            this.isLocationOpen = true;

        } else {
            Alert.meg('위치변경 할 수 없는 제품입니다.');
        }
    }
    //위치변경 선택시
    @action.bound
    locationHelperResult = async (result) => {

        if (result == null || result == undefined) {
            return;
        }

        const saveData = {
            "locId": result.locId
            , "orderNo": this.locationSaveData[0].orderNo
        }

        const params = JSON.stringify({ param1: this.locationSaveData, param2: saveData });

        const { data, status } = await EtcInRepository.locationMoveChange(params);
        if (status == 200) {

            Alert.meg(data.msg);
        } else {
            Alert.meg(data.msg);
        }
        this.getBarcode();
    }

    //바코드사용
    @action.bound
    barcodeUse = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.etcInGridApi);

        if (!selectedRow || selectedRow.length == 0) {
            Alert.meg("선택된 항목이 없습니다.");
            return;
        }
        if (selectedRow.barcodeYn == 'Y') {
            const selectedRows = GridUtil.getGridCheckArray(this.barcodeGridApi);
            if (!selectedRows || selectedRows.length == 0) {
                Alert.meg("바코드를 선택 해주세요.");
                return;
            }
            let useStopCnt = 0;
            for (let i = 0; i < selectedRows.length; i++) {

                if (selectedRows[i].useYn == 'Y') {
                    useStopCnt++;
                }
            }

            if (useStopCnt > 0) {
                Alert.meg('사용가능한 바코드가 있습니다.');
                return;
            } else {
                const { data, status } = await BarcodeHistoryRepository.setBarcodeYnUse(selectedRows);
                if (data.success && status == 200) {
                    Alert.meg(data.msg);

                } else {
                    Alert.meg(data.sqlMessage);
                }
            }
            this.getBarcode();
        } else {
            Alert.meg('바코드사용 할 수 없는 제품입니다.');
        }
    }

    //바코드중지
    @action.bound
    barcodeStop = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.etcInGridApi);

        if (!selectedRow || selectedRow.length == 0) {
            Alert.meg("선택된 항목이 없습니다.");
            return;
        }
        if (selectedRow.barcodeYn == 'Y') {
            const selectedRows = GridUtil.getGridCheckArray(this.barcodeGridApi);
            if (!selectedRows || selectedRows.length == 0) {
                Alert.meg("바코드를 선택 해주세요.");
                return;
            }

            for (let k = 0; k < selectedRows.length; k++) {
                if (selectedRows[k].invenQty > 1) {
                    Alert.meg("재고가 입고된 바코드가 있습니다.");
                    return;
                }
            }

            let useStopCnt = 0;
            for (let i = 0; i < selectedRows.length; i++) {

                if (selectedRows[i].useYn == 'N') {
                    useStopCnt++;
                }
            }



            if (useStopCnt > 0) {
                Alert.meg('사용중지된 바코드가 있습니다.');
                return;
            } else {
                const { data, status } = await BarcodeHistoryRepository.setBarcodeYnCancel(selectedRows);
                if (status == 200 && data.sqlMessage == null) {

                    Alert.meg(data.msg);

                } else {

                    Alert.meg(data.data.sqlMessage);
                }
            }
            this.getBarcode();
        } else {
            Alert.meg('바코드중지 할 수 없는 제품입니다.');
        }
    }

    //입고등록
    @action.bound
    inInsert = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.etcInGridApi);

        if (!selectedRow || selectedRow.length == 0) {
            Alert.meg("선택된 항목이 없습니다.");
            return;
        }
        // if(selectedRow.barcodeYn == 'Y') {

        const selectedRows = GridUtil.getGridCheckArray(this.barcodeGridApi);
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg("선택된 바코드가 없습니다.");
            return;
        }

        let useCnt = 0;
        for (let k = 0; k < selectedRows.length; k++) {
            if (selectedRows[k].useYn == 'N') {
                useCnt++;
            }
        }

        if (useCnt > 0) {
            Alert.meg("사용중지된 바코드가 있습니다.");
            return;
        }

        // if (selectedRow.barcodeYn == 'Y') {
        //     let cnt = 0;
        //     for (let i = 0; i < selectedRows.length; i++) {
        //         if (selectedRows[i].invenQty > 0) {
        //             cnt++;
        //         }
        //     }
        //     if (cnt > 0) {
        //         Alert.meg('재고가 입고된 바코드가 있습니다.');
        //         return;
        //     }
        // }

        const invenYItem = selectedRows.find(x => x.invenYn == 'Y' && x.invenQty > 0);
        if (invenYItem) {
            Alert.meg(`재고가 입고된 바코드가 있습니다.<br />입고등록을 할 수 없습니다.`);
            return;
        }

        const saveData = {
            "ledgerType": selectedRow.ledgerType
            , "orderNo": selectedRow.orderNo
        }

        const params = JSON.stringify({ param1: selectedRows, param2: saveData });
        const { data, status } = await EtcInRepository.inInsert(params);

        if (status == 200) {

            Alert.meg(data.msg);

        } else {
            console.log(data)
            Alert.meg(data.sqlMessage);
        }

        this.getBarcode();
        // } else {
        //     Alert.meg('입고등록 할 수 없는 제품입니다.');
        // }
    }

    //입고취소
    @action.bound
    inCancle = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.etcInGridApi);

        if (!selectedRow || selectedRow.length == 0) {
            Alert.meg("선택된 항목이 없습니다.");
            return;
        }
        // if(selectedRow.barcodeYn == 'Y') {
        const selectedRows = GridUtil.getGridCheckArray(this.barcodeGridApi);
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg("선택된 바코드가 없습니다.");
            return;
        }

        if (selectedRow.barcodeYn == 'Y') {
            let cnt = 0;
            for (let i = 0; i < selectedRows.length; i++) {
                if (selectedRows[i].invenQty < 1) {
                    cnt++;
                }
            }
            if (cnt > 0) {
                Alert.meg('입고되지 않은 바코드가 있습니다.');
                return;
            }
        }

        const saveData = {
            "ledgerType": selectedRow.ledgerType
            , "orderNo": selectedRow.orderNo
        }

        const params = JSON.stringify({ param1: selectedRows, param2: saveData });

        const { data, status } = await EtcInRepository.inCancle(params);

        if (status == 200) {
            Alert.meg(data.msg);

        } else {
            Alert.meg(data.msg);
        }
        this.getBarcode();
        // } else {
        //     Alert.meg('입고취소 할 수 없는 제품입니다.');
        // }
    }

    // 기타입고 그리드
    @action.bound
    setEtcInGridApi = async (gridApi) => {

        this.etcInGridApi = gridApi;

        const columnDefs = [
            {
                headerName: "지시번호"
                , field: "orderNo"
                , width: 70
                , customCellClass: 'cell_align_center'
            },
            {
                headerName: "입고일자"
                , field: "inoutEtcDt"
                , width: 90
                , customCellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "입고유형"
                , field: "ledgerType"
                , width: 80
                , cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.getCodeNm(params, 'MFD002')
            },
            {
                headerName: "제품코드"
                , field: "itemCd"
                , width: 80
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "제품명"
                , field: "itemNm"
                , width: 190
                , cellClass: 'cell_align_left'
            },
            {
                headerName: "입고자"
                , field: "inConfirmUserNm"
                , width: 80
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "등록자"
                , field: "inputEmpNm"
                , width: 80
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "등록일시"
                , field: "inputDtm"
                , width: 120
                , cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm')
            },
            {
                headerName: "비고"
                , field: "remark"
                , width: 200
            }
        ];
        this.etcInGridApi.setColumnDefs(columnDefs);
    }

    // 바코드내역 그리드
    @action.bound
    setBarcodeGridApi = async (barcodeGridApi) => {

        this.barcodeGridApi = barcodeGridApi;

        this.pinnedBottomRowDataAtEtcBarcodeGrid = [
            {
                packSeq: '합계',
                invenQty: 0,
            }
        ];

        this.barcodeGridApi.setColumnDefs(this.createBarcodeColumnDefs());
    }

    createBarcodeColumnDefs = () => {

        const barcodeN = this.barcodeYn == "N" ? true : false;

        return [
            {
                headerName: ""
                , field: "check"
                , width: 29
                , checkboxSelection: true
                , headerCheckboxSelection: true
            },
            {
                headerName: "순번"
                , field: "packSeq"
                , width: 70
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
            },
            {
                headerName: "바코드번호"
                , field: "barcode"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "LOT"
                , field: "lotNo"
                , width: 90
                , cellClass: 'cell_align_center'
                , hide: barcodeN
            },
            {
                headerName: "생산일자"
                , field: "prodDt"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
                , hide: barcodeN
            },
            {
                headerName: "유통기한"
                , field: "expiryDt"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
                , hide: barcodeN
            },
            {
                headerName: "사용여부"
                , field: "useYn"
                , width: 70
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "지시량"
                , field: "inoutQty"
                , width: 90
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
                , pinnedRowCellRenderer: "customPinnedRowRenderer"
                , pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
            },
            {
                headerName: "입고량"
                , field: "invenQty"
                , width: 90
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
                , pinnedRowCellRenderer: "customPinnedRowRenderer"
                , pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
            },
            {
                headerName: "단위"
                , field: "prodUom"
                , width: 65
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "위치"
                , field: "locNm"
                , width: 120
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "처리시간"
                , field: "inputDtm"
                , width: 105
                , valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm')
            },
            {
                headerName: "처리자"
                , field: "inputEmpNm"
                , width: 90
                , cellClass: 'cell_align_center'
            },
        ];

    }
    // 바코드 추가 그리드
    @action.bound
    setbarcodeModalGridApi = async (gridApi) => {
        this.barcodeModalGridApi = gridApi;

        const columnDefs = [
            {
                headerName: ""
                , field: "check"
                , width: 35
                , checkboxSelection: true
                , headerCheckboxSelection: true
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "순번"
                , field: "seq"
                , width: 70
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
            },
            {
                headerName: "바코드번호"
                , field: "barcode"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "상태"
                , field: "lotQualityNm"
                , width: 70
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "제품코드"
                , field: "itemCd"
                , width: 90
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "LOT"
                , field: "lotNo"
                , width: 90
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "생산일자"
                , field: "prodDt"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "유통기한"
                , field: "expiryDt"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "수치"
                , field: "processQty"
                , width: 100
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
            },
            {
                headerName: "위치"
                , field: "locNm"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "처리시간"
                , field: "inputDtm"
                , width: 120
                , cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm')
            },
            {
                headerName: "작업자"
                , field: "inputEmpNm"
                , width: 90
                , cellClass: 'cell_align_center'
            }
        ];
        this.barcodeModalGridApi.setColumnDefs(columnDefs);
    }

}

export default new EtcInStore();