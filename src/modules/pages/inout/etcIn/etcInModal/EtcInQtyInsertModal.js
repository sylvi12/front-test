import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import STextArea from 'components/atoms/input/STextArea';
import SButton from 'components/atoms/button/SButton';

import ItemHelper from 'components/codehelper/ItemHelper';
import SNumericInput from 'components/override/numericinput/SNumericInput';

@inject(stores => ({
    ledgerType: stores.etcInStore.ledgerType
    , ledgerTypeList: stores.etcInStore.ledgerTypeList
    , inoutEtcDt_q: stores.etcInStore.inoutEtcDt_q
    , remark: stores.etcInStore.remark
    , handleChange: stores.etcInStore.handleChange
    //, etcInModalClose: stores.etcInStore.etcInModalClose
    , etcInSave_Qty: stores.etcInStore.etcInSave_Qty
    , selectedItem_Qty: stores.etcInStore.selectedItem_Qty
    , itemHelperResult_Qty: stores.etcInStore.itemHelperResult_Qty
    , handleHelperOpen: stores.etcInStore.handleHelperOpen
    , itemHelperOption_Qty: stores.etcInStore.itemHelperOption_Qty
    , selectedEmp_Qty: stores.etcInStore.selectedEmp_Qty
    , empHelperResult_Qty: stores.etcInStore.empHelperResult_Qty
    , empHelperOption_Qty: stores.etcInStore.empHelperOption_Qty
    , prodQty_q: stores.etcInStore.prodQty_q
    // , itemUom: stores.etcInStore.itemUom


}))

class EtcInQtyInsertModal extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        const { ledgerType
            , ledgerTypeList
            , inoutEtcDt_q
            , remark
            , handleChange
            , etcInSave_Qty
            , etcInModalClose_Qty
            , selectedItem_Qty
            , itemHelperResult_Qty
            , itemHelperOption_Qty
            , prodQty_q
            , handleHelperOpen
            , itemUom
            , selectedEmp_Qty
            , empHelperResult_Qty
            , empHelperOption_Qty } = this.props;
        return (
            <React.Fragment>


                <div className='btn_wrap'>
                    <SButton buttonName={"저장"} onClick={etcInSave_Qty} type={"btnSaveQty"} />
                    {/* <SButton buttonName={"닫기"} onClick={etcInModalClose} type={"delete"} /> */}
                </div>

                <table style={{ width: "100%" }}>
                    <tbody>
                        <tr>
                            <th>입고유형</th>
                            <td>
                                <SSelectBox
                                    id="ledgerType"
                                    value={ledgerType}
                                    // codeGroup={"MFD002"}
                                    optionGroup={ledgerTypeList}
                                    onChange={handleChange}
                                />
                            </td>

                            <th>입고일자</th>
                            <td>
                                <SDatePicker
                                    id="inoutEtcDt_q"
                                    value={inoutEtcDt_q}
                                    onChange={handleChange}
                                />
                            </td>
                        </tr>
                        <tr>
                            <th>제품</th>
                            <td>
                                <ItemHelper
                                    selectedItemId={"selectedItem_Qty"}
                                    defaultValue={selectedItem_Qty}
                                    onChange={handleChange}
                                    onHelperResult={itemHelperResult_Qty}
                                    // onOpen={itemHelperOpen}
                                    helperOption={itemHelperOption_Qty}
                                    onOpen={handleHelperOpen}
                                    cdName={'itemId'}
                                    cdNmName={'itemNm'}
                                />

                            </td>

                            <th>수량</th>
                            <td>
                                <SNumericInput
                                    id={"prodQty_q"}
                                    value={prodQty_q}
                                    onChange={handleChange}
                                />
                                {"KG"}
                            </td>

                            {/* <th>입고자</th>
                                <td>
                                    <EmployeeHelper 
                                        selectedItemId={"selectedEmp"}
                                        defaultValue={selectedEmp}
                                        onChange={handleChange}
                                        onHelperResult={empHelperResult}
                                        // onOpen={empHelperOpen}
                                        helperOption={empHelperOption}
                                        cdName={'empNo'}
                                        cdNmName={'loginNm'}
                                    />
                                </td> */}
                        </tr>
                        <tr>
                            <th>비고</th>
                            <td colSpan="3">
                                <STextArea
                                    id="remark"
                                    value={remark}
                                    onChange={handleChange}
                                    contentWidth={485}
                                />
                            </td>
                        </tr>

                    </tbody>
                </table>

            </React.Fragment>
        );
    }
}

export default EtcInQtyInsertModal;