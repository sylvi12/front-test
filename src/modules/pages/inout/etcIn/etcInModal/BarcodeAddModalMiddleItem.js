import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    barcodeSave: stores.etcInStore.barcodeSave,
    barcodeModalClose: stores.etcInStore.barcodeModalClose
}))

class BarcodeAddModalMiddleItem extends Component {

    constructor(props) {
        super(props);
    }
    
    render() {

        const { barcodeSave, barcodeModalClose } = this.props;
        return(
            <React.Fragment>
                <SButton buttonName={"저장"} onClick={barcodeSave} type={"btnEtcSave"} />
                <SButton buttonName={"닫기"} onClick={barcodeModalClose} type={"default"} />                
            </React.Fragment>
        );
    }
}

export default BarcodeAddModalMiddleItem;