import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
      openBarcodeModal: stores.etcInStore.openBarcodeModal
    , delBarcode: stores.etcInStore.delBarcode
}))

class EtcInInsertMiddleItem extends Component {
    
    constructor(props) {
        super(props);
    }

    render() {

        const {openBarcodeModal, delBarcode } = this.props;

        return(
            <React.Fragment>
                <SButton buttonName={"추가"} onClick={openBarcodeModal} type={"btnEtcAdd"} />
                <SButton buttonName={"삭제"} onClick={delBarcode} type={"btnEtcDelete"} />
            </React.Fragment>

        );
    }
}

export default EtcInInsertMiddleItem;