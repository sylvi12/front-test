import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import STextArea from 'components/atoms/input/STextArea';
import SButton from 'components/atoms/button/SButton';

import ItemHelper from 'components/codehelper/ItemHelper';
import EmployeeHelper from 'components/codehelper/EmployeeHelper';

import EtcInStore from "modules/pages/inout/etcIn/service/EtcInStore";

@inject(stores => ({
      ledgerType: stores.etcInStore.ledgerType
    , ledgerTypeList: stores.etcInStore.ledgerTypeList
    , inoutEtcDt_b: stores.etcInStore.inoutEtcDt_b
    , remark: stores.etcInStore.remark
    , handleChange: stores.etcInStore.handleChange
    //, etcInModalClose: stores.etcInStore.etcInModalClose
    , etcInUpdate: stores.etcInStore.etcInUpdate
    , selectedItem: stores.etcInStore.selectedItem
    , itemHelperResult: stores.etcInStore.itemHelperResult
    , handleHelperOpen: stores.etcInStore.handleHelperOpen
    , itemHelperOption: stores.etcInStore.itemHelperOption
    , selectedEmp: stores.etcInStore.selectedEmp
    , empHelperResult: stores.etcInStore.empHelperResult
    , empHelperOption: stores.etcInStore.empHelperOption
    
}))

class EtcInUpdateModal extends Component {
    constructor(props) {
        super(props);
        EtcInStore.getBu();
    }

    render() {

        const {   ledgerType
                , ledgerTypeList
                , inoutEtcDt_b
                , remark
                , handleChange
                , etcInUpdate
                , etcInModalClose
                , selectedItem
                , itemHelperResult
                , handleHelperOpen
                , itemHelperOption
                , selectedEmp
                , empHelperResult
                , empHelperOption 
                } = this.props;
        return(
            <React.Fragment>
                
                
                    <div className='btn_wrap'>
                        <SButton buttonName={"저장"} onClick={etcInUpdate} type={"btnUpdateBarcode"} />
                        {/* <SButton buttonName={"닫기"} onClick={etcInModalClose} type={"delete"} /> */}
                    </div>
                    
                    <table style={{ width: "100%" }}>
                        <tbody>
                            <tr>
                                <th>입고유형</th>
                                <td>
                                    <SSelectBox
                                        id="ledgerType"
                                        value={ledgerType}
                                        // codeGroup={"MFD002"}
                                        optionGroup={ledgerTypeList}
                                        onChange={handleChange}
                                    />
                                </td>

                                <th>입고일자</th>
                                <td>
                                    <SDatePicker
                                        id="inoutEtcDt_b"
                                        value={inoutEtcDt_b}
                                        onChange={handleChange}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <th>제품</th>
                                <td>
                                    <ItemHelper
                                        selectedItemId={"selectedItem"}
                                        defaultValue={selectedItem}
                                        onChange={handleChange}
                                        onHelperResult={itemHelperResult}
                                        // onOpen={itemHelperOpen}
                                        helperOption={itemHelperOption}
                                        onOpen={handleHelperOpen}
                                        cdName={'itemId'}
                                        cdNmName={'itemNm'}
                                    />

                                </td>

                                <th>입고자</th>
                                <td>
                                    <EmployeeHelper 
                                        selectedItemId={"selectedEmp"}
                                        defaultValue={selectedEmp}
                                        onChange={handleChange}
                                        onHelperResult={empHelperResult}
                                        // onOpen={empHelperOpen}
                                        helperOption={empHelperOption}
                                        cdName={'empNo'}
                                        cdNmName={'loginNm'}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <th>비고</th>
                                <td colSpan="3">
                                    <STextArea
                                        id="remark"
                                        value={remark}
                                        onChange={handleChange}
                                        contentWidth={485}
                                    />
                                </td>
                            </tr>

                        </tbody>
                    </table>
                
            </React.Fragment>
        );
    }
}

export default EtcInUpdateModal;