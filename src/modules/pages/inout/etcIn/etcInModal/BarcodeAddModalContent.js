import React, { Component } from "react";
import { inject } from "mobx-react";

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setbarcodeModalGridApi: stores.etcInStore.setbarcodeModalGridApi,
    barcodeModalGridList: stores.etcInStore.barcodeModalGridList,
    handleCellClicked: stores.etcInStore.handleCellClicked
    //, etcBarcodeUpsertModalSave: stores.etcInStore.etcBarcodeUpsertModalSave
}))

class BarcodeAddModalContent extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setbarcodeModalGridApi, barcodeModalGridList, handleCellClicked } = this.props;
        return (
            <SGrid
                grid={'barcodeModalGrid'}
                gridApiCallBack={setbarcodeModalGridApi}
                rowData={barcodeModalGridList}
                onCellClicked={handleCellClicked}
                editable={false}
                suppressRowClickSelection = {true}
            />
        );
    }
}

export default BarcodeAddModalContent;