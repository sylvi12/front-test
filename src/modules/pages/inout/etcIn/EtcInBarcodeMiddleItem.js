import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
      locationChange: stores.etcInStore.locationChange
    , barcodeUse: stores.etcInStore.barcodeUse
    , barcodeStop: stores.etcInStore.barcodeStop
    , inInsert: stores.etcInStore.inInsert
    , inCancle: stores.etcInStore.inCancle
}))

class EtcInBarcodeMiddleItem extends Component {

    render() {

        const { locationChange, barcodeUse, barcodeStop, inInsert, inCancle} = this.props;

        return (
            <React.Fragment>

                <SButton 
                    className="btn_red"
                    buttonName={'위치변경'}
                    type={'btnLocationChange'}
                    onClick={locationChange}
                />

                <SButton 
                    className="btn_red"
                    buttonName={'바코드사용'}
                    type={'btnBarcodeUse'}
                    onClick={barcodeUse}
                />

                <SButton 
                    className="btn_red"
                    buttonName={'바코드중지'}
                    type={'btnBarcodeStop'}
                    onClick={barcodeStop}
                />

                <SButton 
                    className="btn_red"
                    buttonName={'입고등록'}
                    type={'btnInInsert'}
                    onClick={inInsert}
                />

             <SButton 
                    className="btn_red"
                    buttonName={'입고취소'}
                    type={'btnInCancle'}
                    onClick={inCancle}
                />






                {/* <SButton 
                    className="btn_red"
                    buttonName={'추가'}
                    type={'btnBarcodeAdd'}
                    onClick={openBarcodeAddModal}
                />
                
                <SButton 
                    className="btn_red"
                    buttonName={'삭제'}
                    type={'btnBarcodeDelete'}
                    onClick={delBarcode}
                /> */}
            </React.Fragment>
        );
    }
}

export default EtcInBarcodeMiddleItem;