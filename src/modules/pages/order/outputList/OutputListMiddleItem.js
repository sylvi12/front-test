import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    outsourcingYn: stores.outputListStore.outsourcingYn
    , btnResultRequire: stores.outputListStore.btnResultRequire
    , btnResultConfirm: stores.outputListStore.btnResultConfirm
}))

class OutputListMiddleItem extends Component {

    render() {

        const { outsourcingYn, btnResultRequire, btnResultConfirm } = this.props;
        return (
            <React.Fragment>
                {outsourcingYn == "Y" ? (
                    <SButton
                        buttonName={'실적요청'}
                        type={'btnResultRequire'}
                        onClick={btnResultRequire}
                    />
                ) : (
                        <SButton
                            buttonName={'실적요청확인'}
                            type={'btnResultConfirm'}
                            onClick={btnResultConfirm}
                        />
                    )}
            </React.Fragment>
        )
    }
}

export default OutputListMiddleItem;