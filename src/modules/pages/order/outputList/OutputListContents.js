import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setGridApi: stores.outputListStore.setGridApi
    , outputList: stores.outputListStore.outputList
    , pinnedBottomRowData: stores.outputListStore.pinnedBottomRowData
}))

class OutputListContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setGridApi, outputList, pinnedBottomRowData } = this.props;

        return (
            <SGrid
                grid={'outputGrid'}
                gridApiCallBack={setGridApi}
                rowData={outputList}
                editable={false}
                
                pinnedBottomRowData={pinnedBottomRowData}
            />
        );
    }
}
export default OutputListContents;