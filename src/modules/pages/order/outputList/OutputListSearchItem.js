import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SBuSelectBox from 'components/override/business/SBuSelectBox';
import SButton from 'components/atoms/button/SButton';
import SRadio from 'components/atoms/radio/SRadio';

import OutputListStore from 'modules/pages/order/outputList/service/OutputListStore';

@inject(stores => ({
    buId: stores.outputListStore.buId
    , handleChange: stores.outputListStore.handleChange

    , outsourcingYn: stores.outputListStore.outsourcingYn

    , btnSearch: stores.outputListStore.btnSearch
}))

class OutputListSearchItem extends Component {

    constructor(props) {
        super(props);

    }

    async componentWillMount() {
        if (OutputListStore.alphaCheck == false) {
            await OutputListStore.getBu();
        }
    }

    render() {

        const { buId, handleChange, outsourcingYn, btnSearch } = this.props;

        return (

            <React.Fragment>
                <SBuSelectBox
                    title={"공장"}
                    id={"buId"}
                    value={buId}
                    onChange={handleChange}
                />
                <SRadio
                    title={'외주 구분'}
                    id={"outsourcingYn"}
                    value={outsourcingYn}
                    codeGroup={"SYS001"}
                    onChange={handleChange}
                />
                <div className='search_item_btn'>
                    <SButton
                        buttonName={'조회'}
                        type={'btnSearch'}
                        onClick={btnSearch} />
                </div>
            </React.Fragment>
        )
    }
}

export default OutputListSearchItem;