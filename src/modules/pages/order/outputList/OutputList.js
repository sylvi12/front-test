import React, { Component } from "react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import OutputListSearchItem from "modules/pages/order/outputList/OutputListSearchItem";
import OutputListMiddleItem from "modules/pages/order/outputList/OutputListMiddleItem";
import OutputListContents from "modules/pages/order/outputList/OutputListContents";

class OutputList extends Component {

    render() {

        return (
            <React.Fragment>
                <SearchTemplate searchItem={<OutputListSearchItem />} />
                <ContentsMiddleTemplate contentSubTitle={'생산집계 리스트'} middleItem={<OutputListMiddleItem />} />
                <ContentsTemplate id='outputList' contentItem={<OutputListContents />} />                
            </React.Fragment>
        )
    }
}

export default OutputList;