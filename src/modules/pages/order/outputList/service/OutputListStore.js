import { observable, action } from 'mobx';

import Alert from 'components/override/alert/Alert';
import GridUtil from 'components/override/grid/utils/GridUtil';

import OutputListRepository from 'modules/pages/order/outputList/service/OutputListRepository';
import WorkorderRepository from 'modules/pages/order/workorder/service/WorkorderRepository';
import CommonStore from 'modules/pages/common/service/CommonStore';
import BaseStore from 'utils/base/BaseStore';

class OutputListStore extends BaseStore {
    // searchItem
    @observable alphaCheck = false;
    @observable buId = '';
    @observable outsourcingYn = 'Y';
    // contents
    gridApi = undefined;
    @observable outputList = [];
    @observable pinnedBottomRowData = [];

    constructor() {
        super();
        this.setInitialState();
    }

    @action.bound
    handleChange = (data) => {
        console.log(data);
        this[data.id] = data.value;
    }

    /* MAIN */
    // main contents
    @action.bound
    setGridApi = async (gridApi) => {
        this.gridApi = gridApi;
        this.pinnedBottomRowData = [
            {
                lineNm: '합계',
                prodQty: 0
            }
        ];
        const columnDefs = [
            {
                headerName: ""
                , field: "check"
                , width: 29
                , checkboxSelection: true
                , headerCheckboxSelection: true
            },
            {
                headerName: "작성일"
                , field: "prodDt"
                , width: 75
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "지시번호"
                , field: "orderNo"
                , width: 60
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "라인"
                , field: "lineNm"
                , width: 125
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "직"
                , field: "prodShiftNm"
                , width: 50
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "팀"
                , field: "prodTeamNm"
                , width: 50
                , cellClass: 'cell_align_center'
            },            
            {
                headerName: "시작시각"
                , field: "prodStartDt"
                , width: 60
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "종료시각"
                , field: "prodEndDt"
                , width: 60
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },            
            {
                headerName: "제품코드"
                , field: "itemCd"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "제품명"
                , field: "itemNm"
                , width: 155
            },
            {
                headerName: "실적수량"
                , field: "prodQty"
                , width: 65
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
                , pinnedRowCellRenderer: "customPinnedRowRenderer"
                , pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
            },
            {
                headerName: "단위"
                , field: "prodUom"
                , width: 50
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "작성자"
                , field: "modEmpNm"
                , width: 125
                , cellClass: 'cell_align_center'
            },
        ];
        this.gridApi.setColumnDefs(columnDefs);
    }
    // getBu
    @action.bound
    getBu = () => {
        this.buId = CommonStore.buId;
        this.userInfo = CommonStore.userInfo;
    } 
    // main searchItem 조회 클릭 시
    @action.bound
    btnSearch = async () => {
        const searchData = {
            buId: this.buId
            , outsourcingYn : this.outsourcingYn
        }
        const { data, status } = await OutputListRepository.getList(searchData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            this.outputList = data.data;
            this.gridApi.setRowData(this.outputList);
        }
    }
    // 실적요청
    @action.bound
    btnResultRequire = async () => {
        const selectedRows = this.gridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length <= 0) {
            Alert.meg('선택된 항목이 없습니다.');
            return;
        }
        console.log(selectedRows);
        let count = 0;
        for (let i = 0; i < selectedRows.length; i++) {
            // 라인 다를 때            
            if (this.userInfo.loginId != selectedRows[i].outsourcingLoginNo) {
                Alert.meg("권한이 없습니다.");
                return;
            }
            selectedRows[i].planStartDtm2 = selectedRows[i].planStartDtm2;
            selectedRows[i].planDtm = selectedRows[i].planDtm;
            let arr1 = selectedRows[i].prodDt.substring(0, 4);
            let arr2 = selectedRows[i].prodDt.substring(4, 6);
            let arr3 = selectedRows[i].prodDt.substring(6);
            selectedRows[i].prodDt2 = arr1 + "-" + arr2 + "-" + arr3;
            selectedRows[i].prodQty = Math.ceil(selectedRows[i].prodQty / selectedRows[i].prodUomQty);
            selectedRows[i].prodQtyUom = selectedRows[i].orderUom;
            selectedRows[i].orderMemo = selectedRows[i].orderMemo;
            selectedRows[i].userIdForMail = selectedRows[i].inputEmpNo;
            selectedRows[i].emergencyYN = selectedRows[i].emergencyYN;
            selectedRows[i].buId = this.buId;
            count = count + 1;
        }
        if (count >= 1) {
            const searchData = {
                performanceStatus: '10'
            }
            console.log(selectedRows);
            const params = JSON.stringify({ param1: selectedRows, param2: searchData });
            const { data, status } = await WorkorderRepository.perfomanceRequireConfirm(params);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                Alert.meg("요청되었습니다.");
                this.btnSearch();
            }
        }
    }
    // 실적요청확인
    @action.bound
    btnResultConfirm = async () => {
        const selectedRows = this.gridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg('선택된 항목이 없습니다.');
            return;
        }
        if (!this.userInfo.outsourcingYn && this.userInfo.outsourcingYn == 'Y') {
            Alert.meg("권한이 없습니다.");
            return;
        }
        // for (let i = 0; i < selectedRows.length; i++) {
        //     if (selectedRows[i].reqCnt <= 0) {
        //         Alert.meg("실적요청건수가 없습니다.");
        //         return;
        //     }
        // }
        const searchData = {
            performanceStatus: '20'
        }
        const params = JSON.stringify({ param1: selectedRows, param2: searchData });
        const { data, status } = await WorkorderRepository.perfomanceRequireConfirm(params);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            Alert.meg("확인되었습니다.");
            this.btnSearch();
        }
    }
}

export default new OutputListStore();