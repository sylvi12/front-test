import API from "components/override/api/API";

class OutputListRepository {

    URL = "/api/order/outputlist";
    /* 조회 */
    // 조회
    getList(param) {
        return API.request.post(encodeURI(`${this.URL}/getlist`), param)
    }

    /* 등록 */
    // 실적승인
    requireConfirm(params) {
        return API.request.post(`${this.URL}/requireconfirm`, params)
    }
}
export default new OutputListRepository();