import React, { Component } from "react";
import { inject, observer } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    userInfo: stores.workorderStore.userInfo
    , selectedLine: stores.workorderStore.selectedLine
    , middleButtonType: stores.workorderStore.middleButtonType
    , rollbackStatus: stores.workorderStore.rollbackStatus
    , openProdDtModal: stores.workorderStore.openProdDtModal
    , prodInputDrOpen: stores.workorderStore.prodInputDrOpen
    , openOrderHelperModal: stores.workorderStore.openOrderHelperModal
    , handleSaveClick: stores.workorderStore.handleSaveClick
    , handleUpdateClick: stores.workorderStore.handleUpdateClick
    , handelRequireClick: stores.workorderStore.handelRequireClick
    , handleOrderClick: stores.workorderStore.handleOrderClick
    , btnResultConfirm: stores.workorderStore.btnResultConfirm
    , handleCompleteClick: stores.workorderStore.handleCompleteClick

    , emergencyOrder: stores.workorderStore.emergencyOrder
    , requireAccept: stores.workorderStore.requireAccept
    , packingOrders: stores.workorderStore.packingOrders
    , packingCompletion: stores.workorderStore.packingCompletion

    , btnCancel: stores.workorderStore.btnCancel
    , btnPlanDelete: stores.workorderStore.btnPlanDelete
    , openBarcodeModal: stores.workorderStore.openBarcodeModal
}))
@observer
class WorkorderMiddleItem2 extends Component {

    render() {

        const { userInfo, selectedLine, middleButtonType, rollbackStatus
            , openProdDtModal
            , prodInputDrOpen, openOrderHelperModal
            , handleSaveClick, handleUpdateClick, handelRequireClick, handleOrderClick, btnResultConfirm, handleCompleteClick
            , emergencyOrder, requireAccept, packingOrders, packingCompletion
            , btnCancel, btnPlanDelete, openBarcodeModal
        } = this.props;

        const components = {
            status00buttons: Status00buttons
            , status10buttons: Status10buttons
            , status60buttons: Status60buttons
            , status70buttons: Status70buttons
            , status80buttons: Status80buttons
            , status90buttons: Status90buttons

            , status00_2buttons: Status00_2buttons
            , status10_2buttons: Status10_2buttons
            , status20buttons: Status20buttons
            , status25buttons: Status25buttons
            , status60_2buttons: Status60_2buttons
            , status70_2buttons: Status70_2buttons
            , status80_2buttons: Status80_2buttons
            , status90_2buttons: Status90_2buttons

            , status00_3buttons: Status00_3buttons
            , status10_3buttons: Status10_3buttons
            , status20_3buttons: Status20_3buttons
            , status25_3buttons: Status25_3buttons
            , status60_3buttons: Status60_3buttons
            , status70_3buttons: Status70_3buttons
            , status80_3buttons: Status80_3buttons
            , status90_3buttons: Status90_3buttons
        }
        let ButtonCollection;
        console.log(userInfo.outsourcingYn, selectedLine.outsourcingYn, selectedLine.outsourcingLoginNo);
        // 삼양사
        if (userInfo.outsourcingYn == undefined || userInfo.outsourcingYn == null || userInfo.outsourcingYn == "N") {
            // 자가라인
            if (selectedLine.outsourcingLoginNo == null || selectedLine.outsourcingLoginNo == undefined || selectedLine.outsourcingLoginNo == "") {
                switch (middleButtonType) {
                    case "10":
                        ButtonCollection = components['status10buttons'];
                        break;
                    case "60":
                        ButtonCollection = components['status60buttons'];
                        break;
                    case "70":
                        ButtonCollection = components['status70buttons'];
                        break;
                    case "80":
                        ButtonCollection = components['status80buttons'];
                        break;
                    case "90":
                        ButtonCollection = components['status90buttons'];
                        break;
                    default:
                        // 전체
                        ButtonCollection = components['status00buttons'];
                        break;
                }
                // 외주라인
            } else {
                switch (middleButtonType) {
                    case "10":
                        ButtonCollection = components['status10_2buttons'];
                        break;
                    case "20":
                        ButtonCollection = components['status20buttons'];
                        break;
                    case "25":
                        ButtonCollection = components['status25buttons'];
                        break;
                    case "60":
                        ButtonCollection = components['status60_2buttons'];
                        break;
                    case "70":
                        ButtonCollection = components['status70_2buttons'];
                        break;
                    case "80":
                        ButtonCollection = components['status80_2buttons'];
                        break;
                    case "90":
                        ButtonCollection = components['status90_2buttons'];
                        break;
                    // 전체
                    default:
                        ButtonCollection = components['status00_2buttons'];
                        break;
                }
            }
            // 하도급
        } else {
            switch (middleButtonType) {
                case "10":
                    ButtonCollection = components['status10_3buttons'];
                    break;
                case "20":
                    ButtonCollection = components['status20_3buttons'];
                    break;
                case "25":
                    ButtonCollection = components['status25_3buttons'];
                    break;
                case "60":
                    ButtonCollection = components['status60_3buttons'];
                    break;
                case "70":
                    ButtonCollection = components['status70_3buttons'];
                    break;
                case "80":
                    ButtonCollection = components['status80_3buttons'];
                    break;
                case "90":
                    ButtonCollection = components['status90_3buttons'];
                    break;
                // 전체
                default:
                    ButtonCollection = components['status00_3buttons'];
                    break;
            }
        }
        return (
            <React.Fragment>
                <ButtonCollection openProdDtModal={openProdDtModal} prodInputDrOpen={prodInputDrOpen}
                    openOrderHelperModal={openOrderHelperModal} handleSaveClick={handleSaveClick}
                    handleUpdateClick={handleUpdateClick} handleOrderClick={handleOrderClick}
                    handleCompleteClick={handleCompleteClick}
                    btnCancel={btnCancel} btnPlanDelete={btnPlanDelete}
                    btnResultConfirm={btnResultConfirm}
                    openBarcodeModal={openBarcodeModal} rollbackStatus={rollbackStatus}
                    emergencyOrder={emergencyOrder} requireAccept = {requireAccept}
                    packingOrders={packingOrders} packingCompletion={packingCompletion}
                    handelRequireClick={handelRequireClick}
                />
            </React.Fragment>
        );
    }
}

export default WorkorderMiddleItem2;

const Status00buttons = ({ handleSaveClick }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"계획등록"} onClick={handleSaveClick} type={"btnPlanInsert"} />
        </React.Fragment>
    )
}
const Status10buttons = ({ openOrderHelperModal, handleSaveClick, handleUpdateClick, handleOrderClick, btnPlanDelete }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"생산정보"} onClick={openOrderHelperModal} type={"btnProdInfo"} />
            <SButton buttonName={"계획등록"} onClick={handleSaveClick} type={"btnPlanInsert"} />
            <SButton buttonName={"계획수정"} onClick={handleUpdateClick} type={"btnPlanUpdate"} />
            <SButton buttonName={"지시"} onClick={handleOrderClick} type={"btnWorkOrderByPA"} />
            <SButton buttonName={"삭제"} onClick={btnPlanDelete} type={'btnPlanDelete'} />
        </React.Fragment>
    )
}
const Status60buttons = ({ openProdDtModal, openOrderHelperModal, handleSaveClick, handleUpdateClick, btnCancel, btnPlanDelete, openBarcodeModal }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"생산시간 입력"} onClick={openProdDtModal} type={"btnProdDtmInsert"} />
            <SButton buttonName={"생산정보"} onClick={openOrderHelperModal} type={"btnProdInfo"} />
            <SButton buttonName={"계획등록"} onClick={handleSaveClick} type={"btnPlanInsert"} />
            <SButton buttonName={"지시수정"} onClick={handleUpdateClick} type={"btnPlanUpdate"} />
            <SButton buttonName={"지시취소"} onClick={btnCancel} type={"btnCancel"} />
            <SButton buttonName={"삭제"} onClick={btnPlanDelete} type={'btnPlanDelete'} />
            <SButton buttonName={"바코드발행"} onClick={openBarcodeModal} type={'btnCreateBarcode'} />
        </React.Fragment>
    )
}
const Status70buttons = ({ openProdDtModal, prodInputDrOpen, openOrderHelperModal, handleSaveClick, handleUpdateClick, handleCompleteClick, btnCancel, btnPlanDelete, openBarcodeModal, rollbackStatus }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"생산시간 입력"} onClick={openProdDtModal} type={"btnProdDtmInsert"} />
            <SButton buttonName={"재포장투입일보"} onClick={prodInputDrOpen} type={"btnProdOutput"} />
            <SButton buttonName={"생산정보"} onClick={openOrderHelperModal} type={"btnProdInfo"} />
            <SButton buttonName={"계획등록"} onClick={handleSaveClick} type={"btnPlanInsert"} />
            <SButton buttonName={"지시수정"} onClick={handleUpdateClick} type={"btnPlanUpdate"} />
            <SButton buttonName={"지시완료"} onClick={handleCompleteClick} type={"btnWorkEndByPA"} />
            <SButton buttonName={"지시취소"} onClick={btnCancel} type={"btnCancel"} />
            <SButton buttonName={"삭제"} onClick={btnPlanDelete} type={'btnPlanDelete'} />
            <SButton buttonName={"바코드발행"} onClick={openBarcodeModal} type={'btnCreateBarcode'} />
            <SButton buttonName={"지시상태로"} onClick={rollbackStatus} type={'rollbackStatus'} />
        </React.Fragment>
    )
}
const Status80buttons = ({ openProdDtModal, prodInputDrOpen, openOrderHelperModal, handleSaveClick, btnCancel, btnPlanDelete, rollbackStatus }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"생산시간 입력"} onClick={openProdDtModal} type={"btnProdDtmInsert"} />
            <SButton buttonName={"재포장투입일보"} onClick={prodInputDrOpen} type={"btnProdOutput"} />
            <SButton buttonName={"생산정보"} onClick={openOrderHelperModal} type={"btnProdInfo"} />
            <SButton buttonName={"계획등록"} onClick={handleSaveClick} type={"btnPlanInsert"} />
            <SButton buttonName={"지시취소"} onClick={btnCancel} type={"btnCancel"} />
            <SButton buttonName={"삭제"} onClick={btnPlanDelete} type={'btnPlanDelete'} />
            <SButton buttonName={"생산중 상태로"} onClick={rollbackStatus} type={'rollbackStatus'} />
        </React.Fragment>
    )
}
const Status90buttons = ({ openOrderHelperModal, handleSaveClick, btnPlanDelete }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"생산정보"} onClick={openOrderHelperModal} type={"btnProdInfo"} />
            <SButton buttonName={"계획등록"} onClick={handleSaveClick} type={"btnPlanInsert"} />
            <SButton buttonName={"삭제"} onClick={btnPlanDelete} type={'btnPlanDelete'} />
        </React.Fragment>
    )
}
const Status00_2buttons = ({ handleSaveClick }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"계획등록"} onClick={handleSaveClick} type={"btnPlanInsert"} />
        </React.Fragment>
    )
}
const Status10_2buttons = ({ openOrderHelperModal, handleSaveClick, handleUpdateClick, handelRequireClick, btnPlanDelete }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"생산정보"} onClick={openOrderHelperModal} type={"btnProdInfo"} />
            <SButton buttonName={"계획등록"} onClick={handleSaveClick} type={"btnPlanInsert"} />
            <SButton buttonName={"계획수정"} onClick={handleUpdateClick} type={"btnPlanUpdate"} />
            <SButton buttonName={"작업요청"} onClick={handelRequireClick} type={"btnWorkRequire"} />
            <SButton buttonName={"삭제"} onClick={btnPlanDelete} type={'btnPlanDelete'} />
        </React.Fragment>
    )
}
const Status20buttons = ({ openOrderHelperModal, handleSaveClick, handleUpdateClick, btnCancel, btnPlanDelete }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"생산정보"} onClick={openOrderHelperModal} type={"btnProdInfo"} />
            <SButton buttonName={"계획등록"} onClick={handleSaveClick} type={"btnPlanInsert"} />
            <SButton buttonName={"지시수정"} onClick={handleUpdateClick} type={"btnPlanUpdate"} />
            <SButton buttonName={"지시취소"} onClick={btnCancel} type={"btnCancel"} />
            <SButton buttonName={"삭제"} onClick={btnPlanDelete} type={"btnPlanDelete"} />
        </React.Fragment>
    )
}
const Status25buttons = ({ openOrderHelperModal, handleSaveClick }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"생산정보"} onClick={openOrderHelperModal} type={"btnProdInfo"} />
            <SButton buttonName={"계획등록"} onClick={handleSaveClick} type={"btnPlanInsert"} />
        </React.Fragment>
    )
}
const Status60_2buttons = ({ openProdDtModal, openOrderHelperModal, handleSaveClick, btnPlanDelete, btnResultConfirm, btnCancel }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"생산시간 입력"} onClick={openProdDtModal} type={"btnProdDtmInsert"} />
            <SButton buttonName={"생산정보"} onClick={openOrderHelperModal} type={"btnProdInfo"} />
            <SButton buttonName={"계획등록"} onClick={handleSaveClick} type={"btnPlanInsert"} />
            <SButton buttonName={"지시취소"} onClick={btnCancel} type={"btnCancel"} />
            <SButton buttonName={"삭제"} onClick={btnPlanDelete} type={'btnPlanDelete'} />
            <SButton buttonName={"실적요청확인"} onClick={btnResultConfirm} type={'btnResultConfirm'} />
        </React.Fragment>
    )
}
const Status70_2buttons = ({ openProdDtModal, prodInputDrOpen, openOrderHelperModal, handleSaveClick, handleUpdateClick, btnPlanDelete, btnResultConfirm, rollbackStatus, btnCancel }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"생산시간 입력"} onClick={openProdDtModal} type={"btnProdDtmInsert"} />
            <SButton buttonName={"재포장투입일보"} onClick={prodInputDrOpen} type={"btnProdOutput"} />
            <SButton buttonName={"생산정보"} onClick={openOrderHelperModal} type={"btnProdInfo"} />
            <SButton buttonName={"계획등록"} onClick={handleSaveClick} type={"btnPlanInsert"} />
            <SButton buttonName={"지시수정"} onClick={handleUpdateClick} type={"btnPlanUpdate"} />
            <SButton buttonName={"지시취소"} onClick={btnCancel} type={"btnCancel"} />
            <SButton buttonName={"삭제"} onClick={btnPlanDelete} type={'btnPlanDelete'} />
            <SButton buttonName={"실적요청확인"} onClick={btnResultConfirm} type={'btnResultConfirm'} />
            <SButton buttonName={"지시상태로"} onClick={rollbackStatus} type={'rollbackStatus'} />
        </React.Fragment>
    )
}
const Status80_2buttons = ({ openProdDtModal, prodInputDrOpen, openOrderHelperModal, handleSaveClick, btnPlanDelete, btnResultConfirm, rollbackStatus, btnCancel }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"생산시간 입력"} onClick={openProdDtModal} type={"btnProdDtmInsert"} />
            <SButton buttonName={"재포장투입일보"} onClick={prodInputDrOpen} type={"btnProdOutput"} />
            <SButton buttonName={"생산정보"} onClick={openOrderHelperModal} type={"btnProdDtmInsert"} />
            <SButton buttonName={"계획등록"} onClick={handleSaveClick} type={"btnPlanInsert"} />
            <SButton buttonName={"지시취소"} onClick={btnCancel} type={"btnCancel"} />
            <SButton buttonName={"삭제"} onClick={btnPlanDelete} type={'btnPlanDelete'} />
            <SButton buttonName={"실적요청확인"} onClick={btnResultConfirm} type={'btnResultConfirm'} />
            <SButton buttonName={"생산중 상태로"} onClick={rollbackStatus} type={'rollbackStatus'} />
        </React.Fragment>
    )
}
const Status90_2buttons = ({ openOrderHelperModal, handleSaveClick, btnPlanDelete }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"생산정보"} onClick={openOrderHelperModal} type={"btnProdInfo"} />
            <SButton buttonName={"계획등록"} onClick={handleSaveClick} type={"btnPlanInsert"} />
            <SButton buttonName={"삭제"} onClick={btnPlanDelete} type={'btnPlanDelete'} />
        </React.Fragment>
    )
}
const Status00_3buttons = ({ emergencyOrder }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"긴급지시"} onClick={emergencyOrder} type={"btnEmergencyWorkOrder"} />
        </React.Fragment>
    )
}
const Status10_3buttons = ({ openOrderHelperModal, emergencyOrder, handleUpdateClick }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"생산정보"} onClick={openOrderHelperModal} type={"btnProdInfo"} />
            <SButton buttonName={"긴급지시"} onClick={emergencyOrder} type={"btnEmergencyWorkOrder"} />
            <SButton buttonName={"계획수정"} onClick={handleUpdateClick} type={"btnPlanUpdate"} />
        </React.Fragment>
    )
}
const Status20_3buttons = ({ openOrderHelperModal, emergencyOrder, requireAccept, btnCancel}) => {
    return (
        <React.Fragment>
            <SButton buttonName={"생산정보"} onClick={openOrderHelperModal} type={"btnProdInfo"} />
            <SButton buttonName={"긴급지시"} onClick={emergencyOrder} type={"btnEmergencyWorkOrder"} />
            <SButton buttonName={"지시취소"} onClick={btnCancel} type={"btnCancel"} />
            <SButton buttonName={"작업요청확인"} onClick={requireAccept} type={"btnRequireAccept"} />
        </React.Fragment>
    )
}
const Status25_3buttons = ({ openOrderHelperModal, emergencyOrder, packingOrders }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"생산정보"} onClick={openOrderHelperModal} type={"btnProdInfo"} />
            <SButton buttonName={"긴급지시"} onClick={emergencyOrder} type={"btnEmergencyWorkOrder"} />
            <SButton buttonName={"지시"} onClick={packingOrders} type={"btnWorkOrderByFD"} />
        </React.Fragment>
    )
}
const Status60_3buttons = ({ openProdDtModal, openOrderHelperModal, emergencyOrder, openBarcodeModal }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"생산시간 입력"} onClick={openProdDtModal} type={"btnProdDtmInsert"} />
            <SButton buttonName={"생산정보"} onClick={openOrderHelperModal} type={"btnProdInfo"} />
            <SButton buttonName={"긴급지시"} onClick={emergencyOrder} type={"btnEmergencyWorkOrder"} />
            <SButton buttonName={"바코드발행"} onClick={openBarcodeModal} type={'btnCreateBarcode'} />
        </React.Fragment>
    )
}
const Status70_3buttons = ({ openProdDtModal, openOrderHelperModal, emergencyOrder, packingCompletion, openBarcodeModal, rollbackStatus }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"생산시간 입력"} onClick={openProdDtModal} type={"btnProdDtmInsert"} />
            <SButton buttonName={"생산정보"} onClick={openOrderHelperModal} type={"btnProdInfo"} />
            <SButton buttonName={"긴급지시"} onClick={emergencyOrder} type={"btnEmergencyWorkOrder"} />
            <SButton buttonName={"지시완료"} onClick={packingCompletion} type={"btnWorkEndByFD"} />
            <SButton buttonName={"바코드발행"} onClick={openBarcodeModal} type={'btnCreateBarcode'} />
            <SButton buttonName={"지시상태로"} onClick={rollbackStatus} type={'rollbackStatus'} />
        </React.Fragment>
    )
}
const Status80_3buttons = ({ openProdDtModal, openOrderHelperModal, emergencyOrder, rollbackStatus }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"생산시간 입력"} onClick={openProdDtModal} type={"btnProdDtmInsert"} />
            <SButton buttonName={"생산정보"} onClick={openOrderHelperModal} type={"btnProdDtmInsert"} />
            <SButton buttonName={"긴급지시"} onClick={emergencyOrder} type={"btnEmergencyWorkOrder"} />
            <SButton buttonName={"생산중 상태로"} onClick={rollbackStatus} type={'rollbackStatus'} />
        </React.Fragment>
    )
}
const Status90_3buttons = ({ openOrderHelperModal, emergencyOrder }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"생산정보"} onClick={openOrderHelperModal} type={"btnProdInfo"} />
            <SButton buttonName={"긴급지시"} onClick={emergencyOrder} type={"btnEmergencyWorkOrder"} />
        </React.Fragment>
    )
}



