import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    openProdDtModal: stores.workorderStore.openProdDtModal
    , perfomanceRequire: stores.workorderStore.perfomanceRequire
}))

class WorkorderOutputMiddleItem extends Component {

    render() {

        const { openProdDtModal, perfomanceRequire } = this.props;
        return (
            <React.Fragment>
                <SButton
                    className="btn_red"
                    buttonName={'실적요청'}
                    type={'btnResultRequire'}
                    onClick={perfomanceRequire}
                />
            </React.Fragment>
        )
    }
}

export default WorkorderOutputMiddleItem;