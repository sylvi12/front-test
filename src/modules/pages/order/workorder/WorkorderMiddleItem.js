import React, { Component } from "react";
import { inject, observer } from "mobx-react";

import SButton from 'components/atoms/button/SButton';
import CommonStore from 'modules/pages/common/service/CommonStore';

@inject(stores => ({
    userInfo: stores.workorderStore.userInfo
    , selectedLine: stores.workorderStore.selectedLine
    , openProdDtModal: stores.workorderStore.openProdDtModal
    , prodInputDrOpen: stores.workorderStore.prodInputDrOpen
    , openOrderHelperModal: stores.workorderStore.openOrderHelperModal
    , handleSaveClick: stores.workorderStore.handleSaveClick
    , handleUpdateClick: stores.workorderStore.handleUpdateClick
    , handelRequireClick: stores.workorderStore.handelRequireClick
    , handleOrderClick: stores.workorderStore.handleOrderClick
    , btnResultConfirm: stores.workorderStore.btnResultConfirm
    , handleCompleteClick: stores.workorderStore.handleCompleteClick

    , emergencyOrder: stores.workorderStore.emergencyOrder
    , requireAccept: stores.workorderStore.requireAccept
    , packingOrders: stores.workorderStore.packingOrders
    , packingCompletion: stores.workorderStore.packingCompletion

    , btnCancel: stores.workorderStore.btnCancel
    , btnPlanDelete: stores.workorderStore.btnPlanDelete
    , openBarcodeModal: stores.workorderStore.openBarcodeModal
}))
@observer
class WorkorderMiddleItem extends Component {

    render() {

        const { userInfo, selectedLine
            , openProdDtModal
            , prodInputDrOpen, openOrderHelperModal
            , handleSaveClick, handleUpdateClick, handelRequireClick, handleOrderClick, btnResultConfirm, handleCompleteClick
            , emergencyOrder, requireAccept, packingOrders, packingCompletion
            , btnCancel, btnPlanDelete, openBarcodeModal
        } = this.props;
        console.log(userInfo.outsourcingYn, selectedLine.outsourcingYn);
        if (userInfo.outsourcingYn == undefined || userInfo.outsourcingYn == null || userInfo.outsourcingYn == "N") {
            if (selectedLine.outsourcingYn == "N") {
                return (
                    <React.Fragment>
                        <SButton
                            className="btn_red"
                            buttonName={'생산시간 입력'}
                            type={'btnProdDtmInsert'}
                            onClick={openProdDtModal}
                        />
                        <SButton
                            className="btn_red"
                            buttonName={'재포장투입일보'}
                            type={'btnProdOutput'}
                            onClick={prodInputDrOpen}
                        />
                        <SButton
                            className="btn_red"
                            buttonName={'생산정보'}
                            type={'btnProdInfo'}
                            onClick={openOrderHelperModal}
                        />
                        <SButton
                            className="btn_red"
                            buttonName={'계획등록'}
                            type={'btnPlanInsert'}
                            onClick={handleSaveClick}
                        />
                        <SButton
                            buttonName={'계획수정'}
                            className="btn_red"
                            type={'btnPlanUpdate'}
                            onClick={handleUpdateClick}
                        />
                        <SButton
                            className="btn_red"
                            buttonName={'지시'}
                            type={'btnWorkOrderByPA'}
                            onClick={handleOrderClick}
                        />
                        <SButton
                            buttonName={'지시완료'}
                            type={'btnWorkEndByPA'}
                            onClick={handleCompleteClick}
                        />
                        <SButton
                            className="btn_red"
                            buttonName={'지시취소'}
                            type={'btnCancel'}
                            onClick={btnCancel}
                        />
                        <SButton
                            className="btn_red"
                            buttonName={'삭제'}
                            type={'btnPlanDelete'}
                            onClick={btnPlanDelete}
                        />
                        <SButton
                            buttonName={'바코드발행'}
                            type={'btnCreateBarcode'}
                            onClick={openBarcodeModal}
                        />
                    </React.Fragment>
                );
            } else if (selectedLine.outsourcingYn == "Y") {
                return (
                    <React.Fragment>
                        <SButton
                            className="btn_red"
                            buttonName={'생산시간 입력'}
                            type={'btnProdDtmInsert'}
                            onClick={openProdDtModal}
                        />
                        <SButton
                            className="btn_red"
                            buttonName={'생산정보'}
                            type={'btnProdInfo'}
                            onClick={openOrderHelperModal}
                        />
                        <SButton
                            className="btn_red"
                            buttonName={'계획등록'}
                            type={'btnPlanInsert'}
                            onClick={handleSaveClick}
                        />
                        <SButton
                            buttonName={'계획수정'}
                            className="btn_red"
                            type={'btnPlanUpdate'}
                            onClick={handleUpdateClick}
                        />
                        <SButton
                            className="btn_red"
                            buttonName={'작업요청'}
                            type={'btnWorkRequire'}
                            onClick={handelRequireClick}
                        />
                        <SButton
                            className="btn_red"
                            buttonName={'삭제'}
                            type={'btnPlanDelete'}
                            onClick={btnPlanDelete}
                        />
                        <SButton
                            className="btn_red"
                            buttonName={'실적요청확인'}
                            type={'btnResultConfirm'}
                            onClick={btnResultConfirm}
                        />
                    </React.Fragment>
                );
            } else if (selectedLine.outsourcingYn == undefined || selectedLine.outsourcingYn == null) {
                return (
                    <React.Fragment>
                        <SButton
                            className="btn_red"
                            buttonName={'생산시간 입력'}
                            type={'btnProdDtmInsert'}
                            onClick={openProdDtModal}
                        />
                        <SButton
                            className="btn_red"
                            buttonName={'재포장투입일보'}
                            type={'btnProdOutput'}
                            onClick={prodInputDrOpen}
                        />
                        <SButton
                            className="btn_red"
                            buttonName={'생산정보'}
                            type={'btnProdInfo'}
                            onClick={openOrderHelperModal}
                        />
                        <SButton
                            className="btn_red"
                            buttonName={'계획등록'}
                            type={'btnPlanInsert'}
                            onClick={handleSaveClick}
                        />
                        <SButton
                            buttonName={'계획수정'}
                            className="btn_red"
                            type={'btnPlanUpdate'}
                            onClick={handleUpdateClick}
                        />
                        <SButton
                            className="btn_red"
                            buttonName={'지시'}
                            type={'btnWorkOrderByPA'}
                            onClick={handleOrderClick}
                        />
                        <SButton
                            buttonName={'지시완료'}
                            type={'btnWorkEndByPA'}
                            onClick={handleCompleteClick}
                        />
                        <SButton
                            className="btn_red"
                            buttonName={'작업요청확인'}
                            type={'btnRequireAccept'}
                            onClick={requireAccept}
                        />
                        <SButton
                            className="btn_red"
                            buttonName={'지시취소'}
                            type={'btnCancel'}
                            onClick={btnCancel}
                        />
                        <SButton
                            className="btn_red"
                            buttonName={'삭제'}
                            type={'btnPlanDelete'}
                            onClick={btnPlanDelete}
                        />
                        <SButton
                            buttonName={'바코드발행'}
                            type={'btnCreateBarcode'}
                            onClick={openBarcodeModal}
                        />
                    </React.Fragment>
                );
            }
        } else {
            return (
                <React.Fragment>
                    <SButton
                        className="btn_red"
                        buttonName={'생산시간 입력'}
                        type={'btnProdDtmInsert'}
                        onClick={openProdDtModal}
                    />
                    <SButton
                        className="btn_red"
                        buttonName={'생산정보'}
                        type={'btnProdInfo'}
                        onClick={openOrderHelperModal}
                    />
                    <SButton
                        className="btn_red"
                        buttonName={'긴급지시'}
                        type={'btnEmergencyWorkOrder'}
                        onClick={emergencyOrder}
                    />
                    <SButton
                        className="btn_red"
                        buttonName={'작업요청확인'}
                        type={'btnRequireAccept'}
                        onClick={requireAccept}
                    />
                    <SButton
                        className="btn_red"
                        buttonName={'지시'}
                        type={'btnWorkOrderByFD'}
                        onClick={packingOrders}
                    />
                    <SButton
                        className="btn_red"
                        buttonName={'지시완료'}
                        type={'btnWorkEndByFD'}
                        onClick={packingCompletion}
                    />
                    <SButton
                        buttonName={'바코드발행'}
                        type={'btnCreateBarcode'}
                        onClick={openBarcodeModal}
                    />
                </React.Fragment>
            );
        }
    }
}

export default WorkorderMiddleItem;