import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setWorkorderOutputGridApi: stores.workorderStore.setWorkorderOutputGridApi
    , workorderOutputList: stores.workorderStore.workorderOutputList
    , openWorkorderOutputModal: stores.workorderStore.openWorkorderOutputModal
    , pinnedBottomRowData: stores.workorderStore.pinnedBottomRowData
}))

class WorkorderOutputContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setWorkorderOutputGridApi, workorderOutputList, openWorkorderOutputModal, pinnedBottomRowData } = this.props;

        return (
            <SGrid
                grid={'workorderOutputGrid'}
                gridApiCallBack={setWorkorderOutputGridApi}
                rowData={workorderOutputList}
                editable={false}
                rowDoubleClick={openWorkorderOutputModal}
                pinnedBottomRowData={pinnedBottomRowData}
                suppressRowClickSelection={true}
            />
        );
    }
}
export default WorkorderOutputContents;