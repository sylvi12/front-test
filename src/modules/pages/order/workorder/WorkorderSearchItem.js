import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SDatePicker from 'components/override/datepicker/SDatePicker';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SButton from 'components/atoms/button/SButton';
import SInput from 'components/atoms/input/SInput';
import STreeSelectBox from 'components/override/tree/STreeSelectBox';
import SBuSelectBox from 'components/override/business/SBuSelectBox'

import WorkorderStore from 'modules/pages/order/workorder/service/WorkorderStore';

@inject(stores => ({
    buId: stores.workorderStore.buId
    , handleChange: stores.workorderStore.handleChange
    , buSelectionChanged: stores.workorderStore.buSelectionChanged

    , lineId: stores.workorderStore.lineId
    , lineGroup: stores.workorderStore.lineGroup
    , lineSelectionChanged: stores.workorderStore.lineSelectionChanged

    , dateType_s: stores.workorderStore.dateType_s

    , startDate: stores.workorderStore.startDate
    , endDate: stores.workorderStore.endDate

    , woStatus: stores.workorderStore.woStatus
    , woStatusSelectionChanged: stores.workorderStore.woStatusSelectionChanged

    , item: stores.workorderStore.item
    , handleSearchClick: stores.workorderStore.handleSearchClick

    , userInfo: stores.workorderStore.userInfo
}))

class WorkorderSearchItem extends Component {

    constructor(props) {
        super(props);
    }

    async componentWillMount() {
        if (WorkorderStore.alphaCheck == false) {
            await WorkorderStore.getBu();
            await WorkorderStore.getLine();
        }
    }

    render() {

        const { buId, handleChange, buSelectionChanged
            , lineId, lineGroup, lineSelectionChanged
            , dateType_s
            , startDate, endDate
            , woStatus, woStatusSelectionChanged
            , item
            , handleSearchClick
            , userInfo
        } = this.props;
        console.log(userInfo.outsourcingYn);
        return (

            <React.Fragment>

                <SBuSelectBox
                    title={"공장"}
                    id={"buId"}
                    value={buId}
                    onChange={handleChange}
                    onSelectionChange={buSelectionChanged}
                />
                {userInfo.outsourcingYn == null || userInfo.outsourcingYn == undefined || userInfo.outsourcingYn == 'N' ? (
                    <SSelectBox
                        title={"생산라인"}
                        id={"lineId"}
                        value={lineId}
                        pk={"lineId,buId"}
                        optionGroup={lineGroup}
                        valueMember={'lineId'}
                        displayMember={'lineNm'}
                        onChange={handleChange}
                        onSelectionChange={lineSelectionChanged}
                        contentWidth={140}
                        addOption={"전체"}
                    />
                ) : (
                        <SSelectBox
                            title={"생산라인"}
                            id={"lineId"}
                            value={lineId}
                            pk={"lineId,buId"}
                            optionGroup={lineGroup}
                            valueMember={'lineId'}
                            displayMember={'lineNm'}
                            onChange={handleChange}
                            onSelectionChange={lineSelectionChanged}
                            contentWidth={140}
                        />
                    )}
                <SSelectBox
                    title={" "}
                    titleWidth={50}
                    id={"dateType_s"}
                    value={dateType_s}
                    codeGroup={"SYC003"}
                    onChange={handleChange}
                    contentWidth={90}
                />
                <SDatePicker
                    id={"startDate"}
                    value={startDate}
                    onChange={handleChange}
                />
                <SDatePicker
                    title={"~"}
                    id={"endDate"}
                    value={endDate}
                    onChange={handleChange}
                />
                <STreeSelectBox
                    title={"상태"}
                    id={"woStatus"}
                    value={woStatus}
                    codeGroup={"MFD004"}
                    onChange={handleChange}
                    addOption={"전체"}
                    excludeCode={"99"}
                    onSelectionChange={woStatusSelectionChanged}
                    contentWidth={220}
                />
                <SInput
                    title={'제품'}
                    id={'item'}
                    onChange={handleChange}
                    onEnterKeyDown={handleSearchClick}
                    value={item}
                    contentWidth={150}
                />
                <div className='search_item_btn'>
                    <SButton
                        buttonName={'조회'}
                        type={'btnSearch'}
                        onClick={handleSearchClick} />
                </div>
            </React.Fragment>
        )
    }
}

export default WorkorderSearchItem;