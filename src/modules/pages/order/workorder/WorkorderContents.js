import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setGridApi: stores.workorderStore.setGridApi
    , workOrderList: stores.workorderStore.workOrderList
    , onSelectionChanged: stores.workorderStore.onSelectionChanged
    , openOrderHelperModal: stores.workorderStore.openOrderHelperModal
}))

class WorkorderContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setGridApi, workOrderList, onSelectionChanged, openOrderHelperModal } = this.props;

        return (
            <SGrid
                grid={'workOrderGrid'}
                gridApiCallBack={setGridApi}
                rowData={workOrderList}                
                editable={false}
                rowDoubleClick={openOrderHelperModal}
                onCellClicked={onSelectionChanged}
                suppressRowClickSelection={true}
                headerHeight={0}
            />
        );
    }
}
export default WorkorderContents;