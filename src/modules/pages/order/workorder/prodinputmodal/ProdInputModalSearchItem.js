import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SDatePicker from 'components/override/datepicker/SDatePicker';
import SNumericInput from 'components/override/numericinput/SNumericInput';
import SButton from 'components/atoms/button/SButton';
import SInput from 'components/atoms/input/SInput';



@inject(stores => ({
	 orderNm_p: stores.workorderStore.orderNm_p
	,itemCd_p:stores.workorderStore.itemCd_p
	,itemNm_p:stores.workorderStore.itemNm_p
	,prodDt_p:stores.workorderStore.prodDt_p
	,orderQty_p:stores.workorderStore.orderQty_p
	,prodQty_p:stores.workorderStore.prodQty_p
	,text_p:stores.workorderStore.text_p
	,prodInputSave: stores.workorderStore.prodInputSave
	,handleChange: stores.workorderStore.handleChange
}))
class ProdInputModalSearchItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {
		const { orderNm_p, itemCd_p, itemNm_p, prodDt_p, orderQty_p, prodQty_p, text_p
				,prodInputSave, handleChange } = this.props;
        return (
            <React.Fragment>
				<SButton
					buttonName={'저장'}
					type={'btnProdOutInsert'}
					onClick={prodInputSave}
				/>
				<table style={{ width: "100%" }}>
					<tbody>
						<tr>
							<td className="th">투입일자</td>
							<td>
								<SDatePicker
									id={"prodDt_p"}
									value={prodDt_p}
									onChange={handleChange}
									readOnly={false}
								/>
							</td>
							<td className="th">투입량</td>
							<td>
								<SNumericInput
									id={"prodQty_p"}
									value={prodQty_p}
									readOnly={true}
								/>
							</td>
						</tr>
						<tr>
							<td className="th">비고</td>
							<td colSpan='3'>
								<SInput
									id={"text_p"}
									value={text_p}
									onChange={handleChange}
									contentWidth={440}
								/>
							</td>
						</tr>
					</tbody>
				</table>
            </React.Fragment>
        )
    }
}

export default ProdInputModalSearchItem;

