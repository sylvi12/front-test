import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setProdInputDrGridApi: stores.workorderStore.setProdInputDrGridApi
	,prodInputDrList: stores.workorderStore.prodInputDrList
}))

class ProdInputDrModalContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setProdInputDrGridApi, prodInputDrList } = this.props;

        return (
                <SGrid
                    grid={'prodInputGridApi'}
                    gridApiCallBack={setProdInputDrGridApi}
                    rowData={prodInputDrList}
                    suppressRowClickSelection={true}
                    cellReadOnlyColor={true}
                    editable={false}
                />
        );
    }
}
export default ProdInputDrModalContents;