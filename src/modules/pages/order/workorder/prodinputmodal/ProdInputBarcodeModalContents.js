import React, { Component } from "react";
import { inject } from "mobx-react";

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
	setBarcodeInputModalGridApi: stores.workorderStore.setBarcodeInputModalGridApi
	, locationBarcodeUpsertModalList: stores.workorderStore.locationBarcodeUpsertModalList
	, locationBarcodeUpsertModalDoubleClick: stores.workorderStore.locationBarcodeUpsertModalDoubleClick
}))

class ProdInputBarcodeModelContents extends Component {

	constructor(props) {
		super(props);
	}

	render() {
		const {
			setBarcodeInputModalGridApi
			, locationBarcodeUpsertModalList
			, locationBarcodeUpsertModalDoubleClick
		} = this.props;

		return (
			<SGrid
				grid={'barcodeInputModalGrid'}
				gridApiCallBack={setBarcodeInputModalGridApi}
				rowData={locationBarcodeUpsertModalList}
				rowDoubleClick={locationBarcodeUpsertModalDoubleClick}
				suppressRowClickSelection={true}
				editable={false}
			/>
		);
	}
}

export default ProdInputBarcodeModelContents;