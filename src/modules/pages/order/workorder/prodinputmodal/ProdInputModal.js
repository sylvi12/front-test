import React, { Component } from "react";
import { inject } from "mobx-react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";
import SModal from "components/override/modal/SModal";

import ProdInputModalSearchItem from "modules/pages/order/workorder/prodinputmodal/ProdInputModalSearchItem";
import ProdInputModalMiddleItem from "modules/pages/order/workorder/prodinputmodal/ProdInputModalMiddleItem";
import ProdInputModalContents from "modules/pages/order/workorder/prodinputmodal/ProdInputModalContents";

import ProdInputBarcodeModal from "modules/pages/order/workorder/prodinputmodal/ProdInputBarcodeModal";

@inject(stores => ({
     locationBarcodeUpsertModalIsOpen: stores.workorderStore.locationBarcodeUpsertModalIsOpen
    ,closeLocationBarcodeUpsertModal: stores.workorderStore.closeLocationBarcodeUpsertModal
}))

class ProdInputModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { 
             locationBarcodeUpsertModalIsOpen
            ,closeLocationBarcodeUpsertModal
        } = this.props;
        return (
            <React.Fragment>
                <div className="nonStyle">
                    <SearchTemplate searchItem={<ProdInputModalSearchItem />} />
                    </div>
                    <ContentsMiddleTemplate contentSubTitle={'재포장투입'} middleItem={<ProdInputModalMiddleItem />} />
                    <ContentsTemplate id='prodInput' contentItem={<ProdInputModalContents />} />

                <SModal
                    formId={"order_prod_barcode_p"}
                    isOpen={locationBarcodeUpsertModalIsOpen}
                    onRequestClose={closeLocationBarcodeUpsertModal}
                    contentLabel="바코드 추가"
                    ContentTemplate={'prodInputBarcodeModal'}
                    width={800}
                    height={400}
                    contents={<ProdInputBarcodeModal />}
                />
            </React.Fragment>
        )
    }
}

export default ProdInputModal;