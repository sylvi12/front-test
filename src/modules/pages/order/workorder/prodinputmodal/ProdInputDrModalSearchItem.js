import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SDatePicker from 'components/override/datepicker/SDatePicker';
import SButton from 'components/atoms/button/SButton';
import SNumericInput from 'components/override/numericinput/SNumericInput';
import SInput from 'components/atoms/input/SInput';
import ItemHelper from 'components/codehelper/ItemHelper';

@inject(stores => ({
    orderNo_p: stores.workorderStore.orderNo_p
    , prodsQty_p: stores.workorderStore.prodsQty_p
    , orderQty_p: stores.workorderStore.orderQty_p
    , itemProdHelperOption: stores.workorderStore.itemProdHelperOption
    , selectedProdItem: stores.workorderStore.selectedProdItem
    , prodInputDrSearch: stores.workorderStore.prodInputDrSearch
    , handleChange: stores.workorderStore.handleChange
}))

class ProdInputDrModalSearchItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { orderNo_p, prodsQty_p, orderQty_p
            , itemProdHelperOption, selectedProdItem
            , prodInputDrSearch, handleChange } = this.props;
        return (
            <React.Fragment>
                <div className='search_item_btn'>
                <SButton
                    buttonName={'조회'}
                    type={'btnSearch'}
                    onClick={prodInputDrSearch}
                />
                </div>
                <div className='nonStyle'>
                    <table style={{ width: "100%" }}>
                        <tbody>
                            <tr>
                                <td className="th">지시번호</td>
                                <td>
                                    <SInput
                                        id={"orderNo_p"}
                                        value={orderNo_p}
                                        readOnly={true}
                                    />
                                </td>
                                <td className="th">제품코드/명</td>
                                <td>
                                    <ItemHelper
                                        selectedItemId={"selectedProdItem"}
                                        helperOption={itemProdHelperOption}
                                        onChange={handleChange}
                                        defaultValue={selectedProdItem}
                                        cdName={'itemCd'}
                                        cdNmName={'itemNm'}
                                    />
                                </td>
                                <td className="th">생산량/지시량</td>
                                <td>
                                    <SNumericInput
                                        id={"prodsQty_p"}
                                        value={prodsQty_p}
                                        readOnly={true}
                                    />
                                    <SNumericInput
                                        title={'/'}
                                        titleWidth={20}
                                        id={"orderQty_p"}
                                        value={orderQty_p}
                                        readOnly={true}
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </React.Fragment>
        )
    }
}

export default ProdInputDrModalSearchItem;

