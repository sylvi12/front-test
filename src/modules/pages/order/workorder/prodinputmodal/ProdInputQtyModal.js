import React, { Component } from "react";
import { inject } from "mobx-react";

import SearchTemplate from "components/template/SearchTemplate";
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SButton from 'components/atoms/button/SButton';
import SNumericInput from 'components/override/numericinput/SNumericInput';
import SInput from 'components/atoms/input/SInput';
import ItemHelper from 'components/codehelper/ItemHelper';

@inject(stores => ({
	prodInputQtySave: stores.workorderStore.prodInputQtySave
	, prodDt_q: stores.workorderStore.prodDt_q
	, selectedProdQtyItem: stores.workorderStore.selectedProdQtyItem
	, handleHelperProdResult: stores.workorderStore.handleHelperProdResult
	, itemProdQtyHelperOption: stores.workorderStore.itemProdQtyHelperOption
	, prodQty_q: stores.workorderStore.prodQty_q
	, text_q: stores.workorderStore.text_q
	, handleChange: stores.workorderStore.handleChange
}))

class ProdInputQtyModal extends Component {

	constructor(props) {
		super(props);
	}

	render() {

		const {
			prodInputQtySave
			, prodDt_q
			, selectedProdQtyItem
			, handleHelperProdResult
			, itemProdQtyHelperOption
			, prodQty_q
			, text_q
			, handleChange
		} = this.props;
		return (
			<React.Fragment>
				<div className="nonStyle">
					<SearchTemplate searchItem={
					<ProdInputQtyModalSearchItem
					prodInputQtySave={prodInputQtySave}
					prodDt_q={prodDt_q}
					selectedProdQtyItem={selectedProdQtyItem}
					handleHelperProdResult={handleHelperProdResult}
					itemProdQtyHelperOption={itemProdQtyHelperOption}
					prodQty_q={prodQty_q}
					text_q={text_q}
					handleChange={handleChange}
					 />
					} />
				</div>
			</React.Fragment>
		)
	}
}

export default ProdInputQtyModal;



const ProdInputQtyModalSearchItem = ({ 
	prodInputQtySave
	, prodDt_q
	, selectedProdQtyItem
	, handleHelperProdResult
	, itemProdQtyHelperOption
	, prodQty_q
	, text_q
	, handleChange }) => {
	return (
		<React.Fragment>
				<div className='search_item_btn'>
				<SButton
					buttonName={'저장'}
					type={'btnProdOutInsert'}
					onClick={prodInputQtySave}
				/>
				</div>
				<table style={{ width: "100%" }}>
					<tbody>
						<tr>
							<td className="th">투입일자</td>
							<td>
								<SDatePicker
									id={"prodDt_q"}
									value={prodDt_q}
									onChange={handleChange}
									readOnly={false}
								/>
							</td>
							<td className="th">투입제품</td>
                                <td>
                                    <ItemHelper
										selectedItemId={"selectedProdQtyItem"}
                                        defaultValue={selectedProdQtyItem} //기본값 변수
										onChange={handleChange}
										onHelperResult={handleHelperProdResult} //제품 선택후 결과 반환 func
                                        helperOption={itemProdQtyHelperOption} // 인스턴스 생성 및 세팅 변수
                                        cdName={'itemCd'}
                                        cdNmName={'itemNm'}
                                    />
                                </td>
							<td className="th">투입량</td>
							<td>
								<SNumericInput
									id={"prodQty_q"}
									value={prodQty_q}
									onChange={handleChange}
								/>
							</td>
						</tr>
						<tr>
							<td className="th">비고</td>
							<td colSpan='5'>
								<SInput
									id={"text_q"}
									value={text_q}
									onChange={handleChange}
									contentWidth={440}
								/>
							</td>
						</tr>
					</tbody>
				</table>
		</React.Fragment>
	)
}