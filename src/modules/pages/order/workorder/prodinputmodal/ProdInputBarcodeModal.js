import React, { Component } from 'react';

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import ProdInputBarcodeModalSearchItem from "modules/pages/order/workorder/prodinputmodal/ProdInputBarcodeModalSearchItem";
import ProdInputBarcodeModalMiddleItem from "modules/pages/order/workorder/prodinputmodal/ProdInputBarcodeModalMiddleItem";
import ProdInputBarcodeModalContents from "modules/pages/order/workorder/prodinputmodal/ProdInputBarcodeModalContents";

class ProdInputBarcodeModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <React.Fragment>
                <SearchTemplate searchItem={<ProdInputBarcodeModalSearchItem />} />
                <ContentsMiddleTemplate contentSubTitle={'LOT 정보'} middleItem={<ProdInputBarcodeModalMiddleItem />} />
                <ContentsTemplate id='prodInputBarcodeModal' contentItem={<ProdInputBarcodeModalContents />} />
            </React.Fragment>
        );
    }
}

export default ProdInputBarcodeModal;