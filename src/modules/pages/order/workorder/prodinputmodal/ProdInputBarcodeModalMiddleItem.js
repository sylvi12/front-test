import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    locationBarcodeUpsertModalSave: stores.workorderStore.locationBarcodeUpsertModalSave
}))

class ProdInputBarcodeModalMiddleItem extends Component {

    render() {

        const { locationBarcodeUpsertModalSave } = this.props;
        return (
            <React.Fragment>
                <SButton buttonName={"선택"} onClick={locationBarcodeUpsertModalSave} type={"btnProdBarcodeInsert"} />
            </React.Fragment>
        )
    }
}

export default ProdInputBarcodeModalMiddleItem;