import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setProdInputGridApi: stores.workorderStore.setProdInputGridApi
	,prodInputList: stores.workorderStore.prodInputList
}))

class ProdInputModalContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setProdInputGridApi, prodInputList } = this.props;
        return (
                <SGrid
                    grid={'prodInputGridApi'}
                    gridApiCallBack={setProdInputGridApi}
                    rowData={prodInputList}
                    suppressRowClickSelection={true}
                    cellReadOnlyColor={true}
                    editable={false}
                />
        );
    }
}
export default ProdInputModalContents;