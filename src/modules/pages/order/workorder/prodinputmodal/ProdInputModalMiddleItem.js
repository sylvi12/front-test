import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
	prodInputAdd: stores.workorderStore.prodInputAdd
	,prodInputDelete: stores.workorderStore.prodInputDelete
}))

class ProdInputModalMiddleItem extends Component {

    render() {
        const { prodInputAdd, prodInputDelete } = this.props;
        return (
            <React.Fragment>
                <SButton
                    className="btn_red"
                    buttonName={'추가'}
                    type={'btnProdOutAdd'}
                    onClick={prodInputAdd}
                />  
                <SButton
                    className="btn_red"
                    buttonName={'제거'}
                    type={'btnProdOutRemove'}
                    onClick={prodInputDelete}
                />
            </React.Fragment>
        )
    }
}

export default ProdInputModalMiddleItem;