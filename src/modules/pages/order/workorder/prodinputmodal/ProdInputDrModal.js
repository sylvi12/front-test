import React, { Component } from "react";
import { inject } from "mobx-react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";
import SModal from "components/override/modal/SModal";

import ProdInputDrModalSearchItem from "modules/pages/order/workorder/prodinputmodal/ProdInputDrModalSearchItem";
import ProdInputDrModalMiddleItem from "modules/pages/order/workorder/prodinputmodal/ProdInputDrModalMiddleItem";
import ProdInputDrModalContents from "modules/pages/order/workorder/prodinputmodal/ProdInputDrModalContents";

import ProdInputModal from "modules/pages/order/workorder/prodinputmodal/ProdInputModal";
import ProdInputQtyModal from "modules/pages/order/workorder/prodinputmodal/ProdInputQtyModal";

@inject(stores => ({
    prodInputModalIsOpen: stores.workorderStore.prodInputModalIsOpen
    , closeProdInputModal: stores.workorderStore.closeProdInputModal
    , prodInputQtyModalIsOpen: stores.workorderStore.prodInputQtyModalIsOpen
    , closeProdInputQtyModal: stores.workorderStore.closeProdInputQtyModal
}))

class ProdInputDrModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { 
            prodInputModalIsOpen
            , closeProdInputModal
            , prodInputQtyModalIsOpen
            , closeProdInputQtyModal
        } = this.props;
        return (
            <React.Fragment>
                <div className="nonStyle">
                    <SearchTemplate searchItem={<ProdInputDrModalSearchItem />} />
                </div>
                <ContentsMiddleTemplate contentSubTitle={'재포장투입일보'} middleItem={<ProdInputDrModalMiddleItem />} />
                <ContentsTemplate id='prodInputDr' contentItem={<ProdInputDrModalContents />} />

                <SModal
                    formId={"order_prod_output_p"}
                    isOpen={prodInputQtyModalIsOpen}
                    onRequestClose={closeProdInputQtyModal}
                    contentLabel="재포장지시"
                    ContentTemplate={'prodInputQtyModal'}
                    width={800}
                    height={150}
                    id='prodInputDrQtyModal'
                    contents={<ProdInputQtyModal />}
                />
                <SModal
                    formId={"order_prod_output_p"}
                    isOpen={prodInputModalIsOpen}
                    onRequestClose={closeProdInputModal}
                    contentLabel="재포장지시"
                    ContentTemplate={'prodInputModal'}
                    width={950}
                    height={450}
                    id='prodInputDrModal'
                    contents={<ProdInputModal />}
                />

            </React.Fragment>
        )
    }
}

export default ProdInputDrModal;