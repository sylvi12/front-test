import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';
import SDatePicker from 'components/override/datepicker/SDatePicker';

@inject(stores => ({
    prodStartDt_p: stores.workorderStore.prodStartDt_p
    , prodEndDt_p: stores.workorderStore.prodEndDt_p
    , handleChange: stores.workorderStore.handleChange
    , prodInputDrQtyInput: stores.workorderStore.prodInputDrQtyInput
    , prodInputDrInput: stores.workorderStore.prodInputDrInput
    , prodInputDrCancel: stores.workorderStore.prodInputDrCancel
}))

class ProdInputDrModalMiddleItem extends Component {

    render() {
        const { prodStartDt_p, prodEndDt_p, handleChange
            ,prodInputDrQtyInput
            , prodInputDrInput
            , prodInputDrCancel } = this.props;
        return (
            <React.Fragment>
                <SDatePicker
                    title={"투입일자"}
                    id={"prodStartDt_p"}
                    value={prodStartDt_p}
                    onChange={handleChange}
                />
                <SDatePicker
                    title={'~'}
                    id={"prodEndDt_p"}
                    value={prodEndDt_p}
                    onChange={handleChange}
                    marginRight={10}
                />
                <SButton
                    className="btn_red"
                    buttonName={'추가(수량)'}
                    type={'btnProdOutputQtyInsert'}
                    onClick={prodInputDrQtyInput}
                />
                <SButton
                    className="btn_red"
                    buttonName={'추가(바코드)'}
                    type={'btnProdOutputInsert'}
                    onClick={prodInputDrInput}
                />
                <SButton
                    className="btn_red"
                    buttonName={'삭제'}
                    type={'btnProdOutputCancel'}
                    onClick={prodInputDrCancel}
                />
            </React.Fragment>
        )
    }
}

export default ProdInputDrModalMiddleItem;