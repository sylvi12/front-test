import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SDatePicker from 'components/override/datepicker/SDatePicker';
import SInput from 'components/atoms/input/SInput'
import SButton from 'components/atoms/button/SButton'

@inject(stores => ({
	locationBarcodeUpsertModalBarcode: stores.workorderStore.locationBarcodeUpsertModalBarcode,
	locationBarcodeUpsertModalProdDt: stores.workorderStore.locationBarcodeUpsertModalProdDt,
	locationBarcodeUpsertModalItemNmOrItemCd: stores.workorderStore.locationBarcodeUpsertModalItemNmOrItemCd,
	handleChange: stores.workorderStore.handleChange,
	locationBarcodeUpsertModalSearch: stores.workorderStore.locationBarcodeUpsertModalSearch
}))
class ProdInputBarcodeModalSearchItem extends Component {

	constructor(props) {
		super(props);
	}

	render() {

		const { locationBarcodeUpsertModalBarcode
			, locationBarcodeUpsertModalProdDt
			, locationBarcodeUpsertModalItemNmOrItemCd
			, handleChange
			, locationBarcodeUpsertModalSearch
		} = this.props;

		return (
			<React.Fragment>
				<SInput
					title={"바코드"}
					id={"locationBarcodeUpsertModalBarcode"}
					value={locationBarcodeUpsertModalBarcode}
					onChange={handleChange}
					onEnterKeyDown={locationBarcodeUpsertModalSearch}
				/>

				<SDatePicker
					title={"생산일자"}
					id={"locationBarcodeUpsertModalProdDt"}
					value={locationBarcodeUpsertModalProdDt}
					onChange={handleChange}
				/>

				<SInput
					title={"제품명/코드"}
					id={"locationBarcodeUpsertModalItemNmOrItemCd"}
					value={locationBarcodeUpsertModalItemNmOrItemCd}
					onChange={handleChange}
					onEnterKeyDown={locationBarcodeUpsertModalSearch}
				/>

				<div className='search_item_btn' style={{ right: "" }}>
					<SButton
						buttonName={'조회'}
						type={'btnSearch'}
						onClick={locationBarcodeUpsertModalSearch}
					/>
				</div>

			</React.Fragment>
		)
	}
}

export default ProdInputBarcodeModalSearchItem;