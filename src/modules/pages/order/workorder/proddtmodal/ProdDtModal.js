import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import SNumericInput from 'components/override/numericinput/SNumericInput'
import SInput from 'components/atoms/input/SInput';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import STimePicker from 'components/override/datepicker/STimePicker';
import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    prodDtModalOrderNo: stores.workorderStore.prodDtModalOrderNo
    , handleChange: stores.workorderStore.handleChange

    , prodDtModalLineNm: stores.workorderStore.prodDtModalLineNm

    , prodDtModalWoStatusNm: stores.workorderStore.prodDtModalWoStatusNm

    , prodDtModalItemCd: stores.workorderStore.prodDtModalItemCd

    , prodDtModalItemNm: stores.workorderStore.prodDtModalItemNm

    , prodDtModalProdStartDt: stores.workorderStore.prodDtModalProdStartDt
    , prodDtModalProdStartDtm: stores.workorderStore.prodDtModalProdStartDtm

    , prodDtModalProdEndDt: stores.workorderStore.prodDtModalProdEndDt
    , prodDtModalProdEndDtm: stores.workorderStore.prodDtModalProdEndDtm

    , prodDtModalSave: stores.workorderStore.prodDtModalSave
}))
@observer
class ProdDtModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { prodDtModalOrderNo, handleChange
            , prodDtModalLineNm
            , prodDtModalWoStatusNm
            , prodDtModalItemCd
            , prodDtModalItemNm
            , prodDtModalProdStartDt, prodDtModalProdStartDtm
            , prodDtModalProdEndDt, prodDtModalProdEndDtm
            , prodDtModalSave
        } = this.props;

        return (
            <React.Fragment>
                <table style={{ width: "100%" }}>
                    <tbody>
                        <tr>
                            <th>오더번호</th>
                            <td>
                                <SNumericInput
                                    id="prodDtModalOrderNo"
                                    value={prodDtModalOrderNo}
                                    onChange={handleChange}
                                    readOnly={true}
                                />
                            </td>
                        </tr>
                        <tr>
                            <th>라인명</th>
                            <td>
                                <SInput
                                    id="prodDtModalLineNm"
                                    value={prodDtModalLineNm}
                                    onChange={handleChange}
                                    readOnly={true}
                                />
                            </td>
                        </tr>
                        <tr>
                            <th>상태</th>
                            <td>
                                <SInput
                                    id="prodDtModalWoStatusNm"
                                    value={prodDtModalWoStatusNm}
                                    onChange={handleChange}
                                    readOnly={true}
                                />
                            </td>
                        </tr>
                        <tr>
                            <th>제품코드</th>
                            <td>
                                <SInput
                                    id="prodDtModalItemCd"
                                    value={prodDtModalItemCd}
                                    onChange={handleChange}
                                    readOnly={true}
                                />
                            </td>
                        </tr>
                        <tr>
                            <th>제품명</th>
                            <td>
                                <SInput
                                    id="prodDtModalItemNm"
                                    value={prodDtModalItemNm}
                                    onChange={handleChange}
                                    readOnly={true}
                                />
                            </td>
                        </tr>
                        <tr>
                            <th>생산 시작시간</th>
                            <td>
                                <SDatePicker
                                    id="prodDtModalProdStartDt"
                                    value={prodDtModalProdStartDt}
                                    onChange={handleChange}
                                />
                                <STimePicker
                                    id="prodDtModalProdStartDtm"
                                    value={prodDtModalProdStartDtm}
                                    onChange={handleChange}
                                />
                            </td>
                        </tr>
                        <tr>
                            <th>생산 종료시간</th>
                            <td>
                                <SDatePicker
                                    id="prodDtModalProdEndDt"
                                    value={prodDtModalProdEndDt}
                                    onChange={handleChange}
                                />
                                <STimePicker
                                    id="prodDtModalProdEndDtm"
                                    value={prodDtModalProdEndDtm}
                                    onChange={handleChange}
                                />
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div>
                    <SButton buttonName={"저장"} onClick={prodDtModalSave} type={"btnProdDtmSave"} />
                </div>
            </React.Fragment>
        );
    }
}

export default ProdDtModal;