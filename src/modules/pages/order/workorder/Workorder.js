import React, { Component } from "react";
import { inject } from "mobx-react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";
import SModal from "components/override/modal/SModal";

import ProdInputDrModal from "modules/pages/order/workorder/prodinputmodal/ProdInputDrModal";

import WorkorderSearchItem from "modules/pages/order/workorder/WorkorderSearchItem";
import WorkorderMiddleItem from "modules/pages/order/workorder/WorkorderMiddleItem2";
import WorkorderContents from "modules/pages/order/workorder/WorkorderContents";
import WorkorderModal from "modules/pages/order/workorder/workordermodal/WorkorderModal";
import WorkorderOutputMiddleItem from "modules/pages/order/workorder/WorkorderOutputMiddleItem";
import WorkorderOutputContents from "modules/pages/order/workorder/WorkorderOutputContents";
import BarcodeModal from "modules/pages/order/workorder/barcodemodal/BarcodeModal";
import ProdDtModal from "modules/pages/order/workorder/proddtmodal/ProdDtModal";
import RequireModal from "modules/pages/order/workorder/requiremodal/RequireModal";
import OrderHelper from "components/codehelper/OrderHelper";


@inject(stores => ({
    prodInputDrModalIsOpen: stores.workorderStore.prodInputDrModalIsOpen
    , closeProdInputDrModal: stores.workorderStore.closeProdInputDrModal
    , modalIsOpen: stores.workorderStore.modalIsOpen
    , closeModal: stores.workorderStore.closeModal
    , barcodeModalIsOpen: stores.workorderStore.barcodeModalIsOpen
    , closeBarcodeModal: stores.workorderStore.closeBarcodeModal
    , prodDtModalIsOpen: stores.workorderStore.prodDtModalIsOpen
    , closeProdDtModal: stores.workorderStore.closeProdDtModal
    , requireModalIsOpen: stores.workorderStore.requireModalIsOpen
    , closeRequireModal: stores.workorderStore.closeRequireModal
    , orderHelperModalIsOpen: stores.workorderStore.orderHelperModalIsOpen
    , orderNo_h: stores.workorderStore.orderNo_h
    , handleChange: stores.workorderStore.handleChange
    , orderBarcodeYN: stores.workorderStore.orderBarcodeYN
    , modalTitle: stores.workorderStore.modalTitle
}))

class Workorder extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { prodInputDrModalIsOpen, closeProdInputDrModal
            , modalIsOpen, closeModal
            , barcodeModalIsOpen, closeBarcodeModal
            , prodDtModalIsOpen, closeProdDtModal
            , requireModalIsOpen, closeRequireModal
            , orderHelperModalIsOpen, orderNo_h, handleChange, orderBarcodeYN
            , modalTitle
        } = this.props

        let height = 300;
        if (modalTitle == "계획수정") {
            height = 360;
        } else {
            height = 300;
        }

        return (
            <React.Fragment>
                <div style={{ width: '100%', height: '70%', float: 'left' }}>
                    <SearchTemplate searchItem={<WorkorderSearchItem />} />
                    {/* <div className="middle_wrap">
                        <div className="middle_line_wrap">
                            <div className="middle_line">
                                <div className="sub_tit">
                                    {'포장지시'}
                                </div>
                            </div>
                            <div className="middle_line">
                                <ul className="btn_wrap">
                                    {<WorkorderContents />}
                                </ul>
                            </div>
                        </div>
                    </div> */}
                    <ContentsMiddleTemplate contentSubTitle={'포장지시'} middleItem={<WorkorderMiddleItem />} />
                    <ContentsTemplate id='workorder' contentItem={<WorkorderContents />} />
                </div>
                <div style={{ width: '100%', height: '30%', float: 'left' }}>
                    <ContentsMiddleTemplate contentSubTitle={'생산집계 리스트'} middleItem={<WorkorderOutputMiddleItem />} />
                    <ContentsTemplate id='workorderOutput' contentItem={<WorkorderOutputContents />} />
                </div>

                <SModal
                    formId={"order_report_p"}
                    isOpen={prodInputDrModalIsOpen}
                    onRequestClose={closeProdInputDrModal}
                    contentLabel="재포장투입일보"
                    width={950}
                    height={550}
                    minWidth={750}
                    minHeight={500}
                    id='prodInputDrModal'
                    contents={<ProdInputDrModal />}
                />

                <SModal
                    formId={"order_insert_p"}
                    isOpen={modalIsOpen}
                    onRequestClose={closeModal}
                    contentLabel={modalTitle}
                    width={500}
                    height={height}
                    minWidth={500}
                    minHeight={height}
                    contents={<WorkorderModal />}
                />

                <SModal
                    formId={"order_barcode_insert_p"}
                    isOpen={barcodeModalIsOpen}
                    onRequestClose={closeBarcodeModal}
                    contentLabel="바코드 발행"
                    width={600}
                    height={310}
                    minWidth={600}
                    minHeight={310}
                    contents={<BarcodeModal />}
                />

                <SModal
                    formId={"order_prod_dtm_p"}
                    isOpen={prodDtModalIsOpen}
                    onRequestClose={closeProdDtModal}
                    contentLabel="생산시간 입력"
                    width={400}
                    height={350}
                    minWidth={400}
                    minHeight={350}
                    contents={<ProdDtModal />}
                />

                <SModal
                    formId={"order_require_save_p"}
                    isOpen={requireModalIsOpen}
                    onRequestClose={closeRequireModal}
                    contentLabel="작업요청"
                    width={300}
                    height={170}
                    minWidth={300}
                    minHeight={170}
                    contents={<RequireModal />}
                />

                <OrderHelper
                    formId={"order_prod_info_p"}
                    id={'orderHelperModalIsOpen'}
                    orderHelperModalIsOpen={orderHelperModalIsOpen}
                    title={'생산정보'}
                    onChange={handleChange}
                    orderNo={orderNo_h}
                    barcodeYn={orderBarcodeYN}
                />

            </React.Fragment>
        )
    }
}

export default Workorder;