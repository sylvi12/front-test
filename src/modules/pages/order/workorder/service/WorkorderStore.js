import { observable, action } from 'mobx';
import moment from 'moment';
import 'moment/locale/ko';

import Alert from 'components/override/alert/Alert';
import GridUtil from 'components/override/grid/utils/GridUtil';
import Validator from 'components/override/utils/Validator';
import ObjectUtility from 'utils/object/ObjectUtility'

import WorkorderRepository from 'modules/pages/order/workorder/service/WorkorderRepository';
import CommonStore from 'modules/pages/common/service/CommonStore';
import ItemOptionModel from 'components/codehelper/model/ItemOptionModel';
import EmployeeOptionModel from "components/codehelper/model/EmployeeOptionModel";
import EtcRepository from 'modules/pages/inout/etc/service/EtcRepository';
import OutputRepository from 'modules/pages/order/output/service/OutputRepository';

import LocationMoveStore from "modules/pages/inventory/locationMove/service/LocationMoveStore";
import BaseStore from 'utils/base/BaseStore';

class WorkorderStore extends BaseStore {
    /* orderHelperModal */
    @observable orderNo_h = ''; // OrderHelper 지시번호(ORDER_NO) 변수명
    @observable orderBarcodeYN = ''; // OrderHelper 바코드사용여부 전달
    @observable orderHelperModalIsOpen = false;
    /* EmployeeOptionModel */
    @observable employeeOptionModel = new EmployeeOptionModel().optionModel;

    // Workorder SearchItem
    @observable alphaCheck = false;
    @observable buId = '';
    @observable lineId = '';
    @observable lineGroup = [];
    @observable dateType_s = '01';
    @observable startDate = moment().add(-7, 'days').format('YYYYMMDD');
    @observable endDate = moment().add(2, 'days').format('YYYYMMDD');
    @observable woStatus = '';
    @observable item = '';
    @observable selectedLine = {};

    // Workorder MiddleItem
    @observable middleButtonType = '';

    // Workorder Contents
    workOrderGridApi = undefined;
    @observable workOrderList = [];

    // WorkorderOutputContents    
    workorderOutputGridApi = undefined;
    @observable workorderOutputList = [];
    @observable pinnedBottomRowData = [];

    // WorkOrderModal 변수
    @observable modalTitle = '';
    @observable modalMode = "";
    @observable modalIsOpen = false;
    @observable workOrderModalLineId = '';
    @observable itemHelperOption = new ItemOptionModel().optionModel;
    @observable selectedItem = {};
    @observable workOrderModalPlanStartDtm = '';
    @observable workOrderModalPlanEndDtm = '';
    @observable workOrderModalOrderQty = 0;
    @observable workOrderModalOrderUom = '';
    @observable workOrderModalMemo = '';
    @observable workOrderModalOrderNo = 0;
    @observable workOrderModalStatusNm = '';
    @observable workOrderModalStatus = '';
    @observable selectedLine2 = {}
    statusChange = '';
    workOrderModalItemId = 0;

    // 긴급지시 용 변수
    @observable emergencyYN = '';
    // 계획등록 팝업 유효성 검사
    validator = new Validator([
        {
            field: 'workOrderModalOrderUom',
            method: 'isEmpty',
            message: '제품',
        },
        {
            field: 'workOrderModalOrderQty',
            method: 'isEmpty',
            message: '작업지시량',
        }
    ]);

    // RequireModal 변수
    @observable requireModalIsOpen = false;
    @observable requireModalMemo = "";
    requireSaveData = [];

    // BarcodeModal 변수
    @observable barcodeModalIsOpen = false;
    @observable barcodeModalOrderNo = 0;
    @observable barcodeModalLotNo = "";
    barcodeModalItemId = 0;
    @observable barcodeModalItemCd = '';
    @observable barcodeModalItemNm = '';
    addDate = 0;
    @observable barcodeModalProdDt = '';
    @observable barcodeModalExpiryDt = '';
    @observable barcodeModalProdUomQty = 0;
    @observable barcodeModalProdUom = '';
    @observable barcodeModalPackQty = 0;
    @observable barcodeModalPackUom = '';
    @observable barcodeModalProdQty = 0;
    @observable rackTypeHide = true;
    @observable barcodeModalRackType = "";
    prodEmpNo = '';
    @observable prodEmpNm = '';
    packerEmpNo = '';
    @observable packerEmpNm = '';
    @observable minSeq = 0;
    @observable maxSeq = 0;
    @observable barcodeModalMemo = '';
    // @observable prodEmp = {};
    // @observable packEmp = {};
    // 계획등록 팝업 유효성 검사
    barcodeValidator = new Validator([
        {
            field: 'barcodeModalLotNo',
            method: 'isEmpty',
            message: 'LOT NO',
        },
        {
            field: 'barcodeModalPackQty',
            method: 'isEmpty',
            message: '포장수량',
        },
        // {
        //     field: 'prodEmpNm',
        //     method: 'isEmpty',
        //     message: '생산자',
        // },
        // {
        //     field: 'packerEmpNm',
        //     method: 'isEmpty',
        //     message: '포장자',
        // },
        {
            field: 'minSeq',
            method: 'isEmpty',
            message: '일련번호',
        },
        {
            field: 'maxSeq',
            method: 'isEmpty',
            message: '일련번호',
        }
    ]);

    /* ProdDtModal variables */
    @observable prodDtModalIsOpen = false;
    @observable prodDtModalOrderNo = 0;
    @observable prodDtModalLineNm = '';
    @observable prodDtModalWoStatusNm = '';
    prodDtModalItemId = 0;
    @observable prodDtModalItemCd = '';
    @observable prodDtModalItemNm = '';
    @observable prodDtModalProdStartDt = '';
    @observable prodDtModalProdStartDtm = '00:00';
    @observable prodDtModalProdEndDt = '';
    @observable prodDtModalProdEndDtm = '00:00';

    /* 로그인 유저 정보 */
    @observable userInfo = {};

    constructor() {
        super();
        this.validator.valid();
        this.itemProdQtyHelperOption.barcodeYn = 'N';
        this.itemProdHelperOption.searchVisible = false;
        this.itemProdHelperOption.cdNmReadOnly = true;
        this.itemProdHelperOption.cdNmIsFocus = false;
        this.itemProdHelperOption.cdWidth = 70;
        this.itemProdHelperOption.cdNmWidth = 120;
        this.itemHelperOption.cdNmIsFocus = true;
        this.itemHelperOption.glclass = "S110,S120,S140";
        this.setInitialState();
    }

    @action.bound
    handleChange = (data) => {
        console.log(data);
        this[data.id] = data.value;
    }
    // 유통기한 계산하기
    @action.bound
    makeExpireDate = () => {
        if (this.addDate != '') {
            this.addDate = Number(this.addDate);
            this.barcodeModalExpiryDt = this.barcodeModalProdDt.getAddDays(this.addDate - 1).format("yyyy-MM-dd");
        }
        this.barcodeModalLotNo = this.barcodeModalProdDt.replace(/-/gi, '');
    }
    // 생산량 계산하기
    @action.bound
    calculateProdQty = () => {
        this.barcodeModalProdQty = this.barcodeModalProdUomQty * this.barcodeModalPackQty;
    }
    // 포장수량 계산하기
    @action.bound
    calculatePackQty = () => {
    }
    // main searchItem 공장 선택사항 변경 시
    @action.bound
    buSelectionChanged = async (selectedItem) => {
        await this.getLine();
    }
    // getBu
    @action.bound
    getBu = () => {
        this.buId = CommonStore.buId;
    }
    // search에 라인 넣기
    @action.bound
    getLine = async () => {
        //userInfo에서 아웃소싱 YN을 체크해서 Y이면 해당 라인만 가져오면 된다.
        this.userInfo = CommonStore.userInfo;
        console.log(this.userInfo);
        if (!this.userInfo.outsourcingYn && this.userInfo.outsourcingYn == "Y") {
            const searchData = {
                "loginId": this.userInfo.loginId
            }
            const { data, status } = await WorkorderRepository.getLine2(searchData);
            if (status == 200) {
                this.lineGroup = data.data;
                this.lineGroup2 = data.data;
                this.lineId = data.data[0].lineId;
                this.selectedLine = data.data[0];
                this.selectedLine2 = data.data[0];
                this.alphaCheck = true;
            }
        } else {
            const searchData = {
                "buId": this.buId
            }
            const { data, status } = await WorkorderRepository.getLine(searchData);
            if (status == 200) {
                this.lineGroup = data.data;
                this.lineGroup2 = data.data;
                if (data.data.length > 0) {
                    this.lineId = data.data[0].lineId;
                    this.selectedLine = data.data[0];
                    this.selectedLine2 = data.data[0];
                }
                this.alphaCheck = true;
            }
        }
    }
    // main searchItem 라인 선택사항 변경 시
    @action.bound
    lineSelectionChanged = async (selectedItem) => {
        this.selectedLine = selectedItem;
        // this.handleSearchClick();
    }
    // 계획등록 라인 선택사항 변경 시
    @action.bound
    lineSelectionChanged2 = async (selectedItem) => {
        this.selectedLine2 = selectedItem;
    }
    // main searchItem 상태 선택사항 변경 시
    @action.bound
    woStatusSelectionChanged = async (selectedItem) => {
        // this.handleSearchClick();
    }

    // 포장지시 grid
    @action.bound
    setGridApi = async (gridApi) => {
        this.workOrderGridApi = gridApi;
        const columnDefs = [
            {
                headerName: ""
                , children: [
                    {
                        headerName: ""
                        , field: "check"
                        , width: 29
                        , checkboxSelection: true
                        , headerCheckboxSelection: true
                        , cellClass: 'cell_align_center'
                    }
                ]
            },
            {
                headerName: "지시번호"
                , children: [
                    {
                        headerName: "지시번호"
                        , field: "orderNo"
                        , width: 60
                        , cellClass: 'cell_align_center'
                    }
                ]
            },
            {
                headerName: "상태"
                , children: [
                    {
                        headerName: "상태"
                        , field: "woStatusNm"
                        , width: 85
                        , cellClass: 'cell_align_center'
                        , cellClassRules: {
                            'cellWoStatus10': function (params) {
                                return params.data.woStatus === '10'
                            },
                            'cellWoStatus20': function (params) {
                                return params.data.woStatus === '20'
                            },
                            'cellWoStatus25': function (params) {
                                return params.data.woStatus === '25'
                            },
                            'cellWoStatus30': function (params) {
                                return params.data.woStatus === '30'
                            },
                            'cellWoStatus40': function (params) {
                                return params.data.woStatus === '40'
                            },
                            'cellWoStatus50': function (params) {
                                return params.data.woStatus === '50'
                            },
                            'cellWoStatus60': function (params) {
                                return params.data.woStatus === '60'
                            },
                            'cellWoStatus70': function (params) {
                                return params.data.woStatus === '70'
                            },
                            'cellWoStatus80': function (params) {
                                return params.data.woStatus === '80'
                            },
                            'cellWoStatus90': function (params) {
                                return params.data.woStatus === '90'
                            },
                            'cellWoStatus99': function (params) {
                                return params.data.woStatus === '99'
                            }
                        }
                    }
                ]
            },
            {
                headerName: "실적요청건"
                , children: [
                    {
                        headerName: "실적요청건"
                        , field: "reqCnt"
                        , width: 70
                        , type: "numericColumn"
                        , valueFormatter: GridUtil.numberFormatter
                    }
                ]
            },
            {
                headerName: "생산라인"
                , children: [
                    {
                        headerName: "생산라인"
                        , field: "lineNm"
                        , width: 120
                        , cellClass: 'cell_align_center'
                    }
                ]
            },
            {
                headerName: "제품코드"
                , children: [
                    {
                        headerName: "제품코드"
                        , field: "itemCd"
                        , width: 80
                        , cellClass: 'cell_align_center'
                    }
                ]
            },
            {
                headerName: "제품명"
                , children: [
                    {
                        headerName: "제품명"
                        , field: "itemNm"
                        , width: 150
                    }
                ]
            },
            {
                headerName: "관리유형"
                , children: [
                    {
                        headerName: "관리유형"
                        , field: "barcodeYN"
                        , width: 55
                        , cellClass: 'cell_align_center'
                        , valueFormatter: (params) => GridUtil.getCodeNm(params, 'SYC009')
                    }
                ]
            },
            {
                headerName: "지시일자"
                , children: [
                    {
                        headerName: "지시일자"
                        , field: "planDtm"
                        , width: 90
                        , cellClass: 'cell_align_center'
                    }
                ]
            },
            {
                headerName: "생산일자"
                , children: [
                    {
                        headerName: "생산일자"
                        , field: "prodDtm"
                        , width: 150
                        , cellClass: 'cell_align_center'
                    }
                ]
            },
            {
                headerName: "지시량",
                children: [
                    {
                        headerName: "지시량"
                        , field: "orderQty"
                        , width: 70
                        , type: "numericColumn"
                        , valueFormatter: (params) => GridUtil.numberFormatter(params, 0) //GridUtil.numberFormatter
                        , valueParser: GridUtil.numberParser
                        , pinnedRowCellRenderer: "customPinnedRowRenderer"
                        , pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
                    },
                    {
                        headerName: "지시단위"
                        , field: "orderUom"
                        , width: 40
                        , cellClass: 'cell_align_center'
                    }
                ]
            },
            {
                headerName: "지시중량",
                children: [
                    {
                        headerName: "지시중량"
                        , field: "orderQty2"
                        , width: 70
                        , type: "numericColumn"
                        , valueFormatter: (params) => GridUtil.numberFormatter(params, 0) //GridUtil.numberFormatter
                        , valueParser: GridUtil.numberParser
                        , pinnedRowCellRenderer: "customPinnedRowRenderer"
                        , pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
                    },
                    {
                        headerName: "지시중량단위"
                        , field: "orderUom2"
                        , width: 40
                        , cellClass: 'cell_align_center'
                    }
                ]
            },
            {
                headerName: "생산량"
                , children: [
                    {
                        headerName: "생산량"
                        , field: "prodQty"
                        , width: 65
                        , type: "numericColumn"
                        , valueFormatter: GridUtil.numberFormatter
                    }
                ]
            },
            {
                headerName: "실적량"
                , children: [
                    {
                        headerName: "실적량"
                        , field: "reportQty"
                        , width: 65
                        , type: "numericColumn"
                        , valueFormatter: GridUtil.numberFormatter
                    }
                ]
            },
            {
                headerName: "진척률(%)"
                , children: [
                    {
                        headerName: "진척률(%)"
                        , field: "progressRate"
                        , width: 70
                        , type: "numericColumn"
                        , valueFormatter: GridUtil.numberFormatter
                    }
                ]
            },
            {
                headerName: "계획비고"
                , children: [
                    {
                        headerName: "계획비고"
                        , field: "memo"
                        , width: 200
                    }
                ]
            },
            {
                headerName: "요청 지시비고"
                , children: [
                    {
                        headerName: "요청 지시비고"
                        , field: "orderMemo"
                        , width: 200
                    }
                ]
            }
        ];
        this.workOrderGridApi.setColumnDefs(columnDefs);
    }

    // 생산집계리스트 grid
    @action.bound
    setWorkorderOutputGridApi = async (gridApi) => {
        this.workorderOutputGridApi = gridApi;
        this.pinnedBottomRowData = [
            {
                prodShiftNm: '합계',
                prodQty: 0
            }
        ];

        const columnDefs = [
            {
                headerName: ""
                , field: "check"
                , width: 29
                , checkboxSelection: true
                , headerCheckboxSelection: true
            },
            {
                headerName: "직"
                , field: "prodShiftNm"
                , width: 50
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "팀"
                , field: "prodTeamNm"
                , width: 50
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "작성일"
                , field: "prodDt"
                , width: 75
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "시작시각"
                , field: "prodStartDt"
                , width: 60
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "종료시각"
                , field: "prodEndDt"
                , width: 60
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "지시번호"
                , field: "orderNo"
                , width: 60
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "제품코드"
                , field: "itemCd"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "제품명"
                , field: "itemNm"
                , width: 150
            },
            {
                headerName: "실적량"
                , field: "prodQty"
                , width: 65
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
                , pinnedRowCellRenderer: "customPinnedRowRenderer"
                , pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
            },
            {
                headerName: "단위"
                , field: "prodUom"
                , width: 50
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "승인상태"
                , field: "confirmStatusNm"
                , width: 65
                , cellClass: 'cell_align_center'
            },
        ];
        this.workorderOutputGridApi.setColumnDefs(columnDefs);
    }

    // 조회
    @action.bound
    handleSearchClick = async () => {
        const searchData = {
            "buId": this.buId
            , "lineId": this.lineId
            , "dateType": this.dateType_s
            , "startDate": this.startDate
            , "endDate": this.endDate
            , "woStatus": this.woStatus
            , "item": this.item
        }
        const { data, status } = await WorkorderRepository.getWorkOrders(searchData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            this.workOrderList = data.data;
            this.workOrderGridApi.setRowData(this.workOrderList);
            this.workorderOutputGridApi.setRowData([]);
        }
    }
    // 포장지시 클릭 변경시
    @action.bound
    onSelectionChanged = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.workOrderGridApi);
        this.middleButtonType = selectedRow.woStatus;
        this.searchOutput();
    }
    // 생산집계리스트 조회
    @action.bound
    searchOutput = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.workOrderGridApi);
        if (selectedRow == undefined || selectedRow.length == 0) {
            return;
        }
        let prodDt = "";
        if (selectedRow.prodStartDtm != undefined) {
            prodDt = selectedRow.prodStartDtm.substring(0, 10);
        }
        const searchData = {
            "buId": this.buId
            , "lineId": this.lineId
            , "prodDt": prodDt
            , "orderNo": selectedRow.orderNo
        }
        console.log(searchData);
        const { data, status } = await OutputRepository.getOutput(searchData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            this.workorderOutputList = data.data;
            this.workorderOutputGridApi.setRowData(this.workorderOutputList);
        }
    }

    // 포장지시 그리드 포커싱과 선택하기
    @action.bound
    setFocusAndSelect = async (orderNo) => {        
        GridUtil.setFocusByKey(this.workOrderGridApi, 'orderNo', orderNo);
        this.onSelectionChanged();
    }

    // 계획 등록
    @action.bound
    handleSaveClick = async () => {
        this.itemHelperOption.cdNmReadOnly = false;
        this.itemHelperOption.searchVisible = true;
        this.modalTitle = '계획등록';
        this.modalMode = "add";
        this.workOrderModalLineId = this.lineId;
        this.selectedLine2 = this.selectedLine;
        this.selectedItem = {};
        this.workOrderModalPlanStartDtm = moment().format('YYYYMMDD');
        this.workOrderModalPlanEndDtm = moment().format('YYYYMMDD');
        this.workOrderModalOrderQty = 0;
        this.workOrderModalOrderUom = "";
        this.workOrderModalMemo = "";
        this.workOrderModalOrderNo = 0;
        this.workOrderModalItemId = 0;
        this.workOrderModalItemCd = "";
        this.workOrderModalItemNm = "";
        this.emergencyYN = 'N';
        this.statusChange = '';
        this.modalIsOpen = true;
    }
    // itemhelper옵션 넣어주기
    @action.bound
    setItemHelperOption = async () => {
        this.itemHelperOption.buId = this.buId;
    }
    // 계획 수정
    @action.bound
    handleUpdateClick = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.workOrderGridApi);
        console.log(selectedRow);
        if (!selectedRow || selectedRow.length == 0) {
            Alert.meg("선택된 항목이 없습니다.");
            return;
        }
        if (selectedRow.woStatus >= '80') {
            Alert.meg("수정할 수 없는 상태입니다.");
            return;
        }
        if (!this.userInfo.outsourcingYn && this.userInfo.outsourcingYn == 'Y' && selectedRow.woStatus < "20") {
            Alert.meg("권한이 없습니다.");
            return;
        }
        this.itemHelperOption.cdNmReadOnly = true;
        this.itemHelperOption.searchVisible = false;
        this.selectedLine2 = this.selectedLine;
        this.modalTitle = '계획수정';
        this.modalMode = "edit";
        this.workOrderModalLineId = selectedRow.lineId;
        this.selectedItem = { "itemId": selectedRow.itemId, "itemCd": selectedRow.itemCd, "itemNm": selectedRow.itemNm };
        this.workOrderModalPlanStartDtm = selectedRow.planStartDtm;
        this.workOrderModalPlanEndDtm = selectedRow.planEndDtm;
        this.workOrderModalOrderQty = selectedRow.orderQty;
        this.workOrderModalOrderUom = selectedRow.orderUom;
        this.workOrderModalMemo = selectedRow.memo;
        this.workOrderModalOrderNo = selectedRow.orderNo;
        this.workOrderModalStatusNm = selectedRow.woStatusNm;
        this.workOrderModalStatus = selectedRow.woStatus;

        this.workOrderModalItemId = selectedRow.itemId;
        this.workOrderModalItemCd = selectedRow.itemCd;
        this.workOrderModalItemNm = selectedRow.itemNm;
        this.emergencyYN = 'N';
        this.statusChange = '';
        this.modalIsOpen = true;
    }
    // 작업요청
    handelRequireClick = async () => {
        const selectedRows = this.workOrderGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg('선택된 항목이 없습니다.');
            return;
        }
        for (let i = 0; i < selectedRows.length; i++) {
            if (selectedRows[i].outsourcingLoginNo == null) {
                Alert.meg('외주라인이 아닙니다.');
                return;
            }
            if (selectedRows[i].woStatus != "10") {
                Alert.meg("계획상태가 아닙니다.");
                return;
            }
        }
        this.requireSaveData = selectedRows;
        console.log(this.requireSaveData);
        this.requireModalMemo = '';
        this.requireModalIsOpen = true;
    }
    // 작업요청 닫기
    @action.bound
    closeRequireModal = () => {
        this.requireModalIsOpen = false;
    }
    // 작업요청 저장
    @action.bound
    requireModalSave = async () => {
        let orderNo = 0;
        for (let i = 0; i < this.requireSaveData.length; i++) {
            this.requireSaveData[i].prodDt2 = '';
            this.requireSaveData[i].orderMemo = this.requireModalMemo;
            orderNo = this.requireSaveData[i].orderNo;
        }
        const searchData = {
            "buId": this.buId
            , "lineId": this.lineId
            , "dateType": this.dateType_s
            , "startDate": this.startDate
            , "endDate": this.endDate
            , "woStatus": this.woStatus
            , "item": this.item
            , "changeStatus": "20"
        }
        console.log(this.requireSaveData);
        const params = JSON.stringify({ param1: this.requireSaveData, param2: searchData });
        const { data, status } = await WorkorderRepository.statusChange(params);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            Alert.meg("요청되었습니다.");
            this.workOrderList = data.data;
            this.workOrderGridApi.setRowData(this.workOrderList);
            this.workorderOutputGridApi.setRowData([]);
            this.closeRequireModal();
            this.setFocusAndSelect(orderNo);
        }
    }
    // 지시
    @action.bound
    handleOrderClick = async () => {
        let orderNo = 0;
        const selectedRows = this.workOrderGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg('선택된 항목이 없습니다.');
            return;
        }
        if (this.selectedLine.outsourcingLoginNo != null) {
            Alert.meg("권한이 없습니다.");
            return;
        }
        for (let i = 0; i < selectedRows.length; i++) {
            if (selectedRows[i].woStatus != "10") {
                Alert.meg("계획상태가 아닙니다.");
                return;
            } else {
                orderNo = selectedRows[i].orderNo;
            }
        }
        const searchData = {
            "buId": this.buId
            , "lineId": this.lineId
            , "dateType": this.dateType_s
            , "startDate": this.startDate
            , "endDate": this.endDate
            , "woStatus": this.woStatus
            , "item": this.item
            , "changeStatus": "60"
        }
        const params = JSON.stringify({ param1: selectedRows, param2: searchData });
        const { data, status } = await WorkorderRepository.statusChange(params);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            Alert.meg("변경되었습니다.");
            this.workOrderList = data.data;
            this.workOrderGridApi.setRowData(this.workOrderList);
            this.workorderOutputGridApi.setRowData([]);
            this.setFocusAndSelect(orderNo);            
        }
    }
    // 실적요청확인
    @action.bound
    btnResultConfirm = async () => {
        let orderNo = 0;
        const selectedRows = this.workOrderGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg('선택된 항목이 없습니다.');
            return;
        }
        if (!this.userInfo.outsourcingYn && this.userInfo.outsourcingYn == 'Y') {
            Alert.meg("권한이 없습니다.");
            return;
        }
        for (let i = 0; i < selectedRows.length; i++) {
            if (selectedRows[i].reqCnt <= 0) {
                Alert.meg("실적요청건수가 없습니다.");
                return;
            } else {
                orderNo = selectedRows[i].orderNo;
            }
        }
        const searchData = {
            "buId": this.buId
            , "lineId": this.lineId
            , "dateType": this.dateType_s
            , "startDate": this.startDate
            , "endDate": this.endDate
            , "woStatus": this.woStatus
            , "item": this.item
            , "performanceStatus": '20'
        }
        const params = JSON.stringify({ param1: selectedRows, param2: searchData });
        const { data, status } = await WorkorderRepository.perfomanceRequireConfirm(params);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            Alert.meg("확인되었습니다.");
            this.workOrderList = data.data;
            this.workOrderGridApi.setRowData(this.workOrderList);
            this.searchOutput();
            this.setFocusAndSelect(orderNo); 
        }
    }
    // 계획수정 창에서 완료 눌렀을 때
    @action.bound
    onClickModalComplete = async () => {
        const completeData = {
            orderNo: this.workOrderModalOrderNo
            , woStatus: this.workOrderModalStatus
        }
        const searchData = {
            "buId": this.buId
            , "lineId": this.lineId
            , "dateType": this.dateType_s
            , "startDate": this.startDate
            , "endDate": this.endDate
            , "woStatus": this.woStatus
            , "item": this.item
            , "changeStatus": "80"
        }
        const params = JSON.stringify({ param1: completeData, param2: searchData });
        const { data, status } = await WorkorderRepository.statusChange2(params);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            Alert.meg("변경되었습니다.");
            this.workOrderList = data.data;
            this.workOrderGridApi.setRowData(this.workOrderList);
            this.workorderOutputGridApi.setRowData([]);
            this.closeModal();
            this.setFocusAndSelect(this.workOrderModalOrderNo);
        }
    }

    // 지시완료
    @action.bound
    handleCompleteClick = async () => {
        let orderNo = 0;
        const selectedRows = this.workOrderGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg('선택된 항목이 없습니다.');
            return;
        }
        if (this.selectedLine.outsourcingLoginNo != null) {
            Alert.meg("권한이 없습니다.");
            return;
        }
        for (let i = 0; i < selectedRows.length; i++) {
            if (selectedRows[i].reqCnt > 0) {
                Alert.meg("미처리된 실적요청건이 있습니다.");
                return;
            }
            if (selectedRows[i].prodDtm == '' || selectedRows[i].prodDtm == null || selectedRows[i].prodDtm == undefined) {
                Alert.meg("생산일자가 없습니다.");
                return;
            }
            if (selectedRows[i].woStatus != "70") {
                Alert.meg("생산중이 아닙니다.<br/>생산시간을 입력해주세요.");
                return;                
            }
            orderNo = selectedRows[i].orderNo;
        }
        const searchData = {
            "buId": this.buId
            , "lineId": this.lineId
            , "dateType": this.dateType_s
            , "startDate": this.startDate
            , "endDate": this.endDate
            , "woStatus": this.woStatus
            , "item": this.item
            , "changeStatus": "80"
        }
        const params = JSON.stringify({ param1: selectedRows, param2: searchData });
        const { data, status } = await WorkorderRepository.statusChange(params);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            Alert.meg("변경되었습니다.");
            this.workOrderList = data.data;
            this.workOrderGridApi.setRowData(this.workOrderList);
            this.workorderOutputGridApi.setRowData([]);
            this.setFocusAndSelect(orderNo); 
        }
    }    
    
    // WorkOrderModal 닫기
    @action.bound
    closeModal = () => {
        this.modalIsOpen = false;
    }
    // WorkOrderModal에서 제품 선택 후 결과 반환
    @action.bound
    handleHelperResult = (result) => {
        if (result) {
            this.workOrderModalOrderUom = this.selectedItem.itemUom;
            this.workOrderModalItemId = this.selectedItem.itemId;
            this.workOrderModalItemCd = this.selectedItem.itemCd;
            this.workOrderModalItemNm = this.selectedItem.itemNm;
        }
    }
    // WorkOrderModal에서 지시 눌렀을 때
    @action.bound
    onClickModalOrder = () => {
        this.statusChange = '60';
        this.onClickModalSave();
    }
    // WorkOrderModal에서 요청 눌렀을 때
    @action.bound
    onClickModalRequire = () => {
        this.statusChange = '20';
        this.onClickModalSave();
    }
    // WorkOrderModal에서 등록 눌렀을 때
    @action.bound
    onClickModalSave = async () => {
        const validation = this.validator.validate(this);
        const saveData = {
            "orderNo": this.workOrderModalOrderNo
            , "itemId": this.workOrderModalItemId
            , "itemCd": this.workOrderModalItemCd
            , "itemNm": this.workOrderModalItemNm
            , "lineId": this.workOrderModalLineId
            , "lineNm": this.selectedLine2.lineNm
            , "planStartDtm": this.workOrderModalPlanStartDtm
            , "planEndDtm": this.workOrderModalPlanEndDtm
            , "planStartDtm2": this.workOrderModalPlanStartDtm.substring(0, 10)
            , "planDtm": this.workOrderModalPlanStartDtm + "~" + this.workOrderModalPlanEndDtm
            , "orderQty": this.workOrderModalOrderQty
            , "orderUom": this.workOrderModalOrderUom
            , "memo": this.workOrderModalMemo
            , "emergencyYN": this.emergencyYN
            , "statusChange": this.statusChange
            , "buId": this.buId
            , "outsourcingLoginNo": this.selectedLine.outsourcingLoginNo
        }
        if (!validation.isValid) {
            return;
        }
        if (this.workOrderModalPlanEndDtm < this.workOrderModalPlanStartDtm) {
            Alert.meg("작업지시종료일자가 작업지시시작일자보다 빠릅니다.");
            return;
        }
        const searchData = {
            "buId": this.buId
            , "lineId": this.lineId
            , "dateType": this.dateType_s
            , "startDate": this.startDate
            , "endDate": this.endDate
            , "woStatus": this.woStatus
            , "item": this.item
        }
        const params = JSON.stringify({ param1: saveData, param2: searchData });
        const { data, status } = await WorkorderRepository.woUpsert(params);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            if (this.modalMode == "add") {
                Alert.meg("등록되었습니다.");
                this.closeModal();
                this.workOrderList = data.data;
                this.workOrderGridApi.setRowData(this.workOrderList);
                let i = this.workOrderGridApi.getDisplayedRowCount() - 1;
                GridUtil.setFocusByRowIndex(this.workOrderGridApi, i);
                this.onSelectionChanged();
                this.workorderOutputGridApi.setRowData([]);
            } else {
                Alert.meg("저장되었습니다.");
                this.closeModal();
                this.workOrderList = data.data;
                this.workOrderGridApi.setRowData(this.workOrderList);
                GridUtil.setFocusByKey(this.workOrderGridApi, 'orderNo', this.workOrderModalOrderNo);
                this.onSelectionChanged();
            }
        }
    }
    // 계획수정 창에서 취소 눌렀을 때
    @action.bound
    onClickModalCancel = async () => {
        if (this.workOrderModalStatus < "20" && this.workOrderModalStatus > "80") {
            Alert.meg("계획, 완료, 취소 상태는<br>취소할 수 없습니다.");
            return;
        }
        const cancelData = {
            orderNo: this.workOrderModalOrderNo
            , woStatus: this.workOrderModalStatus
        }
        const searchData = {
            "buId": this.buId
            , "lineId": this.lineId
            , "dateType": this.dateType_s
            , "startDate": this.startDate
            , "endDate": this.endDate
            , "woStatus": this.woStatus
            , "item": this.item
            , "changeStatus": "90"
        }
        const params = JSON.stringify({ param1: cancelData, param2: searchData });
        const value = await Alert.confirm("취소하시겠습니까?").then(result => { return result; });
        if (value) {
            const { data, status } = await WorkorderRepository.statusChange2(params);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                Alert.meg("변경되었습니다.");
                this.workOrderList = data.data;
                this.workOrderGridApi.setRowData(this.workOrderList);
                this.workorderOutputGridApi.setRowData([]);
                this.closeModal();
                this.setFocusAndSelect(this.workOrderModalOrderNo);
            }
        }
        else {
            return;
        }
    }
    // 취소
    @action.bound
    btnCancel = async () => {
        let orderNo = 0;
        const selectedRows = this.workOrderGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg('선택된 항목이 없습니다.');
            return;
        }
        if (!this.userInfo.outsourcingYn && this.userInfo.outsourcingYn == 'Y') {
            Alert.meg("권한이 없습니다.");
            return;
        }
        for (let i = 0; i < selectedRows.length; i++) {
            if (selectedRows[i].woStatus < "20" || selectedRows[i].woStatus > "80") {
                Alert.meg("계획, 완료, 취소 상태는<br>취소할 수 없습니다.");
                return;
            } else {
                orderNo = selectedRows[i].orderNo;
            }
        }
        const searchData = {
            "buId": this.buId
            , "lineId": this.lineId
            , "dateType": this.dateType_s
            , "startDate": this.startDate
            , "endDate": this.endDate
            , "woStatus": this.woStatus
            , "item": this.item
            , "changeStatus": "90"
        }
        const params = JSON.stringify({ param1: selectedRows, param2: searchData });
        const value = await Alert.confirm("취소하시겠습니까?").then(result => { return result; });
        if (value) {
            const { data, status } = await WorkorderRepository.statusChange(params);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                Alert.meg("변경되었습니다.");
                this.workOrderList = data.data;
                this.workOrderGridApi.setRowData(this.workOrderList);
                this.workorderOutputGridApi.setRowData([]);
                this.closeModal();
                this.setFocusAndSelect(this.orderNo);
            }
        }
        else {
            return;
        }
    }
    // 계획수정 창에서 삭제 눌렀을 때
    @action.bound
    onClickModalDelete = async () => {
        if (this.workOrderModalStatus > "10" && this.workOrderModalStatus < "90") {
            Alert.meg("삭제 가능한 상태가 아닙니다.");
            return;
        }
        const deleteData = {
            orderNo: this.workOrderModalOrderNo
            , woStatus: this.workOrderModalStatus
        }
        const searchData = {
            "buId": this.buId
            , "lineId": this.lineId
            , "dateType": this.dateType_s
            , "startDate": this.startDate
            , "endDate": this.endDate
            , "woStatus": this.woStatus
            , "item": this.item
            , "changeStatus": "99"
        }
        const params = JSON.stringify({ param1: deleteData, param2: searchData });
        const value = await Alert.confirm("삭제하시겠습니까?").then(result => { return result; });
        if (value) {
            const { data, status } = await WorkorderRepository.statusChange2(params);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                Alert.meg("변경되었습니다.");
                this.workOrderList = data.data;
                this.workOrderGridApi.setRowData(this.workOrderList);
                this.workorderOutputGridApi.setRowData([]);
                this.closeModal();
            }
        }
        else {
            return;
        }
    }
    // 삭제
    @action.bound
    btnPlanDelete = async () => {
        const selectedRows = this.workOrderGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg('선택된 항목이 없습니다.');
            return;
        }
        for (let i = 0; i < selectedRows.length; i++) {
            if (selectedRows[i].woStatus > "10" && selectedRows[i].woStatus < "90") {
                Alert.meg("삭제 가능한 상태가 아닙니다.");
                return;
            } else {
                orderNo = selectedRows[i].orderNo;
            }
        }
        const searchData = {
            "buId": this.buId
            , "lineId": this.lineId
            , "dateType": this.dateType_s
            , "startDate": this.startDate
            , "endDate": this.endDate
            , "woStatus": this.woStatus
            , "item": this.item
            , "changeStatus": "99"
        }
        const params = JSON.stringify({ param1: selectedRows, param2: searchData });
        const value = await Alert.confirm("삭제하시겠습니까?").then(result => { return result; });
        if (value) {
            const { data, status } = await WorkorderRepository.statusChange(params);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                Alert.meg("변경되었습니다.");
                this.workOrderList = data.data;
                this.workOrderGridApi.setRowData(this.workOrderList);
                this.workorderOutputGridApi.setRowData([]);
                this.closeModal();
            }
        }
        else {
            return;
        }
    }
    // 포장지시 MiddleItem에서 바코드 발행 눌렀을 때
    @action.bound
    openBarcodeModal = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.workOrderGridApi);
        if (!selectedRow || selectedRow.length == 0) {
            Alert.meg("선택된 항목이 없습니다.");
            return;
        }
        // 생산관리자인데 하도급라인인 경우
        console.log(this.userInfo);
        if (!this.userInfo.outsourcingYn && this.userInfo.outsourcingYn != 'Y' && this.selectedLine.outsourcingLoginNo != null) {
            Alert.meg("권한이 없습니다.");
            return;
        }
        // 현장관리인인데 자기 부서가 아닌경우
        if (!this.userInfo.outsourcingYn && this.userInfo.outsourcingYn == 'Y' && this.userInfo.deptCd != selectedRow.deptCd) {
            Alert.meg("권한이 없습니다.");
            return;
        }
        // 바코드 관리 대상이 아닌 경우
        if (selectedRow.barcodeYN == 'N') {
            Alert.meg("바코드 관리 대상이 아닌 제품입니다.");
            return;
        }
        if (selectedRow.woStatus >= "30" && selectedRow.woStatus < "80") {
            this.barcodeModalOrderNo = selectedRow.orderNo;
            this.barcodeModalLotNo = moment().format('YYYYMMDD').replace(/-/gi, '');
            this.barcodeModalItemId = selectedRow.itemId;
            this.barcodeModalItemCd = selectedRow.itemCd;
            this.barcodeModalItemNm = selectedRow.itemNm;
            this.barcodeModalProdDt = moment().format('YYYYMMDD');
            this.addDate = selectedRow.expiryDay;
            this.makeExpireDate();
            this.barcodeModalProdUomQty = selectedRow.prodUomQty;
            this.barcodeModalProdUom = selectedRow.prodUom;
            this.barcodeModalPackQty = selectedRow.packUomQty;
            this.barcodeModalPackUom = selectedRow.packUom;
            this.barcodeModalProdQty = this.barcodeModalProdUomQty * this.barcodeModalPackQty;
            this.rackTypeHide = true;
            this.prodEmpNo = '';
            this.prodEmpNm = '';
            this.packerEmpNo = '';
            this.packerEmpNm = '';
            this.minSeq = 1;
            this.maxSeq = Math.round(selectedRow.orderQty2 / this.barcodeModalProdQty);
            this.barcodeModalMemo = "";

            if (this.buId == "13FA") {
                this.rackTypeHide = false;
                this.barcodeModalRackType = "1";
            }
            this.barcodeModalIsOpen = true;
        } else {
            Alert.meg("바코드를 발행할 수 없는 상태입니다.");
        }
    }
    // BarcodeModal 닫기
    @action.bound
    closeBarcodeModal = () => {
        this.barcodeModalIsOpen = false;
    }
    // BarcodeModal 생산자 정보 return    
    @action.bound
    prodEmployeeReturn = async (result) => {
        if (result) {
            this.prodEmpNo = this.prodEmp.empNo;
            this.prodEmpNm = this.prodEmp.loginNm;
        }
    }
    // BarcodeModal 포장자 정보 return    
    @action.bound
    packEmployeeReturn = async (result) => {
        if (result) {
            this.packerEmpNo = this.packEmp.empNo;
            this.packerEmpNm = this.packEmp.loginNm;
        }
    }
    // 바코드 발행
    @action.bound
    barcodeCreate = async () => {
        const validation = this.barcodeValidator.validate(this);
        if (!validation.isValid) {
            return;
        }
        if (this.barcodeModalPackQty <= 0 || Number.isInteger(this.barcodeModalPackQty) == false) {
            Alert.meg("유효하지 않은 포장수량입니다.");
            return;
        }
        if (this.maxSeq < this.minSeq) {
            Alert.meg("일련번호 범위를 확인해주세요.");
            return;
        }
        if (this.minSeq == 0 || this.maxSeq == 0) {
            Alert.meg("일련번호는 0이 될 수 없습니다.");
            return;
        }
        if (ObjectUtility.convertToDateFromYYYYMMDD(this.barcodeModalExpiryDt) <= ObjectUtility.convertToDateFromYYYYMMDD(this.barcodeModalProdDt)) {
            Alert.meg("유통기한이 생산일자보다 빠릅니다.");
            return;
        }
        if (this.barcodeModalExpiryDt == undefined || this.barcodeModalExpiryDt == "") {
            Alert.meg("유통기한을 직접 입력해주세요.");
            return;
        }
        const checkData = {
            "orderNo": this.barcodeModalOrderNo
            , "minSeq": this.minSeq
            , "maxSeq": this.maxSeq
        }
        const { data, status } = await WorkorderRepository.barcodeCheck(checkData);
        if (data.data.length > 0) {
            const value = await Alert.confirm('이미 재고로 잡힌<br> 바코드가 있습니다.<br>그래도 재발행 하시겠습니까?').then(result => { return result; });
            if (value) {
                const saveData = {
                    "orderNo": this.barcodeModalOrderNo
                    , "lotNo": this.barcodeModalLotNo
                    , "itemId": this.barcodeModalItemId
                    , "prodDt": this.barcodeModalProdDt
                    , "expiryDt": this.barcodeModalExpiryDt
                    , "packUom": this.barcodeModalPackUom
                    , "packQty": this.barcodeModalPackQty
                    , "prodUom": this.barcodeModalProdUom
                    , "prodUomQty": this.barcodeModalProdUomQty
                    , "prodQty": this.barcodeModalProdQty
                    , "rackType": this.barcodeModalRackType
                    , "minSeq": this.minSeq
                    , "maxSeq": this.maxSeq
                    , "memo": this.barcodeModalMemo
                    , "prodEmpNo": this.prodEmpNo
                    , "prodEmpNm": this.prodEmpNm
                    , "packerEmpNo": this.packerEmpNo
                    , "packerEmpNm": this.packerEmpNm
                }
                const { data, status } = await WorkorderRepository.barcodeCreate(saveData);
                if (data.success == false) {
                    Alert.meg(data.msg);
                } else {
                    Alert.meg("발행되었습니다.");
                    this.closeBarcodeModal();
                }
            }
            else {
                return;
            }
        } else {
            const saveData = {
                "orderNo": this.barcodeModalOrderNo
                , "lotNo": this.barcodeModalLotNo
                , "itemId": this.barcodeModalItemId
                , "prodDt": this.barcodeModalProdDt
                , "expiryDt": this.barcodeModalExpiryDt
                , "packUom": this.barcodeModalPackUom
                , "packQty": this.barcodeModalPackQty
                , "prodUom": this.barcodeModalProdUom
                , "prodUomQty": this.barcodeModalProdUomQty
                , "prodQty": this.barcodeModalProdQty
                , "rackType": this.barcodeModalRackType
                , "minSeq": this.minSeq
                , "maxSeq": this.maxSeq
                , "memo": this.barcodeModalMemo
                , "prodEmpNo": this.prodEmpNo
                , "prodEmpNm": this.prodEmpNm
                , "packerEmpNo": this.packerEmpNo
                , "packerEmpNm": this.packerEmpNm
            }
            const { data, status } = await WorkorderRepository.barcodeCreate(saveData);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                Alert.meg("발행되었습니다.");
                this.closeBarcodeModal();
            }
        }
    }
    // 긴급지시
    @action.bound
    emergencyOrder = async () => {
        if (!this.userInfo.outsourcingYn && this.userInfo.outsourcingYn != 'Y') {
            Alert.meg("권한이 없습니다.");
            return;
        }
        this.itemHelperOption.cdNmReadOnly = false;
        this.itemHelperOption.searchVisible = true;
        this.modalTitle = '긴급지시';
        this.modalMode = "add";
        this.workOrderModalLineId = this.lineId;
        this.selectedItem = {};
        this.workOrderModalPlanStartDtm = moment().format('YYYYMMDD');
        this.workOrderModalPlanEndDtm = moment().add(+1, 'days').format('YYYYMMDD');
        this.workOrderModalOrderQty = 0;
        this.workOrderModalOrderUom = "";
        this.workOrderModalMemo = "";
        this.workOrderModalOrderNo = 0;
        this.workOrderModalItemId = 0;
        this.workOrderModalItemCd = "";
        this.workOrderModalItemNm = "";
        this.emergencyYN = 'Y';
        this.statusChange = '';
        this.modalIsOpen = true;
    }
    // 계획수정 창에서 요청확인 눌렀을 때
    @action.bound
    onClickModalRequireConfirm = async () => {
        const cancelData = {
            orderNo: this.workOrderModalOrderNo
            , woStatus: this.workOrderModalStatus
        }
        const searchData = {
            "buId": this.buId
            , "lineId": this.lineId
            , "dateType": this.dateType_s
            , "startDate": this.startDate
            , "endDate": this.endDate
            , "woStatus": this.woStatus
            , "item": this.item
            , "changeStatus": "25"
        }
        const params = JSON.stringify({ param1: cancelData, param2: searchData });
        const { data, status } = await WorkorderRepository.statusChange2(params);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            Alert.meg("변경되었습니다.");
            this.workOrderList = data.data;
            this.workOrderGridApi.setRowData(this.workOrderList);
            this.workorderOutputGridApi.setRowData([]);
            this.closeModal();
            this.setFocusAndSelect(this.workOrderModalOrderNo);
        }
    }
    // 작업요청 확인
    @action.bound
    requireAccept = async () => {
        let orderNo = 0;
        const selectedRows = this.workOrderGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg('선택된 항목이 없습니다.');
            return;
        }
        for (let i = 0; i < selectedRows.length; i++) {
            if (selectedRows[i].woStatus != "20") {
                Alert.meg("작업요청 상태가 아닙니다.");
                return;
            }
            // 부서 다를 때
            if (this.userInfo.deptCd != selectedRows[i].deptCd) {
                Alert.meg("권한이 없습니다.");
                return;
            }
            // 라인 다를 때            
            if (this.userInfo.loginId != selectedRows[i].outsourcingLoginNo) {
                Alert.meg("권한이 없습니다.");
                return;
            }
            orderNo = selectedRows[i].orderNo;
        }
        const searchData = {
            "buId": this.buId
            , "lineId": this.lineId
            , "dateType": this.dateType_s
            , "startDate": this.startDate
            , "endDate": this.endDate
            , "woStatus": this.woStatus
            , "item": this.item
            , "changeStatus": "25"
        }
        const params = JSON.stringify({ param1: selectedRows, param2: searchData });
        const { data, status } = await WorkorderRepository.statusChange(params);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            Alert.meg("변경되었습니다.");
            this.workOrderList = data.data;
            this.workOrderGridApi.setRowData(this.workOrderList);
            this.workorderOutputGridApi.setRowData([]);
            this.setFocusAndSelect(orderNo);
        }
    }
    // 작업지시
    @action.bound
    packingOrders = async () => {
        let orderNo = 0;
        const selectedRows = this.workOrderGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg('선택된 항목이 없습니다.');
            return;
        }
        for (let i = 0; i < selectedRows.length; i++) {
            if (selectedRows[i].woStatus != "25") {
                Alert.meg("작업요청확인 상태가 아닙니다.");
                return;
            }
            if (this.userInfo.deptCd != selectedRows[i].deptCd) {
                Alert.meg("권한이 없습니다.");
                return;
            }
            orderNo = selectedRows[i].orderNo;
        }
        const searchData = {
            "buId": this.buId
            , "lineId": this.lineId
            , "dateType": this.dateType_s
            , "startDate": this.startDate
            , "endDate": this.endDate
            , "woStatus": this.woStatus
            , "item": this.item
            , "changeStatus": "60"
        }
        const params = JSON.stringify({ param1: selectedRows, param2: searchData });
        const { data, status } = await WorkorderRepository.statusChange(params);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            Alert.meg("변경되었습니다.");
            this.workOrderList = data.data;
            this.workOrderGridApi.setRowData(this.workOrderList);
            this.workorderOutputGridApi.setRowData([]);
            this.setFocusAndSelect(orderNo);
        }
    }
    // 완료(하도급)
    @action.bound
    packingCompletion = async () => {
        let orderNo = 0;
        const selectedRows = this.workOrderGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg('선택된 항목이 없습니다.');
            return;
        }
        for (let i = 0; i < selectedRows.length; i++) {
            if (this.userInfo.deptCd != selectedRows[i].deptCd) {
                Alert.meg("권한이 없습니다.");
                return;
            }
            if (selectedRows[i].prodDtm == '' || selectedRows[i].prodDtm == null || selectedRows[i].prodDtm == undefined) {
                Alert.meg("생산일자가 없습니다.");
                return;
            }
            if (selectedRows[i].woStatus != "70") {
                Alert.meg("생산중이 아닙니다.<br/>생산시간을 입력해주세요.");
                return;
            }
            orderNo = selectedRows[i].orderNo;
        }
        const searchData = {
            "buId": this.buId
            , "lineId": this.lineId
            , "dateType": this.dateType_s
            , "startDate": this.startDate
            , "endDate": this.endDate
            , "woStatus": this.woStatus
            , "item": this.item
            , "changeStatus": "80"
        }
        const params = JSON.stringify({ param1: selectedRows, param2: searchData });
        const { data, status } = await WorkorderRepository.statusChange(params);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            Alert.meg("변경되었습니다.");
            this.workOrderList = data.data;
            this.workOrderGridApi.setRowData(this.workOrderList);
            this.workorderOutputGridApi.setRowData([]);
            this.setFocusAndSelect(orderNo);
        }
    }

    /* ProdDtModal */
    // main middleItem 생산시간 입력 클릭 시
    @action.bound
    openProdDtModal = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.workOrderGridApi);
        if (!selectedRow || selectedRow.length == 0) {
            Alert.meg("선택된 항목이 없습니다.");
            return;
        }
        if (selectedRow.woStatus < "60" || selectedRow.woStatus > "80") {
            Alert.meg("생산시간은 지시,생산중,완료<br>상태일때만 입력가능합니다.");
            return;
        }
        let psdt = "";
        let psdtm = "";
        if (selectedRow.prodStartDtm != null) {
            psdt = selectedRow.prodStartDtm.split(" ")[0];
            psdtm = selectedRow.prodStartDtm.split(" ")[1];
        }
        let pedt = "";
        let pedtm = "";
        if (selectedRow.prodEndDtm != null) {
            pedt = selectedRow.prodEndDtm.split(" ")[0];
            pedtm = selectedRow.prodEndDtm.split(" ")[1];
        }
        this.prodDtModalOrderNo = selectedRow.orderNo;
        this.prodDtModalLineNm = selectedRow.lineNm;
        this.prodDtModalWoStatusNm = "생산중"
        this.prodDtModalItemCd = selectedRow.itemCd;
        this.prodDtModalItemNm = selectedRow.itemNm;
        this.prodDtModalProdStartDt = psdt;
        this.prodDtModalProdStartDtm = psdtm;
        this.prodDtModalProdEndDt = pedt;
        this.prodDtModalProdEndDtm = pedtm;

        this.prodDtModalIsOpen = true;
    }
    // ProdDtModal 창 닫기
    @action.bound
    closeProdDtModal = async () => {
        this.prodDtModalIsOpen = false;
    }
    // ProdDtModal 저장 클릭 시
    @action.bound
    prodDtModalSave = async () => {
        let psd = this.prodDtModalProdStartDt + ' ' + this.prodDtModalProdStartDtm;
        let ped = this.prodDtModalProdEndDt + ' ' + this.prodDtModalProdEndDtm;
        if (psd == ' ') {
            psd = null;
        }
        if (ped == ' ') {
            ped = null;
        }
        const saveData = {
            "orderNo": this.prodDtModalOrderNo
            , "prodStartDtm": psd
            , "prodEndDtm": ped
        }
        const { data, status } = await OutputRepository.woProdDtmUpdate(saveData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            Alert.meg("저장되었습니다.");
            this.closeProdDtModal();
            this.handleSearchClick();
        }
    }
    // 생산정보 클릭 시
    @action.bound
    openOrderHelperModal = () => {
        const selectedRow = GridUtil.getSelectedRowData(this.workOrderGridApi);
        if (!selectedRow || selectedRow.length == 0) {
            Alert.meg('조회할 항목을 선택해주세요.')
            return;
        }
        this.orderNo_h = Number(selectedRow.orderNo)
        this.orderBarcodeYN = selectedRow.barcodeYN;
        this.orderHelperModalIsOpen = true;
    }

    // 실적요청
    @action.bound
    perfomanceRequire = async () => {
        const selectedWORow = GridUtil.getSelectedRowData(this.workOrderGridApi);
        const selectedRows = this.workorderOutputGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length <= 0) {
            Alert.meg('선택된 항목이 없습니다.');
            return;
        }
        let count = 0;
        for (let i = 0; i < selectedRows.length; i++) {
            // 라인 다를 때            
            if (this.userInfo.loginId != selectedRows[i].outsourcingLoginNo) {
                Alert.meg("권한이 없습니다.");
                return;
            }
            if (selectedRows[i].confirmStatus == '0') {
                selectedRows[i].planStartDtm2 = selectedWORow.planStartDtm2;
                selectedRows[i].planDtm = selectedWORow.planDtm;
                let arr1 = selectedRows[i].prodDt.substring(0, 4);
                let arr2 = selectedRows[i].prodDt.substring(4, 6);
                let arr3 = selectedRows[i].prodDt.substring(6);
                selectedRows[i].prodDt2 = arr1 + "-" + arr2 + "-" + arr3;
                selectedRows[i].prodQty = Math.ceil(selectedRows[i].prodQty / selectedWORow.prodUomQty);
                selectedRows[i].prodQtyUom = selectedWORow.orderUom;
                selectedRows[i].orderMemo = selectedWORow.orderMemo;
                selectedRows[i].userIdForMail = selectedWORow.inputEmpNo;
                selectedRows[i].emergencyYN = selectedWORow.emergencyYN;
                selectedRows[i].buId = this.buId;
                count = count + 1;
            } else {
                Alert.meg("요청할 수 없는 상태입니다.");
                count = 0;
                return;
            }
        }
        if (count >= 1) {
            const searchData = {
                "buId": this.buId
                , "lineId": this.lineId
                , "dateType": this.dateType_s
                , "startDate": this.startDate
                , "endDate": this.endDate
                , "woStatus": this.woStatus
                , "item": this.item
                , "performanceStatus": '10'
            }
            const params = JSON.stringify({ param1: selectedRows, param2: searchData });
            const { data, status } = await WorkorderRepository.perfomanceRequireConfirm(params);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                Alert.meg("요청되었습니다.");
                this.workOrderList = data.data;
                this.workOrderGridApi.setRowData(this.workOrderList);
                this.searchOutput();
                this.setFocusAndSelect(selectedWORow.orderNo);
            }
        }
    }

    // 이전상태로 바꿔주기
    @action.bound
    rollbackStatus = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.workOrderGridApi);
        if (selectedRow.woStatus=="70") {
            const rollbackData = {
                orderNo: selectedRow.orderNo
                , woStatus: selectedRow.woStatus
            }
            const searchData = {
                "buId": this.buId
                , "lineId": this.lineId
                , "dateType": this.dateType_s
                , "startDate": this.startDate
                , "endDate": this.endDate
                , "woStatus": this.woStatus
                , "item": this.item
                , "changeStatus": "60"
            }
            const params = JSON.stringify({ param1: rollbackData, param2: searchData });
            const value = await Alert.confirm("지시상태로 돌리시겠습니까?").then(result => { return result; });
            if (value) {
                const { data, status } = await WorkorderRepository.statusChange2(params);
                if (data.success == false) {
                    Alert.meg(data.msg);
                } else {
                    Alert.meg("변경되었습니다.");
                    this.workOrderList = data.data;
                    this.workOrderGridApi.setRowData(this.workOrderList);
                    this.workorderOutputGridApi.setRowData([]);
                    this.setFocusAndSelect(selectedRow.orderNo);
                }
            }
            else {
                return;
            }
        } else if (selectedRow.woStatus=="80") {
            const rollbackData = {
                orderNo: selectedRow.orderNo
                , woStatus: selectedRow.woStatus
            }
            const searchData = {
                "buId": this.buId
                , "lineId": this.lineId
                , "dateType": this.dateType_s
                , "startDate": this.startDate
                , "endDate": this.endDate
                , "woStatus": this.woStatus
                , "item": this.item
                , "changeStatus": "70"
            }
            const params = JSON.stringify({ param1: rollbackData, param2: searchData });
            const value = await Alert.confirm("생산중 상태로 돌리시겠습니까?").then(result => { return result; });
            if (value) {
                const { data, status } = await WorkorderRepository.statusChange2(params);
                if (data.success == false) {
                    Alert.meg(data.msg);
                } else {
                    Alert.meg("변경되었습니다.");
                    this.workOrderList = data.data;
                    this.workOrderGridApi.setRowData(this.workOrderList);
                    this.workorderOutputGridApi.setRowData([]);
                    this.setFocusAndSelect(selectedRow.orderNo);
                }
            }
            else {
                return;
            }
        }
    }























































































































































































































































































































































































































































































































































































































































































































































































































































































































































    /* 재포장투입 관련 기능 */
    // 재포장투입 관련 변수
    @observable orderNo_p = 0;      // 생산지시 오더번호
    @observable itemProdHelperOption = new ItemOptionModel().optionModel;
    @observable selectedProdItem = {}; //제품코드,명
    @observable prodsQty_p = 0; //생산량
    @observable orderQty_p = 0; //지시량

    @observable prodStartDt_p = moment().format('YYYYMMDD'); //투입일자 FROM
    @observable prodEndDt_p = moment().format('YYYYMMDD'); //투입일자 TO

    @observable prodInputDrList = [];
    @observable prodInputDrGridApi = undefined;
    @observable prodInputDrModalIsOpen = false; // 생산투입일보 팝업 모달

    // 추가(수량) 관련 변수
    @observable itemProdQtyHelperOption = new ItemOptionModel().optionModel;
    @observable selectedProdQtyItem = {}; //제품코드, 명
    @observable prodDt_q = moment().format('YYYYMMDD');
    @observable prodQty_q = 0;
    @observable text_q = '';

    @observable itemId_q = 0;
    @observable itemCd_q = '';

    @observable prodInputQtyModalIsOpen = false; // 추가(수량) 팝업 모달



    // 추가(바코드) 관련 변수
    @observable prodDt_p = moment().format('YYYYMMDD'); // 투입일자
    @observable prodQty_p = 0;      // 투입량(processQty)
    @observable text_p = '';        // 비고

    @observable prodInputList = [];
    @observable prodInputGridApi = undefined;
    @observable prodInputModalIsOpen = false; // 추가(바코드) 팝업 모달

    @observable selectedList = [];          // 바코드 추가 누적 변수(prodInputList)


    // 재포장지시(바코드) 추가 Modal 관련 변수
    @observable locationBarcodeUpsertModalBarcode = "";
    @observable locationBarcodeUpsertModalProdDt = moment().format('YYYYMMDD');
    @observable locationBarcodeUpsertModalItemNmOrItemCd = "";

    @observable locationBarcodeUpsertModalList = [];
    @observable locationBarcodeUpsertModalGridApi = undefined;

    @observable locationBarcodeUpsertModalIsOpen = false; // 바코드 추가 팝업 모달


    // 생산투입일보 진입 Btn
    @action.bound
    prodInputDrOpen = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.workOrderGridApi);
        if (!selectedRow || selectedRow.length == 0) {
            Alert.meg("생산투입일보 항목을 선택해 주세요");
            return;
        } else {
            this.orderNo_p = selectedRow.orderNo
            this.selectedProdItem = { "itemCd": selectedRow.itemCd, "itemNm": selectedRow.itemNm };
            this.prodsQty_p = selectedRow.prodQty;
            this.orderQty_p = selectedRow.orderQty;

            //ProdDtm 기준으로 보여주며, 없으면 plan으로 대체
            if (selectedRow.prodStartDtm != null && selectedRow.prodEndDtm != null) {
                this.prodStartDt_p = selectedRow.prodStartDtm
                this.prodEndDt_p = selectedRow.prodEndDtm
            } else {
                this.prodStartDt_p = selectedRow.planStartDtm
                this.prodEndDt_p = selectedRow.planEndDtm
            }
            this.prodInputDrModalIsOpen = true;
        }
    }

    // 생산투입일보 '조회' Btn
    @action.bound
    prodInputDrSearch = async () => {
        const submitData = {
            "orderNo_p": this.orderNo_p
            , "prodStartDt_p": this.prodStartDt_p
            , "prodEndDt_p": this.prodEndDt_p
        }
        const { data, status } = await WorkorderRepository.getInputDrSearchList(submitData);
        if (data.success == false) {
            console.log(data.msg)
        } else {
            this.prodInputDrList = data.data;
            this.prodInputDrGridApi.setRowData(this.prodInputDrList);
        }
    }

    // 생산투입일보 '삭제' Btn
    @action.bound
    prodInputDrCancel = async () => {
        const selectedRows = this.prodInputDrGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg('삭제할 항목을 체크 해주세요.');
            return;
        }
        let submitData = [];
        for (var i = 0; i < selectedRows.length; i++) {
            submitData.push(selectedRows[i]);
        }
        const params = JSON.stringify({ param: submitData });
        const { data, status } = await WorkorderRepository.prodInputDrDecision(params);
        if (data.success && status == 200) {
            Alert.meg(data.msg);
        } else {
            Alert.meg(data.sqlMessage);
        }
        this.prodInputDrSearch();
    }

    // ProdInputDrContents (재포장투입일보) 그리드
    @action.bound
    setProdInputDrGridApi = async (gridApi) => {
        this.prodInputDrGridApi = gridApi;
        const columnDefs = [
            {
                headerName: "", field: ""
                , width: 30
                , checkboxSelection: true
                , headerCheckboxSelection: true
                // , editable: function (params) { }
            },
            {
                headerName: "투입일자", field: "tranjDtm", width: 80, cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            { headerName: "바코드", field: "barcode", width: 100, cellClass: 'cell_align_center' },
            { headerName: "제품코드", field: "itemCd", width: 70, cellClass: 'cell_align_center' },
            { headerName: "제품명", field: "itemNm", width: 120, cellClass: 'cell_align_left' },
            {
                headerName: "투입량", field: "processQty", width: 70, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            // {headerName: "재고량", field: "invenQty", width: 70, cellClass: 'cell_align_right', valueFormatter: (params) => GridUtil.numberFormatter(params)},
            // {headerName: "투입여부", field: "delYn", width: 60, cellClass: 'cell_align_center' },
            // {headerName: "생산지시", field: "orderNo", width: 70, cellClass: 'cell_align_center'},
            { headerName: "비고", field: "memo", width: 120, cellClass: 'cell_align_left' },
            {
                headerName: "처리일시", field: "inputDtm", width: 120, cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm')
            },
            { headerName: "처리자", field: "inputEmpNm", width: 70, cellClass: 'cell_align_center' }
        ];
        this.prodInputDrGridApi.setColumnDefs(columnDefs);
    }

    //생산투입일보 팝업 닫기
    @action.bound
    closeProdInputDrModal = () => {
        this.prodInputDrList = [];
        this.prodInputDrModalIsOpen = false;
    }

    ////////////////////////////////////////////////////////////

    // 생산투입일보 '추가(수량)' Btn
    @action.bound
    prodInputDrQtyInput = async () => {
        this.prodInputQtyModalIsOpen = true;
    }

    // WorkOrderModal에서 제품 선택 후 결과 반환
    @action.bound
    handleHelperProdResult = (result) => {
        if (result) {
            this.itemId_q = this.selectedProdQtyItem.itemId;
            this.itemCd_q = this.selectedProdQtyItem.itemCd;
        }
    }


    // 재포장지시(수량) '저장' Btn    
    @action.bound
    prodInputQtySave = async () => {
        if (this.prodQty_q == 0 || this.itemCd_q == null) {
            Alert.meg('투입할 제품/수량을 확인 해주세요.');
            return;
        }

        const submitData = {
            "itemId": this.itemId_q
            , "itemCd": this.itemCd_q
            , "orderNo_p": this.orderNo_p
            , "prodQty_q": this.prodQty_q
            , "text_q": this.text_q
        }

        const { data, status } = await WorkorderRepository.prodInputQtySave(submitData);
        if (data.success && data.data.sqlMessage == null) {
            this.closeProdInputQtyModal();
            Alert.meg(data.msg);
            this.prodInputDrSearch();
        } else {
            Alert.meg(data.data.sqlMessage);
        }
    }

    // 재포장지시(수량) 팝업 닫기
    @action.bound
    closeProdInputQtyModal = () => {
        this.selectedProdQtyItem = {};
        this.prodQty_q = 0;
        this.text_q = '';
        this.prodInputQtyModalIsOpen = false;
    }

    ////////////////////////////////////////////////////////////

    // 생산투입일보 '추가(바코드)' Btn
    @action.bound
    prodInputDrInput = async () => {
        this.prodInputModalIsOpen = true;
    }

    // 재포장지시(바코드) '추가' Btn
    @action.bound
    prodInputAdd = () => {
        this.locationBarcodeUpsertModalIsOpen = true;
    }

    // 재포장지시(바코드) '제거' Btn
    @action.bound
    prodInputDelete = () => {
        let selectedRows = this.prodInputGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg('삭제할 항목을 선택해 주세요.');
            return;
        }
        selectedRows.forEach(selectedRow => {
            this.prodInputGridApi.updateRowData({ remove: [selectedRow] });
            this.prodInputList.remove(selectedRow);
            this.selectedList.remove(selectedRow);
            this.prodQty_p -= Number(selectedRow.processQty);
        })
    }

    // 재포장지시(바코드) '저장' Btn
    @action.bound
    prodInputSave = async () => {
        if (this.prodQty_p == 0 || this.prodInputList.length == 0) {
            Alert.meg('투입할 항목을 확인 해주세요.');
            return;
        }

        const submitData = {
            "orderNo_p": this.orderNo_p
            , "text_p": this.text_p
            , "actionType": 'confirm'
        }

        const params = JSON.stringify({ param1: this.prodInputList, param2: submitData });
        const { data, status } = await WorkorderRepository.prodInputSave(params);
        if (data.success && status == 200) {
            this.closeProdInputModal();
            Alert.meg(data.msg);
            this.prodInputDrSearch();
        } else {
            Alert.meg(data.msg);
        }

    }

    // ProdInputContents 재포장지시 추가(바코드) 그리드
    @action.bound
    setProdInputGridApi = async (gridApi) => {
        this.prodInputGridApi = gridApi;
        const columnDefs = [
            {
                headerName: "", field: ""
                , width: 30
                , checkboxSelection: true
                , headerCheckboxSelection: true
                // , editable: function (params) { }
            },
            {
                headerName: "순번", type: "numericColumn", field: "seq", width: 40, cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
            },
            { headerName: "바코드", field: "barcode", width: 100, cellClass: 'cell_align_center' },
            { headerName: "상태", field: "lotQualityNm", width: 50, cellClass: 'cell_align_center' },
            { headerName: "제품코드", field: "itemCd", width: 70, cellClass: 'cell_align_center' },
            { headerName: "제품명", field: "itemNm", width: 120, cellClass: 'cell_align_left' },
            { headerName: "LOT", field: "lotNo", width: 80, cellClass: 'cell_align_center' },
            { headerName: "생산일자", field: "prodDt", width: 80, cellClass: 'cell_align_center', valueFormatter: GridUtil.dateFormatter },
            { headerName: "유통기한", field: "expiryDt", width: 80, cellClass: 'cell_align_center', valueFormatter: GridUtil.dateFormatter },
            {
                headerName: "재고량", field: "processQty", width: 70, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            { headerName: "위치", field: "locNm", width: 70, cellClass: 'cell_align_center' },
            {
                headerName: "처리일시", field: "inputDtm", width: 120, cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm')
            },
            { headerName: "처리자", field: "inputEmpNm", width: 70, cellClass: 'cell_align_center' }
        ];
        this.prodInputGridApi.setColumnDefs(columnDefs);
    }

    //재포장지시(바코드) 팝업 닫기
    @action.bound
    closeProdInputModal = () => {
        this.selectedList = [];
        this.prodInputList = [];
        this.prodQty_p = 0;
        this.text_p = '';
        this.prodInputGridApi.setRowData([]);
        //this.prodInputGridApi.setRowData(this.prodInputList);
        this.prodInputModalIsOpen = false;
    }

    ////////////////////////////////////////////////////////////

    // 바코드 추가(ProdInputBarcodeModalSearchItem) '조회' Btn
    @action.bound
    locationBarcodeUpsertModalSearch = async () => {
        const searchData = {
            "buId": this.buId
            , "barcode": this.locationBarcodeUpsertModalBarcode
            , "prodDt": this.locationBarcodeUpsertModalProdDt
            , "itemNmOrItemCd": this.locationBarcodeUpsertModalItemNmOrItemCd
        }
        const { data, status } = await EtcRepository.getBarcode(searchData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            this.locationBarcodeUpsertModalList = data.data;
            this.locationBarcodeUpsertModalGridApi.setRowData(this.locationBarcodeUpsertModalList);
        }
    }
    // 바코드 추가 '선택' Btn
    @action.bound
    locationBarcodeUpsertModalSave = async () => {
        const selectedRows = this.locationBarcodeUpsertModalGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg('추가할 항목을 선택해 주세요.');
            return;
        }

        for (var i = 0; i < selectedRows.length; i++) {
            this.selectedList.push(selectedRows[i]);
        }

        //재포장투입 중복품목 제거
        let accumList = this.selectedList.reduce((accumulator, current) => {
            if (checkAlreadyExist(current)) {
                return accumulator;
            } else {
                return [...accumulator, current];
            }

            function checkAlreadyExist(currentVal) {
                return accumulator.some((item) => {
                    return (item.barcode === currentVal.barcode);
                });
            }
        }, []);

        this.prodInputList = accumList.sort(
            function (prev, after) {
                return prev.seq - after.seq
            });

        // 투입수량 합계 계산
        this.prodQty_p = 0;
        for (var i = 0; i < this.prodInputList.length; i++) {
            this.prodQty_p += Number(this.prodInputList[i].processQty);
        }
        console.log('prodINputList', this.prodInputList)
        this.prodInputGridApi.setRowData(this.prodInputList);

        //재포장투입 품목 누적
        // for(var i=0; i<selectedRows.length;i++) {
        //         this.selectedList.push(selectedRows[i]);
        // }
        // this.prodInputList = this.selectedList;

        // 1개품목 선택 후 저장시 기능
        // const selectedRows = LocationMoveStore.locationBarcodeUpsertModalGridApi.getSelectedRows();
        // if (!selectedRows || selectedRows.length == 0) {
        //     Alert.meg("바코드를 선택해 주세요.");
        //     return;
        // }
        // this.prodInputList.push(selectedRows[0]);
        // this.prodInputGridApi.setRowData(this.prodInputList);
        // for (var i = 0; i < selectedRows.length; i++) {
        //     this.prodQty_p += Number(selectedRows[0].processQty);
        // }
        this.locationBarcodeUpsertModalIsOpen = false;
    }

    // LocationBarcodeUpsertModal 더블클릭시
    @action.bound
    locationBarcodeUpsertModalDoubleClick = async () => {
        const selectedRows = GridUtil.getSelectedRowData(this.locationBarcodeUpsertModalGridApi);
        this.selectedList.push(selectedRows);

        //재포장투입 중복품목 제거
        let accumList = this.selectedList.reduce((accumulator, current) => {
            if (checkAlreadyExist(current)) {
                return accumulator;
            } else {
                return [...accumulator, current];
            }

            function checkAlreadyExist(currentVal) {
                return accumulator.some((item) => {
                    return (item.barcode === currentVal.barcode);
                });
            }
        }, []);

        this.prodInputList = accumList.sort(
            function (prev, after) {
                return prev.seq - after.seq
            });

        // 투입수량 합계 계산
        this.prodQty_p = 0;
        for (var i = 0; i < this.prodInputList.length; i++) {
            this.prodQty_p += Number(this.prodInputList[i].processQty);
        }
        console.log('prodINputList', this.prodInputList)
        this.prodInputGridApi.setRowData(this.prodInputList);
    }

    // 바코드추가 팝업 닫기
    @action.bound
    closeLocationBarcodeUpsertModal = async () => {
        this.locationBarcodeUpsertModalBarcode = "";
        this.locationBarcodeUpsertModalProdDt = moment().format('YYYYMMDD');
        this.locationBarcodeUpsertModalItemNmOrItemCd = "";
        this.locationBarcodeUpsertModalList = [];
        this.locationBarcodeUpsertModalIsOpen = false;
    }

    // ProdInputBarcodeModalContents 그리드
    @action.bound
    setBarcodeInputModalGridApi = async (gridApi) => {
        this.locationBarcodeUpsertModalGridApi = gridApi;
        const columnDefs = [
            {
                headerName: ""
                , field: "check"
                , width: 29
                , checkboxSelection: true
                , headerCheckboxSelection: true
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "순번"
                , field: "seq"
                , width: 40
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
            },
            {
                headerName: "바코드번호"
                , field: "barcode"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "상태"
                , field: "lotQualityNm"
                , width: 50
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "제품코드"
                , field: "itemCd"
                , width: 90
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "LOT NO"
                , field: "lotNo"
                , width: 80
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "생산일자"
                , field: "prodDt"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "유통기한"
                , field: "expiryDt"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "재고량"
                , field: "processQty"
                , width: 60
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
            },
            {
                headerName: "위치"
                , field: "locNm"
                , width: 120
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "처리시간"
                , field: "inputDtm"
                , width: 110
                , cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm')
            },
            {
                headerName: "처리자"
                , field: "inputEmpNm"
                , width: 90
                , cellClass: 'cell_align_center'
            }
        ];
        this.locationBarcodeUpsertModalGridApi.setColumnDefs(columnDefs);
    }
}
export default new WorkorderStore();