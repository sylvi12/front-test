import API from "components/override/api/API";

class WorkorderRepository {

    URL = "/api/order/workorder";
    /* 조회 */
    // 포장지시 조회
    getWorkOrders(params) {
        return API.request.post(encodeURI(`${this.URL}/getworkorders`), params)
    }
    // modal에서 라인 셀렉트박스 만들 때
    getLine(params) {
        return API.request.get(encodeURI(`${this.URL}/getline?buId=${params.buId}`))
    }
    // modal에서 라인 셀렉트박스 만들 때
    getLine2(params) {
        return API.request.get(encodeURI(`${this.URL}/getline2?loginId=${params.loginId}`))
    }

    /* 등록 */
    // 포장지시 등록&수정
    woUpsert(params) {
        return API.request.post(`${this.URL}/upsert`, params)
    }
    // barcode 체크
    barcodeCheck(params) {
        return API.request.get(encodeURI(`/api/print/barcodecheck?orderNo=${params.orderNo}&minSeq=${params.minSeq}&maxSeq=${params.maxSeq}`))
    }
    // barcode 등록
    barcodeCreate(param) {
        return API.request.post(`${this.URL}/barcodecreate`, param)
    }
    // 상태 변경
    statusChange(params) {
        return API.request.post(`${this.URL}/statuschange`, params)
    }
    // 상태 변경
    statusChange2(params) {
        return API.request.post(`${this.URL}/statuschange2`, params)
    }
    // 실적 요청 확인
    perfomanceRequireConfirm(params) {
        return API.request.post(`${this.URL}/perfomancerequireconfirm`, params)
    }

    /* 삭제 */
    // 포장지시 삭제
    deleteWos(params) {
        return API.request.post(`${this.URL}/delete`, params)
    }





























































    /* 재포장투입일보 관련 기능 */
    prodInputSave(params) {
        return API.request.post(`${this.URL}/prodinputsave`, params)
    }

    prodInputQtySave(param) {
        return API.request.post(`${this.URL}/prodinputqtysave`, param)
    }

    prodInputDrDecision(params) {
        return API.request.post(`${this.URL}/prodinputdrdecision`, params)
    }

    getInputDrSearchList(params) {
        return API.request.get(encodeURI(`${this.URL}/getinputdrsearchlist?orderNo_p=${params.orderNo_p}&prodStartDt_p=${params.prodStartDt_p}&prodEndDt_p=${params.prodEndDt_p}`))
    }


}
export default new WorkorderRepository();