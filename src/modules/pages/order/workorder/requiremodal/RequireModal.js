import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import STextArea from 'components/atoms/input/STextArea';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SButton from 'components/atoms/button/SButton';
import ItemHelper from 'components/codehelper/ItemHelper';
import SNumericInput from 'components/override/numericinput/SNumericInput';

@inject(stores => ({
    modalMode: stores.workorderStore.modalMode

    , requireModalMemo: stores.workorderStore.requireModalMemo

    , closeRequireModal: stores.workorderStore.closeRequireModal
    , requireModalSave: stores.workorderStore.requireModalSave
    , handleChange: stores.workorderStore.handleChange

}))
@observer
class RequireModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { handleChange
            , requireModalMemo
            , closeRequireModal, requireModalSave
        } = this.props;

        return (
            <React.Fragment>
                <table style={{ width: "100%" }}>
                    <tbody>
                        <tr>
                            <th>비고</th>
                            <td>
                                <STextArea
                                    id="requireModalMemo"
                                    value={requireModalMemo}
                                    onChange={handleChange}
                                    contentWidth={200}
                                    isFocus={true}
                                />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div>
                    <SButton buttonName={"취소"} onClick={closeRequireModal} type={"default"} />
                    <SButton buttonName={"저장"} onClick={requireModalSave} type={"btnRequireSave"} />
                </div>
            </React.Fragment>
        );
    }
}

export default RequireModal;
