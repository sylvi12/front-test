import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import STextArea from 'components/atoms/input/STextArea';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SButton from 'components/atoms/button/SButton';
import ItemHelper from 'components/codehelper/ItemHelper';
import SNumericInput from 'components/override/numericinput/SNumericInput';

import workorderStore from 'modules/pages/order/workorder/service/WorkorderStore';

@inject(stores => ({
    modalMode: stores.workorderStore.modalMode

    , workOrderModalLineId: stores.workorderStore.workOrderModalLineId
    , lineGroup2: stores.workorderStore.lineGroup2
    , lineSelectionChanged2: stores.workorderStore.lineSelectionChanged2

    , handleChange: stores.workorderStore.handleChange
    , itemHelperOption: stores.workorderStore.itemHelperOption
    , setItemHelperOption: stores.workorderStore.setItemHelperOption
    , handleHelperResult: stores.workorderStore.handleHelperResult
    , selectedItem: stores.workorderStore.selectedItem

    , workOrderModalPlanStartDtm: stores.workorderStore.workOrderModalPlanStartDtm
    , workOrderModalPlanEndDtm: stores.workorderStore.workOrderModalPlanEndDtm

    , workOrderModalOrderQty: stores.workorderStore.workOrderModalOrderQty
    , workOrderModalOrderUom: stores.workorderStore.workOrderModalOrderUom

    , workOrderModalMemo: stores.workorderStore.workOrderModalMemo

    , onClickModalSave: stores.workorderStore.onClickModalSave
    , closeModal: stores.workorderStore.closeModal
    , onClickModalOrder: stores.workorderStore.onClickModalOrder
    , onClickModalRequire: stores.workorderStore.onClickModalRequire

    , modalTitle: stores.workorderStore.modalTitle
    , selectedLine2: stores.workorderStore.selectedLine2

    , workOrderModalOrderNo: stores.workorderStore.workOrderModalOrderNo
    , workOrderModalStatusNm: stores.workorderStore.workOrderModalStatusNm
    , workOrderModalStatus: stores.workorderStore.workOrderModalStatus

    , onClickModalDelete: stores.workorderStore.onClickModalDelete
    , onClickModalCancel: stores.workorderStore.onClickModalCancel
    , onClickModalComplete: stores.workorderStore.onClickModalComplete
    , onClickModalRequireConfirm: stores.workorderStore.onClickModalRequireConfirm
}))
@observer
class WorkorderModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { modalMode, handleChange, modalTitle
            , workOrderModalLineId, lineGroup2, lineSelectionChanged2
            , handleHelperResult, selectedItem, itemHelperOption, setItemHelperOption
            , workOrderModalPlanStartDtm, workOrderModalPlanEndDtm
            , workOrderModalOrderQty, workOrderModalOrderUom
            , workOrderModalMemo
            , onClickModalSave, closeModal, onClickModalOrder, onClickModalRequire, onClickModalDelete
            , onClickModalCancel, onClickModalComplete
            , selectedLine2
            , workOrderModalOrderNo, workOrderModalStatusNm, workOrderModalStatus, onClickModalRequireConfirm
        } = this.props;

        const components = {
            status00buttons: Status00buttons
            , status10buttons: Status10buttons
            , status60buttons: Status60buttons
            , status70buttons: Status70buttons
            , status70_2buttons: Status70_2buttons
            , status10_2buttons: Status10_2buttons
            , status20buttons: Status20buttons
            , status25buttons: Status25buttons
        }

        let ButtonCollection;

        let buttonName = '';
        let buttonClick = '';
        let buttonType = '';
        console.log(selectedLine2, workOrderModalStatus);
        if (selectedLine2.outsourcingLoginNo == null || selectedLine2.outsourcingLoginNo == undefined) {
            buttonName = '지시';
            buttonClick = onClickModalOrder;
            buttonType = 'btnPlanOrder';

            switch (workOrderModalStatus) {
                case "10":
                    ButtonCollection = components['status10buttons'];
                    break;
                case "60":
                    ButtonCollection = components['status60buttons'];
                    break;
                case "70":
                    ButtonCollection = components['status70buttons'];
                    break;
                default:
                    ButtonCollection = components['status00buttons'];
                    break;
            }
        } else {
            buttonName = '작업요청';
            buttonClick = onClickModalRequire;
            buttonType = 'btnDirectRequire';

            switch (workOrderModalStatus) {
                case "10":
                    ButtonCollection = components['status10_2buttons'];
                    break;
                case "20":
                    ButtonCollection = components['status20buttons'];
                    break;
                case "25":
                    ButtonCollection = components['status25buttons'];
                    break;
                case "60":
                    ButtonCollection = components['status60buttons'];
                    break;
                case "70":
                    ButtonCollection = components['status70_2buttons'];
                    break;
                default:
                    ButtonCollection = components['status00buttons'];
                    break;
            }
        }
        switch (modalTitle) {
            // 계획수정
            case "계획수정":
                return (
                    <React.Fragment>
                        <table style={{ width: "100%" }}>
                            <tbody>
                                <tr>
                                    <th>지시번호</th>
                                    <td>
                                        <SInput
                                            id={"workOrderModalOrderNo"}
                                            value={workOrderModalOrderNo}
                                            onChange={handleChange}
                                            readOnly={true}
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <th>상태</th>
                                    <td>
                                        <SInput
                                            id={"workOrderModalStatusNm"}
                                            value={workOrderModalStatusNm}
                                            onChange={handleChange}
                                            readOnly={true}
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <th>라인 ID</th>
                                    <td>
                                        <SSelectBox
                                            id={"workOrderModalLineId"}
                                            value={workOrderModalLineId}
                                            optionGroup={lineGroup2}
                                            valueMember={'lineId'}
                                            displayMember={'lineNm'}
                                            onChange={handleChange}
                                            disabled={true}
                                            onSelectionChange={lineSelectionChanged2}
                                            contentWidth={140}
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <th>제품</th>
                                    <td>
                                        <ItemHelper
                                            selectedItemId={"selectedItem"}
                                            helperOption={itemHelperOption}
                                            onOpen={setItemHelperOption}
                                            onChange={handleChange}
                                            onHelperResult={handleHelperResult}
                                            defaultValue={selectedItem}
                                            cdName={'itemId'}
                                            cdNmName={'itemNm'} />
                                    </td>
                                </tr>
                                <tr>
                                    <th>작업지시일자</th>
                                    <td>
                                        <SDatePicker
                                            id="workOrderModalPlanStartDtm"
                                            value={workOrderModalPlanStartDtm}
                                            onChange={handleChange}
                                        />
                                        <SDatePicker
                                            title={"~"}
                                            id="workOrderModalPlanEndDtm"
                                            value={workOrderModalPlanEndDtm}
                                            onChange={handleChange}
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <th>작업지시량</th>
                                    <td>
                                        <SNumericInput
                                            id="workOrderModalOrderQty"
                                            value={workOrderModalOrderQty}
                                            onChange={handleChange}
                                        />
                                        {workOrderModalOrderUom}
                                    </td>
                                </tr>
                                <tr>
                                    <th>비고</th>
                                    <td>
                                        <STextArea
                                            id="workOrderModalMemo"
                                            value={workOrderModalMemo}
                                            onChange={handleChange}
                                            contentWidth={340}
                                        />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div>
                            <ButtonCollection onClickModalDelete={onClickModalDelete} onClickModalSave={onClickModalSave}
                                onClickModalCancel={onClickModalCancel} onClickModalComplete={onClickModalComplete}
                                onClickModalOrder={onClickModalOrder} onClickModalRequire={onClickModalRequire}
                                onClickModalRequireConfirm={onClickModalRequireConfirm} />
                        </div>
                    </React.Fragment>
                );
            // 긴급지시
            case "긴급지시":
                return (
                    <React.Fragment>
                        <table style={{ width: "100%" }}>
                            <tbody>
                                <tr>
                                    <th>라인 ID</th>
                                    <td>
                                        <SSelectBox
                                            id={"workOrderModalLineId"}
                                            value={workOrderModalLineId}
                                            optionGroup={lineGroup2}
                                            valueMember={'lineId'}
                                            displayMember={'lineNm'}
                                            onChange={handleChange}
                                            disabled={false}
                                            onSelectionChange={lineSelectionChanged2}
                                            contentWidth={140}
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <th>제품</th>
                                    <td>
                                        <ItemHelper
                                            selectedItemId={"selectedItem"}
                                            helperOption={itemHelperOption}
                                            onOpen={setItemHelperOption}
                                            onChange={handleChange}
                                            onHelperResult={handleHelperResult}
                                            defaultValue={selectedItem}
                                            cdName={'itemId'}
                                            cdNmName={'itemNm'} />
                                    </td>
                                </tr>
                                <tr>
                                    <th>작업지시일자</th>
                                    <td>
                                        <SDatePicker
                                            id="workOrderModalPlanStartDtm"
                                            value={workOrderModalPlanStartDtm}
                                            onChange={handleChange}
                                        />
                                        <SDatePicker
                                            title={"~"}
                                            id="workOrderModalPlanEndDtm"
                                            value={workOrderModalPlanEndDtm}
                                            onChange={handleChange}
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <th>작업지시량</th>
                                    <td>
                                        <SNumericInput
                                            id="workOrderModalOrderQty"
                                            value={workOrderModalOrderQty}
                                            onChange={handleChange}
                                        />
                                        {workOrderModalOrderUom}
                                    </td>
                                </tr>
                                <tr>
                                    <th>비고</th>
                                    <td>
                                        <STextArea
                                            id="workOrderModalMemo"
                                            value={workOrderModalMemo}
                                            onChange={handleChange}
                                            contentWidth={340}
                                        />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div>
                            <SButton buttonName={"지시"} onClick={onClickModalOrder} type={'btnEmergencyOrder'} />
                        </div>
                    </React.Fragment>
                );
            // 계획등록
            default:
                return (
                    <React.Fragment>
                        <table style={{ width: "100%" }}>
                            <tbody>
                                <tr>
                                    <th>라인 ID</th>
                                    <td>
                                        <SSelectBox
                                            id={"workOrderModalLineId"}
                                            value={workOrderModalLineId}
                                            optionGroup={lineGroup2}
                                            valueMember={'lineId'}
                                            displayMember={'lineNm'}
                                            onChange={handleChange}
                                            disabled={false}
                                            onSelectionChange={lineSelectionChanged2}
                                            contentWidth={140}
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <th>제품</th>
                                    <td>
                                        <ItemHelper
                                            selectedItemId={"selectedItem"}
                                            helperOption={itemHelperOption}
                                            onOpen={setItemHelperOption}
                                            onChange={handleChange}
                                            onHelperResult={handleHelperResult}
                                            defaultValue={selectedItem}
                                            cdName={'itemId'}
                                            cdNmName={'itemNm'} />
                                    </td>
                                </tr>
                                <tr>
                                    <th>작업지시일자</th>
                                    <td>
                                        <SDatePicker
                                            id="workOrderModalPlanStartDtm"
                                            value={workOrderModalPlanStartDtm}
                                            onChange={handleChange}
                                        />
                                        <SDatePicker
                                            title={"~"}
                                            id="workOrderModalPlanEndDtm"
                                            value={workOrderModalPlanEndDtm}
                                            onChange={handleChange}
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <th>작업지시량</th>
                                    <td>
                                        <SNumericInput
                                            id="workOrderModalOrderQty"
                                            value={workOrderModalOrderQty}
                                            onChange={handleChange}
                                        />
                                        {workOrderModalOrderUom}
                                    </td>
                                </tr>
                                <tr>
                                    <th>비고</th>
                                    <td>
                                        <STextArea
                                            id="workOrderModalMemo"
                                            value={workOrderModalMemo}
                                            onChange={handleChange}
                                            contentWidth={340}
                                        />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div>
                            <SButton buttonName={buttonName} onClick={buttonClick} type={buttonType} />
                            <SButton buttonName={"계획"} onClick={onClickModalSave} type={"btnPlan"} />
                        </div>
                    </React.Fragment>
                );
        }

    }
}

export default WorkorderModal;

const Status10buttons = ({ onClickModalDelete, onClickModalOrder, onClickModalSave }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"삭제"} onClick={onClickModalDelete} type={"btnPlanDelete"} />
            <SButton buttonName={"지시"} onClick={onClickModalOrder} type={"btnOrder"} />
            <SButton buttonName={"저장"} onClick={onClickModalSave} type={'btnPlanUpdateSave'} />
        </React.Fragment>
    )
}
const Status60buttons = ({ onClickModalCancel, onClickModalSave }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"지시취소"} onClick={onClickModalCancel} type={"btnCancel"} />
            <SButton buttonName={"저장"} onClick={onClickModalSave} type={'btnPlanUpdateSave'} />
        </React.Fragment>
    )
}
const Status70buttons = ({ onClickModalCancel, onClickModalComplete, onClickModalSave }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"지시취소"} onClick={onClickModalCancel} type={"btnCancel"} />                        
            <SButton buttonName={"완료"} onClick={onClickModalComplete} type={"btnCompletePA"} />
            <SButton buttonName={"저장"} onClick={onClickModalSave} type={'btnPlanUpdateSave'} />
        </React.Fragment>
    )
}
const Status70_2buttons = ({ onClickModalCancel, onClickModalComplete, onClickModalSave }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"지시취소"} onClick={onClickModalCancel} type={"btnCancel"} />
            <SButton buttonName={"완료"} onClick={onClickModalComplete} type={"btnCompleteByFD"} />
            <SButton buttonName={"저장"} onClick={onClickModalSave} type={'btnPlanUpdateSave'} />
        </React.Fragment>
    )
}
const Status10_2buttons = ({ onClickModalDelete, onClickModalRequire, onClickModalSave }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"삭제"} onClick={onClickModalDelete} type={"btnPlanDelete"} />
            <SButton buttonName={"작업요청"} onClick={onClickModalRequire} type={"btnDirectRequire"} />
            <SButton buttonName={"저장"} onClick={onClickModalSave} type={'btnPlanUpdateSave'} />
        </React.Fragment>
    )
}
const Status20buttons = ({ onClickModalOrder, onClickModalRequireConfirm, onClickModalSave, onClickModalDelete }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"삭제"} onClick={onClickModalDelete} type={"btnPlanDelete"} />
            <SButton buttonName={"지시"} onClick={onClickModalOrder} type={"btnOrderByFD"} />
            <SButton buttonName={"요청확인"} onClick={onClickModalRequireConfirm} type={"btnRequireConfirm"} />
            <SButton buttonName={"저장"} onClick={onClickModalSave} type={'btnPlanUpdateSaveByPA'} />
        </React.Fragment>
    )
}

const Status25buttons = ({ onClickModalCancel, onClickModalOrder, onClickModalSave }) => {
    return (
        <React.Fragment>
            <SButton buttonName={"지시취소"} onClick={onClickModalCancel} type={"btnCancel"} />
            <SButton buttonName={"지시"} onClick={onClickModalOrder} type={"btnOrderByFW"} />
            <SButton buttonName={"지시"} onClick={onClickModalOrder} type={"btnOrderByFD"} />
            <SButton buttonName={"저장"} onClick={onClickModalSave} type={'btnPlanUpdateSave'} />
        </React.Fragment>
    )
}

const Status00buttons = ({}) => {
    return (
        <React.Fragment>
        </React.Fragment>
    )
}
