import React, { Component } from "react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ProductionByDateSearchItem from "modules/pages/order/productionByDate/ProductionByDateSearchItem";
import ProductionByDateContents from "modules/pages/order/productionByDate/ProductionByDateContents";

class ProductionByDate extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return(
            <React.Fragment>
                <SearchTemplate searchItem={<ProductionByDateSearchItem />} />
                <ContentsMiddleTemplate contentSubTitle={'일별 생산량 조회 (집계기준 : 바코드 생산일자)'} />
                <ContentsTemplate id="ProductionByDate" contentItem={<ProductionByDateContents />} />
            </React.Fragment>
        );
    }
}

export default ProductionByDate;