import API from "components/override/api/API";

class ProductionByDateRepository {
    
    URL = "/api/order/productionByDate";
    
    /* 조회 */
    getProdByDate(param) {
        return API.request.get(encodeURI(`${this.URL}/getprodbydate?buId=${param.buId}&invenFromDt=${param.invenFromDt}&invenToDt=${param.invenToDt}&itemCdNm=${param.itemCdNm}&lotQuality=${param.lotQuality}&uomGubn=${param.uomGubn}&searchGubn=${param.searchGubn}&qtyGubn=${param.qtyGubn}`))
    }

}

export default new ProductionByDateRepository();