import { observable, action } from 'mobx';
import moment from 'moment';
import 'moment/locale/ko';

import BaseStore from 'utils/base/BaseStore';
import CommonStore from 'modules/pages/common/service/CommonStore';
import GridUtil from 'components/override/grid/utils/GridUtil';

import ItemCategoryRepository from 'modules/pages/system/itemCategory/service/ItemCategoryRepository';
import ProductionByDateRepository from 'modules/pages/order/productionByDate/service/ProductionByDateRepository';
import Alert from 'components/override/alert/Alert';

class ProductionByDateStore extends BaseStore {

    constructor() {
        super()
        this.setInitialState();
    }

    //검색 조건 변수
    @observable buId = CommonStore.buId;
    @observable buList = CommonStore.buList;
    @observable invenFromDt = moment().add(-7, 'days').format('YYYYMMDD');
    @observable invenToDt = moment().format('YYYYMMDD');
    @observable itemCdNm = '';
    @observable lotQuality = '';
    @observable searchGubn = 'L';
    @observable categoryCd = '';
    // @observable invenCheck = 'Y';
    @observable uomGubn = 10;
    @observable alphaCheck = false;
    @observable categoryGroup = []
    @observable lv = '1,2,3'
    @observable pinnedBottomRowData = [];

    @observable searchQty = "P,O,E";
    searchQtyGroup = [
        { cd: 'P', cdNm: '생산량' }
        , { cd: 'O', cdNm: '실적량' }
        , { cd: 'E', cdNm: 'ERP' }
    ];

    //그리드 변수
    @observable productionByDateList = []

    //데이터 고정 변수
    togle = false;

    //공장 불러오기
    @action.bound
    getBu = async () => {
        this.buId = CommonStore.buId;
        this.alphaCheck = true;
    }
    @action.bound
    handleChange = (data) => {
        this[data.id] = data.value;
        console.log(data.id, this[data.id])
    }

    //조회
    @action.bound
    handleSearchClick = async () => {

        if (this.searchQty == "") {
            Alert.meg("필드를 하나 이상 선택해주세요.");
            return;
        }

        const searchData = {
            "buId": this.buId
            , "invenFromDt": this.invenFromDt
            , "invenToDt": this.invenToDt
            , "itemCdNm": this.itemCdNm
            , "lotQuality": this.lotQuality
            , "uomGubn": this.uomGubn
            , "searchGubn": this.searchGubn
            , "qtyGubn": this.searchQty
        }

        const { data, status } = await ProductionByDateRepository.getProdByDate(searchData);

        if (status == 200) {

            this.productionByDateList = data.data;
            this.generateColumns();
            this.gridApi.setRowData(this.productionByDateList);

            this.togle = true;

        } else {
            Alert.meg(data.msg);
        }


    }

    @action.bound
    setGridApi(gridApi) {
        this.gridApi = gridApi;

        if (this.togle == true) {
            this.generateColumns();
        }
    }

    generateColumns = () => {

        var columnDefs = [];
        if (this.searchGubn == 'L') {
            columnDefs = [
                {
                    headerName: "라인"
                    , field: "lineNm"
                    , width: 110
                    , cellClass: 'cell_align_center'
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params)
                    }
                    , pinned: "left"
                },
                {
                    headerName: "제품코드"
                    , field: "itemCd"
                    , width: 100
                    , cellClass: 'cell_align_center'
                    , pinned: "left"
                },
                {
                    headerName: "제품"
                    , field: "itemNm"
                    , width: 150
                    , cellClass: 'cell_align_left'
                    , pinned: "left"
                },
                {
                    headerName: "단위"
                    , field: "uom"
                    , width: 50
                    , cellClass: 'cell_align_center'
                    , pinned: "left"
                }
            ];
        }
        else {

            const lv1 = (element) => element == 1;
            const lv1Hide = !this.lv.split(',').some(lv1)
            const lv2 = (element) => element == 2;
            const lv2Hide = !this.lv.split(',').some(lv2)
            const lv3 = (element) => element == 3;
            const lv3Hide = !this.lv.split(',').some(lv3)
            const lv4 = (element) => element == 4;
            const lv4Hide = !this.lv.split(',').some(lv4)

            columnDefs = [
                {
                    headerName: "레벨1"
                    , field: "lv1Nm"
                    , width: 100
                    , cellClass: 'cell_align_center'
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params)
                    }
                    , hide: lv1Hide
                    , pinned: "left"
                },
                {
                    headerName: "레벨2"
                    , field: "lv2Nm"
                    , width: 100
                    , cellClass: 'cell_align_center'
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params, false, ['lv1Nm', 'lv2Nm'])
                    }
                    , hide: lv2Hide
                    , pinned: "left"
                },
                {
                    headerName: "레벨3"
                    , field: "lv3Nm"
                    , width: 100
                    , cellClass: 'cell_align_center'
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params, false, ['lv1Nm', 'lv3Nm'])
                    }
                    , hide: lv3Hide
                    , pinned: "left"
                },
                {
                    headerName: "레벨4"
                    , field: "lv4Nm"
                    , width: 100
                    , cellClass: 'cell_align_center'
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params, false, ['lv1Nm', 'lv4Nm'])
                    }
                    , hide: lv4Hide
                    , pinned: "left"
                },
                {
                    headerName: "제품코드"
                    , field: "itemCd"
                    , width: 100
                    , cellClass: 'cell_align_center'
                    , pinned: "left"
                },
                {
                    headerName: "제품"
                    , field: "itemNm"
                    , width: 150
                    , cellClass: 'cell_align_left'
                    , pinned: "left"
                },
                {
                    headerName: "단위"
                    , field: "uom"
                    , width: 50
                    , cellClass: 'cell_align_center'
                    , pinned: "left"
                }
            ];
        }

        let date = [];

        const day = moment(this.invenToDt, 'YYYYMMDD').diff(this.invenFromDt, 'day');

        if (day > 30) {
            Alert.meg("31일을 초과했습니다.");
            return;
        }

        for (let i = 0; i < day + 1; i++) {

            let addDay = moment(this.invenFromDt).add(i, 'days').format('YYYY-MM-DD');
            date.push(addDay);
        }

        const bottom = [
            {
                uom: '합계',
            }
        ]

        const visibleProdQty = this.searchQty.indexOf('P') >= 0;
        const visibleOutputQty = this.searchQty.indexOf('O') >= 0;
        const visibleErpQty = this.searchQty.indexOf('E') >= 0;

        for (let k = 1; k < date.length + 1; k++) {
            let days;

            if (this.searchQty.split(',').length == 1) {
                if (visibleProdQty) {
                    days = this.addProdQty(date, k);
                } else if (visibleOutputQty) {
                    days = this.addOutputQty(date, k);
                } else if (visibleErpQty) {
                    days = this.addErpQty(date, k);
                }
            } else {
                days = {
                    headerName: `${date[k - 1]}`
                    , children: [
                    ]
                }

                if (visibleProdQty) {
                    days.children.push(this.addProdQty(date, k));
                    bottom[0][`day${k}`] = 0;
                }

                if (visibleOutputQty) {
                    days.children.push(this.addOutputQty(date, k));
                    bottom[0][`outputDay${k}`] = 0;
                }

                if (visibleErpQty) {
                    days.children.push(this.addErpQty(date, k));
                    bottom[0][`erpDay${k}`] = 0;
                }
            }

            columnDefs.push(days);
        }

        this.pinnedBottomRowData = bottom;
        this.gridApi.setColumnDefs(columnDefs);
    }

    addProdQty = (date, index) => {
        const check = this.searchQty.split(',').length == 1;
        return {
            headerName: check ? `${date[index - 1]}` : '생산량'
            , field: "day" + index
            , width: check ? 80 : 70
            , cellClass: 'cell_align_right'
            , valueFormatter: (params) => GridUtil.numberFormatter(params)
            // , cellClassRules: {
            //     'highlight_red': function (params) {
            //         if (!check) {
            //             if (params.value && params.data[`outputDay${index}`] && params.node.rowPinned != "bottom") {
            //                 if (params.value != params.data[`outputDay${index}`]) {
            //                     return true;
            //                 }
            //             }
            //         }
            //     }
            // }
        }
    }

    addOutputQty = (date, index) => {
        const check = this.searchQty.split(',').length == 1;
        return {
            headerName: check ? `${date[index - 1]}` : '실적량'
            , field: "outputDay" + index
            , width: check ? 80 : 70
            , cellClass: 'cell_align_right'
            , valueFormatter: (params) => GridUtil.numberFormatter(params)
            , cellClassRules: {
                'highlight_red': function (params) {
                    if (!check) {
                        if (params.data[`day${index}`] && params.node.rowPinned != "bottom") {
                            if (params.value != params.data[`day${index}`]) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
    }

    addErpQty = (date, index) => {
        const check = this.searchQty.split(',').length == 1;
        return {
            headerName: check ? `${date[index - 1]}` : 'ERP'
            , field: "erpDay" + index
            , width: check ? 80 : 70
            , cellClass: 'cell_align_right'
            , valueFormatter: (params) => GridUtil.numberFormatter(params)
            , cellClassRules: {
                'highlight_background': function (params) {
                    if (!check) {
                        if (params.node.rowPinned != "bottom") {
                            if (params.value != params.data[`outputDay${index}`] ||
                                params.data[`day${index}`] != params.data[`outputDay${index}`]) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
    }

    @action.bound
    SelectionChanged = async (selectedItem) => {
        this.searchGubn = selectedItem
    }
}

export default new ProductionByDateStore();