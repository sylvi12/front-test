import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid';

@inject(stores => ({
    setGridApi: stores.productionByDateStore.setGridApi
    ,productionByDateList: stores.productionByDateStore.productionByDateList
    ,pinnedBottomRowData: stores.productionByDateStore.pinnedBottomRowData
}))

class ProductionByDateContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setGridApi, productionByDateList, pinnedBottomRowData} = this.props;

        const rowClassRules = {
            'highlight_red_background': function (params) {
                if (params.data.lineId == -1) {
                    return true;
                }
            }
        }
        return (
            <SGrid
                grid={'productionByDateGrid'}
                gridApiCallBack={setGridApi}
                rowData={productionByDateList}
                pinnedBottomRowData={pinnedBottomRowData}
                cellReadOnlyColor={true}
                editable={false}
                rowClassRules={rowClassRules}
            />
        );
    }
}
export default ProductionByDateContents;