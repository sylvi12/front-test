import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SButton from 'components/atoms/button/SButton';
import SInput from 'components/atoms/input/SInput';
import SCheckBox from 'components/atoms/checkbox/SCheckBox';
import SRadio from 'components/atoms/radio/SRadio'
import SBuSelectBox from 'components/override/business/SBuSelectBox';
import STreeSelectBox from 'components/override/tree/STreeSelectBox';

import ProductionByDateStore from 'modules/pages/order/productionByDate/service/ProductionByDateStore';

@inject(stores => ({
    handleChange: stores.productionByDateStore.handleChange
    , handleSearchClick: stores.productionByDateStore.handleSearchClick
    , buId: stores.productionByDateStore.buId
    // , buList: stores.productionByDateStore.buList
    , invenFromDt: stores.productionByDateStore.invenFromDt
    , invenToDt: stores.productionByDateStore.invenToDt
    // , buSelectionChanged: stores.productionByDateStore.buSelectionChanged
    , itemCdNm: stores.productionByDateStore.itemCdNm
    // , invenCheck: stores.productionByDateStore.invenCheck
    , lotQuality: stores.productionByDateStore.lotQuality
    , uomGubn: stores.productionByDateStore.uomGubn
    , searchGubn: stores.productionByDateStore.searchGubn
    , SelectionChanged: stores.productionByDateStore.SelectionChanged
    , lv: stores.productionByDateStore.lv
    , searchQty: stores.productionByDateStore.searchQty
    , searchQtyGroup: stores.productionByDateStore.searchQtyGroup
}))

class ProductionByDateSearchItem extends Component {
    constructor(props) {
        super(props);

    }

    async componentWillMount() {
        if (ProductionByDateStore.alphaCheck == false) {
            await ProductionByDateStore.getBu();
        }
    }

    render() {

        const { buId
            // , buList
            , invenFromDt
            , invenToDt
            , handleChange
            , handleSearchClick
            , lv
            , buSelectionChanged
            , itemCdNm
            , uomGubn
            , lotQuality
            , searchGubn
            , invenCheck
            , searchQty, searchQtyGroup
            , SelectionChanged } = this.props;

        return (
            <React.Fragment>
                <div className='search_item_btn'>
                    <SButton
                        buttonName={'조회'}
                        type={'btnSearch'}
                        onClick={handleSearchClick}
                    />
                </div>
                <div style={{ display: "inline_block" }}>
                    <div className='search_line'>
                        <SBuSelectBox
                            title={"공장"}
                            id={"buId"}
                            value={buId}
                            onChange={handleChange}
                            isAddAll={false}
                        />

                        <SDatePicker
                            title={"기간"}
                            id={"invenFromDt"}
                            value={invenFromDt}
                            onChange={handleChange}
                        />

                        <SDatePicker
                            title={"~"}
                            id={"invenToDt"}
                            value={invenToDt}
                            onChange={handleChange}
                        />

                        <SInput
                            title={"제품"}
                            id={"itemCdNm"}
                            value={itemCdNm}
                            onChange={handleChange}
                            onEnterKeyDown={handleSearchClick}
                        />

                        <SSelectBox
                            title={"기준단위"}
                            id={"uomGubn"}
                            value={uomGubn}
                            codeGroup={"MFD015"}
                            onChange={handleChange}
                        />

                    </div>
                    <div className='search_line'>

                        <STreeSelectBox
                            title={'수량기준'}
                            id={"searchQty"}
                            value={searchQty}
                            optionGroup={searchQtyGroup}
                            onChange={handleChange}
                            contentWidth={250}
                        />

                        <SRadio
                            title={'그룹기준'}
                            id={"searchGubn"}
                            value={searchGubn}
                            codeGroup={"MFD018"}
                            onChange={handleChange}
                            onSelectionChange={SelectionChanged}
                        />

                        {searchGubn == 'I' ? (
                            <STreeSelectBox
                                title={"제품군"}
                                id={"lv"}
                                value={lv}
                                codeGroup={"MFD019"}
                                onChange={handleChange}
                                addOption={"전체"}
                                excludeCode={"99"}
                                contentWidth={220}
                            />
                        ) : (null)}

                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default ProductionByDateSearchItem;