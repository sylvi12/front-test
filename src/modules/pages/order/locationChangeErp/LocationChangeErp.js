import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate'
import ContentsTemplate from 'components/template/ContentsTemplate';

import LocationChangeErpMiddleItem from 'modules/pages/order/locationChangeErp/LocationChangeErpMiddleItem';
import LocationChangeErpContents from 'modules/pages/order/locationChangeErp/LocationChangeErpContents';
import LocationChangeErpItemContents from 'modules/pages/order/locationChangeErp/LocationChangeErpItemContents';
import LocationChangeErpItemMiddleItem from 'modules/pages/order/locationChangeErp/LocationChangeErpItemMiddleItem';
import LocationChangeErpSearchItem from 'modules/pages/order/locationChangeErp/LocationChangeErpSearchItem';

class LocationChangeErp extends Component {


    constructor(props) {
        super(props);

    }

    render() {

        return (
            <React.Fragment>
                <div style={{ width: '100%', height: '50%'}}>
                    <SearchTemplate searchItem={<LocationChangeErpSearchItem />} />
                    <ContentsTemplate id='barcodeTracking' contentItem={
                        <div className='basic'>
                                <ContentsMiddleTemplate contentSubTitle={'오더목록'} middleItem={<LocationChangeErpMiddleItem />} />
                                <ContentsTemplate id='versionContnents' contentItem={<LocationChangeErpContents />} />
                        </div>
                    } />
                </div>
                <div style={{ width: '100%', height: 'calc(50% - 10px)', marginTop: '10px'}}>
                    <SearchTemplate searchItem={<LocationChangeErpItemMiddleItem />} />
                    <ContentsTemplate id='barcodeTracking1' contentItem={
                        <div className='basic'>
                                <ContentsMiddleTemplate contentSubTitle={'바코드목록'} />
                                <ContentsTemplate id='deviceContnents' contentItem={<LocationChangeErpItemContents />} />
                        </div>
                    } />
                </div>
            </React.Fragment>
        )
    }
}
export default LocationChangeErp;