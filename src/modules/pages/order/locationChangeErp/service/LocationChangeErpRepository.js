import API from 'components/override/api/API';

class locationChangeErpRepository {

    URL = "/api/order/erplocationchange";
 
    getOrderList(params) {
        return API.request.get(encodeURI(`${this.URL}/getorder?buId=${params.buId}&orderDt=${params.orderDt}&orderNo=${params.orderNo}`))
    }

    getOrderItemList(param) {
        return API.request.get(encodeURI(`${this.URL}/getorderitemlist?orderNo=${param.orderNo}`))
    }

    upsertLocationList(params) {
        return API.request.post(`${this.URL}/upsertlocationlist`, params)
    }
}

export default new locationChangeErpRepository();