import { observable, action, toJS } from 'mobx';
import BaseStore from 'utils/base/BaseStore';

import CommonStore from 'modules/pages/common/service/CommonStore';
import CommonRepository from 'modules/pages/common/service/CommonRepository';
import GridUtil from 'components/override/grid/utils/GridUtil';

import LocationChangeErpRepository from 'modules/pages/order/locationChangeErp/service/LocationChangeErpRepository';
import Alert from 'components/override/alert/Alert';

import moment from 'moment';
import 'moment/locale/ko';

class LocationChangeErpStore extends BaseStore {

    constructor() {
        super()
        this.setInitialState()
    }

    @observable orderYymm = moment().format('YYYYMM'); //생산월
    @observable buId = CommonStore.buId;

    changeOrderGridApi = undefined;
    changeOrderList = []

    changeItemListGridApi = undefined;
    changeItemList = []
    @observable isLocationChangeList = [
    ];
    @observable targetLocId = 'toLocId'

    @observable orderNo = ''
    @observable locationId = '22'

    @observable ledgerType = ''

    @action.bound
    initGetOrderList = async () => {

        const params = {
            'buId': this.buId
            , 'orderDt': this.orderYymm
            , 'orderNo': this.orderNo
        }

        if (isNaN(this.orderNo)) {
            Alert.meg("지시번호를 다시 입력해주세요.")
            this.orderNo = ''
            return
        }

        const { data, status } = await LocationChangeErpRepository.getOrderList(params);
        if (status == 200) {
            this.changeOrderList = data.data
            this.changeOrderGridApi.setRowData(this.changeOrderList);
            this.changeItemListGridApi.setRowData([])
        }
    }

    @action.bound
    orderGridIsRowSelectable = (rowNode) => {

        if (!rowNode.data) {
            return;
        }

        if (rowNode.data.ifStatus == "I") {
            return false;
        } else {
            return true;
        }
    }

    @action.bound
    setChangeOrderGridApi = async (gridApi) => {

        this.changeOrderGridApi = gridApi;

        const columnDefs = [
            {
                headerName: "지시번호", field: "orderNo", width: 70, cellClass: 'cell_align_center', editable: false
                , cellRenderer: "rowSpanCellRender"
                , rowSpan: function (params) {
                    return GridUtil.rowSpanning(params)
                }
            }
            , {
                headerName: "홀드사유", field: "holdNm", width: 100, cellClass: 'cell_align_center', editable: false
                , cellRenderer: "rowSpanCellRender"
                , rowSpan: function (params) {
                    return GridUtil.rowSpanning(params, false, ['orderNo', 'holdNm'])
                }
            }
            , { headerName: "제품코드", field: "itemCd", width: 80, editable: false }
            , { headerName: "제품명", field: "itemNm", width: 300, editable: false }
            , { headerName: "총수량", field: "processQty", width: 80, cellClass: 'cell_align_center', editable: false }
            , { headerName: "미전송수량", field: "disTransQty", width: 80, cellClass: 'cell_align_center', editable: false }
            , { headerName: "전송수량", field: "transQty", width: 80, cellClass: 'cell_align_center', editable: false }
            , {
                headerName: "상태"
                , field: "lotQuality"
                , width: 70
                , editable: false
                , cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.getCodeNm(params, 'MFD003')
            }
            , {
                headerName: "홀드등록자", field: "inputEmpNm", width: 80, cellClass: 'cell_align_center', editable: false
                , cellRenderer: "rowSpanCellRender"
                , rowSpan: function (params) {
                    return GridUtil.rowSpanning(params, false, ['orderNo', 'inputEmpNm'])
                }
            }
            , {
                headerName: "홀드등록일", field: "inputDtm", width: 150, cellClass: 'cell_align_center', editable: false
                , cellRenderer: "rowSpanCellRender"
                , rowSpan: function (params) {
                    return GridUtil.rowSpanning(params, false, ['orderNo', 'inputEmpNm'])
                }
            },
            , {
                headerName: "전송상태", field: "ifStatus", width: 80, cellClass: 'cell_align_center', editable: false
                , valueFormatter: (params) => GridUtil.getCodeNm(params, 'MFD022')
            }
        ];
        this.changeOrderGridApi.setColumnDefs(columnDefs);
    }

    @action.bound
    setChangeItemListGridApi = async (gridApi) => {

        this.changeItemListGridApi = gridApi;

        var columnDefs = [
            {
                headerName: "", field: ""
                , width: 30
                , checkboxSelection: true
                , editable: false
                , headerCheckboxSelection: true
            }
            , { headerName: "지시번호", field: "orderNo", width: 70, cellClass: 'cell_align_center', editable: false }
            , { headerName: "바코드", field: "barcode", width: 150, editable: false }
            , { headerName: "제품코드", field: "itemCd", width: 80, editable: false }
            , { headerName: "제품명", field: "itemNm", width: 300, editable: false }
            , { headerName: "processQty", field: "processQty", width: 80, hide: true }
            , { headerName: "processUom", field: "processUom", width: 80, hide: true }
            , { headerName: "수량", field: "displayQty", width: 80, editable: false }
            , { headerName: "현재로케이션", field: "fromErpLocationId", width: 100, cellClass: 'cell_align_center', editable: false }
            , { headerName: "변경로케이션", field: "toErpLocationId", width: 100, cellClass: 'cell_align_center' }
            , { headerName: "itemId", field: "itemId", width: 100, hide: true }
            , {
                headerName: "전송상태", field: "ifStatus", width: 80, cellClass: 'cell_align_center', editable: false
                , valueFormatter: (params) => GridUtil.getCodeNm(params, 'MFD022')
            }
        ];

        this.changeItemListGridApi.setColumnDefs(columnDefs);
        const selected_Yn = CommonStore.codeObject["MFD021"]

        
        var locatonList = []
        this.isLocationChangeList = []

        selected_Yn.forEach(element => {
            if(element.desc1.split(',').indexOf(this.buId,0) != -1 && 
               element.desc2.split(',').indexOf(this.ledgerType,0) != -1) {
                locatonList.push(element)
                this.isLocationChangeList.push(element)
            }
        });

        const col_a = GridUtil.findColumnDef(columnDefs, 'fromErpLocationId');
        const col_b = GridUtil.findColumnDef(columnDefs, 'toErpLocationId');
        GridUtil.setSelectCellByList(col_a, locatonList, 'cd', 'cdNm');
        GridUtil.setSelectCellByList(col_b, locatonList, 'cd', 'cdNm');

        this.changeItemListGridApi.setColumnDefs(columnDefs);
    }

    @action.bound
    clickSearch = async () => {
        this.initGetOrderList();
    }

    @action.bound
    clickSend = async () => {

        const tempList = this.changeItemListGridApi.getSelectedRows()

        let sendList = []

        if (tempList.length == 0) {
            Alert.meg("선택된 항목이 없습니다.")
            return
        }

        tempList.forEach(element => {
            if(element.fromErpLocationId != element.toErpLocationId) {
                sendList.push(element)
            }
        });

        if (sendList.length == 0) {
            Alert.meg("변경 할 위치와 현 위치가 같습니다.")
            return
        }

        const { data, status } = await LocationChangeErpRepository.upsertLocationList(sendList);

        if (status == 200) {
            this.selectItemListSearch();
            this.initGetOrderList();
        }
    }

    @action.bound
    selectItemListSearch = async () => {

        const selectedRows = GridUtil.getSelectedRowData(this.changeOrderGridApi)

        if (selectedRows == undefined) {
            return
        }

        const param = {
            'orderNo': selectedRows.orderNo
        }

        this.ledgerType = selectedRows.ledgerType

        const { data, status } = await LocationChangeErpRepository.getOrderItemList(param)

        if (status == 200) {
            this.changeItemList = data.data
            this.changeItemListGridApi.setRowData(this.changeItemList)
            this.setChangeItemListGridApi(this.changeItemListGridApi)
        }
    }

    @action.bound
    clickChangeLocId = async () => {
        const changeList = this.changeItemListGridApi.getSelectedRows()

        if (changeList.length > 0) {
            changeList.forEach(element => {
                if (this.targetLocId == 'toLocId') {
                    element.toErpLocationId = this.locationId
                } else {
                    element.fromErpLocationId = this.locationId
                }
            });
            this.changeItemListGridApi.setRowData(this.changeItemList)
        } else {
            Alert.meg("선택된 항목이 없습니다.")
            return
        }
    }

    handleChange = (data) => {
        this[data.id] = data.value

        if (data.id == 'orderYymm') {
            this.orderYymm = moment(this.orderYymm).format('YYYYMM'); //생산월
        }
    }



}

export default new LocationChangeErpStore();