import React, { Component } from 'react';
import { inject } from 'mobx-react';
import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setChangeOrderGridApi: stores.locationChangeErpStore.setChangeOrderGridApi
    , changeOrderList: stores.locationChangeErpStore.changeOrderList
    , handleDeviceChanged: stores.locationChangeErpStore.handleDeviceChanged
    , initGetOrderList: stores.locationChangeErpStore.initGetOrderList
    , selectItemListSearch: stores.locationChangeErpStore.selectItemListSearch
}))
class LocationChangeErpContents extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { initGetOrderList } = this.props;
        initGetOrderList();
    }

    render() {

        const { setChangeOrderGridApi, changeOrderList, selectItemListSearch} = this.props;

        return (
            <div className="panel_body" style={{ height: '100%' }}>
                <SGrid
                    grid={'changeOrderGrid'}
                    gridApiCallBack={setChangeOrderGridApi}
                    rowData={changeOrderList}
                    onCellClicked={selectItemListSearch}
                    cellReadOnlyColor={false}
                />
            </div>
        );
    }
}
export default LocationChangeErpContents;