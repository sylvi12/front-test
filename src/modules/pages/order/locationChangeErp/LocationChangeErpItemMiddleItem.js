import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton'
import SSelectBox from 'components/atoms/selectbox/SSelectBox'

@inject(stores => ({
     clickChangeLocId: stores.locationChangeErpStore.clickChangeLocId
    ,handleChange: stores.locationChangeErpStore.handleChange
    ,locationId: stores.locationChangeErpStore.locationId
    ,clickSend: stores.locationChangeErpStore.clickSend
    ,isLocationChangeList : stores.locationChangeErpStore.isLocationChangeList
}))
class LocationChangeErpItemMiddleItem extends Component {

    render() {

        const {handleChange, locationId, clickSend, clickChangeLocId, isLocationChangeList} = this.props;
        return (
            <React.Fragment>
                
                <div className='search_item_btn'>
                    <SButton
                        buttonName={'전체위치변경'}
                        type={'btnChange'}
                        onClick={clickChangeLocId}
                    />

                    <SButton
                        buttonName={'전송'}
                        type={'btnSend'}
                        onClick={clickSend}
                    />
                </div>
                <div className='search_line'>
                    <SSelectBox
                        title={"전체변경위치"}
                        id="locationId"
                        value={locationId}
                        optionGroup={isLocationChangeList}
                        valueMember={"cd"}
                        displayMember={"cdNm"}
                        onChange={handleChange}
                    />
                    
                </div>
            </React.Fragment>
        )
    }
}

export default LocationChangeErpItemMiddleItem;