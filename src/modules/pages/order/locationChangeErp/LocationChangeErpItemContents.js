import React, { Component } from 'react';
import { inject } from 'mobx-react';
import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
      setChangeItemListGridApi: stores.locationChangeErpStore.setChangeItemListGridApi
    , orderGridIsRowSelectable: stores.locationChangeErpStore.orderGridIsRowSelectable
    , changeItemList: stores.locationChangeErpStore.changeItemList
    
}))
class LocationChangeErpItemContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setChangeItemListGridApi, changeItemList, orderGridIsRowSelectable} = this.props;

        return (
            <div className="panel_body" style={{ height: '100%' }}>
                <SGrid
                    grid={'ChangeItemListGrid'}
                    gridApiCallBack={setChangeItemListGridApi}
                    rowData={changeItemList}
                    suppressRowClickSelection={true}
                    isRowSelectable={orderGridIsRowSelectable}
                    cellReadOnlyColor={false}
                />
            </div>
        );
    }
}
export default LocationChangeErpItemContents;