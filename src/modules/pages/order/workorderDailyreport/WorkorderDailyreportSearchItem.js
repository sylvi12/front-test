import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SDatePicker from 'components/override/datepicker/SDatePicker';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SButton from 'components/atoms/button/SButton';
import SInput from 'components/atoms/input/SInput';
import STree from 'components/override/tree/STree';
import SBuSelectBox from 'components/override/business/SBuSelectBox'

import WorkorderDailyreportStore from 'modules/pages/order/workorderDailyreport/service/WorkorderDailyreportStore';

@inject(stores => ({
    buId: stores.workorderDailyreportStore.buId
    , handleChange: stores.workorderDailyreportStore.handleChange
    , buSelectionChanged: stores.workorderDailyreportStore.buSelectionChanged

    , lineId: stores.workorderDailyreportStore.lineId
    , lineGroup: stores.workorderDailyreportStore.lineGroup
    , lineSelectionChanged: stores.workorderDailyreportStore.lineSelectionChanged

    , dateType_s: stores.workorderDailyreportStore.dateType_s

    , startDate: stores.workorderDailyreportStore.startDate
    , endDate: stores.workorderDailyreportStore.endDate

    , woStatus: stores.workorderDailyreportStore.woStatus
    , woStatusSelectionChanged: stores.workorderDailyreportStore.woStatusSelectionChanged

    , item: stores.workorderDailyreportStore.item
    , handleSearchClick: stores.workorderDailyreportStore.handleSearchClick
}))

class WorkorderDailyreportSearchItem extends Component {

    constructor(props) {
        super(props);
    }

    async componentWillMount() {
        if (WorkorderDailyreportStore.alphaCheck == false) {
            await WorkorderDailyreportStore.getBu();
            await WorkorderDailyreportStore.getLine();
        }

    }

    render() {

        const { buId, handleChange, buSelectionChanged
            , lineId, lineGroup, lineSelectionChanged
            , dateType_s
            , startDate, endDate
            , woStatus, woStatusSelectionChanged
            , item
            , handleSearchClick } = this.props;

        return (

            <React.Fragment>

                <SBuSelectBox
                    title={"공장"}
                    id={"buId"}
                    value={buId}
                    onChange={handleChange}
                    onSelectionChange={buSelectionChanged}
                />

                <SSelectBox
                    title={"생산라인"}
                    id={"lineId"}
                    value={lineId}
                    pk={"lineId,buId"}
                    optionGroup={lineGroup}
                    valueMember={'lineId'}
                    displayMember={'lineNm'}
                    onChange={handleChange}
                    onSelectionChange={lineSelectionChanged}
                    contentWidth={130}
                />

                <SSelectBox
                    title={" "}
                    titleWidth={50}
                    id={"dateType_s"}
                    value={dateType_s}
                    codeGroup={"SYC003"}
                    onChange={handleChange}
                    defaultValue={"01"}
                    contentWidth={90}
                />
                <SDatePicker
                    id={"startDate"}
                    value={startDate}
                    onChange={handleChange}
                />
                <SDatePicker
                    title={"~"}
                    id={"endDate"}
                    value={endDate}
                    onChange={handleChange}
                />
                <STree
                    title={"상태"}
                    id={"woStatus"}
                    value={woStatus}
                    codeGroup={"MFD004"}
                    onChange={handleChange}
                    addOption={"전체"}
                    onSelectionChange={woStatusSelectionChanged}
                    contentWidth={220}
                />
                <SInput
                    title={'제품'}
                    id={'item'}
                    onChange={handleChange}
                    onEnterKeyDown={handleSearchClick}
                    value={item}
                    contentWidth={150}
                />

                <div className='search_item_btn'>
                    <SButton
                        buttonName={'조회'}
                        type={'btnSearch'}
                        onClick={handleSearchClick} />
                </div>
            </React.Fragment>
        )
    }
}

export default WorkorderDailyreportSearchItem;