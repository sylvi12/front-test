import React, { Component } from "react";
import { inject } from "mobx-react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";
import SModal from "components/override/modal/SModal";

import WorkorderDailyreportSearchItem from "modules/pages/order/workorderDailyreport/WorkorderDailyreportSearchItem";
import WorkorderDailyreportContents from "modules/pages/order/workorderDailyreport/WorkorderDailyreportContents";

@inject(stores => ({
}))

class WorkorderDailyreport extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {
        } = this.props

        return (
            <React.Fragment>
                <SearchTemplate searchItem={<WorkorderDailyreportSearchItem />} />
                <ContentsMiddleTemplate contentSubTitle={'포장지시 내역'} />
                <ContentsTemplate id='workorderDailyreport' contentItem={<WorkorderDailyreportContents />} />
            </React.Fragment>
        )
    }
}

export default WorkorderDailyreport;