import { observable, action } from 'mobx';
import moment from 'moment';
import 'moment/locale/ko';

import Alert from 'components/override/alert/Alert';
import GridUtil from 'components/override/grid/utils/GridUtil';
import BaseStore from 'utils/base/BaseStore';
import CommonStore from 'modules/pages/common/service/CommonStore';

import WorkorderDailyreportRepository from 'modules/pages/order/workorderDailyreport/service/WorkorderDailyreportRepository';
import WorkorderRepository from 'modules/pages/order/workorder/service/WorkorderRepository';



class WorkorderDailyreportStore extends BaseStore {

    // Main SearchItem
    @observable alphaCheck = false;
    @observable buId = '';
    @observable lineId = '';
    @observable lineGroup = [];
    @observable dateType_s = '';
    @observable startDate = moment().add(-7, 'days').format('YYYYMMDD');
    @observable endDate = moment().format('YYYYMMDD');
    @observable woStatus = '';
    @observable item = '';
    selectedLine = {};

    // Main Contents
    workorderDailyreportGridApi = undefined;
    @observable gridHeader = [];
    @observable workorderDailyreportList = [];

    /* 로그인 유저 정보 */
    @observable userInfo = {};

    constructor() {
        super();
        this.setInitialState();
    }

    @action.bound
    handleChange = (data) => {
        console.log(data);
        this[data.id] = data.value;
    }
    // main searchItem 공장 선택사항 변경 시
    @action.bound
    buSelectionChanged = async (selectedItem) => {
        await this.getLine();
    }
    // getBu
    @action.bound
    getBu = () => {
        this.buId = CommonStore.buId;
    }
    // search에 라인 넣기
    @action.bound
    getLine = async () => {
        //userInfo에서 아웃소싱 YN을 체크해서 Y이면 해당 라인만 가져오면 된다.
        this.userInfo = CommonStore.userInfo;
        if (!this.userInfo.outsourcingYn && this.userInfo.outsourcingYn == "Y") {
            const searchData = {
                "loginId": this.userInfo.loginId
            }
            const { data, status } = await WorkorderRepository.getLine2(searchData);
            if (status == 200) {
                this.lineGroup = data.data;
                this.lineId = data.data[0].lineId;
                this.alphaCheck = true;
            }
        } else {
            const searchData = {
                "buId": this.buId
            }
            const { data, status } = await WorkorderRepository.getLine(searchData);
            if (status == 200) {
                this.lineGroup = data.data;
                if (data.data.length > 0) {
                    this.lineId = data.data[0].lineId;
                }
                this.alphaCheck = true;
            }
        }
    }
    // main searchItem 라인 선택사항 변경 시
    @action.bound
    lineSelectionChanged = async (selectedItem) => {
        this.selectedLine = selectedItem;
        // this.handleSearchClick();
    }
    // main searchItem 상태 선택사항 변경 시
    @action.bound
    woStatusSelectionChanged = async (selectedItem) => {
        // this.handleSearchClick();
    }

    // 포장지시 grid
    @action.bound
    setGridApi = async (gridApi) => {
        this.workorderDailyreportGridApi = gridApi;
        this.generateColumns();
    }
    // 조회
    @action.bound
    handleSearchClick = async () => {
        const searchData = {
            "buId": this.buId
            , "lineId": this.lineId
            , "dateType": this.dateType_s
            , "startDate": this.startDate
            , "endDate": this.endDate
            , "woStatus": this.woStatus
            , "item": this.item
        }
        const { data, status } = await WorkorderDailyreportRepository.getDailyreport(searchData);
        if (status == 200) {
            this.gridHeader = data[0];
            this.workorderDailyreportList = data[1];
            this.generateColumns();
        } else {

        }
    }
    // 그리드 컬럼 만들기
    @action.bound
    generateColumns = async () => {
        const columnDefs = [
            {
                headerName: "라인"
                , field: "lineNm"
                , width: 90
                , cellClass: 'cell_align_center'
                , cellRenderer: "rowSpanCellRender"
                , rowSpan: function(params) {
                    return GridUtil.rowSpanning(params);
                }
                , pinned: "left"
            }

        ];
        let columns = [];
        let size = this.gridHeader.length;
        for (let i = 0; i < size; i++) {
            columns = {
                headerName: `${this.gridHeader[i].colDate}`
                , field: `${this.gridHeader[i].colNm}`
                , width: 220
                // , cellClass: 'cell_align_center'
                , editable: false
                , cellClass: "cell-wrap-text"
                , autoHeight: true
                , cellRenderer: "dailyCellRender"
                , colSpan: GridUtil.colSpanning

            }
            columnDefs.push(columns);
        }
        this.workorderDailyreportGridApi.setColumnDefs(columnDefs);
        this.workorderDailyreportGridApi.setRowData(this.workorderDailyreportList);
    }
}
export default new WorkorderDailyreportStore();