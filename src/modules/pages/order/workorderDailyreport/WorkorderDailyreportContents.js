import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setGridApi: stores.workorderDailyreportStore.setGridApi
    , workorderDailyreportList: stores.workorderDailyreportStore.workorderDailyreportList
    , onSelectionChanged: stores.workorderDailyreportStore.onSelectionChanged
    , handleUpdateClick: stores.workorderDailyreportStore.handleUpdateClick
}))

class WorkorderDailyreportContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setGridApi, workorderDailyreportList, onSelectionChanged, handleUpdateClick } = this.props;

        return (
            <SGrid
                grid={'workorderDailyreportGrid'}
                gridApiCallBack={setGridApi}
                rowData={workorderDailyreportList}
                onCellClicked={onSelectionChanged}
                editable={false}
                rowDoubleClick={handleUpdateClick}
                suppressRowClickSelection={true}
            />
        );
    }
}
export default WorkorderDailyreportContents;