import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SRadio from 'components/atoms/radio/SRadio';

@inject(stores => ({
     mergeType: stores.itemChangeErpStore.mergeType
    ,mergeTypeList: stores.itemChangeErpStore.mergeTypeList
    ,handleChange: stores.itemChangeErpStore.handleChange
}))
class ItemChangeErpMiddleItem extends Component {

    render() {

        const {mergeType, mergeTypeList, handleChange} = this.props;
        return (
            <React.Fragment>
                <SRadio title={"기준"}
                        id={"mergeType"}
                        value={mergeType}
                        optionGroup={mergeTypeList}
                        onChange={handleChange}
                     />
            </React.Fragment>
        )
    }
}

export default ItemChangeErpMiddleItem;