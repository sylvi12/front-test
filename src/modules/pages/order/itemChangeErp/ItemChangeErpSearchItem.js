import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton';
import SInput from 'components/atoms/input/SInput';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SBuSelectBox from 'components/override/business/SBuSelectBox';

@inject(stores => ({
    buId: stores.itemChangeErpStore.buId
    ,orderYymm: stores.itemChangeErpStore.orderYymm
    ,orderNo: stores.itemChangeErpStore.orderNo
    ,handleChange: stores.itemChangeErpStore.handleChange
    ,clickSearch: stores.itemChangeErpStore.clickSearch
}))

class WorkorderErpSearchItem extends Component {

    constructor(props) {
        super(props);
    }
    
    render() {

        const {
             buId
            ,orderYymm
            ,orderNo
            ,handleChange
            ,clickSearch
        } = this.props;
        return (
            <React.Fragment>
                <div className='search_item_btn'>
                    <SButton
                        buttonName={'조회'}
                        type={'btnSearch'}
                        onClick={clickSearch} 
                    />
                </div>
                <div className='search_line'>
                    <SBuSelectBox
                        title={'공장'}
                        id={"buId"}
                        value={buId}
                        isAddAll={false}
                        onChange={handleChange}
                        //onSelectionChange={handleSearchClick}
                    />
                    <SDatePicker
                        title={'출고월'}
                        id={"orderYymm"}
                        value={orderYymm}
                        onChange={handleChange}
                        dateFormat={"yyyy-MM"}
                    />
                    <SInput
                        title={'지시번호'}
                        id={'orderNo'}
                        value={orderNo}
                        onChange={handleChange}
                        onEnterKeyDown={clickSearch}
                    />
                </div>
            </React.Fragment>
        )
    }
}

export default WorkorderErpSearchItem;