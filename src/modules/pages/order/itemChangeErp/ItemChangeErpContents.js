import React, { Component } from 'react';
import { inject } from 'mobx-react';
import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setChangeOrderGridApi: stores.itemChangeErpStore.setChangeOrderGridApi
    , changeOrderList: stores.itemChangeErpStore.changeOrderList
    , handleDeviceChanged: stores.itemChangeErpStore.handleDeviceChanged
    , initGetOrderList: stores.itemChangeErpStore.initGetOrderList
    , selectItemListSearch: stores.itemChangeErpStore.selectItemListSearch
    , orderGridIsRowSelectable: stores.itemChangeErpStore.orderGridIsRowSelectable
}))
class ItemChangeErpContents extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { initGetOrderList } = this.props;
        initGetOrderList();
    }

    render() {

        const { setChangeOrderGridApi, changeOrderList, selectItemListSearch, orderGridIsRowSelectable } = this.props;

        return (
            <div className="panel_body" style={{ height: '100%' }}>
                <SGrid
                    grid={'changeOrderGrid'}
                    gridApiCallBack={setChangeOrderGridApi}
                    rowData={changeOrderList}
                    onCellClicked={selectItemListSearch}
                    suppressRowClickSelection={true}
                    cellReadOnlyColor={false}
                    isRowSelectable={orderGridIsRowSelectable}
                />
            </div>
        );
    }
}
export default ItemChangeErpContents;