import React, { Component } from 'react';
import { inject } from 'mobx-react';
import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
      setChangeItemListGridApi: stores.itemChangeErpStore.setChangeItemListGridApi
    , changeItemList: stores.itemChangeErpStore.changeItemList
    
}))
class ItemChangeErpItemContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setChangeItemListGridApi, changeItemList } = this.props;

        return (
            <div className="panel_body" style={{ height: '100%' }}>
                <SGrid
                    grid={'ChangeItemListGrid'}
                    gridApiCallBack={setChangeItemListGridApi}
                    rowData={changeItemList}
                    cellReadOnlyColor={false}
                />
            </div>
        );
    }
}
export default ItemChangeErpItemContents;