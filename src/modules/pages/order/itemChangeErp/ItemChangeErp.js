import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate'
import ContentsTemplate from 'components/template/ContentsTemplate';

import ItemChangeErpMiddleItem from 'modules/pages/order/itemChangeErp/ItemChangeErpMiddleItem';
import ItemChangeErpContents from 'modules/pages/order/itemChangeErp/ItemChangeErpContents';
import ItemChangeErpItemContents from 'modules/pages/order/itemChangeErp/ItemChangeErpItemContents';
import ItemChangeErpItemMiddleItem from 'modules/pages/order/itemChangeErp/ItemChangeErpItemMiddleItem';
import ItemChangeErpSearchItem from 'modules/pages/order/itemChangeErp/ItemChangeErpSearchItem';

class ItemChangeErp extends Component {

    
    constructor(props) {
        super(props);
        
    }

    render() {
        
        return (
            <React.Fragment>
                <SearchTemplate searchItem={<ItemChangeErpSearchItem />} />
                <ContentsTemplate id='barcodeTracking' contentItem={
                    <div className='basic'>
                        <div style={{ width: '55%', height: '100%', float: 'left' }}>
                            <ContentsMiddleTemplate contentSubTitle={'오더목록'}  middleItem={<ItemChangeErpMiddleItem />}/>
                            <ContentsTemplate id='versionContnents' contentItem={<ItemChangeErpContents />} />
                        </div>
                        <div style={{ width: '45%', height: '100%', float: 'right' }}>
                            <ContentsMiddleTemplate contentSubTitle={'바코드목록'} middleItem={<ItemChangeErpItemMiddleItem/>} />
                            <ContentsTemplate id='deviceContnents' contentItem={<ItemChangeErpItemContents />}/>
                        </div>
                    </div>
                    } />
            </React.Fragment>
        )
    }
}

export default ItemChangeErp;