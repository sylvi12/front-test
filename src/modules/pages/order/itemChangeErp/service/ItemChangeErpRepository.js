import API from 'components/override/api/API';

class ItemChangeErpRepository {

    URL = "/api/order/erpitemchange";
 
    getOrderList(params) {
        return API.request.get(encodeURI(`${this.URL}/getorder?buId=${params.buId}&orderDt=${params.orderDt}&orderNo=${params.orderNo}`))
    }

    getOrderItemList(param) {
        return API.request.get(encodeURI(`${this.URL}/getorderitemlist?orderNo=${param.orderNo}&mergeType=${param.mergeType}&orderType=${param.orderType}&inoutDetailNo=${param.inoutDetailNo}`))
    }

    changeOrderStatus(params) {
        return API.request.post(`${this.URL}/updatestatus`, params)
    }
}

export default new ItemChangeErpRepository();