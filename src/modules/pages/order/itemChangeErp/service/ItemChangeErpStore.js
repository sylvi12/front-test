import { observable, action, toJS } from 'mobx';
import BaseStore from 'utils/base/BaseStore';

import CommonStore from 'modules/pages/common/service/CommonStore';
import CommonRepository from 'modules/pages/common/service/CommonRepository';
import GridUtil from 'components/override/grid/utils/GridUtil';

import ItemChangeErpRepository from 'modules/pages/order/itemChangeErp/service/ItemChangeErpRepository';
import Alert from 'components/override/alert/Alert';

import moment from 'moment';
import 'moment/locale/ko';

class ItemChangeErpStore extends BaseStore {

    constructor() {
        super()
        this.setInitialState()
    }

    @observable orderYymm = moment().format('YYYYMM'); //생산월
    @observable buId = CommonStore.buId;

    changeOrderGridApi = undefined;
    changeOrderList = []

    changeItemListGridApi = undefined;
    changeItemList = []

    @observable mergeType = 'Y'
    @observable orderNo = ''
    // @observable tranType = 'Y'

    mergeTypeList = [
        { cd: "Y", cdNm: "바코드모드" },
        { cd: "N", cdNm: "수량모드" },
    ];

    // tranTypeList = [
    //     { cd: "Y", cdNm: "전송" },
    //     { cd: "N", cdNm: "반송" },
    // ];

    @action.bound
    initGetOrderList = async () => {

        const params = {
            'buId': this.buId
            , 'orderDt': this.orderYymm
            , 'orderNo': this.orderNo
        }

        if (isNaN(this.orderNo)) {
            Alert.meg("지시번호를 다시 입력해주세요.")
            this.orderNo = ''
            return
        }

        const { data, status } = await ItemChangeErpRepository.getOrderList(params);
        if (status == 200) {
            this.changeOrderList = data.data
            this.changeOrderGridApi.setRowData(this.changeOrderList);
            this.changeItemListGridApi.setRowData([])
        }
    }

    @action.bound
    orderGridIsRowSelectable = (rowNode) => {
        if(this.tranType == 'Y') {
            if (rowNode.data.inoutStatus == 10 || rowNode.data.inoutStatus == 40) {
                return true
            }
            return false
        } 
        else {
            if (rowNode.data.inoutStatus == 20) {
                return true
            }
            return false
        }
    }

    @action.bound
    setChangeOrderGridApi = async (gridApi) => {

        this.changeOrderGridApi = gridApi;

        const columnDefs = [
            {
                headerName: "", field: ""
                , width: 30
                , checkboxSelection: true
                , headerCheckboxSelection: true
            }
            , { headerName: "지시번호", field: "orderNo", width: 70, cellClass: 'cell_align_center', editable: false }
            , { headerName: "전송상태", field: "inoutStatusNm", width: 100, cellClass: 'cell_align_center', editable: false }
            , { headerName: "상세번호", field: "inoutDetailNo", width: 80, cellClass: 'cell_align_center', hide: true, editable: false }
            , { headerName: "지시타입", field: "orderType", cellClass: 'cell_align_center', hide: true, editable: false }
            , { headerName: "품변제품코드", field: "itemCd", width: 100, editable: false }
            , { headerName: "품변제품", field: "itemNm", width: 120, editable: false }
            , { headerName: "전송자", field: "modEmpNm", width: 80, cellClass: 'cell_align_center', editable: false }
            , { headerName: "전송시간", field: "modDtm", cellClass: 'cell_align_center', width: 100, editable: false }
            , { headerName: "비고", field: "ifDesc", width: 300, editable: false }
            , { headerName: "전송타입", field: "tranType", hide: true}
        ];
        this.changeOrderGridApi.setColumnDefs(columnDefs);
    }

    @action.bound
    setChangeItemListGridApi = async (gridApi) => {

        this.changeItemListGridApi = gridApi;

        var columnDefs = []

        if (this.mergeType == 'Y') {
            columnDefs = [
                {
                    headerName: "지시번호", field: "orderNo", width: 70, cellClass: 'cell_align_center', editable: false, cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params)
                    }
                }
                , {
                    headerName: "제품코드", field: "itemCd", width: 80, editable: false, cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params, false, ['orderNo', 'itemCd'])
                    }
                }
                , {
                    headerName: "제품명", width: 300, field: "itemNm", editable: false, cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params, false, ['orderNo', 'itemNm'])
                    }
                }
                , { headerName: "수량", field: "processQty", width: 100, editable: false }
                , { headerName: "바코드", field: "barcode", width: 100, editable: false }
            ];
        }
        else {
            columnDefs = [
                { headerName: "지시번호", field: "orderNo", width: 70, cellClass: 'cell_align_center', editable: false }
                , { headerName: "제품코드", field: "itemCd", width: 80, editable: false }
                , { headerName: "제품명", field: "itemNm", width: 300, editable: false }
                , { headerName: "수량", field: "processQty", width: 100, editable: false }
            ];

        }

        this.changeItemListGridApi.setColumnDefs(columnDefs);
    }

    @action.bound
    clickSearch = async () => {
        this.initGetOrderList();
    }

    @action.bound
    clickSend = async () => {

        const sendList = this.changeOrderGridApi.getSelectedRows()

        if (sendList.length == 0) {
            Alert.meg("선택된 항목이 없습니다.")
            return
        }

        const { data, status } = await ItemChangeErpRepository.changeOrderStatus(sendList);

        if (status == 200) {
            this.initGetOrderList();
        }
    }

    @action.bound
    selectItemListSearch = async () => {

        const selectedRows = GridUtil.getSelectedRowData(this.changeOrderGridApi)

        if (selectedRows == undefined) {
            return
        }

        const param = {
            'mergeType': this.mergeType
            , 'orderNo': selectedRows.orderNo
            , 'orderType': selectedRows.orderType
            , 'inoutDetailNo': selectedRows.inoutDetailNo
        }

        const { data, status } = await ItemChangeErpRepository.getOrderItemList(param)

        if (status == 200) {
            this.changeItemList = data.data
            this.changeItemListGridApi.setRowData(this.changeItemList)
            this.setChangeItemListGridApi(this.changeItemListGridApi)
        }

    }

    handleChange = (data) => {
        
        this[data.id] = data.value

        if (data.id == 'orderYymm') {
            this.orderYymm = moment(this.orderYymm).format('YYYYMM'); //생산월
        }

        if(data.id == 'tranType') {
            this.setChangeOrderGridApi(this.changeOrderGridApi)
        }

        if (data.id == 'mergeType') {
            switch (data.value) {
                case 'Y':
                    this.mergeTitle = '바코드모드'
                    break
                case 'N':
                    this.mergeTitle = '수량모드'
                    break
            }
            this.selectItemListSearch()
        }
    }



}

export default new ItemChangeErpStore();