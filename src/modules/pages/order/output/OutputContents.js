import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setGridApi: stores.outputStore.setGridApi,
    inputBarcodeList: stores.outputStore.inputBarcodeList,
    selectBarcodeLocSearch: stores.outputStore.selectBarcodeLocSearch
}))

class OutputContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setGridApi, inputBarcodeList, selectBarcodeLocSearch} = this.props;

        return (
            <SGrid
                grid={'outputGrid'}
                gridApiCallBack={setGridApi}
                rowData={inputBarcodeList}
                onSelectionChanged={selectBarcodeLocSearch}
                editable={false}
            />
        );
    }
}
export default OutputContents;