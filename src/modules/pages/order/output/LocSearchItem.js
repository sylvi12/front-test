import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    // packingRequirements: stores.outputStore.packingRequirements,
    confrimProd: stores.outputStore.confrimProd,
}))

class LocSearchItem extends Component {

    render() {

        const {confrimProd} = this.props;
        return (
            <React.Fragment>
                <div className='search_item_btn'>
                    <SButton
                        className="btn_red"
                        buttonName={'입고 확정'}
                        type={'btnProdInsert'}
                        onClick={confrimProd}
                    />
                </div>
            </React.Fragment>
        )
    }
}

export default LocSearchItem;