import API from "components/override/api/API";

class OutputRepository {

    URL = "/api/order/output";
    /* 조회 */
    // 공장 셀렉트박스 만들 때

    getBarcode() {
        return API.request.get(encodeURI(`barcode/getbarcode`))
    }

    getLoclist(barcode) {
        return API.request.get(encodeURI(`loc/getloclist/${barcode}`))
    }

    insertProd(params) {
        console.log('여기오지?')
        return API.request.post(encodeURI(`stock/setstockinsert`), params);
    }

    // outputModal 조회
    getModalOutput(params) {
        return API.request.get(encodeURI(`${this.URL}/getmodaloutput?buId=${params.buId}&prodDt=${params.prodDt}&prodStartDt=${params.prodStartDt}&prodEndDt=${params.prodEndDt}&prodShift=${params.prodShift}&prodTeam=${params.prodTeam}&orderNo=${params.orderNo}`))
    }
}
export default new OutputRepository();