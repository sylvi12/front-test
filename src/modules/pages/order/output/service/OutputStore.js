import { observable, action } from 'mobx';
import moment from 'moment';
import 'moment/locale/ko';

import Alert from 'components/override/alert/Alert';
import GridUtil from 'components/override/grid/utils/GridUtil';
import BaseStore from 'utils/base/BaseStore';

import OutputRepository from 'modules/pages/order/output/service/OutputRepository';

class OutputStore extends BaseStore {

    // Prod contents
    gridApi = undefined;
    @observable inputBarcodeList = [];

    // Loc contents
    locGridApi = undefined;
    @observable vacancyLocList = [];

    // search Key
    barcode = ''
    locId = ''
    


    constructor() {
        super();
        this.setInitialState();
    }

    @action.bound
    handleChange = (data) => {
        console.log(data);
        this[data.id] = data.value;
    }

    /* MAIN */
    // main contents
    @action.bound
    setGridApi = async (gridApi) => {

        this.gridApi = gridApi;

        const columnDefs = [
            {
                headerName: "바코드"
                , field: "barcode"
                , width: 150
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "제품명"
                , field: "itemNm"
                , width: 200
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "제품코드"
                , field: "itemCd"
                , width: 100
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "중량(KG)"
                , field: "qty"
                , width: 80
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "제조일자"
                , field: "makeDt"
                , width: 100
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "유통기한"
                , field: "efftDt"
                , width: 100
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
        ];
        this.gridApi.setColumnDefs(columnDefs);
    }

    @action.bound
    setLocGridApi = async (gridApi) => {

        this.locGridApi = gridApi;

        const columnDefs = [
            {
                headerName: "Sector"
                , field: "locNm"
                , width: 150
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "SectorID"
                , field: "locCd"
                , hide: true
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "적재가능량"
                , field: "capaQty"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "현적재량"
                , field: "onhandQty"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "적재여부"
                , field: "useYn"
                , width: 80
                , cellClass: 'cell_align_center'
            },
        ];
        this.locGridApi.setColumnDefs(columnDefs);
    }

    @action.bound
    handleSearchClick = async () => {
        
        const { data, status } = await OutputRepository.getBarcode();

        if (status !=  200) {
            Alert.meg('서버와 연결이 끊켰습니다.');
        } else {
            this.inputBarcodeList = data;

            console.log(this.inputBarcodeList , '??')
            this.gridApi.setRowData(this.inputBarcodeList);
        }
    }

    @action.bound
    confrimProd = async () => {

        const selectedRow = this.locGridApi.getSelectedRows()[0];


        console.log(selectedRow, 'selectedRow???')
        
        if(selectedRow.locCd != undefined) {

            const saveData = {
                "locCd": selectedRow.locCd
                , "barcode": this.barcode
            }
    
            const params = JSON.stringify(saveData);

            const { data, status } = await OutputRepository.insertProd(params);

            if (status !=  200) {
                Alert.meg('서버와 연결이 끊켰습니다.');
            } else {
                Alert.meg('입고가 완료되었습니다.');
                this.handleSearchClick();
                this.locGridApi.setRowData([]);
            }
        }
    }

    @action.bound
    selectBarcodeLocSearch = async () => {

        const selectedRow = this.gridApi.getSelectedRows()[0];
        
        if(selectedRow.barcode != undefined) {

            this.barcode = selectedRow.barcode

            const { data, status } = await OutputRepository.getLoclist(selectedRow.barcode);

            if (status !=  200) {
                Alert.meg('서버와 연결이 끊켰습니다.');
            } else {
                this.vacancyLocList = data;
                this.locGridApi.setRowData(this.vacancyLocList);
            }

        }
    }
}

export default new OutputStore();