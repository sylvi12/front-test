import React, { Component } from "react";
import { inject } from "mobx-react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";
import OutputSearchItem from "modules/pages/order/output/OutputSearchItem";
import OutputContents from "modules/pages/order/output/OutputContents";
import LocContents from "modules/pages/order/output/LocContents";
import LocSearchItem from "modules/pages/order/output/LocSearchItem";



@inject(stores => ({
    modalIsOpen: stores.outputStore.modalIsOpen
    , closeModal: stores.outputStore.closeModal

    , prodDtModalIsOpen: stores.outputStore.prodDtModalIsOpen
    , closeProdDtModal: stores.outputStore.closeProdDtModal

    , prodQtyModalIsOpen: stores.outputStore.prodQtyModalIsOpen
    , closeProdQtyModal: stores.outputStore.closeProdQtyModal

    , isLocationOpen: stores.outputStore.isLocationOpen
    , locationBuId: stores.outputStore.locationBuId
    , locationHelperResult: stores.outputStore.locationHelperResult
    , handleChange: stores.outputStore.handleChange
}))

class Output extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { 
        } = this.props

        return (
            <React.Fragment>
                <div className='basic'>
                        <div style={{ width: '50%', height: '100%', float: 'left' }}>
                            <ContentsMiddleTemplate contentSubTitle={'입고목록'} middleItem={<OutputSearchItem />} />
                            <ContentsTemplate id='form' contentItem={<OutputContents />} />
                        </div>
                        <div style={{ width: '50%', height: '100%', float: 'right' }}>
                            <ContentsMiddleTemplate contentSubTitle={'가용창고현황'} middleItem={<LocSearchItem />} />
                            <ContentsTemplate id='btn' contentItem={<LocContents />} />
                        </div>
                    </div>
            </React.Fragment>
        )
    }
}

export default Output;