import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";
import SNumericInput from 'components/override/numericinput/SNumericInput';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SInput from 'components/atoms/input/SInput';
import SButton from 'components/atoms/button/SButton';
import SGrid from 'components/override/grid/SGrid';

import ItemHelper from 'components/codehelper/ItemHelper';
import ItemModalHelper from 'components/codehelper/ItemModalHelper';

@inject(stores => ({
	erpPartDivideInsert: stores.workorderErpStore.erpPartDivideInsert,
	lineNo: stores.workorderErpStore.lineNo,
	issueDay: stores.workorderErpStore.issueDay,
	fromItemCd: stores.workorderErpStore.fromItemCd,
	fromItemNm: stores.workorderErpStore.fromItemNm,
	issueQty: stores.workorderErpStore.issueQty,
	issueUom: stores.workorderErpStore.issueUom,
	changeQty: stores.workorderErpStore.changeQty,
	isItemOpen: stores.workorderErpStore.isItemOpen,
	erpPartDivideAdd: stores.workorderErpStore.erpPartDivideAdd,
	itemHelperOption: stores.workorderErpStore.itemHelperOption,
	handleChange: stores.workorderErpStore.handleChange,
	handleHelperResult: stores.workorderErpStore.handleHelperResult,
	selectedItem: stores.workorderErpStore.selectedItem,
	setWorkorderErpPartDivideGridApi: stores.workorderErpStore.setWorkorderErpPartDivideGridApi,
	workorderErpPartDivideList: stores.workorderErpStore.workorderErpPartDivideList,
	erpPartDivideCancel: stores.workorderErpStore.erpPartDivideCancel
}))

export default class WorkorderErpDivideModal extends Component {

	constructor(props) {
		super(props);
	}

	render() {

		const {
			erpPartDivideInsert
			, erpPartDivideCancel
			, lineNo
			, issueDay
			, fromItemCd
			, fromItemNm
			, issueQty
			, issueUom
			, setWorkorderErpPartDivideGridApi
			, workorderErpPartDivideList
			, changeQty
			, isItemOpen
			, erpPartDivideAdd
			, itemHelperOption
			, handleChange
			, handleHelperResult
			, selectedItem
		} = this.props;

		return (
			<React.Fragment>
				<div className="nonStyle">
					<SearchTemplate contentSubTitle={'분할전송 From'} searchItem={
						<WorkorderErpDivideSearchItem
							erpPartDivideInsert={erpPartDivideInsert}
							lineNo={lineNo}
							issueDay={issueDay}
							fromItemCd={fromItemCd}
							fromItemNm={fromItemNm}
							issueQty={issueQty}
							issueUom={issueUom}
							changeQty={changeQty}
							handleChange={handleChange}
						/>} />
				</div>
				<ContentsMiddleTemplate contentSubTitle={'분할 상세'} middleItem={
				<WorkorderErpDivideMiddleItem
					isItemOpen={isItemOpen}
					erpPartDivideAdd={erpPartDivideAdd}
					itemHelperOption={itemHelperOption}
					handleChange={handleChange}
					handleHelperResult={handleHelperResult}
					selectedItem={selectedItem}
					erpPartDivideCancel={erpPartDivideCancel}
				 />} />
				<ContentsTemplate id='erpDivide' contentItem={
					<WorkorderErpDivideContents
						setWorkorderErpPartDivideGridApi={setWorkorderErpPartDivideGridApi}
						workorderErpPartDivideList={workorderErpPartDivideList}
					/>} />
			</React.Fragment>
		);
	}
}

const WorkorderErpDivideSearchItem = ({ erpPartDivideInsert, lineNo, issueDay, fromItemCd, fromItemNm, issueQty, issueUom, changeQty, handleChange }) => {
	return (
		<React.Fragment>
			<div className="search_item_btn">
			<SButton
				buttonName={'전송'}
				type={'btnPartDivideInsert'}
				onClick={erpPartDivideInsert}
			/>
			</div>
			<table style={{ width: "100%" }}>
				<tbody>
					<tr>
						<td className="th">실적순번</td>
						<td>
							<SNumericInput
								id={"lineNo"}
								value={lineNo}
								readOnly={true}
							/>
						</td>
						<td className="th">사용일자</td>
						<td>
							<SDatePicker
								id={"issueDay"}
								value={issueDay}
								onChange={handleChange}
								readOnly={true}
							/>
						</td>
					</tr>
					<tr>
						<td className="th">품목</td>
						<td>
							<SInput
								id={"fromItemCd"}
								value={fromItemCd}
								readOnly={true}
							/>
							<SInput
								id={"fromItemNm"}
								value={fromItemNm}
								readOnly={true}
							/>
						</td>
						<td className="th">분할량</td>
						<td>
							<SNumericInput
								id={"changeQty"}
								value={changeQty}
								readOnly={true}
							/>
						</td>
					</tr>
					<tr>
						<td className="th">사용량</td>
						<td>
							<SNumericInput
								id={"issueQty"}
								value={issueQty}
								readOnly={true}
							/>
							<SInput
								id={"issueUom"}
								value={issueUom}
								readOnly={true}
							/>
						</td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</React.Fragment>
	)
}

const WorkorderErpDivideMiddleItem = ({
	isItemOpen
	, handleHelperResult
	, itemHelperOption
	, erpPartDivideAdd
	, handleChange
	, erpPartDivideCancel
}) => {
	return (
		<React.Fragment>
			<ItemModalHelper
                    id={'isItemOpen'}
                    isItemOpen={isItemOpen}
                    handleChange={handleChange}
                    onHelperResult={handleHelperResult}
                    helperOption={itemHelperOption}
            />
			<SButton
				className="btn_red"
				buttonName={'추가'}
				type={'btnErpPartDivideAdd'}
				onClick={erpPartDivideAdd}
			/>
			<SButton
				className="btn_red"
				buttonName={'삭제'}
				type={'btnErpPartDivideCancel'}
				onClick={erpPartDivideCancel}
			/>
		</React.Fragment>
	)
}

const WorkorderErpDivideContents = ({ setWorkorderErpPartDivideGridApi, workorderErpPartDivideList }) => {
	return (
		<React.Fragment>
			<SGrid
				grid={'workorderGrpGrid'}
				gridApiCallBack={setWorkorderErpPartDivideGridApi}
				rowData={workorderErpPartDivideList}
				suppressRowClickSelection={true}
				cellReadOnlyColor={true}
				editable={true}
			/>
		</React.Fragment>
	)
}