import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import SButton from 'components/atoms/button/SButton';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';
import SGrid from 'components/override/grid/SGrid';
import SInput from 'components/atoms/input/SInput';
import SModal from "components/override/modal/SModal";
import SNumericInput from 'components/override/numericinput/SNumericInput';
import STab from 'components/override/tab/STab';

import WorkorderErpDivideModal from "modules/pages/order/workordererp/workordererpmodal/WorkorderErpDivideModal"


@inject(stores => ({
    setOutputGridApi: stores.workorderErpStore.setOutputGridApi,
    outputList: stores.workorderErpStore.outputList,
    pinnedBottomRowData: stores.workorderErpStore.pinnedBottomRowData,
    pinnedOutputBottomRowData: stores.workorderErpStore.pinnedOutputBottomRowData,
    currentQty: stores.workorderErpStore.currentQty,
    groupQtyUpdate: stores.workorderErpStore.groupQtyUpdate,
    groupQtyCancel: stores.workorderErpStore.groupQtyCancel,
    setWorkorderErpGrpGridApi: stores.workorderErpStore.setWorkorderErpGrpGridApi,
    workorderErpGrpList: stores.workorderErpStore.workorderErpGrpList,
    adjustQty: stores.workorderErpStore.adjustQty,
    setWorkorderErpFirstGridApi: stores.workorderErpStore.setWorkorderErpFirstGridApi,
    erpFirstList: stores.workorderErpStore.erpFirstList,
    setWorkorderErpSecondGridApi: stores.workorderErpStore.setWorkorderErpSecondGridApi,
    erpSecondList: stores.workorderErpStore.erpSecondList,
    setWorkorderErpThirdGridApi: stores.workorderErpStore.setWorkorderErpThirdGridApi,
    erpThirdList: stores.workorderErpStore.erpThirdList,
    setUseOfRawMaterialGridApi: stores.workorderErpStore.setUseOfRawMaterialGridApi,
    useOfRawMaterialList: stores.workorderErpStore.useOfRawMaterialList,
    adjustChangeQty: stores.workorderErpStore.adjustChangeQty,
    useOfRawMaterialInsert: stores.workorderErpStore.useOfRawMaterialInsert,
    useOfRawMaterialCancel: stores.workorderErpStore.useOfRawMaterialCancel,
    activeIndex: stores.workorderErpStore.activeIndex,
    handleErpTabChange: stores.workorderErpStore.handleErpTabChange,
    workorderErpDivideModalIsOpen: stores.workorderErpStore.workorderErpDivideModalIsOpen,
    workorderErpDivideCloseModal: stores.workorderErpStore.workorderErpDivideCloseModal,
    openWorkorderErpDivideModal: stores.workorderErpStore.openWorkorderErpDivideModal,
}))

export default class WorkorderErpModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setOutputGridApi, outputList, pinnedBottomRowData, pinnedOutputBottomRowData,
            currentQty, groupQtyUpdate, groupQtyCancel,
            setWorkorderErpGrpGridApi, workorderErpGrpList, adjustQty,
            setWorkorderErpFirstGridApi, erpFirstList, setWorkorderErpSecondGridApi, erpSecondList,
            setWorkorderErpThirdGridApi, erpThirdList, setUseOfRawMaterialGridApi, useOfRawMaterialList, adjustChangeQty,
            useOfRawMaterialInsert, useOfRawMaterialCancel, workorderErpDivideModalIsOpen, workorderErpDivideCloseModal,
            openWorkorderErpDivideModal
        } = this.props;


        return (
            <React.Fragment>
                {/* ////////////////WMS TAB MENU /////////////// */}
                <STab activeIndex={this.props.activeIndex} onChange={this.props.handleErpTabChange} id={"activeIndex"} parentHeight={800}>
                    <div style={{ width: '100%', height: '100%', float: 'left', overflowY: 'auto' }} header="WMS">
                        <ContentsMiddleTemplate contentSubTitle={'운전일보'} expandTargetId= {"outputGrid"} />
                        <WorkorderErpOutput
                            id={"WorkorderErpOutput"}
                            setOutputGridApi={setOutputGridApi}
                            outputList={outputList}
                            pinnedOutputBottomRowData={pinnedOutputBottomRowData}
                        />

                        <ContentsMiddleTemplate contentSubTitle={'생산실적'} expandTargetId= {"workorderGrpGrid"}
                            middleItem={
                                <WorkorderErpGroupBtn
                                    currentQty={currentQty}
                                    groupQtyUpdate={groupQtyUpdate}
                                    groupQtyCancel={groupQtyCancel}
                                />} />
                        <WorkorderErpGroup
                            setWorkorderErpGrpGridApi={setWorkorderErpGrpGridApi}
                            workorderErpGrpList={workorderErpGrpList}
                            pinnedBottomRowData={pinnedBottomRowData}
                            adjustQty={adjustQty}
                        />

                        {/* <ContentsMiddleTemplate contentSubTitle={'원료사용'} expandTargetId= {"useOfRawMaterialGrid"}
                            middleItem={
                                <WorkorderErpBomBtn
                                    openWorkorderErpDivideModal={openWorkorderErpDivideModal}
                                    useOfRawMaterialInsert={useOfRawMaterialInsert}
                                    useOfRawMaterialCancel={useOfRawMaterialCancel}
                                />} />
                        <WorkorderErpBom
                            setUseOfRawMaterialGridApi={setUseOfRawMaterialGridApi}
                            useOfRawMaterialList={useOfRawMaterialList}
                            adjustChangeQty={adjustChangeQty}
                        /> */}

                    </div>
                    {/* ////////////////ERP TAB MENU /////////////// */}
                    <div style={{ width: '100%', height: '100%', float: 'left', overflowY: 'auto' }} header="ERP">
                        <ContentsMiddleTemplate contentSubTitle={'WORK ORDER'} expandTargetId= {"workorderErpFirstGrid"}/>
                        <WorkorderErpFirstInfo
                            setWorkorderErpFirstGridApi={setWorkorderErpFirstGridApi}
                            erpFirstList={erpFirstList}
                        />

                        <ContentsMiddleTemplate contentSubTitle={'PART LIST'} expandTargetId= {"workorderErpSecondGrid"}/>
                        <WorkorderErpSecondInfo
                            setWorkorderErpSecondGridApi={setWorkorderErpSecondGridApi}
                            erpSecondList={erpSecondList}
                        />

                        <ContentsMiddleTemplate contentSubTitle={'ITEM LEDGER'} expandTargetId= {"workorderErpThirdGrid"}/>
                        <WorkorderErpThirdInfo
                            setWorkorderErpThirdGridApi={setWorkorderErpThirdGridApi}
                            erpThirdList={erpThirdList}
                        />
					</div>
				</STab>

            <SModal
                formId={'order_workorder_erp_partdivide_p'}
                id={'partDivideModal'}
                isOpen={workorderErpDivideModalIsOpen}
                onRequestClose={workorderErpDivideCloseModal}
                contentLabel="분할전송"
                width={600}
                height={450}
                contents={<WorkorderErpDivideModal />}
            />
			</React.Fragment >
		);
    }
}

const WorkorderErpOutput = ({ setOutputGridApi, outputList, pinnedOutputBottomRowData }) => {

    return (
        <SGrid
            grid={'outputGrid'}
            gridApiCallBack={setOutputGridApi}
            rowData={outputList}
            cellReadOnlyColor={true}
            editable={false}
            pinnedBottomRowData={pinnedOutputBottomRowData}
            domLayout={"autoHeight"}
        />
    )
}

const WorkorderErpGroupBtn = ({ groupQtyUpdate, groupQtyCancel, currentQty }) => {
    return (
        <React.Fragment>
            <SNumericInput
                title="투입량"
                value={currentQty}
                readOnly={true}
            />
            <SButton
                className="btn_red"
                buttonName={'전송'}
                type={'btnErpProdInsert'}
                onClick={groupQtyUpdate}
            />
            <SButton
                className="btn_red"
                buttonName={'취소'}
                type={'btnErpProdCancel'}
                onClick={groupQtyCancel}
            />
        </React.Fragment>
    )
}

const WorkorderErpGroup = ({ setWorkorderErpGrpGridApi, workorderErpGrpList, pinnedBottomRowData, adjustQty }) => {
    console.log('pinnedBottomRowData', pinnedBottomRowData);
    return (
        <SGrid
            grid={'workorderGrpGrid'}
            gridApiCallBack={setWorkorderErpGrpGridApi}
            rowData={workorderErpGrpList}
            suppressRowClickSelection={true}
            cellReadOnlyColor={true}
            editable={false}
            pinnedBottomRowData={pinnedBottomRowData}
            onCellValueChanged={adjustQty}
            domLayout={"autoHeight"}
        />
    )
}

const WorkorderErpBomBtn = ({ openWorkorderErpDivideModal, useOfRawMaterialInsert, useOfRawMaterialCancel }) => {
    return (
        <React.Fragment>
            <SButton
                className="btn_red"
                buttonName={'분할전송'}
                onClick={openWorkorderErpDivideModal}
                type={'btnErpPartDivideInsert'}
            />
            <SButton
                className="btn_red"
                buttonName={'전송'}
                onClick={useOfRawMaterialInsert}
                type={'btnErpPartInsert'}
            />
            <SButton
                className="btn_red"
                buttonName={'취소'}
                onClick={useOfRawMaterialCancel}
                type={'btnErpPartCancel'}
            />
        </React.Fragment>
    )
}

const WorkorderErpBom = ({ setUseOfRawMaterialGridApi, useOfRawMaterialList, adjustChangeQty }) => {
    return (
        <SGrid
            grid={'useOfRawMaterialGrid'}
            gridApiCallBack={setUseOfRawMaterialGridApi}
            rowData={useOfRawMaterialList}
            suppressRowClickSelection={true}
            cellReadOnlyColor={true}
            onCellValueChanged={adjustChangeQty}
            domLayout={"autoHeight"}
        // editable={false}
        />
    )
}

//ERP TAB_WORK ORDER
const WorkorderErpFirstInfo = ({ setWorkorderErpFirstGridApi, erpFirstList }) => {
    return (
        <SGrid
            grid={'workorderErpFirstGrid'}
            gridApiCallBack={setWorkorderErpFirstGridApi}
            rowData={erpFirstList}
            cellReadOnlyColor={true}
            editable={false}
            domLayout={"autoHeight"}
        />
    )
}

//ERP TAB_PART LIST
const WorkorderErpSecondInfo = ({ setWorkorderErpSecondGridApi, erpSecondList }) => {
    return (
        <SGrid
            grid={'workorderErpSecondGrid'}
            gridApiCallBack={setWorkorderErpSecondGridApi}
            rowData={erpSecondList}
            cellReadOnlyColor={true}
            editable={false}
            domLayout={"autoHeight"}
        />
    )
}

//ERP TAB_ITEM LEDGER
const WorkorderErpThirdInfo = ({ setWorkorderErpThirdGridApi, erpThirdList }) => {
    return (
        <SGrid
            grid={'workorderErpThirdGrid'}
            gridApiCallBack={setWorkorderErpThirdGridApi}
            rowData={erpThirdList}
            cellReadOnlyColor={true}
            editable={false}
            domLayout={"autoHeight"}
        />
    )
}