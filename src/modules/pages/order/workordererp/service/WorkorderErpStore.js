import { observable, action } from 'mobx';
import moment from 'moment';
import 'moment/locale/ko';

import Alert from 'components/override/alert/Alert';
import GridUtil from 'components/override/grid/utils/GridUtil';

import WorkorderErpRepository from 'modules/pages/order/workordererp/service/WorkorderErpRepository';
import WorkorderRepository from 'modules/pages/order/workorder/service/WorkorderRepository';
import ItemOptionModel from 'components/codehelper/model/ItemOptionModel';
import BaseStore from 'utils/base/BaseStore';
import CommonStore from 'modules/pages/common/service/CommonStore';
import { Grid } from 'ag-grid-community';

class WorkorderErpStore extends BaseStore {

	//ERP실적전송 WorkorderErpSearchItem 변수
	@observable buIdCheck = false;
	@observable buId = ""; 								//공장
	@observable startDtm = moment().add(-7, 'days').format('YYYY-MM-DD'); //생산월
	@observable endDtm   = moment().format('YYYY-MM-DD'); //생산월
	@observable ifStatus = '';	 						//전송여부
	@observable woStatus = ''; 							//지시상태
	@observable orderNo = ''; 							//지시번호
	@observable itemCdNm = '';							//품목코드 or 품목명

	// 벌크 모달
	@observable searchStartDtm = moment().add(-7, 'days').format('YYYY-MM-DD')
	@observable searchEndDtm = moment().format('YYYY-MM-DD')
	@observable bulkItemCd = '';
	@observable dateType = "Y"
	@observable bulkList = [];
	@observable bulkModalIsOpen = false
	@observable bulkListGridApi = undefined
	dateTypeList = [
        { cd: "Y", cdNm: "출고일" },
        { cd: "N", cdNm: "확정일" },
    ];

	//ERP실적전송 WorkorderErpContents 변수
	@observable workOrderErpGridApi = undefined;
	@observable workOrderErpList = [];
	@observable pinnedBottomRowData = [];

	//생산실적전송 Modal WorkorderErpModal 운전일보 변수
	@observable workorderErpModalIsOpen = false;
	@observable outputGridApi = undefined;
	@observable outputList = [];
	@observable pinnedOutputBottomRowData = [
		{
			prodShiftNm: '합계',
			prodQty: 0
		}
	];

	//생산실적전송 Modal WorkorderErpModal 운전일보 변수
	@observable workorderErpGrpGridApi = undefined;
	@observable workorderErpGrpList = [];
	@observable pinnedBottomRowData = [
		{
			prodDt: '합계',
			prodQty: 0,
			repackQty: 0,
			outputQty: 0,
			ifQty: 0,
			notIfQty: 0

		}
	];
	//생산실적전송(Modal WorkorderErpModal) 투입량 변수
	@observable currentQtyStore = 0;
	@observable currentQty = 0;
	@observable ifQtyResult = 0; //전송량(ifQty) 변수

	//생산실적전송(Modal WorkorderErpModal) 원료사용 변수
	@observable useOfRawMaterialGridApi = undefined;
	@observable useOfRawMaterialList = [];
	@observable urmChangeQty = 0;
	@observable erpLocIdList = [];

	//생산실적전송(Modal WorkorderErpModal) ERP (WORK ORDER / PART LIST / ITEM LEDGER) 변수
	@observable erpFirstList = [];
	@observable workorderErpFirstGridApi = undefined;
	@observable erpSecondList = [];
	@observable workorderErpSecondGridApi = undefined;
	@observable erpThirdList = [];
	@observable workorderErpThirdGridApi = undefined;

	//생산실적전송(Modal WorkorderErpModal) TAB 전환 변수
	@observable activeIndex = 0;


	//분할전송(Modal_SubModal WorkorderErpDivideModal) 변수
	@observable lineNo = 0;
	@observable issueDay = moment().format('YYYYMMDD');
	@observable fromItemCd = '';
	@observable fromItemNm = '';
	@observable issueQty = 0;
	@observable issueUom = '';
	@observable changeQty = 0;
	@observable workorderErpDivideModalIsOpen = false;
	@observable workorderErpPartDivideGridApi = undefined;
	@observable workorderErpPartDivideList = [];
	
	//ItemHelperOption
	@observable itemHelperOption = new ItemOptionModel().optionModel;
	@observable isItemOpen = false;
	@observable selectedItem = {};

	//라인
	@observable lineId = ''; 							//라인
	@observable lineGroup = []



	constructor() {
		super();
		this.itemHelperOption.mode = 'fix';
		this.itemHelperOption.cdNmIsFocus = true;
		this.itemHelperOption.glclass = "S210,S220,S231";
		this.itemHelperOption.buId = this.buId;
		this.setInitialState();
	}

	//RP실적전송(WorkorderErpSearchItem) 공장 조회옵션 기본 설정
	@action.bound
	getBuId = async ()=> {
		this.buId = CommonStore.buId;
		this.buIdCheck = true;

		const searchData = {
			"buId": this.buId
		}
		const { data, status } = await WorkorderRepository.getLine(searchData);
		if (status == 200) {
			this.lineGroup = data.data;
		}
	}

	//ERP실적전송(WorkorderErpSearchItem) 입력값 확인 및 변경
	@action.bound
	handleChange = async (data) => {
		console.log('handleChange', data)
		this[data.id] = data.value;

		if(data.id == 'buId') {
			const searchData = {
				"buId": this.buId
			}
			const { data, status } = await WorkorderRepository.getLine(searchData);
			if (status == 200) {
				this.lineGroup = data.data;
			}
		}

	}


	//ERP실적전송(WorkorderErpSearchItem) '조회'Btn
	@action.bound
	handleSearchClick = async () => {
		const submitData = {
			"buId": this.buId
			, "startDtm": this.startDtm
			, "endDtm": this.endDtm
			// , "process": this.process
			, "lineId": this.lineId
			, "ifStatus": this.ifStatus
			, "woStatus": this.woStatus
			, "orderNo": Number(this.orderNo)
			, "itemCdNm": this.itemCdNm
			, "lineId" : this.lineId
		}
		console.log('parameter', submitData)
		const { data, status } = await WorkorderErpRepository.getWoList(submitData);
		if (status === 200 && data.success) {
			this.workOrderErpList = data.data;
			this.workOrderErpGridApi.setRowData(this.workOrderErpList);
		} else {
			Alert.meg(data.msg);
		}
	}

	//ERP실적전송(WorkorderErpSearchItem) 작업지시조회 그리드
	@action.bound
	setWorkOrderGridApi = async (gridApi) => {
		this.workOrderErpGridApi = gridApi;
		this.pinnedBottomRowData = [
            {
                orderNo: '합계',
				prodQty: 0,
				ifQty: 0,
				notIfQty: 0,
            }
        ];
		const columnDefs = [
			{
				headerName: "",
				children: [
					{
						headerName: "", field: ""
						, width: 30
						, checkboxSelection: true
						, headerCheckboxSelection: true
						// , editable: function (params) { }
					},
					{ headerName: "지시번호", field: "orderNo", width: 70, cellClass: 'cell_align_center' },
					{ headerName: "제품코드", field: "itemCd", width: 100, cellClass: 'cell_align_center' },
					{
						headerName: "제품명", field: "itemNm", width: 140, cellClass: 'cell_align_left'
					},
					{
						headerName: "생산시작일자", field: "startDt", width: 120, cellClass: 'cell_align_center'
						, valueFormatter: GridUtil.dateFormatter
					},
					{
						headerName: "생산종료일자", field: "endDt", width: 120, cellClass: 'cell_align_center'
						, valueFormatter: GridUtil.dateFormatter
					},
					{
						headerName: "지시상태", field: "woStatus", width: 100, cellClass: 'cell_align_center'
						, valueFormatter: (params) => GridUtil.getCodeNm(params, 'MFD004')
					},
					{
						headerName: "ERP오더번호", field: "erpOrderNo", width: 100, cellClass: 'cell_align_center'
					},
					{
						headerName: "단위", field: "orderUom", width: 70, cellClass: 'cell_align_center'
					}
				]
			},
			{
				headerName: "생산량",
				children: [
					{
						headerName: "지시수량", field: "prodQty", width: 100, cellClass: 'cell_align_right'
						, valueFormatter: (params) => GridUtil.numberFormatter(params)
					},
					{
						headerName: "전송수량", field: "ifQty", width: 100, cellClass: 'cell_align_right'
						, valueFormatter: (params) => GridUtil.numberFormatter(params)
					},
					{
						headerName: "미전송", field: "notIfQty", width: 100, cellClass: 'cell_align_right'
						, valueFormatter: (params) => GridUtil.numberFormatter(params)
					}
				]
			}
			// {
			// 	headerName: "원료량",
			// 	children: [
			// 		{
			// 			headerName: "지시수량", field: "-", width: 100, cellClass: 'cell_align_right'
			// 			, valueFormatter: (params) => GridUtil.numberFormatter(params)
			// 		},
			// 		{
			// 			headerName: "전송수량", field: "-", width: 100, cellClass: 'cell_align_right'
			// 			, valueFormatter: (params) => GridUtil.numberFormatter(params)
			// 		},
			// 		{
			// 			headerName: "미전송", field: "-", width: 100, cellClass: 'cell_align_center'
			// 			, valueFormatter: (params) => GridUtil.getCodeNm(params, 'MFD003')
			// 		}
			// 	]
			// }
		];
		this.workOrderErpGridApi.setColumnDefs(columnDefs);
	}

	//ERP실적전송(WorkorderErpMiddleItem) '전송' Btn
	@action.bound
	erpForwarding = async () => {
		const selectedRows = this.workOrderErpGridApi.getSelectedRows();
		if (!selectedRows || selectedRows.length == 0) {
			Alert.meg('전송할 항목을 체크해주세요.');
			return;
		}
		for (let i = 0; i < selectedRows.length; i++) {
			if (selectedRows[i].ifStatus != null || selectedRows[i].erpOrderNo != null) {
				Alert.meg('전송중인 항목 입니다.')
				return;
			}
		}
		const value = await Alert.confirm("전송하시겠습니까?").then(result => { return result; });
		if (value) {
			const { data, status } = await WorkorderErpRepository.setErpWoInsert(selectedRows);
			if (data.success == false) {
				Alert.meg(data.msg);
			}
			// if (status === 200 && data.success) {
			// 	console.log('성공', data.data)
			// 	Alert.meg('전송 성공');
			// } else {
			// 	Alert.meg('전송 실패');
			// }
		} else {
			return;
		}
	}

	//ERP실적전송(WorkorderErpMiddleItem) 'Excel' Btn
	@action.bound
	excelPrint = () => {
		console.log('Excel Print')
	}

	//ERP실적전송(WorkorderErpCotents) 더블클릭 Action
	@action.bound
	handleDoubleClick = async () => {
		const selectedRowData = GridUtil.getSelectedRowData(this.workOrderErpGridApi);
		// FIX ME 주석해제
		if (selectedRowData.erpOrderNo != null) {
		const searchData = {
			"buId": selectedRowData.buId,
			"lineId": selectedRowData.lineId,
			"prodDt": selectedRowData.startDt,
			"orderNo": selectedRowData.orderNo,
			"orderYymm": selectedRowData.orderYymm,
			"erpOrderNo_i": Number(selectedRowData.erpOrderNo)
		}

		console.log('DoubleClick_param', searchData)

		// 화면 로드 전에 가져와야 하는 부분
		await this.getErpLocIdDropDownList(searchData);
		await this.getProcessQty(searchData);
		this.workorderErpModalIsOpen = true;
		if (this.activeIndex == 0) {
			const { data, status } = await WorkorderErpRepository.getOutput(searchData);
			if (data.success == false) {
				Alert.meg(data.msg);
			} else {
				this.outputList = data.data;
				this.outputGridApi.setRowData(this.outputList);
			}
			await this.getMiddleWorkOrderErp(searchData);
			//await this.setUseOfRawMaterial(searchData);

		}
		else if (this.activeIndex == 1) {
			this.workorderErpModalIsOpen = true;
			await this.getWorkorderErpFirst(searchData);
			await this.getWorkorderErpSecond(searchData);
			await this.getWorkorderErpThird(searchData);
		}
		// FIX ME 주석해제
		} else {
			console.log('erpOrderNo 확인불가')
		}
	}

	//생산실적전송(Modal WorkorderErpModal) 운전일보 그리드
	@action.bound
	setOutputGridApi = async (gridApi) => {
		this.outputGridApi = gridApi;
		const columnDefs = [
			{
				headerName: "직"
				, field: "prodShiftNm"
				, width: 50
				, cellClass: 'cell_align_center'
			},
			{
				headerName: "팀"
				, field: "prodTeamNm"
				, width: 50
				, cellClass: 'cell_align_center'
			},
			{
				headerName: "시작시각"
				, field: "prodStartDt"
				, width: 90
				, cellClass: 'cell_align_center'
				, valueFormatter: GridUtil.dateFormatter
			},
			{
				headerName: "종료시각"
				, field: "prodEndDt"
				, width: 90
				, cellClass: 'cell_align_center'
				, valueFormatter: GridUtil.dateFormatter
			},
			{
				headerName: "지시번호"
				, field: "orderNo"
				, width: 90
				, cellClass: 'cell_align_center'
			},
			{
				headerName: "제품코드"
				, field: "itemCd"
				, width: 100
				, cellClass: 'cell_align_center'
			},
			{
				headerName: "제품명"
				, field: "itemNm"
				, width: 155
				, cellClass: 'cell_align_left'
			},
			{
				headerName: "단위"
				, field: "prodUom"
				, width: 50
				, cellClass: 'cell_align_center'
			},
			{
				headerName: "실적수량"
				, field: "prodQty"
				, width: 100
				, type: "numericColumn"
				, valueFormatter: GridUtil.numberFormatter
				, valueParser: GridUtil.numberParser
				, pinnedRowCellRenderer: "customPinnedRowRenderer"
				, pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
			}
		];
		this.outputGridApi.setColumnDefs(columnDefs);
	}

	//생산실적전송(Modal WorkorderErpModal) 생산실적 리스트
	@action.bound
	getMiddleWorkOrderErp = async (searchData) => {
		const { data, status } = await WorkorderErpRepository.getOutputErpGrp(searchData);
		if (data.success == false) {
			Alert.meg(data.msg);
		} else {
			this.workorderErpGrpList = data.data;
			this.workorderErpGrpGridApi.setRowData(this.workorderErpGrpList);
		}
	}

	//생산실적전송(Modal WorkorderErpModal) 생산실적 그리드
	@action.bound
	setWorkorderErpGrpGridApi = async (gridApi) => {
		this.workorderErpGrpGridApi = gridApi;
		const columnDefs = [
			// { headerName: "prodOutputId", field: "prodOutputId", width: 50, cellClass: 'cell_align_center' },
			{
				headerName: "", field: ""
				, width: 30
				, checkboxSelection: true
				, headerCheckboxSelection: true
				// , editable: function (params) { }
			},
			{
				headerName: "생산일"
				, field: "prodDt"
				, width: 90
				, cellClass: 'cell_align_center'
				, valueFormatter: GridUtil.dateFormatter
			},
			{
				headerName: "제품코드"
				, field: "itemCd"
				, width: 90
				, cellClass: 'cell_align_center'
			},
			{
				headerName: "제품명"
				, field: "itemNm"
				, width: 150
				, cellClass: 'cell_align_center'
			},
			{
				headerName: "전송상태"
				, field: "ifStatus"
				, width: 90
				, cellClass: 'cell_align_center'
			},
			{
				headerName: "단위"
				, field: "prodUom"
				, width: 50
				, cellClass: 'cell_align_center'
			},
			{
				headerName: "생산량"
				, field: "prodQty"
				, width: 90
				, cellClass: 'cell_align_right'
				, valueFormatter: GridUtil.numberFormatter
				, valueParser: GridUtil.numberParser
				, pinnedRowCellRenderer: "customPinnedRowRenderer"
				, pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
			},
			// {headerName: "재투입량 단위", field: "repackQtyUom", width: 90, cellClass: 'cell_align_center' },
			{
				headerName: "재투입량"
				, field: "repackQty"
				, width: 90
				, cellClass: 'cell_align_right'
				, editable: true
				, valueFormatter: GridUtil.numberFormatter
				, valueParser: GridUtil.numberParser
				, pinnedRowCellRenderer: "customPinnedRowRenderer"
				, pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
			},
			// {headerName: "실적량단위", field: "outputQtyUom", width: 90, cellClass: 'cell_align_center' },
			{
				headerName: "실적량"
				, field: "outputQty"
				, width: 90
				, cellClass: 'cell_align_right'
				, valueFormatter: GridUtil.numberFormatter
				, valueParser: GridUtil.numberParser
				, pinnedRowCellRenderer: "customPinnedRowRenderer"
				, pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
			},
			// {headerName: "전송량단위", field: "ifQtyUom", width: 90, cellClass: 'cell_align_center' },
			{
				headerName: "전송량"
				, field: "ifQty"
				, width: 90
				, cellClass: 'cell_align_right'
				, valueFormatter: GridUtil.numberFormatter
				, valueParser: GridUtil.numberParser
				, pinnedRowCellRenderer: "customPinnedRowRenderer"
				, pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
			},
			// {headerName: "미전송량단위", field: "notIfQtyUom", width: 90, cellClass: 'cell_align_center' },
			{
				headerName: "미전송량"
				, field: "notIfQty"
				, width: 90
				, cellClass: 'cell_align_right'
				, valueFormatter: GridUtil.numberFormatter
				, valueParser: GridUtil.numberParser
				, pinnedRowCellRenderer: "customPinnedRowRenderer"
				, pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
			}
			// {headerName: "", field: "prodQty", width: 100
			// 	, type: "numericColumn"
			// 	, valueFormatter: GridUtil.numberFormatter
			// 	, valueParser: GridUtil.numberParser
			// 	, pinnedRowCellRenderer: "customPinnedRowRenderer"
			// 	, pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
			// }
		];
		this.workorderErpGrpGridApi.setColumnDefs(columnDefs);
	}

	//생산실적전송(Modal WorkorderErpModal) 생산실적 투입량Data
	@action.bound
	getProcessQty = async (searchData) => {
		const { data, status } = await WorkorderErpRepository.getProcessQty(searchData);
		if (data.success == false) {
			Alert.meg(data.msg);
		} else {
			if (data.length > 0) {
				this.currentQtyStore = parseFloat(data.data.processQty - data.data.repackQty)
			} else {
				this.currentQtyStore = 0;
			}
			this.currentQty = this.currentQtyStore;
		}
	}

	//생산실적전송(Modal WorkorderErpModal) 생산실적 재투입량 변경 Event
	@action.bound
	adjustQty = (event) => {
		console.log('event', event);
		let accumValue = this.currentQtyStore;
		if (event.column.colId == "repackQty") {
			// console.log('Error 확인', this.currentQtyStore, accumValue, this.pinnedBottomRowData[0].repackQty)
			// if (accumValue - Number(this.pinnedBottomRowData[0].repackQty) < 0) {
			// 	event.node.setDataValue('repackQty', event.oldValue)
			// 	Alert.meg('투입량 이하로 입력할 수 없습니다.');
			// 	return;
			// }
			// else {
				event.node.setDataValue('outputQty', Number(event.data.prodQty) - Number(event.data.repackQty));
				this.ifQtyResult = Number(this.pinnedBottomRowData[0].prodQty) - Number(this.pinnedBottomRowData[0].repackQty);
				this.currentQty = accumValue - Number(this.pinnedBottomRowData[0].repackQty);
			// }
		}
		//console.log('pinned', this.pinnedBottomRowData[0].repackQty);
		//console.log('dasdasasdas',this.pinnedBottomRowData[0].outputQty);
	}


	//생산실적전송(Modal WorkorderErpModal) 생산실적 전송 Btn
	@action.bound
	groupQtyUpdate = async () => {
		const selectedRows = this.workorderErpGrpGridApi.getSelectedRows();
		if (!selectedRows || selectedRows.length == 0) {
			Alert.meg('전송할 항목을 체크 해주세요.');
			return;
		} else {
			for (let i = 0; i < selectedRows.length; i++) {
				switch (selectedRows[i].ifStatus) {
					case '취소중':
					case '전송중':
						Alert.meg('처리중 입니다.')
						return;
					case '전송완료':
						Alert.meg('이미 전송완료된 품목이 있습니다.')
						return;
					case '전송실패', '취소완료':
					default:
						continue;
				}
			}
			// selectedRows.forEach(selectedRow => {});

			// null, 전송실패, 취소완료시 전송 가능
			const actionType = { "actionType": 'CONFIRM' }
			const params = JSON.stringify({ param1: selectedRows, param2: actionType });
			const value = await Alert.confirm("전송하시겠습니까?").then(result => { return result; });
			if (value) {
				const { data, status } = await WorkorderErpRepository.setErpWoProdOutputInsert(params);
				if (data.success == false) {
					Alert.meg(data.msg);
				} else {
					this.handleDoubleClick();
				}
			} else {
				return;
			}
		}
	}

	//생산실적전송(Modal WorkorderErpModal) 생산실적 전송취소 Btn
	@action.bound
	groupQtyCancel = async () => {
		const selectedRows = this.workorderErpGrpGridApi.getSelectedRows();
		if (!selectedRows || selectedRows.length == 0) {
			Alert.meg('취소할 항목을 체크 해주세요.');
			return;
		} else {
			for (let i = 0; i < selectedRows.length; i++) {
				if (!selectedRows[i].ifStatus) {
					return;
				}
				switch (selectedRows[i].ifStatus) {
					case '취소중':
					case '전송중':
						Alert.meg('처리중 입니다.')
						return;
					case '전송실패':
						Alert.meg('취소 할 수 없습니다.')
						return;
					case '취소실패', '전송완료':
					default:
						continue;
				}
			}

			// 취소실패, 전송완료시 전송취소 가능
			const actionType = { "actionType": 'CANCEL' }
			const params = JSON.stringify({ param1: selectedRows, param2: actionType });
			const value = await Alert.confirm("전송취소하시겠습니까?").then(result => { return result; });
			if (value) {
				const { data, status } = await WorkorderErpRepository.setErpWoProdOutputInsert(params);
				if (data.success == false) {
					Alert.meg(data.msg);
				} else {
					this.handleDoubleClick();
				}
			} else {
				return;
			}
		}
	}


	// //생산실적전송(Modal WorkorderErpModal) 원료사용 리스트
	// @action.bound
	// setUseOfRawMaterial = async (searchData) => {
	// 	const { data, status } = await WorkorderErpRepository.getUseOfRawMaterial(searchData);
	// 	if (data.success == false) {
	// 		Alert.meg(data.msg);
	// 	} else {
	// 		this.useOfRawMaterialList = data.data;
	// 		this.useOfRawMaterialGridApi.setRowData(this.useOfRawMaterialList);
	// 	}
	// }

	//생산실적전송(Modal WorkorderErpModal) 원료사용 그리드
	// @action.bound
	// setUseOfRawMaterialGridApi = async (gridApi) => {
	// 	console.log('setUseOfRawMaterialGridApi');
	// 	this.useOfRawMaterialGridApi = gridApi;
	// 	const columnDefs = [
	// 		{
	// 			headerName: "사용일", field: "issueDay", width: 90, cellClass: "cell_align_center"
	// 			, editable: false
	// 			, cellRenderer: "rowSpanCellRender"
	// 			, valueFormatter: GridUtil.dateFormatter
	// 			, rowSpan: function (params) {
	// 				return GridUtil.rowSpanning(params);
	// 			}
	// 		},
	// 		{
	// 			headerName: "실제기준 ITEM(품목/명)",
	// 			children: [
	// 				{ headerName: "품목코드", field: "toItemCd", width: 80, cellClass: "cell_align_center", editable: false },
	// 				{ headerName: "품목명", field: "toItemNm", width: 120, cellClass: "cell_align_left", editable: false },
	// 			]
	// 		},
	// 		{ headerName: "제품구분", field: "glClassNm", width: 70, cellClass: "cell_align_center", editable: false },
	// 		{
	// 			headerName: "", field: ""
	// 			, width: 30
	// 			, checkboxSelection: true
	// 			, headerCheckboxSelection: true
	// 			// , editable: function (params) { }
	// 		},
	// 		{
	// 			headerName: "",
	// 			children: [
	// 				{ headerName: "전송상태", field: "ifStatus", width: 70, cellClass: "cell_align_center", editable: false },
	// 				{ headerName: "단위", field: "issueUom", width: 50, cellClass: "cell_align_center", editable: false },
	// 				{
	// 					headerName: "사용량", field: "issueQty", width: 70, cellClass: "cell_align_right", editable: false
	// 					, valueFormatter: (params) => GridUtil.numberFormatter(params)
	// 				},
	// 				{
	// 					headerName: "조정량", field: "changeQty", width: 70, cellClass: "cell_align_right", editable: false
	// 					, valueFormatter: (params) => GridUtil.numberFormatter(params)
	// 				},
	// 				{
	// 					headerName: "확정량", field: "confirmQty", width: 70, cellClass: "cell_align_right", editable: true
	// 					, valueFormatter: (params) => GridUtil.numberFormatter(params)
	// 				},
	// 				{ headerName: "LOCATION", field: "erpLocId", width: 70, cellClass: "cell_align_center", editable: true },
	// 				{
	// 					headerName: "전송량", field: "ifQty", width: 70, cellClass: "cell_align_right", editable: false
	// 					, valueFormatter: (params) => GridUtil.numberFormatter(params)
	// 				},
	// 				{
	// 					headerName: "미전송량", field: "notIfQty", width: 70, cellClass: "cell_align_right", editable: false
	// 					, valueFormatter: (params) => GridUtil.numberFormatter(params)
	// 				},
	// 			]
	// 		},
	// 		{
	// 			headerName: "BOM기준 ITEM(품목/명)",
	// 			children: [
	// 				{ headerName: "품목코드", field: "fromItemCd", width: 80, cellClass: "cell_align_center", editable: false },
	// 				{ headerName: "품목명", field: "fromItemNm", width: 120, cellClass: "cell_align_left", editable: false },
	// 			]
	// 		},
	// 		{
	// 			headerName: "",
	// 			children: [
	// 				{ headerName: "순번", field: "lineNo", width: 70, cellClass: "cell_align_center", editable: false },
	// 				{ headerName: "Op_Seq", field: "operationSeq", width: 70, cellClass: "cell_align_center", editable: false },
	// 			]
	// 		}
	// 	];

	// 	const col = GridUtil.findColumnDef(columnDefs, 'erpLocId');
	// 	console.log('col', col, this.erpLocIdList)
	// 	GridUtil.setSelectCellByList(col, this.erpLocIdList, "erpLocId", "erpLocId");

	// 	this.useOfRawMaterialGridApi.setColumnDefs(columnDefs);
	// }

	//생산실적전송(Modal WorkorderErpModal) 원료사용 확정량 변경 Event
	@action.bound
	adjustChangeQty = (event) => {
		if (event.column.colId == "confirmQty") {
			if (Number(event.data.confirmQty) > Number(event.data.issueQty)) {
				event.node.setDataValue('confirmQty', event.oldValue)
				Alert.meg('사용량을 초과할 수 없습니다.');
				return;
			}
			event.node.setDataValue('changeQty', Number(event.data.confirmQty) - Number(event.data.issueQty));
			this.urmChangeQty = Number(event.data.confirmQty) - Number(event.data.issueQty);
			console.log('확정량confQty-사용량issueQty = 조정량urmChangeQty', this.urmChangeQty);
		}
	}

	//생산실적전송(Modal WorkorderErpModal) 원료사용 LOCATION Dropdown
	@action.bound
	getErpLocIdDropDownList = async (searchData) => {
		const { data, status } = await WorkorderErpRepository.getErpLocIdDropdownList(searchData);
		if (data.success == false) {
			Alert.meg(data.msg);
		} else {
			this.erpLocIdList = data.data; // cd:IDX, cdNm: erpLocId
			console.log('this.erpLocIdList', this.erpLocIdList)
		}
	}

	//생산실적전송(Modal WorkorderErpModal) 원료사용 전송 Btn
	@action.bound
	useOfRawMaterialInsert = async () => {
		const selectedRows = this.useOfRawMaterialGridApi.getSelectedRows();
		console.log('selectedRows', selectedRows)
		if (!selectedRows || selectedRows.length == 0) {
			Alert.meg('전송할 항목을 체크 해주세요.');
			return;
		} else {
			for (let i = 0; i < selectedRows.length; i++) {
				if (selectedRows[i].erpLocId == null) {
					Alert.meg('LOCATION 정보를 선택 해주세요.');
					return;
				}
				switch (selectedRows[i].ifStatus) {
					case '취소중':
					case '전송중':
						Alert.meg('처리중 입니다.')
						return;
					case '전송완료':
						Alert.meg('이미 전송완료된 품목이 있습니다.')
						return;
					case '전송실패', '취소완료':
					default:
						continue;
				}
			}

			// null, 전송실패, 취소완료시 전송 가능		
			const actionType = { "actionType": 'CONFIRM' }
			const params = JSON.stringify({ param1: selectedRows, param2: actionType });
			const value = await Alert.confirm("전송하시겠습니까?").then(result => { return result; });
			if (value) {
				const { data, status } = await WorkorderErpRepository.setUseOfRawMaterialInsert(params);
				if (data.success == false) {
					Alert.meg(data.msg);
				} else {
					this.handleDoubleClick();
				}
			} else {
				return;
			}
		}
	}

	//생산실적전송(Modal WorkorderErpModal) 원료사용 전송취소 Btn
	@action.bound
	useOfRawMaterialCancel = async () => {
		const selectedRows = this.useOfRawMaterialGridApi.getSelectedRows();
		if (!selectedRows || selectedRows.length == 0) {
			Alert.meg('취소할 항목을 체크 해주세요.');
			return;
		} else {
			for (let i = 0; i < selectedRows.length; i++) {
				switch (selectedRows[i].ifStatus) {
					case '취소중':
					case '전송중':
						Alert.meg('처리중 입니다.')
						return;
					case null:
					case '전송실패':
						Alert.meg('취소 할 수 없습니다.')
						return;
					case '취소실패', '전송완료':
					default:
						continue;
				}
			}

			const actionType = { "actionType": 'CANCEL' }
			const params = JSON.stringify({ param1: selectedRows, param2: actionType });
			const value = await Alert.confirm("전송취소하시겠습니까?").then(result => { return result; });
			if (value) {
				const { data, status } = await WorkorderErpRepository.setUseOfRawMaterialInsert(params);
				if (data.success == false) {
					Alert.meg(data.msg);
				} else {
					this.handleDoubleClick();
				}
			} else {
				return;
			}
		}
	}


	//분할전송(Modal_SubModal WorkorderErpDivideModal) 분할전송 Btn
	@action.bound
	openWorkorderErpDivideModal = async () => {
		const selectedRowData = GridUtil.getSelectedRowData(this.useOfRawMaterialGridApi);
		if (!selectedRowData) {
			Alert.meg('분할전송 항목을 선택 해주세요.');
			return;
		} else if (selectedRowData.ifStatus != null)  {
			Alert.meg('분할전송 할 수 없는 항목입니다.');
			return;
		} else {
			this.lineNo = selectedRowData.lineNo //순번
			this.issueDay = selectedRowData.issueDay // 사용일자
			this.fromItemCd = selectedRowData.fromItemCd // 품목코드
			this.fromItemNm = selectedRowData.fromItemNm // 품목명
			this.issueQty = selectedRowData.issueQty // 사용량
			this.issueUom = selectedRowData.issueUom // 사용량 단위
			this.changeQty = selectedRowData.changeQty // 분할량
			this.workorderErpDivideModalIsOpen = true;
		}
	}

	//분할전송(Modal_SubModal WorkorderErpDivideModal) 분할전송 그리드
	@action.bound
	setWorkorderErpPartDivideGridApi = async (gridApi) => {
		this.workorderErpPartDivideGridApi = gridApi;
		const columnDefs = [
			{
				headerName: "", field: ""
				, width: 30
				, checkboxSelection: true
				, headerCheckboxSelection: true
				// , editable: function (params) { }
			},
			{ headerName: "품목", field: "toItemCd", width: 100, cellClass: "cell_align_center", editable: false },
			{ headerName: "품목명", field: "toItemNm", width: 150, cellClass: "cell_align_left", editable: false },
			{ headerName: "위치", field: "erpLocId", width: 70, cellClass: "cell_align_center" },
			// { headerName: "단위", field: "issueUom", width: 70, cellClass: "cell_align_center" },
			{ headerName: "사용량", field: "issueQty", width: 70, cellClass: "cell_align_right"
			, valueFormatter: GridUtil.numberFormatter },
		];

		const col = GridUtil.findColumnDef(columnDefs, 'erpLocId');
		GridUtil.setSelectCellByList(col, this.erpLocIdList, "erpLocId", "erpLocId");

		this.workorderErpPartDivideGridApi.setColumnDefs(columnDefs);
	}

	//분할전송(Modal_SubModal WorkorderErpDivideModal) ItemHelper 검색결과 Row 추가
	@action.bound
	handleHelperResult = (result) => {
		if (result) {
			console.log(result)
			let count = this.workorderErpPartDivideGridApi.getDisplayedRowCount();
			const codeHdmt = {
				toItemId: result[0].itemId,
				toItemCd: result[0].itemCd,
				toItemNm: result[0].itemNm,
				erpLocId: '',
				issueUom: this.issueUom,
				issueQty: 0,
			};
			const addResult = this.workorderErpPartDivideGridApi.updateRowData({ add: [codeHdmt], addIndex: count });
			GridUtil.setFocus(this.workorderErpPartDivideGridApi, addResult.add[0]);
		}
	}

	//분할전송(Model_SubModal WorkorderErpDivideModal) 분할전송 '전송' Btn
	@action.bound
	erpPartDivideInsert = async () => {
		const selectedRows = this.workorderErpPartDivideGridApi.getSelectedRows();
		if (!selectedRows || selectedRows.length == 0) {
			Alert.meg('전송할 목록을 선택 해주세요.');
			return;
		} else {
			for (let i = 0; i < selectedRows.length; i++) {
				if (selectedRows[i].erpLocId == null || selectedRows[i].issueQty == null || selectedRows[i].issueQty == 0) {
					Alert.meg('위치/사용량을 입력 해주세요.');
					return;
				}
			}

			const selectedRowData = GridUtil.getSelectedRowData(this.useOfRawMaterialGridApi);
			selectedRowData.actionType = 'CONFIRM';
			const value = await Alert.confirm("전송하시겠습니까?").then(result => { return result; });
			if (value) {
				const params = JSON.stringify({ param1: selectedRows, param2: selectedRowData });
				const { data, status } = await WorkorderErpRepository.setErpPartDivideInsert(params);
				if (data.success == false) {
					Alert.meg(data.msg);
				} else {
					this.workorderErpDivideCloseModal();
					this.handleDoubleClick();
				}
			} else {
				return;
			}
		}
	}

	//분할전송(Model_SubModal WorkorderErpDivideModal) 그리드 Row '추가' Btn
	@action.bound
	erpPartDivideAdd = () => {
		this.isItemOpen = true;
	}

    //분할전송(Model_SubModal WorkorderErpDivideModal) 그리드 Row '삭제' Btn
    @action.bound
    erpPartDivideCancel = () => {
        const selectedRows = this.workorderErpPartDivideGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg('삭제할 항목을 선택해 주세요.');
            return;
        }
        selectedRows.forEach(selectedRow => {
            this.workorderErpPartDivideGridApi.updateRowData({ remove: [selectedRow] });
        })
    }

	//분할전송(Modal_SubModal WorkorderErpDivideModal) 닫기 Btn
	@action.bound
	workorderErpDivideCloseModal = () => {
		this.selectedItem = {};
		this.workorderErpDivideModalIsOpen = false;
	}

	//생산실적전송(Modal WorkorderErpModal) Tab 전환 Event
	@action.bound
	handleErpTabChange = async (data) => {
		console.log('handleChange', data)
		this[data.id] = data.value;

		if (data.id == 'activeIndex') {
			this.handleDoubleClick();
		}
	}

	//생산실적전송(Modal WorkorderErpModal) ERP WORK ORDER 리스트
	@action.bound
	getWorkorderErpFirst = async (searchData) => {
		const { data, status } = await WorkorderErpRepository.getWorkorderErpFirst(searchData);
		if (data.success == false) {
			Alert.meg(data.msg);
		} else {
			this.erpFirstList = data.data;
			this.workorderErpFirstGridApi.setRowData(this.erpFirstList);
		}
	}

	//생산실적전송(Modal WorkorderErpModal) ERP WORK ORDER 그리드
	@action.bound
	setWorkorderErpFirstGridApi = async (gridApi) => {
		this.workorderErpFirstGridApi = gridApi;
		const columnDefs = [
			// { headerName: "CO", field: "waco", width: 100, cellClass:"cell_align_center" },
			// { headerName: "BU", field: "wamcu", width: 100, cellClass:"cell_align_center" },
			{ headerName: "Order Number", field: "wadoco", width: 100, cellClass: "cell_align_center" },
			{ headerName: "Status", field: "wasrst", width: 70, cellClass: "cell_align_center" },
			{ headerName: "WC Code", field: "wlmcu", width: 100, cellClass: "cell_align_center" },
			{ headerName: "WC Name", field: "wldsc1", width: 150, cellClass: "cell_align_left" },
			{ headerName: "Item Code", field: "walitm", width: 100, cellClass: "cell_align_center" },
			{ headerName: "Item Name", field: "wadl01", width: 150, cellClass: "cell_align_left" },
			{ headerName: "UOM", field: "wauom", width: 70, cellClass: "cell_align_center" },
			{
				headerName: "Order Qty", field: "wauorg", width: 100, cellClass: "cell_align_right"
				, valueFormatter: GridUtil.numberFormatter
			},
			{
				headerName: "Completed Qty", field: "wasoqs", width: 100, cellClass: "cell_align_right"
				, valueFormatter: GridUtil.numberFormatter
			},
			{ headerName: "From Date", field: "wastrt", width: 100, cellClass: "cell_align_center" },
			{ headerName: "To Date", field: "wadrqj", width: 100, cellClass: "cell_align_center" }
		];
		this.workorderErpFirstGridApi.setColumnDefs(columnDefs);
	}

	//생산실적전송(Modal WorkorderErpModal) ERP PART LIST 리스트
	@action.bound
	getWorkorderErpSecond = async (searchData) => {
		const { data, status } = await WorkorderErpRepository.getWorkorderErpSecond(searchData);
		if (data.success == false) {
			Alert.meg(data.msg);
		} else {
			this.erpSecondList = data.data;
			this.workorderErpSecondGridApi.setRowData(this.erpSecondList);
		}
	}

	//생산실적전송(Modal WorkorderErpModal) ERP PART LIST 그리드
	@action.bound
	setWorkorderErpSecondGridApi = async (gridApi) => {
		this.workorderErpSecondGridApi = gridApi;
		const columnDefs = [
			{ headerName: "Line No", field: "wmcpnb", width: 70, cellClass: "cell_align_center" },
			{ headerName: "CO/BY", field: "wmcoby", width: 70, cellClass: "cell_align_center" },
			{ headerName: "Item Code", field: "wmcpil", width: 100, cellClass: "cell_align_center" },
			{ headerName: "Item Name", field: "wmdl01", width: 150, cellClass: "cell_align_left" },
			{ headerName: "OP Seq", field: "wmopsq", width: 70, cellClass: "cell_align_center" },
			{
				headerName: "Order Qty", field: "wmuorg", width: 100, cellClass: "cell_align_right"
				, valueFormatter: GridUtil.numberFormatter
			},
			{
				headerName: "Issued Qty", field: "wmtrqt", width: 100, cellClass: "cell_align_right"
				, valueFormatter: GridUtil.numberFormatter
			},
			{ headerName: "UOM", field: "wmum", width: 70, cellClass: "cell_align_center" },
			{ headerName: "Location", field: "wmlocn", width: 70, cellClass: "cell_align_center" },
			{ headerName: "UKID", field: "wmukid", width: 100, cellClass: "cell_align_center" },
		];
		this.workorderErpSecondGridApi.setColumnDefs(columnDefs);
	}

	//생산실적전송(Modal WorkorderErpModal) ERP ITEM LEDGER 리스트
	@action.bound
	getWorkorderErpThird = async (searchData) => {
		const { data, status } = await WorkorderErpRepository.getWorkorderErpThird(searchData);
		if (data.success == false) {
			Alert.meg(data.msg);
		} else {
			this.erpThirdList = data.data;
			this.workorderErpThirdGridApi.setRowData(this.erpThirdList);
		}
	}

	@action.bound
    bulkGridIsRowSelectable = (rowNode) => {

        if (!rowNode.data) {
            return;
        }

        if (rowNode.data.ifStatus == "I") {
            console.log(rowNode.data)
            return false;
        } else {
            console.log(rowNode.data, '2')
            return true;
        }
    }
	
	@action.bound
	bulkModalColseModal = () => {
		this.bulkModalIsOpen = false
	}

	@action.bound
	sendErpBulkProd = async () => {

		const sendList = this.bulkListGridApi.getSelectedRows()

        if (sendList.length == 0) {
            Alert.meg("선택된 항목이 없습니다.")
            return
		}
		
	   const { data, status } = await WorkorderErpRepository.upsertBulkList(sendList);

	   if(status == 200)
	   {
		   Alert.meg("저장 되었습니다.")
		   this.onClickBulkSearch();
	   }
	}

	@action.bound
	onClickBulkSearch = async () => {
		const searchData = {
			'itemCd' :  this.bulkItemCd
		   , 'buId'  :  this.buId
		   ,'searchStartDtm' : this.searchStartDtm
		   ,'searchEndDtm' : this.searchEndDtm
		   ,'dateType' : this.dateType	
	   }

	   console.log(searchData, 'searchData');
	   const { data, status } = await WorkorderErpRepository.getBulkList(searchData);

	   if(status == 200) {
		   this.bulkList = data.data;
		   this.bulkListGridApi.setRowData(this.bulkList);
	   }
	}

	@action.bound
	clickOpenBulkModal = () => {
		this.bulkModalIsOpen = true
	}

	//생산실적전송(Modal WorkorderErpModal) ERP ITEM LEDGER 그리드
	@action.bound
	setWorkorderErpThirdGridApi = async (gridApi) => {
		this.workorderErpThirdGridApi = gridApi;
		const columnDefs = [
			{ headerName: "Order Number", field: "ildoco", width: 100, cellClass: "cell_align_center" },
			{ headerName: "Order Type", field: "ildcto", width: 70, cellClass: "cell_align_center" },
			{ headerName: "Doc Type", field: "ildct", width: 70, cellClass: "cell_align_center" },
			{ headerName: "Item Code", field: "illitm", width: 100, cellClass: "cell_align_center" },
			{ headerName: "Item Name", field: "imdsc1", width: 150, cellClass: "cell_align_left" },
			{ headerName: "Tranj Date", field: "iltrdj", width: 100, cellClass: "cell_align_center" },
			{
				headerName: "Tranj Qty", field: "iltrqt", width: 100, cellClass: "cell_align_right"
				, valueFormatter: GridUtil.numberFormatter
			},
			{ headerName: "UOM", field: "iltrum", width: 70, cellClass: "cell_align_center" },
			{ headerName: "Location", field: "illocn", width: 70, cellClass: "cell_align_center" },
			{ headerName: "UKID", field: "ilukid", width: 100, cellClass: "cell_align_center" }
		];
		this.workorderErpThirdGridApi.setColumnDefs(columnDefs);
	}


	// 벌크 
	@action.bound
	setBulkListGridApi = async (gridApi) => {
		this.bulkListGridApi = gridApi;
		const columnDefs = [
			{
                headerName: "", field: ""
                , width: 30
                , checkboxSelection: true
                , editable: false
                , headerCheckboxSelection: true
            },
			{ headerName: "문서번호", field: "tmdlvh", width: 100, cellClass: "cell_align_center" },
			{ headerName: "제품코드", field: "itemCd", width: 80},
			{ headerName: "수량", field: "processQty", width: 80},
			{ headerName: "단위", field: "uom", width: 60, cellClass: "cell_align_center" },
			{ headerName: "출고일", field: "tmodat", width: 100, cellClass: "cell_align_center" },
			{ headerName: "출고확정일", field: "cfmdat", width: 100, cellClass: "cell_align_center" },
			{ headerName: "itemId", field: "itemId", hide : true},
		];
		this.bulkListGridApi.setColumnDefs(columnDefs);
	}

	//생산실적전송(Modal WorkorderErpModal) 닫기 Btn
	@action.bound
	workorderErpCloseModal = () => {
		//Modal Close시, Grid의 객체 초기화를 위해 선언
		this.workorderErpGrpList = [];
		this.workorderErpGrpList = [];
		this.useOfRawMaterialList= [];
		this.currentQty = 0;
		this.erpFirstList = [];
		this.erpSecondList = [];
		this.erpThirdList = [];
		this.workorderErpModalIsOpen = false;
	}

}
export default new WorkorderErpStore();