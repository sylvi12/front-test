import API from "components/override/api/API";

class WorkorderErpRepository {

	URL = "/api/order/workordererp";

	//ERP실적전송(WorkorderErpSearchItem) '조회'Btn
	getWoList(param) {
		return API.request.post(`${this.URL}/getWoList`, param);
	}

	//ERP실적전송(WorkorderErpMiddleItem) '전송' Btn
	setErpWoInsert(params) {
		return API.request.post(`${this.URL}/seterpwoinsert`, params)
	}

	//생산집계 리스트 조회("/api/order/output/getoutput" 참조)
	//기존 '운전일보' URL과 동일
	getOutput(param) {
		return API.request.post(encodeURI(`${this.URL}/getoutput`), param)
	}

	//생산실적전송(Modal WorkorderErpModal) 생산실적 리스트
	getOutputErpGrp(param) {
		return API.request.post(encodeURI(`${this.URL}/getoutputerpgrp`), param)
	}

	//생산실적전송(Modal WorkorderErpModal) 생산실적 투입량Data
	getProcessQty(param) {
		return API.request.post(`${this.URL}/getprocessqty`, param)
	}

	//생산실적전송(Modal WorkorderErpModal) 생산실적 전송,전송취소 Btn
	setErpWoProdOutputInsert(params) {
		return API.request.post(`${this.URL}/seterpwoprodoutputinsert`, params)
	}

	//생산실적전송(Modal WorkorderErpModal) 원료사용 리스트
	getUseOfRawMaterial(param) {
		return API.request.post(`${this.URL}/getuseofrawmaterial`, param);
	}

	//생산실적전송(Modal WorkorderErpModal) 원료사용 LOCATION Dropdown
	getErpLocIdDropdownList(param) {
		return API.request.get(encodeURI(`${this.URL}/geterplociddropdownlist?buId=${param.buId}`))
	}

	//생산실적전송(Modal WorkorderErpModal) 원료사용 전송,전송취소 Btn
	setUseOfRawMaterialInsert(params) {
		return API.request.post(`${this.URL}/setuseofrawmaterialinsert`, params)
	}

	//분할전송(Modal WorkorderErpDivideModal) 분할 전송 Btn
	setErpPartDivideInsert(params) {
		return API.request.post(`${this.URL}/seterppartdivideinsert`, params)
	}

	//생산실적전송(Modal WorkorderErpModal) ERP WORK ORDER 리스트
	getWorkorderErpFirst(param) {
		return API.request.get(encodeURI(`/api/erp/getworkordererpfirst?buId=${param.buId}&erpOrderNo_i=${param.erpOrderNo_i}`))
	}

	//생산실적전송(Modal WorkorderErpModal) ERP PART LIST 리스트
	getWorkorderErpSecond(param) {
		return API.request.get(encodeURI(`/api/erp/getworkordererpsecond?buId=${param.buId}&erpOrderNo_i=${param.erpOrderNo_i}`))
	}

	//생산실적전송(Modal WorkorderErpModal) ERP ITEM LEDGER 리스트
	getWorkorderErpThird(param) {
		return API.request.get(encodeURI(`/api/erp/getworkordererpthird?buId=${param.buId}&erpOrderNo_i=${param.erpOrderNo_i}`))
	}

	// 벌크 조회
	getBulkList(param) {
		return API.request.get(encodeURI(`/api/bulk/getslsbulklist?buId=${param.buId}&searchStartDtm=${param.searchStartDtm}&searchEndDtm=${param.searchEndDtm}&itemCd=${param.itemCd}&dateType=${param.dateType}`))
	}

	// 벌크 생산실적 등록
	upsertBulkList(params) {
		return API.request.post(`/api/bulk/upsertbulklist`, params)
	}
	
}

export default new WorkorderErpRepository();
