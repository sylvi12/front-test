import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid';

@inject(stores => ({
    setWorkOrderGridApi: stores.workorderErpStore.setWorkOrderGridApi
    , workOrderErpList: stores.workorderErpStore.workOrderErpList
    , handleDoubleClick: stores.workorderErpStore.handleDoubleClick
    , pinnedBottomRowData: stores.workorderErpStore.pinnedBottomRowData
}))

class WorkorderErpContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {
            setWorkOrderGridApi
            , workOrderErpList
            , handleDoubleClick
            , pinnedBottomRowData
        } = this.props;

        return (
            <React.Fragment>
                <SGrid
                    grid={'workorderErpGrid'}
                    gridApiCallBack={setWorkOrderGridApi}
                    rowData={workOrderErpList}
                    rowDoubleClick={handleDoubleClick}
                    suppressRowClickSelection={true}
                    cellReadOnlyColor={true}
                    editable={false}
                    pinnedBottomRowData={pinnedBottomRowData}
                />
            </React.Fragment>
        )
    }
}
export default WorkorderErpContents;