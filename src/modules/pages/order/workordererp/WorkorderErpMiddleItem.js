import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    erpForwarding: stores.workorderErpStore.erpForwarding
    , excelPrint: stores.workorderErpStore.excelPrint
    , clickOpenBulkModal: stores.workorderErpStore.clickOpenBulkModal
}))

class WorkorderErpMiddleItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {
            erpForwarding
            , excelPrint
            , clickOpenBulkModal
        } = this.props;

        return (
            <React.Fragment>
                <SButton
                    className="btn_bulk"
                    buttonName={'벌크전송'}
                    type={'btnErpBulkInsert'}
                    onClick={clickOpenBulkModal}
                />
                <SButton
                    className="btn_red"
                    buttonName={'전송'}
                    type={'btnErpInsert'}
                    onClick={erpForwarding}
                />
                <SButton
                    className="btn_red"
                    buttonName={'Excel'}
                    type={'btnExcuteFile'}
                    onClick={excelPrint}
                />
            </React.Fragment>
        )
    }
}

export default WorkorderErpMiddleItem;