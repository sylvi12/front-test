import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setBulkListGridApi : stores.workorderErpStore.setBulkListGridApi
    , bulkList : stores.workorderErpStore.bulkList
    , bulkGridIsRowSelectable : stores.workorderErpStore.bulkGridIsRowSelectable
}))

class WorkorderBulkModalContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { setBulkListGridApi, bulkList, bulkGridIsRowSelectable } = this.props;

        return (
            <SGrid
                grid={'outputModalGrid'}
                gridApiCallBack={setBulkListGridApi}
                rowData={bulkList}
                editable={false}                
                suppressRowClickSelection={true}
                isRowSelectable={bulkGridIsRowSelectable}
                cellReadOnlyColor={false}
            />
        );
    }
}
export default WorkorderBulkModalContents;