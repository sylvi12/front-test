import React, { Component } from 'react';
import { inject } from 'mobx-react';



import SearchTemplate from "components/template/SearchTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";


import WorkorderBulkModalContents from "modules/pages/order/workordererp/workorderBulkModal/WorkorderBulkModalContents";
import WorkorderBulkModalSearchItem from "modules/pages/order/workordererp/workorderBulkModal/WorkorderBulkModalSearchItem";
import WorkorderBulkModalMiddleItem from "modules/pages/order/workordererp/workorderBulkModal/WorkorderBulkModalMiddleItem";


@inject(stores => ({
}))

class Output extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <React.Fragment>
                <SearchTemplate searchItem={<WorkorderBulkModalSearchItem />} />
                <ContentsMiddleTemplate contentSubTitle={'벌크실적추가'} middleItem={<WorkorderBulkModalMiddleItem />} />
                <ContentsTemplate id='bulkModal' contentItem={<WorkorderBulkModalContents />} />
            </React.Fragment>
        )
    }
}

export default Output;