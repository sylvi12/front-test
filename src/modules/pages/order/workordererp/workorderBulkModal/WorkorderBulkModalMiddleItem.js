import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    sendErpBulkProd: stores.workorderErpStore.sendErpBulkProd
}))

class WorkorderBulkModalMiddleItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {
            sendErpBulkProd
        } = this.props;

        return (
            <React.Fragment>
                <SButton
                    buttonName={'저장'}
                    type={'btnSearch'}
                    onClick={sendErpBulkProd} 
                />
            </React.Fragment>
        )
    }
}

export default WorkorderBulkModalMiddleItem;