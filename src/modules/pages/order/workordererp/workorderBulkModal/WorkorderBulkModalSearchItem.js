import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton';
import SInput from 'components/atoms/input/SInput';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SRadio from 'components/atoms/radio/SRadio';
import SBuSelectBox from 'components/override/business/SBuSelectBox';

@inject(stores => ({
    bulkItemCd : stores.workorderErpStore.bulkItemCd
    ,searchStartDtm : stores.workorderErpStore.searchStartDtm
    ,searchEndDtm : stores.workorderErpStore.searchEndDtm
    ,onClickBulkSearch : stores.workorderErpStore.onClickBulkSearch
    ,dateType : stores.workorderErpStore.dateType
    ,dateTypeList : stores.workorderErpStore.dateTypeList
    ,handleChange : stores.workorderErpStore.handleChange
}))

class WorkorderErpSearchItem extends Component {

    constructor(props) {
        super(props);
    }
    
    render() {
        const {
            searchStartDtm
            ,searchEndDtm
            ,bulkItemCd
            ,handleChange
            ,onClickBulkSearch
            ,dateType
            ,dateTypeList
        } = this.props;
        return (
            <React.Fragment>
                <div className='search_item_btn'>
                    <SButton
                        buttonName={'조회'}
                        type={'btnSearch'}
                        onClick={onClickBulkSearch} 
                    />
                </div>
                
                <div className='search_line'>
                    <SRadio title={"기준"}
                        id={"dateType"}
                        value={dateType}
                        optionGroup={dateTypeList}
                        onChange={handleChange}
                     />
                    <SDatePicker
                        title={''}
                        id={"searchStartDtm"}
                        value={searchStartDtm}
                        onChange={handleChange}
                        dateFormat={"yyyy-MM-dd"}
                    />
                    <SDatePicker
                        title={'~'}
                        id={"searchEndDtm"}
                        value={searchEndDtm}
                        onChange={handleChange}
                        dateFormat={"yyyy-MM-dd"}
                    />
                    <SInput
                        title={'제품코드'}
                        id={'bulkItemCd'}
                        value={bulkItemCd}
                        onChange={handleChange}
                        onEnterKeyDown={onClickBulkSearch}
                    />
                </div>
            </React.Fragment>
        )
    }
}

export default WorkorderErpSearchItem;