import React, { Component } from "react";
import { inject } from "mobx-react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";
import SModal from "components/override/modal/SModal";
import WorkorderErpModal from "modules/pages/order/workordererp/workordererpmodal/WorkorderErpModal"
import WorkorderBulkModal from "modules/pages/order/workordererp/workorderBulkModal/WorkorderBulkModal"


import WorkorderErpSearchItem from "modules/pages/order/workordererp/WorkorderErpSearchItem";
import WorkorderErpMiddleItem from "modules/pages/order/workordererp/WorkorderErpMiddleItem";
import WorkorderErpContents from "modules/pages/order/workordererp/WorkorderErpContents";
import SButton from "components/atoms/button/SButton";
import WorkorderErpStore from "modules/pages/order/workordererp/service/WorkorderErpStore";

@inject(stores => ({
    workorderErpModalIsOpen: stores.workorderErpStore.workorderErpModalIsOpen
    , workorderErpCloseModal: stores.workorderErpStore.workorderErpCloseModal
    , buIdCheck: stores.workorderErpStore.workorderErpCloseModal
    , bulkModalIsOpen : stores.workorderErpStore.bulkModalIsOpen
    , bulkModalColseModal : stores.workorderErpStore.bulkModalColseModal
}))
class WorkorderErp extends Component {

    constructor(props) {
        super(props);
    }

    async componentWillMount() {
        if (WorkorderErpStore.buIdCheck == false) {
            await WorkorderErpStore.getBuId();
        }
    }

    render() {

        const {
            workorderErpModalIsOpen
            , workorderErpCloseModal
            , bulkModalColseModal
            , bulkModalIsOpen
        } = this.props

        return (
            <React.Fragment>
                <div style={{ width: '100%', height: '100%', float: 'left' }}>
                    <SearchTemplate searchItem={<WorkorderErpSearchItem />} />
                    <ContentsMiddleTemplate contentSubTitle={'작업지시조회'} middleItem={<WorkorderErpMiddleItem />} />
                    <ContentsTemplate id='workordererp' contentItem={<WorkorderErpContents />} />
                </div>
                 <SModal
                    formId={'order_workorder_erp_inquiry_p'}
                    isOpen={workorderErpModalIsOpen}
                    onRequestClose={workorderErpCloseModal}
                    contentLabel="생산실적전송"
                    width={1400}
                    height={800}
                    contents={<WorkorderErpModal />}
                />

                <SModal
                    formId={'order_workorder_bulk_p'}
                    isOpen={bulkModalIsOpen}
                    onRequestClose={bulkModalColseModal}
                    contentLabel="벌크실적전송"
                    width={800}
                    height={500}
                    contents={<WorkorderBulkModal />}
                />
            </React.Fragment>
        )
    }
}

export default WorkorderErp;