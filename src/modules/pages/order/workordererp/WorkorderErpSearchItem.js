import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton';
import SInput from 'components/atoms/input/SInput';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SBuSelectBox from 'components/override/business/SBuSelectBox';

@inject(stores => ({
    buId: stores.workorderErpStore.buId
    ,startDtm: stores.workorderErpStore.startDtm
    ,endDtm: stores.workorderErpStore.endDtm
    ,lineId: stores.workorderErpStore.lineId
    ,lineGroup: stores.workorderErpStore.lineGroup
    ,ifStatus: stores.workorderErpStore.ifStatus
    ,woStatus: stores.workorderErpStore.woStatus
    ,orderNo: stores.workorderErpStore.orderNo
    ,itemCdNm: stores.workorderErpStore.itemCdNm
    ,lineSelectionChanged: stores.workorderErpStore.lineSelectionChanged
    ,handleChange: stores.workorderErpStore.handleChange
    ,handleSearchClick: stores.workorderErpStore.handleSearchClick
}))

class WorkorderErpSearchItem extends Component {

    constructor(props) {
        super(props);
    }
    
    render() {
        const forwardType = [
            {
                cd: 'S'
                , cdNm: '전송'
            }
            , {
                cd: 'N'
                , cdNm: '미전송'
            }
        ]
        const {
             buId
            ,startDtm
            ,endDtm
            ,lineId
            ,ifStatus
            ,woStatus
            ,lineSelectionChanged
            ,lineGroup
            ,orderNo
            ,itemCdNm
            ,handleChange
            ,handleSearchClick
        } = this.props;
        return (
            <React.Fragment>
                <div className='search_item_btn'>
                    <SButton
                        buttonName={'조회'}
                        type={'btnSearch'}
                        onClick={handleSearchClick} 
                    />
                </div>
                <div className='search_line'>
                    <SBuSelectBox
                        title={'공장'}
                        id={"buId"}
                        value={buId}
                        isAddAll={false}
                        onChange={handleChange}
                        //onSelectionChange={handleSearchClick}
                    />
                    <SDatePicker
                        title={'생산일'}
                        id={"startDtm"}
                        value={startDtm}
                        onChange={handleChange}
                        dateFormat={"yyyy-MM-dd"}
                    />
                    <SDatePicker
                        title={'~'}
                        id={"endDtm"}
                        value={endDtm}
                        onChange={handleChange}
                        dateFormat={"yyyy-MM-dd"}
                    />
                    <SSelectBox
                    title={"생산라인"}
                    id={"lineId"}
                    value={lineId}
                    pk={"lineId,buId"}
                    optionGroup={lineGroup}
                    valueMember={'lineId'}
                    displayMember={'lineNm'}
                    addOption={"전체"}
                    onChange={handleChange}
                    contentWidth={130}
                    />
                    <SSelectBox
                        title={'구분'}
                        id={"ifStatus"}
                        value={ifStatus}
                        optionGroup={forwardType}
                        valueMember={'cd'}
                        displayMember={'cdNm'}
                        onChange={handleChange}
                        addOption={"전체"}
                        //onSelectionChange={handleSearchClick}
                    />
                    <SSelectBox
                        title={'지시상태'}
                        id={"woStatus"}
                        value={woStatus}
                        codeGroup={"MFD004"}
                        onChange={handleChange}
                        //onSelectionChange={handleSearchClick}
                        addOption={"전체"}
                    />
                    <SInput
                        title={'지시번호'}
                        id={'orderNo'}
                        value={orderNo}
                        onChange={handleChange}
                        //onEnterKeyDown={handleSearchClick}
                    />
                    <SInput
                        title={'품목'}
                        id={'itemCdNm'}
                        value={itemCdNm}
                        onChange={handleChange}
                        onEnterKeyDown={handleSearchClick}
                    />
                </div>
            </React.Fragment>
        )
    }
}

export default WorkorderErpSearchItem;