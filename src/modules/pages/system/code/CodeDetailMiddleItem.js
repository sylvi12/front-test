import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton'

@inject(stores => ({
    handleDetailSaveClick: stores.codeStore.handleDetailSaveClick,
    handleDetailAddClick: stores.codeStore.handleDetailAddClick,
    handleDetailDeleteClick: stores.codeStore.handleDetailDeleteClick,
}))
class CodeDetailMiddleItem extends Component {

    render() {

        const { handleDetailSaveClick, handleDetailAddClick, handleDetailDeleteClick } = this.props;
        return (
            <React.Fragment>
                <SButton
                    buttonName={'저장'}
                    type={'btnDetailSave'}
                    onClick={handleDetailSaveClick}
                />
                <SButton
                    buttonName={'추가'}
                    type={'btnDetailAdd'}
                    onClick={handleDetailAddClick}
                />
                <SButton
                    buttonName={'삭제'}
                    type={'btnDetailDelete'}
                    onClick={handleDetailDeleteClick}
                />
            </React.Fragment>
        )
    }
}

export default CodeDetailMiddleItem;