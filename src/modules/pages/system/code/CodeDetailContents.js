import React, { Component } from 'react';
import { inject } from 'mobx-react';
import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setDetailGridApi: stores.codeStore.setDetailGridApi,
    detailList: stores.codeStore.detailList,
}))
class CodeDetailContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setDetailGridApi, detailList } = this.props;

        return (
            <div className="panel_body" style={{ height: '100%' }}>
                <SGrid
                    grid={'codeDetailGrid'}
                    gridApiCallBack={setDetailGridApi}
                    rowData={detailList}
                    cellReadOnlyColor={true}
                />
            </div>
        );
    }
}
export default CodeDetailContents;