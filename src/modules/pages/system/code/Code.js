import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import SearchTemplate from 'components/template/SearchTemplate'
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate'
import ContentsTemplate from 'components/template/ContentsTemplate';

import CodeSearchItem from 'modules/pages/system/code/CodeSearchItem';
import CodeMasterMiddleItem from 'modules/pages/system/code/CodeMasterMiddleItem';
import CodeMasterContents from 'modules/pages/system/code/CodeMasterContents';
import CodeDetailMiddleItem from 'modules/pages/system/code/CodeDetailMiddleItem';
import CodeDetailContents from 'modules/pages/system/code/CodeDetailContents';

@inject(stores => ({
    handleChange: stores.codeStore.handleChange,
    handleHelperResult: stores.codeStore.handleHelperResult,
    selectedItem: stores.codeStore.selectedItem,
}))
@observer
class Code extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        return (
            <React.Fragment>

                <div style={{ width: '100%', height: '50%', float: 'left' }}>
                    <SearchTemplate searchItem={<CodeSearchItem />} />
                    <ContentsMiddleTemplate contentSubTitle={'코드 그룹'} middleItem={<CodeMasterMiddleItem />} />
                    <ContentsTemplate id='codeMaster' contentItem={<CodeMasterContents />} />
                </div>

                <div style={{ width: '100%', height: '50%', float: 'left' }}>
                    <ContentsMiddleTemplate contentSubTitle={'코드 종류'} middleItem={<CodeDetailMiddleItem />} />
                    <ContentsTemplate id='codeDetail' contentItem={<CodeDetailContents />} />
                </div>

            </React.Fragment>
        )
    }
}

export default Code;