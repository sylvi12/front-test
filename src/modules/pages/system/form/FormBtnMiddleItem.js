import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
      btnSaveClick: stores.formStore.btnSaveClick
    , btnAddClick: stores.formStore.btnAddClick
    , btnDeleteClick: stores.formStore.btnDeleteClick
}))

class FormBtnMiddleItem extends Component {
    
    render() {

        const { btnSaveClick, btnAddClick, btnDeleteClick } = this.props;
        return (
            <React.Fragment>
                <SButton
                    className="btn_red"
                    buttonName={'저장'}
                    type={'btnSave'}
                    onClick={btnSaveClick}
                />
                <SButton
                    buttonName={'추가'}
                    className="btn_red"
                    type={'btnAdd'}
                    onClick={btnAddClick}
                />
                <SButton
                    buttonName={'삭제'}
                    type={'btnDelete'}
                    onClick={btnDeleteClick}
                />
            </React.Fragment>
        )
    }
}

export default FormBtnMiddleItem;