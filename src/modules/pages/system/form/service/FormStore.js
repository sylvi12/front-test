import { observable, action } from 'mobx';

import Alert from 'components/override/alert/Alert';
import GridUtil from 'components/override/grid/utils/GridUtil';

import FormRepository from 'modules/pages/system/form/service/FormRepository';

class FormStore {

    @observable formId_s = '';
    @observable formType_s = 'WEB'; //기본값: WEB

    formList = [];

    formGridApi = undefined;

    //버튼 변수
    btnList = [];
    btnGridApi = undefined;

    @action.bound
    handleChange = (data) => {
        console.log(data);
        this[data.id] = data.value;
    }

    @action.bound
    setGridApi = async (gridApi) => {

        this.formGridApi = gridApi;

        const columnDefs = [
            {
                headerName: "프로그램ID"
                , field: "formId"
                , width: 200
                , editable: function (params) {
                    if (params.node.data.newItem == 'Y') {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                , customCellClass: 'cell_align_center'
            },
            { headerName: "프로그램명", field: "formNm", width: 150 },
            { headerName: "프로그램 설명", field: "formDesc", width: 250 },
            { headerName: "프로그램 주소", field: "formUrl", width: 200 },
            { headerName: "프로그램 타입", field: "formType", width: 120, cellClass: 'cell_align_center' },
            { headerName: "프로그램 클래스", field: "formClass", width: 120, cellClass: 'cell_align_center' },
            { headerName: "등록자", field: "inputEmpNm", width: 100, editable: false, customCellClass: 'cell_align_center' },
            { headerName: "등록일", field: "inputDtm", width: 200, editable: false }
        ];

        this.formGridApi.setColumnDefs(columnDefs);
    }

    // 조회
    @action.bound
    handleSearchClick = async () => {

        const submitdata = {
            "formId": this.formId_s
            , "formNm": this.formId_s
            , "formType": this.formType_s
        }

        const { data, status } = await FormRepository.getForms(submitdata);

        if (status == 200) {
            this.formList = data.data;
            this.formGridApi.setRowData(this.formList);
        }
    }

    // 저장
    @action.bound
    handleSaveClick = async () => {

        const saveArr = GridUtil.getGridSaveArray(this.formGridApi);

        if (saveArr.length == 0) {
            Alert.meg('저장할 데이터가 없습니다.');
            return;
        }

        const searchData = {
            "formId": this.formId_s
            , "formNm": this.formId_s
            , "formType": this.formType_s
        }

        const param = JSON.stringify({ param1: saveArr, param2: searchData });

        const { data, status } = await FormRepository.saveForms(param);

        if (status == 200) {
            this.formList = data.data;
            this.formGridApi.setRowData(this.formList);

            Alert.meg("저장되었습니다.");
        }
    }

    // 추가
    @action.bound
    handleAddClick = () => {

        let lastindex = this.formGridApi.getLastDisplayedRow();

        const formHdmt = {
            formId: ''
            , formNm: ''
            , formDesc: ''
            , formUrl: ''
            , formType: ''
            , formClass: ''
            , newItem: 'Y'
        };
        const result = this.formGridApi.updateRowData({ add: [formHdmt], addIndex: lastindex + 1 });
        GridUtil.setFocus(this.formGridApi, result.add[0]);
    }

    // 삭제
    @action.bound
    handleDeleteClick = async () => {

        const selectedRows = this.formGridApi.getSelectedRows();

        if (selectedRows.length == 0) {
            Alert.meg('삭제할 항목을 선택해 주세요.');
            return;
        }

        const subData = {
            "formId": selectedRows[0].formId
            , "formNm": this.formId_s
            , "formType": this.formType_s
            , "searchFormId": this.formId_s
        }

        const value = await Alert.confirm("삭제하시겠습니끼?").then(result => { return result; });
        if (value) {
            const { data, status } = await FormRepository.deleteForms(subData);

            if (status == 200) {
                this.formList = data.data;
                this.formGridApi.setRowData(this.formList);
                Alert.meg("삭제되었습니다.");
            }
        }
        else {
            return;
        }
    }

    //버튼 영역

    @action.bound
    setBtnGridApi = async (gridApi) => {

        this.btnGridApi = gridApi;

        const columnDefs = [
            { headerName: "버튼 NO", field: "btnNo", width: 150, hide: true },
            { 
                headerName: "프로그램ID"
                , field: "formId"
                , width: 200
                , editable: false
                , customCellClass: 'cell_align_center'
            },
            { headerName: "버튼ID", field: "btnId", width: 150 },
            { headerName: "버튼명", field: "btnNm", width: 150 },
            { headerName: "등록자", field: "inputEmpNm", width: 100, editable: false, customCellClass: 'cell_align_center' },
            { headerName: "등록일", field: "inputDtm", width: 200, editable: false }
        ];

        this.btnGridApi.setColumnDefs(columnDefs);
    }

    // 버튼 조회
    @action.bound
    getFormBtn = async () => {

        const selectedRows = GridUtil.getSelectedRowData(this.formGridApi);
        const submitdata = {
            "formId": selectedRows.formId
        }

        const { data, status } = await FormRepository.getBtn(submitdata);

        if (status == 200) {
            this.btnList = data.data;
            this.btnGridApi.setRowData(this.btnList);
        }
    }

    // 버튼 저장
    @action.bound
    btnSaveClick = async () => {

        const saveArr = GridUtil.getGridSaveArray(this.btnGridApi);
        const selectedRows = GridUtil.getSelectedRowData(this.formGridApi);
        
        if (saveArr.length == 0) {
            Alert.meg('저장할 데이터가 없습니다.');
            return;
        }

        const searchData = {
            "formId": selectedRows.formId
        }
        
        const param = JSON.stringify({ param1: saveArr, param2: searchData });

        const { data, status } = await FormRepository.saveBtn(param);

        if (status == 200) {
            this.btnList = data.data;
            this.btnGridApi.setRowData(this.btnList);

            Alert.meg("저장되었습니다.");
        }
    }

    // 버튼 추가
    @action.bound
    btnAddClick = () => {

        const selectedRows = GridUtil.getSelectedRowData(this.formGridApi);
        let formId;
        
        if (selectedRows) {
            formId =  selectedRows.formId;
        } else {
            Alert.meg("프로그램ID를 선택해주세요.");
            return;
        }

        let lastIndex = this.btnGridApi.getLastDisplayedRow();

        let btnHdmt = {
              btnNo: ''
            , formId: formId
            , btnId: ''
            , btnNm: ''            
        };
        const result = this.btnGridApi.updateRowData({ add: [btnHdmt], addIndex: lastIndex + 1 });
        GridUtil.setFocus(this.btnGridApi, result.add[0]);
    }

    // 버튼 삭제
    @action.bound
    btnDeleteClick = async () => {

        // const selectedRows = this.btnGridApi.getSelectedRows();
        const selectedRows = GridUtil.getSelectedRowData(this.btnGridApi);
        if (!selectedRows) {
            Alert.meg('삭제할 항목을 선택해 주세요.');
            return;
        }

        const subData = {
            "btnNo": selectedRows.btnNo
            , "formId": selectedRows.formId
        }

        const value = await Alert.confirm("삭제하시겠습니끼?").then(result => { return result; });

        if (value) {
            const { data, status } = await FormRepository.deleteBtn(subData);

            if (status == 200) {
                this.btnList = data.data;
                this.btnGridApi.setRowData(this.btnList);
                Alert.meg("삭제되었습니다.");
            }
        }
        else {
            return;
        }
    }

}

export default new FormStore();