import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid';

@inject(stores => ({
      setBtnGridApi: stores.formStore.setBtnGridApi
    , btnList: stores.formStore.btnList
}))

class FormBtnContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setBtnGridApi, btnList } = this.props;

        return (

            <SGrid
                grid={'btnGrid'}
                gridApiCallBack={setBtnGridApi}
                rowData={btnList}
                cellReadOnlyColor={true}
            />
        );
    }
}

export default FormBtnContents;