import React, { Component } from "react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import FormSearchItem from "modules/pages/system/form/FormSearchItem";
import FormMiddleItem from "modules/pages/system/form/FormMiddleItem";
import FormContents from "modules/pages/system/form/FormContents";

import FormBtnMiddleItem from "modules/pages/system/form/FormBtnMiddleItem";
import FormBtnContents from "modules/pages/system/form/FormBtnContents";

class Form extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <React.Fragment>
                <div style={{ width: '100%', height: '60%'}}>
                    <SearchTemplate searchItem={<FormSearchItem />} />
                    <ContentsMiddleTemplate contentSubTitle={'프로그램 정보'} middleItem={<FormMiddleItem />} />
                    <ContentsTemplate id='form' contentItem={<FormContents />} />
                </div>
                <div style={{ width: '100%', height: '40%'}}>
                    <ContentsMiddleTemplate contentSubTitle={'버튼 정보'} middleItem={<FormBtnMiddleItem />} />
                    <ContentsTemplate id='btn' contentItem={<FormBtnContents />} />
                </div>
            </React.Fragment>
        )
    }
}

export default Form;