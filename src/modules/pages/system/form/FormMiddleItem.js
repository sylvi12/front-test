import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    handleSaveClick: stores.formStore.handleSaveClick,
    handleAddClick: stores.formStore.handleAddClick,
    handleDeleteClick: stores.formStore.handleDeleteClick
}))

class FormMiddleItem extends Component {

    render() {

        const { handleSaveClick, handleAddClick, handleDeleteClick } = this.props;
        return (
            <React.Fragment>
                <SButton
                    className="btn_red"
                    buttonName={'저장'}
                    type={'btnSave'}
                    onClick={handleSaveClick}
                />
                <SButton
                    buttonName={'추가'}
                    className="btn_red"
                    type={'btnAdd'}
                    onClick={handleAddClick}
                />
                <SButton
                    buttonName={'삭제'}
                    type={'btnDelete'}
                    onClick={handleDeleteClick}
                />
            </React.Fragment>
        )
    }
}

export default FormMiddleItem;