
import API from 'components/override/api/API'
import alert from 'components/override/alert/Alert'

class StorageRepository {

    URL = "/api/storage";

    async downloadBarcodeSetup() {
        const downUrl = encodeURI(`${this.URL}/file?path=wpf,barcode&filename=SYS-WMS-BARCODE.msi`);
        await this.fileDownload(downUrl);
    }

    async downloadIPCSetup() {
        const downUrl = encodeURI(`${this.URL}/file?path=wpf,ipc&filename=SYS-WMS-IPC.msi`);
        await this.fileDownload(downUrl);
    }

    async downloadZebraSetup() {
        const downUrl = encodeURI(`${this.URL}/file?path=wpf,barcode&filename=zebra.exe`);
        await this.fileDownload(downUrl);
    }

    async downloadZebraFont() {
        const downUrl = encodeURI(`${this.URL}/file?path=wpf,barcode&filename=Font.zip`);
        await this.fileDownload(downUrl);
    }

    // 파일 다운로드
    async fileDownload(downUrl) {
        const config = {
            headers: {
                Pragma: 'no-cache'
            },
            responseType: 'blob',
        }

        await API.file.get(`${downUrl}`, config).then((res) => {

            let disposition = res.headers['content-disposition']
            let filename = '';

            try {
                filename = this.replaceAll(decodeURI(disposition.match(/fileName="(.*)"/)[1]), "+", " ");
                console.log('download fileName:', filename);
            } catch (error) {
                console.warn(error);
                alert.meg('다운로드 중 문제가 발생 하였습니다.');
                return;
            }
            if (navigator.appVersion.toString().indexOf('.NET') > 0) {
                window.navigator.msSaveBlob(new Blob([res.data]), filename);
            } else {
                const url = window.URL.createObjectURL(new Blob([res.data], { type: 'application/vnd.ms-excel' }));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', filename);
                document.body.appendChild(link);
                link.click();
            }
        });
    }

    replaceAll(str, searchStr, replaceStr) {
        return str.split(searchStr).join(replaceStr);
    }
}

export default new StorageRepository();
