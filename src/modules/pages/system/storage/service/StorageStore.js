import StorageRepository from 'modules/pages/system/storage/service/StorageRepository'
import { observable, action } from 'mobx';

class StorageStore {

    @observable downloadStateText = "";

    @action.bound
    async downloadFile(target) {

        this.downloadStateText = "다운로드중입니다.";

        const spinnersection = document.querySelector('#downloadsection');
        spinnersection.style.display = 'block';

        switch (target) {
            case "barcode":
                await StorageRepository.downloadBarcodeSetup();
                break;
            case "ipc":
                await StorageRepository.downloadIPCSetup();
                break;
            case "zebra":
                await StorageRepository.downloadZebraSetup();
                break;
            case "font":
                await StorageRepository.downloadZebraFont();
                break;
        }

        spinnersection.style.display = 'none';
        this.downloadStateText = "다운로드 완료되었습니다.";
    }
}

export default new StorageStore();