import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from "components/atoms/button/SButton";

@inject(stores => ({
    onClickGrpBtnSave: stores.grpStore.onClickGrpBtnSave
    , closeModal: stores.grpStore.closeModal
}))

class GrpModalMiddleItem extends Component {

    render() {
        const { onClickGrpBtnSave, closeModal } = this.props;

        return (
            <React.Fragment>
                <SButton buttonName={"선택"} onClick={onClickGrpBtnSave} type={"btnGrpBtnChoose"} />
                <SButton buttonName={"닫기"} onClick={closeModal} type={"default"} />
            </React.Fragment>
        );
    }
}
export default GrpModalMiddleItem;