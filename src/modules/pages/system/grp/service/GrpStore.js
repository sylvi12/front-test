import { observable, action } from 'mobx';

import Alert from 'components/override/alert/Alert';
import GridUtil from 'components/override/grid/utils/GridUtil';
import Validator from 'components/override/utils/Validator';
import BaseStore from 'utils/base/BaseStore';

import GrpRepository from "modules/pages/system/grp/service/GrpRepository";

class GrpStore extends BaseStore {

    // 조회
    @observable menuId_s = '';
    @observable modalSearchText_s = '';
    @observable initCheck = '';

    // 그리드
    @observable gridApi = undefined;
    @observable grpList;
    @observable gridApiUser = undefined;
    @observable userList = [];
    @observable gridApiMenu = undefined;
    menuList = [];
    @observable gridApiBu = undefined;
    @observable buList;
    @observable gridApiDept = undefined;
    @observable deptList;
    @observable gridApiModal = undefined;
    grpBtnList = [];

    // 유저 Helper모달
    @observable isEmpOpen = false;
    @observable temp = [];

    // 권한 모달
    @observable modalIsOpen = false;

    // 권한 복사 모달
    @observable copyModalOpen = false;

    validator = new Validator([
        {
            field: 'newGrpId',
            method: 'isEmpty',
            message: '신규권한ID',
        },
        {
            field: 'newGrpNm',
            method: 'isEmpty',
            message: '신규권한명',
        }
    ])

    constructor() {
        // this.root = root;
        super();
        this.setInitialState();
    }

    // 버튼 권한 Modal
    handleAddClick = async () => {

        const selectRow = GridUtil.getSelectedRowData(this.gridApiMenu);

        const btnSearch = {
            "grpId": selectRow.grpId
            , "grpMenuId": selectRow.grpMenuId
            , "formId": selectRow.formId
        }

        this.modalIsOpen = true;

        const result = await GrpRepository.getBtn(btnSearch);

        if (result.status == 200) {
            this.grpBtnList = result.data.data;
            this.gridApiModal.setRowData(this.grpBtnList);
        }
    }

    // 권한 조회
    @action.bound
    searchGrp = async () => {
        const { data, status } = await GrpRepository.getGrps();
        if (status == 200) {
            this.grpList = data.data;
            this.gridApi.setRowData(this.grpList);
        }
    }

    // 권한 그리드
    @action.bound
    setGridApi = (gridApi) => {
        this.gridApi = gridApi;

        this.columnDefs = [
            {
                headerName: "권한ID"
                , field: "grpId"
                , width: 100
                , editable: function (params) {
                    if (params.node.data.newItem == 'Y') {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            },
            { headerName: "권한명", field: "grpNm", width: 150 },
            { headerName: "설명", field: "grpDesc", width: 190 },
        ];

        this.gridApi.setColumnDefs(this.columnDefs);
        this.gridApi.setRowData(this.grpList);
    }

    // 공장 그리드
    @action.bound
    setGridApiBu = (gridApiBu) => {
        this.gridApiBu = gridApiBu;

        this.columnDefs = [

            {
                headerName: "선택"
                , field: "masterYn"
                , width: 50
                , cellRenderer: 'rowCellCheckbox'
                // , headerCheckboxSelection: true
                // , checkboxSelection: true
                , cellClass: 'cell_align_center'
            },
            { headerName: "공장ID", field: "buId", width: 100 },
            { headerName: "공장명", field: "buNm", width: 100 }

        ];

        this.gridApiBu.setColumnDefs(this.columnDefs);
        this.gridApiBu.setRowData(this.buList);
    }

    // 부서 그리드
    @action.bound
    setGridApiDept = (gridApiDept) => {
        this.gridApiDept = gridApiDept;

        this.columnDefs = [
            {
                headerName: "선택"
                , field: "masterYn"
                , width: 50
                , cellRenderer: 'rowCellCheckbox'
                , cellClass: 'cell_align_center'
            },
            { headerName: "공장", field: "buNm", width: 80, cellClass: 'cell_align_center' },
            { headerName: "부서코드", field: "deptCd", width: 80, cellClass: 'cell_align_center' },
            { headerName: "부서명", field: "deptNm", width: 100 }

        ];

        this.gridApiDept.setColumnDefs(this.columnDefs);
        this.gridApiDept.setRowData(this.deptList);
    }

    // 유저 그리드
    @action.bound
    setGridApiUser = (gridApiUser) => {
        this.gridApiUser = gridApiUser;

        const columnDefs = [
            {
                headerName: ""
                , field: "check"
                , width: 29
                , checkboxSelection: true
                , headerCheckboxSelection: true
                , cellClass: 'cell_align_center'
            },
            { headerName: "계정명", field: "userId", width: 90, cellClass: 'cell_align_center' }
            , { headerName: "성명", field: "loginNm", width: 150, cellClass: 'cell_align_center' }
            , { headerName: "생산관리자여부", field: "masterYn", width: 100, cellClass: 'cell_align_center' }
        ];

        this.gridApiUser.setColumnDefs(columnDefs);
        this.gridApiUser.setRowData(this.userList);
    }

    // 메뉴 그리드
    @action.bound
    setGridApiMenu = (gridApiMenu) => {
        this.gridApiMenu = gridApiMenu;

        const columnDefs = [
            {
                headerName: "선택"
                , field: "masterYn"
                , width: 50
                , cellRenderer: 'rowCellCheckbox'
                , cellClass: 'cell_align_center'

            },
            { headerName: "메뉴ID", field: "menuId", width: 80, cellClass: 'cell_align_center' }
            , { headerName: "메뉴명", field: "menuNm", width: 300 }
            , {
                headerName: "버튼명"
                , field: "btnList"
                , width: 250
            },
            {
                headerName: "권한버튼명"
                , field: "btnGrpList"
                , width: 250
            },
            {
                headerName: "버튼권한"
                , field: "grp"
                , width: 80
                , cellRenderer: "rowCellButtonRender"
                , cellRendererParams: { buttonName: "권한등록", onClick: this.handleAddClick }
            }
        ];

        this.gridApiMenu.setColumnDefs(columnDefs);
        this.gridApiMenu.setRowData(this.menuList);
    }

    // 선택 권한
    searchGprId = () => {

        const rows = this.gridApi.getSelectedRows();
        const grpId = rows == '' ? '' : rows[0].grpId;
        this.initCheck = 'Y';
        return grpId;
    }

    // 메뉴 조회
    searchMenu = async (param) => {
        const grpId = this.searchGprId();

        if (grpId == '') {
            this.gridApiMenu.setRowData([]);
            return;
        }

        const menuSearchdata = {
            "menuId": param.menuId == '' ? '' : param.menuId
            , "menuNm": param.menuNm == '' ? '' : param.menuNm
            , "grpId": grpId
        }

        const result = await GrpRepository.getMenu(menuSearchdata);

        if (result.status == 200) {
            this.menuList = result.data.data;

            this.gridApiMenu.setRowData(this.menuList);
        }
    }

    // 유저 조회
    @action.bound
    searchUser = async () => {
        const grpId = this.searchGprId();

        const result = await GrpRepository.getUsers(grpId);

        if (result.status == 200) {
            this.userList = result.data.data;
            this.gridApiUser.setRowData(this.userList);
        }
    }

    // 공장 조회
    searchBu = async () => {
        const grpId = this.searchGprId();

        const result = await GrpRepository.getBu(grpId);

        if (result.status == 200) {
            this.buList = result.data.data;
            this.gridApiBu.setRowData(this.buList);
        }
    }

    // 부서 조회
    searchDept = async () => {
        const grpId = this.searchGprId();

        const result = await GrpRepository.getDept(grpId);

        if (result.status == 200) {
            this.deptList = result.data.data;
            this.gridApiDept.setRowData(this.deptList);
        }
    }

    // 권한 그리드 선택시 자동 조회(메뉴, 유저)
    @action.bound
    onClickGrpRow = async () => {
        this.gridApiUser.setRowData([]);

        const userSearchdata = this.searchGprId();

        this.searchBu();
        this.searchDept();

        const result = await GrpRepository.getUsers(userSearchdata);
        if (result.status == 200) {
            this.userList = result.data.data;
            this.gridApiUser.setRowData(this.userList);

            const menuSearchdata = {
                "menuId": ''
                , "menuNm": ''
            }
            this.searchMenu(menuSearchdata);
        }

    }

    // 버튼 권한 그리드
    @action.bound
    setGridApiModal = (gridApiModal) => {
        this.gridApiModal = gridApiModal;

        const columnDefs = [
            { headerName: "선택", field: "masterYn", width: 35, cellRenderer: 'rowCellCheckbox' }
            , { headerName: "버튼ID", field: "btnId", width: 90 }
            , { headerName: "버튼명", field: "btnNm", width: 150 }
        ];

        this.gridApiModal.setColumnDefs(columnDefs);
        this.gridApiModal.setRowData(this.grpBtnList);
    }

    @action.bound
    handleChange = (data) => {
        console.log(data)
        this[data.id] = data.value;
    }

    //메뉴 조회
    @action.bound
    onClickMenuSearch = async () => {

        const menuSearchdata = {
            "menuId": this.menuId_s
            , "menuNm": this.menuId_s
        }
        this.searchMenu(menuSearchdata);
    }

    // 버튼 권한 닫기
    @action.bound
    closeModal = () => {
        this.modalIsOpen = false;
    }
    // 유저 모달 조회
    // @action.bound
    // onClickModalSearch = async () => {
    //     const userSearchdata = {
    //         "userId": this.modalSearchText_s
    //         , "loginNm": this.modalSearchText_s
    //     }

    //     const { data, status } = await GrpRepository.getModal(userSearchdata);
    //     if (status == 200) {
    //         this.gridApiModal.setRowData(data.data);
    //     }
    // }

    // 행 추가
    @action.bound
    onClickGrpAdd = () => {
        let lastindex = this.gridApi.getLastDisplayedRow();

        const menuHdmt = {
            grpId: ''
            , grpNm: ''
            , grpDesc: ''
            , useFlag: 'Y'
            , newItem: 'Y'
            , edit: 'Y'
        };

        const result = this.gridApi.updateRowData({ add: [menuHdmt], addIndex: lastindex + 1 });
        GridUtil.setFocus(this.gridApi, result.add[0]);
    }

    // 유저 팝업
    @action.bound
    onClickUserAdd = () => {
        this.temp = [];
        const rows = this.gridApi.getSelectedRows();

        if (rows == '') {
            Alert.meg("권한을 먼저 선택해 주십시오.");
            return;
        }

        //
        this.userList.forEach(x => {
            this.temp.push({ 'empNo': x.userId });
        });

        this.isEmpOpen = true;

    }

    // 사원 선택하면 유저 저장
    @action.bound
    lotHelperResult = (result) => {
        if (result) {
            this.onClickUserSave(result);
        }
    }


    // 권한 저장
    @action.bound
    onClickGrpSave = async () => {
        const saveArr = GridUtil.getGridSaveArray(this.gridApi);
        if (saveArr.length == 0) {
            Alert.meg('저장할 데이터가 없습니다.');
            return;
        }

        const param = JSON.stringify({ param: saveArr });
        const result = await GrpRepository.updateGrp(param);
        if (result.status == 200) {
            Alert.meg("저장되었습니다.");
            // 저장 후 메뉴 자동 조회
            const menuSearchdata = {
                "menuId": ''
                , "menuNm": ''
            }
            this.searchGrp();
            this.searchMenu(menuSearchdata);
        } else {
            Alert.meg("저장중 오류가 발생 했습니다.");
        }
    }

    // 유저 저장
    @action.bound
    onClickUserSave = async (saveArr) => {

        const grpId = this.searchGprId();

        saveArr.forEach(obj => {
            obj.grpId = grpId;
        });

        const param = JSON.stringify({ param: saveArr });

        const { data, status } = await GrpRepository.updateUser(param);

        // 성공
        if (status == 200) {
            // Alert.meg(data.msg);
            Alert.meg('저장되었습니다.');
            // 유저 재조회
            this.searchUser();
        }
        // 실패
        else {
            Alert.meg(data.msg);
            // Alert.meg('저장중 오류가 발생 하였습니다.');
        }

    }

    // 공장 저장
    @action.bound
    onClickBuSave = async () => {

        const saveArr = GridUtil.getGridSaveArray(this.gridApiBu);
        if (saveArr.length == 0) {
            Alert.meg('저장할 데이터가 없습니다.');
            return;
        }
        const grpId = this.searchGprId();

        // Set 권한
        saveArr.forEach(obj => {
            obj.grpId = grpId;
        });

        const param = JSON.stringify({ param: saveArr });
        const { data, status } = await GrpRepository.updateBu(param);

        if (status == 200) {
            // Alert.meg(data.msg);
            Alert.meg("저장되었습니다.");
            this.searchBu();
        } else {
            Alert.meg(data.msg);
        }
    }

    // 부서 저장
    @action.bound
    onClickDeptSave = async () => {

        const saveArr = GridUtil.getGridSaveArray(this.gridApiDept);
        if (saveArr.length == 0) {
            Alert.meg('저장할 데이터가 없습니다.');
            return;
        }
        const grpId = this.searchGprId();

        // Set 권한
        saveArr.forEach(obj => {
            obj.grpId = grpId;
        });

        const param = JSON.stringify({ param: saveArr });
        console.log(param);

        const { data, status } = await GrpRepository.updateDept(param);

        if (status == 200) {
            // Alert.meg(data.msg);
            Alert.meg("저장되었습니다.");
            this.searchDept();
        } else {
            Alert.meg(data.msg);
        }
    }

    // 메뉴 저장
    @action.bound
    onClickMenuSave = async () => {
        const saveArr = GridUtil.getGridSaveArray(this.gridApiMenu);
        const grpId = this.searchGprId();

        if (saveArr.length == 0) {
            Alert.meg("아이템을 선택해 주세요.");
            return;
        }
        // Set 권한
        saveArr.forEach(obj => {
            obj.grpId = grpId;
        });

        const param = JSON.stringify({ param: saveArr });
        console.log(param);
        const result = await GrpRepository.updateMenu(param);

        // 성공
        if (result.status == 200) {
            Alert.meg('저장되었습니다.');
            // 삭제 후 메뉴 자동 조회
            const menuSearchdata = {
                "menuId": ''
                , "menuNm": ''
            }
            this.searchMenu(menuSearchdata);
        }
        // 실패
        else {
            Alert.meg('저장중 오류가 발생 하였습니다.');
        }
    }

    // 버튼 권한 저장
    @action.bound
    onClickGrpBtnSave = async () => {

        const saveArr = GridUtil.getGridSaveArray(this.gridApiModal);
        if (saveArr.length == 0) {
            Alert.meg('저장할 데이터가 없습니다.');
            return;
        }
        const grpId = this.searchGprId();

        // Set 권한
        saveArr.forEach(obj => {
            obj.grpId = grpId;
        });

        const param = JSON.stringify({ param: saveArr });
        const { data, status } = await GrpRepository.updateGrpBtn(param);

        if (status == 200) {

            // Alert.meg(data.msg);
            Alert.meg("저장되었습니다.");

            const menuSearchdata = {
                "menuId": this.menuId_s
                , "menuNm": this.menuId_s
            }
            this.searchMenu(menuSearchdata);

            this.modalIsOpen = false;
        } else {
            Alert.meg(data.msg);
        }

    }

    // 유저 삭제
    @action.bound
    onClickUserDelete = async () => {

        // const DeleteArr = GridUtil.getGridSaveArray(this.gridApiUser);
        const DeleteArr = GridUtil.getGridCheckArray(this.gridApiUser);
        const grpId = this.searchGprId();

        if (DeleteArr.length == 0) {
            Alert.meg('삭제할 항목을 선택해 주세요.');
            return;
        }

        // Set 권한
        DeleteArr.forEach(obj => {
            obj.grpId = grpId;
        });

        const param = JSON.stringify({ param: DeleteArr });

        const confirm = await Alert.confirm(`선택한 유저를 삭제하시겠습니까?`);

        if (confirm) {

            const { data, status } = await GrpRepository.deleteUser(param);

            if (status == 200) {
                // Alert.meg(data.msg);
                Alert.meg('삭제되었습니다.');
                // 유저 재조회
                this.searchUser();
            }
            else {
                // Alert.meg(data.msg);
                Alert.meg('삭제중 오류가 발생 하였습니다.');
            }
        }
    }

    // 권한 삭제
    @action.bound
    onClickGrpDelete = async () => {

        const grpId = this.searchGprId();
        const rows = this.gridApi.getSelectedRows();

        if (grpId == '' && rows[0] == null) {
            Alert.meg('삭제할 항목을 선택해 주세요.');
            return;
        }

        const confirm = await Alert.confirm(`삭제하시겠습니까?`);

        if (confirm) {
            const result = await GrpRepository.deleteGrp(grpId);
            if (result.status == 200) {
                Alert.meg('삭제되었습니다.');
                this.grpList = result.data.data;
                this.gridApi.setRowData(this.grpList);
                this.searchMenu();
            }
            else {
                Alert.meg('삭제중 오류가 발생 하였습니다.');
            }
        }
    }

    //복사할 권한 ID 변수
    fromGrpId = '';
    @observable newGrpId = '';
    @observable newGrpNm = '';
    @observable grpDesc = '';

    // 권한 복사
    @action.bound
    onClickGrpCopy = () => {

        const selectRow = GridUtil.getSelectedRowData(this.gridApi);

        if (!selectRow || selectRow == undefined || selectRow == null) {
            Alert.meg("선택된 권한ID가 없습니다.");
            return;
        }
        this.fromGrpId = selectRow.grpId;
        this.newGrpId = '';
        this.newGrpNm = '';
        this.grpDesc = '';
        this.copyModalOpen = true;
    }

    //권한복사 모달 닫기
    @action.bound
    copyModalClose = () => {
        this.copyModalOpen = false;
    }

    //권한복사 저장
    @action.bound
    copySave = async () => {

        const validation = this.validator.validate(this);

        if (!validation.isValid) {
            return;
        }

        const saveData = {
            "fromGrpId": this.fromGrpId
            , "grpId": this.newGrpId
            , "grpNm": this.newGrpNm
            , "grpDesc": this.grpDesc
        }

        const { data, status } = await GrpRepository.copyGrp(saveData);
        if (status == 200) {

            this.grpList = data.data;
            this.gridApi.setRowData(this.grpList);

            // Alert.meg(data.msg);
            Alert.meg("복사되었습니다.");

            const index = this.grpList.findIndex(x => x.grpId == this.grpId);
            GridUtil.setFocusByRowIndex(this.gridApi, index);
        }

        this.copyModalClose();
    }

}

export default new GrpStore();