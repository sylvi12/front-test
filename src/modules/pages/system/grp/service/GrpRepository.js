import API from "components/override/api/API";

class GrpRepository {

    URL = "/api/system/grp";

    //// 조회

    // 권한
    getGrps() {
        return API.request.get(`${this.URL}/getgrps`);
    }

    //버튼 권한
    getBtn(params) {
        return API.request.get(`${this.URL}/getbtn?grpId=${params.grpId}&grpMenuId=${params.grpMenuId}&formId=${params.formId}`);
    }

    // 메뉴
    getMenu(params) {
        return API.request.get(encodeURI(`${this.URL}/getmenu?grpId=${params.grpId}&menuId=${params.menuId}&menuNm=${params.menuNm}`));
    }

    // 유저
    getUsers(grpId) {
        return API.request.get(`${this.URL}/getusers?grpId=${grpId}`);
    }

    // 공장
    getBu(grpId) {
        return API.request.get(`${this.URL}/getbu?grpId=${grpId}`);
    }

    // 부서
    getDept(grpId) {
        return API.request.get(`${this.URL}/getdept?grpId=${grpId}`);
    }

    // 유저 모달
    getModal(params) {
        return API.request.get(encodeURI(`${this.URL}/getmodal?loginId=${params.loginId}&loginNm=${params.loginNm}`));
    }

    //// 저장

    // 권한 
    updateGrp(params){
        return API.request.post(`${this.URL}/updategrp`, params);
    }

    // 유저 
    updateUser(params){
        return API.request.post(`${this.URL}/updateuser`, params);
    }
    
    // 공장
    updateBu(params){
        return API.request.post(`${this.URL}/updatebu`, params);
    }

    // 부서
    updateDept(params){
        return API.request.post(`${this.URL}/updatedept`, params);
    }

    // 메뉴 
    updateMenu(params){
        return API.request.post(`${this.URL}/updatemenu`, params);
    }

    // 버튼 권한
    updateGrpBtn(params){
        return API.request.post(`${this.URL}/updategrpbtn`, params);
    }

    ///// 삭제

    // 권한
    deleteGrp(pram) {
        return API.request.delete(encodeURI(`${this.URL}/deletegrp/?grpId=${pram}`))
    }
    
    // 유저
    deleteUser(prams) {
        return API.request.post(`${this.URL}/deleteuser`,prams)
    }

    //권한 복사
    copyGrp(param) {
        console.log(param)
        return API.request.post(`${this.URL}/copygrp`,param);
    }
}
export default new GrpRepository();