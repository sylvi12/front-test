import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SButton from "components/atoms/button/SButton";

@inject(stores => ({
    menuId_s: stores.grpStore.menuId_s
    , onClickMenuSearch: stores.grpStore.onClickMenuSearch
    , handleChange: stores.grpStore.handleChange
    , onClickMenuSave: stores.grpStore.onClickMenuSave
}))
class GrpMenuMiddleItem extends Component {
    render() {
        const { menuId_s, onClickMenuSearch, handleChange, onClickMenuSave } = this.props;

        return (
            <div>
                <ul style={{ float: 'right' }}>
                    <SButton buttonName={'조회'} onClick={onClickMenuSearch} type={'btnMenuSearch'} />
                    <SButton buttonName={'저장'} onClick={onClickMenuSave} type={'btnMenuSave'} />
                </ul>
                <ul style={{ float: 'right' }}>
                    <SInput isFocus={true} id={"menuId_s"} value={menuId_s} onKeyDown={onClickMenuSearch} onChange={handleChange} placeholderValue={"메뉴명/ID"} />
                </ul>
            </div>
        );
    }
}

export default GrpMenuMiddleItem;
