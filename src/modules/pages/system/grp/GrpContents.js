import React, { Component } from 'react';
import { inject } from 'mobx-react';
import SGrid from 'components/override/grid/SGrid';
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import GrpMiddleItem from 'modules/pages/system/grp/GrpMiddleItem';
import GrpUserMiddleItem from 'modules/pages/system/grp/GrpUserMiddleItem';
import GrpMenuMiddleItem from 'modules/pages/system/grp/GrpMenuMiddleItem';
import GrpBuMiddleItem from 'modules/pages/system/grp/GrpBuMiddleItem';
import GrpDeptMiddleItem from 'modules/pages/system/grp/GrpDeptMiddleItem';

@inject(stores => ({
    setGridApiUser: stores.grpStore.setGridApiUser
    , setGridApiMenu: stores.grpStore.setGridApiMenu
    , grpList: stores.grpStore.grpList
    , menuList: stores.grpStore.menuList
    , userList: stores.grpStore.userList
    , buList: stores.grpStore.buList
    , deptList: stores.grpStore.deptList
    , grpStore: stores.grpStore
    , onCellValueChanged: stores.grpStore.onCellValueChanged
}))

class GrpContents extends Component {

    constructor(props) {
        super(props);
    }
    render() {
        const { setGridApiUser, setGridApiMenu, grpStore, onCellValueChanged } = this.props;

        return (
            <div className='basic'>

                <div style={{ width: '100%', height: '40%' }}>
                    <div style={{ width: '31.5%', height: '100%', float: 'left' }}>
                        <ContentsMiddleTemplate contentSubTitle={"권한"} middleItem={<GrpMiddleItem />} />

                        <ContentsTemplate id='grpList' contentItem={
                            <div className="panel_body" style={{ height: '100%' }}>
                                <SGrid grid={"grpGrid"} gridApiCallBack={grpStore.setGridApi} onSelectionChanged={grpStore.onClickGrpRow} rowData={grpStore.grpList} cellReadOnlyColor={true} />
                            </div>
                        } />
                    </div>

                    <div style={{ width: '37%', height: '100%', float: 'left' }}>

                        <div style={{ width: '100%', height: '50%', float: 'left' }}>
                            <ContentsMiddleTemplate contentSubTitle={"공장"} middleItem={<GrpBuMiddleItem />}/>
                            <ContentsTemplate id='buList' contentItem={
                                <div className="panel_body" style={{ height: '100%' }}>
                                    <SGrid grid={"buGrid"} gridApiCallBack={grpStore.setGridApiBu} editable={false} rowData={grpStore.buList} />
                                </div>
                            } />
                        </div>

                        <div style={{ width: '100%', height: '50%', float: 'left' }}>
                            <ContentsMiddleTemplate contentSubTitle={"부서"} middleItem={<GrpDeptMiddleItem />}/>
                            <ContentsTemplate id='deptList' contentItem={
                                <div className="panel_body" style={{ height: '100%' }}>
                                    <SGrid grid={"deptGrid"} gridApiCallBack={grpStore.setGridApiDept} editable={false} rowData={grpStore.deptList} />
                                </div>
                            } />
                        </div>
                    </div>
                    
                    <div style={{ width: '31.5%', height: '100%', float: 'right' }}>
                        <ContentsMiddleTemplate contentSubTitle={"사용자"} middleItem={<GrpUserMiddleItem />} />

                        <ContentsTemplate id='userList' contentItem={
                            <div className="panel_body" style={{ height: '100%' }}>
                                <SGrid grid={"userGrid"} gridApiCallBack={setGridApiUser} editable={false} rowData={grpStore.userList} />
                            </div>
                        } />
                    </div>
                </div>

                <div style={{ width: '100%', height: '60%' }}>

                    <ContentsMiddleTemplate contentSubTitle={"메뉴"} middleItem={<GrpMenuMiddleItem />} />

                    <ContentsTemplate id='menuList' contentItem={
                        <div className="panel_body" style={{ height: '100%' }}>
                            <SGrid grid={"menuGrid"} gridApiCallBack={setGridApiMenu} editable={false} rowData={grpStore.menuList} handleChange={grpStore.onCellValueChanged} />
                        </div>
                    } />

                </div>
            </div>
        );
    }
}
export default GrpContents;