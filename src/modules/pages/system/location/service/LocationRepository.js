import API from "components/override/api/API";

class LocationRepository {

    URL = "/api/location";
    
    getLocation(params) {
        return API.request.get(encodeURI(`${this.URL}/list?buId=${params.buId}&level=${params.level || ""}&locNm=${params.locNm || ""}`))
    }

}
export default new LocationRepository();