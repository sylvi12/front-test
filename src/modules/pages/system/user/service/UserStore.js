import { observable, action } from 'mobx';

import Validator from 'components/override/utils/Validator';
import alert from 'components/override/alert/Alert';
import FileRepository from 'utils/file/FileRepository';

import UserRepository from "modules/pages/system/user/service/UserRepository"

import ObjectUtility from 'utils/object/ObjectUtility';
import CommonStore from 'modules/pages/common/service/CommonStore';
import BaseStore from 'utils/base/BaseStore'
import GridUtil from 'components/override/grid/utils/GridUtil';

class UserStore extends BaseStore {

    // 조회 변수
    @observable empNo_s = '';
    @observable useYN_s = '';

    // 그리드 변수
    gridApi = undefined;
    @observable userList;

    // modal변수
    @observable loginId = '';
    @observable editType = '';
    @observable empNo = '';
    @observable loginNm = '';
    @observable loginPass = '';
    @observable deptCd = '';
    @observable loginEmail = '';
    @observable loginTel = '';
    @observable loginMoblTelno = '';
    // @observable langId = '';
    @observable masterYn = '';
    @observable useYN = '';

    @observable buId = CommonStore.buId;
    @observable buList = CommonStore.buList;
    @observable hrYN = '';
    @observable outsourcingYn = '';

    @observable modalIsOpen = false;

    validator = new Validator([
        {
            field: 'loginId',
            method: 'isEmpty',
            message: '로그인ID'
        },
        {
            field: 'empNo',
            method: 'isEmpty',
            message: '사번'
        },
        {
            field: 'loginNm',
            method: 'isEmpty',
            message: '이름'
        },
        {
            field: 'loginPass',
            method: 'isEmpty',
            message: '비밀번호'
        }
    ]);

    // @observable plnt = '';    

    constructor() {
        super()
        this.validator.valid();
        this.setInitialState();
        
    }

    //공장불러오기
    @action.bound
    getBu = () => {
        this.buList = CommonStore.buList;
        this.buId = CommonStore.buId;
    }
    // 그리드
    @action.bound
    setGridApi(gridApi) {
        this.gridApi = gridApi;

        const columnDefs = [
            { headerName: "공장", field: "buId", width: 100, cellClass: 'cell_align_center' },
            { headerName: "로그인ID", field: "loginId", width: 100, cellClass: 'cell_align_center' },
            { headerName: "사번", field: "empNo", width: 90, cellClass: 'cell_align_center' },
            { headerName: "이름", field: "loginNm", width: 80, cellClass: 'cell_align_center' },
            { headerName: "이메일", field: "loginEmail", width: 200 },
            { headerName: "전화번호", field: "loginTel", width: 120 },
            { headerName: "모바일", field: "loginMoblTelno", width: 120 },
            // { headerName: "언어", field: "langNm", width: 80, cellClass: 'cell_align_center' },
            { headerName: "생산관리자여부", field: "masterYn", width: 100, cellClass: 'cell_align_center' },
            { headerName: "사용여부", field: "useYN", width: 90, cellClass: 'cell_align_center' },
            { headerName: "HR여부", field: "hrYN", width: 90, cellClass: 'cell_align_center' },
            { headerName: "외주여부", field: "outsourcingYn", width: 90, cellClass: 'cell_align_center' },
            { headerName: "등록자", field: "inputEmpNm", width: 80, cellClass: 'cell_align_center' },
            { headerName: "등록일", field: "inputDtm", width: 150 },

        ];
        GridUtil.setSelectCellByList(columnDefs[0], CommonStore.buList, "buId", "buNm"); 
        this.gridApi.setColumnDefs(columnDefs);
    }

    @action.bound
    handleChange(data) {
        console.log(data);
        this[data.id] = data.value;
    }

    // 조회
    @action.bound
    onClickSearch = async () => {

        const submitdata = {
            "empNo": this.empNo_s,
            "useYN": this.useYN_s,
            "buId": this.buId
        }
        const { data, status } = await UserRepository.getUser(submitdata);
        if (status == 200) {
            this.userList = data.data;
            this.gridApi.setRowData(this.userList);
        }
    }

    // 입력 팝업
    @action.bound
    onClickAdd() {

        const ynOption = CommonStore.codeObject['SYS001'];

        this.loginId = '';
        this.empNo = '';
        this.loginNm = '';
        this.loginPass = '';
        this.deptCd = '';
        this.plnt = '';
        this.loginTel = '';
        this.loginEmail = '';
        this.loginMoblTelno = '';
        // this.langId = langOption[0].cd;
        this.masterYn = ynOption[0].cd;
        this.useYN = ynOption[0].cd;

        this.buId = CommonStore.buId;
        this.buList = CommonStore.buList;
        this.hrYN = ynOption[0].cd;
        this.outsourcingYn = ynOption[0].cd;

        this.editType = 'add'
        this.modalIsOpen = true;
    }

    //수정
    @action.bound
    onClickEdit() {

        const selectedRows = this.gridApi.getSelectedRows();

        if (selectedRows.length == 0) {
            alert.meg('수정할 항목을 선택해 주세요.');
            return;
        }
        
        this.buList = CommonStore.buList;
        this.buId = selectedRows[0].buId;
        this.loginId = selectedRows[0].loginId;
        this.empNo = selectedRows[0].empNo;
        this.loginNm = selectedRows[0].loginNm;
        this.loginPass = selectedRows[0].loginPass;
        this.deptCd = selectedRows[0].deptCd;
        this.loginTel = selectedRows[0].loginTel;
        this.loginEmail = selectedRows[0].loginEmail;
        this.loginMoblTelno = selectedRows[0].loginMoblTelno;
        //this.langId = selectedRows[0].langId;
        this.masterYn = selectedRows[0].masterYn;
        this.useYN = selectedRows[0].useYN;
        this.hrYN = selectedRows[0].hrYN;
        this.outsourcingYn = selectedRows[0].outsourcingYn;

        this.editType = 'edit'
        this.modalIsOpen = true;
    }


    @action.bound
    closeModal() {
        this.modalIsOpen = false;
    }

    // 저장
    @action.bound
    async onClickSave() {

        const validation = this.validator.validate(this);

        //validation check
        if (!validation.isValid) {
            return;
        }

        const dataForm = {
            "buId": this.buId,
            "loginId": this.loginId,
            "empNo": this.empNo,
            "loginNm": this.loginNm,
            "loginPass": this.loginPass,
            "deptCd": this.deptCd,
            "loginEmail": this.loginEmail,
            "loginTel": this.loginTel,
            "loginMoblTelno": this.loginMoblTelno,
            // "langId": "",
            "masterYn": this.masterYn,
            "useYN": this.useYN,
            "hrYN": this.hrYN,
            "outsourcingYn": this.outsourcingYn
        };
        
        
        const searchdata = {
            "empNo": this.empNo_s,
            "useYN": this.useYN_s
        }

        const param = JSON.stringify({ param1: dataForm, param2: searchdata });

        const { data, status } = await UserRepository.saveUser(param);

        if (status == 200) {
            alert.meg('저장되었습니다.');
            this.modalIsOpen = false;
            this.userList = data.data;
            this.gridApi.setRowData(this.userList);

            const index = this.userList.findIndex(x => x.loginId == this.loginId);
            GridUtil.setFocusByRowIndex(this.gridApi, index);
        }
        else {
            alert.meg("저장중 오류가 발생 했습니다.");
        }
    }

    // 삭제
    @action.bound
    async onClickDelete() {

        const selectedRows = this.gridApi.getSelectedRows();

        if (selectedRows.length == 0) {
            alert.meg('삭제할 항목을 선택해 주세요.');
            return;
        }

        const result = await alert.confirm(`[${selectedRows[0].loginId}] 사용자를 삭제하시겠습니까?`);

        if (result) {
            const deleteData = {
                "loginId": selectedRows[0].loginId
            }
            const { data, status } = await UserRepository.deleteUser(deleteData);
            if (status == 200) {
                alert.meg('삭제되었습니다.');
                await this.onClickSearch();
            }
            else {
                alert.meg('삭제중 오류가 발생 하였습니다.');
            }
        }
    }

    @action.bound
    onClickExcel = async () => {

        if (this.gridApi.getLastDisplayedRow() < 0) {
            alert.meg("조회 후 사용해 주세요.");
            return;
        }
        const params = {
            fileName: "사용자 관리",
            sheetName: "사용자 정보"
        }
        this.gridApi.exportDataAsExcel(params);
    }
}

export default new UserStore();