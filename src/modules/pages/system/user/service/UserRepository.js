import API from "components/override/api/API";

class UserRepository {

    URL = "/api/system/user";

    // 조회
    getUser(params) {
        return API.request.get(encodeURI(`${this.URL}/getusers?empNo=${params.empNo}&useYN=${params.useYN}&buId=${params.buId}`))
        // ? params.buId : ""
    }

    // 저장
    saveUser(params) {
        return API.request.post(`${this.URL}/save`, params)
    }

    // 삭제
    deleteUser(params) {
        console.log(params.loginId);
        return API.request.delete(encodeURI(`${this.URL}/delete?loginId=${params.loginId}`))
    }

}
export default new UserRepository();