import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SearchTemplate from 'components/template/SearchTemplate'
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate'
import ContentsTemplate from 'components/template/ContentsTemplate'
import SModal from 'components/override/modal/SModal';

import UserSearchItem from 'modules/pages/system/user/UserSearchItem'
import UserMiddleItem from 'modules/pages/system/user/UserMiddleItem'
import UserContents from 'modules/pages/system/user/UserContents'
import UserModal from 'modules/pages/system/user/UserModal'

@inject(stores => ({
    modalIsOpen: stores.userStore.modalIsOpen,
    closeModal: stores.userStore.closeModal
}))

class User extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { modalIsOpen, closeModal } = this.props;

        return (
            <React.Fragment>

                <SearchTemplate searchItem={<UserSearchItem />} />
                <ContentsMiddleTemplate contentSubTitle={'사용자 정보'} middleItem={<UserMiddleItem />} />
                <ContentsTemplate id='user' contentItem={<UserContents />} />

                <SModal
                    formId={"system_userSave_p"}
                    isOpen={modalIsOpen}
                    onRequestClose={closeModal}
                    contentLabel="사용자 저장"
                    width={600}
                    height={380}
                    minWidth={600}
                    minHeight={300}
                    contents={<UserModal />}
                />
            </React.Fragment>
        )
    }
}

export default User;