import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton';


@inject(stores => ({
    onClickDelete: stores.userStore.onClickDelete,
    onClickEdit: stores.userStore.onClickEdit,
    onClickAdd: stores.userStore.onClickAdd
}))
class UserMiddleItem extends Component {
    render() {
        const { onClickDelete, onClickEdit, onClickAdd } = this.props;

        return (
            <div>
                <SButton buttonName={'추가'} onClick={onClickAdd} type={"btnAdd"} />
                <SButton buttonName={'수정'} onClick={onClickEdit} type={"btnModify"} />
                <SButton buttonName={'삭제'} onClick={onClickDelete} type={"btnDelete"} />
            </div>
        );
    }
}

export default UserMiddleItem;
