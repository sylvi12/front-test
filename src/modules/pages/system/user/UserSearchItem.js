import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SButton from 'components/atoms/button/SButton';
import UserStore from 'modules/pages/system/user/service/UserStore';

@inject(stores => ({
    empNo_s: stores.userStore.empNo_s,
    handleChange: stores.userStore.handleChange,
    onClickSearch: stores.userStore.onClickSearch,
    useYN_s: stores.userStore.useYN_s,
    onClickExcel: stores.userStore.onClickExcel,
    buId: stores.userStore.buId,
    buList: stores.userStore.buList
}))

@observer
class UserSearchItem extends Component {

    constructor(props) {
        super(props);
        UserStore.getBu();
    }

    render() {

        const { empNo_s, handleChange, onClickSearch, useYN_s, onClickExcel, buId, buList } = this.props;

        return (
            <React.Fragment>
                <SSelectBox
                    title={"공장"}
                    id={"buId"}
                    value={buId}
                    onChange={handleChange}
                    optionGroup={buList}
                    valueMember={"buId"}
                    displayMember={"buNm"}
                    addOption={"전체"}
                />
                <SInput title={"사번/이름"} id={"empNo_s"} value={empNo_s} onChange={handleChange} onEnterKeyDown={onClickSearch} isFocus={true} /> 
                {/* onKeyDown={onClickSearch} */}
                <SSelectBox title="사용유무" onChange={handleChange} codeGroup={"SYS001"} width={100} id={"useYN_s"} value={useYN_s} addOption="ALL" />

                <div className='search_item_btn'>
                    <SButton buttonName={'조회'} onClick={onClickSearch} type={'btnSearch'} />
                    <SButton buttonName={'엑셀'} onClick={onClickExcel} type={'btnExcel'} />
                </div>
            </React.Fragment>
        );
    }
}

export default UserSearchItem;