import API from "components/override/api/API";

class ItemCategoryRepository {

    URL = "api/system/itemCategory";

    getItemCategory(params) {
        return API.request.get(encodeURI(`${this.URL}/search?buId=${params.buId}`))
    }
}
export default new ItemCategoryRepository();