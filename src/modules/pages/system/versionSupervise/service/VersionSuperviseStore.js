import { observable, action, toJS } from 'mobx';
import BaseStore from 'utils/base/BaseStore';

import CommonStore from 'modules/pages/common/service/CommonStore';
import CommonRepository from 'modules/pages/common/service/CommonRepository';
import GridUtil from 'components/override/grid/utils/GridUtil';

import VersionSuperviseRepository from 'modules/pages/system/versionSupervise/service/VersionSuperviseRepository';
import Alert from 'components/override/alert/Alert';


class VersionSuperviseStore extends BaseStore {

    constructor() {
        super()
        this.setInitialState()
    }

    deviceGridApi = undefined;
    deviceList =  []

    versionGridApi = undefined;
    versionList = []

    //버전 추가
    @action.bound
    addClick = () => {
        
        let count = this.versionGridApi.getDisplayedRowCount();
        const selectedRows = GridUtil.getSelectedRowData(this.deviceGridApi);

        this.versionList.forEach(item => 
            {
                item.useYn = 'N'
            }
        )
        const verMt = {
            device: selectedRows.cdNm,
            version: '',
            useYn: 'Y',
            modDtm: '',
            remark: '',
            newItem: 'Y',
        };
        const result = this.versionGridApi.updateRowData({ add: [verMt], addIndex: count });
        GridUtil.setFocus(this.versionGridApi, result.add[0]);
    }

    @action.bound
    initDevice = async () => {
        this.deviceList = CommonStore.codeObject["SYS009"];
    }

    @action.bound
    setDeviceGridApi = async (gridApi) => {

        this.deviceGridApi = gridApi;

        const columnDefs = [
            {headerName: "디바이스", field: "cdNm", width: 100, customCellClass: 'cell_align_center'}
        ];
        this.deviceGridApi.setColumnDefs(columnDefs);
        this.deviceGridApi.setRowData(this.deviceList);
    }

    @action.bound
    setVersionGridApi = async (gridApi) => {

        this.versionGridApi = gridApi;

        const selectOption = GridUtil.convertSelectMap(toJS(CommonStore.codeObject['SYS001']));

        const columnDefs = [
            {headerName: "디바이스", field: "device"
              , editable: function (params) {
                    if (params.node.data.newItem == 'Y') {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
              , width: 100, customCellClass: 'cell_align_center'
            }
            ,{headerName: "버전", field: "version"
              , editable: function (params) {
                    if (params.node.data.newItem == 'Y') {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
              , width: 100, customCellClass: 'cell_align_center'
            }
            ,{
                headerName: "사용여부"
                , field: "useYn"
                , width: 80
                , cellClass: 'cell_align_center'
                , cellEditor: "agSelectCellEditor"
                , cellEditorParams: { values: GridUtil.extractValues(selectOption) }
                , cellClass: 'cell_align_center'
            }
            ,{headerName: "배포일", field: "modDtm", editable: false, width: 150, customCellClass: 'cell_align_center'}
            ,{headerName: "비고", field: "remark"
              , editable: function (params) {
                if (params.node.data.newItem == 'Y') {
                    return true;
                }
                else {
                    return false;
                }
              }
              , width: 350, customCellClass: 'cell_align_center'
            }
        ];
        this.versionGridApi.setColumnDefs(columnDefs);
        this.versionGridApi.setRowData(this.versionList);
    }

    @action.bound
    handleDeviceChanged = async () => {
        const selectedRows = GridUtil.getSelectedRowData(this.deviceGridApi);

        const { data, status } = await VersionSuperviseRepository.getVer(selectedRows.cdNm);

        if(status == 200) {
            this.versionList = data.data;
            this.versionGridApi.setRowData(this.versionList);
        }
    }

    @action.bound
    saveClick = async () => {

        const saveArr = GridUtil.getGridSaveArray(this.versionGridApi);

        if (saveArr.length == 0) {
            Alert.meg('저장할 데이터가 없습니다.');
            return;
        }

        const params = 
        {
             'device' : saveArr[0].device
            ,'version': saveArr[0].version
            ,'useYn'  : saveArr[0].useYn
            ,'remark' : saveArr[0].remark 
        }

        const param = JSON.stringify({ param: params});

        const { data, status } = await VersionSuperviseRepository.saveVersion(param);

        if (status == 200) {
            this.handleDeviceChanged()
            Alert.meg("저장되었습니다.");
        }
    }
 
}

export default new VersionSuperviseStore();