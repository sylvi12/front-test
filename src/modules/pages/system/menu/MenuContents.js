import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setGridApi: stores.menuMtStore.setGridApi,
    menuList: stores.menuMtStore.menuList,
    getForm: stores.menuMtStore.getForm
}))

class MenuContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setGridApi, menuList, getForm } = this.props;

        return (
            <SGrid
                grid={'menuGrid'}
                gridApiCallBack={setGridApi}
                rowData={menuList}
                onCellClicked={getForm}
                cellReadOnlyColor={true}
            />
        );
    }
}
export default MenuContents;