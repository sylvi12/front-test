import React, { Component } from 'react';
import { inject } from "mobx-react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import MenuSearchItem from "modules/pages/system/menu/MenuSearchItem";
import MenuMiddleItem from "modules/pages/system/menu/MenuMiddleItem";
import MenuContents from "modules/pages/system/menu/MenuContents";
import MenuFormMiddleItem from 'modules/pages/system/menu/MenuFormMiddleItem';
import MenuFormContents from 'modules/pages/system/menu/MenuFormContents';

import MenuFormModal from 'modules/pages/system/menu/menuFormModal/MenuFormModal';
import SModal from 'components/override/modal/SModal';

@inject(stores => ({
    formAddModalOpen:stores.menuMtStore.formAddModalOpen,
    formAddModalClose:stores.menuMtStore.formAddModalClose
}))


class Menu extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {   formAddModalOpen
                , formAddModalClose } = this.props; 
                
        return (
            <React.Fragment>
                <div style={{ width: '100%', height: '50%'}}>
                    <SearchTemplate searchItem={<MenuSearchItem />} />
                    <ContentsMiddleTemplate contentSubTitle={'메뉴 정보'} middleItem={<MenuMiddleItem />} />
                    <ContentsTemplate id='menu' contentItem={<MenuContents />} />
                </div>
                <div style={{ width: '100%', height: '50%'}}>
                    <ContentsMiddleTemplate contentSubTitle={'프로그램 정보'} middleItem={<MenuFormMiddleItem />} />
                    <ContentsTemplate id='menuForm' contentItem={<MenuFormContents />} />
                </div>


                <SModal
                    formId={"system_menu_p"}
                    id="formAdd"
                    isOpen={formAddModalOpen}
                    onRequestClose={formAddModalClose}
                    contentLabel="프로그램 추가"
                    width={500}
                    height={550}
                    contents={<MenuFormModal />}
                />


            </React.Fragment>
        )
    }
}

export default Menu;