import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setGridApiForm: stores.menuMtStore.setGridApiForm,
    formList: stores.menuMtStore.formList
}))

class MenuFormContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setGridApiForm, formList } = this.props;

        return (
            <SGrid
                grid={'formGrid'}
                gridApiCallBack={setGridApiForm}
                rowData={formList}
                //editable={false}
            />
        );
    }
}
export default MenuFormContents;