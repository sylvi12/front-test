import { observable, action } from 'mobx';

import Alert from 'components/override/alert/Alert'
import GridUtil from 'components/override/grid/utils/GridUtil'

import BaseStore from 'utils/base/BaseStore'
import MenuMtRepository from 'modules/pages/system/menu/service/MenuMtRepository';

class MenuMtStore extends BaseStore {

    constructor() {
        super()
        this.setInitialState();
    }

    //메뉴 변수
    @observable menuId_s = '';

    menuList = [];

    menuGridApi = undefined;

    // 프로그램 변수
    formList = [];

    formGridApi = undefined;

    // 프로그램 모달 변수
    @observable formId_s = '';
    @observable formAddModalOpen = false;
    formModalList = [];
    formModalGridApi = undefined;
    formType = 'WEB';


    @action.bound
    handleChange = (data) => {
        this[data.id] = data.value;
    }

    @action.bound
    setGridApi = async (gridApi) => {

        this.menuGridApi = gridApi;

        const columnDefs = [
            {
                headerName: "메뉴ID"
                , field: "menuId"
                , width: 80
                , editable: function (params) {
                    if (params.node.data.newItem == 'Y') {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            },
            , { headerName: "메뉴명", field: "menuNm", width: 150 }
            , { headerName: "상위ID", field: "parentMenuId", width: 80 }
            , { headerName: "프로그램 ID", field: "formId", width: 200 }
            , { headerName: "파라미터", field: "param", width: 100 }
            , { headerName: "깊이", field: "depthOpen", hide: true }
            , { headerName: "순서", field: "sort", width: 80, type: "numericColumn", valueFormatter: GridUtil.numberFormatter }
            , { headerName: "아이콘명", field: "iconNm", width: 100 }
            , { headerName: "하위여부", field: "childYn", width: 100, cellClass: 'cell_align_center' }
            , { headerName: "메뉴여부", field: "displayYn", width: 100, cellClass: 'cell_align_center' }
            , { headerName: "HELP URL", field: "helpId", width: 150 }
            , { headerName: "비고", field: "menuDesc", width: 300 }
        ];

        this.menuGridApi.setColumnDefs(columnDefs);
    }

    // 조회
    @action.bound
    handleSearchClick = async () => {

        const submitdata = {
            "menuId": this.menuId_s
            , "menuNm": this.menuId_s
        }

        const { data, status } = await MenuMtRepository.getMenuMts(submitdata);

        if (status == 200) {
            this.menuList = data.data;
            this.menuGridApi.setRowData(this.menuList);
            this.formGridApi.setRowData([]);
        }
    }

    // 저장
    @action.bound
    handleSaveClick = async () => {

        const saveArr = GridUtil.getGridSaveArray(this.menuGridApi);

        if (saveArr.length == 0) {
            Alert.meg('저장할 데이터가 없습니다.');
            return;
        }

        const searchData = {
            "menuId": this.menuId_s
            , "menuNm": this.menuId_s
        }

        const params = JSON.stringify({ param1: saveArr, param2: searchData });

        const { data, status } = await MenuMtRepository.saveMenuMts(params);

        if (status == 200) {
            this.menuList = data.data;
            this.menuGridApi.setRowData(this.menuList);

            Alert.meg("저장되었습니다.");
        }
    }

    // 추가
    @action.bound
    handleAddClick = () => {

        let lastindex = this.menuGridApi.getLastDisplayedRow();

        const menuMt = {
            menuId: ''
            , menuNm: ''
            , parentMenuId: ''
            , formId: ''
            , param: ''
            , depthOpen: 'none'
            , sort: ''
            , iconNm: ''
            , childYn: ''
            , displayYn: ''
            , helpId: ''
            , menuDesc: ''
            , newItem: 'Y'
        };
        const result = this.menuGridApi.updateRowData({ add: [menuMt], addIndex: lastindex + 1 });
        GridUtil.setFocus(this.menuGridApi, result.add[0]);
    }

    // 삭제
    @action.bound
    handleDeleteClick = async () => {

        const selectedRows = this.menuGridApi.getSelectedRows();

        if (selectedRows.length == 0) {
            Alert.meg('삭제할 항목을 선택해 주세요.');
            return;
        }

        const subData = {
            "menuId": selectedRows[0].menuId
            , "searchMenuId": this.menuId_s
        }

        const value = await Alert.confirm("삭제하시겠습니끼?").then(result => { return result; });
        if (value) {
            const { data, status } = await MenuMtRepository.deleteMenuMts(subData);

            if (status == 200) {
                this.menuList = data.data;
                this.menuGridApi.setRowData(this.menuList);
                Alert.meg("삭제되었습니다.");
            }
        }
        else {
            return;
        }
    }


    /* ------------------- 프로그램 영역 ----------------------- */

    //메뉴 Row 클릭 이벤트
    @action.bound
    getForm = async () => {

        const selectedRow = GridUtil.getSelectedRowData(this.menuGridApi);

        const searchData = {
            "menuId": selectedRow.menuId
        }

        const { data, status } = await MenuMtRepository.getForm(searchData);

        if (status == 200) {

            this.formList = data.data;
            this.formGridApi.setRowData(this.formList);

        }
    }

    //프로그램 추가
    @action.bound
    formAddClick = async () => {

        const selectedRow = GridUtil.getSelectedRowData(this.menuGridApi);
        if (!selectedRow || selectedRow == '' || selectedRow == undefined) {
            Alert.meg("선택된 메뉴가 없습니다.");
            return;
        }

        this.formModalList = [];
        this.formAddModalOpen = true;
        this.modalSearchClick();
    }

    //프로그램 저장
    @action.bound
    fromSaveClick = async () => {
        // const saveArr = GridUtil.getGridSaveArray(this.formGridApi);
        const selectedRow = GridUtil.getSelectedRowData(this.menuGridApi);

        if (!selectedRow || selectedRow == '' || selectedRow == undefined) {
            Alert.meg("선택된 메뉴가 없습니다.");
            return;
        }

        let saveArr = [];

        this.formGridApi.forEachNode(rowNode => {
            saveArr.push(rowNode.data);
        })

        if (saveArr.length == 0) {
            Alert.meg('저장할 데이터가 없습니다.');
            return;
        }

        let cnt = 0;
        for (let i = 0; i < saveArr.length; i++) {
            if (saveArr[i].mainYn == 'Y') {
                cnt++
                if (cnt > 1) {
                    Alert.meg("메인여부는 하나이상 체크할 수 없습니다.")
                    return;
                }
            }
        }

        const saveData = {
            "menuId": selectedRow.menuId
        }

        const params = JSON.stringify({ param1: saveArr, param2: saveData });
        const value = await Alert.confirm("저장하시겠습니까?").then(result => { return result; });
        if (value) {
            const { data, status } = await MenuMtRepository.saveForms(params);

            if (status == 200) {

                this.formList = data.data;
                this.formGridApi.setRowData(this.formList);

                Alert.meg("저장되었습니다.");
                // Alert.meg(data.msg);
            }
        } else return;
    }

    //프로그램 삭제
    @action.bound
    fromDeleteClick = async () => {
        const menuSelectedRow = GridUtil.getSelectedRowData(this.menuGridApi);
        const formSelectedRow = GridUtil.getSelectedRowData(this.formGridApi);

        if (!menuSelectedRow || menuSelectedRow == '' || menuSelectedRow == undefined) {
            Alert.meg("선택된 메뉴가 없습니다.");
            return;
        }

        const saveData = {
            "menuId": menuSelectedRow.menuId
            , "formId": formSelectedRow.formId
        }

        let result = this.formList.filter(obj => obj.formId == formSelectedRow.formId);

        if (result.length < 1) {

            this.formGridApi.updateRowData({ remove: [formSelectedRow] });

        } else {
            const value = await Alert.confirm("데이터가 있는 ID입니다. 삭제하시겠습니끼?").then(result => { return result; });
            if (value) {
                const { data, status } = await MenuMtRepository.deleteForm(saveData);

                if (status == 200) {
                    this.formList = data.data;
                    this.formGridApi.setRowData(this.formList);
                    // Alert.meg(data.msg);
                    Alert.meg("삭제되었습니다..");
                }
            }
            else {
                return;
            }
        }
    }


    //프로그램 그리드
    @action.bound
    setGridApiForm = async (gridApiForm) => {

        this.formGridApi = gridApiForm;

        const columnDefs = [
            {
                headerName: "프로그램 ID"
                , field: "formId"
                , width: 200
                , cellClass: 'cell_align_center'
                , editable: false

            },
            {
                headerName: "프로그램명"
                , field: "formNm"
                , width: 150
                , cellClass: 'cell_align_left'
                , editable: false
            },
            {
                headerName: "메인여부"
                , field: "mainYn"
                , width: 80
                , cellRenderer: 'rowCellCheckbox'
                , cellClass: 'cell_align_center'
                , editable: false
            }

        ];

        this.formGridApi.setColumnDefs(columnDefs);
    }


    /* -------------------------- 프로그램 모달 영역 ----------------------- */

    //프로그램 모달 닫기
    @action.bound
    formAddModalClose = () => {
        this.formAddModalOpen = false;
    }
    //조회
    @action.bound
    modalSearchClick = async () => {

        const searchData = {
            "formId": this.formId_s
            , "formNm": this.formId_s
            , "formType": this.formType
        }

        const modalList = [];
        this.formGridApi.forEachNode(rowNode => {
            modalList.push(rowNode.data);
        })


        const { data, status } = await MenuMtRepository.getmodalFrom(searchData);

        if (status == 200) {
            const list = [];
            data.data.forEach(item => {
                if (!modalList.some(x => x.formId == item.formId)) {
                    list.push(item);
                }
            });

            this.formModalGridApi.setRowData(list);

        }

    }

    //선택 클릭
    @action.bound
    choiceClick = () => {
        const selectedRows = GridUtil.getGridCheckArray(this.formModalGridApi);

        this.formGridApi.updateRowData({ add: selectedRows });


        this.formAddModalClose();

    }

    //모달의 프로그램 그리드
    @action.bound
    setformModalGridApi = async (gridApiFormModal) => {

        this.formModalGridApi = gridApiFormModal;

        const columnDefs = [
            {
                headerName: ""
                , field: "check"
                , width: 29
                , checkboxSelection: true
                , headerCheckboxSelection: true
            },
            {
                headerName: "프로그램ID"
                , field: "formId"
                , width: 200
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "프로그램명"
                , field: "formNm"
                , width: 150
                , cellClass: 'cell_align_left'
            }

        ];

        this.formModalGridApi.setColumnDefs(columnDefs);
    }
}

export default new MenuMtStore();