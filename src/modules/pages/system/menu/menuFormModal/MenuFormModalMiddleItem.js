import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    choiceClick: stores.menuMtStore.choiceClick
}))

class MenuFormModalMiddleItem extends Component {

    render() {

        const { choiceClick } = this.props;
        return (
            <React.Fragment>
                <SButton
                    buttonName={'선택'}
                    type={'btnChoice'}
                    onClick={choiceClick}
                />
                
            </React.Fragment>
        )
    }
}

export default MenuFormModalMiddleItem;