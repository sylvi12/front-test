import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setformModalGridApi: stores.menuMtStore.setformModalGridApi,
    formModalList: stores.menuMtStore.formModalList
}))

class MenuFormModalContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setformModalGridApi, formModalList } = this.props;

        return (
            <SGrid
                grid={'formModalGrid'}
                gridApiCallBack={ setformModalGridApi}
                rowData={formModalList}
                suppressRowClickSelection={true}
                
                editable={false}
            />
        );
    }
}
export default MenuFormModalContents;