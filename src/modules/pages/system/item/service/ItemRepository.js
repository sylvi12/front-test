import API from "components/override/api/API";

class ItemRepository {

    URL = "api/system/item";

    getItems(params) {
        return API.request.get(encodeURI(`${this.URL}/search?glclass=${params.glclass}&searchText=${params.searchText}`))
    }

    setYnState(params) {
        return API.request.post(`${this.URL}/setynstate`, params)
    }

    getUom(params) {
        return API.request.get(encodeURI(`${this.URL}/getuom?itemId=${params.itemId}`))
    }
    getBom(params) {
        return API.request.get(encodeURI(`${this.URL}/getbom?itemId=${params.itemId}&buId=${params.buId}`))
    }

    //UOM 추가,변경
    updateUomData(params) {
        return API.request.post(`${this.URL}/updateuomdata`, params)
    }

    //UOM 삭제
    deleteUomData(params) {
        return API.request.post(`${this.URL}/deleteuomdata`, params)
    }

    getConfValue(param) {
        console.log('dddd')
    }
}
export default new ItemRepository();