import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setGridApi: stores.itemStore.setGridApi
    , itemList: stores.itemStore.itemList
    , getTheOthers: stores.itemStore.getTheOthers
    , setChangeYn: stores.itemStore.setChangeYn
}))

class ItemContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setGridApi, itemList, getTheOthers, setChangeYn } = this.props;

        return (
            <SGrid
                grid={'itemGrid'}
                gridApiCallBack={setGridApi}
                rowData={itemList}
                onSelectionChanged={getTheOthers}
                cellReadOnlyColor={true}
                editable={false}
                onCellValueChanged={setChangeYn}
            />
        );
    }
}
export default ItemContents;