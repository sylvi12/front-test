import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SButton from 'components/atoms/button/SButton';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';

@inject(stores => ({
    glclass: stores.itemStore.glclass,
    searchText: stores.itemStore.searchText,
    handleChange: stores.itemStore.handleChange,
    handleSearchClick: stores.itemStore.handleSearchClick
}))

class ItemSearchItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { glclass, searchText, handleChange, handleSearchClick } = this.props;

        return (

            <React.Fragment>
                <SSelectBox
                    title={"GLCLASS"}
                    id={"glclass"}
                    value={glclass}
                    codeGroup={"MT002"}
                    onChange={handleChange}
                />
                <SInput
                    title={"제품명/코드"}
                    id={"searchText"}
                    value={searchText}
                    onChange={handleChange}
                    onEnterKeyDown={handleSearchClick}
                />
                <div className='search_item_btn'>
                    <SButton
                        buttonName={'조회'}
                        type={'btnSearch'}
                        onClick={handleSearchClick} />
                </div>
            </React.Fragment>
        )
    }
}

export default ItemSearchItem;