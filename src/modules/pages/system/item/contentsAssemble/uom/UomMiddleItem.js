import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    btnInsertUom: stores.itemStore.btnInsertUom
    , btnAddUom: stores.itemStore.btnAddUom
    , btnDeleteUom: stores.itemStore.btnDeleteUom
}))

class UomMiddleItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {
            btnInsertUom
            , btnAddUom
            , btnDeleteUom
        } = this.props;

        return (
            <React.Fragment>
                <SButton
                    className="btn_red"
                    buttonName={'저장'}
                    type={'btnInsertUom'}
                    onClick={btnInsertUom}
                />
                <SButton
                    className="btn_red"
                    buttonName={'추가'}
                    type={'btnAddUom'}
                    onClick={btnAddUom}
                />
                <SButton
                    className="btn_red"
                    buttonName={'삭제'}
                    type={'btnDeleteUom'}
                    onClick={btnDeleteUom}
                />
            </React.Fragment>
        )
    }
}

export default UomMiddleItem;