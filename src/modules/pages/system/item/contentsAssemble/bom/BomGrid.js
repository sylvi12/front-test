import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setBomGridApi: stores.itemStore.setBomGridApi
    , bomList: stores.itemStore.bomList
}))

class BomGrid extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setBomGridApi, bomList } = this.props;

        return (
            <SGrid
                grid={'bomGrid'}
                gridApiCallBack={setBomGridApi}
                rowData={bomList}
                editable={false}
            />
        );
    }
}
export default BomGrid;