import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton'

// @inject(stores => ({
//     onClickAdd: stores.userStore.onClickAdd,
//     onClickEdit: stores.userStore.onClickEdit,
//     onClickDelete: stores.userStore.onClickDelete,
//     menuStore: stores.menuStore
// }))
class TestMiddleItem extends Component {
    render() {
        return (
            <React.Fragment>
                <SButton buttonName={'추가'} type='insert' />
                <SButton buttonName={'수정'} type='update'/>
                <SButton buttonName={'삭제'} type='delete'/>
            </React.Fragment>
        );
    }
}

export default TestMiddleItem;
