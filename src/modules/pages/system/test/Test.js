import React, { Component } from 'react';
import { inject } from 'mobx-react';

import STitle from 'components/atoms/label/STitle'
import SearchTemplate from 'components/template/SearchTemplate'
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate'
import ContentsTemplate from 'components/template/ContentsTemplate'

import TestSearchItem from 'modules/pages/system/test/TestSearchItem'
import TestMiddleItem from 'modules/pages/system/test/TestMiddleItem'
import TestContents from 'modules/pages/system/test/TestContents'

class Test extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { } = this.props;

        return (
            <React.Fragment>
                {/* <STitle title={'사용자 관리'} /> */}

                <SearchTemplate searchItem={<TestSearchItem />} />
                <ContentsMiddleTemplate middleItem={<TestMiddleItem />} contentSubTitle={'사용자 관리'}/>
                <ContentsTemplate id='user' contentItem={<TestContents />} />

            </React.Fragment>
        )
    }
}

export default Test;