// layouts
import AuthStore from 'modules/layouts/main/service/AuthStore'
import MainStore from 'modules/layouts/main/service/MainStore'

// pages
import CommonStore from 'modules/pages/common/service/CommonStore'
import LoginStore from 'modules/pages/login/service/LoginStore'

// pages-inout
import DisuseStore from 'modules/pages/inout/disuse/service/DisuseStore'
import EtcStore from 'modules/pages/inout/etc/service/EtcStore'
import InoutStore from 'modules/pages/inout/inout/service/InoutStore'
import EtcInStore from 'modules/pages/inout/etcIn/service/EtcInStore'
import BarcodeTrackingStore from 'modules/pages/inout/barcodeTracking/service/BarcodeTrackingStore'
// import LocationInoutStore from 'modules/pages/inout/locationInout/service/LocationInoutStore'


// pages-inventory
import BarcodeHistoryStore from 'modules/pages/inventory/barcodeHistory/service/BarcodeHistoryStore';
import CurrentStore from 'modules/pages/inventory/current/service/CurrentStore';
import LocationMoveStore from 'modules/pages/inventory/locationMove/service/LocationMoveStore';
import PositionStore from 'modules/pages/inventory/position/service/PositionStore';
import QualityStatusStore from 'modules/pages/inventory/qualityStatus/service/QualityStatusStore';
import CurrentByDateStore from 'modules/pages/inventory/currentByDate/service/CurrentByDateStore';
import MaterialLedgerStore from 'modules/pages/inventory/materialLedger/service/MaterialLedgerStore';
import CompareStore from 'modules/pages/inventory/compare/service/CompareStore';

// pages-order
import OutputStore from 'modules/pages/order/output/service/OutputStore'
import OutputListStore from 'modules/pages/order/outputList/service/OutputListStore'
import WorkorderStore from 'modules/pages/order/workorder/service/WorkorderStore'
import WorkorderErpStore from 'modules/pages/order/workordererp/service/WorkorderErpStore'
import WorkorderDailyreportStore from 'modules/pages/order/workorderDailyreport/service/WorkorderDailyreportStore'
import ProductionByDateStore from 'modules/pages/order/productionByDate/service/ProductionByDateStore'
import ItemChangeErpStore from 'modules/pages/order/itemChangeErp/service/ItemChangeErpStore'
import LocationChangeErpStore from 'modules/pages/order/locationChangeErp/service/LocationChangeErpStore'



// pages-system
import CodeStore from 'modules/pages/system/code/service/CodeStore'
import FormStore from 'modules/pages/system/form/service/FormStore'
import GrpStore from 'modules/pages/system/grp/service/GrpStore'
import ItemStore from 'modules/pages/system/item/service/ItemStore'
import MenuMtStore from 'modules/pages/system/menu/service/MenuMtStore'
import UserStore from 'modules/pages/system/user/service/UserStore'
import VersionSuperviseStore from 'modules/pages/system/versionSupervise/service/VersionSuperviseStore'

// pages-standard
import BuStore from 'modules/pages/standard/bu/service/BuStore'
import CategoryStore from 'modules/pages/standard/category/service/CategoryStore'
import LineStore from 'modules/pages/standard/line/service/LineStore'
import ItemUomExtendStore from 'modules/pages/standard/itemUomExtend/service/ItemUomExtendStore'
import ItemChangeMappingStore from 'modules/pages/standard/itemChangeMapping/service/ItemChangeMappingStore'
import LocationStore from 'modules/pages/standard/location/service/LocationStore'

// test
import TestStore from 'modules/pages/system/test/service/TestStore'

// etc
import StorageStore from 'modules/pages/system/storage/service/StorageStore'

class RootStore {

    constructor() {

        // layouts
        this.authStore = AuthStore
        this.mainStore = MainStore

        // pages
        this.commonStore = CommonStore
        this.loginStore = LoginStore

        // pages-inout
        this.disuseStore = DisuseStore
        this.etcStore = EtcStore
        this.inoutStore = InoutStore
        this.etcInStore = EtcInStore
        this.barcodeTrackingStore = BarcodeTrackingStore
        // this.locationInoutStore = LocationInoutStore
        
        // pages-inventory
        this.barcodeHistoryStore = BarcodeHistoryStore;
        this.currentStore = CurrentStore;
        this.locationMoveStore = LocationMoveStore;
        this.positionStore = PositionStore;
        this.qualityStatusStore = QualityStatusStore;
        this.currentByDateStore = CurrentByDateStore;
        this.materialLedgerStore = MaterialLedgerStore;
        this.compareStore = CompareStore;

        // pages-order
        this.outputStore = OutputStore
        this.outputListStore = OutputListStore
        this.workorderStore = WorkorderStore
        this.workorderErpStore = WorkorderErpStore
        this.workorderDailyreportStore = WorkorderDailyreportStore
        this.productionByDateStore = ProductionByDateStore
        this.itemChangeErpStore = ItemChangeErpStore
        this.locationChangeErpStore = LocationChangeErpStore

        // pages-system
        this.codeStore = CodeStore
        this.formStore = FormStore
        this.grpStore = GrpStore
        this.itemStore = ItemStore
        this.menuMtStore = MenuMtStore
        this.userStore = UserStore
        this.versionSuperviseStore = VersionSuperviseStore

        // pages-standard
        this.buStore = BuStore
        this.categoryStore = CategoryStore
        this.lineStore = LineStore
        this.itemUomExtendStore = ItemUomExtendStore
        this.itemChangeMappingStore = ItemChangeMappingStore
        this.locationStore = LocationStore

        // test
        this.testStore = TestStore

        // etc
        this.storageStore = StorageStore
    }

    reloadStore = (formClass) => {

        if (!formClass) {
            return
        }

        const className = formClass.substring(0, 1).toLowerCase() + formClass.substring(1, formClass.length) + "Store"

        try {
            this[className].reset()
        }
        catch (exception) {
            console.warn(`Store 초기화 실패: ${className}`)
            console.warn("가이드 확인 후 수정 바랍니다.")
        }
    }
}

//Singleton
const instance = new RootStore()
export default instance