import { observable, action } from 'mobx';
import moment from 'moment';
import 'moment/locale/ko';

import BaseStore from 'utils/base/BaseStore';
import CommonStore from 'modules/pages/common/service/CommonStore';
import GridUtil from 'components/override/grid/utils/GridUtil';

import CurrentByDateRepository from 'modules/pages/inventory/currentByDate/service/CurrentByDateRepository';
import Alert from 'components/override/alert/Alert';

class CurrentByDateStore extends BaseStore {

    constructor() {
        super()
        this.setInitialState();
    }

    //검색 조건 변수
    @observable buId = CommonStore.buId;
    @observable buList = CommonStore.buList;
    @observable invenFromDt = moment().add(-7, 'days').format('YYYYMMDD');
    @observable invenToDt = moment().format('YYYYMMDD');
    @observable itemCdNm = '';
    @observable lotQuality = '';
    @observable invenCheck = 'Y';
    @observable uomGubn = 10;

    @observable alphaCheck = false;

    //그리드 변수
    @observable currentByDateList = []

    //데이터 고정 변수
    togle = false;

    //공장 불러오기
    @action.bound
    getBu = async () => {
        this.buId = CommonStore.buId;
        this.alphaCheck = true;
    }
    @action.bound
    handleChange = (data) => {
        this[data.id] = data.value;
    }

    //조회
    @action.bound
    handleSearchClick = async () => {

        const day = moment(this.invenToDt, 'YYYYMMDD').diff(this.invenFromDt, 'day');

        if (day > 30) {
            Alert.meg("31일을 초과했습니다.");
            return;
        }

        const searchData = {
            "buId": this.buId
            , "invenFromDt": this.invenFromDt
            , "invenToDt": this.invenToDt
            , "invenCheck": this.invenCheck
            , "itemCdNm": this.itemCdNm
            , "lotQuality": this.lotQuality
            , "uomGubn": this.uomGubn
        }

        const { data, status } = await CurrentByDateRepository.getCurrentByDate(searchData);

        if (status == 200) {

            this.currentByDateList = data.data;
            this.generateColumns();
            this.gridApi.setRowData(this.currentByDateList);

            this.togle = true;

        } else {
            Alert.meg(data.msg);
        }

    }

    // 공장 선택시 research
    @action.bound
    buSelectionChanged = async () => {
        this.handleSearchClick();

    }

    @action.bound
    setGridApi(gridApi) {
        this.gridApi = gridApi;

        if (this.togle == true) {
            this.generateColumns();
        }
    }

    generateColumns = async () => {

        const columnDefs = [
            {
                headerName: "제품코드"
                , field: "itemCd"
                , width: 100
                , cellClass: 'cell_align_center'
                , pinned: "left"
            },
            {
                headerName: "제품"
                , field: "itemNm"
                , width: 150
                , cellClass: 'cell_align_left'
                , pinned: "left"
            },
            {
                headerName: "단위"
                , field: "uom"
                , width: 50
                , cellClass: 'cell_align_center'
                , pinned: "left"
            }
        ];
        let date = [];

        const day = moment(this.invenToDt, 'YYYYMMDD').diff(this.invenFromDt, 'day');

        for (let i = 0; i < day + 1; i++) {

            let addDay = moment(this.invenFromDt).add(i, 'days').format('YYYY-MM-DD');
            date.push(addDay);
        }

        for (let k = 1; k < date.length + 1; k++) {
            let days = {
                headerName: `${date[k - 1]}`
                , field: "day" + k
                , width: 100
                , cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            }
            columnDefs.push(days);
        }

        this.gridApi.setColumnDefs(columnDefs);

    }
}

export default new CurrentByDateStore();