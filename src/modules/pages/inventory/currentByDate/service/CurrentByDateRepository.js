import API from "components/override/api/API";

class CurrentByDateRepository {
    
    URL = "/api/inventory/currentByDate";
    
    /* 조회 */
    getCurrentByDate(param) {
        return API.request.get(encodeURI(`${this.URL}/getcurrentbydate?buId=${param.buId}&invenFromDt=${param.invenFromDt}&invenToDt=${param.invenToDt}&invenCheck=${param.invenCheck}&itemCdNm=${param.itemCdNm}&lotQuality=${param.lotQuality}&uomGubn=${param.uomGubn}`))
    }

}

export default new CurrentByDateRepository();