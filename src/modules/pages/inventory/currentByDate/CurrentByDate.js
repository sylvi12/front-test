import React, { Component } from "react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import CurrentByDateSearchItem from "modules/pages/inventory/currentByDate/CurrentByDateSearchItem";
import CurrentByDateContents from "modules/pages/inventory/currentByDate/CurrentByDateContents";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";

class CurrentByDate extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return(
            <React.Fragment>
                <SearchTemplate searchItem={<CurrentByDateSearchItem />} />
                <ContentsMiddleTemplate contentSubTitle={'일자별재고 현황'} />
                <ContentsTemplate id="currentByDate" contentItem={<CurrentByDateContents />} />  
            </React.Fragment>
        );
    }
}

export default CurrentByDate;