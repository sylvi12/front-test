import React, { Component } from 'react';
import { inject } from 'mobx-react';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SButton from 'components/atoms/button/SButton';
import SInput from 'components/atoms/input/SInput';
import SCheckBox from 'components/atoms/checkbox/SCheckBox';

import CurrentByDateStore from 'modules/pages/inventory/currentByDate/service/CurrentByDateStore';
import SBuSelectBox from 'components/override/business/SBuSelectBox';

@inject(stores => ({
      handleChange: stores.currentByDateStore.handleChange
    , handleSearchClick: stores.currentByDateStore.handleSearchClick
    , buId: stores.currentByDateStore.buId
    , buList: stores.currentByDateStore.buList
    , invenFromDt: stores.currentByDateStore.invenFromDt
    , invenToDt: stores.currentByDateStore.invenToDt
    , buSelectionChanged: stores.currentByDateStore.buSelectionChanged
    , itemCdNm: stores.currentByDateStore.itemCdNm
    , invenCheck: stores.currentByDateStore.invenCheck
    , lotQuality: stores.currentByDateStore.lotQuality
    , uomGubn: stores.currentByDateStore.uomGubn
    
}))

class CurrentByDateSearchItem extends Component {
    constructor(props) {
        super(props);
        
    }

    async componentWillMount() {
        if (CurrentByDateStore.alphaCheck == false) {
            await CurrentByDateStore.getBu();
        }
    }

    render() {

        const {   buId
                , buList
                , invenFromDt
                , invenToDt
                , handleChange
                , handleSearchClick
                , buSelectionChanged 
                , itemCdNm 
                , uomGubn 
                , lotQuality
                , invenCheck} = this.props;

        return(
            <React.Fragment>
                <div className='search_item_btn'>
                    <SButton
                        buttonName={'조회'}
                        type={'btnSearch'}
                        onClick={handleSearchClick}
                    />
                </div>
                <div style={{ display: "inline_block" }}>
                    <div className='search_line'>
                        <SBuSelectBox
                            title={"공장"}
                            id={"buId"}
                            value={buId}
                            onChange={handleChange}
                            //onSelectionChange={buSelectionChanged}
                        />

                        <SDatePicker
                        title={"기간"}
                        id={"invenFromDt"}
                        value={invenFromDt}
                        onChange={handleChange}

                        />
                        <SDatePicker
                            title={"~"}
                            id={"invenToDt"}
                            value={invenToDt}
                            onChange={handleChange}
                        />

                        <SCheckBox
                            title={"실재고여부"}
                            id={"invenCheck"}
                            value={invenCheck}
                            //checked={}
                            onChange={handleChange}
                        />

                        <SInput
                            title={"제품"}
                            id={"itemCdNm"}
                            value={itemCdNm}
                            onChange={handleChange}
                            onEnterKeyDown={handleSearchClick}
                        />
                        
                        
                    </div>
                    <div className='search_line'>
                
                        

                        

                        <SSelectBox
                            title={"품질상태"}
                            id={"lotQuality"}
                            value={lotQuality}
                            codeGroup={"MFD003"}
                            onChange={handleChange}
                            addOption={"전체"}
                        />

                        <SSelectBox
                            title={"기준단위"}
                            id={"uomGubn"}
                            value={uomGubn}
                            codeGroup={"MFD015"}
                            onChange={handleChange}
                        />
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default CurrentByDateSearchItem;