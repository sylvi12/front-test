import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid';

@inject(stores => ({
    setGridApi: stores.currentByDateStore.setGridApi,
    currentByDateList: stores.currentByDateStore.currentByDateList
}))

class CurrentByDateContents extends Component {
    
    constructor(props) {
        super(props);
    }

    render() {

        const { setGridApi, currentByDateList } = this.props;

        return(
            <SGrid
                grid={'currentByDateGrid'}
                gridApiCallBack={setGridApi}
                rowData={currentByDateList}
                cellReadOnlyColor={true}
                editable={false}
            />
        );
    }
}
export default CurrentByDateContents;