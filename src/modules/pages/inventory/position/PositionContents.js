import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid';

@inject(stores => ({
    setGridApi: stores.positionStore.setGridApi
    , positionList: stores.positionStore.positionList
    , getBarcode: stores.positionStore.getBarcode
    , pinnedBottomRowData: stores.positionStore.pinnedBottomRowData
}))

class PostionContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setGridApi, positionList, getBarcode, pinnedBottomRowData } = this.props;

        return (
            <SGrid
                grid={'positionGrid'}
                gridApiCallBack={setGridApi}
                rowData={positionList}
                onSelectionChanged={getBarcode}
                cellReadOnlyColor={true}
                editable={false}
                pinnedBottomRowData={pinnedBottomRowData}
            />
        );
    }
}

export default PostionContents;