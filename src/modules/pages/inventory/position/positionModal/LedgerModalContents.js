import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setOrderLedgerGridApi: stores.positionStore.setOrderLedgerGridApi,
    orderLedgerDataList: stores.positionStore.orderLedgerDataList
}))

class LedgerModalContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setOrderLedgerGridApi, orderLedgerDataList } = this.props;

        return (
            <SGrid
                grid={'orderLedgerGrid'}
                gridApiCallBack={setOrderLedgerGridApi}
                rowData={orderLedgerDataList}
                //onSelectionChanged={getLedgerGroup}
                cellReadOnlyColor={true}
                editable={false}
            />
        );
    }
}
export default LedgerModalContents;