import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";
import SModal from "components/override/modal/SModal";

import LedgerModalSearchItem from "modules/pages/inventory/position/positionModal/LedgerModalSearchItem";
import LedgerModalMiddleItem from "modules/pages/inventory/position/positionModal/LedgerModalMiddleItem";
import LedgerModalContents from "modules/pages/inventory/position/positionModal/LedgerModalContents";

import LedgerChgModal from "modules/pages/inventory/position/positionModal/LedgerChgModal";
import LedgerHisModal from "modules/pages/inventory/position/positionModal/LedgerHisModal";

@inject(stores => ({
    ledgerApModModalIsOpen: stores.positionStore.ledgerApModModalIsOpen,
    ledgerApModHisModalIsOpen: stores.positionStore.ledgerApModHisModalIsOpen,

    closeledgerApModModal: stores.positionStore.closeledgerApModModal,
    closeledgerApModHisModal: stores.positionStore.closeledgerApModHisModal
}))

class LedgerModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {
            ledgerApModModalIsOpen,
            ledgerApModHisModalIsOpen,
            closeledgerApModModal,
            closeledgerApModHisModal
        } = this.props;

        return (
            <React.Fragment>
				<SearchTemplate searchItem={<LedgerModalSearchItem />} />
				<ContentsMiddleTemplate contentSubTitle={'바코드 수불내역'} middleItem={<LedgerModalMiddleItem />} />
				<ContentsTemplate id='ledger' contentItem={<LedgerModalContents />} />

                <SModal
                    formId={"inventory_ledger_update_p"}
                    id="btnLedgerUpdateForm"
                    isOpen={ledgerApModModalIsOpen}
                    onRequestClose={closeledgerApModModal}
                    contentLabel="수불적용일자 변경"
                    width={600}
                    height={350}
                    contents={<LedgerChgModal />}
                />

                 <SModal
                    formId={"inventory_ledger_update_p"}
                    id="btnLedgerUpdateHisForm"
                    isOpen={ledgerApModHisModalIsOpen}
                    onRequestClose={closeledgerApModHisModal}
                    contentLabel="수불적용일자 변경이력"
                    width={640}
                    height={350}
                    contents={<LedgerHisModal />}
                />
            </React.Fragment>
        );
    }
}

export default LedgerModal;