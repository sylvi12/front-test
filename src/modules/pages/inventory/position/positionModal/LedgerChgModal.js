import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SInput from 'components/atoms/input/SInput';

@inject(stores => ({
    tranjDt: stores.positionStore.tranjDt,
    tranjDtEdit: stores.positionStore.tranjDtEdit,
    tranjDtReason: stores.positionStore.tranjDtReason,
    handleChange: stores.positionStore.handleChange,
    ledgerChange: stores.positionStore.ledgerChange
}))

// 수불적용일자 변경 Popup
class LedgerChgModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { tranjDt, tranjDtEdit, tranjDtReason, ledgerChange, handleChange } = this.props;
        return (
            <React.Fragment>
                <table style={{ width: "100%" }}>
                    <tbody>
                    <tr>
                        <th>기존일자</th>
                        <td>
                            <SDatePicker
                                id={"tranjDt"}
                                value={tranjDt}
                                onChange={handleChange}
                                readOnly={true}
                            />
                        </td>
                    </tr>
                    <tr>
                        <th>변경일자</th>
                        <td>
                            <SDatePicker
                                id={"tranjDtEdit"}
                                value={tranjDtEdit}
                                onChange={handleChange}
                            />
                        </td>
                    </tr>
                    <tr>
                        <th>변경사유</th>
                        <td>
                            <SInput
                                id={"tranjDtReason"}
                                value={tranjDtReason}
                                onChange={handleChange}
                            />
                        </td>
                    </tr>
                </tbody>
                </table>
            <div>
                <SButton buttonName={'저장'} type={'btnLedgerUpdate'} onClick={ledgerChange} />
                {/* <SButton buttonName={"취소"} onClick={closeModal} type={"default"} /> */}
            </div>
            </React.Fragment >
        );
    }
}

export default LedgerChgModal;