import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid';


@inject(stores => ({
    setLedgerHisGridApi: stores.positionStore.setLedgerHisGridApi, 
    ledgerHisList: stores.positionStore.ledgerHisList
}))

// 수불적용일자 변경이력 Popup
class LedgerHisModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setLedgerHisGridApi, ledgerHisList } = this.props;
        return (
            <React.Fragment>
                <SGrid
                grid={'ledgerHisGrid'}
                gridApiCallBack={setLedgerHisGridApi}
                rowData={ledgerHisList}
                cellReadOnlyColor={true}
                editable={false}
                />
            </React.Fragment>
        );
    }
}

export default LedgerHisModal;