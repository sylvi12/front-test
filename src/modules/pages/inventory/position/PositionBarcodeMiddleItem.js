import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    openLedgerModal: stores.positionStore.openLedgerModal,
    openOrderDetailModal: stores.positionStore.openOrderDetailModal
}))

class PositionBarcodeMiddleItem extends Component {
    
    render() {

        const { openLedgerModal, openOrderDetailModal } = this.props;

        return    (
            <React.Fragment>
                <SButton 
                    className="btn_red"
                    buttonName={'수불내역'}
                    type={'btnLedger'}
                    onClick={openLedgerModal}
                />
                
                <SButton 
                    className="btn_red"
                    buttonName={'생산정보'}
                    type={'btnOrder'}
                    onClick={openOrderDetailModal}
                />
            </React.Fragment>
        )
    }
}

export default PositionBarcodeMiddleItem;