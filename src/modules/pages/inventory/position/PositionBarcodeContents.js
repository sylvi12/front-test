import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid';

@inject(stores => ({
    setBarcodeGridApi: stores.positionStore.setBarcodeGridApi
    , barcodeList: stores.positionStore.barcodeList
    , pinnedBottomRowBarcodeData: stores.positionStore.pinnedBottomRowBarcodeData
}))

class PositionBarcodeContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setBarcodeGridApi, barcodeList, pinnedBottomRowBarcodeData } = this.props;

        return (
            <SGrid
                grid={'barcodeGrid'}
                gridApiCallBack={setBarcodeGridApi}
                rowData={barcodeList}
                editable={false}
                pinnedBottomRowData={pinnedBottomRowBarcodeData}
            />
        )
    }
}

export default PositionBarcodeContents;