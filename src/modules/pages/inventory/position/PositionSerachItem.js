import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SButton from 'components/atoms/button/SButton';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SCheckBox from 'components/atoms/checkbox/SCheckBox';
import STreeSelectBox from 'components/override/tree/STreeSelectBox';
import SBuSelectBox from 'components/override/business/SBuSelectBox';

import PositionStore from 'modules/pages/inventory/position/service/PositionStore';

@inject(stores => ({
      buId: stores.positionStore.buId
    , invenDt: stores.positionStore.invenDt
    , subTotalId: stores.positionStore.subTotalId
    , invenCheck: stores.positionStore.invenCheck
    , categoryCd: stores.positionStore.categoryCd
    , categoryGroup: stores.positionStore.categoryGroup
    , itemCdNm: stores.positionStore.itemCdNm
    , locId: stores.positionStore.locId
    , locList: stores.positionStore.locList
    , lotQuality: stores.positionStore.lotQuality
    , handleChange: stores.positionStore.handleChange
    , handleSearchClick: stores.positionStore.handleSearchClick
    , dateSelectionChange: stores.positionStore.dateSelectionChange
    , buSelectionChanged: stores.positionStore.buSelectionChanged
    , uomGubn: stores.positionStore.uomGubn
    // , qtyCd: stores.positionStore.qtyCd
    , setItemCategory: stores.positionStore.setItemCategory
    , getLocation: stores.positionStore.getLocation
}))

class PostionSearchItem extends Component {

    constructor(props) {
        super(props);
    }
    
    async componentWillMount() {
        if (PositionStore.alphaCheck == false) {
            await this.props.getLocation();
            await this.props.setItemCategory();
            await PositionStore.getBu();
        }
    }

    render() {
        
        const subTotaloption = [
            {
                cd: "01",
                cdNm: "제품,일자"
            },
            {
                cd: "02",
                cdNm: "제품"
            }
        ];

        // const qtyGroup = [
        //     {
        //         qtyCd: '00'
        //         , qtyNm: '수량'
        //         , pqtyCd: null
        //         , level: 1
        //     },
        //     {
        //         qtyCd: '01'
        //         , qtyNm: 'PL'
        //         , pqtyCd: '00'
        //         , level: 2
        //     },
        //     {
        //         qtyCd: '02'
        //         , qtyNm: '생산'
        //         , pqtyCd: '00'
        //         , level: 2
        //     },
        //     {
        //         qtyCd: '03'
        //         , qtyNm: '재고'
        //         , pqtyCd: '00'
        //         , level: 2
        //     },
        //     {
        //         qtyCd: '04'
        //         , qtyNm: '가용'
        //         , pqtyCd: '00'
        //         , level: 2
        //     },
        //     {
        //         qtyCd: '05'
        //         , qtyNm: '홀드'
        //         , pqtyCd: '00'
        //         , level: 2
        //     }
        // ];

        const {   buId
                , invenDt
                , subTotalId
                , invenCheck
                , categoryCd
                , categoryGroup
                , itemCdNm
                , locId
                , locList
                , lotQuality
                , handleChange
                , handleSearchClick
                , dateSelectionChange
                , buSelectionChanged
                , uomGubn 
                // , qtyCd
            } = this.props;
        
        return (
            <React.Fragment>
                <div className='search_item_btn'>
                    <SButton
                        buttonName={'조회'}
                        type={'btnSearch'}
                        onClick={handleSearchClick}
                    />
                </div>
                <div style={{ display: "inline_block" }}>
                    <div className='search_line'>
                        <SBuSelectBox
                            title={"공장"}
                            id={"buId"}
                            value={buId}
                            onChange={handleChange}
                            onSelectionChange={buSelectionChanged}
                        />

                        <SDatePicker
                            title={"기준일자"}
                            id={"invenDt"}
                            value={invenDt}
                            onChange={handleChange}
                        />

                        <SCheckBox
                            title={"실재고여부"}
                            id={"invenCheck"}
                            value={invenCheck}
                            onChange={handleChange}
                        />
                        
                        <SSelectBox
                            title={"그룹옵션"}
                            id={"subTotalId"}
                            value={subTotalId}
                            optionGroup={subTotaloption}
                            onChange={handleChange}
                            marginLeft={78}
                        />

                        <SSelectBox 
                            title={"위치"}
                            id={"locId"}
                            value={locId}
                            pk={"locId,buId"}
                            optionGroup={locList}
                            valueMember={"locId"}
                            displayMember={"locNm"}
                            onChange={handleChange}
                            setList={locList}
                            onSelectionChange={dateSelectionChange}
                            addOption={"전체"}
                        />

                        {/* <STreeSelectBox
                            title={"수량그룹"}
                            id={"qtyCd"}
                            value={qtyCd}
                            optionGroup={qtyGroup}
                            pIdMember={"pqtyCd"}
                            titleMember={"qtyNm"}
                            valueMember={"qtyCd"}
                            onChange={handleChange}
                            contentWidth={200}
                        /> */}
                    </div>
                    <div className='search_line'>
                    
                        <STreeSelectBox 
                            title={"제품군"}
                            id={"categoryCd"}
                            value={categoryCd}
                            optionGroup={categoryGroup}
                            pIdMember={"pcategoryCd"}
                            titleMember={"categoryNm"}
                            valueMember={"categoryCd"}
                            onChange={handleChange}
                            contentWidth={310}
                            addOption={"전체"}
                        />

                        <SInput
                            title={"제품"}
                            id={"itemCdNm"}
                            value={itemCdNm}
                            onChange={handleChange}
                            onEnterKeyDown={handleSearchClick}
                        />

                        <SSelectBox
                            title={"품질상태"}
                            id={"lotQuality"}
                            value={lotQuality}
                            codeGroup={"MFD003"}
                            onChange={handleChange}
                            addOption={"전체"}
                        />

                        <SSelectBox
                            title={"기준단위"}
                            id={"uomGubn"}
                            value={uomGubn}
                            codeGroup={"MFD015"}
                            onChange={handleChange}
                        />
                    </div>
                </div>
            </React.Fragment> 
        )
    }
}

export default PostionSearchItem;