import API from 'components/override/api/API';

class PositionRepository {
    
    URL = "/api/inventory/position";
    
    /* 조회 */
    // 제품별 재고 조회
    getPositions(params) {
        console.log('params', params);
        return API.request.get(encodeURI(`${this.URL}/search?buId=${params.buId}&itemCdNm=${params.itemCdNm}&lotQuality=${params.lotQuality}&invenCheck=${params.invenCheck}&invenDt=${params.invenDt}&subTotalId=${params.subTotalId}&locId=${params.locId}&uomGubn=${params.uomGubn}&categoryCd=${params.categoryCd}`))
    }
    // 바코드 조회
    getBarcode(params) {
        console.log('RepBarcode', params);
        return API.request.get(encodeURI(`${this.URL}/barcode?buId=${params.buId}&itemCd=${params.itemCd}&locId=${params.locId}&lotNo=${params.lotNo}&prodDt=${params.prodDt}&expiryDt=${params.expiryDt}`))
    }

    //수불내역 조회
    getLedger(params) {
        console.log('Ledger', params.locId);
        return API.request.get(encodeURI(`${this.URL}/getLedger?locId=${params.locId}&barcode=${params.barcode}&startDate=${params.startDate}&endDate=${params.endDate}`));
    }
    
}

export default new PositionRepository();