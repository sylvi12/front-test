import { observable, action } from 'mobx';
import moment from 'moment';
import 'moment/locale/ko';

import CommonStore from 'modules/pages/common/service/CommonStore';
import BaseStore from 'utils/base/BaseStore';

import GridUtil from "components/override/grid/utils/GridUtil";
import Alert from 'components/override/alert/Alert';

import PositionRepository from 'modules/pages/inventory/position/service/PositionRepository';
import LocationRepository from 'modules/pages/system/location/service/LocationRepository';

import BarcodeHistoryRepository from 'modules/pages/inventory/barcodeHistory/service/BarcodeHistoryRepository';
import ItemCategoryRepository from 'modules/pages/system/itemCategory/service/ItemCategoryRepository';

class PositionStore extends BaseStore {

    constructor() {
        super()
        this.setInitialState();
    }

    //검색조건 변수
    @observable buId = CommonStore.buId;
    @observable buList = CommonStore.buList;
    @observable itemCdNm = '';
    @observable lotQuality = '';
    @observable invenCheck = 'Y';
    @observable invenDt = moment().format('YYYYMMDD');
    @observable subTotalId = '01';
    @observable locId = '';
    @observable locList = [];
    @observable uomGubn = 10;
    @observable categoryCd = '';
    @observable categoryGroup = [];

    //모달변수 변수
    @observable ledgerModalOpen = false;
    @observable ledgerApModModalIsOpen = false;
    @observable ledgerApModHisModalIsOpen = false;

    // 수불 변수
    @observable startDate_l = ''; // 수불내역 조건 조회기간Start
    @observable endDate_l = moment().format('YYYYMMDD'); // 수불내역 조건 조회기간End
    @observable orderType = ''; // 수불내역 지시타입
    @observable ledgerId = 0; // 수불적용일자 변경 ledgerId
    @observable tranjDt = ''; // 기존일자
    @observable tranjDtEdit = moment().format('YYYYMMDD'); // 변경일자
    @observable tranjDtReason = ''; // 변경사유

    //오더 변수
    @observable orderDetailModalIsOpen = false;

    //바코드내역 변수
    @observable ledgerApModModalIsOpen = false;
    @observable orderType = '';
    @observable orderNo = 0;
    @observable itemCd = '';
    @observable itemNm = '';
    @observable inventoryId = '';
    @observable packSeq = 0;
    @observable lotId = 0;
    @observable ledgerId = 0;
    @observable barcode = '';
    @observable startDate = '';
    @observable lotNo = 0;
    @observable invenQty = 0;
    @observable prodUom = '';
    @observable startDate = moment().add(-14, 'days').format('YYYYMMDD');
    @observable endDate = moment().format('YYYYMMDD');

    // 생산정보 변수
    @observable orderHelperModalIsOpen = false;
    @observable orderNo_h = '';
    @observable orderBarcodeYN = '';

    //그리드 변수
    positionList = [];
    positionGridApi = undefined;
    @observable pinnedBottomRowData = [];
    barcodeList = [];
    barcodeGridApi = undefined;
    @observable pinnedBottomRowBarcodeData = [];
    ledgerHisList = [];
    ledgerHisGridApi = undefined;

    //모달 그리드 변수
    orderLedgerDataList = [];
    orderLedgerGridApi = undefined;

    @observable alphaCheck = false;

    //수량그룹 변수
    // @observable qtyCd = '';

    //공장 불러오기
    @action.bound
    getBu = async () => {
        this.buId = CommonStore.buId;
    }

    @action.bound
    handleChange = (data) => {
        this[data.id] = data.value;
        this.alphaCheck = true;
    }

    // search에 위치List 넣기
    @action.bound
    getLocation = async () => {
        const searchData = {
            "buId": this.buId
            , "level": 1
        }
        const { data, status } = await LocationRepository.getLocation(searchData);

        if (status == 200) {
            this.locList = data.data;
        }
    }

    // search 공장 선택시 위치List 변경
    @action.bound
    buSelectionChanged = async () => {
        this.getLocation();
        this.setItemCategory();
    }

    //조회
    @action.bound
    handleSearchClick = async () => {

        const searchData = {
            "buId": this.buId,
            "itemCdNm": this.itemCdNm,
            "lotQuality": this.lotQuality,
            "invenCheck": this.invenCheck,
            "invenDt": this.invenDt,
            "subTotalId": this.subTotalId,
            "locId": this.locId,
            "uomGubn": this.uomGubn,
            "categoryCd": this.categoryCd
        }

        const { data, status } = await PositionRepository.getPositions(searchData);

        if (status == 200) {
            if (subTotalId.value == '02') {
                this.positionGridApi.columnController.setColumnsVisible(["lotNo", "LOT NO"], false);
                this.positionGridApi.columnController.setColumnsVisible(["prodDt", "생산일자"], false);
                this.positionGridApi.columnController.setColumnsVisible(["expiryDt", "유통기한"], false);
            } else {
                this.positionGridApi.columnController.setColumnsVisible(["lotNo", "LOT NO"], true);
                this.positionGridApi.columnController.setColumnsVisible(["prodDt", "생산일자"], true);
                this.positionGridApi.columnController.setColumnsVisible(["expiryDt", "유통기한"], true);
            }

            //수량그룹 조건에 맞게 제어
            // const qtyValue = this.qtyCd.split(',');
            // this.qtyList(qtyValue);
        }

        this.positionList = data.data;
        this.positionGridApi.setRowData(this.positionList);

        this.barcodeGridApi.setRowData([]);
        this.barcodList = [];
    }

    //수량그룹 제어
    // qtyList = (qty) => {

    //     if(qty.filter(x => x == '01') == '01') {
    //         this.positionGridApi.columnController.setColumnsVisible(["plQty", "PL수량"], true);
    //     }else if(qty.filter(x => x == '01') != '01') {
    //         this.positionGridApi.columnController.setColumnsVisible(["plQty", "PL수량"], false);
    //     }

    //     if(qty.filter(x => x == '02') == '02') {
    //         this.positionGridApi.columnController.setColumnsVisible(["prodQty", "생산수량"], true);
    //     }else if(qty.filter(x => x == '02') != '02') {
    //         this.positionGridApi.columnController.setColumnsVisible(["prodQty", "생산수량"], false);
    //     }

    //     if(qty.filter(x => x == '03') == '03') {
    //         this.positionGridApi.columnController.setColumnsVisible(["invenQty", "재고수량"], true);
    //     }else if(qty.filter(x => x == '03') != '03') {
    //         this.positionGridApi.columnController.setColumnsVisible(["invenQty", "재고수량"], false);
    //     }

    //     if(qty.filter(x => x == '04') == '04') {
    //         this.positionGridApi.columnController.setColumnsVisible(["availableQty", "가용수량"], true);
    //     }else if(qty.filter(x => x == '04') != '04') {
    //         this.positionGridApi.columnController.setColumnsVisible(["availableQty", "가용수량"], false);
    //     }

    //     if(qty.filter(x => x == '05') == '05') {
    //         this.positionGridApi.columnController.setColumnsVisible(["holdQty", "홀드수량"], true);
    //     }else if(qty.filter(x => x == '05') != '05') {
    //         this.positionGridApi.columnController.setColumnsVisible(["holdQty", "홀드수량"], false);
    //     }

    // }

    //제품별 재고 클릭 변경시 바코드 조회
    @action.bound
    getBarcode = async () => {
        const selectedRow = this.positionGridApi.getSelectedRows()[0];
        let submitData = [];

        if (selectedRow.lotNo == null) {
            submitData = {
                "buId": selectedRow.buId,
                "locId": selectedRow.locId,
                "itemCd": selectedRow.itemCd,
                "lotNo": '',
                "prodDt": '',
                "expiryDt": ''
            }
        } else {
            submitData = {
                "buId": selectedRow.buId,
                "locId": selectedRow.locId,
                "itemCd": selectedRow.itemCd,
                "lotNo": selectedRow.lotNo,
                "prodDt": selectedRow.prodDt,
                "expiryDt": selectedRow.expiryDt
            }
        }

        const { data, status } = await PositionRepository.getBarcode(submitData);

        if (status == 200) {
            this.barcodList = data.data;
            this.barcodeGridApi.setRowData(this.barcodList);
        }
    }

    //수불내역 클릭시 Modal 오픈
    @action.bound
    openLedgerModal = () => {

        const selectedRow = this.barcodeGridApi.getSelectedRows()[0];

        if (!selectedRow || selectedRow.length === 0) {
            Alert.meg("항목을 선택해 주세요.");
            return;
        }

        this.barcode = selectedRow.barcode

        //오더타입 빈 값일경우
        if (selectedRow.orderType === null) {
            this.orderType = ''
        } else {
            this.orderType = selectedRow.orderType
        }

        this.orderNo = selectedRow.orderNo
        this.itemCd = selectedRow.itemCd
        this.itemNm = selectedRow.itemNm
        this.inventoryId = selectedRow.inventoryId
        this.packSeq = selectedRow.packSeq
        this.barcode = selectedRow.barcode
        this.lotNo = selectedRow.lotNo
        this.invenQty = selectedRow.invenQty
        this.prodUom = selectedRow.prodUom

        this.ledgerModalOpen = true;
        this.ledgerModelClick();
    }

    //수불Modal 데이터 바인딩
    @action.bound
    ledgerModelClick = async () => {

        const selectedRow = this.barcodeGridApi.getSelectedRows()[0];
        const positionSelect = this.positionGridApi.getSelectedRows()[0];
        const searchData = {
            "locId": positionSelect.locId
            , "barcode": selectedRow.barcode
            , "startDate": this.startDate_l
            , "endDate": this.endDate_l
        }

        const { data, status } = await PositionRepository.getLedger(searchData);

        if (status === 200) {
            this.orderLedgerDataList = data.data;
            this.orderLedgerGridApi.setRowData(this.orderLedgerDataList);
        } else {
            alert.meg('error', data);
        }
    }

    //수불적용일자 변경 클릭
    ledgerApModModal = () => {

        const selectedRow = this.orderLedgerGridApi.getSelectedRows()[0];

        if (!selectedRow || selectedRow.length === 0) {
            Alert.meg("항목을 선택해 주세요.");
            return;
        }

        this.ledgerId = selectedRow.ledgerId;
        this.lotId = selectedRow.lotId;
        this.barcode = selectedRow.barcode;
        this.tranjDt = selectedRow.tranjDt;
        this.ledgerApModModalIsOpen = true;
    }

    // 수불적용일자 변경 - 저장 클릭
    @action.bound
    ledgerChange = async () => {
        const submitdata = {
            "ledgerId": this.ledgerId,
            "lotId": this.lotId,
            "barcode": this.barcode,
            "tranjDt": this.tranjDt,
            "tranjDtEdit": this.tranjDtEdit,
            "tranjDtReason": this.tranjDtReason
        }

        if (this.tranjDt === this.tranjDtEdit.replace(/-/gi, '') || this.tranjDtReason === '') {
            Alert.meg("변경일자 및 사유를 확인해주세요");
            return;
        } else {
            const param = JSON.stringify({ param: submitdata });
            const { data, status } = await BarcodeHistoryRepository.setLedgerChange(param);
            if (status === 200) {
                Alert.meg(data.msg);
            } else {
                Alert.meg(data.msg);
            }
        }
    }

    // BarcodeHistoryLedgerHisModal(수불적용일자 변경이력) 변경이력Btn 클릭시
    @action.bound
    ledgerApModHisModal = async () => {
        const selectedRow = this.orderLedgerGridApi.getSelectedRows()[0];
        if (!selectedRow || selectedRow.length === 0) {
            Alert.meg("항목을 선택해 주세요.");
            return;
        }

        this.ledgerApModHisModalIsOpen = true;
        const submitdata = {
            "ledgerId": selectedRow.ledgerId
        }

        const { data, status } = await BarcodeHistoryRepository.getLedgerApModHisModal(submitdata);
        if (status === 200) {
            this.ledgerHisList = data.data;
            this.ledgerHisGridApi.setRowData(this.ledgerHisList);
        } else {
            Alert.meg(data.msg);
        }
    }

    //생산정보 Modal 열기(BarcodeHistory 컴포넌트에서 가져옴)
    @action.bound
    openOrderDetailModal = async () => {
        const selectedRow = this.barcodeGridApi.getSelectedRows()[0];
        if (!selectedRow || selectedRow.length === 0) {
            Alert.meg("항목을 선택해 주세요.");
            return;
        }

        this.orderNo_h = Number(selectedRow.orderNo);
        this.orderBarcodeYN = selectedRow.barcodeYn;
        this.orderHelperModalIsOpen = true;
    }

    //수불내역 Modal 닫기
    @action.bound
    closeModal = () => {
        this.ledgerModalOpen = false;
    }

    //수불적용일자 변경 Modal 닫기
    @action.bound
    closeledgerApModModal = () => {
        this.ledgerApModModalIsOpen = false;
    }

    //수불적용일자 변경이력 Modal 닫기
    @action.bound
    closeledgerApModHisModal = () => {
        this.ledgerApModHisModalIsOpen = false;
    }

    //오더정보 Modal 닫기
    @action.bound
    closeOrderDetailModal = () => {
        this.orderDetailModalIsOpen = false;
    }

    //제품별 재고 내역 Grid
    @action.bound
    setGridApi = async (gridApi) => {
        this.positionGridApi = gridApi;
        this.pinnedBottomRowData = [
            {
                buNm: '합계',
                plQty: 0,
                prodQty: 0,
                invenQty: 0,
                availableQty: 0,
                holdQty: 0,
                disuseQty: 0
            }
        ];
        const columnDefs = [

            { headerName: "공장ID", field: "buId", width: 100, hide: true },
            { headerName: "오더", field: "orderNo", width: 100, hide: true },
            { headerName: "수불날짜", field: "invenDt", width: 100, hide: true },
            {
                headerName: "공장"
                , field: "buNm"
                , width: 100
                , cellClass: 'cell_align_center'

            },
            { headerName: "위치ID", field: "locId", width: 100, hide: true },
            {
                headerName: "위치"
                , field: "locNm"
                , width: 130
                , cellClass: 'cell_align_center'

            }, {
                headerName: "제품코드"
                , field: "itemCd"
                , width: 80
                , cellClass: 'cell_align_center'

            },
            {
                headerName: "제품명"
                , field: "itemNm"
                , width: 150
                , cellClass: 'cell_align_left'

            },
            {
                headerName: "제품규격"
                , field: "spec"
                , width: 135
                , cellClass: 'cell_align_left'

            },
            {
                headerName: "LOT"
                , field: "lotNo"
                , width: 80
                , cellClass: 'cell_align_center'

            },
            {
                headerName: "생산일자"
                , field: "prodDt"
                , width: 100
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter

            },
            {
                headerName: "유통기한"
                , field: "expiryDt"
                , width: 100
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter

            },
            {
                headerName: "단위"
                , field: "fromUom"
                , width: 50
                , cellClass: 'cell_align_center'
                , type: "numericColumn"
                , valueFormatter: (params) => GridUtil.numberFormatter(params, 0)
                , valueParser: GridUtil.numberParser

            },
            {
                headerName: "PL수량"
                , field: "plQty"
                , width: 70
                , cellClass: 'cell_align_right'
                , cellStyle: { "color": "#0054FF" }
                , hide: true
            },
            {
                headerName: "생산수량"
                , field: "prodQty"
                , width: 70
                , cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
                , cellStyle: { "color": "#0054FF" }

            },
            {
                headerName: "재고수량"
                , field: "invenQty"
                , width: 70
                , cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
                , cellStyle: { "color": "#0054FF" }

            },
            {
                headerName: "가용수량"
                , field: "availableQty"
                , width: 70
                , cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
                , cellStyle: { "color": "#0054FF" }

            },
            {
                headerName: "홀드수량"
                , field: "holdQty"
                , width: 70
                , cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
                , cellStyle: { "color": "#0054FF" }

            },
            {
                headerName: "불용수량"
                , field: "disuseQty"
                , width: 70
                , cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
                , cellStyle: { "color": "#0054FF" }
            }
        ];

        this.positionGridApi.setColumnDefs(columnDefs);
        if (subTotalId.value == '02') {
            this.positionGridApi.columnController.setColumnsVisible(["lotNo", "LOT NO"], false);
            this.positionGridApi.columnController.setColumnsVisible(["prodDt", "생산일자"], false);
            this.positionGridApi.columnController.setColumnsVisible(["expiryDt", "유통기한"], false);
        }
    }

    //바코드 내역 Grid
    @action.bound
    setBarcodeGridApi = async (gridApi) => {
        this.barcodeGridApi = gridApi;
        this.pinnedBottomRowBarcodeData = [
            {
                packSeq: '합계',
                prodQty: 0,
                invenQty: 0,
                availableQty: 0,
                holdQty: 0,
                disuseQty: 0
            }
        ];
        const columnDefs = [
            { headerName: "포장순번", field: "packSeq", width: 70, cellClass: 'cell_align_right' },
            {
                headerName: "지시타입"
                , field: "orderType"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            { headerName: "지시번호", field: "orderNo", width: 70, cellClass: 'cell_align_right' },
            { headerName: "생산라인", field: "locId", width: 120, cellClass: 'cell_align_left' },
            { headerName: "바코드", field: "barcode", width: 100, cellClass: 'cell_align_center' },
            { headerName: "제품코드", field: "itemCd", width: 80, cellClass: 'cell_align_center' },
            { headerName: "제품명", field: "itemNm", width: 150, cellClass: 'cell_align_left' },
            { headerName: "규격", field: "spec", width: 80, cellClass: 'cell_align_left' },
            { headerName: "LOT", field: "lotNo", width: 80, cellClass: 'cell_align_center' },
            { headerName: "생산일자", field: "prodDt", width: 100, cellClass: 'cell_align_center', valueFormatter: GridUtil.dateFormatter },
            { headerName: "유통기한", field: "expiryDt", width: 100, cellClass: 'cell_align_center', valueFormatter: GridUtil.dateFormatter },
            { headerName: "생산수량", field: "prodQty", width: 70, cellClass: 'cell_align_right', valueFormatter: (params) => GridUtil.numberFormatter(params) },
            { headerName: "재고수량", field: "invenQty", width: 70, cellClass: 'cell_align_right', valueFormatter: (params) => GridUtil.numberFormatter(params) },
            { headerName: "가용수량", field: "availableQty", width: 70, cellClass: 'cell_align_right', valueFormatter: (params) => GridUtil.numberFormatter(params) },
            { headerName: "홀드수량", field: "holdQty", width: 70, cellClass: 'cell_align_right', valueFormatter: (params) => GridUtil.numberFormatter(params) },
            { headerName: "불용수량", field: "disuseQty", width: 70, cellClass: 'cell_align_right', valueFormatter: (params) => GridUtil.numberFormatter(params) },
            { headerName: "품질상태", field: "lotQuality", width: 80, cellClass: 'cell_align_center', valueFormatter: (params) => GridUtil.getCodeNm(params, 'MFD003') }
        ];

        this.barcodeGridApi.setColumnDefs(columnDefs);
    }

    // OrderModalContents (바코드 수불내역) 그리드
    @action.bound
    setOrderLedgerGridApi = async (gridApi) => {
        this.orderLedgerGridApi = gridApi;
        const columnDefs = [
            {
                headerName: "수불적용일자"
                , field: "tranjDt"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "수불일시"
                , field: "tranjDtm"
                , width: 120
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "수불타입"
                , field: "ledgerType"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.getCodeNm(params, 'MFD002')
            },
            {
                headerName: "지시타입"
                , field: "orderType"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.getCodeNm(params, 'MFD001')
            },
            {
                headerName: "지시번호"
                , field: "orderNo"
                , width: 70
                , cellClass: 'cell_align_right'
            },
            {
                headerName: "처리수량", field: "processQty", width: 100, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            {
                headerName: "재고수량", field: "invenQty", width: 100, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            {
                headerName: "가용수량", field: "availableQty", width: 100, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            {
                headerName: "홀드수량", field: "holdQty", width: 100, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            {
                headerName: "불용수량", field: "disuseQty", width: 100, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            { headerName: "위치", field: "locNm", width: 140, cellClass: 'cell_align_center' },
            { headerName: "원본바코드", field: "fromTo", width: 100, cellClass: 'cell_align_center' },
        ];
        this.orderLedgerGridApi.setColumnDefs(columnDefs);
    }

    // 수불적용일자 변경이력 Grid
    @action.bound
    setLedgerHisGridApi = async (gridApi) => {
        this.ledgerHisGridApi = gridApi;
        const columnDefs = [
            { headerName: "순번", field: "ledgerEditId", width: 70, cellClass: 'cell_align_center' },
            { headerName: "변경일시", field: "tranjDtm", width: 100, cellClass: 'cell_align_center', valueFormatter: GridUtil.dateFormatter },
            { headerName: "기존일자", field: "tranjDt", width: 100, cellClass: 'cell_align_center', valueFormatter: GridUtil.dateFormatter },
            { headerName: "변경일자", field: "tranjDtEdit", width: 100, cellClass: 'cell_align_center', valueFormatter: GridUtil.dateFormatter },
            { headerName: "변경사유", field: "tranjDtReason", width: 200, cellClass: 'cell_align_left' }
        ];
        this.ledgerHisGridApi.setColumnDefs(columnDefs);
    }

    //제품군
    @action.bound
    setItemCategory = async () => {

        const params = {
            buId: this.buId
        }
        const { data, status } = await ItemCategoryRepository.getItemCategory(params);

        if (data.success) {
            this.categoryGroup = data.data;
        }
    }
}

export default new PositionStore();
