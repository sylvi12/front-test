import React, { Component } from "react";
import { inject } from "mobx-react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import PositionSerachItem from "modules/pages/inventory/position/PositionSerachItem";
import PositionContents from "modules/pages/inventory/position/PositionContents";
import PositionBarcodeMiddleItem from "modules/pages/inventory/position/PositionBarcodeMiddleItem";
import PositionBarcodeContents from "modules/pages/inventory/position/PositionBarcodeContents";

import SModal from "components/override/modal/SModal";
import LedgerModal from "modules/pages/inventory/position/positionModal/LedgerModal";
import OrderHelper from "components/codehelper/OrderHelper";

@inject(stores => ({
    ledgerModalOpen: stores.positionStore.ledgerModalOpen
    , closeModal: stores.positionStore.closeModal
    , orderHelperModalIsOpen: stores.positionStore.orderHelperModalIsOpen
    , orderNo_h: stores.positionStore.orderNo_h
    , orderBarcodeYN: stores.positionStore.orderBarcodeYN
    , handleChange: stores.positionStore.handleChange
}))

class Position extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {   ledgerModalOpen
                , closeModal
                , orderHelperModalIsOpen
                , orderNo_h
                , orderBarcodeYN
                , handleChange
            
            } = this.props;

        return (
            <React.Fragment>
                <div style={{ width: '100%', height: '65%', float: 'left' }}>
                    <SearchTemplate searchItem={<PositionSerachItem />} />
                    <ContentsMiddleTemplate contentSubTitle={'위치별재고 현황'} />
                    <ContentsTemplate id="position" contentItem={<PositionContents />} />
                </div>

                <div style={{ width: '100%', height: '35%', float: 'left' }}>
                    <ContentsMiddleTemplate contentSubTitle={'바코드 내역'} middleItem={<PositionBarcodeMiddleItem />}/>
                    <ContentsTemplate id="barcode" contentItem={<PositionBarcodeContents />} />
                </div>

                <SModal
                    formId={"inventory_ledger_info_p"}
                    id="ledger"
                    isOpen={ledgerModalOpen}
                    onRequestClose={closeModal}
                    contentLabel="수불내역"
                    width={1230}
                    height={500}
                    contents={<LedgerModal />}
                />
                
                <OrderHelper
                    formId={"order_prod_info_p"}
                    id={'orderHelperModalIsOpen'}
                    orderHelperModalIsOpen={orderHelperModalIsOpen}
                    title={'생산정보'}
                    onChange={handleChange}
                    orderNo={orderNo_h}
                    barcodeYn={orderBarcodeYN}
                />
            
            </React.Fragment>
        )
    }
}

export default Position;