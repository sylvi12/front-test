import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setLocationLocationGridApi: stores.locationMoveStore.setLocationLocationGridApi
    , locationLocationList: stores.locationMoveStore.locationLocationList
    , getBarcode: stores.locationMoveStore.getBarcode
}))

class LocationLocationContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setLocationLocationGridApi, locationLocationList, getBarcode } = this.props;

        return (
            <SGrid
                grid={'locationLocationGrid'}
                gridApiCallBack={setLocationLocationGridApi}
                rowData={locationLocationList}
                onSelectionChanged={getBarcode}
                editable={false}
            />
        );
    }
}
export default LocationLocationContents;