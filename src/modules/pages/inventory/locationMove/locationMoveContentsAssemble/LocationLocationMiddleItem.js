import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    locationMiddleInsert: stores.locationMoveStore.locationMiddleInsert
    , locationMiddleMoving: stores.locationMoveStore.locationMiddleMoving
    , locationMiddleDelete: stores.locationMoveStore.locationMiddleDelete
    , locationMiddleConfirm: stores.locationMoveStore.locationMiddleConfirm
    , locationMiddleCancel: stores.locationMoveStore.locationMiddleCancel
}))

class LocationLocationMiddleItem extends Component {

    render() {

        const { locationMiddleInsert, locationMiddleMoving, locationMiddleDelete, locationMiddleConfirm, locationMiddleCancel } = this.props;
        return (
            <React.Fragment>
                <SButton
                    className="btn_red"
                    buttonName={'등록'}
                    type={'btnLocationInsert'}
                    onClick={locationMiddleInsert}
                />
                {/* <SButton
                    className="btn_red"
                    buttonName={'이동'}
                    type={'update'}
                    onClick={locationMiddleMoving}
                /> */}
                <SButton
                    className="btn_red"
                    buttonName={'삭제'}
                    type={'btnLocationDelete'}
                    onClick={locationMiddleDelete}
                />
                <SButton
                    className="btn_red"
                    buttonName={'확정'}
                    type={'btnLocationConfirm'}
                    onClick={locationMiddleConfirm}
                />
                <SButton
                    className="btn_red"
                    buttonName={'확정취소'}
                    type={'btnLocationCancel'}
                    onClick={locationMiddleCancel}
                />
            </React.Fragment>
        )
    }
}

export default LocationLocationMiddleItem;