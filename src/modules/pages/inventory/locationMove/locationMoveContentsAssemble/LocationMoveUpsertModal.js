import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import STextArea from 'components/atoms/input/STextArea';
import SButton from 'components/atoms/button/SButton';
import SIconButton from 'components/atoms/button/SIconButton';

import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';

import LocationBarcodeMiddleItem from "modules/pages/inventory/locationMove/locationMoveContentsAssemble/LocationBarcodeMiddleItem";
import LocationBarcodeContents from "modules/pages/inventory/locationMove/locationMoveContentsAssemble/LocationBarcodeContents";

import HoldInsertModalContents from "modules/pages/inventory/qualityStatus/qualityStatusModal/HoldInsertModalContents";

@inject(stores => ({
    locationMoveUpsertModalLocNm: stores.locationMoveStore.locationMoveUpsertModalLocNm
    , handleChange: stores.locationMoveStore.handleChange
    , openLocationHelper: stores.locationMoveStore.openLocationHelper

    , locationMoveUpsertModalMoveDt: stores.locationMoveStore.locationMoveUpsertModalMoveDt

    , locationMoveUpsertModalRemark: stores.locationMoveStore.locationMoveUpsertModalRemark

    , closeLocationMoveUpsertModal: stores.locationMoveStore.closeLocationMoveUpsertModal
    , locationMoveUpsertModalSave: stores.locationMoveStore.locationMoveUpsertModalSave
}))
@observer
class LocationMoveUpsertModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { locationMoveUpsertModalLocNm, handleChange, openLocationHelper
            , locationMoveUpsertModalMoveDt
            , locationMoveUpsertModalRemark
            , closeLocationMoveUpsertModal
            , locationMoveUpsertModalSave
        } = this.props;

        return (
            <React.Fragment>
                <div className='btn_wrap'>
                    <SButton buttonName={"저장"} onClick={locationMoveUpsertModalSave} type={"btnLocationSave"} />
                    <SButton buttonName={"닫기"} onClick={closeLocationMoveUpsertModal} type={"default"} />
                </div>

                <table style={{ width: "100%" }}>
                    <tbody>
                        <tr>
                            <th>이동위치</th>
                            <td>
                                <div style={{ display: "inline-block" }}>
                                    <SInput
                                        id="locationMoveUpsertModalLocNm"
                                        value={locationMoveUpsertModalLocNm}
                                        onChange={handleChange}
                                        contentWidth={110}
                                    />
                                </div>
                                <SIconButton type={"search"} onClick={openLocationHelper} />
                            </td>
                        </tr>
                        <tr>
                            <th>이동일자</th>
                            <td>
                                <SDatePicker
                                    id="locationMoveUpsertModalMoveDt"
                                    value={locationMoveUpsertModalMoveDt}
                                    onChange={handleChange}
                                />
                            </td>
                        </tr>
                        <tr>
                            <th>비고</th>
                            <td>
                                <STextArea
                                    id="locationMoveUpsertModalRemark"
                                    value={locationMoveUpsertModalRemark}
                                    onChange={handleChange}
                                    contentWidth={690}
                                />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <ContentsMiddleTemplate contentSubTitle={'바코드 내역'} middleItem={<LocationBarcodeMiddleItem />} />
                <ContentsTemplate id='holdInsertModal' contentItem={<HoldInsertModalContents />} />

                {/* <ContentsTemplate id='locationBarcode' contentItem={<LocationBarcodeContents />} /> */}
                {/* HOLD관리Component (같은스토어에 있는 Grid를 불러오면 더이상 작동하지 않음)*/}


            </React.Fragment>
        );
    }
}

export default LocationMoveUpsertModal;