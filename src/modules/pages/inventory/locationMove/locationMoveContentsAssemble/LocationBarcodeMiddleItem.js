import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    locationBarcodeRegister: stores.locationMoveStore.locationBarcodeRegister
    , locationBarcodeDelete: stores.locationMoveStore.locationBarcodeDelete
}))

class DisuseBarcodeMiddleItem extends Component {

    render() {

        const { locationBarcodeRegister, locationBarcodeDelete } = this.props;
        return (
            <React.Fragment>
                <SButton
                    className="btn_red"
                    buttonName={'추가'}
                    type={'btnBarcodeAdd'}
                    onClick={locationBarcodeRegister}
                />
                <SButton
                    className="btn_red"
                    buttonName={'삭제'}
                    type={'btnBarcodeDelete'}
                    onClick={locationBarcodeDelete}
                />
            </React.Fragment>
        )
    }
}

export default DisuseBarcodeMiddleItem;