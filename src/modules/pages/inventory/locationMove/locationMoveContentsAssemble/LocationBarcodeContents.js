import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setLocationBarcodeGridApi: stores.locationMoveStore.setLocationBarcodeGridApi
    , locationBarcodeList: stores.locationMoveStore.locationBarcodeList
    , pinnedBottomRowDataAtLocationBarcodeGrid: stores.locationMoveStore.pinnedBottomRowDataAtLocationBarcodeGrid
}))

class LocationBarcodeContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setLocationBarcodeGridApi, locationBarcodeList, pinnedBottomRowDataAtLocationBarcodeGrid, handleCellClicked } = this.props;

        return (
            <SGrid
                grid={'locationBarcodeGrid'}
                gridApiCallBack={setLocationBarcodeGridApi}
                rowData={locationBarcodeList}
                editable={false}
                pinnedBottomRowData={pinnedBottomRowDataAtLocationBarcodeGrid}
                suppressRowClickSelection = {true}
                onCellClicked={handleCellClicked}
            />
        );
    }
}
export default LocationBarcodeContents;