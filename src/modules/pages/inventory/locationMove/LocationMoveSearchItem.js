import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SButton from 'components/atoms/button/SButton'
import LocationMoveStore from 'modules/pages/inventory/locationMove/service/LocationMoveStore';
import SBuSelectBox from 'components/override/business/SBuSelectBox';

@inject(stores => ({
    buId: stores.locationMoveStore.buId,
    buGroup: stores.locationMoveStore.buGroup,
    handleChange: stores.locationMoveStore.handleChange,
    startDt: stores.locationMoveStore.startDt,
    handleSearchClick: stores.locationMoveStore.handleSearchClick,
}))
class LocationMoveSearchItem extends Component {

    constructor(props) {
        super(props);
    }

    async componentWillMount() {
        if (LocationMoveStore.alphaCheck == false) {
            await LocationMoveStore.getBu();
        }
    }

    render() {

        const { buId, handleChange
            , startDt
            , handleSearchClick
        } = this.props;

        return (
            <React.Fragment>

                <SBuSelectBox
                    title={"공장"}
                    id={"buId"}
                    value={buId}
                    onChange={handleChange}
                />

                <SDatePicker
                    title={"이동일자"}
                    id="startDt"
                    value={startDt}
                    onChange={handleChange}
                />

                <div className='search_item_btn'>
                    <SButton buttonName={'조회'} onClick={handleSearchClick} type={'btnSearch'} />
                </div>

            </React.Fragment>
        )
    }
}

export default LocationMoveSearchItem;