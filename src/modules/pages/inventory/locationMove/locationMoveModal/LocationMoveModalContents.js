import React, { Component } from "react";
import { inject } from "mobx-react";

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setLocationMoveModalGridApi: stores.locationMoveStore.setLocationMoveModalGridApi
    , locationMoveModalList: stores.locationMoveStore.locationMoveModalList
    , locationMoveModalMoving: stores.locationMoveStore.locationMoveModalMoving
}))

class LocationMoveModalContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setLocationMoveModalGridApi, locationMoveModalList, locationMoveModalMoving } = this.props;

        return (
            <SGrid
                grid={'locationMoveModalGrid'}
                gridApiCallBack={setLocationMoveModalGridApi}
                rowData={locationMoveModalList}
                rowDoubleClick={locationMoveModalMoving}
                editable={false}
            />
        );
    }
}

export default LocationMoveModalContents;