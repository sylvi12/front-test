import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SInput from 'components/atoms/input/SInput'
import SButton from 'components/atoms/button/SButton'

@inject(stores => ({
    locationMoveModalGlclass: stores.locationMoveStore.locationMoveModalGlclass
    , handleChange: stores.locationMoveStore.handleChange
    , locationMoveModalItemNmOrItemCd: stores.locationMoveStore.locationMoveModalItemNmOrItemCd
    , locationMoveModalSearch: stores.locationMoveStore.locationMoveModalSearch
    , locationMoveModalBarcode: stores.locationMoveStore.locationMoveModalBarcode
    , locationMoveModalLocNm: stores.locationMoveStore.locationMoveModalLocNm
}))
class LocationMoveModalSearchItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { locationMoveModalGlclass, handleChange
            , locationMoveModalItemNmOrItemCd, locationMoveModalSearch
            , locationMoveModalBarcode
            , locationMoveModalLocNm
        } = this.props;

        return (
            <React.Fragment>
                <SSelectBox
                    title={"GLCLASS"}
                    id={"locationMoveModalGlclass"}
                    value={locationMoveModalGlclass}
                    codeGroup={"MT002"}
                    onChange={handleChange}
                    addOption={"전체"}
                />
                <SInput
                    title={"제품명/코드"}
                    id={"locationMoveModalItemNmOrItemCd"}
                    value={locationMoveModalItemNmOrItemCd}
                    onChange={handleChange}
                    onEnterKeyDown={locationMoveModalSearch}
                />
                <SInput
                    title={"바코드"}
                    id={"locationMoveModalBarcode"}
                    value={locationMoveModalBarcode}
                    onChange={handleChange}
                    onEnterKeyDown={locationMoveModalSearch}
                />
                <SInput
                    title={"위치"}
                    id={"locationMoveModalLocNm"}
                    value={locationMoveModalLocNm}
                    onChange={handleChange}
                />

                <div className='search_item_btn' style={{ right: "" }}>
                    <SButton buttonName={'조회'} onClick={locationMoveModalSearch} type={'select'} />
                </div>

            </React.Fragment>
        )
    }
}

export default LocationMoveModalSearchItem;