import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SNumericInput from 'components/override/numericinput/SNumericInput';
import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    locationMoveModalToLocNm: stores.locationMoveStore.locationMoveModalToLocNm
    , handleChange: stores.locationMoveStore.handleChange
    , locationMoveModalMovingQty: stores.locationMoveStore.locationMoveModalMovingQty
    , locationMoveModalMoving: stores.locationMoveStore.locationMoveModalMoving
}))
class LocationMoveModalMiddleItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { locationMoveModalToLocNm, handleChange
            , locationMoveModalMovingQty
            , locationMoveModalMoving
        } = this.props;

        return (
            <React.Fragment>
                <div className="msdisplay wkdisplay">
                    <SInput
                        title={"이동위치"}
                        id={"locationMoveModalToLocNm"}
                        value={locationMoveModalToLocNm}
                        onChange={handleChange}
                    />
                    <SNumericInput
                        title={"이동수량"}
                        id={"locationMoveModalMovingQty"}
                        value={locationMoveModalMovingQty}
                        onChange={handleChange}
                    />
                    <SButton buttonName={'이동'} onClick={locationMoveModalMoving} type={'update'} />
                </div>
            </React.Fragment>
        )
    }
}

export default LocationMoveModalMiddleItem;