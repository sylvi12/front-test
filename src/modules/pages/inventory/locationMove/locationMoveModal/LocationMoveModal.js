import React, { Component } from 'react';

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import LocationMoveModalSearchItem from "modules/pages/inventory/locationMove/locationMoveModal/LocationMoveModalSearchItem";
import LocationMoveModalMiddleItem from "modules/pages/inventory/locationMove/locationMoveModal/LocationMoveModalMiddleItem";
import LocationMoveModalContents from "modules/pages/inventory/locationMove/locationMoveModal/LocationMoveModalContents";

class LocationMoveModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <React.Fragment>
                <SearchTemplate searchItem={<LocationMoveModalSearchItem />} />
                <ContentsMiddleTemplate contentSubTitle={'현재고 내역'} middleItem={<LocationMoveModalMiddleItem />} />
                <ContentsTemplate id='locationMoveModal' contentItem={<LocationMoveModalContents />} />
            </React.Fragment>
        );
    }
}

export default LocationMoveModal;