import React, { Component } from "react";

import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import LocationLocationMiddleItem from "modules/pages/inventory/locationMove/locationMoveContentsAssemble/LocationLocationMiddleItem";
import LocationLocationContents from "modules/pages/inventory/locationMove/locationMoveContentsAssemble/LocationLocationContents";
import LocationBarcodeMiddleItem from "modules/pages/inventory/locationMove/locationMoveContentsAssemble/LocationBarcodeMiddleItem";
import LocationBarcodeContents from "modules/pages/inventory/locationMove/locationMoveContentsAssemble/LocationBarcodeContents";

class LocationMoveContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { } = this.props

        return (
            <React.Fragment>
                <div style={{ width: '100%', height: '50%' }}>
                    <ContentsMiddleTemplate contentSubTitle={'이동지시'} middleItem={<LocationLocationMiddleItem />} />
                    <ContentsTemplate id='locationLocation' contentItem={<LocationLocationContents />} />
                </div>
                <div style={{ width: '100%', height: '50%' }}>
                    <ContentsMiddleTemplate contentSubTitle={'바코드 내역'} middleItem={<LocationBarcodeMiddleItem />} />
                    <ContentsTemplate id='locationBarcode' contentItem={<LocationBarcodeContents />} />
                </div>
            </React.Fragment>
        )
    }
}

export default LocationMoveContents;