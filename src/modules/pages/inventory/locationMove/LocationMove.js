import React, { Component } from "react";
import { inject } from "mobx-react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";
import SModal from "components/override/modal/SModal";
import LocationHelper from "components/codehelper/LocationHelper";
import BarcodeAddHelper from "components/codehelper/BarcodeAddHelper";

import LocationMoveSearchItem from "modules/pages/inventory/locationMove/LocationMoveSearchItem";
import LocationLocationMiddleItem from "modules/pages/inventory/locationMove/locationMoveContentsAssemble/LocationLocationMiddleItem";
import LocationLocationContents from "modules/pages/inventory/locationMove/locationMoveContentsAssemble/LocationLocationContents";
import LocationBarcodeMiddleItem from "modules/pages/inventory/locationMove/locationMoveContentsAssemble/LocationBarcodeMiddleItem";
import LocationBarcodeContents from "modules/pages/inventory/locationMove/locationMoveContentsAssemble/LocationBarcodeContents";
import LocationMoveContents from "modules/pages/inventory/locationMove/LocationMoveContents";
import LocationMoveUpsertModal from "modules/pages/inventory/locationMove/locationMoveContentsAssemble/LocationMoveUpsertModal";
import LocationMoveModal from "modules/pages/inventory/locationMove/locationMoveModal/LocationMoveModal";
import LocationBarcodeUpsertModal from "modules/pages/inventory/locationMove/locationBarcodeUpsertModal/LocationBarcodeUpsertModal";

@inject(stores => ({
    locationMoveUpsertModalIsOpen: stores.locationMoveStore.locationMoveUpsertModalIsOpen
    , closeLocationMoveUpsertModal: stores.locationMoveStore.closeLocationMoveUpsertModal

    , locationMoveModalIsOpen: stores.locationMoveStore.locationMoveModalIsOpen
    , closeLocationMoveModal: stores.locationMoveStore.closeLocationMoveModal

    , locationBarcodeUpsertModalIsOpen: stores.locationMoveStore.locationBarcodeUpsertModalIsOpen
    , closeLocationBarcodeUpsertModal: stores.locationMoveStore.closeLocationBarcodeUpsertModal

    , isLocationOpen: stores.locationMoveStore.isLocationOpen
    , locationBuId: stores.locationMoveStore.locationBuId
    , locationHelperResult: stores.locationMoveStore.locationHelperResult
    , handleChange: stores.locationMoveStore.handleChange

    , isBarcodeHelperOpen: stores.locationMoveStore.isBarcodeHelperOpen
    , barocdeHelperResult: stores.locationMoveStore.barocdeHelperResult
    , barcodeHelplerOrderNo: stores.locationMoveStore.barcodeHelplerOrderNo
    , barcodeHelplerledgerType: stores.locationMoveStore.barcodeHelplerledgerType
}))

class LocationMove extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { locationMoveUpsertModalIsOpen, closeLocationMoveUpsertModal
            , locationMoveModalIsOpen, closeLocationMoveModal
            , locationBarcodeUpsertModalIsOpen, closeLocationBarcodeUpsertModal
            , isLocationOpen, locationBuId, locationHelperResult, handleChange
            , isBarcodeHelperOpen, barocdeHelperResult, barcodeHelplerOrderNo, barcodeHelplerledgerType
        } = this.props

        return (
            <React.Fragment>
                <div style={{ width: '100%', height: '60%' }}>
                    <SearchTemplate searchItem={<LocationMoveSearchItem />} />
                    <ContentsMiddleTemplate contentSubTitle={'이동 내역'} middleItem={<LocationLocationMiddleItem />} />
                    <ContentsTemplate id='locationLocation' contentItem={<LocationLocationContents />} />
                </div>
                <div style={{ width: '100%', height: '40%' }}>
                    <ContentsMiddleTemplate contentSubTitle={'바코드 내역'} middleItem={<LocationBarcodeMiddleItem />} />
                    <ContentsTemplate id='locationBarcode' contentItem={<LocationBarcodeContents />} />
                </div>
                {/* <ContentsTemplate id='locationMove' contentItem={<LocationMoveContents />} /> */}

                <SModal
                    formId={"inventory_locationMove_p"}
                    isOpen={locationMoveUpsertModalIsOpen}
                    onRequestClose={closeLocationMoveUpsertModal}
                    contentLabel="지시등록"
                    width={800}
                    height={500}
                    contents={<LocationMoveUpsertModal />}
                />
                <SModal
                    isOpen={locationMoveModalIsOpen}
                    onRequestClose={closeLocationMoveModal}
                    contentLabel="이동등록"
                    width={1000}
                    height={500}
                    contents={<LocationMoveModal />}
                />
                <SModal
                    formId={"inout_barcode_add_p"}
                    isOpen={locationBarcodeUpsertModalIsOpen}
                    onRequestClose={closeLocationBarcodeUpsertModal}
                    contentLabel="바코드 추가"
                    width={1220}
                    height={530}
                    contents={<LocationBarcodeUpsertModal />}
                />
                <LocationHelper
                    id={'isLocationOpen'}
                    isLocationOpen={isLocationOpen}
                    buId={locationBuId}
                    buReadOnly={true}
                    onChange={handleChange}
                    onHelperResult={locationHelperResult}
                />
                <BarcodeAddHelper
                    id={'isBarcodeHelperOpen'}
                    isBarcodeOpen={isBarcodeHelperOpen}
                    barcodeHelplerOrderNo={barcodeHelplerOrderNo}
                    barcodeHelplerledgerType={barcodeHelplerledgerType}
                    handleChange={handleChange}
                    onHelperResult={barocdeHelperResult}
                />
            </React.Fragment>
        )
    }
}

export default LocationMove;