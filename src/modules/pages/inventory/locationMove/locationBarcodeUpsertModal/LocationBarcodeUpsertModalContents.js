import React, { Component } from "react";
import { inject } from "mobx-react";

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setLocationBarcodeUpsertModalGridApi: stores.locationMoveStore.setLocationBarcodeUpsertModalGridApi
    , locationBarcodeUpsertModalList: stores.locationMoveStore.locationBarcodeUpsertModalList
    , locationBarcodeUpsertModalSave: stores.locationMoveStore.locationBarcodeUpsertModalSave
}))

class LocationBarcodeUpsertModalContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setLocationBarcodeUpsertModalGridApi, locationBarcodeUpsertModalList, locationBarcodeUpsertModalSave } = this.props;

        return (
            <SGrid
                grid={'locationBarcodeUpsertModalGrid'}
                gridApiCallBack={setLocationBarcodeUpsertModalGridApi}
                rowData={locationBarcodeUpsertModalList}
                rowDoubleClick={locationBarcodeUpsertModalSave}
                editable={false}
            />
        );
    }
}

export default LocationBarcodeUpsertModalContents;