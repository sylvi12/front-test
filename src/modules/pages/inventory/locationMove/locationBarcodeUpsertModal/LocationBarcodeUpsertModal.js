import React, { Component } from 'react';

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import LocationBarcodeUpsertModalSearchItem from "modules/pages/inventory/locationMove/locationBarcodeUpsertModal/LocationBarcodeUpsertModalSearchItem";
import LocationBarcodeUpsertModalMiddleItem from "modules/pages/inventory/locationMove/locationBarcodeUpsertModal/LocationBarcodeUpsertModalMiddleItem";
import LocationBarcodeUpsertModalContents from "modules/pages/inventory/locationMove/locationBarcodeUpsertModal/LocationBarcodeUpsertModalContents";

class LocationBarcodeUpsertModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <React.Fragment>
                <SearchTemplate searchItem={<LocationBarcodeUpsertModalSearchItem />} />
                <ContentsMiddleTemplate contentSubTitle={'LOT 정보'} middleItem={<LocationBarcodeUpsertModalMiddleItem />} />
                <ContentsTemplate id='locationBarcodeUpsertModal' contentItem={<LocationBarcodeUpsertModalContents />} />
            </React.Fragment>
        );
    }
}

export default LocationBarcodeUpsertModal;