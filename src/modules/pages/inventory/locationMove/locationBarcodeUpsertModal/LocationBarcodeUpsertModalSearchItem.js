import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SDatePicker from 'components/override/datepicker/SDatePicker';
import SInput from 'components/atoms/input/SInput'
import SButton from 'components/atoms/button/SButton'

@inject(stores => ({
    locationBarcodeUpsertModalBarcode: stores.locationMoveStore.locationBarcodeUpsertModalBarcode
    , handleChange: stores.locationMoveStore.handleChange
    , locationBarcodeUpsertModalProdDt: stores.locationMoveStore.locationBarcodeUpsertModalProdDt
    , locationBarcodeUpsertModalItemNmOrItemCd: stores.locationMoveStore.locationBarcodeUpsertModalItemNmOrItemCd
    , locationBarcodeUpsertModalSearch: stores.locationMoveStore.locationBarcodeUpsertModalSearch
}))
class LocationBarcodeUpsertModalSearchItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { locationBarcodeUpsertModalBarcode, handleChange
            , locationBarcodeUpsertModalProdDt
            , locationBarcodeUpsertModalItemNmOrItemCd
            , locationBarcodeUpsertModalSearch
        } = this.props;

        return (
            <React.Fragment>

                <SInput
                    title={"바코드"}
                    id={"locationBarcodeUpsertModalBarcode"}
                    value={locationBarcodeUpsertModalBarcode}
                    onChange={handleChange}
                    onEnterKeyDown={locationBarcodeUpsertModalSearch}
                />

                <SDatePicker
                    title={"생산일자"}
                    id="locationBarcodeUpsertModalProdDt"
                    value={locationBarcodeUpsertModalProdDt}
                    onChange={handleChange}
                />

                <SInput
                    title={"제품명/코드"}
                    id={"locationBarcodeUpsertModalItemNmOrItemCd"}
                    value={locationBarcodeUpsertModalItemNmOrItemCd}
                    onChange={handleChange}
                    onEnterKeyDown={locationBarcodeUpsertModalSearch}
                />

                <div className='search_item_btn' style={{ right: "" }}>
                    <SButton buttonName={'조회'} onClick={locationBarcodeUpsertModalSearch} type={'btnSearch'} />
                </div>

            </React.Fragment>
        )
    }
}

export default LocationBarcodeUpsertModalSearchItem;