import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    closeLocationBarcodeUpsertModal: stores.locationMoveStore.closeLocationBarcodeUpsertModal
    , locationBarcodeUpsertModalSave: stores.locationMoveStore.locationBarcodeUpsertModalSave
}))

class LocationBarcodeUpsertModalMiddleItem extends Component {

    render() {

        const { closeLocationBarcodeUpsertModal, locationBarcodeUpsertModalSave } = this.props;
        return (
            <React.Fragment>
                <SButton buttonName={"저장"} onClick={locationBarcodeUpsertModalSave} type={"btnSave"} />
                <SButton buttonName={"닫기"} onClick={closeLocationBarcodeUpsertModal} type={"default"} />
            </React.Fragment>
        )
    }
}

export default LocationBarcodeUpsertModalMiddleItem;