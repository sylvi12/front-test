import API from "components/override/api/API";

class LocationMoveRepository {

    URL = "/api/inventory/locationmove";
    /* 조회 */
    // 이동이력 그리드 조회
    getLocation(params) {
        return API.request.get(encodeURI(`${this.URL}/getlocation?buId=${params.buId}&startDt=${params.startDt}`))
    }
    // LocationMoveModal 조회
    getItems(params) {
        return API.request.get(encodeURI(`${this.URL}/getitems?glclass=${params.glclass}&itemNmOrItemCd=${params.itemNmOrItemCd}&barcode=${params.barcode}&locNm=${params.locNm}`))
    }

    /* 등록 */
    // LocationMoveUpsertModal 저장
    saveLocationMoveOrder(params) {
        return API.request.post(encodeURI(`${this.URL}/savelocationmoveorder`), params)
    }

    /* 삭제 */
    delete(params) {
        return API.request.post(encodeURI(`${this.URL}/deletelocationmoveorder`), params)
    }

}
export default new LocationMoveRepository();