import { observable, action } from 'mobx';
import moment from 'moment';
import 'moment/locale/ko';

import Alert from 'components/override/alert/Alert';
import GridUtil from 'components/override/grid/utils/GridUtil';
import Validator from 'components/override/utils/Validator';

import LocationMoveRepository from 'modules/pages/inventory/locationMove/service/LocationMoveRepository';
import OutputRepository from 'modules/pages/order/output/service/OutputRepository';
import InoutRepository from 'modules/pages/inout/inout/service/InoutRepository';
import EtcRepository from 'modules/pages/inout/etc/service/EtcRepository';
import CommonStore from 'modules/pages/common/service/CommonStore';

import QualityStatusStore from 'modules/pages/inventory/qualityStatus/service/QualityStatusStore';
import BaseStore from 'utils/base/BaseStore';

class LocationMoveStore extends BaseStore {

    // LocationMoveSearchItem 변수
    @observable buId = CommonStore.buId;
    // @observable buGroup = [];
    @observable startDt = moment().format('YYYYMMDD');

    // LocationLocationContents 변수
    locationLocationGridApi = undefined;
    @observable locationLocationList = [];

    // LocationBarcodeContents 변수
    locationBarcodeGridApi = undefined;
    @observable locationBarcodeList = [];
    @observable pinnedBottomRowDataAtLocationBarcodeGrid = [];
    barcodeOrderNo = 0;

    // LocationMoveUpsertModal 변수
    @observable locationMoveUpsertModalIsOpen = false;
    locationMoveUpsertModalOrderNo = 0;
    @observable locationMoveUpsertModalLocNm = "";
    @observable locationMoveUpsertModalMoveDt = "";
    @observable locationMoveUpsertModalRemark = "";

    // LocationMoveUpsertModal 유효성 검사
    validator = new Validator([
        {
            field: 'locationMoveUpsertModalLocNm',
            method: 'isEmpty',
            message: '이동위치'
        },
        {
            field: 'locationMoveUpsertModalMoveDt',
            method: 'isEmpty',
            message: '이동일자'
        }
    ])

    /* LocationMoveModal 변수 */
    @observable locationMoveModalIsOpen = false;
    // LocationMoveModal SearchItem 변수
    @observable locationMoveModalGlclass = "";
    @observable locationMoveModalItemNmOrItemCd = "";
    @observable locationMoveModalBarcode = "";
    @observable locationMoveModalLocNm = "";
    // LocationMoveModal MiddleItem 변수
    @observable locationMoveModalToLocNm = "";
    @observable locationMoveModalMovingQty = 0;
    // LocationMoveModal Contents 변수
    locationMoveModalGridApi = undefined;
    @observable locationMoveModalList = [];

    /* LocationBarcodeUpsertModel */
    @observable locationBarcodeUpsertModalIsOpen = false;
    // LocationBarcodeUpsertModel SearchItem 변수
    locationBarcodeUpsertModalOrderNo = 0;
    @observable locationBarcodeUpsertModalBarcode = "";
    @observable locationBarcodeUpsertModalProdDt = "";
    @observable locationBarcodeUpsertModalItemNmOrItemCd = "";
    // LocationBarcodeUpsertModel Contents 변수
    locationBarcodeUpsertModalGridApi = undefined;
    @observable locationBarcodeUpsertModalList = [];

    /* BarcodeHelpler variables */
    @observable isBarcodeHelperOpen = false;
    @observable barcodeHelplerOrderNo = 0;
    @observable barcodeHelplerledgerType = '';

    //바코드내역 추가 키 변수
    key = '';

    //등록에서 바코드 추가 변수
    add = [];

    // locatcionHelper 변수
    @observable isLocationOpen = false;
    @observable locationBuId = '';

    @observable alphaCheck = false;

    constructor() {
        super();
        this.validator.valid();
        this.setInitialState();
    }

    //공장 불러오기
    @action.bound
    getBu = async () => {
        this.buId = CommonStore.buId;
        this.alphaCheck = true;
    }

    @action.bound
    handleChange = (data) => {
        console.log(data);
        this[data.id] = data.value;
    }

    // LocationMoveSearchItem 조회 클릭 시
    @action.bound
    handleSearchClick = async () => {
        const searchData = {
            "buId": this.buId
            , "startDt": this.startDt
        }
        const { data, status } = await LocationMoveRepository.getLocation(searchData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            this.locationLocationList = data.data;
            this.locationLocationGridApi.setRowData(this.locationLocationList);
            //조회시 바코드 내역은 초기화
            this.locationBarcodeGridApi.setRowData([]);
        }
    }
    // LocationLocationMiddleItem 등록 클릭 시
    @action.bound
    locationMiddleInsert = async () => {

        let allBu = this.buId.split(',');

        if (allBu.length > 1) {
            Alert.meg("선택된 공장이 없습니다.");
            return;
        }

        this.locationMoveUpsertModalLocNm = "";
        this.locationMoveUpsertModalMoveDt = moment().format('YYYYMMDD');
        this.locationMoveUpsertModalRemark = "";
        this.locationMoveUpsertModalIsOpen = true;

        //바코드내역 - 추가버튼 다르게 사용하기 위해 넣음
        this.key = 'Y';
    }
    // LocationLocationMiddleItem 이동 클릭 시
    @action.bound
    locationMiddleMoving = async () => {
        this.locationMoveModalGlclass = "";
        this.locationMoveModalItemNmOrItemCd = "";
        this.locationMoveModalBarcode = "";
        this.locationMoveModalLocNm = "";
        this.locationMoveModalIsOpen = true;
    }

    // LocationLocationMiddleItem 삭제 클릭 시
    @action.bound
    locationMiddleDelete = async () => {

        const selectedRows = this.locationLocationGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg("선택된 항목이 없습니다.");
            return;
        }
        for (let i = 0; i < selectedRows.length; i++) {
            if (selectedRows[i].moveStatus == '20' || selectedRows[i].moveStatus == '60') {
                Alert.meg("상태를 확인해 주세요.");
                return;
            }
        }
        const searchData = {
            "buId": this.buId
            , "startDt": this.startDt
        }
        const value = await Alert.confirm("삭제하시겠습니까?").then(result => { return result; });
        const params = JSON.stringify({ param1: selectedRows, param2: searchData });
        if (value) {
            const { data, status } = await LocationMoveRepository.delete(params);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                this.locationLocationList = data.data;
                this.locationLocationGridApi.setRowData(this.locationLocationList);
                this.locationBarcodeGridApi.setRowData([]);
                Alert.meg("완료되었습니다.");
            }
        }
        else {
            return;
        }

    }
    // LocationLocationMiddleItem 확정 클릭 시
    @action.bound
    locationMiddleConfirm = async () => {
        const selectedRow = this.locationLocationGridApi.getSelectedRows()[0];
        if (!selectedRow || selectedRow.length == 0) {
            Alert.meg("선택된 대상이 없습니다.");
            return;
        }
        if (selectedRow.moveStatus == '20') {
            Alert.meg("상태를 확인해 주세요.");
            return;
        }
        if (this.locationBarcodeGridApi.getDisplayedRowCount() <= 0) {
            Alert.meg("바코드가 없습니다.");
            return;
        }
        const confirmData = {
            "orderNo": selectedRow.orderNo
        }
        const { data, status } = await EtcRepository.confirm(confirmData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            Alert.meg("확정되었습니다.");
            this.handleSearchClick();
        }
    }
    // LocationLocationMiddleItem 확정취소 클릭 시
    @action.bound
    locationMiddleCancel = async () => {
        const selectedRow = this.locationLocationGridApi.getSelectedRows()[0];
        if (!selectedRow || selectedRow.length == 0) {
            Alert.meg("확정취소할 이동지시를 선택해 주세요");
            return;
        }
        if (selectedRow.moveStatus != '20') {
            Alert.meg("상태를 확인해 주세요.");
            return;
        }
        const confirmData = {
            "orderNo": selectedRow.orderNo
        }
        const value = await Alert.confirm("취소하시겠습니까?").then(result => { return result; });
        if (value) {
            const { data, status } = await EtcRepository.cancel(confirmData);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                this.handleSearchClick();
            }
        }
        else {
            return;
        }
    }

    // LocationLocationContents 그리드
    @action.bound
    setLocationLocationGridApi = async (gridApi) => {
        this.locationLocationGridApi = gridApi;
        const columnDefs = [
            {
                headerName: "지시번호"
                , field: "orderNo"
                , width: 90
                , cellClass: 'cell_align_right'
                
            },
            {
                headerName: "이동일자"
                , field: "moveDt"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "이동위치"
                , field: "locNm"
                , width: 120
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "상태"
                , field: "moveStatusNm"
                , width: 50
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "비고"
                , field: "remark"
                , width: 200
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "등록자"
                , field: "inputEmpNm"
                , width: 75
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "등록일"
                , field: "inputDtm"
                , width: 105
                , cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm')
            }
        ];
        this.locationLocationGridApi.setColumnDefs(columnDefs);
    }

    // LocationLocationContents 그리드 로우 클릭 시
    @action.bound
    getBarcode = async () => {
        const selectedRow = this.locationLocationGridApi.getSelectedRows()[0];
        if (!selectedRow || selectedRow.length == 0) {
            return;
        }
        this.barcodeOrderNo = selectedRow.orderNo;
        this.locationBarcodeUpsertModalOrderNo = selectedRow.orderNo;
        this.searchBarcode();
    }

    // LocationBarcodeMiddleItem 추가 클릭 시
    @action.bound
    locationBarcodeRegister = async () => {
        //지시등록에서 바코드 추가시 Alert 건너뜀
        if (this.key != 'Y') {
            const selectedRow = this.locationLocationGridApi.getSelectedRows()[0];
            if (!selectedRow || selectedRow.length == 0) {
                Alert.meg("이동지시를 선택해 주세요.");
                return;
            }
            if (selectedRow.moveStatus == '20') {
                Alert.meg("이미 확정된 이동지시 입니다.");
                return;
            }

            this.locationBarcodeUpsertModalBarcode = "";
            this.locationBarcodeUpsertModalProdDt = moment().format('YYYYMMDD');
            this.locationBarcodeUpsertModalItemNmOrItemCd = "";
            this.locationBarcodeUpsertModalIsOpen = true;
            this.locationBarcodeUpsertModalList = [];

            //공통 바코드 추가 모델(조건부 때문에 주석처리)
            // this.barcodeHelplerOrderNo = selectedRow.orderNo;
            // this.barcodeHelplerledgerType = selectedRow.ledgerType;
            // this.isBarcodeHelperOpen = true;
        } else {
            this.locationBarcodeUpsertModalBarcode = "";
            this.locationBarcodeUpsertModalProdDt = moment().format('YYYYMMDD');
            this.locationBarcodeUpsertModalItemNmOrItemCd = "";
            this.locationBarcodeUpsertModalIsOpen = true;
            this.locationBarcodeUpsertModalList = [];
        }
    }
    // barcodeHelper 반환
    @action.bound
    barocdeHelperResult = async (result) => {
        if (result) {
            this.key = '';
            this.searchBarcode();
        }
    }
    // LocationBarcodeMiddleItem 삭제 클릭 시
    @action.bound
    locationBarcodeDelete = async () => {

        console.log(this.key);
        if (this.key == 'Y') { //등록에서 제거 클릭시
            const selectedRows = QualityStatusStore.holdBarcodeGridApi.getSelectedRows();

            selectedRows.forEach(selectedRow => {
                QualityStatusStore.holdBarcodeGridApi.updateRowData({ remove: [selectedRow] });
                const index = this.add.findIndex(x => x.barcode == selectedRow.barcode);

                this.add.splice(index, 1);
            })

        } else {
            const selectedRow = this.locationLocationGridApi.getSelectedRows()[0];
            if (selectedRow) {
                if (selectedRow.moveStatus == '20') {
                    Alert.meg("이미 확정된 이동지시 입니다.");
                    return;
                }
            } else {
                Alert.meg("이동지시를 선택해 주세요.");
                return;
            }
            const selectedRows = this.locationBarcodeGridApi.getSelectedRows();
            if (!selectedRows || selectedRows.length == 0) {
                Alert.meg("삭제할 바코드를 선택해 주세요.");
                return;
            }
            const searchData = {
                "orderNo": this.barcodeOrderNo
            }
            const params = JSON.stringify({ param1: selectedRows, param2: searchData });
            const value = await Alert.confirm("삭제하시겠습니까?").then(result => { return result; });
            if (value) {
                const { data, status } = await InoutRepository.deleteBarcode(params);
                if (data.success == false) {
                    Alert.meg(data.msg);
                } else {
                    this.locationBarcodeList = data.data;
                    this.locationBarcodeGridApi.setRowData(this.locationBarcodeList);
                }
            }
            else {
                return;
            }
        }
    }

    // LocationBarcodeContents
    @action.bound
    setLocationBarcodeGridApi = async (gridApi) => {
        this.locationBarcodeGridApi = gridApi;
        this.pinnedBottomRowDataAtLocationBarcodeGrid = [
            {
                seq: '합계',
                processQty: 0
            }
        ];
        const columnDefs = [
            {
                headerName: "",
                children: [
                    {
                        headerName: ""
                        , field: "check"
                        , width: 29
                        , checkboxSelection: true
                        , headerCheckboxSelection: true
                    },
                    {
                        headerName: "순번"
                        , field: "seq"
                        , width: 70
                        , type: "numericColumn"
                        , valueFormatter: GridUtil.numberFormatter
                        , valueParser: GridUtil.numberParser
                    },
                    {
                        headerName: "바코드번호"
                        , field: "barcode"
                        , width: 100
                        , cellClass: 'cell_align_center'
                    },
                    {
                        headerName: "상태"
                        , field: "lotQualityNm"
                        , width: 50
                        , cellClass: 'cell_align_center'
                    },
                    {
                        headerName: "제품코드"
                        , field: "itemCd"
                        , width: 90
                        , cellClass: 'cell_align_center'
                    },
                    {
                        headerName: "LOT NO"
                        , field: "lotNo"
                        , width: 80
                        , cellClass: 'cell_align_center'
                    },
                    {
                        headerName: "생산일자"
                        , field: "prodDt"
                        , width: 90
                        , cellClass: 'cell_align_center'
                        , valueFormatter: GridUtil.dateFormatter
                    },
                    {
                        headerName: "유통기한"
                        , field: "expiryDt"
                        , width: 90
                        , cellClass: 'cell_align_center'
                        , valueFormatter: GridUtil.dateFormatter
                    }
                ]
            },
            {
                headerName: "수량",
                children: [
                    {
                        headerName: "재고량"
                        , field: "processQty"
                        , width: 100
                        , type: "numericColumn"
                        , valueFormatter: GridUtil.numberFormatter
                        , valueParser: GridUtil.numberParser
                        , pinnedRowCellRenderer: "customPinnedRowRenderer"
                        , pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
                    },
                    {
                        headerName: "단위"
                        , field: "processUom"
                        , width: 65
                        , cellClass: 'cell_align_center'
                    }
                ]
            },
            {
                headerName: "",
                children: [
                    {
                        headerName: "위치"
                        , field: "locNm"
                        , width: 120
                        , cellClass: 'cell_align_center'
                    },
                    {
                        headerName: "처리시간"
                        , field: "inputDtm"
                        , width: 105
                        , cellClass: 'cell_align_center'
                        , valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm')
                    },
                    {
                        headerName: "처리자"
                        , field: "inputEmpNm"
                        , width: 90
                        , cellClass: 'cell_align_center'
                    }
                ]
            }
        ];
        this.locationBarcodeGridApi.setColumnDefs(columnDefs);
    }

    // LocationBarcodeContents 조회
    @action.bound
    searchBarcode = async () => {
        const searchData = {
            "orderNo": this.barcodeOrderNo
        }
        const { data, status } = await InoutRepository.getPallet(searchData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            this.locationBarcodeList = data.data;
            this.locationBarcodeGridApi.setRowData(this.locationBarcodeList);
        }
    }

    // LocationMoveUpsertModal 닫기
    @action.bound
    closeLocationMoveUpsertModal = async () => {
        this.locationMoveUpsertModalIsOpen = false;
        this.key = '';
    }
    // LocationMoveUpsertModal 저장 클릭 시
    @action.bound
    locationMoveUpsertModalSave = async () => {

        const validation = this.validator.validate(this);

        if (!validation.isValid) {
            return;
        }

        const saveData = {
            "locNm": this.locationMoveUpsertModalLocNm
            , "moveDt": this.locationMoveUpsertModalMoveDt
            , "remark": this.locationMoveUpsertModalRemark

            , "buId": this.buId
            , "startDt": this.startDt
        }

        //saveData로 이동
        // const searchData = {
        //     "buId": this.buId,
        //     "startDt": this.startDt
        // }

        // if (!selectedRows || selectedRows.length == 0) {
        //     Alert.meg("바코드를 선택해 주세요.");
        //     return;
        // }

        // const params = JSON.stringify({ param1: saveData, param2: searchData });
        const value = await Alert.confirm("저장하시겠습니까?").then(result => { return result; });
        if (value) {
            QualityStatusStore.holdBarcodeGridApi.selectAll();
            const selectedRows = QualityStatusStore.holdBarcodeGridApi.getSelectedRows();
            const params = JSON.stringify({ param1: selectedRows, param2: saveData });
            const { data, status } = await LocationMoveRepository.saveLocationMoveOrder(params);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                this.locationLocationList = data.data;
                this.locationLocationGridApi.setRowData(this.locationLocationList);
                this.locationMoveUpsertModalIsOpen = false;
                //key값 초기화
                this.key = '';
                GridUtil.setFocusByRowIndex(this.locationLocationGridApi, this.locationLocationList.length - 1);
            }
        } else {
            QualityStatusStore.holdBarcodeGridApi.deselectAll();
            return;
        }
    }

    // LocationMoveModal 닫기
    @action.bound
    closeLocationMoveModal = async () => {
        this.locationMoveModalIsOpen = false;
    }
    // LocationMoveModal 조회 클릭 시
    @action.bound
    locationMoveModalSearch = async () => {
        const searchData = {
            "glclass": this.locationMoveModalGlclass
            , "itemNmOrItemCd": this.locationMoveModalItemNmOrItemCd
            , "barcode": this.locationMoveModalBarcode
            , "locNm": this.locationMoveModalLocNm
        }
        const { data, status } = await LocationMoveRepository.getItems(searchData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            this.locationMoveModalList = data.data;
            this.locationMoveModalGridApi.setRowData(this.locationMoveModalList);
        }
    }
    // LocationMoveModal 이동 클릭 시
    @action.bound
    locationMoveModalMoving = async () => {
        const saveData = {
            "orderNo": this.disuseUpsertModalOrderNo
            , "outDisuseDt": this.disuseUpsertModalOutDisuseDt
            , "remark": this.disuseUpsertModalRemark
        }
        // const searchData = {
        //     "buId": this.buId
        //     , "startDt": this.disuseUpsertModalOutDisuseDt
        //     , "itemNmOrItemCd": this.itemNmOrItemCd
        // }
        // const params = JSON.stringify({ param1: saveData, param2: searchData });
        // const { data, status } = await DisuseRepository.saveDisuse(params);
        // if (data.success == false) {
        //     Alert.meg(data.msg);
        // } else {
        //     this.disuseDisuseList = data.data;
        //     this.disuseDisuseGridApi.setRowData(this.disuseDisuseList);
        // }
        // this.closeDisuseUpsertModal();
    }
    // LocationMoveModal 그리드
    @action.bound
    setLocationMoveModalGridApi = async (gridApi) => {
        this.locationMoveModalGridApi = gridApi
        const columnDefs = [
            {
                headerName: "제품코드"
                , field: "itemCd"
                , width: 90
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "제품명"
                , field: "itemNm"
                , width: 100
            },
            {
                headerName: "LOT NO"
                , field: "lotNo"
                , width: 90
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "바코드"
                , field: "barcode"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "위치명"
                , field: "locNm"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "현재고"
                , field: "processQty"
                , width: 100
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
            },
            {
                headerName: "단위"
                , field: "processUom"
                , width: 65
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "생산일자"
                , field: "prodDt"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "유통기한"
                , field: "expiryDt"
                , width: 100
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            }
        ];
        this.locationMoveModalGridApi.setColumnDefs(columnDefs);
    }

    // LocationBarcodeUpsertModal 조회 클릭 시
    @action.bound
    locationBarcodeUpsertModalSearch = async () => {
        const searchData = {
            "buId": this.buId
            , "barcode": this.locationBarcodeUpsertModalBarcode
            , "prodDt": this.locationBarcodeUpsertModalProdDt
            , "itemNmOrItemCd": this.locationBarcodeUpsertModalItemNmOrItemCd
        }
        const { data, status } = await EtcRepository.getBarcode(searchData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            this.locationBarcodeUpsertModalList = data.data;

            let moveList = [];
            this.locationBarcodeUpsertModalList.forEach(item => {
                if (item.lotQuality == "10") {
                    moveList.push(item);
                }
            })

            this.locationBarcodeUpsertModalGridApi.setRowData(moveList);
        }
    }

    // LocationBarcodeUpsertModal 닫기
    @action.bound
    closeLocationBarcodeUpsertModal = async () => {
        this.locationBarcodeUpsertModalIsOpen = false;
    }

    // LocationBarcodeUpsertModal 저장 클릭 시
    @action.bound
    locationBarcodeUpsertModalSave = async () => {

        if (this.key == 'Y') {
            //지시등록에서 바코드를 추가 저장 할 때
            const selectedRows = this.locationBarcodeUpsertModalGridApi.getSelectedRows();
            if (!selectedRows || selectedRows.length == 0) {
                Alert.meg("바코드를 선택해 주세요.");
                return;
            }

            selectedRows.forEach(item => {
                if (!this.add.some(x => x.barcode == item.barcode)) {
                    this.add.push(item);
                }
            })

            // QualityStatusStore.holdBarcodeList = selectedRows;
            QualityStatusStore.holdBarcodeGridApi.setRowData(this.add);
            this.locationBarcodeUpsertModalIsOpen = false;
            // QualityStatusStore.holdBarcodeList = [];

        } else {
            //지시등록이 아닌 곳에서 바코드 추가 저장 할 때
            
            const selectedRows = this.locationBarcodeUpsertModalGridApi.getSelectedRows();
            if (!selectedRows || selectedRows.length == 0) {
                Alert.meg("바코드를 선택해 주세요.");
                return;
            }
            const searchData = {
                "orderNo": this.locationBarcodeUpsertModalOrderNo
            }

            const params = JSON.stringify({ param1: selectedRows, param2: searchData });
            const { data, status } = await EtcRepository.saveBarcode(params);
            if (data.success == false) {
                Alert.meg(data.msg);
                return;
            } else {
                this.locationBarcodeList = data.data;
                this.locationBarcodeGridApi.setRowData(this.locationBarcodeList);
                this.locationBarcodeUpsertModalIsOpen = false;
            }

        }


    }

    // LocationBarcodeUpsertModal 그리드
    @action.bound
    setLocationBarcodeUpsertModalGridApi = async (gridApi) => {
        this.locationBarcodeUpsertModalGridApi = gridApi;
        const columnDefs = [
            {
                headerName: ""
                , field: "check"
                , width: 29
                , checkboxSelection: true
                , headerCheckboxSelection: true
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "순번"
                , field: "seq"
                , width: 40
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
            },
            {
                headerName: "바코드번호"
                , field: "barcode"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "상태"
                , field: "lotQualityNm"
                , width: 50
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "제품코드"
                , field: "itemCd"
                , width: 90
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "LOT NO"
                , field: "lotNo"
                , width: 80
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "생산일자"
                , field: "prodDt"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "유통기한"
                , field: "expiryDt"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "재고량"
                , field: "processQty"
                , width: 60
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
            },
            {
                headerName: "위치"
                , field: "locNm"
                , width: 120
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "처리시간"
                , field: "inputDtm"
                , width: 110
                , cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm')
            },
            {
                headerName: "처리자"
                , field: "inputEmpNm"
                , width: 90
                , cellClass: 'cell_align_center'
            }
        ];
        this.locationBarcodeUpsertModalGridApi.setColumnDefs(columnDefs);
    }

    // locationHelper 열기
    @action.bound
    openLocationHelper = async () => {
        this.locationBuId = this.buId;
        this.isLocationOpen = true;
    }
    // locationHelper 선택 반환
    @action.bound
    locationHelperResult = async (result) => {
        this.locationMoveUpsertModalLocNm = result.locNm;
    }
}
export default new LocationMoveStore();