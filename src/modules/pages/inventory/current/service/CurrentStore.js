import { observable, action } from 'mobx';
import moment from 'moment';
import 'moment/locale/ko';

import GridUtil from 'components/override/grid/utils/GridUtil';

import CommonStore from 'modules/pages/common/service/CommonStore';
import BaseStore from 'utils/base/BaseStore';

import CurrentRepository from 'modules/pages/inventory/current/service/CurrentRepository';
import ItemCategoryRepository from 'modules/pages/system/itemCategory/service/ItemCategoryRepository';

class CurrentStore extends BaseStore {

    constructor() {
        super()
        this.setInitialState();
    }

    //검색조건 변수
    @observable buId = CommonStore.buId;
    @observable buList = CommonStore.buList;
    @observable itemCdNm = '';
    @observable lotQuality = '';
    @observable invenCheck = 'Y';
    @observable invenDt = moment().format('YYYYMMDD');
    @observable subTotalId = '01';
    @observable uomGubn = 10;
    @observable categoryCd = '';
    @observable categoryGroup = [];
    // @observable qtyCd = '01,02,03,04,05';
    // qtyGroup = [
    //     {
    //         qtyCd: '01'
    //         , qtyNm: 'PL'
    //     },
    //     {
    //         qtyCd: '02'
    //         , qtyNm: '생산'
    //     },
    //     {
    //         qtyCd: '03'
    //         , qtyNm: '재고'
    //     },
    //     {
    //         qtyCd: '04'
    //         , qtyNm: '가용'
    //     },
    //     {
    //         qtyCd: '05'
    //         , qtyNm: '홀드'
    //     }
    // ];

    //현재고 그리드 변수
    currentList = [];
    currentGridApi = undefined;
    @observable pinnedBottomRowData = [];

    @observable alphaCheck = false;


    //공장 불러오기
    @action.bound
    getBu = async () => {
        this.buId = CommonStore.buId;
        this.alphaCheck = true;
    }

    @action.bound
    handleChange = (data) => {
        this[data.id] = data.value;
    }

    // search 공장 선택시 위치List 변경
    @action.bound
    buSelectionChanged = async () => {
        this.setItemCategory();
    }

    // 조회
    @action.bound
    handleSearchClick = async () => {

        const searchData = {
            "buId": this.buId
            , "itemCdNm": this.itemCdNm
            , "lotQuality": this.lotQuality
            , "invenCheck": this.invenCheck
            , "invenDt": this.invenDt
            , "subTotalId": this.subTotalId
            , "uomGubn": this.uomGubn
            , "categoryCd": this.categoryCd
        }

        const { data, status } = await CurrentRepository.getCurrents(searchData);

        if (status == 200) {
            this.currentList = data.data;
            this.currentGridApi.setColumnDefs(this.getColumnDefs());
            this.currentGridApi.setRowData(this.currentList);
        }
    }

    getColumnDefs = () => {

        const subTotalId02 = subTotalId.value == '02' ? true : false;
        // const visible01 = this.qtyCd.indexOf('01') >= 0 ? false : true;
        // const visible02 = this.qtyCd.indexOf('02') >= 0 ? false : true;
        // const visible03 = this.qtyCd.indexOf('03') >= 0 ? false : true;
        // const visible04 = this.qtyCd.indexOf('04') >= 0 ? false : true;
        // const visible05 = this.qtyCd.indexOf('05') >= 0 ? false : true;
        
        return [

            { headerName: "공장ID", field: "buId", width: 100, hide: true },
            {
                headerName: "공장"
                , field: "buNm"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "제품코드"
                , field: "itemCd"
                , width: 80
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "제품명"
                , field: "itemNm"
                , width: 150
                , cellClass: 'cell_align_left'
            },
            {
                headerName: "제품규격"
                , field: "spec"
                , width: 135
                , cellClass: 'cell_align_left'
            },
            {
                headerName: "LOT"
                , field: "lotNo"
                , width: 80
                , cellClass: 'cell_align_center'
                , hide: subTotalId02
            },
            {
                headerName: "생산일자"
                , field: "prodDt"
                , width: 100
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
                , hide: subTotalId02
            },
            {
                headerName: "유통기한"
                , field: "expiryDt"
                , width: 100
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
                , hide: subTotalId02
            },
            {
                headerName: "단위"
                , field: "fromUom"
                , width: 50
                , cellClass: 'cell_align_center'
                , type: "numericColumn"
            },
            {
                headerName: "PL수량"
                , field: "plQty"
                , width: 70
                , cellClass: 'cell_align_right'
                , cellStyle: { "color": "#0054FF" }
                
            },
            {
                headerName: "생산수량"
                , field: "prodQty"
                , width: 70
                , cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
                , cellStyle: { "color": "#0054FF" }
                
            },
            {
                headerName: "재고수량"
                , field: "invenQty"
                , width: 70
                , cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
                , cellStyle: { "color": "#0054FF" }
                
            },
            {
                headerName: "가용수량"
                , field: "availableQty"
                , width: 70
                , cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
                , cellStyle: { "color": "#0054FF" }
                
            },
            {
                headerName: "홀드수량"
                , field: "holdQty"
                , width: 70
                , cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
                , cellStyle: { "color": "#0054FF" }
                
            },
            {
                headerName: "불용수량"
                , field: "disuseQty"
                , width: 70
                , cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
                , cellStyle: { "color": "#0054FF" }
                
            }
        ]
    }

    //제퓸군 목록
    @action.bound
    setItemCategory = async () => {

        const params = {
            buId: this.buId
        }

        const { data, status } = await ItemCategoryRepository.getItemCategory(params);

        if (data.success) {
            this.categoryGroup = data.data;
        }
    }

    @action.bound
    setGridApi = async (gridApi) => {
        this.currentGridApi = gridApi;
        this.pinnedBottomRowData = [
            {
                buNm: '합계',
                plQty: 0,
                prodQty: 0,
                invenQty: 0,
                availableQty: 0,
                holdQty: 0,
                disuseQty: 0
            }
        ];
        this.currentGridApi.setColumnDefs(this.getColumnDefs());
    }
}

export default new CurrentStore();
