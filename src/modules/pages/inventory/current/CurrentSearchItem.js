import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SButton from 'components/atoms/button/SButton';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SCheckBox from 'components/atoms/checkbox/SCheckBox';
import STreeSelectBox from 'components/override/tree/STreeSelectBox';
import SBuSelectBox from 'components/override/business/SBuSelectBox';

import CurrentStore from 'modules/pages/inventory/current/service/CurrentStore';

@inject(stores => ({
      buId: stores.currentStore.buId
    , invenDt: stores.currentStore.invenDt
    , subTotalId: stores.currentStore.subTotalId
    , invenCheck: stores.currentStore.invenCheck
    , categoryCd: stores.currentStore.categoryCd
    , categoryGroup: stores.currentStore.categoryGroup
    , itemCdNm: stores.currentStore.itemCdNm
    , lotQuality: stores.currentStore.lotQuality
    , handleChange: stores.currentStore.handleChange
    , handleSearchClick: stores.currentStore.handleSearchClick
    , uomGubn: stores.currentStore.uomGubn
    , buSelectionChanged: stores.currentStore.buSelectionChanged
    // , qtyCd: stores.currentStore.qtyCd
    // , qtyGroup: stores.currentStore.qtyGroup
    , setItemCategory: stores.currentStore.setItemCategory
}))

class CurrentSearchItem extends Component {

    constructor(props) {
        super(props);
    }

    async componentWillMount() {
        if (CurrentStore.alphaCheck == false) {
            await CurrentStore.getBu();
            await this.props.setItemCategory();
        }
    }

    render() {
        const subTotaloption = [
            {
                cd: "01",
                cdNm: "제품,일자"
            },
            {
                cd: "02",
                cdNm: "제품"
            }
        ];

        const {   buId
                , invenDt
                , subTotalId
                , invenCheck
                , categoryCd
                , categoryGroup
                , itemCdNm
                , lotQuality
                , handleChange
                , handleSearchClick
                , uomGubn
                , buSelectionChanged
                // , qtyCd
                // , qtyGroup
            } = this.props;

        return (
            
            <React.Fragment>
                <div className='search_item_btn'>
                    <SButton
                        buttonName={'조회'}
                        type={'btnSearch'}
                        onClick={handleSearchClick}
                    />
                </div>
                <div style={{ display: "inline_block" }}>
                    <div className='search_line'>
                        <SBuSelectBox
                            title={"공장"}
                            id={"buId"}
                            value={buId}
                            onChange={handleChange}
                            onSelectionChange={buSelectionChanged}
                        />

                        <SDatePicker
                            title={"기준일자"}
                            id={"invenDt"}
                            value={invenDt}
                            onChange={handleChange}
                        />

                        <SCheckBox
                            title={"실재고여부"}
                            id={"invenCheck"}
                            value={invenCheck}
                            onChange={handleChange}
                            
                        />
                    
                        <SSelectBox
                            title={"그룹옵션"}
                            id={"subTotalId"}
                            value={subTotalId}
                            optionGroup={subTotaloption}
                            onChange={handleChange}
                            marginLeft={78}
                        />
                        
                        {/* <STreeSelectBox
                            title={"수량그룹"}
                            id={"qtyCd"}
                            value={qtyCd}
                            optionGroup={qtyGroup}
                            titleMember={"qtyNm"}
                            valueMember={"qtyCd"}
                            onChange={handleChange}
                            contentWidth={200}
                        /> */}

                    </div>
                    <div className='search_line'>

                        <STreeSelectBox 
                            title={"제품군"}
                            id={"categoryCd"}
                            value={categoryCd}
                            optionGroup={categoryGroup}
                            pIdMember={"pcategoryCd"}
                            titleMember={"categoryNm"}
                            valueMember={"categoryCd"}
                            onChange={handleChange}
                            contentWidth={310}
                            
                        />
                        <SInput
                            title={"제품"}
                            id={"itemCdNm"}
                            value={itemCdNm}
                            onChange={handleChange}
                            onEnterKeyDown={handleSearchClick}
                        />
                        <SSelectBox
                            title={"품질상태"}
                            id={"lotQuality"}
                            value={lotQuality}
                            codeGroup={"MFD003"}
                            onChange={handleChange}
                            addOption={"전체"}
                        />

                        <SSelectBox
                            title={"기준단위"}
                            id={"uomGubn"}
                            value={uomGubn}
                            codeGroup={"MFD015"}
                            onChange={handleChange}
                        />
                    </div>
                </div>   
            </React.Fragment>
        )
    }
}

export default CurrentSearchItem;
