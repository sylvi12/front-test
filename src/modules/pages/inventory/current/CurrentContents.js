import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid';

@inject(stores => ({
    setGridApi: stores.currentStore.setGridApi
    , currentList: stores.currentStore.currentList
    , pinnedBottomRowData: stores.currentStore.pinnedBottomRowData
}))

class CurrentContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setGridApi, currentList, pinnedBottomRowData } = this.props;

        return (
            <SGrid
                grid={'currentGrid'}
                gridApiCallBack={setGridApi}
                rowData={currentList}
                cellReadOnlyColor={true}
                editable={false}
                pinnedBottomRowData={pinnedBottomRowData}
            />
        );
    }
}
export default CurrentContents;