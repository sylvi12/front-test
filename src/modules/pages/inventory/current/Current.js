import React, { Component } from "react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import CurrentSearchItem from "modules/pages/inventory/current/CurrentSearchItem";
import CurrentContents from "modules/pages/inventory/current/CurrentContents";

class Current extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        return (
            <React.Fragment>
                <SearchTemplate searchItem={<CurrentSearchItem />} />
                <ContentsMiddleTemplate contentSubTitle={'현재고 현황'}/>
                <ContentsTemplate id="current" contentItem={<CurrentContents />} />  
            </React.Fragment>
        )
    }
}

export default Current;