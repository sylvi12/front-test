import { observable, action } from 'mobx';
import moment from 'moment';
import 'moment/locale/ko';

import GridUtil from 'components/override/grid/utils/GridUtil';

import CommonStore from 'modules/pages/common/service/CommonStore';
import BaseStore from 'utils/base/BaseStore';

import CompareRepository from 'modules/pages/inventory/compare/service/CompareRepository';
import ItemCategoryRepository from 'modules/pages/system/itemCategory/service/ItemCategoryRepository';

class CompareStore extends BaseStore {

    constructor() {
        super()
        this.setInitialState();
    }

    //검색조건 변수
    @observable buId = CommonStore.buId;
    @observable buList = CommonStore.buList;
    @observable item = '';
    @observable invenFromDt = moment().add(-1, 'days').format('YYYYMMDD');
    @observable invenToDt = moment().format('YYYYMMDD');
    @observable uomGubn = 10;
    @observable categoryCd = '';
    @observable categoryGroup = [];

    //현재고 그리드 변수
    compareList = [];
    compareGridApi = undefined;
    @observable pinnedBottomRowData = [];

    @observable alphaCheck = false;

    //공장 불러오기
    @action.bound
    getBu = async () => {
        this.alphaCheck = true;
    }

    @action.bound
    handleChange = (data) => {
        this[data.id] = data.value;
    }

    // search 공장 선택시 위치List 변경
    @action.bound
    buSelectionChanged = async () => {
        this.setItemCategory();
    }

    // 조회
    @action.bound
    handleSearchClick = async () => {

        const searchData = {
            "buId": this.buId
            , "item": this.item
            , "invenFromDt": this.invenFromDt
            , "invenToDt": this.invenToDt
            , "uomGubn": this.uomGubn
        }

        const { data, status } = await CompareRepository.getCompare(searchData);

        if (status == 200) {
            this.compareList = data.data;
            this.compareGridApi.setRowData(this.compareList);
        }
    }

    getColumnDefs = () => {

        const qtyWidth = 80;

        return [
            {
                headerName: "공장ID"
                , field: "buId"
                , width: 100
                , hide: true
            },
            {
                headerName: "제품ID"
                , field: "itemId"
                , width: 80
                , cellClass: 'cell_align_center'
                , pinned: "left"
            },
            {
                headerName: "제품코드"
                , field: "itemCd"
                , width: 80
                , cellClass: 'cell_align_center'
                , pinned: "left"
            },
            {
                headerName: "제품명"
                , field: "itemNm"
                , width: 150
                , pinned: "left"
            },
            {
                headerName: "제품규격"
                , field: "spec"
                , width: 135
                , pinned: "left"
            },
            {
                headerName: "단위"
                , field: "uom"
                , width: 50
                , cellClass: 'cell_align_center'
                , pinned: "left"
            },
            {
                headerName: "생산"
                , children: [
                    {
                        headerName: "WMS"
                        , children: [
                            {
                                headerName: "생산량"
                                , field: "wmsProdQty"
                                , width: qtyWidth
                                , cellClass: 'cell_align_right'
                                , valueFormatter: GridUtil.numberFormatter
                            },
                        ],
                    },
                    {
                        headerName: "ERP"
                        , children: [
                            {
                                headerName: "생산량"
                                , field: "erpProdQty"
                                , width: qtyWidth
                                , cellClass: 'cell_align_right'
                                , valueFormatter: GridUtil.numberFormatter
                            },
                            {
                                headerName: "ERP미전송량"
                                , field: "erpNotSentQty"
                                , width: qtyWidth
                                , cellClass: 'cell_align_right'
                                , valueFormatter: GridUtil.numberFormatter
                            },
                        ],
                    }
                ]
            },
            {
                headerName: "재고"
                , children: [
                    {
                        headerName: "WMS"
                        , children: [
                            {
                                headerName: "전체"
                                , field: "wmsInvenQty"
                                , width: qtyWidth
                                , cellClass: 'cell_align_right bold'
                                , valueFormatter: GridUtil.numberFormatter
                            },
                            {
                                headerName: "가용"
                                , field: "wmsAvailableQty"
                                , width: qtyWidth
                                , cellClass: 'cell_align_right'
                                , valueFormatter: GridUtil.numberFormatter        
                            },
                            {
                                headerName: "HOLD"
                                , field: "wmsHoldQty"
                                , width: qtyWidth
                                , cellClass: 'cell_align_right'
                                , valueFormatter: GridUtil.numberFormatter
        
                            },
                            {
                                headerName: "불용"
                                , field: "wmsDisuseQty"
                                , width: qtyWidth
                                , cellClass: 'cell_align_right'
                                , valueFormatter: GridUtil.numberFormatter
                            },
                        ]
                    },
                    {
                        headerName: "ERP"
                        , children: [
                            {
                                headerName: "전체"
                                , field: "erpInvenQty"
                                , width: qtyWidth
                                , cellClass: 'cell_align_right bold'
                                , valueFormatter: GridUtil.numberFormatter
                            },
                            {
                                headerName: "가용"
                                , field: "erpAvailableQty"
                                , width: qtyWidth
                                , cellClass: 'cell_align_right'
                                , valueFormatter: GridUtil.numberFormatter
                            },
                            {
                                headerName: "불용"
                                , field: "erpDisuseQty"
                                , width: qtyWidth
                                , cellClass: 'cell_align_right'
                                , valueFormatter: GridUtil.numberFormatter
                            },
                            {
                                headerName: "(임시)기타"
                                , field: "erpEtcQty"
                                , width: qtyWidth
                                , cellClass: 'cell_align_right'
                                , valueFormatter: GridUtil.numberFormatter
                            },
                            {
                                headerName: "SLS출고미처리"
                                , field: "slsWaitingQty"
                                , width: qtyWidth + 30
                                , cellClass: 'cell_align_right'
                                , valueFormatter: GridUtil.numberFormatter
                            },
                        ]
                    }
                ]
            },
        ]
    }

    //제퓸군 목록
    @action.bound
    setItemCategory = async () => {

        const params = {
            buId: this.buId
        }
        const { data, status } = await ItemCategoryRepository.getItemCategory(params);

        if (data.success) {
            this.categoryGroup = data.data;
        }

        this.alphaCheck = true;
    }

    @action.bound
    setGridApi = async (gridApi) => {
        this.compareGridApi = gridApi;
        this.pinnedBottomRowData = [
            {
                buNm: '합계',
                wmsProdQty: 0,
                erpProdQty: 0,
                erpNotSentQty: 0,
                wmsInvenQty: 0,
                wmsAvailableQty: 0,
                wmsHoldQty: 0,
                wmsDisuseQty: 0,
                erpInvenQty: 0,
                erpAvailableQty: 0,
                erpDisuseQty: 0,
                slsWaitingQty: 0,
            }
        ];
        this.compareGridApi.setColumnDefs(this.getColumnDefs());
    }
}

export default new CompareStore();
