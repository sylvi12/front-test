import API from "components/override/api/API";

class CurrentRepository {
    
    URL = "/api/inventory/compare";
    
    // 조회
    getCompare(params) {
        return API.request.get(encodeURI(`${this.URL}/search?buId=${params.buId}&invenFromDt=${params.invenFromDt}&invenToDt=${params.invenToDt}&item=${params.item}&uomGubn=${params.uomGubn}`))
    }
}
export default new CurrentRepository();