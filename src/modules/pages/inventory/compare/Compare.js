import React, { Component } from "react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import CompareSearchItem from "modules/pages/inventory/compare/CompareSearchItem";
import CompareContents from "modules/pages/inventory/compare/CompareContents";

class Compare extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        return (
            <React.Fragment>
                <SearchTemplate searchItem={<CompareSearchItem />} />
                <ContentsMiddleTemplate contentSubTitle={'재고 비교'}/>
                <ContentsTemplate id="compare" contentItem={<CompareContents />} />  
            </React.Fragment>
        )
    }
}

export default Compare;