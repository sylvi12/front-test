import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SButton from 'components/atoms/button/SButton';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SCheckBox from 'components/atoms/checkbox/SCheckBox';
import STreeSelectBox from 'components/override/tree/STreeSelectBox';
import SBuSelectBox from 'components/override/business/SBuSelectBox';

@inject(stores => ({
    buId: stores.compareStore.buId
    , invenToDt: stores.compareStore.invenToDt
    , invenFromDt: stores.compareStore.invenFromDt
    , item: stores.compareStore.item
    , handleChange: stores.compareStore.handleChange
    , handleSearchClick: stores.compareStore.handleSearchClick
    , uomGubn: stores.compareStore.uomGubn
    , alphaCheck: stores.compareStore.alphaCheck
}))

class CompareSearchItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { buId
            , invenFromDt
            , invenToDt
            , item
            , handleChange
            , handleSearchClick
            , uomGubn
        } = this.props;

        return (

            <React.Fragment>
                <div className='search_item_btn'>
                    <SButton
                        buttonName={'조회'}
                        type={'btnSearch'}
                        onClick={handleSearchClick}
                    />
                </div>
                <div style={{ display: "inline_block" }}>
                    <div className='search_line'>
                        <SBuSelectBox
                            title={"공장"}
                            id={"buId"}
                            value={buId}
                            isAddAll={false}
                            onChange={handleChange}
                        />

                        <SDatePicker
                            title={"기준일자"}
                            id={"invenFromDt"}
                            value={invenFromDt}
                            onChange={handleChange}
                        />

                        <SDatePicker
                            title={"~"}
                            id={"invenToDt"}
                            value={invenToDt}
                            onChange={handleChange}
                        />

                        <SInput
                            title={"제품"}
                            id={"item"}
                            value={item}
                            onChange={handleChange}
                            onEnterKeyDown={handleSearchClick}
                        />

                        <SSelectBox
                            title={"기준단위"}
                            id={"uomGubn"}
                            value={uomGubn}
                            codeGroup={"MFD015"}
                            onChange={handleChange}
                        />
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default CompareSearchItem;
