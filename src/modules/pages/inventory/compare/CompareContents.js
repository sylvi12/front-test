import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid';

@inject(stores => ({
    setGridApi: stores.compareStore.setGridApi
    , compareList: stores.compareStore.compareList
    , pinnedBottomRowData: stores.compareStore.pinnedBottomRowData
}))

class CompareContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setGridApi, compareList, pinnedBottomRowData } = this.props;

        return (
            <SGrid
                grid={'compareGrid'}
                gridApiCallBack={setGridApi}
                rowData={compareList}
                editable={false}
                pinnedBottomRowData={pinnedBottomRowData}
            />
        );
    }
}

export default CompareContents;