import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'
import BarcodeHistoryStore from "modules/pages/inventory/barcodeHistory/service/BarcodeHistoryStore";

@inject(stores => ({
    setGridGrpApi: stores.barcodeHistoryStore.setGridGrpApi,
    barcodeHistoryGrpList: stores.barcodeHistoryStore.barcodeHistoryGrpList
    , pinnedBottomGrpRowData: stores.barcodeHistoryStore.pinnedBottomGrpRowData
}))

class BarcodeHistoryGrpContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setGridGrpApi, barcodeHistoryGrpList, pinnedBottomGrpRowData } = this.props;

        return (
            <SGrid
                grid={'barcodeHistoryGrpGrid'}
                gridApiCallBack={setGridGrpApi}
                rowData={barcodeHistoryGrpList}
                cellReadOnlyColor={false}
                editable={false}
                pinnedBottomRowData={pinnedBottomGrpRowData}
            />
        );
    }
}
export default BarcodeHistoryGrpContents;