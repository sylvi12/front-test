import React, { Component } from "react";
import { inject } from "mobx-react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";
import SModal from "components/override/modal/SModal";

import BarcodeHistorySearchItem from "modules/pages/inventory/barcodeHistory/BarcodeHistorySearchItem";
import BarcodeHistoryMiddleItem from "modules/pages/inventory/barcodeHistory/BarcodeHistoryMiddleItem";
import BarcodeHistoryContents from "modules/pages/inventory/barcodeHistory/BarcodeHistoryContents";
import BarcodeHistoryGrpContents from "modules/pages/inventory/barcodeHistory/BarcodeHistoryGrpContents";

import BarcodeHistoryModal from "modules/pages/inventory/barcodeHistory/barcodehistorymodal/BarcodeHistoryModal";
import BarcodeHistoryLocChgModal from "modules/pages/inventory/barcodeHistory/barcodehistorymodal/BarcodeHistoryLocChgModal";
import BarcodeHistoryOrderDetailModal from "modules/pages/inventory/barcodeHistory/barcodehistorymodal/BarcodeHistoryOrderDetailModal";
import DivideModal from "modules/pages/inventory/barcodeHistory/divideModal/DivideModal";
import MergeModal from "modules/pages/inventory/barcodeHistory/mergeModal/MergeModal";
import { AbstractSelectionHandle } from "ag-grid-enterprise/dist/lib/widgets/selection/abstractSelectionHandle";
import OrderHelper from "components/codehelper/OrderHelper";
import RackTypeModal from "modules/pages/inventory/barcodeHistory/racktypeModal/RackTypeModal";

import BarcodeHistoryStore from "modules/pages/inventory/barcodeHistory/service/BarcodeHistoryStore";


@inject(stores => ({
    orderModalIsOpen: stores.barcodeHistoryStore.orderModalIsOpen
    , closeModal: stores.barcodeHistoryStore.closeModal
    , locChgModalIsOpen: stores.barcodeHistoryStore.locChgModalIsOpen
    , closeLocChgModal: stores.barcodeHistoryStore.closeLocChgModal
    , orderHelperModalIsOpen: stores.barcodeHistoryStore.orderHelperModalIsOpen
    , divideModalIsOpen: stores.barcodeHistoryStore.divideModalIsOpen
    , closeDivideModal: stores.barcodeHistoryStore.closeDivideModal
    , mergeModalIsOpen: stores.barcodeHistoryStore.mergeModalIsOpen
    , closeMergeModal: stores.barcodeHistoryStore.closeMergeModal
    , handleChange: stores.barcodeHistoryStore.handleChange
    , orderNo_h: stores.barcodeHistoryStore.orderNo_h
    , rackTypeModalIsOpen: stores.barcodeHistoryStore.rackTypeModalIsOpen
    , closeRackTypeModal: stores.barcodeHistoryStore.closeRackTypeModal
}))


class BarcodeHistory extends Component {

    constructor(props) {
        super(props);
    }

    async componentWillMount() {
        if (BarcodeHistoryStore.buIdCheck == false) {
            await BarcodeHistoryStore.getBuId();
        }
    }

    render() {

        const { orderModalIsOpen, closeModal, locChgModalIsOpen, closeLocChgModal, orderHelperModalIsOpen
            , divideModalIsOpen, closeDivideModal, mergeModalIsOpen, closeMergeModal, handleChange, orderNo_h
            , rackTypeModalIsOpen, closeRackTypeModal
        } = this.props;
        return (
            <React.Fragment>
                <div style={{ width: '100%', height: '60%', float: 'left' }}>
                    <SearchTemplate searchItem={<BarcodeHistorySearchItem />} />
                    <ContentsMiddleTemplate contentSubTitle={'바코드 내역'} middleItem={<BarcodeHistoryMiddleItem />} />
                    <ContentsTemplate id='barcode' contentItem={<BarcodeHistoryContents />} />
                </div>
                <div style={{ width: '100%', height: '40%', float: 'left' }}>
                    <ContentsMiddleTemplate contentSubTitle={'바코드 내역 집계'} />
                    <ContentsTemplate id='barcodeHistoryGrp' contentItem={<BarcodeHistoryGrpContents />} />
                </div>

                <SModal
                    formId={"inout_inout_locChg_p"}
                    isOpen={locChgModalIsOpen}
                    onRequestClose={closeLocChgModal}
                    contentLabel="위치변경"
                    width={400}
                    height={300}
                    contents={<BarcodeHistoryLocChgModal />}
                />

                <SModal
                    formId={"inventory_ledger_info_p"}
                    isOpen={orderModalIsOpen}
                    onRequestClose={closeModal}
                    contentLabel="수불내역"
                    width={1000}
                    height={500}
                    contents={<BarcodeHistoryModal />}
                />

                {/* <SModal
                    isOpen={orderDetailModalIsOpen}
                    onRequestClose={closeOrderDetailModal}
                    contentLabel="생산정보"
                    width={'80%'} //RefSize.700
                    height={'80%'} //RefSize.450
                    minWidth={800}
                    minHeight={400}
                    maxWidth={1230}
                    maxHeight={580}
                    contents={<BarcodeHistoryOrderDetailModal />}
                /> */}

                <OrderHelper
                    formId={"order_prod_info_p"}
                    id={'orderHelperModalIsOpen'}
                    orderHelperModalIsOpen={orderHelperModalIsOpen}
                    title={'생산정보'}
                    onChange={handleChange}
                    orderNo={orderNo_h}
                //barcodeYn={'N'} // 바코드 관리여부(value: Y/N)
                />

                <SModal
                    formId={"inventory_barcode_divide_p"}
                    isOpen={divideModalIsOpen}
                    onRequestClose={closeDivideModal}
                    contentLabel="분할"
                    width={500}
                    height={500}
                    contents={<DivideModal />}
                />

                <SModal
                    formId={"inventory_barcode_merge_p"}
                    isOpen={mergeModalIsOpen}
                    onRequestClose={closeMergeModal}
                    contentLabel="합짐"
                    width={500}
                    height={800}
                    contents={<MergeModal />}
                />

                <SModal
                    formId={"rack_type_p"}
                    isOpen={rackTypeModalIsOpen}
                    onRequestClose={closeRackTypeModal}
                    contentLabel="RACK TYPE 설정"
                    width={250}
                    height={130}
                    contents={<RackTypeModal />}
                />

            </React.Fragment>
        )
    }
}

export default BarcodeHistory;