import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setGridApi: stores.barcodeHistoryStore.setGridApi,
    barcodeHistoryList: stores.barcodeHistoryStore.barcodeHistoryList,
    getBarcodeGrp: stores.barcodeHistoryStore.getBarcodeGrp
    , pinnedBottomRowData: stores.barcodeHistoryStore.pinnedBottomRowData
}))

class BarcodeHistoryContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setGridApi, barcodeHistoryList, getBarcodeGrp, pinnedBottomRowData } = this.props;

        return (
            <SGrid
                grid={'barcodeHistoryGrid'}
                gridApiCallBack={setGridApi}
                rowData={barcodeHistoryList}
                onCellClicked={getBarcodeGrp}
                suppressRowClickSelection={true}
                cellReadOnlyColor={true}
                editable={false}
                pinnedBottomRowData={pinnedBottomRowData}
            />
        );
    }
}
export default BarcodeHistoryContents;