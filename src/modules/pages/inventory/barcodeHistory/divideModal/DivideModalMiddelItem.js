import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    divideModalSave: stores.barcodeHistoryStore.divideModalSave
    , closeDivideModal: stores.barcodeHistoryStore.closeDivideModal
}))

class DivideModalMiddelItem extends Component {

    render() {

        const { divideModalSave, closeDivideModal } = this.props;
        return (
            <React.Fragment>
                <SButton
                    className="btn_red"
                    buttonName={'저장'}
                    type={'btnBarcodeDivideInsert'}
                    onClick={divideModalSave}
                />
            </React.Fragment>
        )
    }
}

export default DivideModalMiddelItem;