import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SNumericInput from 'components/override/numericinput/SNumericInput';
import SGrid from 'components/override/grid/SGrid';

import DivideModalMiddelItem from "modules/pages/inventory/barcodeHistory/divideModal/DivideModalMiddelItem";
import DivideModalMiddelItemBarcode from "modules/pages/inventory/barcodeHistory/divideModal/DivideModalMiddelItemBarcode";

@inject(stores => ({
    handleChange: stores.barcodeHistoryStore.handleChange

    , divideBarcode: stores.barcodeHistoryStore.divideBarcode
    , divideLotNo: stores.barcodeHistoryStore.divideLotNo

    , divideItemCd: stores.barcodeHistoryStore.divideItemCd
    , divideItemNm: stores.barcodeHistoryStore.divideItemNm

    , divideCurrQty: stores.barcodeHistoryStore.divideCurrQty
    , divideChangeQty: stores.barcodeHistoryStore.divideChangeQty
    , divideUom: stores.barcodeHistoryStore.divideUom

    , divideRemark: stores.barcodeHistoryStore.divideRemark

    , setDivideModalGridApi: stores.barcodeHistoryStore.setDivideModalGridApi
    , divideModalList: stores.barcodeHistoryStore.divideModalList
    , pinnedBottomRowDataAtDivideModalGrid: stores.barcodeHistoryStore.pinnedBottomRowDataAtDivideModalGrid
    , minusQty: stores.barcodeHistoryStore.minusQty
}))
@observer
class DivideModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { handleChange
            , divideBarcode, divideLotNo
            , divideItemCd, divideItemNm
            , divideCurrQty, divideChangeQty, divideUom
            , divideRemark
            , setDivideModalGridApi, divideModalList, pinnedBottomRowDataAtDivideModalGrid, minusQty
        } = this.props;

        return (
            <React.Fragment>
                <div style={{ width: "100%", height: "45%" }}>
                    <div className="middle_wrap" style={{ padding: "0px" }}>
                        <div className="sub_tit">
                            {'현재 LOT 정보'}
                        </div>
                        <ul className="btn_wrap">
                            {<DivideModalMiddelItem />}
                        </ul>
                    </div>
                    <table style={{ width: "100%" }}>
                        <tbody>
                            <tr>
                                <td className="th">바코드</td>
                                <td>
                                    <SInput
                                        id={"divideBarcode"}
                                        value={divideBarcode}
                                        onChange={handleChange}
                                        readOnly={true}
                                    />
                                </td>
                                <td className="th">LOT NO</td>
                                <td>
                                    <SInput
                                        id="divideLotNo"
                                        value={divideLotNo}
                                        onChange={handleChange}
                                        readOnly={true}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td className="th">제품코드</td>
                                <td>
                                    <SInput
                                        id={"divideItemCd"}
                                        value={divideItemCd}
                                        onChange={handleChange}
                                        readOnly={true}
                                    />
                                </td>
                                <td className="th">제품명</td>
                                <td>
                                    <SInput
                                        id={"divideItemNm"}
                                        value={divideItemNm}
                                        onChange={handleChange}
                                        readOnly={true}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td className="th">현재수량</td>
                                <td>
                                    <SNumericInput
                                        id={"divideCurrQty"}
                                        value={divideCurrQty}
                                        onChange={handleChange}
                                        readOnly={true}
                                    />
                                    {divideUom}
                                </td>
                                <td className="th">변경수량</td>
                                <td>
                                    <SNumericInput
                                        id={"divideChangeQty"}
                                        value={divideChangeQty}
                                        onChange={handleChange}
                                        readOnly={true}
                                    />
                                    {divideUom}
                                </td>
                            </tr>
                            <tr>
                                <th>비고</th>
                                <td colSpan="3">
                                    <SInput
                                        id="divideRemark"
                                        value={divideRemark}
                                        onChange={handleChange}
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div style={{ width: "100%", height: "35%" }}>
                    <div className="middle_wrap" style={{ padding: "0px" }}>
                        <div className="sub_tit">
                            {'분할 바코드'}
                        </div>
                        <ul className="btn_wrap">
                            {<DivideModalMiddelItemBarcode />}
                        </ul>
                    </div>
                    <SGrid
                        grid={'divideModalGrid'}
                        gridApiCallBack={setDivideModalGridApi}
                        rowData={divideModalList}
                        pinnedBottomRowData={pinnedBottomRowDataAtDivideModalGrid}
                        onCellValueChanged={minusQty}
                    />
                </div>
            </React.Fragment>
        );
    }
}

export default DivideModal;