import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    divideModalAdd: stores.barcodeHistoryStore.divideModalAdd
    , divideModalDelete: stores.barcodeHistoryStore.divideModalDelete
}))

class DivideModalMiddelItemBarcode extends Component {

    render() {

        const { divideModalAdd, divideModalDelete } = this.props;
        return (
            <React.Fragment>
                <SButton
                    className="btn_red"
                    buttonName={'추가'}
                    type={'btnBarcodeDivideAdd'}
                    onClick={divideModalAdd}
                />
                <SButton
                    className="btn_red"
                    buttonName={'삭제'}
                    type={'btnBarcodeDivideDelete'}
                    onClick={divideModalDelete}
                />
            </React.Fragment>
        )
    }
}

export default DivideModalMiddelItemBarcode;