import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    rackTypeModalRackType: stores.barcodeHistoryStore.rackTypeModalRackType
    , handleChange: stores.barcodeHistoryStore.handleChange
    , btnRackTypeSave: stores.barcodeHistoryStore.btnRackTypeSave
}))
@observer
class RackTypeModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { rackTypeModalRackType, handleChange, btnRackTypeSave } = this.props;

        return (
            <React.Fragment>
                <table style={{ width: "100%" }}>
                    <tbody>
                        <tr>
                            <th>{'RackType'}</th>
                            <td>
                                <SSelectBox
                                    id={"rackTypeModalRackType"}
                                    value={rackTypeModalRackType}
                                    codeGroup={"MFD009"}
                                    onChange={handleChange}
                                />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div>
                    <SButton
                        className="btn_red"
                        buttonName={'저장'}
                        type={'btnRackTypeSave'}
                        onClick={btnRackTypeSave}
                    />
                </div>
            </React.Fragment>
        );
    }
}

export default RackTypeModal;