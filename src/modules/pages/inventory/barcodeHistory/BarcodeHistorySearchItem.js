import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SNumericInput from 'components/override/numericinput/SNumericInput';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SBuSelectBox from 'components/override/business/SBuSelectBox';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SButton from 'components/atoms/button/SButton';

import BarcodeHistoryStore from 'modules/pages/inventory/barcodeHistory/service/BarcodeHistoryStore';

@inject(stores => ({
    buId: stores.barcodeHistoryStore.buId,
    dateType: stores.barcodeHistoryStore.dateType,
    startDate: stores.barcodeHistoryStore.startDate,
    endDate: stores.barcodeHistoryStore.endDate,
    prodId: stores.barcodeHistoryStore.prodId,
    prodGroup: stores.barcodeHistoryStore.prodGroup,
    itemNmOrItemCd: stores.barcodeHistoryStore.itemNmOrItemCd,
    locIds: stores.barcodeHistoryStore.locIds,
    _lotNo: stores.barcodeHistoryStore._lotNo,
    lineGroup: stores.barcodeHistoryStore.lineGroup,
    _lotQuality: stores.barcodeHistoryStore._lotQuality,
    ledgerHistory: stores.barcodeHistoryStore.ledgerHistory,
    _orderNo: stores.barcodeHistoryStore._orderNo,
    _barcode: stores.barcodeHistoryStore._barcode,
    invenOpt: stores.barcodeHistoryStore.invenOpt,
    uomType: stores.barcodeHistoryStore.uomType,
    handleChange: stores.barcodeHistoryStore.handleChange,
    locChange: stores.barcodeHistoryStore.locChange,
    dateSelectionChange: stores.barcodeHistoryStore.dateSelectionChange,
    getLocation: stores.barcodeHistoryStore.getLocation,
    handleSearchClick: stores.barcodeHistoryStore.handleSearchClick,
    getBarcodeGrpBtn: stores.barcodeHistoryStore.getBarcodeGrpBtn
}))

class BarcodeHistorySearchItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        //생산일자or유통기한
        const contype = [
            {
                cd: '01'
                , cdNm: '생산일자'
            }
            , {
                cd: '02'
                , cdNm: '유통기한'
            }
        ]
        const uomTypeSel = [
            {
                cd: '기준단위'
                , cdNm: 10
            }
            , {
                cd: '생산단위'
                , cdNm: 20
            }
            , {
                cd: '물류단위'
                , cdNm: 30
            }
        ]
        const { buId
            , dateType
            , startDate
            , endDate
            , prodId
            , prodGroup
            , itemNmOrItemCd
            , _lotNo
            , locIds
            , lineGroup
            , _lotQuality
            , ledgerHistory
            , _orderNo
            , _barcode
            , invenOpt
            , uomType
            , handleChange
            , getBarcodeGrpBtn
            , dateSelectionChange
            , getLocation
            , handleSearchClick } = this.props;
        return (
            <React.Fragment>
                <div className='search_item_btn'>
                    <SButton
                        buttonName={'조회'}
                        type={'btnSearch'}
                        onClick={handleSearchClick} />
                </div>
                <div className='search_line'>
                    <SBuSelectBox
                        title={"공장"}
                        id={"buId"}
                        value={buId}
                        isAddAll={false}
                        onChange={handleChange}
                        onSelectionChange={getLocation}
                    />

                    <SSelectBox
                        id={"dateType"}
                        value={dateType}
                        optionGroup={contype}
                        valueMember={'cd'}
                        displayMember={'cdNm'}
                        onChange={handleChange}
                        onSelectionChange={dateSelectionChange}
                        contentWidth={80}
                        marginLeft={10}
                    />

                    <SDatePicker
                        id={"startDate"}
                        value={startDate}
                        onChange={handleChange}

                    />
                    <SDatePicker
                        title={"~"}
                        id={"endDate"}
                        value={endDate}
                        onChange={handleChange}
                        marginRight={77}
                    />

                    <SSelectBox
                        title={"제품군"}
                        id={"prodId"}
                        value={prodId}
                        optionGroup={prodGroup}
                        // valueMember={'buId'}
                        // displayMember={'buNm'}
                        onChange={handleChange}
                        addOption={'전체'}
                    />

                    <SInput
                        title={"제품"}
                        id={"itemNmOrItemCd"}
                        value={itemNmOrItemCd}
                        onChange={handleChange}
                        onEnterKeyDown={handleSearchClick}
                        contentWidth={110}
                    />

                </div>
                <div className='search_line'>
                    <SSelectBox
                        title={"위치"}
                        id={"locIds"}
                        value={locIds}
                        optionGroup={lineGroup}
                        valueMember={"locId"}
                        displayMember={"locNm"}
                        onChange={handleChange}
                        //onSelectionChange={handleSearchClick}
                        addOption={"전체"}
                    />

                    <SInput
                        title={"LOT"}
                        id={"_lotNo"}
                        value={_lotNo}
                        onChange={handleChange}
                        onEnterKeyDown={handleSearchClick}
                    />

                    <SSelectBox
                        title={"품질상태"}
                        id={"_lotQuality"}
                        value={_lotQuality}
                        codeGroup={"MFD003"}
                        onChange={handleChange}
                        //onSelectionChange={handleSearchClick}
                        addOption={"전체"}
                    />

                    <SSelectBox
                        title={'수불이력'}
                        id={"ledgerHistory"}
                        value={ledgerHistory}
                        codeGroup={"MFD002"}
                        onChange={handleChange}
                        addOption={"전체"}
                    />
                    <SInput
                        title={"지시번호"}
                        id={"_orderNo"}
                        value={_orderNo}
                        onChange={handleChange}
                        onEnterKeyDown={handleSearchClick}
                    />
                </div>
                <div className='search_line'>
                    <SInput
                        title={"바코드"}
                        id={"_barcode"}
                        value={_barcode}
                        onChange={handleChange}
                        onEnterKeyDown={handleSearchClick}
                    />

                    <SSelectBox
                        title={'재고여부'}
                        id={"invenOpt"}
                        value={invenOpt}
                        codeGroup={"SYS004"}
                        onChange={handleChange}
                        //onSelectionChange={handleSearchClick}
                        addOption={"전체"}
                    />

                    <SSelectBox
                        title={'기준단위'}
                        id={"uomType"}
                        value={uomType}
                        optionGroup={uomTypeSel}
                        valueMember={'cdNm'}
                        displayMember={'cd'}
                        onChange={handleChange}
                        //onSelectionChange={getBarcodeGrpBtn}
                    />
                </div>
            </React.Fragment>
        )
    }
}

export default BarcodeHistorySearchItem;

