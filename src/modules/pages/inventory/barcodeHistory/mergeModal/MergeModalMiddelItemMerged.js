import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    mergeModalAdd: stores.barcodeHistoryStore.mergeModalAdd
    , mergeModalDelete: stores.barcodeHistoryStore.mergeModalDelete
}))

class MergeModalMiddelItemMerged extends Component {

    render() {

        const { mergeModalAdd, mergeModalDelete } = this.props;
        return (
            <React.Fragment>
                <SButton
                    className="btn_red"
                    buttonName={'추가'}
                    type={'btnBarcodeMergeAdd'}
                    onClick={mergeModalAdd}
                />
                <SButton
                    className="btn_red"
                    buttonName={'삭제'}
                    type={'btnBarcodeMergeDelete'}
                    onClick={mergeModalDelete}
                />
            </React.Fragment>
        )
    }
}

export default MergeModalMiddelItemMerged;