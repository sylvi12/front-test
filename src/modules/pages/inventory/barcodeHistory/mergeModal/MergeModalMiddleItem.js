import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    mergeModalSave: stores.barcodeHistoryStore.mergeModalSave
    , closeMergeModal: stores.barcodeHistoryStore.closeMergeModal
}))

class MergeModalMiddleItem extends Component {

    render() {

        const { mergeModalSave, closeMergeModal } = this.props;
        return (
            <React.Fragment>
                <SButton
                    className="btn_red"
                    buttonName={'저장'}
                    type={'btnBarcodeMergeInsert'}
                    onClick={mergeModalSave}
                />
            </React.Fragment>
        )
    }
}

export default MergeModalMiddleItem;