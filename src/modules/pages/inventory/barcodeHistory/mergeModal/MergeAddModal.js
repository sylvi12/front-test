import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import SButton from 'components/atoms/button/SButton';
import SGrid from 'components/override/grid/SGrid';

@inject(stores => ({
    setmergeAddModalGridApi: stores.barcodeHistoryStore.setmergeAddModalGridApi
    , mergeAddModalList: stores.barcodeHistoryStore.mergeAddModalList
    , mergeAddModalAdd: stores.barcodeHistoryStore.mergeAddModalAdd
}))
@observer
class MergeAddModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setmergeAddModalGridApi, mergeAddModalList, mergeAddModalAdd
        } = this.props;

        return (
            <React.Fragment>
                <div className="middle_wrap" style={{ padding: "0px" }}>
                    <div className="btn_wrap">
                        <SButton
                            className="btn_red"
                            buttonName={'선택'}
                            type={'default'}
                            onClick={mergeAddModalAdd}
                        />
                    </div>
                </div>
                <div style={{ width: '100%', height: '90%' }}>
                    <SGrid
                        grid={'mergeAddModalGrid'}
                        gridApiCallBack={setmergeAddModalGridApi}
                        rowData={mergeAddModalList}
                        rowDoubleClick={mergeAddModalAdd}
                        editable={false}
                    />
                </div>
            </React.Fragment>
        );
    }
}

export default MergeAddModal;