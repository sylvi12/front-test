import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SNumericInput from 'components/override/numericinput/SNumericInput';
import SGrid from 'components/override/grid/SGrid';
import SModal from "components/override/modal/SModal";

import MergeModalMiddleItem from "modules/pages/inventory/barcodeHistory/mergeModal/MergeModalMiddleItem";
import MergeModalMiddelItemMerged from "modules/pages/inventory/barcodeHistory/mergeModal/MergeModalMiddelItemMerged";
import MergeAddModal from "modules/pages/inventory/barcodeHistory/mergeModal/MergeAddModal";

@inject(stores => ({
    setBeforeMergeGridApi: stores.barcodeHistoryStore.setBeforeMergeGridApi
    , beforeMergeList: stores.barcodeHistoryStore.beforeMergeList
    , pinnedBottomRowDataAtBeforeMergeGrid: stores.barcodeHistoryStore.pinnedBottomRowDataAtBeforeMergeGrid
    , beforeMergeGridSelectionChange: stores.barcodeHistoryStore.beforeMergeGridSelectionChange

    , handleChange: stores.barcodeHistoryStore.handleChange

    , mergeBarcode: stores.barcodeHistoryStore.mergeBarcode
    , mergeLotNo: stores.barcodeHistoryStore.mergeLotNo

    , mergeItemCd: stores.barcodeHistoryStore.mergeItemCd
    , mergeItemNm: stores.barcodeHistoryStore.mergeItemNm

    , mergeCurrQty: stores.barcodeHistoryStore.mergeCurrQty
    , mergeChangeQty: stores.barcodeHistoryStore.mergeChangeQty

    , mergeRemark: stores.barcodeHistoryStore.mergeRemark

    , setMergedGridApi: stores.barcodeHistoryStore.setMergedGridApi
    , mergedList: stores.barcodeHistoryStore.mergedList
    , pinnedBottomRowDataAtMergedGrid: stores.barcodeHistoryStore.pinnedBottomRowDataAtMergedGrid

    , mergeAddModalIsOpen: stores.barcodeHistoryStore.mergeAddModalIsOpen
    , closeMergeAddModal: stores.barcodeHistoryStore.closeMergeAddModal
}))
@observer
class MergeModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setBeforeMergeGridApi, beforeMergeList, pinnedBottomRowDataAtBeforeMergeGrid, beforeMergeGridSelectionChange
            , handleChange
            , mergeBarcode, mergeLotNo
            , mergeItemCd, mergeItemNm
            , mergeCurrQty, mergeChangeQty
            , mergeRemark
            , setMergedGridApi, mergedList, pinnedBottomRowDataAtMergedGrid
            , mergeAddModalIsOpen, closeMergeAddModal
        } = this.props;

        return (
            <React.Fragment>
                <div style={{ width: "100%", height: "33%" }}>
                    <div className="middle_wrap" style={{ padding: "0px" }}>
                        <div className="sub_tit">
                            {'합짐 바코드 선택'}
                        </div>
                    </div>
                    <div style={{ width: "100%", height: "80%" }}>
                        <SGrid
                            grid={'beforeMergeGrid'}
                            gridApiCallBack={setBeforeMergeGridApi}
                            rowData={beforeMergeList}
                            pinnedBottomRowData={pinnedBottomRowDataAtBeforeMergeGrid}
                            onSelectionChanged={beforeMergeGridSelectionChange}
                        />
                    </div>
                </div>
                <div style={{ width: "100%", height: "25%" }}>
                    <div className="middle_wrap" style={{ padding: "0px" }}>
                        <div className="sub_tit">
                            {'선택 바코드 정보'}
                        </div>
                        <ul className="btn_wrap">
                            {<MergeModalMiddleItem />}
                        </ul>
                    </div>
                    <table style={{ width: "100%" }}>
                        <tbody>
                            <tr>
                                <td className="th">바코드</td>
                                <td>
                                    <SInput
                                        id={"mergeBarcode"}
                                        value={mergeBarcode}
                                        onChange={handleChange}
                                        readOnly={true}
                                    />
                                </td>
                                <td className="th">LOT NO</td>
                                <td>
                                    <SInput
                                        id="mergeLotNo"
                                        value={mergeLotNo}
                                        onChange={handleChange}
                                        readOnly={true}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td className="th">제품코드</td>
                                <td>
                                    <SInput
                                        id={"mergeItemCd"}
                                        value={mergeItemCd}
                                        onChange={handleChange}
                                        readOnly={true}
                                    />
                                </td>
                                <td className="th">제품명</td>
                                <td>
                                    <SInput
                                        id={"mergeItemNm"}
                                        value={mergeItemNm}
                                        onChange={handleChange}
                                        readOnly={true}
                                    />
                                </td>

                            </tr>
                            <tr>
                                <td className="th">현재수량</td>
                                <td>
                                    <SNumericInput
                                        id={"mergeCurrQty"}
                                        value={mergeCurrQty}
                                        onChange={handleChange}
                                        readOnly={true}
                                    />
                                </td>
                                <td className="th">변경수량</td>
                                <td>
                                    <SNumericInput
                                        id={"mergeChangeQty"}
                                        value={mergeChangeQty}
                                        onChange={handleChange}
                                        readOnly={true}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <th>비고</th>
                                <td colSpan="3">
                                    <SInput
                                        id="mergeRemark"
                                        value={mergeRemark}
                                        onChange={handleChange}
                                        contentWidth={355}
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div style={{ width: "100%", height: "33%" }}>
                    <div className="middle_wrap" style={{ padding: "0px" }}>
                        <div className="sub_tit">
                            {'합짐 LOT'}
                        </div>
                        <ul className="btn_wrap">
                            {<MergeModalMiddelItemMerged />}
                        </ul>
                    </div>
                    <SGrid
                        grid={'mergedGrid'}
                        gridApiCallBack={setMergedGridApi}
                        rowData={mergedList}
                        pinnedBottomRowData={pinnedBottomRowDataAtMergedGrid}
                    />
                </div>
                <SModal
                    formId={"inventory_barcode_merge_add_p"}
                    isOpen={mergeAddModalIsOpen}
                    onRequestClose={closeMergeAddModal}
                    contentLabel="추가"
                    width={800}
                    height={500}
                    contents={<MergeAddModal />}
                />
            </React.Fragment>
        );
    }
}

export default MergeModal;