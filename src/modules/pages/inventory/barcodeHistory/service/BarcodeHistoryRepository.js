import API from "components/override/api/API";

class BarcodeHistoryRepository {

    URL = "/api/inventory/barcodeHistory";

    // 메인 output 조회
    getOutput(params) {
        console.log('params', params);
        return API.request.get(encodeURI(`${this.URL}/getOutput?buId=${params.buId}`
            + `&dateType=${params.dateType}&startDate=${params.startDate}&endDate=${params.endDate}`
            + `&prodId=${params.prodId}&itemNmOrItemCd=${params.itemNmOrItemCd}&locId=${params.locId}`
            + `&_lotNo=${params._lotNo}&_lotQuality=${params._lotQuality}&ledgerHistory=${params.ledgerHistory}`
            + `&_orderNo=${params._orderNo}&_barcode=${params._barcode}&invenOpt=${params.invenOpt}&uomType=${params.uomType}`));
        //return API.request.post(`${this.URL}/getOutput`, params)
    }

    getOrderLedger(params) {
        return API.request.get(encodeURI(`${this.URL}/getOrderLedger?lotId=${params.lotId}`
            + `&barcode=${params.barcode}&startDate_l=${params.startDate_l}&endDate_l=${params.endDate_l}&uomType=${params.uomType}`));
    }

    // 바코드 내역 집계 조회(기존)
    // getBarcodeGrp(params) {
    //     return API.request.get(encodeURI(`${this.URL}/getBarcodeGrp?orderNo=${params.orderNo}&uomType=${params.uomType}`));
    // }

    // 바코드 내역 집계 조회(변경)
    getBarcodeGrpMod(param) {
        return API.request.post(encodeURI(`${this.URL}/getbarcodegrpmod`), param);
    }

    // 수불적용일자 변경
    setLedgerChange(params) {
        return API.request.post(encodeURI(`${this.URL}/setLedgerChange`), params);
    }

    getLedgerApModHisModal(params) {
        return API.request.get(encodeURI(`${this.URL}/getLedgerApModHisModal?ledgerId=${params.ledgerId}`));
    }

    getOrderInfo(params) {
        return API.request.get(encodeURI(`${this.URL}/getOrderInfo?orderNo=${params.orderNo}`));
    }

    getOrderInfo2(param) {
        return API.request.get(encodeURI(`${this.URL}/getorderinfo2?orderNo=${param.orderNo}`));
    }

    getOrderList(params) {
        return API.request.get(encodeURI(`${this.URL}/getOrderList?orderNo=${params.orderNo}`));
    }

    getSubOrderList(param) {
        return API.request.get(encodeURI(`${this.URL}/getsuborderlist?orderNo=${param.orderNo}`));
    }

    locationHelperResult(params) {
        return API.request.post(encodeURI(`${this.URL}/locationHelperResult`), params);
    }

    // return API.request.get(encodeURI(`${this.URL}/list?buId=${params.buId}&level=${params.level || ""}&locNm=${params.locNm || ""}`))


    locationMoveInsert(param) {
        return API.request.post(encodeURI(`/api/barcode/move`), param)
    }

    setBarcodeYnCancel(param) {
        return API.request.post(encodeURI(`${this.URL}/setbarcodeyncancel`), param);
    }

    setBarcodeYnUse(param) {
        return API.request.post(encodeURI(`${this.URL}/setbarcodeynuse`), param);
    }

    //생산등록
    createLedgerAutoLabelerByBarcode(params) {
        return API.request.post(encodeURI(`${this.URL}/createLedgerautolabelerbybarcode`), params)
    }

    //선입고등록
    preProdinsertList(params) {
        return API.request.post(encodeURI(`${this.URL}/preprodinsert`), params)
    }


    // 생산취소
    deleteOutputModal(params) {
        return API.request.post(encodeURI(`${this.URL}/deleteoutputmodal`), params)
    }

    //생산량조정
    prodQtyModalSave(param) {
        return API.request.post(encodeURI(`${this.URL}/prodqtymodalsave`), param)
    }












































    // 분할
    divide(param) {
        return API.request.post(encodeURI(`api/inventory/splitmerge/divide`), param)
    }
    // 합짐
    merge(param) {
        return API.request.post(encodeURI(`api/inventory/splitmerge/merge`), param)
    }
    // rackTypeSave
    rackTypeSave(param) {
        console.log(param);
        return API.request.post(encodeURI(`${this.URL}/racktypesave`), param)
    }
}

export default new BarcodeHistoryRepository();