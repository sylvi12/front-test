import { observable, action } from 'mobx';
import moment from 'moment';
import 'moment/locale/ko';

import Alert from 'components/override/alert/Alert';
import GridUtil from 'components/override/grid/utils/GridUtil';

import BarcodeHistoryRepository from 'modules/pages/inventory/barcodeHistory/service/BarcodeHistoryRepository';
import LocationRepository from 'modules/pages/system/location/service/LocationRepository';
import OutputRepository from 'modules/pages/order/output/service/OutputRepository';

import CommonStore from 'modules/pages/common/service/CommonStore';
import BaseStore from 'utils/base/BaseStore';

class BarcodeHistoryStore extends BaseStore {

    // 바코드내역 SearchItem 변수
    @observable buIdCheck = false;
    @observable buId = "";
    @observable lineGroup = []; // 위치
    @observable dateType = '01'; // 생산일자, 유통기한
    @observable startDate = moment().add(-14, 'days').format('YYYYMMDD'); // 기간Start
    @observable endDate = moment().format('YYYYMMDD'); // 기간End
    @observable prodId = ''; // 제품군
    @observable prodGroup = []; // 제품 군 그룹
    @observable itemNmOrItemCd = ''; // 제품
    @observable locId = 0; //위치 -
    @observable locIds = 0; //위치
    @observable _lotNo = ''; // LOT NO - 
    @observable _lotQuality = ''; // 품질상태 - 
    @observable ledgerHistory = ''; // 수불이력
    @observable _orderNo = ''; // 지시번호 -
    @observable _barcode = ''; // 바코드 - 
    @observable invenQty = ''; // 재고수량
    @observable invenOpt = 'Y'; // 재고여부
    @observable lotId = ''; //생산라인??
    @observable uomType = 10;
    @observable prevOrderNo = 0;

    @observable lotNo = ''; // LOT NO - 
    @observable lotQuality = ''; // 품질상태 - 
    @observable orderNo = ''; // 지시번호 -
    @observable barcode = ''; // 바코드 - 

    // ModalBox Event 변수
    @observable locChgModalIsOpen = false;
    @observable orderModalIsOpen = false;
    @observable orderDetailModalIsOpen = false;
    @observable ledgerApModModalIsOpen = false;
    @observable ledgerApModHisModalIsOpen = false;
    @observable orderHelperModalIsOpen = false;

    // 그리드 변수
    @observable barcodeHistoryList = [];
    @observable barcodeHistoryGrpList = [];
    @observable barcodeHistoryLedgerHisList = [];
    @observable orderLedgerDataList = [];
    @observable orderDetailDataList = [];
    barcodeGridApi = undefined;
    barcodeGridGrpApi = undefined;
    ledgerHisGridApi = undefined;
    orderLedgerGridApi = undefined;
    orderDetailGridApi = undefined;
    @observable pinnedBottomRowData = [];
    @observable pinnedBottomGrpRowData = [];

    // 수불관련 변수
    @observable startDate_l = ''; // 수불내역 조건 조회기간Start
    @observable endDate_l = moment().format('YYYYMMDD'); // 수불내역 조건 조회기간End
    @observable orderType = ''; // 수불내역 지시타입
    @observable ledgerId = 0; // 수불적용일자 변경 ledgerId
    @observable tranjDt = ''; // 기존일자
    @observable tranjDtEdit = moment().format('YYYYMMDD'); // 변경일자
    @observable tranjDtReason = ''; // 변경사유

    // 오더관련 변수
    @observable planStartDtm = moment().format('YYYYMMDD');
    @observable planEndDtm = moment().format('YYYYMMDD');
    @observable orderQty = 0;
    @observable prodQty = 0;
    @observable progress = '';
    @observable orderNo_h = '';

    // 생산정보 Summary관련 변수
    @observable pinnedBottomRowData = [];


    constructor() {
        super();
        // this.validator.valid();
        // this.itemHelperOption.cdNmIsFocus = true;
        this.setInitialState();
    }

    // BarcodeHistorySearchItem 변경 Event
    @action.bound
    handleChange = (data) => {
        console.log(data);
        this[data.id] = data.value;
    }
    @action.bound
    getBuId = async () => {
        this.buId = CommonStore.buId;
        this.buIdCheck = true;
    }

    // BarcodeHistorySearchItem '위치' SelectBox 기본설정
    @action.bound
    getLocation = async () => {
        const searchData = {
            "buId": this.buId
            , "level": 1
        }
        const { data, status } = await LocationRepository.getLocation(searchData);
        if (status == 200) {
            this.lineGroup = data.data;
            this.locIds = 0;
        }
    }

    // BarcodeHistorySearchItem 생산일자,유통기한 Select 변경 시
    @action.bound
    dateSelectionChange = (item) => {
        if (item.cdNm === '생산일자') {
            this.startDate = moment().add(-14, 'days').format('YYYYMMDD');
            this.endDate = moment().format('YYYYMMDD');
        } else if (item.cdNm === '유통기한') {
            this.startDate = moment().add(-30, 'days').format('YYYYMMDD');
            this.endDate = moment().add(+30, 'days').format('YYYYMMDD');
        }
        // this.handleSearchClick();
    }

    // BarcodeHistorySearchItem 조회Btn 클릭 시
    @action.bound
    handleSearchClick = async () => {
        if (this._orderNo == null || isNaN(Number(this._orderNo))) {
            Alert.meg('지시번호를 확인 해주세요.')
            return;
        }
        this.barcodeHistoryGrpList = [];
        const searchData = {
            "buId": this.buId
            , "dateType": this.dateType
            , "startDate": this.startDate
            , "endDate": this.endDate
            , "prodId": this.prodId
            , "itemNmOrItemCd": this.itemNmOrItemCd
            , "locId": this.locIds
            , "_lotNo": this._lotNo
            , "_lotQuality": this._lotQuality
            , "ledgerHistory": this.ledgerHistory
            , "_orderNo": this._orderNo
            , "_barcode": this._barcode
            , "invenOpt": this.invenOpt
            , "uomType": this.uomType
        }
        const { data, status } = await BarcodeHistoryRepository.getOutput(searchData);
        if (status === 200) {
            this.barcodeHistoryList = data.data;
            this.barcodeGridApi.setRowData(this.barcodeHistoryList);
            this.getBarcodeGrpBtn();
        } else {
            alert.meg(data.msg);
        }
    }

    //지시번호 Enter 클릭Event
    @action.bound
    getBarcodeGrpBtn = async () => {
        if (this._orderNo == null || isNaN(Number(this._orderNo))) {
            Alert.meg('지시번호를 확인 해주세요.')
            return;
        }
        this.barcodeHistoryGrpList = [];

        //SP 1 적용 쿼리 PARAMETER
        // const submitdata = {
        //     "orderNo": Number(this._orderNo)
        //     ,"uomType": this.uomType
        // }

        //SP 2 적용 쿼리 PARAMETER
        const submitdata = {
            "buId": this.buId
            , "dateType": this.dateType
            , "startDate": this.startDate
            , "endDate": this.endDate
            , "itemNmOrItemCd": this.itemNmOrItemCd
            , "locId": this.locIds
            , "lotNo": this._lotNo
            , "lotQuality": this._lotQuality
            , "orderNo": Number(this._orderNo)
            , "barcode": this._barcode
            , "invenOpt": this.invenOpt
            , "uomType": this.uomType
        }
        //SP 1 적용 쿼리 FUNC
        //const { data, status } = await BarcodeHistoryRepository.getBarcodeGrp(submitdata);

        //SP 2 적용 쿼리 FUNC
        const { data, status } = await BarcodeHistoryRepository.getBarcodeGrpMod(submitdata);

        if (status === 200) {
            this.barcodeHistoryGrpList = data.data;
            this.barcodeGridGrpApi.setRowData(this.barcodeHistoryGrpList);
        } else {
            Alert.meg(data.msg);
        }
    }

    //바코드 내역 집계 조회용
    @action.bound
    getBarcodeGrp = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.barcodeGridApi);
        if (this.prevOrderNo != selectedRow.orderNo) {
            //SP 1 적용 쿼리 PARAMETER
            // const submitdata = {
            //     "orderNo": selectedRow.orderNo
            //     , "uomType": this.uomType
            // }

            //SP 2 적용 쿼리 PARAMETER
            const submitdata = {
                "buId": this.buId
                , "dateType": this.dateType
                , "startDate": this.startDate
                , "endDate": this.endDate
                , "itemNmOrItemCd": this.itemNmOrItemCd
                , "locId": this.locIds
                , "lotNo": this._lotNo
                , "lotQuality": this._lotQuality
                , "orderNo": selectedRow.orderNo
                , "barcode": this._barcode
                , "invenOpt": this.invenOpt
                , "uomType": this.uomType
            }

            //SP 1 적용 쿼리 FUNC
            //const { data, status } = await BarcodeHistoryRepository.getBarcodeGrp(submitdata);

            //SP 2 적용 쿼리 FUNC
            const { data, status } = await BarcodeHistoryRepository.getBarcodeGrpMod(submitdata);
            if (status === 200) {
                this.barcodeHistoryGrpList = data.data;
                this.barcodeGridGrpApi.setRowData(this.barcodeHistoryGrpList);
            } else {
                Alert.meg(data.msg);
            }
        }
        this.prevOrderNo = selectedRow.orderNo;
    }
    ////////////////////////////////////////////////////////////////////////////////////////

    // 수불내역 Button 클릭시 Modal 조회
    @action.bound
    openOrderModal = () => {
        const selectedRow = GridUtil.getSelectedRowData(this.barcodeGridApi);
        if (!selectedRow || selectedRow.length === 0) {
            Alert.meg("항목을 선택해주세요.");
            return;
        }
        this.lotId = selectedRow.lotId
        this.barcode = selectedRow.barcode
        //오더타입 빈 값일경우
        if (selectedRow.orderTypeNm === null) {
            this.orderType = ''
        } else {
            this.orderType = selectedRow.orderTypeNm
        }
        this.orderNo = selectedRow.orderNo
        this.itemCd = selectedRow.itemCd
        this.itemNm = selectedRow.itemNm
        this.inventoryId = selectedRow.inventoryId
        this.packSeq = selectedRow.packSeq
        this.barcode = selectedRow.barcode
        this.lotNo = selectedRow.lotNo
        this.invenQty = selectedRow.invenQty
        this.prodUom = selectedRow.prodUom

        this.orderModalIsOpen = true;
        this.orderModelClick();
    }

    // OrderModalSearchItem(수불내역) 조회Btn 클릭시
    @action.bound
    orderModelClick = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.barcodeGridApi);
        const searchData = {
            "lotId": selectedRow.lotId
            , "barcode": selectedRow.barcode
            , "startDate_l": this.startDate_l
            , "endDate_l": this.endDate_l
            , "uomType": this.uomType
        }
        const { data, status } = await BarcodeHistoryRepository.getOrderLedger(searchData);
        if (status === 200) {
            this.orderLedgerDataList = data.data;
            this.orderLedgerGridApi.setRowData(this.orderLedgerDataList);
        } else {
            alert.meg('error', data);
        }
    }

    // BarcodeHistoryModal(수불내역) 수불적용일자 변경Btn 클릭시
    @action.bound
    ledgerApModModal = () => {
        const selectedRow = GridUtil.getSelectedRowData(this.orderLedgerGridApi);

        if (!selectedRow || selectedRow.length === 0) {
            Alert.meg("항목을 선택해 주세요.");
            return;
        }
        this.ledgerId = selectedRow.ledgerId;
        this.lotId = selectedRow.lotId;
        this.barcode = selectedRow.barcode;
        this.tranjDt = selectedRow.tranjDt;
        this.ledgerApModModalIsOpen = true;
    }

    // BarcodeHistoryLedgerChgModal(수불적용일자 변경) 저장Btn 클릭시
    @action.bound
    ledgerChange = async () => {
        const submitData = {
            "ledgerId": this.ledgerId,
            "lotId": this.lotId,
            "barcode": this.barcode,
            "tranjDt": this.tranjDt,
            "tranjDtEdit": this.tranjDtEdit,
            "tranjDtReason": this.tranjDtReason,

            "startDate_l": this.startDate_l,
            "endDate_l": this.endDate_l,
            "uomType": this.uomType
        }
        if (this.tranjDt == this.tranjDtEdit.replace(/-/gi, '') || this.tranjDtReason == '') {
            Alert.meg("변경일자 및 사유를 확인해주세요.");
            return;
        } else {
            const value = await Alert.confirm("변경하시겠습니까?").then(result => { return result; });
            if (value) {
                const { data, status } = await BarcodeHistoryRepository.setLedgerChange(submitData);
                if (data.success == false || status != 200) {
                    Alert.meg(data.msg);
                } else {
                    this.orderLedgerDataList = data.data;
                    this.orderLedgerGridApi.setRowData(this.orderLedgerDataList);
                    this.closeledgerApModModal();
                }
            } else {
                return;
            }
        }
    }


    // BarcodeHistoryLedgerHisModal(수불적용일자 변경이력) 변경이력Btn 클릭시
    @action.bound
    ledgerApModHisModal = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.orderLedgerGridApi);
        if (!selectedRow || selectedRow.length === 0) {
            Alert.meg("항목을 선택해 주세요.");
            return;
        }

        this.ledgerApModHisModalIsOpen = true;
        const submitdata = {
            "ledgerId": selectedRow.ledgerId
        }

        const { data, status } = await BarcodeHistoryRepository.getLedgerApModHisModal(submitdata);
        if (status === 200) {
            this.barcodeHistoryLedgerHisList = data.data;
            this.ledgerHisGridApi.setRowData(this.barcodeHistoryLedgerHisList);
        } else {
            Alert.meg(data.msg);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////

    //위치변경 클릭시 Modal 활성
    @action.bound
    openLocModal = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.barcodeGridApi);

        if (!selectedRow || selectedRow.length == 0) {
            Alert.meg("위치변경 항목을 선택해 주세요.");
            return;
        }

        this.locId = selectedRow.locId;
        this.barcode = selectedRow.barcode;
        this.orderNo = selectedRow.orderNo;
        this.locChgModalIsOpen = true;
    }
    //위치변경 관련 Event
    @action.bound
    locationHelperResult = async (result) => {
        if (result != null) {
            const saveData = {
                "barcode": this.barcode
                , "locId": result.locId
                , "orderNo": this.orderNo
            }
            const { data, status } = await BarcodeHistoryRepository.locationMoveInsert(saveData);
            if (status === 200) {
                Alert.meg(data.msg);
                this.handleSearchClick();
            }
            this.locChgModalIsOpen = false;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////

    @action.bound
    openOrderDetailModal = () => {
        const selectedRow = GridUtil.getSelectedRowData(this.barcodeGridApi);
        if (!selectedRow || selectedRow.length == 0) {
            Alert.meg('조회할 항목을 선택해주세요.')
        }
        else {
            this.orderNo_h = Number(selectedRow.orderNo)
            this.orderHelperModalIsOpen = true;
        }
    }


    //오더정보 조회 Btn (첫 시작시 자동조회 ON)
    @action.bound
    orderDetailModelClick = async () => {
        const submitdata = {
            "orderNo": this.orderNo
        }

        const { data, status } = await BarcodeHistoryRepository.getOrderList(submitdata);
        if (status === 200) {
            this.orderDetailDataList = data.data;
            this.orderDetailGridApi.setRowData(this.orderDetailDataList);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////

    //위치변경 Modal 닫기
    @action.bound
    closeLocChgModal = () => {
        this.locChgModalIsOpen = false;
    }

    //수불내역 Modal 닫기
    @action.bound
    closeModal = () => {
        //this.clearVariable();
        this.orderModalIsOpen = false;
    }

    //수불적용일자 변경 Modal 닫기
    @action.bound
    closeledgerApModModal = () => {
        this.tranjDtEdit = moment().format('YYYYMMDD');
        this.tranjDtReason = '';
        this.ledgerApModModalIsOpen = false;
    }

    //수불적용일자 변경이력 Modal 닫기
    @action.bound
    closeledgerApModHisModal = () => {
        this.barcodeHistoryLedgerHisList = [];
        this.ledgerApModHisModalIsOpen = false;
    }

    //오더정보 Modal 닫기
    @action.bound
    closeOrderDetailModal = () => {
        this.orderDetailModalIsOpen = false;
    }
    ////////////////////////////////////////////////////////////////////////////////////////

    // BarcodeHistoryContent 그리드
    @action.bound
    setGridApi = async (gridApi) => {
        this.barcodeGridApi = gridApi;
        this.pinnedBottomRowData = [
            {
                orderNo: '합계',
                prodQty: 0,
                invenQty: 0,
                availableQty: 0,
                holdQty: 0,
                disuseQty: 0
            }
        ];
        const columnDefs = [
            {
                headerName: "", field: ""
                , width: 30
                , checkboxSelection: true
                , headerCheckboxSelection: true
                // , editable: function (params) { }
            },

            { headerName: "지시번호", field: "orderNo", width: 60, cellClass: 'cell_align_center' },
            { headerName: "순번", field: "packSeq", width: 40, cellClass: 'cell_align_center' },
            { headerName: "바코드", field: "barcode", width: 100, cellClass: 'cell_align_center' },
            { headerName: "제품코드", field: "itemCd", width: 70, cellClass: 'cell_align_center' },
            { headerName: "제품명", field: "itemNm", width: 120, cellClass: 'cell_align_left' },
            { headerName: "규격", field: "spec", width: 70, cellClass: 'cell_align_left' },
            { headerName: "LOT", field: "lotNo", width: 80, cellClass: 'cell_align_center' },
            { headerName: "생산일자", field: "prodDt", width: 80, cellClass: 'cell_align_center', valueFormatter: GridUtil.dateFormatter },
            { headerName: "유통기한", field: "expiryDt", width: 80, cellClass: 'cell_align_center', valueFormatter: GridUtil.dateFormatter },
            { headerName: "단위", field: "prodUom", width: 50, cellClass: 'cell_align_center' },
            {
                headerName: "생산수량", field: "prodQty", width: 70, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            {
                headerName: "재고수량", field: "invenQty", width: 70, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            {
                headerName: "가용수량", field: "availableQty", width: 70, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            {
                headerName: "홀드수량", field: "holdQty", width: 70, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            {
                headerName: "불용수량", field: "disuseQty", width: 70, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            {
                headerName: "품질상태", field: "lotQuality", width: 70, cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.getCodeNm(params, 'MFD003')
            },
            { headerName: "위치", field: "locNm", width: 100, cellClass: 'cell_align_center' },
            {
                headerName: "지시타입", field: "orderType", width: 80, cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.getCodeNm(params, 'MFD001')
            },
            { headerName: "생산라인", field: "lineNm", width: 80, cellClass: 'cell_align_center' },
            // { headerName: "Search", field: "searchText", width: 90, cellClass: 'cell_align_left' },
            // valueFormatter 참조용
            // , valueFormatter: (params) => GridUtil.numberFormatter(params, 6)},
        ];
        this.barcodeGridApi.setColumnDefs(columnDefs);
    }

    // BarcodeHistoryGrpContent (바코드 내역집계) 그리드
    @action.bound
    setGridGrpApi = async (gridApi) => {
        this.barcodeGridGrpApi = gridApi;
        this.pinnedBottomGrpRowData = [
            {
                itemCd: '합계',
                plQty: 0,
                prodQty: 0,
                invenQty: 0,
                availableQty: 0,
                holdQty: 0,
                disuseQty: 0
            }
        ];
        const columnDefs = [
            { headerName: "제품코드", field: "itemCd", width: 70, cellClass: 'cell_align_center' },
            { headerName: "제품명", field: "itemNm", width: 120 },
            { headerName: "규격", field: "spec", width: 70 },
            { headerName: "LOT", field: "lotNo", width: 80, cellClass: 'cell_align_center' },
            { headerName: "생산일자", field: "prodDt", width: 80, cellClass: 'cell_align_center', valueFormatter: GridUtil.dateFormatter },
            { headerName: "유통기한", field: "expiryDt", width: 80, cellClass: 'cell_align_center', valueFormatter: GridUtil.dateFormatter },
            { headerName: "단위", field: "prodUom", width: 50, cellClass: 'cell_align_center' },
            {
                headerName: "PL수량", field: "plQty", width: 80, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            {
                headerName: "생산수량", field: "prodQty", width: 80, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            {
                headerName: "재고수량", field: "invenQty", width: 80, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            {
                headerName: "가용수량", field: "availableQty", width: 80, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            {
                headerName: "홀드수량", field: "holdQty", width: 80, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            {
                headerName: "불용수량", field: "disuseQty", width: 80, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            }
            // { headerName: "Search", field: "searchText", width: 90 },
        ];
        this.barcodeGridGrpApi.setColumnDefs(columnDefs);
    }

    // OrderModalContents (바코드 수불내역) 그리드
    @action.bound
    setOrderLedgerGridApi = async (gridApi) => {
        this.orderLedgerGridApi = gridApi;
        const columnDefs = [
            {
                headerName: "수불적용일자", field: "tranjDt", width: 80, cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "수불일시", field: "tranjDtm", width: 120, cellClass: 'cell_align_center'
                , valueFormatter: (param) => GridUtil.dateFormatter(param, 'dtm')
            },
            { headerName: "수불타입", field: "ledgerNm", width: 80, cellClass: 'cell_align_center' },
            {
                headerName: "지시타입", field: "orderType", width: 80, cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.getCodeNm(params, 'MFD001')
            },
            { headerName: "지시번호", field: "orderNo", width: 60, cellClass: 'cell_align_center' },
            {
                headerName: "처리수량", field: "processQty", width: 70, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            {
                headerName: "재고수량", field: "invenQty", width: 70, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            {
                headerName: "가용수량", field: "availableQty", width: 70, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            {
                headerName: "홀드수량", field: "holdQty", width: 70, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            { headerName: "위치", field: "locNm", width: 80, cellClass: 'cell_align_center' },
            { headerName: "원본바코드", field: "-", width: 70, cellClass: 'cell_align_center' },
            { headerName: "FROM_TO", field: "fromTo", width: 70, cellClass: 'cell_align_center' }
            // , valueFormatter: (params) => GridUtil.numberFormatter(params, 6)},
        ];
        this.orderLedgerGridApi.setColumnDefs(columnDefs);
    }

    // BarcodeHistoryLedgerHisModel (수불적용일자 변경이력) 그리드
    @action.bound
    setLedgerHisGridApi = async (gridApi) => {
        this.ledgerHisGridApi = gridApi;
        const columnDefs = [
            { headerName: "순번", field: "ledgerEditId", width: 40, cellClass: 'cell_align_center' },
            {
                headerName: "변경일시", field: "tranjDtm", width: 130, cellClass: 'cell_align_center'
                , valueFormatter: (param) => GridUtil.dateFormatter(param, 'dtm')
            },
            {
                headerName: "기존일자", field: "tranjDt", width: 80, cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "변경일자", field: "tranjDtEdit", width: 80, cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            { headerName: "변경사유", field: "tranjDtReason", width: 150, cellClass: 'cell_align_left' }
        ];
        this.ledgerHisGridApi.setColumnDefs(columnDefs);
    }

    // OrderDetailModalContents (생산정보 조회) 그리드
    @action.bound
    setOrderDetailGridApi = async (gridApi) => {
        this.orderDetailGridApi = gridApi;
        this.pinnedBottomRowData = [
            {
                packSeq: '합계',
                prodQty: 0,
                invenQty: 0,
                availableQty: 0,
                holdQty: 0

            }
        ]
        const columnDefs = [
            {
                headerName: "", field: ""
                , width: 30
                , checkboxSelection: true
                , headerCheckboxSelection: true
                // , editable: function (params) { }
            },
            { headerName: "포장순번", field: "packSeq", width: 70, cellClass: 'cell_align_center' },
            { headerName: "바코드", field: "barcode", width: 100, cellClass: 'cell_align_center' },
            { headerName: "LOT", field: "lotNo", width: 80, cellClass: 'cell_align_center' },
            {
                headerName: "생산일자", field: "prodDt", width: 70, cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "유통기한", field: "expiryDt", width: 70, cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "생산수량", field: "prodQty", width: 70, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
                , pinnedRowCellRenderer: "customPinnedRowRenderer"
                , pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
            },
            {
                headerName: "재고수량", field: "invenQty", width: 70, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
                , pinnedRowCellRenderer: "customPinnedRowRenderer"
                , pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
            },
            {
                headerName: "가용수량", field: "availableQty", width: 70, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
                , pinnedRowCellRenderer: "customPinnedRowRenderer"
                , pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
            },
            {
                headerName: "홀드수량", field: "holdQty", width: 70, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
                , pinnedRowCellRenderer: "customPinnedRowRenderer"
                , pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
            },
            {
                headerName: "품질상태", field: "lotQuality", width: 70, cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.getCodeNm(params, 'MFD003')
            },
            { headerName: "위치", field: "locNm", width: 100, cellClass: 'cell_align_center' },
            {
                headerName: "처리일시", field: "inputDtm", width: 70, cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            { headerName: "처리자", field: "inputEmpNm", width: 70, cellClass: 'cell_align_center' }
        ];
        this.orderDetailGridApi.setColumnDefs(columnDefs);
    }








































































































































































































    /* 분할 관련 기능들입니다. */
    // 분할 Modal 변수
    @observable divideModalIsOpen = false;
    @observable divideBarcode = "";
    @observable divideLotNo = "";
    @observable divideItemCd = "";
    @observable divideItemNm = "";
    @observable divideCurrQty = 0;
    @observable divideChangeQty = 0;
    @observable divideUom = '';
    @observable divideRemark = "";
    divideGridApi = undefined;
    @observable divideModalList = [];
    @observable pinnedBottomRowDataAtDivideModalGrid = [];
    processQtyArr = "";
    // 분할 모달 열기 barcodeGridApi
    @action.bound
    divide = () => {
        const selectedRows = this.barcodeGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg("분할할 재고를 선택해 주세요");
            return;
        }
        if (!selectedRows || selectedRows.length > 1) {
            Alert.meg("분할할 재고를 1개만 선택해 주세요");
            return;
        }
        const selectedRow = selectedRows[0];
        this.divideBarcode = selectedRow.barcode;
        this.divideLotNo = selectedRow.lotNo;
        this.divideItemCd = selectedRow.itemCd;
        this.divideItemNm = selectedRow.itemNm;
        this.divideCurrQty = selectedRow.invenQty;
        this.divideChangeQty = selectedRow.invenQty;
        this.divideUom = selectedRow.prodUom;
        this.divideRemark = "";
        this.processQtyArr = "";
        this.divideModalIsOpen = true;
    }
    // 분할 모달 닫기
    @action.bound
    closeDivideModal = () => {
        this.divideModalIsOpen = false;
    }
    // 분할 모달 저장
    @action.bound
    divideModalSave = async () => {
        this.divideGridApi.forEachNode(rowNode => {
            if (rowNode.data.divideQty > 0) {
                this.processQtyArr += rowNode.data.divideQty + ",";
            }
        })
        const size = this.processQtyArr.length;
        this.processQtyArr = this.processQtyArr.substring(0, size - 1);
        console.log(this.processQtyArr);
        if (this.divideChangeQty <= 0) {
            Alert.meg("변경 수량이 0 이하로는 분할할 수 없습니다.");
            return;
        }
        const divideData = {
            "barcode": this.divideBarcode
            , "processQtyArr": this.processQtyArr
            , "uom": this.divideUom
        }
        const param = JSON.stringify({ param: divideData });
        const { data, status } = await BarcodeHistoryRepository.divide(param);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            this.divideModalIsOpen = false;
            this.handleSearchClick();
        }
    }
    // DivideModal 추가 클릭 시
    @action.bound
    divideModalAdd = async () => {
        let lastindex = this.divideGridApi.getLastDisplayedRow();
        const divide = {
            divideQty: 0
        };
        const result = this.divideGridApi.updateRowData({ add: [divide], addIndex: lastindex + 1 });
        GridUtil.setFocus(this.divideGridApi, result.add[0]);
    }
    // DivideModal 삭제 클릭 시
    @action.bound
    divideModalDelete = async () => {
        let selectedRows = this.divideGridApi.getSelectedRows();
        if (selectedRows.length == 0) {
            Alert.meg('삭제할 항목을 선택해 주세요.');
            return;
        }
        selectedRows.forEach(selectedRow => {
            this.divideGridApi.updateRowData({ remove: [selectedRow] });
        })
    }
    // DivideModal 그리드
    @action.bound
    setDivideModalGridApi = async (gridApi) => {
        this.divideGridApi = gridApi;
        // this.pinnedBottomRowDataAtDivideModalGrid = [
        //     {
        //         seq: '합계',
        //         processQty: 0
        //     }
        // ];
        const columnDefs = [
            {
                headerName: "수량"
                , field: "divideQty"
                , width: 100
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
            }
        ];
        this.divideGridApi.setColumnDefs(columnDefs);
    }
    // DivideModal 그리드 변경시
    @action.bound
    minusQty = async (event) => {
        let minusSum = 0;
        this.divideGridApi.forEachNode(rowNode => {
            minusSum += rowNode.data.divideQty;
            if (this.divideCurrQty - minusSum <= 0) {
                Alert.meg("변경수량이 0이하로는 분할할 수 없습니다.");
            }
        })
        this.divideChangeQty = this.divideCurrQty - minusSum;
    }

    /* 합짐 버튼 관련 기능들 입니다.*/
    // 합짐 Modal 변수
    @observable mergeModalIsOpen = false;
    beforeMergeGridApi = undefined;
    @observable beforeMergeList = [];
    @observable pinnedBottomRowDataAtBeforeMergeGrid = [];
    @observable mergeBarcode = "";
    @observable mergeLotNo = "";
    @observable mergeItemCd = "";
    @observable mergeItemNm = "";
    @observable mergeCurrQty = 0;
    @observable mergeChangeQty = 0;
    @observable mergeRemark = "";
    mergedGridApi = undefined;
    @observable mergedList = [];
    @observable pinnedBottomRowDataAtMergedGrid = [];
    @observable mergeAddModalIsOpen = false;
    mergedAddGridApi = undefined;
    @observable mergeAddModalList = [];

    // 합짐 모달 열기
    @action.bound
    merge = async () => {
        const selectedRows = this.barcodeGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length <= 1) {
            Alert.meg("합짐할 바코드를 2개이상 선택해 주세요");
            return;
        }
        let mergedRows = [];
        for (let i = 1; i < selectedRows.length; i++) {
            mergedRows.push(selectedRows[i]);
        }
        let availableQty = 0;
        for (let i = 0; i < selectedRows.length; i++) {
            availableQty += selectedRows[i].invenQty
        }
        this.mergeBarcode = selectedRows[0].barcode;
        this.mergeLotNo = selectedRows[0].lotNo;
        this.mergeItemCd = selectedRows[0].itemCd;
        this.mergeItemNm = selectedRows[0].itemNm;
        this.mergeCurrQty = availableQty;
        this.mergeChangeQty = availableQty;
        this.mergeRemark = "";

        this.beforeMergeList = selectedRows;

        this.mergedList = mergedRows;

        this.mergeModalIsOpen = true;
    }
    // 합짐 모달 닫기
    @action.bound
    closeMergeModal = () => {
        this.mergeModalIsOpen = false;
    }
    // 합짐 모달 합짐바코드 선택 그리드
    setBeforeMergeGridApi = (gridApi) => {
        this.beforeMergeGridApi = gridApi;
        this.pinnedBottomRowDataAtBeforeMergeGrid = [
            {
                barcode: '합계',
                availableQty: 0
            }
        ];
        const columnDefs = [
            {
                headerName: "바코드"
                , field: "barcode"
                , width: 100
                , cellClass: 'cell_align_center'
                , editable: false
            },
            {
                headerName: "LOT"
                , field: "lotNo"
                , width: 100
                , cellClass: 'cell_align_center'
                , editable: false
            },
            {
                headerName: "수량"
                , field: "availableQty"
                , width: 100
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
                , pinnedRowCellRenderer: "customPinnedRowRenderer"
                , pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
                , editable: false
            }
        ];
        this.beforeMergeGridApi.setColumnDefs(columnDefs);
        this.beforeMergeGridApi.setRowData(this.beforeMergeList);
    }
    // 합짐 모달 합짐 바코드 선택이 바뀔 시
    @action.bound
    beforeMergeGridSelectionChange = () => {
        const selectedRow = this.beforeMergeGridApi.getSelectedRows()[0];
        this.mergedList = [];
        this.beforeMergeGridApi.forEachNode(rowNode => {
            if (rowNode.selected == false) {
                this.mergedList.push(rowNode.data);
            }
        })
        this.mergeBarcode = selectedRow.barcode;
        this.mergeLotNo = selectedRow.lotNo;
        this.mergeItemCd = selectedRow.itemCd;
        this.mergeItemNm = selectedRow.itemNm;
        this.mergedGridApi.setRowData(this.mergedList);
    }
    // 합짐 모달 저장 클릭 시
    @action.bound
    mergeModalSave = async () => {
        let targetBarcodeArr = "";
        this.mergedGridApi.forEachNode(rowNode => {
            if (rowNode.data == undefined) {
                return;
            }
            targetBarcodeArr += rowNode.data.barcode + ","
        })
        const size = targetBarcodeArr.length;
        targetBarcodeArr = targetBarcodeArr.substring(0, size - 1);
        console.log(targetBarcodeArr);
        const mergeData = {
            "qtyUpBarcode": this.mergeBarcode
            , "targetBarcodeArr": targetBarcodeArr
            , "buId": CommonStore.buId
        }
        const param = JSON.stringify({ param: mergeData });
        const { data, status } = await BarcodeHistoryRepository.merge(param);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            this.mergeModalIsOpen = false;
            this.handleSearchClick();
        }
    }
    // 합짐 모달 추가 클릭 시
    @action.bound
    mergeModalAdd = () => {
        this.mergeAddModalIsOpen = true;
    }
    // 합짐 추가 모달 닫기
    @action.bound
    closeMergeAddModal = () => {
        this.mergeAddModalIsOpen = false;
    }
    // 합짐 모달 추가 그리드
    @action.bound
    setmergeAddModalGridApi = (gridApi) => {
        this.mergedAddGridApi = gridApi;
        const columnDefs = [
            {
                headerName: "", field: ""
                , width: 30
                , checkboxSelection: true
                , headerCheckboxSelection: true
                // , editable: function (params) { }
            },

            { headerName: "지시번호", field: "orderNo", width: 60, cellClass: 'cell_align_center' },
            { headerName: "순번", field: "packSeq", width: 40, cellClass: 'cell_align_center' },
            { headerName: "바코드", field: "barcode", width: 100, cellClass: 'cell_align_center' },
            { headerName: "제품코드", field: "itemCd", width: 70, cellClass: 'cell_align_center' },
            { headerName: "제품명", field: "itemNm", width: 120, cellClass: 'cell_align_left' },
            { headerName: "규격", field: "spec", width: 70, cellClass: 'cell_align_left' },
            { headerName: "LOT", field: "lotNo", width: 80, cellClass: 'cell_align_center' },
            { headerName: "생산일자", field: "prodDt", width: 70, cellClass: 'cell_align_center', valueFormatter: GridUtil.dateFormatter },
            { headerName: "유통기한", field: "expiryDt", width: 70, cellClass: 'cell_align_center', valueFormatter: GridUtil.dateFormatter },
            { headerName: "단위", field: "prodUom", width: 50, cellClass: 'cell_align_center' },
            {
                headerName: "생산수량", field: "prodQty", width: 70, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            {
                headerName: "재고수량", field: "invenQty", width: 70, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            {
                headerName: "가용수량", field: "availableQty", width: 70, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            {
                headerName: "홀드수량", field: "holdQty", width: 70, cellClass: 'cell_align_right'
                , valueFormatter: (params) => GridUtil.numberFormatter(params)
            },
            {
                headerName: "품질상태", field: "lotQuality", width: 70, cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.getCodeNm(params, 'MFD003')
            },
            { headerName: "위치", field: "locNm", width: 100, cellClass: 'cell_align_center' },
            {
                headerName: "지시타입", field: "orderType", width: 80, cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.getCodeNm(params, 'MFD001')
            },
            { headerName: "생산라인", field: "lineNm", width: 80, cellClass: 'cell_align_center' },
            // { headerName: "Search", field: "searchText", width: 90, cellClass: 'cell_align_left' },
            // valueFormatter 참조용
            // , valueFormatter: (params) => GridUtil.numberFormatter(params, 6)},
        ];
        this.mergedAddGridApi.setColumnDefs(columnDefs);
        const tempList = [];
        this.barcodeHistoryList.forEach(item => {
            if (!this.beforeMergeList.some(x => x.barcode == item.barcode)) {
                tempList.push(item);
            }            
        });
        const list = [];
        tempList.forEach(item => {
            if (!this.mergedList.some(x => x.barcode == item.barcode)) {
                list.push(item);
            }            
        });
        this.mergedAddGridApi.setRowData(list);
        // this.mergedAddGridApi.setRowData(this.barcodeHistoryList);
    }
    // 합짐 모달 추가 그리드 반환
    @action.bound
    mergeAddModalAdd = () => {
        const selectedRows = this.mergedAddGridApi.getSelectedRows();
        for(let i=0; i<selectedRows.length; i++) {
            this.mergedList.push(selectedRows[i]);
        }
        this.mergedGridApi.setRowData(this.mergedList);
        this.mergeAddModalIsOpen = false;
    }
    // 합짐 모달 삭제 클릭 시
    @action.bound
    mergeModalDelete = () => {
        let selectedRows = this.mergedGridApi.getSelectedRows();
        if (selectedRows.length == 0) {
            Alert.meg('삭제할 항목을 선택해 주세요.');
            return;
        }
        let mergeSum = 0;
        selectedRows.forEach(selectedRow => {
            mergeSum += selectedRow.availableQty;
            this.mergedGridApi.updateRowData({ remove: [selectedRow] });
        })
        this.mergeChangeQty = this.mergeCurrQty - mergeSum;
    }
    // 합짐 모달 합짐 LOT 그리드
    setMergedGridApi = (gridApi) => {
        this.mergedGridApi = gridApi;
        this.pinnedBottomRowDataAtMergedGrid = [
            {
                barcode: '합계',
                availableQty: 0
            }
        ];
        const columnDefs = [
            {
                headerName: "바코드"
                , field: "barcode"
                , width: 100
                , cellClass: 'cell_align_center'
                , editable: false
            },
            {
                headerName: "LOT"
                , field: "lotNo"
                , width: 100
                , cellClass: 'cell_align_center'
                , editable: false
            },
            {
                headerName: "수량"
                , field: "availableQty"
                , width: 100
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
                , pinnedRowCellRenderer: "customPinnedRowRenderer"
                , pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
                , editable: false
            }
        ];
        this.mergedGridApi.setColumnDefs(columnDefs);
        this.mergedGridApi.setRowData(this.mergedList);
    }

    /* RACK TYPE 설정 버튼 관련 변수 및 기능 */
    // RackType Modal 변수
    @observable rackTypeModalIsOpen = false;
    rackTypeModalBarcode = "";
    @observable rackTypeModalRackType = "";

    // RackType 모달 열기
    @action.bound
    btnRackTypeSetting = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.barcodeGridApi);
        if (!selectedRow) {
            Alert.meg("바코드를 선택해 주세요");
            return;
        }
        this.rackTypeModalBarcode = selectedRow.barcode;
        if (selectedRow.rackType == undefined || selectedRow.rackType == null || selectedRow.rackType == "") {
            this.rackTypeModalRackType = "1";
        } else {
            this.rackTypeModalRackType = selectedRow.rackType;
        }
        this.rackTypeModalIsOpen = true;
    }
    // RackType 모달 닫기
    @action.bound
    closeRackTypeModal = () => {
        this.rackTypeModalIsOpen = false;
    }
    // RackType 저장
    @action.bound
    btnRackTypeSave = async () => {
        const rackTypeData = {
            barcode: this.rackTypeModalBarcode
            , rackType: this.rackTypeModalRackType
        }
        const { data, status } = await BarcodeHistoryRepository.rackTypeSave(rackTypeData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            Alert.meg("저장되었습니다.");
            this.rackTypeModalIsOpen = false;
            this.handleSearchClick();
        }
    }
}
export default new BarcodeHistoryStore();