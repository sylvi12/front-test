import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    btnRackTypeSetting: stores.barcodeHistoryStore.btnRackTypeSetting
    , openLocModal: stores.barcodeHistoryStore.openLocModal
    , openOrderModal: stores.barcodeHistoryStore.openOrderModal
    , openOrderDetailModal: stores.barcodeHistoryStore.openOrderDetailModal
    , divide: stores.barcodeHistoryStore.divide
    , merge: stores.barcodeHistoryStore.merge
}))

class BarcodeHistoryMiddleItem extends Component {

    render() {

        const {
            btnRackTypeSetting
            , openLocModal
            , openOrderModal
            , openOrderDetailModal
            , divide
            , merge
        } = this.props;
        return (
            <React.Fragment>
                <SButton
                    className="btn_red"
                    buttonName={'RACK TYPE 설정'}
                    type={'btnRackTypeSetting'}
                    onClick={btnRackTypeSetting}
                />
                <SButton
                    className="btn_red"
                    buttonName={'분할'}
                    type={'btnBarcodeDivide'}
                    onClick={divide}
                />
                <SButton
                    className="btn_red"
                    buttonName={'합짐'}
                    type={'btnBarcodeMerge'}
                    onClick={merge}
                />
                <SButton
                    className="btn_red"
                    buttonName={'위치변경'}
                    type={'btnLocationUpdateForm'}
                    onClick={openLocModal}
                />
                <SButton
                    className="btn_red"
                    buttonName={'수불내역'}
                    type={'btnLedgerForm'}
                    onClick={openOrderModal}
                />
                <SButton
                    className="btn_red"
                    buttonName={'생산정보'}
                    type={'btnProdInfo'}
                    onClick={openOrderDetailModal}
                />
            </React.Fragment>
        )
    }
}

export default BarcodeHistoryMiddleItem;