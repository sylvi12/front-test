import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SInput from 'components/atoms/input/SInput';
import STextArea from 'components/atoms/input/STextArea';

@inject(stores => ({
    tranjDt: stores.barcodeHistoryStore.tranjDt,
    tranjDtEdit: stores.barcodeHistoryStore.tranjDtEdit,
    tranjDtReason: stores.barcodeHistoryStore.tranjDtReason,
    handleChange: stores.barcodeHistoryStore.handleChange,
    ledgerChange: stores.barcodeHistoryStore.ledgerChange
}))

// 수불적용일자 변경 Popup
class BarcodeHistoryLedgerChgModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { tranjDt, tranjDtEdit, tranjDtReason, ledgerChange, handleChange } = this.props;
        return (
            <React.Fragment>
                <div className="search_item_btn">
                    <SButton
                        buttonName={'변경'}
                        type={'btnLedgerUpdate'}
                        onClick={ledgerChange}
                    />
                </div>
                <div className="nonStyle">
                <table style={{ width: "100%" }}>
                    <tbody>
                        <tr>
                            <th>기존일자</th>
                            <td>
                                <SDatePicker
                                    id={"tranjDt"}
                                    value={tranjDt}
                                    onChange={handleChange}
                                    readOnly={true}
                                />
                            </td>
                        </tr>
                        <tr>
                            <th>변경일자</th>
                            <td>
                                <SDatePicker
                                    id={"tranjDtEdit"}
                                    value={tranjDtEdit}
                                    onChange={handleChange}
                                />
                            </td>
                        </tr>
                        <tr>
                            <th>변경사유</th>
                            <td>
                                {/* <SInput
                                    id={"tranjDtReason"}
                                    value={tranjDtReason}
                                    onChange={handleChange}
                                /> */}
                                <STextArea
                                    id="tranjDtReason"
                                    value={tranjDtReason}
                                    onChange={handleChange}
                                    contentWidth={250}
                                    contentHeight={100}
                                />
                            </td>
                        </tr>
                    </tbody>
                </table>
                </div>
            </React.Fragment >
        );
    }
}

export default BarcodeHistoryLedgerChgModal;