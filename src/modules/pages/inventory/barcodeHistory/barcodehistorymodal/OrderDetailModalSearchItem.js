import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SNumericInput from 'components/override/numericinput/SNumericInput';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SButton from 'components/atoms/button/SButton';

import BarcodeHistoryStore from 'modules/pages/inventory/barcodeHistory/service/BarcodeHistoryStore';

@inject(stores => ({
	orderType: stores.barcodeHistoryStore.orderType,
	orderNo: stores.barcodeHistoryStore.orderNo,
	itemCd: stores.barcodeHistoryStore.itemCd,
	itemNm: stores.barcodeHistoryStore.itemNm,
	orderQty: stores.barcodeHistoryStore.orderQty,
	prodQty: stores.barcodeHistoryStore.prodQty,
	planStartDtm: stores.barcodeHistoryStore.planStartDtm,
	planEndDtm: stores.barcodeHistoryStore.planEndDtm,
	progress: stores.barcodeHistoryStore.progress,
	handleChange: stores.barcodeHistoryStore.handleChange,
	orderDetailModelClick: stores.barcodeHistoryStore.orderDetailModelClick
}))

class OrderDetailModalSearchItem extends Component {

	constructor(props) {
		super(props);
	}

	render() {
		const { orderType, orderNo, itemCd, itemNm, orderQty, prodQty, planStartDtm, planEndDtm, progress,
			handleChange, orderDetailModelClick } = this.props;
		return (
			<React.Fragment>
				<div className='search_item_btn'>
					<SButton
						buttonName={'조회'}
						type={'btnSearch'}
						onClick={orderDetailModelClick}
					/>

				</div>
				<table style={{ width: "100%", padding: "0px" }}>
					<tbody>
						<tr>
							<td className="th">지시타입</td>
							<td>
								<SInput
									id={"orderType"}
									value={orderType}
									readOnly={true}
								/>
							</td>
							<td className="th">지시번호</td>
							<td>
								<SInput
									id={"orderNo"}
									value={orderNo}
									readOnly={true}
								/>
							</td>
							<td className="th">제품코드</td>
							<td>
								<SInput
									id={"itemCd"}
									value={itemCd}
									readOnly={true}
								/>
							</td>
							<td className="th">지시수량</td>
							<td>
								<SNumericInput
									id={"orderQty"}
									value={orderQty}
									readOnly={true}
								/>
							</td>
							<td className="th">진척율</td>
							<td>
								<SInput
									id={"progress"}
									value={progress}
									readOnly={true}
								/>
							</td>
						</tr>
						<tr>
							<td className="th">지시기간</td>
							<td colSpan="3">
								<SDatePicker
									id={"planStartDtm"}
									value={planStartDtm}
									onChange={handleChange}
									readOnly={true}
								/>
								<SDatePicker
									title={"~"}
									id={"planEndDtm"}
									value={planEndDtm}
									onChange={handleChange}
									readOnly={true}
								/>
							</td>
							<td className="th">제품명</td>
							<td>
								<SInput
									id={"itemNm"}
									value={itemNm}
									readOnly={true}
								/>
							</td>
							<td className="th">진척수량</td>
							<td>
								<SNumericInput
									id={"prodQty"}
									value={prodQty}
									readOnly={true}
								/>
							</td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</React.Fragment>
		)
	}
}

export default OrderDetailModalSearchItem;

