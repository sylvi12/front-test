import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
	handleChange: stores.barcodeHistoryStore.handleChange
}))

class OrderDetailModelMiddleItem extends Component {

	render() {

		const { handleChange } = this.props;
		return (
			<React.Fragment>
				<SButton
					className="btn_red"
					buttonName={'바코드사용'}
					type={'btnBarcodeUse'}
					onClick={handleChange}
				/>
				<SButton
					className="btn_red"
					buttonName={'바코드중지'}
					type={'btnBarcodeCancel'}
					onClick={handleChange}
				/>
				<SButton
					className="btn_red"
					buttonName={'생산등록'}
					type={'btnProdInsert'}
					onClick={handleChange}
				/>
				<SButton
					className="btn_red"
					buttonName={'생산취소'}
					type={'btnProdCancel'}
					onClick={handleChange}
				/>
				<SButton
					className="btn_red"
					buttonName={'생산량조정'}
					type={'btnProdChange'}
					onClick={handleChange}
				/>
			</React.Fragment>
		)
	}
}

export default OrderDetailModelMiddleItem;