import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
	setOrderDetailGridApi: stores.barcodeHistoryStore.setOrderDetailGridApi,
	pinnedBottomRowData: stores.barcodeHistoryStore.pinnedBottomRowData,
	barcodeHistoryOrderDetailList: stores.barcodeHistoryStore.barcodeHistoryOrderDetailList
}))

class OrderDetailModalContents extends Component {

	constructor(props) {
		super(props);
	}

	render() {

		const { setOrderDetailGridApi, barcodeHistoryOrderDetailList } = this.props;

		return (
			<SGrid
				grid={'orderDetailGrid'}
				gridApiCallBack={setOrderDetailGridApi}
				rowData={barcodeHistoryOrderDetailList}
				pinnedBottomRowData={this.props.pinnedBottomRowData}
				suppressRowClickSelection={true}
				cellReadOnlyColor={true}
				editable={false}
			/>
		);
	}
}
export default OrderDetailModalContents;