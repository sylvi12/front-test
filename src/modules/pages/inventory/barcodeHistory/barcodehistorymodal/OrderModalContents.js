import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setOrderLedgerGridApi: stores.barcodeHistoryStore.setOrderLedgerGridApi,
    orderLedgerDataList: stores.barcodeHistoryStore.orderLedgerDataList
}))

class OrderModalContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setOrderLedgerGridApi, orderLedgerDataList } = this.props;

        return (
            <SGrid
                grid={'orderLedgerGrid'}
                gridApiCallBack={setOrderLedgerGridApi}
                rowData={orderLedgerDataList}
                suppressRowClickSelection={true}
                cellReadOnlyColor={true}
                editable={false}
            />
        );
    }
}
export default OrderModalContents;