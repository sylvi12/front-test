import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    ledgerApModModal:stores.barcodeHistoryStore.ledgerApModModal,
    ledgerApModHisModal:stores.barcodeHistoryStore.ledgerApModHisModal
}))

class OrderModalMiddlesItem extends Component {

    render() {

        const { ledgerApModModal, ledgerApModHisModal } = this.props;
        return (
            <React.Fragment>
				<SButton
                    className="btn_red"
                    buttonName={'수불적용일자 변경'}
                    type={'btnLedgerUpdateForm'}
                    onClick={ledgerApModModal}
                />
                <SButton
                    className="btn_red"
                    buttonName={'수불적용일자 변경이력'}
                    type={'btnLedgerUpdateHisForm'}
                    onClick={ledgerApModHisModal}
                />
            </React.Fragment>
        )
    }
}

export default OrderModalMiddlesItem;