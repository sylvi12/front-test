import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";
import SModal from "components/override/modal/SModal";

import OrderModalSearchItem from "modules/pages/inventory/barcodeHistory/barcodehistorymodal/OrderModalSearchItem";
import OrderModalMiddlesItem from "modules/pages/inventory/barcodeHistory/barcodehistorymodal/OrderModalMiddlesItem";
import OrderModalContents from "modules/pages/inventory/barcodeHistory/barcodehistorymodal/OrderModalContents";

import BarcodeHistoryLedgerChgModal from "modules/pages/inventory/barcodeHistory/barcodehistorymodal/BarcodeHistoryLedgerChgModal";
import BarcodeHistoryLedgerHisModal from "modules/pages/inventory/barcodeHistory/barcodehistorymodal/BarcodeHistoryLedgerHisModal";

@inject(stores => ({
    ledgerApModModalIsOpen: stores.barcodeHistoryStore.ledgerApModModalIsOpen,
    ledgerApModHisModalIsOpen: stores.barcodeHistoryStore.ledgerApModHisModalIsOpen,

    closeledgerApModModal: stores.barcodeHistoryStore.closeledgerApModModal,
    closeledgerApModHisModal: stores.barcodeHistoryStore.closeledgerApModHisModal
}))

class BarcodeHistoryModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {
            ledgerApModModalIsOpen,
            ledgerApModHisModalIsOpen,
            closeledgerApModModal,
            closeledgerApModHisModal
        } = this.props;

        return (
            <React.Fragment>
                <div className = "nonStyle">
				<SearchTemplate searchItem={<OrderModalSearchItem />} />
                </div>
				<ContentsMiddleTemplate contentSubTitle={'바코드 수불내역'} middleItem={<OrderModalMiddlesItem />} />
				<ContentsTemplate id='order' contentItem={<OrderModalContents />} />

                <SModal
                    formId={"inventory_ledger_update_p"}
                    isOpen={ledgerApModModalIsOpen}
                    onRequestClose={closeledgerApModModal}
                    contentLabel="수불적용일자 변경"
                    width={400}
                    height={280}
                    contents={<BarcodeHistoryLedgerChgModal />}
                />

                 <SModal
                    formId={"inventory_ledger_update_p"}
                    isOpen={ledgerApModHisModalIsOpen}
                    onRequestClose={closeledgerApModHisModal}
                    contentLabel="수불적용일자 변경이력"
                    width={550}
                    height={350}
                    contents={<BarcodeHistoryLedgerHisModal />}
                />
            </React.Fragment>
        );
    }
}

export default BarcodeHistoryModal;