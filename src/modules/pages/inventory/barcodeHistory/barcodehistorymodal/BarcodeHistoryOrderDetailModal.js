import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import OrderDetailModalSearchItem from "modules/pages/inventory/barcodeHistory/barcodehistorymodal/OrderDetailModalSearchItem";
import OrderDetailModalMiddleItem from "modules/pages/inventory/barcodeHistory/barcodehistorymodal/OrderDetailModalMiddleItem";
import OrderDetailModalContents from "modules/pages/inventory/barcodeHistory/barcodehistorymodal/OrderDetailModalContents";

@inject(stores => ({
}))

// 생산정보 Modal
class BarcodeHistoryOrderDetailModal extends Component {

	constructor(props) {
		super(props);
	}

	render() {

		const {  } = this.props;
		return (
			<React.Fragment>
				<SearchTemplate searchItem={<OrderDetailModalSearchItem />} />
				<ContentsMiddleTemplate contentSubTitle={'지시 바코드 내역'} middleItem={<OrderDetailModalMiddleItem />} />
				<ContentsTemplate id='order' contentItem={<OrderDetailModalContents />} />

				{/* TODO 바코드 사용,바코드 중지, 생산등록, 생산 취소, 생산량 조정 Event 생성필요 */}
			</React.Fragment>
		);
	}
}

export default BarcodeHistoryOrderDetailModal;