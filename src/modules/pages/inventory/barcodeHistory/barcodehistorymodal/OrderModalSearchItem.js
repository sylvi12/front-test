import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SNumericInput from 'components/override/numericinput/SNumericInput';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SButton from 'components/atoms/button/SButton';

import BarcodeHistoryStore from 'modules/pages/inventory/barcodeHistory/service/BarcodeHistoryStore';

@inject(stores => ({
	orderType: stores.barcodeHistoryStore.orderType,
	orderNo: stores.barcodeHistoryStore.orderNo,
	itemCd: stores.barcodeHistoryStore.itemCd,
	itemNm: stores.barcodeHistoryStore.itemNm,
	barcode: stores.barcodeHistoryStore.barcode,
	startDate_l: stores.barcodeHistoryStore.startDate_l,
	endDate_l: stores.barcodeHistoryStore.endDate_l,
	lotNo: stores.barcodeHistoryStore.lotNo,
	invenQty: stores.barcodeHistoryStore.invenQty,
	prodUom: stores.barcodeHistoryStore.prodUom,
	packSeq: stores.barcodeHistoryStore.packSeq,

	handleChange: stores.barcodeHistoryStore.handleChange,
	orderModelClick: stores.barcodeHistoryStore.orderModelClick
}))

class OrderModalSearchItem extends Component {

	constructor(props) {
		super(props);
	}

	render() {
		const { orderType, orderNo, itemCd, itemNm, barcode, startDate_l, endDate_l, lotNo, invenQty, prodUom, packSeq,
			handleChange, orderModelClick } = this.props;
		return (
			<React.Fragment>
				<div className='search_item_btn'>
					<SButton
						buttonName={'조회'}
						type={'btnSearch'}
						onClick={orderModelClick} />
				</div>
				<table style={{ width: "100%", padding: "0px" }}>
					<tbody>
						<tr>
							<td className="th">지시타입</td>
							<td>
								<SInput
									id={"orderType"}
									value={orderType}
									onChange={handleChange}
									readOnly={true}
								/>
							</td>
							<td className="th">지시번호</td>
							<td>
								<SInput
									id={"orderNo"}
									value={orderNo}
									onChange={handleChange}
									readOnly={true}
								/>
							</td>
							<td className="th">포장순번</td>
							<td>
								<SInput
									id={"pack_seq"}
									value={packSeq}
									onChange={handleChange}
									readOnly={true}
								/>
							</td>
							<td className="th">바코드</td>
							<td>
								<SInput
									id={"barcode"}
									value={barcode}
									onChange={handleChange}
									readOnly={true}
								/>
							</td>
						</tr>
						<tr>
							<td className="th">제품코드</td>
							<td>
								<SInput
									id={"itemCd"}
									value={itemCd}
									onChange={handleChange}
									readOnly={true}
								/>
							</td>
							<td className="th">제품명</td>
							<td>
								<SInput
									id={"itemNm"}
									value={itemNm}
									onChange={handleChange}
									readOnly={true}
								/>
							</td>
							<td className="th">재고수량</td>
							<td>
								<SNumericInput
									id={"invenQty"}
									value={invenQty}
									onChange={handleChange}
									readOnly={true}
								/>
							</td>

							<td className="th">단위</td>
							<td>
								<SInput
									id={"prodUom"}
									value={prodUom}
									onChange={handleChange}
									readOnly={true}
								/>
							</td>
						</tr>
						<tr>
							<td className="th">LOT</td>
							<td>
								<SInput
									id={"lotNo"}
									value={lotNo}
									onChange={handleChange}
									readOnly={true}
								/>
							</td>
							<td className="th">수불적용일자</td>
							<td colSpan="5">
								<SDatePicker
									id={"startDate_l"}
									value={startDate_l}
									onChange={handleChange}
								/>
								<SDatePicker
									title={"~"}
									id={"endDate_l"}
									value={endDate_l}
									onChange={handleChange}
								/>
							</td>
						</tr>
					</tbody>
				</table>
			</React.Fragment>
		)
	}
}

export default OrderModalSearchItem;

