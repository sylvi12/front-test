import React, { Component } from 'react';
import { inject } from 'mobx-react';

import LocationHelper from "components/codehelper/LocationHelper";
import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";


@inject(stores => ({
    locChgModalIsOpen: stores.barcodeHistoryStore.locChgModalIsOpen,
    locationBuId: stores.barcodeHistoryStore.locationBuId,
    handleChange: stores.barcodeHistoryStore.handleChange,
    locationHelperResult: stores.barcodeHistoryStore.locationHelperResult
}))

// 위치변경 Popup
class BarcodeHistoryLocChgModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { locChgModalIsOpen, locationBuId, handleChange, locationHelperResult } = this.props;
        return (
            <React.Fragment>
                <LocationHelper
                    id={'locChgModalIsOpen'}
                    isLocationOpen={locChgModalIsOpen}
                    buId={locationBuId}
                    buReadOnly={true}
                    onChange={handleChange}
                    onHelperResult={locationHelperResult}
                />
            </React.Fragment>
        )
    }
}

export default BarcodeHistoryLocChgModal;