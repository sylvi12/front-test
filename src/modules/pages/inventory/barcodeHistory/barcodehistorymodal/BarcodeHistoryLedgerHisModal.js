import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid';


@inject(stores => ({
    setLedgerHisGridApi: stores.barcodeHistoryStore.setLedgerHisGridApi, 
    barcodeHistoryLedgerHisList: stores.barcodeHistoryStore.barcodeHistoryLedgerHisList
}))

// 수불적용일자 변경이력 Popup
class BarcodeHistoryLedgerHisModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setLedgerHisGridApi, barcodeHistoryLedgerHisList } = this.props;
        return (
            <React.Fragment>
                <SGrid
                grid={'ledgerHisGrid'}
                gridApiCallBack={setLedgerHisGridApi}
                rowData={barcodeHistoryLedgerHisList}
                cellReadOnlyColor={true}
                editable={false}
                />
            </React.Fragment>
        );
    }
}

export default BarcodeHistoryLedgerHisModal;