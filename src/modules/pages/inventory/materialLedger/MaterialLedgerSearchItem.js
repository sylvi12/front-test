import React, { Component } from 'react';
import { inject } from 'mobx-react';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SButton from 'components/atoms/button/SButton';
import SInput from 'components/atoms/input/SInput';
import SCheckBox from 'components/atoms/checkbox/SCheckBox';

import MaterialLedgerStore from 'modules/pages/inventory/materialLedger/service/MaterialLedgerStore';
import SBuSelectBox from 'components/override/business/SBuSelectBox';

@inject(stores => ({
    handleChange: stores.materialLedgerStore.handleChange
  , handleSearchClick: stores.materialLedgerStore.handleSearchClick
  , buId: stores.materialLedgerStore.buId
  , buList: stores.materialLedgerStore.buList
  , invenFromDt: stores.materialLedgerStore.invenFromDt
  , invenToDt: stores.materialLedgerStore.invenToDt
  , buSelectionChanged: stores.materialLedgerStore.buSelectionChanged
  , itemCdNm: stores.materialLedgerStore.itemCdNm
  , invenCheck: stores.materialLedgerStore.invenCheck
  , detailCheck: stores.materialLedgerStore.detailCheck
  , uomGubn: stores.materialLedgerStore.uomGubn
  
}))


class MaterialLedgerSearchItem extends Component {
    
    constructor(props) {
        super(props);
        
    }

    async componentWillMount() {
        if (MaterialLedgerStore.alphaCheck == false) {
            await MaterialLedgerStore.getBu();
        }
    }

    render() {

        const {   buId
                , buList
                , invenFromDt
                , invenToDt
                , handleChange
                , handleSearchClick
                , buSelectionChanged 
                , itemCdNm 
                , uomGubn
                , invenCheck
                , detailCheck } = this.props;

        return(
            <React.Fragment>
                <div className='search_item_btn'>
                    <SButton
                        buttonName={'조회'}
                        type={'btnSearch'}
                        onClick={handleSearchClick}
                    />
                </div>
                <div style={{ display: "inline_block" }}>
                    <div className='search_line'>

                        <SBuSelectBox
                            title={"공장"}
                            id={"buId"}
                            value={buId}
                            optionGroup={buList}
                            onChange={handleChange}
                            //onSelectionChange={buSelectionChanged}
                        />
                        

                        <SDatePicker
                            title={"기간"}
                            id={"invenFromDt"}
                            value={invenFromDt}
                            onChange={handleChange}

                        />
                        <SDatePicker
                            title={"~"}
                            id={"invenToDt"}
                            value={invenToDt}
                            onChange={handleChange}
                        />

                        <SCheckBox
                            title={"실재고여부"}
                            id={"invenCheck"}
                            value={invenCheck}
                            //checked={}
                            onChange={handleChange}
                        />

                        <SInput
                            title={"제품"}
                            id={"itemCdNm"}
                            value={itemCdNm}
                            onChange={handleChange}
                            onEnterKeyDown={handleSearchClick}
                        />

                        <SSelectBox
                            title={"기준단위"}
                            id={"uomGubn"}
                            value={uomGubn}
                            codeGroup={"MFD015"}
                            onChange={handleChange}
                        />

                        <SCheckBox
                            title={"상세보기"}
                            id={"detailCheck"}
                            value={detailCheck}
                            //checked={}
                            onChange={handleChange}
                        />
                        
                    </div>
                </div>



            </React.Fragment>
        );
    }
}

export default MaterialLedgerSearchItem;