import API from "components/override/api/API";

class MaterialLedgerRepository {

    URL = "/api/inventory/materialLedger";
    
    //조회
    getMaterialLedger(param) {
        return API.request.get(encodeURI(`${this.URL}/getmaterialledger?buId=${param.buId}&invenFromDt=${param.invenFromDt}&invenToDt=${param.invenToDt}&invenCheck=${param.invenCheck}&itemCdNm=${param.itemCdNm}&uomGubn=${param.uomGubn}`))
    }
}

export default new MaterialLedgerRepository();