import { observable, action } from 'mobx';
import moment from 'moment';
import 'moment/locale/ko';

import BaseStore from 'utils/base/BaseStore';
import CommonStore from 'modules/pages/common/service/CommonStore';
import MaterialLedgerRepository from 'modules/pages/inventory/materialLedger/service/MaterialLedgerRepository';
import GridUtil from 'components/override/grid/utils/GridUtil';
import Alert from 'components/override/alert/Alert';

class MaterialLedgerStore extends BaseStore {

    constructor() {
        super()
        this.setInitialState();
    }

    //검색 조건 변수
    @observable buId = CommonStore.buId;
    @observable buList = CommonStore.buList;
    @observable invenFromDt = moment().add(-7, 'days').format('YYYYMMDD');
    @observable invenToDt = moment().format('YYYYMMDD');
    @observable itemCdNm = '';
    @observable invenCheck = 'Y';
    @observable uomGubn = 10;
    @observable detailCheck = 'N'

    //그리드 변수
    @observable materialLedgerList = [];
    materialLedgerGridApi = undefined;
    @observable pinnedBottomRowData = [];

    @observable alphaCheck = false;

    //공장 불러오기
    @action.bound
    getBu = async () => {
        this.buId = CommonStore.buId;
        this.buList = CommonStore.buList;
        this.alphaCheck = true;
    }

    @action.bound
    handleChange = (data) => {
        this[data.id] = data.value;
    }

    //조회
    @action.bound
    handleSearchClick = async () => {

        const searchData = {
            "buId": this.buId
            , "invenFromDt": this.invenFromDt
            , "invenToDt": this.invenToDt
            , "invenCheck": this.invenCheck
            , "itemCdNm": this.itemCdNm
            , "uomGubn": this.uomGubn
        }

        const { data, status } = await MaterialLedgerRepository.getMaterialLedger(searchData);

        if (status == 200) {
            this.materialLedgerList = data.data;
            this.materialLedgerGridApi.setColumnDefs(this.getColumnDefs());
            this.materialLedgerGridApi.setRowData(this.materialLedgerList);
        } else {
            Alert.meg(data.msg);
        }
    }

    @action.bound
    setGridApi = async (gridApi) => {
        this.materialLedgerGridApi = gridApi;
        this.pinnedBottomRowData = [
            {
                buId: '합계',
                openInvenQty: 0,
                openAvailableQty: 0,
                openHoldQty: 0,
                openDisuseQty: 0,
                icQty: 0,
                inPQty: 0,
                otQty: 0,
                inTQty: 0,
                coQty: 0,
                ceQty: 0,
                clQty: 0,
                cpQty: 0,
                cfQty: 0,
                cjQty: 0,
                inCQty: 0,
                c11Qty: 0,
                inEtcQty: 0,
                inQty: 0,
                izQty: 0,
                outPQty: 0,
                stQty: 0,
                outTQty: 0,
                soQty: 0,
                seQty: 0,
                slQty: 0,
                spQty: 0,
                sfQty: 0,
                sjQty: 0,
                outSQty: 0,
                lzQty: 0,
                outEtcQty: 0,
                outQty: 0,
                hdQty: 0,
                haQty: 0,
                etcQty: 0,
                closeInvenQty: 0,
                closeAvailableQty: 0,
                closeHoldQty: 0,
                closeDisuseQty: 0
            }
        ];
        this.materialLedgerGridApi.setColumnDefs(this.getColumnDefs());
    }

    getColumnDefs = () => {

        const detailCheck = this.detailCheck == "Y" ? true : false;

        const columnDefs = [
            {
                headerName: "",
                children: [
                    {
                        headerName: "공장"
                        , field: "buId"
                        , width: 100
                        , cellClass: 'cell_align_center'
                        , pinned: "left"
                    },
                    {
                        headerName: "제품코드"
                        , field: "itemCd"
                        , width: 80
                        , cellClass: 'cell_align_center'
                        , pinned: "left"
                    },
                    {
                        headerName: "제품명"
                        , field: "itemNm"
                        , width: 130
                        , cellClass: 'cell_align_left'
                        , pinned: "left"
                    },
                    {
                        headerName: "단위"
                        , field: "uom"
                        , width: 50
                        , cellClass: 'cell_align_center'
                        , pinned: "left"
                    }
                ]
            },
            {
                headerName: "기초재고",
                children: [
                    {
                        headerName: "총재고"
                        , field: "openInvenQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "가용"
                        , field: "openAvailableQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "HOLD"
                        , field: "openHoldQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "불용"
                        , field: "openDisuseQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    }
                ]
            },
            {
                headerName: "입고",
                children: [
                    {
                        headerName: "생산"
                        , field: "icQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , type: "numericColumn"
                        , hide: !detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "생산"
                        , field: "inPQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , type: "numericColumn"
                        , hide: detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "이체"
                        , field: "otQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , type: "numericColumn"
                        , hide: !detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "이체"
                        , field: "inTQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , type: "numericColumn"
                        , hide: detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "반품"
                        , field: "coQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: !detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "직수출"
                        , field: "ceQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: !detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "로컬"
                        , field: "clQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: !detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "완제품"
                        , field: "cpQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: !detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "무상견본"
                        , field: "cfQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: !detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "광고선전"
                        , field: "cjQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: !detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "반입"
                        , field: "inCQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "상품"
                        , field: "c11Qty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: !detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "기타"
                        , field: "inEtcQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "합계"
                        , field: "inQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                ]
            },
            {
                headerName: "출고",
                children: [
                    {
                        headerName: "재포장"
                        , field: "izQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: !detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "재포장"
                        , field: "outPQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "이체"
                        , field: "stQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: !detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "이체"
                        , field: "outTQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "내수"
                        , field: "soQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: !detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "직수출"
                        , field: "seQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: !detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "로컬"
                        , field: "slQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: !detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "완제품"
                        , field: "spQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: !detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "무상견본"
                        , field: "sfQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: !detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "광고선전"
                        , field: "sjQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: !detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "출하"
                        , field: "outSQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "원당재투입"
                        , field: "lzQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: !detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "기타"
                        , field: "outEtcQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "합계"
                        , field: "outQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    }
                ]
            },
            {
                headerName: "HOLD",
                children: [
                    {
                        headerName: "HOLD"
                        , field: "hdQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: !detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "불용"
                        , field: "haQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , hide: !detailCheck
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "합계"
                        , field: "etcQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                ]
            },
            {
                headerName: "기말재고",
                children: [
                    {
                        headerName: "총재고"
                        , field: "closeInvenQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)

                    },
                    {
                        headerName: "가용"
                        , field: "closeAvailableQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)

                    },
                    {
                        headerName: "HOLD"
                        , field: "closeHoldQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    },
                    {
                        headerName: "불용"
                        , field: "closeDisuseQty"
                        , width: 80
                        , cellClass: 'cell_align_right'
                        , valueFormatter: (params) => GridUtil.numberFormatter(params)
                    }
                ]
            }
        ];

        GridUtil.setSelectCellByList(columnDefs[0].children[0], this.buList, "buId", "buNm");

        return columnDefs;
    }
}

export default new MaterialLedgerStore();