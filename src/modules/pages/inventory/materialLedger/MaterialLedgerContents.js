import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid';

@inject(stores => ({
    setGridApi: stores.materialLedgerStore.setGridApi
    , materialLedgerList: stores.materialLedgerStore.materialLedgerList
    , pinnedBottomRowData: stores.materialLedgerStore.pinnedBottomRowData
}))

class MaterialLedgerContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setGridApi, materialLedgerList, pinnedBottomRowData } = this.props;

        return (
            <SGrid
                grid={'materialLedgerGrid'}
                gridApiCallBack={setGridApi}
                rowData={materialLedgerList}
                cellReadOnlyColor={true}
                editable={false}
                pinnedBottomRowData={pinnedBottomRowData}
            />
        );
    }
}

export default MaterialLedgerContents;