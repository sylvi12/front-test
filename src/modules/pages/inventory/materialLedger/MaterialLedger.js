import React, { Component } from "react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import MaterialLedgerSearchItem from "modules/pages/inventory/materialLedger/MaterialLedgerSearchItem";
import MaterialLedgerContents from "modules/pages/inventory/materialLedger/MaterialLedgerContents";

class MaterialLedger extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <React.Fragment>
                <SearchTemplate searchItem={<MaterialLedgerSearchItem />} />
                <ContentsMiddleTemplate contentSubTitle={'수불부'}/>
                <ContentsTemplate id="current" contentItem={<MaterialLedgerContents />} /> 
            </React.Fragment>
        );
    }
}

export default MaterialLedger;