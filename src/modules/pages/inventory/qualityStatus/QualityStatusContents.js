import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid';

@inject(stores => ({
    setGridApi: stores.qualityStatusStore.setGridApi,
    holdList: stores.qualityStatusStore.holdList,
    getBarcode: stores.qualityStatusStore.getBarcode,
    openModalEdit: stores.qualityStatusStore.openModalEdit
}))

class QualityStatusContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setGridApi, holdList, getBarcode, openModalEdit } = this.props;
        
        return (
            <SGrid 
                grid={'holdGrid'}
                gridApiCallBack={setGridApi}
                rowData={holdList}
                onSelectionChanged={getBarcode}
                rowDoubleClick={openModalEdit}
                cellReadOnlyColor={true}
                editable={false}
            />
        );
    }
}

export default QualityStatusContents;