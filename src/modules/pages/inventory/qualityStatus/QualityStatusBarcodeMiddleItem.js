import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    addBarcode: stores.qualityStatusStore.addBarcode,
    delBarcode: stores.qualityStatusStore.delBarcode,
    genuine: stores.qualityStatusStore.genuine,
    disuseBarcode: stores.qualityStatusStore.disuseBarcode
}))

class QualityStatusBarcodeMiddleItem extends Component {

    render() {

        const { addBarcode, delBarcode, genuine, disuseBarcode } = this.props;
        return (
            <React.Fragment>
                <SButton
                    className="btn_red"
                    buttonName={'추가'}
                    type={'btnAddBarcode'}
                    onClick={addBarcode}
                />
                {/* <SButton
                    className="btn_red"
                    buttonName={'삭제'}
                    type={'btnDelBarcode'}
                    onClick={delBarcode}
                /> */}
                <SButton
                    className="btn_red"
                    buttonName={'정품'}
                    type={'btnBarcodeGenuine'}
                    onClick={genuine}
                />
                <SButton
                    className="btn_red"
                    buttonName={'불용'}
                    type={'btnBarcodeDisuse'}
                    onClick={disuseBarcode}
                />
            </React.Fragment>
        )
    }
}

export default QualityStatusBarcodeMiddleItem;