import { observable, action } from 'mobx';
import moment from 'moment';
import 'moment/locale/ko';

import Alert from 'components/override/alert/Alert';
import GridUtil from "components/override/grid/utils/GridUtil";
import Validator from 'components/override/utils/Validator';

import CommonStore from 'modules/pages/common/service/CommonStore';
import BaseStore from 'utils/base/BaseStore';

import QualityStatusRepository from "modules/pages/inventory/qualityStatus/service/QualityStatusRepository";
import EtcRepository from 'modules/pages/inout/etc/service/EtcRepository';

import EmployeeOptionModel from 'components/codehelper/model/EmployeeOptionModel';

import ItemOptionModel from 'components/codehelper/model/ItemOptionModel';

class QualityStatusStore extends BaseStore {

    constructor() {
        super()
        this.itemHelperOption_Qty.barcodeYn = 'N';
        this.setInitialState();
        this.validator.valid();
    }

    //필수항목 체크
    validator = new Validator([
        {
            field: 'findUserNo',
            method: 'isEmpty',
            message: '발견자'
        }
    ]);

    QtyValidator = new Validator([
        {
            field: 'findUserNo',
            method: 'isEmpty',
            message: '발견자'
        },
        {
            field: 'itemCd',
            method: 'isEmpty',
            message: '제품'
        }
    ]);

    //employee 변수
    @observable itemHelperOption = new EmployeeOptionModel().optionModel; //ItemHelper Option
    @observable selectedItemId = {}; //ItemHelper ReturnValue

    //ItemHelper(수량) 변수
    @observable itemHelperOption_Qty = new ItemOptionModel().optionModel; //ItemHelper Option
    @observable selectedItem_Qty = {}; //ItemHelper ReturnValue

    //조회 변수
    @observable buId = '';//CommonStore.buId;
    // @observable buList = CommonStore.buList;
    @observable findDtm = moment().add(-7, 'days').format('YYYYMMDD');
    @observable endFindDtm = moment().format('YYYYMMDD');
    @observable holdReasonId = 0;

    @observable reasonList = [];
    @observable modalReasonList = [];

    //Modal변수
    @observable holdModalOpen = false;
    @observable barcodeModalOpen = false;
    @observable modalFindDtm = moment().format('YYYYMMDD');
    @observable handleCellClicked = "";
    key = ''

    //등록 변수
    @observable time = moment().format('HH:mm');
    @observable modalHoldReasonId = 0;
    @observable holdContents = '';
    orderNo = 0;
    findUserNo = '';
    findUserNm = '';
    ledgerType = '';
    lotQuality = '';
    option = '';
    @observable buIdModal = '';
    @observable itemCdNm = '';

    //등록(수량) 변수
    @observable holdQtyModalOpen = false;
    itemId = '';
    itemCd = '';
    itemNm = '';
    @observable prodQty_q = 0;
    itemUom = 'KG';

    //그리드 변수
    holdList = [];
    holdGridApi = undefined;
    barcodeList = [];
    barcodeGridApi = undefined;
    @observable barcodeTotalDataGrid = [];
    barcodeModalGridApi = undefined;
    @observable addBarcodeList = []
    holdBarcodeGridApi = undefined;
    holdBarcodeList = [];

    //바코드 추가 변수
    @observable prodDt = '';
    @observable itemCd = '';
    @observable barcode = '';

    //등록에서 바코드 추가 변수
    add = [];

    @observable alphaCheck = false;

    //수정 모달 변수
    @observable holdUpdateModalOpen = false;
    @observable holdQtyUpdateModalOpen = false;

    //등록(수량) 열기
    @action.bound
    holdQtyInsert = () => {

        let allBu = this.buId.split(',');

        if (allBu.length > 1) {
            Alert.meg("선택된 공장이 없습니다.");
            return;
        }

        this.modalFindDtm = moment().format('YYYYMMDD');
        this.time = moment().format('HH:mm');

        this.itemId = '';
        this.itemCd = '';
        this.itemNm = '';
        // this.itemUom = '';
        this.findUserNo = '';
        this.findUserNm = '';
        this.holdContents = '';

        this.selectedItem_Qty.itemId = '';
        this.selectedItem_Qty.itemCd = '';
        this.selectedItem_Qty.itemNm = '';

        this.selectedItemId.loginNm = '';
        this.selectedItemId.empNo = '';

        this.prodQty_q = 0;

        this.buIdModal = this.buId;

        this.ledgerType = 'HD';
        this.option = '20';
        this.modalHoldReasonId = 1;
        this.holdQtyModalOpen = true;
    }

    //등록(수량) 닫기
    @action.bound
    holdQtyModalClose = () => {
        this.holdQtyModalOpen = false;
    }

    //ItemHelper 항목 선택전
    @action.bound
    handleHelperOpen = () => {
        this.itemHelperOption_Qty.buId = this.buId;
    }

    //ItemHelper(수량)) 항목 선택후
    @action.bound
    itemHelperResult_Qty = (result) => {

        if (result) {
            this.itemId = this.selectedItem_Qty.itemId;
            this.itemCd = this.selectedItem_Qty.itemCd;
            this.itemNm = this.selectedItem_Qty.itemNm;
            // this.itemUom = this.selectedItem_Qty.itemUom;
        }
    }

    //등록(수량) 저장
    @action.bound
    holdQtySave = async () => {

        const validation = this.QtyValidator.validate(this);
        if (!validation.isValid) {
            return;
        }

        const saveData = {
            "orderNo": this.orderNo
            , "holdDt": moment().format('YYYYMMDD')
            , "holdReasonId": this.modalHoldReasonId
            , "findUserNo": this.findUserNo
            , "findUserNm": this.findUserNm

            , "itemId": this.itemId
            , "processQty": this.prodQty_q
            , "processUom": this.itemUom
            , "buId": this.buIdModal
            , "findDtm": this.modalFindDtm + ' ' + this.time
            , "holdContents": this.holdContents
            , "ledgerType": this.ledgerType
            , "lotQuality": this.option
            , "option": this.option
            //   , "buId": this.buId
        }

        const value = await Alert.confirm("저장하시겠습니까?").then(result => { return result; });
        if (value) {

            const params = JSON.stringify({ param: saveData });

            let { data, status } = await QualityStatusRepository.saveHoldQty(params);

            if (status === 200) {

                this.holdList = data.data;
                this.holdGridApi.setRowData(this.holdList);

                this.holdQtyModalClose();

                //저장한 데이터로 검색조건으로 체인지
                this.findDtm = this.modalFindDtm;
                this.holdReasonId = this.modalHoldReasonId;
                this.buId = this.buIdModal;

                GridUtil.setFocusByRowIndex(this.holdGridApi, this.holdList.length - 1);
                this.getBarcode();
                Alert.meg("저장되었습니다.");
                // Alert.meg(data.msg);

            } else {
                Alert.meg(data.msg);
                return;
            }

            this.option = '';
        } else {
            return;
        }
    }




    //공장(메인) 불러오기
    @action.bound
    getBu = async () => {
        this.buId = CommonStore.buId;
        this.alphaCheck = true;
    }

    //공장(Modal) 불러오기
    @action.bound
    getBuModal = async () => {
        this.buIdModal = this.buId;
    }

    // 검색조건 - 메인 홀드사유List 넣기
    @action.bound
    getReason = async () => {
        const searchData = {
            "buId": this.buId
        }

        const { data, status } = await QualityStatusRepository.getReason(searchData);

        if (status == 200) {
            this.reasonList = data.data;
            this.modalReasonList = data.data;
        }
    }

    // 검색조건 - 메인 홀드사유List 넣기
    @action.bound
    getReasonModal = async () => {
        const searchData = {
            "buId": this.buId
        }

        const { data, status } = await QualityStatusRepository.getReason(searchData);

        if (status == 200) {
            this.reasonList = data.data;
            this.modalReasonList = data.data;
        }
    }


    // 검색조건 공장 선택 - 홀드사유List 변경
    @action.bound
    buSelectionChanged = async () => {

        this.getReason();
        // this.handleSearchClick();
    }

    //사원선택하고 발생(전역번수에 사원 선언)
    @action.bound
    handleHelperResult = (result) => {
        if (result) {
            this.findUserNo = this.selectedItemId.empNo;
            this.findUserNm = this.selectedItemId.loginNm;
        }
    }

    //search 값이 변하는지 확인
    @action.bound
    handleChange = (data) => {

        if (data.id == 'holdContents') {
            if (this.holdContents.length > 499) {
                data.value = data.value.substring(0, 500);
            }
        }

        if (data.id == 'selectedItemId') {
            if (data.value.loginNm == '') {
                this.findUserNo = '';
            }
        }

        if (data.id == 'selectedItem_Qty') {
            if (data.value.itemNm == '') {
                this.itemCd = '';
            }
        }

        this[data.id] = data.value;
    }

    // 품질상태관리 - 조회 버튼 클릭 
    @action.bound
    handleSearchClick = async () => {

        if (this.holdReasonId == '') {
            this.holdReasonId = 0
        }

        if (this.findDtm > this.endFindDtm) {
            var temp = this.findDtm
            this.findDtm = this.endFindDtm
            this.endFindDtm = temp
        }

        const searchData = {
            "buId"              : this.buId
            , "findDtm"         : this.findDtm
            , "endFindDtm"      : this.endFindDtm
            , "holdReasonId"    : this.holdReasonId
            , "searchItem"      : this.itemCdNm
        }

        let { data, status } = await QualityStatusRepository.getHold(searchData);

        if (status == 200) {
            this.holdList = data.data;
            this.holdGridApi.setRowData(this.holdList);
            this.barcodeGridApi.setRowData([]);
        } else {
            Alert.meg(data.msg);
        }
    }

    //등록 버튼 클릭
    @action.bound
    holdInsert = async () => {

        let allBu = this.buId.split(',');

        if (allBu.length > 1) {
            Alert.meg("선택된 공장이 없습니다.");
            return;
        }

        this.buIdModal = this.buId;
        this.selectedItemId.loginNm = '';
        this.selectedItemId.empNo = '';
        this.findUserNo = '';
        this.findUserNm = '';
        this.add = [];
        this.holdContents = '';
        this.holdModalOpen = true;
        this.ledgerType = 'HD';
        this.option = '20';
        this.key = 'Y';
        this.modalHoldReasonId = 1;
    }

    //  HOLD등록 - 저장 클릭
    @action.bound
    holdSave = async () => {

        //필수항목 체크
        const validation = this.validator.validate(this);

        //validation check
        if (!validation.isValid) {
            return;
        }

        const saveData = {
            "orderNo": this.orderNo
            , "holdDt": moment().format('YYYYMMDD')
            , "holdReasonId": this.modalHoldReasonId
            , "findUserNo": this.findUserNo
            , "findUserNm": this.findUserNm

            , "buId": this.buIdModal
            , "findDtm": this.modalFindDtm + ' ' + this.time
            , "holdContents": this.holdContents
            , "ledgerType": this.ledgerType
            , "lotQuality": this.option
            , "option": this.option
            // , "buId": this.buId
            , "itemId": ''
            , "processQty": 0
            , "processUom": ''
        }

        const value = await Alert.confirm("저장하시겠습니까?").then(result => { return result; });

        if (value) {
            this.holdBarcodeGridApi.selectAll();
            const selectedRows = this.holdBarcodeGridApi.getSelectedRows();
            const params = JSON.stringify({ param1: selectedRows, param2: saveData });
            let { data, status } = await QualityStatusRepository.saveHold(params);

            if (status === 200) {

                this.holdList = data.data;
                this.holdGridApi.setRowData(this.holdList);

                this.holdModalOpen = false;

                //저장한 데이터로 검색조건으로 체인지
                this.findDtm = this.modalFindDtm;
                this.holdReasonId = this.modalHoldReasonId;
                this.buId = this.buIdModal;
                this.handleSearchClick()
                GridUtil.setFocusByRowIndex(this.holdGridApi, this.holdList.length - 1);
                this.getBarcode();
                Alert.meg(data.msg);

            } else {
                Alert.meg(data.msg);
                return;
            }

            this.selectedItemId = {};
            this.findUserNm = "";
            this.holdContents = "";
            this.option = '';
        } else {
            return;
        }
    }

    // HOLD 등록 - 바코드목록 - 추가 클릭
    @action.bound
    addBarcodeModal = async () => {
        this.prodDt = moment().format('YYYYMMDD');
        this.itemCd = "";
        this.key = "Y";
        this.barcodeModalOpen = true;
    }

    // //HOLD등록 - 닫기 클릭
    @action.bound
    holdClose = async () => {
        this.holdModalOpen = false;
        this.selectedItemId = {};
        this.findUserNm = "";
        this.holdContents = "";
        this.key = "";
        this.option = '';
    }

    //HOLD등록 - X 클릭
    @action.bound
    holdModalClose = async () => {
        this.holdModalOpen = false;
        this.selectedItemId = {};
        this.findUserNm = "";
        this.holdContents = "";
        this.key = "";
        this.option = '';
    }

    //HOLD 그리드 ROW 클릭 
    @action.bound
    getBarcode = async () => {

        const selectedRow = GridUtil.getSelectedRowData(this.holdGridApi);
        if (selectedRow) {
            this.orderNo = selectedRow.orderNo;

        }

        const searchData = {
            "orderNo": this.orderNo
            //,"barcodeYn": selectedRow.barcodeYn
        }


        let { data, status } = await QualityStatusRepository.getBarcodeHis(searchData);

        if (status == 200) {

            this.barcodeList = data.data;

            this.barcodeGridApi.setRowData(this.barcodeList);
        }
    }

    //바코드내역 - 추가 클릭
    @action.bound
    addBarcode = async () => {
        const selectedRow = this.holdGridApi.getSelectedRows()[0];
        if (!selectedRow || selectedRow.length == 0) {
            Alert.meg("HOLD목록을 선택해 주세요.");
            return;
        }
        if (selectedRow.barcodeYn == 'N') {
            Alert.meg("추가 할수 없는 제품입니다.");
            return;
        }
        this.key = "";

        this.orderNo = selectedRow.orderNo;
        this.prodDt = moment().format('YYYYMMDD');
        this.itemCd = "";
        this.option = '20'; //confirom 키
        this.ledgerType = 'HD';
        this.barcodeModalOpen = true;
    }

    //바코드 추가 - 조회 버튼 클릭
    @action.bound
    barcodeSearch = async () => {

        const searchData = {
            "buId": this.buId
            , "barcode": this.barcode
            , "prodDt": this.prodDt
            , "itemNmOrItemCd": this.itemCd
        }

        let { data, status } = await EtcRepository.getBarcode(searchData);

        if (status == 200) {
            this.barcodeModalList = data.data;

            let holdList = [];
            this.barcodeModalList.forEach(item => {
                if (item.lotQuality == "10") {
                    holdList.push(item);
                }
            })
            this.barcodeModalGridApi.setRowData(holdList);

        } else {
            Alert.meg(data.msg);
        }
    }

    // 바코드 추가 - 저장 클릭
    @action.bound
    barcodeSave = async () => {
        if (this.key == 'Y') { //HOLD목록에서 저장 클릭시

            let selectedRows = this.barcodeModalGridApi.getSelectedRows();

            if (!selectedRows || selectedRows.length == 0) {
                Alert.meg("바코드를 선택해 주세요.");
                return;
            }

            selectedRows.forEach(item => {
                if (!this.add.some(x => x.barcode == item.barcode)) {
                    this.add.push(item);
                }
            })

            // this.holdBarcodeList = selectedRows;
            this.holdBarcodeGridApi.setRowData(this.add);
            this.barcodeModalOpen = false;

            this.key = "";



        } else { //바코드내역에서 추가 - 저장 클릭

            const selectedRows = this.barcodeModalGridApi.getSelectedRows();

            if (!selectedRows || selectedRows.length == 0) {
                Alert.meg("바코드를 선택해 주세요.");
                return;
            }

            const searchData = {
                "orderNo": this.orderNo
                , "lotQuality": this.option
                , "ledgerType": this.ledgerType
                , "option": this.option
            }

            const value = await Alert.confirm("저장하시겠습니까?").then(result => { return result; });

            if (value) {

                const params = JSON.stringify({ param1: selectedRows, param2: searchData });


                let { data, status } = await QualityStatusRepository.saveBarcode(params);

                if (status == 200) {
                    Alert.meg(data.msg);
                    // this.barcodeList = data.data;

                    // this.barcodeGridApi.setRowData(this.barcodeList);
                    this.getBarcode();
                    this.barcodeModalOpen = false;
                } else {
                    Alert.meg(data.msg);
                    return;
                }
            } else {
                return;
            }
        }
    }

    //HOLD 삭제
    @action.bound
    delHold = async () => {
        const selectedRows = this.holdGridApi.getSelectedRows();

        const holdRow = this.barcodeList;

        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg("HOLD를 선택해 주세요.");
            return;
        }

        for (let i = 0; i < holdRow.length; i++) {

            if (holdRow[i].lotQuality != '10') {
                Alert.meg("상태가 정상이 아닌 바코드가 있습니다.");
                return;
            }
        }

        const searchData = {
            "buId": this.buId
            , "findDtm": this.findDtm
            , "holdReasonId": this.holdReasonId

        }

        const value = await Alert.confirm("삭제하시겠습니까?").then(result => { return result; });

        if (value) {

            const params = JSON.stringify({ param1: selectedRows, param2: searchData });

            let { data, status } = await QualityStatusRepository.delHold(params);

            if (status == 200) {

                this.holdList = data.data;
                this.holdGridApi.setRowData(this.holdList);
                this.barcodeGridApi.setRowData([]);
                Alert.meg(data.msg);
            } else {
                Alert.meg(data.msg);
                return;
            }
        } else {
            return;
        }

    }

    //바코드추가 - x 클릭
    @action.bound
    barcodeModalClose = async () => {
        this.barcodeModalOpen = false;
    }

    // 정품 클릭
    @action.bound
    genuine = async () => {

        this.lotQuality = '10';

        const holdRow = this.holdGridApi.getSelectedRows()[0];
        // const barcodRow = this.barcodeGridApi.getSelectedRows();
        const barcodRow = GridUtil.getGridCheckArray(this.barcodeGridApi)
        if (!barcodRow || barcodRow.length == 0) {
            Alert.meg("항목을 선택해 주세요.");
            return;
        }

        for (let i = 0; i < barcodRow.length; i++) {

            if (!(barcodRow[i].lotQuality == '20' || barcodRow[i].lotQuality == '30')) {
                Alert.meg("HOLD상태가 아닌 바코드가 있습니다.");

                GridUtil.setFocusByRowIndex(this.barcodeGridApi, barcodRow[i]);
                return;
            }

        }

        const genuineData = {
            "orderNo": holdRow.orderNo
            , "option": this.lotQuality
        }

        const value = await Alert.confirm("정품으로 등록하시겠습니까?").then(result => { return result; });

        const params = JSON.stringify({ param1: barcodRow, param2: genuineData });

        if (value) {
            const { data, status } = await QualityStatusRepository.genuine(params);
            if (status == 200) {
                Alert.meg(data.msg);
                this.getBarcode();

            } else {
                Alert.meg(data.msg);
            }
        } else {
            return;
        }
    }

    //불용
    @action.bound
    disuseBarcode = async () => {
        this.lotQuality = '30';

        const holdRow = GridUtil.getSelectedRowData(this.holdGridApi)
        // const barcodRow = this.barcodeGridApi.getSelectedRows();
        const barcodRow = GridUtil.getGridCheckArray(this.barcodeGridApi)
        if (!barcodRow || barcodRow.length == 0) {
            Alert.meg("바코드를 선택해 주세요.");
            return;
        }

        for (let i = 0; i < barcodRow.length; i++) {

            if (barcodRow[i].lotQuality != '20') {
                Alert.meg("HOLD상태가 아닌 바코드가 있습니다.");
                // GridUtil.setFocusByRowIndex(this.barcodeGridApi, barcodRow[i]);
                return;
            }

        }

        const genuineData = {
            "orderNo": holdRow.orderNo
            , "option": this.lotQuality
        }

        const value = await Alert.confirm("불용처리하시겠습니까?").then(result => { return result; });

        const params = JSON.stringify({ param1: barcodRow, param2: genuineData });

        if (value) {
            const { data, status } = await QualityStatusRepository.genuine(params);
            if (status == 200) {
                Alert.meg(data.msg);
                // this.barcodeList = data.data;
                // this.barcodeGridApi.setRowData(this.barcodeList);
                this.getBarcode();

            } else {
                Alert.meg(data.msg);
            }
        } else {
            return;
        }
    }




    //제거 클릭시
    @action.bound
    holdBarcodeDelete = async () => {


        const selectedRows = this.holdBarcodeGridApi.getSelectedRows();

        selectedRows.forEach(selectedRow => {
            this.holdBarcodeGridApi.updateRowData({ remove: [selectedRow] });
            const index = this.add.findIndex(x => x.barcode == selectedRow.barcode);

            this.add.splice(index, 1);
        })
    }






    //HOLD ROW 더블 클릭 - 수정 모달창 열기
    @action.bound
    openModalEdit = () => {

        const selectedRow = GridUtil.getSelectedRowData(this.holdGridApi);

        if (selectedRow.barcodeYn == 'Y') {

            for (let i = 0; i < this.barcodeList.length; i++) {

                if (this.barcodeList[i].lotQuality != "10") {
                    Alert.meg("상태가 정상이 아닌 바코드가 있습니다.");
                    return;
                }
            }

            const fullDtm = selectedRow.findDtm;
            const dtm = fullDtm.substring(0, 10);
            const time = fullDtm.substring(11, 16);


            this.buIdModal = selectedRow.buId;
            this.modalFindDtm = dtm;
            this.time = time;
            this.modalHoldReasonId = selectedRow.holdReasonId;

            this.selectedItemId.loginNm = selectedRow.findUserNm;
            this.selectedItemId.empNo = selectedRow.findUserNo;
            this.findUserNm = selectedRow.findUserNm;
            this.findUserNo = selectedRow.findUserNo;

            this.holdContents = selectedRow.holdContents;



            this.holdUpdateModalOpen = true;



        } else {

            for (let i = 0; i < this.barcodeList.length; i++) {

                if (this.barcodeList[i].lotQuality != "10") {
                    Alert.meg("상태가 정상이 아닌 바코드가 있습니다.");
                    return;
                }
            }

            const fullDtm = selectedRow.findDtm;
            const dtm = fullDtm.substring(0, 10);
            const time = fullDtm.substring(11, 16);


            this.buIdModal = selectedRow.buId;
            this.modalFindDtm = dtm;
            this.time = time;
            this.modalHoldReasonId = selectedRow.holdReasonId;

            this.selectedItemId.loginNm = selectedRow.findUserNm;
            this.selectedItemId.empNo = selectedRow.findUserNo;
            this.findUserNm = selectedRow.findUserNm;
            this.findUserNo = selectedRow.findUserNo;

            this.selectedItem_Qty.itemId = this.barcodeList[0].itemId;
            this.selectedItem_Qty.itemCd = this.barcodeList[0].itemCd;
            this.selectedItem_Qty.itemNm = this.barcodeList[0].itemNm;
            this.itemId = this.barcodeList[0].itemId;
            this.itemCd = this.barcodeList[0].itemCd;
            this.itemNm = this.barcodeList[0].itemNm;


            this.holdContents = selectedRow.holdContents;
            this.prodQty_q = this.barcodeList[0].holdQty;



            this.holdQtyUpdateModalOpen = true;

        }
    }

    //수정(바코드) 저장
    @action.bound
    holdUpdate = async () => {

        const selectedRow = GridUtil.getSelectedRowData(this.holdGridApi);

        const saveData = {
            "orderNo": selectedRow.orderNo
            , "holdDt": moment().format('YYYYMMDD')
            , "holdReasonId": this.modalHoldReasonId
            , "holdContents": this.holdContents
            , "findUserNo": this.findUserNo
            , "findUserNm": this.findUserNm
            , "findDtm": this.modalFindDtm + ' ' + this.time
            , "buId": this.buIdModal

        }

        const value = await Alert.confirm("저장하시겠습니까?").then(result => { return result; });

        if (value) {
            const params = JSON.stringify({ param: saveData });

            let { data, status } = await QualityStatusRepository.updateHold(params);

            if (status == 200) {
                Alert.meg(data.msg);
                this.holdList = data.data;

                this.holdGridApi.setRowData(this.holdList);
                this.holdUpdateModalOpen = false;

            } else {
                Alert.meg(data.msg);
                return;
            }
        } else {
            return;
        }

    }

    //수정(수량) 저장
    @action.bound
    holdQtyUpdate = async () => {

    }





    //수정(바코드) 닫기
    @action.bound
    holdUpdateModalClose = () => {
        this.holdUpdateModalOpen = false;
    }

    //수정(수량) 닫기
    @action.bound
    holdQtyUpdateModalClose = () => {
        this.holdQtyUpdateModalOpen = false;
    }













    //HoLD Grid
    @action.bound
    setGridApi = async (gridApi) => {

        this.holdGridApi = gridApi;

        const columnDefs = [

            {
                headerName: "지시번호"
                , field: "orderNo"
                , width: 80
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "발견일자"
                , field: "findDtm"
                , width: 80
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            }, {
                headerName: "HOLD사유"
                , field: "holdNm"
                , width: 115
                , cellClass: 'cell_align_left'
            },
            {
                headerName: "발견자"
                , field: "findUserNm"
                , width: 80
                , cellClass: 'cell_align_center'

            },
            {
                headerName: "비고"
                , field: "holdContents"
                , width: 430
                , cellClass: 'cell_align_left'
            },
            {
                headerName: "등록자"
                , field: "inputEmpNm"
                , width: 80
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "등록일"
                , field: "inputDtm"
                , width: 80
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            }
        ];
        this.holdGridApi.setColumnDefs(columnDefs);
    }

    //HOLD 바코드 내역 Grid
    @action.bound
    setBarcodeGridApi = async (gridApi) => {

        this.barcodeGridApi = gridApi;
        this.barcodeTotalDataGrid = [
            {
                seq: "합계",
                availableQty: 0,
                holdQty: 0,
                disuseQty: 0
            }
        ];
        const columnDefs = [
            {
                headerName: "",
                children: [
                    {
                        headerName: ""
                        , field: "check"
                        , width: 35
                        , checkboxSelection: true
                        , headerCheckboxSelection: true
                    },
                    {
                        headerName: "순번"
                        , field: "seq"
                        , width: 70
                        , type: "numericColumn"
                        , valueFormatter: GridUtil.numberFormatter
                        , valueParser: GridUtil.numberParser
                    },
                    {
                        headerName: "바코드번호"
                        , field: "barcode"
                        , width: 100
                        , cellClass: 'cell_align_center'
                    },
                    {
                        headerName: "상태"
                        , field: "lotQuality"
                        , width: 70
                        , cellClass: 'cell_align_center'
                        , valueFormatter: (params) => GridUtil.getCodeNm(params, 'MFD003')
                    },
                    {
                        headerName: "제품코드"
                        , field: "itemCd"
                        , width: 90
                        , cellClass: 'cell_align_center'
                    },
                    {
                        headerName: "LOT"
                        , field: "lotNo"
                        , width: 90
                        , cellClass: 'cell_align_center'
                    },
                    {
                        headerName: "생산일자"
                        , field: "prodDt"
                        , width: 90
                        , cellClass: 'cell_align_center'
                        , valueFormatter: GridUtil.dateFormatter
                    },
                    {
                        headerName: "유통기한"
                        , field: "expiryDt"
                        , width: 100
                        , cellClass: 'cell_align_center'
                        , valueFormatter: GridUtil.dateFormatter
                    }
                ]
            },
            {
                headerName: "수량",
                children: [
                    {
                        headerName: "가용량"
                        , field: "availableQty"
                        , width: 80
                        , type: "numericColumn"
                        , valueFormatter: GridUtil.numberFormatter
                        , valueParser: GridUtil.numberParser
                        , pinnedRowCellRenderer: "customPinnedRowRenderer"
                        , pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
                    },
                    {
                        headerName: "홀드량"
                        , field: "holdQty"
                        , width: 80
                        , type: "numericColumn"
                        , valueFormatter: GridUtil.numberFormatter
                        , valueParser: GridUtil.numberParser
                        , pinnedRowCellRenderer: "customPinnedRowRenderer"
                        , pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
                    },
                    {
                        headerName: "불용량"
                        , field: "disuseQty"
                        , width: 80
                        , type: "numericColumn"
                        , valueFormatter: GridUtil.numberFormatter
                        , valueParser: GridUtil.numberParser
                        , pinnedRowCellRenderer: "customPinnedRowRenderer"
                        , pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
                    },
                    {
                        headerName: "단위"
                        , field: "processUom"
                        , width: 65
                        , cellClass: 'cell_align_center'
                    }
                ]
            },
            {
                headerName: "",
                children: [
                    {
                        headerName: "위치"
                        , field: "locNm"
                        , width: 100
                        , cellClass: 'cell_align_center'
                    },
                    {
                        headerName: "처리시간"
                        , field: "inputDtm"
                        , width: 105
                        , cellClass: 'cell_align_center'
                        , valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm')
                    },
                    {
                        headerName: "처리자"
                        , field: "inputEmpNm"
                        , width: 90
                        , cellClass: 'cell_align_center'
                    }
                ]
            }
        ];
        this.barcodeGridApi.setColumnDefs(columnDefs);
    }

    // 바코드 추가 그리드
    @action.bound
    setbarcodeModalGridApi = async (gridApi) => {
        this.barcodeModalGridApi = gridApi;

        const columnDefs = [
            {
                headerName: ""
                , field: "check"
                , width: 35
                , checkboxSelection: true
                , headerCheckboxSelection: true
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "순번"
                , field: "seq"
                , width: 70
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
            },
            {
                headerName: "바코드번호"
                , field: "barcode"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "상태"
                , field: "lotQualityNm"
                , width: 70
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "제품코드"
                , field: "itemCd"
                , width: 90
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "LOT"
                , field: "lotNo"
                , width: 90
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "생산일자"
                , field: "prodDt"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "유통기한"
                , field: "expiryDt"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "재고량"
                , field: "processQty"
                , width: 100
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
            },
            {
                headerName: "위치"
                , field: "locNm"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "처리시간"
                , field: "inputDtm"
                , width: 105
                , cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm')
            },
            {
                headerName: "처리자"
                , field: "inputEmpNm"
                , width: 90
                , cellClass: 'cell_align_center'
            }
        ];
        this.barcodeModalGridApi.setColumnDefs(columnDefs);
    }

    //Modal바코드 그리드
    @action.bound
    setHoldBarcodeGridApi = async (gridApi) => {

        this.holdBarcodeGridApi = gridApi;

        const columnDefs = [
            {
                headerName: ""
                , field: "check"
                , width: 35
                , checkboxSelection: true
                , headerCheckboxSelection: true
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "순번"
                , field: "seq"
                , width: 70
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
            },
            {
                headerName: "바코드번호"
                , field: "barcode"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "상태"
                , field: "lotQualityNm"
                , width: 70
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "제품코드"
                , field: "itemCd"
                , width: 90
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "LOT"
                , field: "lotNo"
                , width: 90
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "생산일자"
                , field: "prodDt"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "유통기한"
                , field: "expiryDt"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "재고량"
                , field: "processQty"
                , width: 100
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
            },
            {
                headerName: "위치"
                , field: "locNm"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "처리시간"
                , field: "inputDtm"
                , width: 105
                , cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm')
            },
            {
                headerName: "처리자"
                , field: "inputEmpNm"
                , width: 90
                , cellClass: 'cell_align_center'
            }
        ];

        this.holdBarcodeGridApi.setColumnDefs(columnDefs);
    }
}

export default new QualityStatusStore();