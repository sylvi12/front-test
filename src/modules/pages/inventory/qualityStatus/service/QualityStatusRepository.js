import API from 'components/override/api/API';

class QualityStatusRepository {

    URL = "api/inventory/qualityStatus";

    /* 조회 */
    //HOLD사유 리스트 가져오기
    getReason(params) {
        return API.request.get(encodeURI(`${this.URL}/getReason?buId=${params.buId}`))
    }

    // HOLD목록 조회
    getHold(params) {
        
        return API.request.get(encodeURI(`${this.URL}/search?buId=${params.buId}&findDtm=${params.findDtm}&endFindDtm=${params.endFindDtm}&holdReasonId=${params.holdReasonId}&searchItem=${params.searchItem}`));
    }
    getBarcodeHis(params) {
        console.log(params);
        return API.request.get(encodeURI(`${this.URL}/getBarcodeHis?orderNo=${params.orderNo}&barcodeYn=${params.barcodeYn}`));
    }

    /* 등록 */
    // HOLD 등록
    saveHold(params) {
        
        return API.request.post(encodeURI(`${this.URL}/saveHold`), params);
    }

    //HOLD 등록(수량)
    saveHoldQty(param) {
        return API.request.post(encodeURI(`${this.URL}/saveholdqty`), param);
    }

    /* 수정 */
    //수정(바코드)
    updateHold(param) {
        return API.request.post(encodeURI(`${this.URL}/updatehold`), param);
    }

    // 바코드 등록
    saveBarcode(params) {
        
        return API.request.post(encodeURI(`${this.URL}/saveBarcode`), params);
    }

    /* 확정 */
    confirm(param) {
        return API.request.post(encodeURI(`${this.URL}/confirm`), param)
    }

    /* 정품 */
    genuine(param) {
        
        return API.request.post(encodeURI(`${this.URL}/genuine`), param)
    }
    
    /* 삭제 */
    delHold(params) {
        return API.request.post(encodeURI(`${this.URL}/delhold`), params)
    }

}

export default new QualityStatusRepository();