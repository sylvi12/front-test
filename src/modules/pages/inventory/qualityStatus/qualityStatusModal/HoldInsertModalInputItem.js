import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';
import SInput from "components/atoms/input/SInput";
import SSelectBox from "components/atoms/selectbox/SSelectBox";
import SDatePicker from "components/override/datepicker/SDatePicker";

import QualityStatusStore from 'modules/pages/inventory/qualityStatus/service/QualityStatusStore';

@inject(stores => ({
    holdSave: stores.qualityStatusStore.holdSave,
    holdClose: stores.qualityStatusStore.holdClose,
    buId: stores.qualityStatusStore.buId, 
    buList: stores.qualityStatusStore.buList, 
    findDtm: stores.qualityStatusStore.findDtm, 
    findUserNm: stores.qualityStatusStore.findUserNm, 
    holdReasonId: stores.qualityStatusStore.holdReasonId, 
    holdContents: stores.qualityStatusStore.holdContents, 
    handleChange: stores.qualityStatusStore.handleChange
}))

class HoldInsertModalInputItem extends Component {

    constructor(props) {
        super(props);
        QualityStatusStore.getBu();
    }

    render() {

        const holdReasonIdOption = [
            {
                cd: "1",
                cdNm: "파대"
            },
            {
                cd: "2",
                cdNm: "난대"
            }
        ]
        const { holdSave, holdClose, buId, buList, findDtm, findUserNm, holdReasonId, holdContents, handleChange} = this.props;

        return(
            <React.Fragment>
                <div>
                    {/* <SButton
                        buttonName={"닫기"}
                        type={"default"}
                        onClick={holdClose}
                    /> */}
                    <SButton
                        buttonName={"저장"}
                        type={"btnHoldSave"}
                        onClick={holdSave}
                    />
                </div>

                <table style={{ width: "100%" }}>
                    <tbody>
                        <tr>
                            <th>공장</th>
                            <td>
                                <SSelectBox
                                    id={"buId"}
                                    value={buId}
                                    onChange={handleChange}
                                    optionGroup={buList}
                                    valueMember={"buId"}
                                    displayMember={"buNm"}
                                />
                            </td>
                               
                            <th>발견일시</th>
                            <td>
                                <SDatePicker
                                    id="findDtm"
                                    value={findDtm}
                                    onChange={handleChange}
                                />
                            </td>
                        </tr>
                        <tr>
                            <th>발견자</th>
                            <td>
                                <SInput
                                    id="findUserNm"
                                    value={findUserNm}
                                    onChange={handleChange}
                                />
                            </td>
                            <th>HOLD구분</th>
                            <td>
                                {/* <SSelectBox
                                    title={"홀드사유"}
                                    id={"holdReasonId"}
                                    value={holdReasonId}
                                    optionGroup={holdReasonIdOption}
                                    onChange={handleChange}
                                    addOption={"전체"}
                                /> */}
                            </td>
                        </tr>
                        <tr>
                            <th>내용</th>
                            <td colSpan="3">
                                <SInput
                                    id="holdContents"
                                    value={holdContents}
                                    onChange={handleChange}
                                    contentWidth={400}
                                />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </React.Fragment>
        );
    }
}

export default HoldInsertModalInputItem;