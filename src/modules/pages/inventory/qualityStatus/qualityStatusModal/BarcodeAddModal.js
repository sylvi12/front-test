import React, { Component } from 'react';

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import BarcodeAddModalSearchItem from "modules/pages/inventory/qualityStatus/qualityStatusModal/BarcodeAddModalSearchItem";
import BarcodeAddModalMiddleItem from 'modules/pages/inventory/qualityStatus/qualityStatusModal/BarcodeAddModalMiddleItem';
import BarcodeAddModalContent from 'modules/pages/inventory/qualityStatus/qualityStatusModal/BarcodeAddModalContent';

class BarcodeAddModal extends Component {

    constructor(props){
        super(props);
    }

    render() {
        return (
            <React.Fragment>
                
                    <SearchTemplate searchItem={<BarcodeAddModalSearchItem />} />
                    <ContentsMiddleTemplate contentSubTitle={'LOT 정보'} middleItem={<BarcodeAddModalMiddleItem />} />
                
                    <ContentsTemplate id='etcBarcodeUpsertModal' contentItem={<BarcodeAddModalContent />} />
                
            </React.Fragment>
        );
    }
}

export default BarcodeAddModal;