import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    addBarcodeModal: stores.qualityStatusStore.addBarcodeModal
    , holdBarcodeDelete: stores.qualityStatusStore.holdBarcodeDelete
}))

class HoldInsertModalMiddleItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {addBarcodeModal, holdBarcodeDelete} = this.props;

        return(
            <React.Fragment>
                <SButton buttonName={"추가"} onClick={addBarcodeModal} type={"btnAddBarcode"} />
                <SButton
                    className="btn_red"
                    buttonName={'제거'}
                    type={'btnBarcodeDelete'}
                    onClick={holdBarcodeDelete}
                />
            </React.Fragment>

        );
    }
}

export default HoldInsertModalMiddleItem;