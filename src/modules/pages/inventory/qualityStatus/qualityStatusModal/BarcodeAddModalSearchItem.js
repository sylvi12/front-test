import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SDatePicker from 'components/override/datepicker/SDatePicker';
import SInput from 'components/atoms/input/SInput'
import SButton from 'components/atoms/button/SButton'

@inject(stores => ({
    barcode: stores.qualityStatusStore.barcode,
    prodDt: stores.qualityStatusStore.prodDt,
    ItemCd: stores.qualityStatusStore.ItemCd,
    handleChange: stores.qualityStatusStore.handleChange,
    barcodeSearch: stores.qualityStatusStore.barcodeSearch
}))

class BarcodeAddModalSearchItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { barcode, prodDt, ItemCd, handleChange, barcodeSearch } = this.props;
        return (
            <React.Fragment>

                <SInput
                    title={"바코드"}
                    id={"barcode"}
                    value={barcode}
                    onChange={handleChange}
                    onEnterKeyDown={barcodeSearch}
                />

                <SDatePicker
                    title={"생산일자"}
                    id="prodDt"
                    value={prodDt}
                    onChange={handleChange}
                />

                <SInput
                    title={"제품"}
                    id={"ItemCd"}
                    value={ItemCd}
                    onChange={handleChange}
                    onEnterKeyDown={barcodeSearch}
                />

                <div className='search_item_btn' style={{ right: "" }}>
                    <SButton buttonName={'조회'} onClick={barcodeSearch} type={'btnSearch'} />
                </div>
            </React.Fragment>
        );
    }
}

export default BarcodeAddModalSearchItem;