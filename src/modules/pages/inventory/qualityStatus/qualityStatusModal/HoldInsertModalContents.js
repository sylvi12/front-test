import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid';

@inject(stores => ({
    setHoldBarcodeGridApi: stores.qualityStatusStore.setHoldBarcodeGridApi,
    holdBarcodeList: stores.qualityStatusStore.holdBarcodeList,
    barcodeTotalDataGrid: stores.qualityStatusStore.barcodeTotalDataGrid,
    handleCellClicked: stores.qualityStatusStore.handleCellClicked
}))

class HoldInsertModalContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setHoldBarcodeGridApi, holdBarcodeList, barcodeTotalDataGrid, handleCellClicked } = this.props;
        return(
                //콜백이름 바꾸기
            <SGrid 
                grid={'holdBarcodeGrid'}
                gridApiCallBack={setHoldBarcodeGridApi}
                rowData={holdBarcodeList}
                editable={false}
                onCellClicked={handleCellClicked}
                pinnedBottomRowData={barcodeTotalDataGrid}
                suppressRowClickSelection = {true}
                
            />


        );
    }
    

}

export default HoldInsertModalContents;