import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    holdInsert: stores.qualityStatusStore.holdInsert,
    holdConfirm: stores.qualityStatusStore.holdConfirm,
    confirmCancel: stores.qualityStatusStore.confirmCancel,
    delHold: stores.qualityStatusStore.delHold,
    holdQtyInsert: stores.qualityStatusStore.holdQtyInsert
}))

class QualityStatusMiddleItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {
         
        const { holdInsert, delHold, holdQtyInsert, holdConfirm, confirmCancel } = this.props;

        return(
            <React.Fragment>
                
                <SButton
                    className="btn_red"
                    buttonName={'등록(수량)'}
                    type={'btnHoldQtyInsert'}
                    onClick={holdQtyInsert}
                />
                <SButton
                    className="btn_red"
                    buttonName={'등록(바코드)'}
                    type={'btnHoldInsert'}
                    onClick={holdInsert}
                />


                <SButton
                    className="btn_red"
                    buttonName={'삭제'}
                    type={'btnDelHold'}
                    onClick={delHold}
                />
                {/* <SButton
                    className="btn_red"
                    buttonName={'확정취소'}
                    type={'update'}
                    onClick={confirmCancel}
                /> */}
            </React.Fragment>
        );
    }
}

export default QualityStatusMiddleItem;