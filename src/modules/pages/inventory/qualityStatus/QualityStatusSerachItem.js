import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SButton from 'components/atoms/button/SButton';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';

import QualityStatusStore from 'modules/pages/inventory/qualityStatus/service/QualityStatusStore';
import SBuSelectBox from 'components/override/business/SBuSelectBox';

@inject(stores => ({
    handleChange: stores.qualityStatusStore.handleChange
    , handleSearchClick: stores.qualityStatusStore.handleSearchClick
    , buId: stores.qualityStatusStore.buId
    , buList: stores.qualityStatusStore.buList
    , findDtm: stores.qualityStatusStore.findDtm
    , endFindDtm: stores.qualityStatusStore.endFindDtm
    , invenCheck: stores.qualityStatusStore.invenCheck
    , lotQuality: stores.qualityStatusStore.lotQuality
    , itemCdNm: stores.qualityStatusStore.itemCdNm
    , locId: stores.qualityStatusStore.locId
    , locList: stores.qualityStatusStore.locList
    , handleSearchClick: stores.qualityStatusStore.handleSearchClick
    , dateSelectionChange: stores.qualityStatusStore.dateSelectionChange
    , buSelectionChanged: stores.qualityStatusStore.buSelectionChanged
    , holdReasonId: stores.qualityStatusStore.holdReasonId
    , reasonList: stores.qualityStatusStore.reasonList
    , itemCdNm: stores.qualityStatusStore.itemCdNm

}))

class QualityStatusSerachItem extends Component {

    constructor(props) {
        super(props);

    }

    async componentWillMount() {
        if (QualityStatusStore.alphaCheck == false) {
            await QualityStatusStore.getBu();
            await QualityStatusStore.getReason();
        }
    }

    render() {

        const { buId
            , buList
            , findDtm
            , holdReasonId
            , reasonList
            , dateSelectionChange
            , handleChange
            , handleSearchClick
            , endFindDtm
            , buSelectionChanged
            , itemCdNm } = this.props;

        return (
            <React.Fragment>
                <div className='search_item_btn'>
                    <SButton
                        buttonName={'조회'}
                        type={'btnSearch'}
                        onClick={handleSearchClick}
                    />
                </div>
                <div style={{ display: "inline_block" }}>
                    <div className='search_line'>
                        <SBuSelectBox
                            title={"공장"}
                            id={"buId"}
                            value={buId}
                            onChange={handleChange}
                            onSelectionChange={buSelectionChanged}
                        />

                        <SDatePicker
                            title={"발견일자"}
                            id={"findDtm"}
                            value={findDtm}
                            onChange={handleChange}
                        />

                        <SDatePicker
                            title={"~"}
                            id={"endFindDtm"}
                            value={endFindDtm}
                            onChange={handleChange}
                        />

                        <SSelectBox
                            title={"HOLD사유"}
                            id={"holdReasonId"}
                            value={holdReasonId}
                            optionGroup={reasonList}
                            valueMember={"holdReasonId"}
                            displayMember={"holdNm"}
                            onChange={handleChange}
                            setList={reasonList}
                            onSelectionChange={dateSelectionChange}
                            addOption={"전체"}
                            contentWidth={250}
                        />

                        <SInput
                            title={"제품"}
                            id={"itemCdNm"}
                            value={itemCdNm}
                            onChange={handleChange}
                            onEnterKeyDown={handleSearchClick}
                        />
                    </div>
                </div>
            </React.Fragment>

        );
    }

}

export default QualityStatusSerachItem;