import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid';

@inject(stores => ({
    setBarcodeGridApi: stores.qualityStatusStore.setBarcodeGridApi,
    barcodeList: stores.qualityStatusStore.barcodeList,
    barcodeTotalDataGrid: stores.qualityStatusStore.barcodeTotalDataGrid,
    handleCellClicked: stores.qualityStatusStore.handleCellClicked
}))

class QualityStatusBarcodeContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setBarcodeGridApi, barcodeList, barcodeTotalDataGrid, handleCellClicked } =this.props;
        
        return(

            <SGrid 
                grid={'barcodeGrid'}
                gridApiCallBack={setBarcodeGridApi}
                rowData={barcodeList}
                editable={false}
                onCellClicked={handleCellClicked}
                pinnedBottomRowData={barcodeTotalDataGrid}
                suppressRowClickSelection = {true}
            />

        );
    }
}

export default QualityStatusBarcodeContents;