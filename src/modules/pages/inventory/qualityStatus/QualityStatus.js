import React, { Component } from "react";
import { inject } from "mobx-react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";
import SModal from "components/override/modal/SModal";

import QualityStatusSerachItem from "modules/pages/inventory/qualityStatus/QualityStatusSerachItem";
import QualityStatusContents from "modules/pages/inventory/qualityStatus/QualityStatusContents";
import QualityStatusBarcodeContents from "modules/pages/inventory/qualityStatus/QualityStatusBarcodeContents";
import QualityStatusMiddleItem from "modules/pages/inventory/qualityStatus/QualityStatusMiddleItem";
import QualityStatusBarcodeMiddleItem from "modules/pages/inventory/qualityStatus/QualityStatusBarcodeMiddleItem";

import HoldInsertModal from "modules/pages/inventory/qualityStatus/qualityStatusModal/HoldInsertModal";

import BarcodeAddModal from "modules/pages/inventory/qualityStatus/qualityStatusModal/BarcodeAddModal";
import HoldQtyInsertModal from "modules/pages/inventory/qualityStatus/qualityStatusModal/HoldQtyInsertModal";
import HoldQtyUpdateModal from "./qualityStatusUpdateModal/HoldQtyUpdateModal";
import HoldUpdateModal from "./qualityStatusUpdateModal/HoldUpdateModal";

@inject(stores => ({
    holdModalOpen: stores.qualityStatusStore.holdModalOpen,
    holdModalClose: stores.qualityStatusStore.holdModalClose,
    barcodeModalOpen: stores.qualityStatusStore.barcodeModalOpen,
    barcodeModalClose: stores.qualityStatusStore.barcodeModalClose,
    holdQtyModalOpen: stores.qualityStatusStore.holdQtyModalOpen,
    holdQtyModalClose: stores.qualityStatusStore.holdQtyModalClose,
    holdQtyUpdateModalOpen: stores.qualityStatusStore.holdQtyUpdateModalOpen,
    holdQtyUpdateModalClose: stores.qualityStatusStore.holdQtyUpdateModalClose,
    holdUpdateModalOpen: stores.qualityStatusStore.holdUpdateModalOpen,
    holdUpdateModalClose: stores.qualityStatusStore.holdUpdateModalClose

}))

class QualityStatus extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { holdModalOpen, holdModalClose, barcodeModalOpen, barcodeModalClose, holdQtyModalOpen, holdQtyModalClose
                , holdQtyUpdateModalOpen
                , holdQtyUpdateModalClose
                , holdUpdateModalOpen
                , holdUpdateModalClose } = this.props;
        return (
            <React.Fragment>

                <div style={{ width: '100%', height: '65%', float: 'left' }}>
                    <SearchTemplate searchItem={<QualityStatusSerachItem />} />
                    <ContentsMiddleTemplate contentSubTitle={'HOLD 내역'} middleItem={<QualityStatusMiddleItem />} />
                    <ContentsTemplate id="qualityStatus" contentItem={<QualityStatusContents />} />
                </div>
                
                <div style={{ width: '100%', height: '35%', float: 'left' }}>
                    <ContentsMiddleTemplate contentSubTitle={'바코드 내역'} middleItem={<QualityStatusBarcodeMiddleItem />} />
                    <ContentsTemplate id="barcode" contentItem={<QualityStatusBarcodeContents />} />
                </div>

                <SModal
                    formId={"inventory_qualityStatus_hold_p"}
                    id="hold"
                    isOpen={holdModalOpen}
                    onRequestClose={holdModalClose}
                    contentLabel="HOLD 등록(바코드)"
                    width={800}
                    height={500}
                    contents={<HoldInsertModal />}
                />

                <SModal
                    formId={"inventory_qualityStatus_hold_qty_p"}
                    id="holdQty"
                    isOpen={holdQtyModalOpen}
                    onRequestClose={holdQtyModalClose}
                    contentLabel="HOLD 등록(수량)"
                    width={800}
                    height={300}
                    contents={<HoldQtyInsertModal />}
                />

                <SModal
                    formId={"inventory_qualityStatus_hold_p"}
                    id="hold"
                    isOpen={holdUpdateModalOpen}
                    onRequestClose={holdUpdateModalClose}
                    contentLabel="HOLD 수정(바코드)"
                    width={800}
                    height={300}
                    contents={<HoldUpdateModal />}
                />

                <SModal
                    formId={"inventory_qualityStatus_hold_qty_update_p"}
                    id="holdQtyUpdate"
                    isOpen={holdQtyUpdateModalOpen}
                    onRequestClose={holdQtyUpdateModalClose}
                    contentLabel="HOLD 수정(수량)"
                    width={800}
                    height={300}
                    contents={<HoldQtyUpdateModal />}
                />

                <SModal
                    formId={"inout_barcode_add_p"}
                    id="addBarcode"
                    isOpen={barcodeModalOpen}
                    onRequestClose={barcodeModalClose}
                    contentLabel="바코드 추가"
                    width={1220}
                    height={530}
                    contents={<BarcodeAddModal />}
                />

                

            </React.Fragment>

        );
    }

}

export default QualityStatus;