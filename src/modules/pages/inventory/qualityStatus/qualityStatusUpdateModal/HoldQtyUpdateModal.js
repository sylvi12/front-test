import React, { Component } from 'react';
import { inject } from 'mobx-react';

import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import HoldInsertModalMiddleItem from "modules/pages/inventory/qualityStatus/qualityStatusModal/HoldInsertModalMiddleItem";
import HoldInsertModalContents from "modules/pages/inventory/qualityStatus/qualityStatusModal/HoldInsertModalContents";

import SButton from 'components/atoms/button/SButton';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';

import STimePicker from 'components/override/datepicker/STimePicker';
import EmployeeHelper from 'components/codehelper/EmployeeHelper';
import STextArea from 'components/atoms/input/STextArea';

import QualityStatusStore from 'modules/pages/inventory/qualityStatus/service/QualityStatusStore';
import SBuSelectBox from 'components/override/business/SBuSelectBox';
import ItemHelper from 'components/codehelper/ItemHelper';
import SNumericInput from 'components/override/numericinput/SNumericInput';

@inject(stores => ({
     
    
     buIdModal: stores.qualityStatusStore.buIdModal
    
    , modalFindDtm: stores.qualityStatusStore.modalFindDtm
    , time: stores.qualityStatusStore.time
    , modalHoldReasonId: stores.qualityStatusStore.modalHoldReasonId
    , holdContents: stores.qualityStatusStore.holdContents
    , handleChange: stores.qualityStatusStore.handleChange
    
    , selectedItemId: stores.qualityStatusStore.selectedItemId
    , handleHelperResult: stores.qualityStatusStore.handleHelperResult
    , handleHelperOpen: stores.qualityStatusStore.handleHelperOpen
    , itemHelperOption: stores.qualityStatusStore.itemHelperOption
    , buSelectionChanged: stores.qualityStatusStore.buSelectionChanged
    , modalReasonList: stores.qualityStatusStore.modalReasonList
    , dateSelectionChange: stores.qualityStatusStore.dateSelectionChange
    , selectedItem_Qty: stores.qualityStatusStore.selectedItem_Qty
    , itemHelperResult_Qty: stores.qualityStatusStore.itemHelperResult_Qty
    , itemHelperOption_Qty: stores.qualityStatusStore.itemHelperOption_Qty
    , handleHelperOpen_Qty: stores.qualityStatusStore.handleHelperOpen_Qty
    , prodQty_q: stores.qualityStatusStore.prodQty_q
    
    , holdQtyUpdate: stores.qualityStatusStore.holdQtyUpdate
}))

class HoldQtyUpdateModal extends Component {

    constructor(props) {
        super(props);
        
    }

    render(){

        const {   
                
                
                 buIdModal
                
                , modalFindDtm
                , time
                , modalHoldReasonId
                , modalReasonList
                , dateSelectionChange
                , holdContents
                , handleChange
                , selectedItemId
                , handleHelperResult
                , handleHelperOpen
                , itemHelperOption
                

                , selectedItem_Qty
                , itemHelperResult_Qty
                , itemHelperOption_Qty
                , handleHelperOpen_Qty
                , prodQty_q
                
                , holdQtyUpdate
            
            } = this.props;

        return(
            <React.Fragment>
                
                
                
                
                
                    <div className='btn_wrap'>
                        <SButton buttonName={"저장"} type={"btnHoldQtyUpdate"} onClick={holdQtyUpdate} />
                        
                    </div>
                    <div>
                    <table style={{ width: "100%"  }}>
                        <tbody>
                            <tr>
                                <th>공장</th>
                                <td>

                                <SBuSelectBox
                                    
                                    id={"buId"}
                                    value={buIdModal}
                                    onChange={handleChange}
                                    disabled={true}
                                    //onSelectionChange={buSelectionChanged}
                                />
                                    
                                </td>
                                
                                <th>발견일시</th>
                                <td>
                                    <SDatePicker
                                        id="modalFindDtm"
                                        value={modalFindDtm}
                                        onChange={handleChange}
                                    />
                                    <STimePicker 
                                        id="time"
                                        value={time}
                                        onChange={handleChange}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <th>제품</th>
                                <td>
                                    <ItemHelper
                                        selectedItemId={"selectedItem_Qty"}
                                        defaultValue={selectedItem_Qty}
                                        onChange={handleChange}
                                        onHelperResult={itemHelperResult_Qty}
                                        // onOpen={itemHelperOpen}
                                        helperOption={itemHelperOption_Qty}
                                        onOpen={handleHelperOpen_Qty}
                                        cdName={'itemId'}
                                        cdNmName={'itemNm'}
                                        disabled={true}
                                    />

                                </td>

                                <th>수량</th>
                                <td>
                                    <SNumericInput
                                        id={"prodQty_q"}
                                        value={prodQty_q}
                                        onChange={handleChange}
                                    />
                                    {"KG"}
                                </td>
                                
                                
                            </tr>
                            <tr>
                                <th>발견자</th>
                                <td>
                                    <EmployeeHelper 
                                        selectedItemId={"selectedItemId"}
                                        defaultValue={selectedItemId}
                                        onChange={handleChange}
                                        onHelperResult={handleHelperResult}
                                        onOpen={handleHelperOpen}
                                        helperOption={itemHelperOption}
                                        cdName={'empNo'}
                                        cdNmName={'loginNm'}
                                    />
                                </td>
                                <th>HOLD사유</th>
                                <td>
                                    <SSelectBox
                                        id={"modalHoldReasonId"}
                                        value={modalHoldReasonId}
                                        optionGroup={modalReasonList}
                                        valueMember={"holdReasonId"}
                                        displayMember={"holdNm"}
                                        onChange={handleChange}
                                        setList={modalReasonList}
                                        onSelectionChange={dateSelectionChange}
                                        contentWidth={250}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <th>내용</th>
                                <td colSpan="3">
                                    <STextArea
                                        id="holdContents"
                                        value={holdContents}
                                        onChange={handleChange}
                                        contentWidth={700}
                                    />
                                </td>
                                
                            </tr>
                        </tbody>
                    </table>
                    </div>
            </React.Fragment>

        );
    }
}

export default HoldQtyUpdateModal;