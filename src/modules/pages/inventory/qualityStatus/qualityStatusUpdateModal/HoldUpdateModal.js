import React, { Component } from 'react';
import { inject } from 'mobx-react';

import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import HoldInsertModalMiddleItem from "modules/pages/inventory/qualityStatus/qualityStatusModal/HoldInsertModalMiddleItem";
import HoldInsertModalContents from "modules/pages/inventory/qualityStatus/qualityStatusModal/HoldInsertModalContents";

import SButton from 'components/atoms/button/SButton';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';

import STimePicker from 'components/override/datepicker/STimePicker';
import EmployeeHelper from 'components/codehelper/EmployeeHelper';
import STextArea from 'components/atoms/input/STextArea';

import QualityStatusStore from 'modules/pages/inventory/qualityStatus/service/QualityStatusStore';
import SBuSelectBox from 'components/override/business/SBuSelectBox';

import CommonStore from 'modules/pages/common/service/CommonStore';

@inject(stores => ({
    holdUpdate: stores.qualityStatusStore.holdUpdate
    // , holdClose: stores.qualityStatusStore.holdClose
    // , buId: stores.qualityStatusStore.buId
    , buIdModal: stores.qualityStatusStore.buIdModal
    , buList: stores.qualityStatusStore.buList
    , modalFindDtm: stores.qualityStatusStore.modalFindDtm
    , time: stores.qualityStatusStore.time
    , modalHoldReasonId: stores.qualityStatusStore.modalHoldReasonId
    , holdContents: stores.qualityStatusStore.holdContents
    , handleChange: stores.qualityStatusStore.handleChange
    , findUserClick: stores.qualityStatusStore.findUserClick
    , selectedItemId: stores.qualityStatusStore.selectedItemId
    , handleHelperResult: stores.qualityStatusStore.handleHelperResult
    , handleHelperOpen: stores.qualityStatusStore.handleHelperOpen
    , itemHelperOption: stores.qualityStatusStore.itemHelperOption
    , buSelectionChanged: stores.qualityStatusStore.buSelectionChanged
    , modalReasonList: stores.qualityStatusStore.modalReasonList
    , dateSelectionChange: stores.qualityStatusStore.dateSelectionChange
}))

class HoldUpdateModal extends Component {

    constructor(props) {
        super(props);
        
        // QualityStatusStore.getBuModal();
        // QualityStatusStore.getReasonModal();
    }

    

    render(){

        const {   holdUpdate
                
                , buIdModal
                , buList
                , modalFindDtm
                , time
                , modalHoldReasonId
                , modalReasonList
                , dateSelectionChange
                , holdContents
                , handleChange
                , selectedItemId
                , handleHelperResult
                , handleHelperOpen
                , itemHelperOption
                 } = this.props;

        return(
            <React.Fragment>
                {/* <SearchTemplate searchItem={<HoldInsertModalInputItem />} /> */}
                
                
                
                
                    <div className='btn_wrap'>
                        <SButton buttonName={"저장"} type={"btnHoldUpdate"} onClick={holdUpdate} />
                        
                    </div>

                    <table style={{ width: "100%"  }}>
                        <tbody>
                            <tr>
                                <th>공장</th>
                                <td>

                                <SBuSelectBox
                                    
                                    id={"buIdModal"}
                                    value={buIdModal}
                                    onChange={handleChange}
                                    disabled={true}
                                    //onSelectionChange={buSelectionChanged}
                                />
                                    {/* <SSelectBox
                                        id={"buId"}
                                        value={buId}
                                        onChange={handleChange}
                                        optionGroup={buList}
                                        valueMember={"buId"}
                                        displayMember={"buNm"}
                                        onSelectionChange={buSelectionChanged}
                                        disabled={true}
                                        
                                    /> */}
                                </td>
                                
                                <th>발견일시</th>
                                <td>
                                    <SDatePicker
                                        id="modalFindDtm"
                                        value={modalFindDtm}
                                        onChange={handleChange}
                                    />
                                    <STimePicker 
                                        id="time"
                                        value={time}
                                        onChange={handleChange}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <th>발견자</th>
                                <td>
                                    <EmployeeHelper 
                                        selectedItemId={"selectedItemId"}
                                        defaultValue={selectedItemId}
                                        onChange={handleChange}
                                        onHelperResult={handleHelperResult}
                                        onOpen={handleHelperOpen}
                                        helperOption={itemHelperOption}
                                        cdName={'empNo'}
                                        cdNmName={'loginNm'}
                                    />
                                </td>
                                <th>HOLD사유</th>
                                <td>
                                    <SSelectBox
                                        id={"modalHoldReasonId"}
                                        value={modalHoldReasonId}
                                        optionGroup={modalReasonList}
                                        valueMember={"holdReasonId"}
                                        displayMember={"holdNm"}
                                        onChange={handleChange}
                                        setList={modalReasonList}
                                        onSelectionChange={dateSelectionChange}
                                        contentWidth={250}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <th>내용</th>
                                <td colSpan="3">
                                    <STextArea
                                        id="holdContents"
                                        value={holdContents}
                                        onChange={handleChange}
                                        contentWidth={700}
                                    />
                                </td>
                                
                            </tr>
                        </tbody>
                    </table>
            </React.Fragment>

        );
    }
}

export default HoldUpdateModal;