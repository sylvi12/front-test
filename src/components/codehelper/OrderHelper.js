import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { observable } from 'mobx'
import moment from 'moment';
import 'moment/locale/ko';

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import SNumericInput from 'components/override/numericinput/SNumericInput';
import ItemHelper from 'components/codehelper/ItemHelper';
import SInput from 'components/atoms/input/SInput'
import SButton from 'components/atoms/button/SButton'
import SGrid from 'components/override/grid/SGrid'
import GridUtil from 'components/override/grid/utils/GridUtil';
import SModal from 'components/override/modal/SModal'
import SDatePicker from 'components/override/datepicker/SDatePicker'
import Alert from 'components/override/alert/Alert';

import ItemOptionModel from 'components/codehelper/model/ItemOptionModel';

import BarcodeHistoryRepository from 'modules/pages/inventory/barcodeHistory/service/BarcodeHistoryRepository'
import { isThisISOWeek } from 'date-fns';


// 생산정보 Modal
class OrderModalHelper extends Component {

	@observable orderHelperModalIsOpen = false;

	constructor(props) {
		super(props);
		this.state = {
			orderHelperModalIsOpen: false

		}
	}


	// Onload 정보
	static getDerivedStateFromProps = (nextProps, prevState) => {
		if (nextProps.orderHelperModalIsOpen != prevState.orderHelperModalIsOpen) {
			return { orderHelperModalIsOpen: nextProps.orderHelperModalIsOpen }
		}
		return null;
	}

	// orderHelperModalIsOpen ture or false 처리
	handleClose = (result) => {
		const { id, onChange } = this.props;
		if (onChange) {
			onChange({ id: id, value: false })
		}
	}

	render() {
		return (
			<SModal
				formId={this.props.formId}
				isOpen={this.state.orderHelperModalIsOpen}
				onRequestClose={this.handleClose}
				contentLabel={this.props.title}
				// width={"80%"}
				// height={"80%"}
				// minWidth={800}
				// minHeight={500}
				width={1100}
				height={530}
				contents={<OrderHelper {...this.props} />}
			/>
		)
	}
}
export default OrderModalHelper;

@observer
class OrderHelper extends Component {

	//ORDER INFO와 LIST를 불러오기 위한 변수
	@observable orderNo = 0;
	@observable orderTypeNm = '';
	@observable lineId = 0;
	@observable lineNm = '';
	@observable itemCd = '';
	@observable itemNm = '';
	@observable orderQty = 0;
	@observable prodQty = 0;
	@observable reportQty = 0;
	@observable progress = '';
	@observable planStartDtm = moment().format('YYYYMMDD');
	@observable planEndDtm = moment().format('YYYYMMDD');
	@observable prodStartDtm = moment().format('YYYYMMDD');
	@observable prodEndDtm = moment().format('YYYYMMDD');

	@observable orderDetailDataList = [];
	@observable orderDetailGridApi = undefined;
	@observable pinnedBottomRowData = [];

	@observable orderDetailSubDataList = [];
	@observable orderDetailSubGridApi = undefined;

	@observable prodOrderNo = 0;
	@observable prodQtyModalBarcode = '';
	@observable prodQtyModalProdQtyUom = '';
	@observable prodQtyModalProdQty = 0;
	@observable prodQtyModalIsOpen = false;
	@observable prodQtySaveData = {};

	constructor(props) {
		super(props);
	}

	handleChange = (data) => {
		this[data.id] = data.value;
	}

	rowClassRules = {
		'useYn_Y': function (params) {
			return params.data.useYn == 'Y'
		},
		'useYn_N': function (params) {
			return params.data.useYn == 'N'
		}
	}


	//생산정보 Modal에서 Event 발생시 재조회 function
	generateOrderInfo = async () => {
		console.log('generate barcodeYn', this.props.barcodeYn)
		console.log('generate orderNo', this.props.orderNo)

		const submitData = {
			"orderNo": Number(this.props.orderNo)
		}
		if (this.props.orderNo != null) {
			const { data, status } = await BarcodeHistoryRepository.getOrderInfo2(submitData);
			if (status === 200 && data.data != null) {
				const successData = data.data

				this.orderNo = successData.orderNo
				this.orderTypeNm = successData.orderTypeNm == null ? '' : successData.orderTypeNm
				this.lineId = successData.lineId
				this.lineNm = successData.lineNm
				this.itemCd = successData.itemCd
				this.itemNm = successData.itemNm
				this.orderQty = successData.orderQty == null ? 0 : successData.orderQty
				this.prodQty = successData.prodQty == null ? 0 : successData.prodQty
				this.reportQty = successData.reportQty == null ? 0 : successData.reportQty
				this.progress = (successData.progress == null ? 0 : successData.progress).toFixed(6) + '%'
				this.planStartDtm = successData.planStartDtm
				this.planEndDtm = successData.planEndDtm
				this.prodStartDtm = successData.prodStartDtm
				this.prodEndDtm = successData.prodEndDtm

				if (this.props.barcodeYn == 'Y') {
					const { data, status } = await BarcodeHistoryRepository.getOrderList(submitData);
					if (status === 200) {
						this.orderDetailDataList = data.data;
						this.orderDetailGridApi.setRowData(this.orderDetailDataList);
					}
				} else if (this.props.barcodeYn == 'N') {
					const { data, status } = await BarcodeHistoryRepository.getSubOrderList(submitData);
					if (status === 200) {
						this.orderDetailSubDataList = data.data;
						this.orderDetailSubGridApi.setRowData(this.orderDetailSubDataList);
					}
				}
			} else {
				Alert.meg('생산정보가 없습니다.')
				const { id, onChange } = this.props;
				if (onChange) {
					onChange({ id: id, value: false })
				}
			}
		}
	}

	//ORDER INFO
	async componentDidMount() {
		console.log('barcodeYn value=', this.props.barcodeYn)
		const submitData = {
			"orderNo": Number(this.props.orderNo)
		}
		if (this.props.orderNo != null) {
			const { data, status } = await BarcodeHistoryRepository.getOrderInfo2(submitData);
			if (status === 200 && data.data != null) {
				const successData = data.data
				console.log('successData', successData);
				this.orderNo = successData.orderNo
				this.orderTypeNm = successData.orderTypeNm == null ? '' : successData.orderTypeNm
				this.lineId = successData.lineId
				this.lineNm = successData.lineNm
				this.itemCd = successData.itemCd
				this.itemNm = successData.itemNm
				// this.selectedItem = {"itemCd": successData.itemCd, "itemNm": successData.itemNm };
				this.orderQty = successData.orderQty == null ? 0 : successData.orderQty
				this.prodQty = successData.prodQty == null ? 0 : successData.prodQty
				this.reportQty = successData.reportQty == null ? 0 : successData.reportQty
				this.progress = (successData.progress == null ? 0 : successData.progress).toFixed(6) + '%'
				//this.progress = ((successData.prodQty / successData.orderQty) * 100).toFixed(2) + '%'
				this.planStartDtm = successData.planStartDtm
				this.planEndDtm = successData.planEndDtm
				this.prodStartDtm = successData.prodStartDtm
				this.prodEndDtm = successData.prodEndDtm
				this.orderDetailModelClick();
			} else {
				Alert.meg('생산정보가 없습니다.')
				const { id, onChange } = this.props;
				if (onChange) {
					onChange({ id: id, value: false })
				}
			}
		}
	}

	//자동조회 Event
	orderDetailModelClick = async () => {
		const submitData = {
			"orderNo": Number(this.props.orderNo)
		}

		console.log('자동조회 진입Yn값 확인', this.props.barcodeYn)
		if (this.props.barcodeYn == 'Y') {
			const { data, status } = await BarcodeHistoryRepository.getOrderList(submitData);
			if (status === 200) {
				this.orderDetailDataList = data.data;
				this.orderDetailGridApi.setRowData(this.orderDetailDataList);
			}
		} else if (this.props.barcodeYn == 'N') {
			const { data, status } = await BarcodeHistoryRepository.getSubOrderList(submitData);
			if (status === 200) {
				this.orderDetailSubDataList = data.data;
				this.orderDetailSubGridApi.setRowData(this.orderDetailSubDataList);
			}
		}
	}

	//바코드사용 Btn
	setBarcodeYnUse = async () => {
		let selectedRows = this.orderDetailGridApi.getSelectedRows();
		if (!selectedRows || selectedRows.length == 0) {
			Alert.meg('생산정보를 선택 해주세요.');
			return;
		}

		let useStopCnt = 0;
		for (let i = 0; i < selectedRows.length; i++) {
			if (selectedRows[i].useYn == 'Y') {
				useStopCnt++;
			}
		}
		if (useStopCnt > 0) {
			Alert.meg(useStopCnt + ' 개의 처리불가 품목이 있습니다.');
			return;
		} else {
			const value = await Alert.confirm("사용처리 하시겠습니까?").then(result => { return result; });
			if (value) {
				const { data, status } = await BarcodeHistoryRepository.setBarcodeYnUse(selectedRows);
				if (data.success == false) {
					Alert.meg(data.msg);
				} else {
					Alert.meg('성공하였습니다.')
					this.generateOrderInfo();
					//this.orderDetailModelClick();
				}
			}
		}
	}

	//바코드중지 Btn
	setBarcodeYnCancel = async () => {
		let selectedRows = this.orderDetailGridApi.getSelectedRows();
		console.log('selectedRows', selectedRows)
		if (!selectedRows || selectedRows.length == 0) {
			Alert.meg('생산정보를 선택 해주세요.');
			return;
		}

		let useStopCnt = 0;
		for (let i = 0; i < selectedRows.length; i++) {
			if (selectedRows[i].useYn == 'N' || selectedRows[i].prodQty != null || selectedRows[i].inputDtm != null) {
				useStopCnt++;
			}
		}
		if (useStopCnt > 0) {
			Alert.meg(useStopCnt + ' 개의 처리불가 품목이 있습니다.');
			return;
		} else {
			const value = await Alert.confirm("사용중지 하시겠습니까?").then(result => { return result; });
			if(value) {
			const { data, status } = await BarcodeHistoryRepository.setBarcodeYnCancel(selectedRows);
			if (data.success == false) {
				Alert.meg(data.msg);
			} else {
				Alert.meg('성공하였습니다.');
				this.generateOrderInfo();
				//this.orderDetailModelClick();
				}
			}
		}
	}

	// 생산등록 Btn
	prodInsert = async () => {
		let useYnFlag = 0; //사용여부 flag
		let prodFlag = 0; // 생산등록여부 flag
		let submitSelectedRows = []; // 적용대상 Row
		const selectedRows = this.orderDetailGridApi.getSelectedRows();

		if (selectedRows.length == 0) {
			Alert.meg('생산정보를 선택해 주세요.');
			return;
		} else {
			for (let i = 0; i < selectedRows.length; i++) {
				if (selectedRows[i].useYn == 'N') {
					useYnFlag++;
				} else if (selectedRows[i].prodQty != null || selectedRows[i].invenQty != null || selectedRows[i].inputDtm != null) {
					prodFlag++;
				} else {
					// 생산등록 대상 바코드
					selectedRows[i].scanYn = 'N';
					submitSelectedRows.push(selectedRows[i]);
				}
			}

			if (useYnFlag != 0) {
				Alert.meg('사용중지된 품목이 있습니다.');
				return;
			} else if (prodFlag != 0) {
				Alert.meg('이미 생산등록된 품목이 있습니다.');
				return;
			} else if (submitSelectedRows.length > 0 && useYnFlag == 0 && prodFlag == 0) {
				//OutputStore 의 prodInsert async ('생산등록' 사용)
				const value = await Alert.confirm("생산등록 하시겠습니까?").then(result => { return result; });
				if (value) {
					const { data, status } = await BarcodeHistoryRepository.createLedgerAutoLabelerByBarcode(submitSelectedRows);
					if (data.success == false) {
						Alert.meg(data.msg);
					} else {
						Alert.meg("입고되었습니다.");
					}
					//this.orderDetailModelClick();
					this.generateOrderInfo();
				} else {
					return;
				}
			}
		}
	}

	// 선입고 등록 Btn
	preProdInsert = async () => {
		let useYnFlag = 0; //사용여부 flag
		let prodFlag = 0; // 생산등록여부 flag
		let submitSelectedRows = []; // 적용대상 Row
		const selectedRows = this.orderDetailGridApi.getSelectedRows();

		if (selectedRows.length == 0) {
			Alert.meg('생산정보를 선택해 주세요.');
			return;
		} else {
			for (let i = 0; i < selectedRows.length; i++) {
				if (selectedRows[i].useYn == 'N') {
					useYnFlag++;
				} else if (selectedRows[i].prodQty != null || selectedRows[i].invenQty != null || selectedRows[i].inputDtm != null) {
					prodFlag++;
				} else {
					// 생산등록 대상 바코드
					selectedRows[i].scanYn = 'N';
					submitSelectedRows.push(selectedRows[i]);
				}
			}

			if (useYnFlag != 0) {
				Alert.meg('사용중지된 품목이 있습니다.');
				return;
			} else if (prodFlag != 0) {
				Alert.meg('이미 생산등록된 품목이 있습니다.');
				return;
			} else if (submitSelectedRows.length > 0 && useYnFlag == 0 && prodFlag == 0) {
				const value = await Alert.confirm("선입고 하시겠습니까?").then(result => { return result; });
				if (value) {
					const {data, status } = await BarcodeHistoryRepository.preProdinsertList(submitSelectedRows);
					
					if(status == 200)
					{
						if (data.success == false) {
							Alert.meg(data.msg);
						} else {
							Alert.meg("선입고되었습니다.");
						}
						this.generateOrderInfo();
					}
				} else {
					return;
				}
			}
		}
	}

	// OutputModal MiddleItem 생산취소 클릭 시
	invenCancel = async () => {
		let useYnFlag = 0; //사용여부 flag
		let prodFlag = 0; // 생산등록여부 flag
		let submitSelectedRows = []; // 적용대상 Row
		const selectedRows = this.orderDetailGridApi.getSelectedRows();
		if (selectedRows.length == 0) {
			Alert.meg("생산정보를 선택해 주세요.");
			return;
		} else {
			for (let i = 0; i < selectedRows.length; i++) {
				if (selectedRows[i].useYn == 'N') {
					useYnFlag++;
				} else if (selectedRows[i].prodQty == null || selectedRows[i].invenQty == null || selectedRows[i].inputDtm == null) {
					prodFlag++;
				} else {
					// 생산취소 대상 바코드
					submitSelectedRows.push(selectedRows[i]);
				}
			}

			if (useYnFlag != 0) {
				Alert.meg('사용중지된 품목이 있습니다.');
				return;
			} else if (prodFlag != 0) {
				Alert.meg('생산등록되지 않은 품목이 있습니다.');
				return;
			} else if (submitSelectedRows.length > 0 && useYnFlag == 0 && prodFlag == 0) {
				//OutputStore 의 prodInsert async ('생산등록' 사용)
				const value = await Alert.confirm("생산취소 하시겠습니까?").then(result => { return result; });
				if (value) {
					const { data, status } = await BarcodeHistoryRepository.deleteOutputModal(submitSelectedRows);
					// const { data, status } = await OutputRepository.deleteOutputModal(params);
					if (data.success == false) {
						Alert.meg(data.msg);
					} else {
						Alert.meg("생산 취소되었습니다.");
					}
					//this.orderDetailModelClick();
					this.generateOrderInfo();
				} else {
					return;
				}
			}
		}
	}

	/* ProqQty Modal */
	// 생산수량 변경 modal 열기
	openProdQtyModal = async () => {
		this.prodQtySaveData = {};
		const selectedRow = GridUtil.getSelectedRowData(this.orderDetailGridApi);
		if (!selectedRow) {
			Alert.meg("생산정보를 선택해 주세요.");
			return;
		}
		if (selectedRow.invenQty == undefined || selectedRow.inputDtm == null) {
			Alert.meg("생산등록되지 않은 품목입니다.");
			return;
		}
		console.log(selectedRow);
		this.prodQtyModalBarcode = selectedRow.barcode;
		this.prodQtyModalProdQtyUom = selectedRow.prodUom;
		this.prodQtyModalProdQty = selectedRow.prodQty;
		this.prodOrderNo = selectedRow.orderNo;
		this.prodQtyModalIsOpen = true;
	}

	// 생산수량 변경 modal 닫기
	closeProdQtyModal = async () => {
		this.prodQtyModalIsOpen = false;
	}

	// ProdQtyModal MiddleItem 저장 클릭시
	prodQtyModalSave = async () => {

		this.prodQtySaveData = {
			"orderNo": this.prodOrderNo
			, "barcode": this.prodQtyModalBarcode
            , "processQty": this.prodQtyModalProdQty
            , "processUom": this.prodQtyModalProdQtyUom
        }

		const { data, status } = await BarcodeHistoryRepository.prodQtyModalSave(this.prodQtySaveData);
		if (data.success == false) {
			Alert.meg(data.msg);
		} else {
			Alert.meg("저장되었습니다.");
			this.closeProdQtyModal();
			this.generateOrderInfo();
			//this.orderDetailModelClick();
		}
	}


	setOrderDetailGridApi = async (gridApi) => {
		this.orderDetailGridApi = gridApi;
		this.pinnedBottomRowData = [
			{
				packSeq: '합계',
				prodQty: 0,
				invenQty: 0,
				availableQty: 0,
				holdQty: 0

			}
		]
		const columnDefs = [
			{
				headerName: "", field: ""
				, width: 30
				, checkboxSelection: true
				, headerCheckboxSelection: true
				, readOnly: false
				// , editable: function (params) { }
			},
			{ headerName: "순번", field: "packSeq", width: 45, cellClass: 'cell_align_center' },
			{ headerName: "바코드", field: "barcode", width: 100, cellClass: 'cell_align_center' },
			{ headerName: "LOT", field: "lotNo", width: 80, cellClass: 'cell_align_center' },
			{
				headerName: "생산일자", field: "prodDt", width: 90, cellClass: 'cell_align_center'
				, valueFormatter: GridUtil.dateFormatter
			},
			{
				headerName: "유통기한", field: "expiryDt", width: 90, cellClass: 'cell_align_center'
				, valueFormatter: GridUtil.dateFormatter
			},
			{ headerName: "사용여부", field: "useYn", width: 55, cellClass: 'cell_align_center' },
			{ headerName: "단위", field: "prodUom", width: 50, cellClass: 'cell_align_center' },
			{
				headerName: "생산수량", field: "prodQty", width: 70, cellClass: 'cell_align_right'
				, valueFormatter: (params) => GridUtil.numberFormatter(params)
				, pinnedRowCellRenderer: "customPinnedRowRenderer"
				, pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
			},
			{
				headerName: "재고수량", field: "invenQty", width: 70, cellClass: 'cell_align_right'
				, valueFormatter: (params) => GridUtil.numberFormatter(params)
				, pinnedRowCellRenderer: "customPinnedRowRenderer"
				, pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
			},
			{
				headerName: "가용수량", field: "availableQty", width: 70, cellClass: 'cell_align_right'
				, valueFormatter: (params) => GridUtil.numberFormatter(params)
				, pinnedRowCellRenderer: "customPinnedRowRenderer"
				, pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
			},
			{
				headerName: "홀드수량", field: "holdQty", width: 70, cellClass: 'cell_align_right'
				, valueFormatter: (params) => GridUtil.numberFormatter(params)
				, pinnedRowCellRenderer: "customPinnedRowRenderer"
				, pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
			},
			{
				headerName: "품질상태", field: "lotQuality", width: 70, cellClass: 'cell_align_center'
				, valueFormatter: (params) => GridUtil.getCodeNm(params, 'MFD003')
			},
			{ headerName: "위치", field: "locNm", width: 80, cellClass: 'cell_align_center' },
			{
				headerName: "처리일시", field: "inputDtm", width: 120, cellClass: 'cell_align_center'
				, valueFormatter: (param) => GridUtil.dateFormatter(param, 'dtm')
			},
			{ headerName: "처리자", field: "inputEmpNm", width: 90, cellClass: 'cell_align_center' },
			{ headerName: "선입고", field: "alreadyInvenYn", width: 90, cellClass: 'cell_align_center', hide: true }
		];
		this.orderDetailGridApi.setColumnDefs(columnDefs);
	}

	setOrderDetailSubGridApi = async (gridApi) => {
		this.orderDetailSubGridApi = gridApi;
		this.pinnedBottomRowData = [
			{
				prodDt: '합계',
				prodQty: 0
			}
		]
		const columnDefs = [
			{
				headerName: "생산일자", field: "prodDt", width: 100, cellClass: 'cell_align_center'
				, valueFormatter: GridUtil.dateFormatter
			},
			{ headerName: "단위", field: "prodUom", width: 60, cellClass: 'cell_align_center' },
			{
				headerName: "생산량", field: "prodQty", width: 70, cellClass: 'cell_align_right'
				, valueFormatter: (params) => GridUtil.numberFormatter(params)
				, pinnedRowCellRenderer: "customPinnedRowRenderer"
				, pinnedRowCellRendererParams: { style: { "fontWeight": "bold" } }
			}
		];
		this.orderDetailSubGridApi.setColumnDefs(columnDefs);
	}


	render() {
		return (
			<React.Fragment>
				<div className="nonStyle">
					<SearchTemplate
						searchItem={
							<SearchItem
								orderDetailModelClick={this.orderDetailModelClick}
								orderNo={this.orderNo}
								orderTypeNm={this.orderTypeNm}
								lineId={this.lineId}
								lineNm={this.lineNm}
								itemCd={this.itemCd}
								itemNm={this.itemNm}
								orderQty={this.orderQty}
								prodQty={this.prodQty}
								reportQty={this.reportQty}
								progress={this.progress}
								planStartDtm={this.planStartDtm}
								planEndDtm={this.planEndDtm}
								prodStartDtm={this.prodStartDtm}
								prodEndDtm={this.prodEndDtm}
								// itemHelperOption = {this.itemHelperOption}
								// selectedItem = {this.selectedItem}
								handleChange={this.handleChange}
							/>} />
				</div>

				{this.props.barcodeYn == 'Y' ?
					(
						<ContentsMiddleTemplate
							contentSubTitle={'생산 내역'}

							middleItem={
								<MiddleItem
									setBarcodeYnUse={this.setBarcodeYnUse}
									setBarcodeYnCancel={this.setBarcodeYnCancel}
									prodInsert={this.prodInsert}
									preProdInsert={this.preProdInsert}
									invenCancel={this.invenCancel}
									openProdQtyModal={this.openProdQtyModal}
								/>} />
					) : (
						<ContentsMiddleTemplate
							contentSubTitle={'생산 내역'}
							middleItem={
								<MiddleItemSub />
							} />
					)
				}

				{this.props.barcodeYn == 'Y' ?
					(<ContentsTemplate
						id='orderHelper'
						contentItem={
							<ContentItem
								setOrderDetailGridApi={this.setOrderDetailGridApi}
								orderDetailDataList={this.orderDetailDataList}
								pinnedBottomRowData={this.pinnedBottomRowData}
								rowClassRules={this.rowClassRules}
							/>} />

					) : (
						<ContentsTemplate
							id='orderHelper'
							contentItem={
								<ContentSubItem
									setOrderDetailSubGridApi={this.setOrderDetailSubGridApi}
									orderDetailSubDataList={this.orderDetailSubDataList}
									pinnedBottomRowData={this.pinnedBottomRowData}
								/>} />
					)
				}

				<SModal
					id={"order_prod_qty_p"}
					formId={"order_prod_qty_p"}
					isOpen={this.prodQtyModalIsOpen}
					onRequestClose={this.closeProdQtyModal}
					contentLabel="생산수량 입력"
					width={300}
					height={120}
					contents={
						<ProdQtyModal
							prodQtyModalProdQty={this.prodQtyModalProdQty}
							handleChange={this.handleChange}
							prodQtyModalProdQtyUom={this.prodQtyModalProdQtyUom}
							prodQtyModalSave={this.prodQtyModalSave}
						/>}
				/>
			</React.Fragment>
		);
	}
}

//SearchItem
const SearchItem = ({
	orderDetailModelClick
	, orderNo
	, orderTypeNm
	, lineId
	, lineNm
	, itemCd
	, itemNm
	, orderQty
	, prodQty
	, reportQty
	, progress
	, planStartDtm
	, planEndDtm
	, prodStartDtm
	, prodEndDtm
	// ,itemHelperOption+
	// ,selectedItem
	, handleChange
}) => {
	return (
		<React.Fragment>
			<div className='search_item_btn'>
				<SButton
					buttonName={'조회'}
					type={'btnSearch'}
					onClick={orderDetailModelClick}
				/>
			</div>
			<table style={{ width: "100%", padding: "0px !important" }}>
				<tbody>
					<tr>
						<td className="th no_padding">생산라인</td>
						<td>
							<SInput
								id={"lineNm"}
								value={lineNm}
								readOnly={true}
								contentWidth={80}
							/>
						</td>
						<td className="th no_padding">지시타입</td>
						<td>
							<SInput
								id={"orderTypeNm"}
								value={orderTypeNm}
								readOnly={true}
								contentWidth={80}
							/>
						</td>
						<td className="th no_padding">지시번호</td>
						<td>
							<SInput
								id={"orderNo"}
								value={orderNo}
								readOnly={true}
								contentWidth={80}
							/>
						</td>
						<td className="th no_padding">제품코드/명</td>
						<td>
							{/* <ItemHelper
                                selectedItemId={"selectedItem"}
                                helperOption={itemHelperOption}
                                onChange={handleChange}
                                defaultValue={selectedItem}
                                cdName={'itemCd'}
                                cdNmName={'itemNm'}
							/> */}
							<SInput
								id={"itemCd"}
								value={itemCd}
								readOnly={true}
								contentWidth={70}
							/>
							<SInput
								id={"itemNm"}
								value={itemNm}
								readOnly={true}
								contentWidth={100}
							/>
						</td>
						<td className="th no_padding">지시일자</td>
						<td colSpan="3">
							<SDatePicker
								id={"planStartDtm"}
								value={planStartDtm}
								onChange={handleChange}
								readOnly={true}
								contentWidth={80}
							/>
							<SDatePicker
								title={"~"}
								id={"planEndDtm"}
								value={planEndDtm}
								onChange={handleChange}
								readOnly={true}
								contentWidth={80}
							/>
						</td>
					</tr>
					<tr>
						<td className="th no_padding">지시량</td>
						<td>
							<SNumericInput
								id={"orderQty"}
								value={orderQty}
								readOnly={true}
								contentWidth={80}
							/>
						</td>
						<td className="th no_padding">생산량</td>
						<td>
							<SNumericInput
								id={"prodQty"}
								value={prodQty}
								readOnly={true}
								contentWidth={80}
							/>
						</td>
						<td className="th no_padding">실적량</td>
						<td>
							<SNumericInput
								id={"reportQty"}
								value={reportQty}
								readOnly={true}
								contentWidth={80}
							/>
						</td>
						<td className="th no_padding">진척율</td>
						<td>
							<SInput
								id={"progress"}
								value={progress}
								readOnly={true}
							/>
						</td>
						<td className="th no_padding">생산일자</td>
						<td colSpan="3">
							<SDatePicker
								id={"prodStartDtm"}
								value={prodStartDtm}
								onChange={handleChange}
								readOnly={true}
								contentWidth={80}
							/>
							<SDatePicker
								title={"~"}
								id={"prodEndDtm"}
								value={prodEndDtm}
								onChange={handleChange}
								readOnly={true}
								contentWidth={80}
							/>
						</td>
					</tr>
				</tbody>
			</table>
		</React.Fragment>
	)
}

const MiddleItem = ({
	setBarcodeYnUse
	, setBarcodeYnCancel
	, prodInsert
	, preProdInsert
	, invenCancel
	, openProdQtyModal
}) => {
	return (
		<React.Fragment>
			<SButton
				className="btn_red"
				buttonName={'바코드사용'}
				type={'btnBarcodeUse'}
				onClick={setBarcodeYnUse}
			/>
			<SButton
				className="btn_red"
				buttonName={'바코드중지'}
				type={'btnBarcodeCancel'}
				onClick={setBarcodeYnCancel}
			/>
			<SButton
				className="btn_red"
				buttonName={'선입고등록'}
				type={'btnPreProdInsert'}
				onClick={preProdInsert}
			/>
			<SButton
				className="btn_red"
				buttonName={'생산등록'}
				type={'btnProdInsert'}
				onClick={prodInsert}
			/>
			<SButton
				className="btn_red"
				buttonName={'생산취소'}
				type={'btnProdCancel'}
				onClick={invenCancel}
			/>
			<SButton
				className="btn_red"
				buttonName={'생산량조정'}
				type={'btnProdChange'}
				onClick={openProdQtyModal}
			/>
		</React.Fragment>
	)
}

const MiddleItemSub = ({ }) => {
	return (
		<React.Fragment></React.Fragment>
	)
}

const ContentItem = ({ setOrderDetailGridApi, orderDetailDataList, pinnedBottomRowData, rowClassRules }) => {
	return (
		<SGrid
			grid={'orderDetailGrid'}
			gridApiCallBack={setOrderDetailGridApi}
			rowData={orderDetailDataList}
			pinnedBottomRowData={pinnedBottomRowData}
			suppressRowClickSelection={true}
			cellReadOnlyColor={true}
			rowClassRules={rowClassRules}
			editable={false}
		/>
	)
}

const ContentSubItem = ({ setOrderDetailSubGridApi, orderDetailSubDataList, pinnedBottomRowData }) => {
	return (
		<SGrid
			grid={'orderDetailSubGrid'}
			gridApiCallBack={setOrderDetailSubGridApi}
			rowData={orderDetailSubDataList}
			pinnedBottomRowData={pinnedBottomRowData}
			suppressRowClickSelection={true}
			cellReadOnlyColor={true}
			editable={false}
		/>
	)
}


const ProdQtyModal = ({ prodQtyModalProdQty, handleChange, prodQtyModalProdQtyUom, prodQtyModalSave }) => {
	return (
		<React.Fragment>
			<table style={{ width: "100%" }}>
				<tbody>
					<tr>
						<th>생산수량</th>
						<td>
							<SNumericInput
								id="prodQtyModalProdQty"
								value={prodQtyModalProdQty}
								onChange={handleChange}
							/>
							{prodQtyModalProdQtyUom}
						</td>
					</tr>
				</tbody>
			</table>
			<div>
				<SButton buttonName={"저장"} onClick={prodQtyModalSave} type={"btnProdQtySave"} />
			</div>
		</React.Fragment>
	)
}

OrderHelper.defaultProps = {
	// input binding property
	barcodeYn: 'Y'
}