import API from "components/override/api/API";

class HelperRepository {

    URL = "/api/helper";

    //작업지시
    getWorkorder(params) {
        return API.request.get(encodeURI(`${this.URL}/workorder?dateType=${params.dateType}&lineId=${params.lineId}&startDate=${params.startDate}&endDate=${params.endDate}&woStatus=${params.woStatus}&orderNo=${params.orderNo}&item=${params.item}&buId=${params.buId}`))
    }

    //Lot추가: 지시품목
    getInoutOrder(params) {
        return API.request.get(encodeURI(`${this.URL}/lot/inout?inoutDetailNo=${params.inoutDetailNo}`))
    }

    //Lot추가: 바코드 목록
    getAvailableBarcodeList(params) {
        return API.request.get(encodeURI(`${this.URL}/lot/barcode?inoutDetailNo=${params.inoutDetailNo}&prodDt=${params.prodDt}&barcode=${params.barcode}`))
    }

    //사원
    getEmployee(params) {
        return API.request.get(encodeURI(`/api/system/user/getusers?empNo=${params.empNo}&useYN=${params.useYN}&buId=${params.buId}`))
    }

    //제품
    getItem(params) {
        return API.request.get(encodeURI(`${this.URL}/item?item=${params.item}&glclass=${params.glclass}&buId=${params.buId}&barcodeYn=${params.barcodeYn}`))
    }

}
export default new HelperRepository();