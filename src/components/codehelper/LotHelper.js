import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { observable } from 'mobx'
import SearchTemplate from 'components/template/SearchTemplate'
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';
import SGrid from 'components/override/grid/SGrid'
import Alert from 'components/override/alert/Alert';
import GridUtil from 'components/override/grid/utils/GridUtil';
import SInput from 'components/atoms/input/SInput'
import SDatePicker from 'components/override/datepicker/SDatePicker'
import SNumericInput from 'components/override/numericinput/SNumericInput'
import SButton from 'components/atoms/button/SButton'
import HelperRepository from 'components/codehelper/service/HelperRepository'
import SModal from 'components/override/modal/SModal'
import InoutRepository from 'modules/pages/inout/inout/service/InoutRepository';

export default class LotModalHelper extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false
        }
    }

    static getDerivedStateFromProps = (nextProps, prevState) => {
        if (nextProps.isLotOpen != prevState.isModalOpen) {
            return { isModalOpen: nextProps.isLotOpen }
        }
        return null;
    }

    handleClose = (result) => {
        const { onHelperResult, id, handleChange } = this.props;

        if (onHelperResult) {
            onHelperResult(result);
        }

        if (handleChange) {
            handleChange({ id: id, value: false })
        }
    }

    render() {

        return (
            <SModal
                isOpen={this.state.isModalOpen}
                onRequestClose={() => this.handleClose(null)}
                contentLabel={this.props.title}
                width={1080}
                contents={<LotHelper {...this.props} onHelperResult={this.handleClose} />}
            />
        )
    }
}

LotModalHelper.propTypes = {
    onHelperResult: PropTypes.func,
    title: PropTypes.string,
    inoutDetailNo: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    isLotOpen: PropTypes.bool.isRequired,
}

LotModalHelper.defaultProps = {
    title: "Lot 추가",
}

@observer
class LotHelper extends Component {

    @observable orderList = [];
    @observable barcodeList = [];

    orderGridApi = undefined;
    barcodeGridApi = undefined;

    @observable prodDt = '';
    @observable barcode = '';
    @observable selectQty = 0;


    // 지시품목에서 선택한 항목
    inoutDetailNo = null;
    detailLedgerType;
    barcodeYn = "Y";
    @observable inoutQty = 0;

    constructor(props) {
        super(props);
    }

    async componentDidMount() {
        this.inoutDetailNo = this.props.inoutDetailNo;
        await this.handleSearch();

    }
    handleChange = (data) => {
        this[data.id] = data.value;
    }

    handleSearch = async () => {

        const params = {
            inoutDetailNo: this.inoutDetailNo
        }

        const { data, status } = await HelperRepository.getInoutOrder(params);

        if (data.success) {
            this.orderList = data.data;
            this.orderGridApi.setRowData(this.orderList);

            if (this.orderList.length > 0) {
                GridUtil.setFocusByRowIndex(this.orderGridApi, this.orderList.findIndex(x => x.inoutDetailNo == this.inoutDetailNo));
            }
        }
    }

    handleBarcodeSearch = async () => {

        this.selectQty = 0;

        const params = {
            inoutDetailNo: this.inoutDetailNo,
            prodDt: this.prodDt,
            barcode: this.barcode,
        }

        const { data, status } = await HelperRepository.getAvailableBarcodeList(params);

        if (data.success) {

            const { selectedBarcodeList } = this.props;

            if (selectedBarcodeList) {
                const tempList = [];

                data.data.forEach(item => {
                    if (item.barcode.substring(0, 2) == "99") {
                        tempList.push(item);
                    } else if (!selectedBarcodeList.some(x => x.barcode == item.barcode)) {
                        tempList.push(item);
                    }
                });

                this.barcodeList = tempList;
            } else {
                this.barcodeList = data.data;
            }

            this.barcodeGridApi.setRowData(this.barcodeList);
        }
    }

    handleSelectionChanged = async () => {
        const selectedRow = this.orderGridApi.getSelectedRows()[0];
        this.inoutDetailNo = selectedRow.inoutDetailNo;
        this.inoutQty = selectedRow.inoutQty ? selectedRow.inoutQty : 0;
        this.barcodeYn = selectedRow.barcodeYn;
        this.detailLedgerType = selectedRow.detailLedgerType;
        this.getLotInfoColumnDefs();
        await this.handleBarcodeSearch();
    }

    handleSave = async () => {
        const orderSelectedRow = this.orderGridApi.getSelectedRows()[0];

        const inoutQty = Number(orderSelectedRow.inoutQty);
        const saveQty = Number(orderSelectedRow.processQty);

        if ((this.selectQty + saveQty) > inoutQty) {
            Alert.meg("선택한 수량과 기저장된 수량의 합이 지시수량보다 큽니다.");
            return;
        }

        const searchData = {
            "orderNo": orderSelectedRow.orderNo
            , "inoutDetailNo": orderSelectedRow.inoutDetailNo
            , "itemId": orderSelectedRow.itemId
        }

        const barcodeSelectedRow = this.barcodeGridApi.getSelectedRows();

        for (let i = 0; i < barcodeSelectedRow.length; i++) {
            if (orderSelectedRow.barcodeYn = 'N' && barcodeSelectedRow[i].prodDt == '') {
                Alert.meg("수량관리대상은 생산일자가 입력되어야 합니다.");
                return;
            }
            if (barcodeSelectedRow[i].processQty > barcodeSelectedRow[i].packQty) {
                Alert.meg("재고량보다 클 수 없습니다.");
                return;
            }
            barcodeSelectedRow[i].ledgerType = orderSelectedRow.ledgerType;
        }

        const params = JSON.stringify({ param1: barcodeSelectedRow, param2: searchData });

        const { data, status } = await InoutRepository.saveBarcode(params);
        if (data.success) {
            Alert.meg("저장되었습니다");
            this.handleSearch();
        } else {
            Alert.meg(data.msg);
        }

        // 자동 종료 안함
        // const { onHelperResult } = this.props;
        // if (onHelperResult) {
        //     onHelperResult(barcodeSelectedRow);
        // }
    }

    handleClose = () => {
        const { onHelperResult } = this.props;

        if (onHelperResult) {
            onHelperResult(null);
        }
    }

    setOrderGridApi = (gridApi) => {
        this.orderGridApi = gridApi;

        const columnDefs = [
            {
                headerName: "수불타입"
                , children: [
                    {
                        headerName: "수불타입"
                        , field: "orderTypeNm"
                        , width: 100
                        , cellClass: "cell_align_center"
                    }
                ]
            },
            {
                headerName: "제품코드"
                , children: [
                    {
                        headerName: "제품코드"
                        , field: "itemCd"
                        , width: 85
                        , cellClass: "cell_align_center"
                    }
                ]
            },
            {
                headerName: "제품명"
                , children: [
                    {
                        headerName: "제품명"
                        , field: "itemNm"
                        , width: 200
                    }
                ]
            },
            {
                headerName: "지시수량",
                children: [
                    {
                        headerName: "수량"
                        , field: "inoutQty"
                        , width: 65
                        , type: "numericColumn"
                        , valueFormatter: (params) => GridUtil.numberFormatter(params, 2)
                    },
                    {
                        headerName: "단위"
                        , field: "qtyUom"
                        , width: 40
                        , cellClass: "cell_align_center"
                    },
                ]
            },
            {
                headerName: "지시중량",
                children: [
                    {
                        headerName: "중량"
                        , field: "qtywei"
                        , width: 65
                        , type: "numericColumn"
                        , valueFormatter: GridUtil.numberFormatter
                    },
                    {
                        headerName: "단위"
                        , field: "wgtunt"
                        , width: 40
                        , cellClass: "cell_align_center"
                    },
                ]
            },
            {
                headerName: "기저장된 수량",
                children: [
                    {
                        headerName: "수량"
                        , field: "processQty"
                        , width: 65
                        , type: "numericColumn"
                        , valueFormatter: (params) => GridUtil.numberFormatter(params, 2)
                    },
                    {
                        headerName: "단위"
                        , field: "qtyUom"
                        , width: 40
                        , cellClass: "cell_align_center"
                    },
                ]
            },
        ];

        this.orderGridApi.setColumnDefs(columnDefs);
    }

    setLotInfoGridApi = (gridApi) => {

        this.barcodeGridApi = gridApi;
        this.getLotInfoColumnDefs();
    }

    getLotInfoColumnDefs = () => {

        const isEditableProdDt = this.barcodeYn == "N" ? true : false;
        const isEditableProcessQty = this.detailLedgerType != "ST" ? true : false;

        const columnDefs = [
            {
                headerName: "바코드"
                , children: [
                    {
                        headerName: "바코드"
                        , field: "barcode"
                        , width: 130
                        , editable: false
                        , cellClass: "cell_align_center"
                        , checkboxSelection: true
                        , headerCheckboxSelection: true
                    }
                ]
                , checkboxSelection: true
                , headerCheckboxSelection: true
            },
            {
                headerName: "생산일자"
                , children: [
                    {
                        headerName: "생산일자"
                        , field: "prodDt"
                        , width: 80
                        , editable: isEditableProdDt
                        , cellClass: "cell_align_center"
                        , valueFormatter: GridUtil.dateFormatter
                    }
                ]
            },
            {
                headerName: "유통기한"
                , children: [
                    {
                        headerName: "유통기한"
                        , field: "expiryDt"
                        , width: 80
                        , editable: false
                        , cellClass: "cell_align_center"
                        , valueFormatter: GridUtil.dateFormatter
                    }
                ]
            },
            {
                headerName: "LOT NO"
                , children: [
                    {
                        headerName: "LOT NO"
                        , field: "lotNo"
                        , width: 75
                        , editable: false
                        , cellClass: "cell_align_center"
                    }
                ]
            },
            {
                headerName: "제품코드"
                , children: [
                    {
                        headerName: "제품코드"
                        , field: "itemCd"
                        , width: 80
                        , editable: false
                        , cellClass: "cell_align_center"
                    }
                ]
            },
            {
                headerName: "제품명"
                , children: [
                    {
                        headerName: "제품명"
                        , field: "itemNm"
                        , width: 200
                        , editable: false
                    }
                ]
            },
            {
                headerName: "가용량"
                , children: [
                    {
                        headerName: "가용량"
                        , field: "availableQty"
                        , width: 60
                        , editable: false
                        , type: "numericColumn"
                        , valueFormatter: GridUtil.numberFormatter
                    },
                    {
                        headerName: "단위"
                        , field: "prodUom"
                        , width: 50
                        , editable: false
                        , cellClass: "cell_align_center"
                    },
                ]
            },
            {
                headerName: "가용수량"
                , children: [
                    {
                        headerName: "가용수량"
                        , field: "packQty"
                        , width: 60
                        , editable: false
                        , type: "numericColumn"
                        , valueFormatter: (params) => GridUtil.numberFormatter(params, 2)
                    },
                    {
                        headerName: "단위"
                        , field: "packUom"
                        , width: 50
                        , editable: false
                        , cellClass: "cell_align_center"
                    },
                ]
            },
            {
                headerName: "출고수량"
                , children: [
                    {
                        headerName: "출고수량"
                        , field: "processQty"
                        , width: 60
                        , editable: isEditableProcessQty
                        , type: "numericColumn"
                        , valueFormatter: (params) => GridUtil.numberFormatter(params, 2)
                    },
                ]
            },
            {
                headerName: "위치"
                , children: [
                    {
                        headerName: "위치"
                        , field: "locNm"
                        , width: 125
                        , editable: false
                    }
                ]
            },
            {
                headerName: "LOT품질"
                , children: [
                    {
                        headerName: "LOT품질"
                        , field: "lotQualityNm"
                        , width: 60
                        , editable: false
                        , cellClass: "cell_align_center"
                    }
                ]
            },
        ];

        this.barcodeGridApi.setColumnDefs(columnDefs);
    }

    render() {
        return (
            <React.Fragment>
                <div className='basic'>
                    <div style={{ width: '100%', height: '30%' }}>
                        <OrderItem
                            {...this.props}
                            orderList={this.orderList}
                            onChange={this.handleChange}
                            gridApiCallBack={this.setOrderGridApi}
                            onSelectionChanged={this.handleSelectionChanged}
                        />
                    </div>
                    <div style={{ width: '100%', height: '70%' }}>
                        <LotInfoItem
                            prodDt={this.prodDt}
                            barcode={this.barcode}
                            selectQty={this.selectQty}
                            inoutQty={this.inoutQty}
                            onChange={this.handleChange}
                            onSearchClick={this.handleBarcodeSearch}
                            onSaveClick={this.handleSave}
                            onCloseClick={this.handleClose}
                            gridApiCallBack={this.setLotInfoGridApi}
                        />
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

@observer
class OrderItem extends Component {

    render() {

        const { orderList, gridApiCallBack, onSelectionChanged } = this.props;

        return (
            <React.Fragment>
                <ContentsMiddleTemplate contentSubTitle={"지시품목"} />
                <ContentsTemplate
                    id='addLotGrid'
                    contentItem={
                        <SGrid
                            grid={'addLotGrid'}
                            rowData={orderList}
                            gridApiCallBack={gridApiCallBack}
                            onSelectionChanged={onSelectionChanged}
                            editable={false}
                            headerHeight={0}
                        />
                    }
                />
            </React.Fragment>
        )
    }
}

@observer
class LotInfoItem extends Component {

    handleSelectionChanged = (event) => {

        let selectQty = 0;

        const { api } = event;

        const inoutQty = this.props.inoutQty;

        api.getModel().rowsToDisplay.forEach(item => {
            if (item.selected) {
                if (!item.data.processQty || item.data.processQty == 0) {
                    if (inoutQty < item.data.packQty) {
                        item.data.processQty = inoutQty;
                    } else {
                        item.data.processQty = item.data.packQty;
                    }
                }
                item.data.processUom = item.data.packUom;
                selectQty += Number(item.data.processQty);
            } else {
                item.data.processQty = null;
            }
        })

        api.redrawRows();

        if (!selectQty) {
            selectQty = 0;
        }

        this.props.onChange({ id: 'selectQty', value: selectQty });
    }

    handleCellValueChanged = (event) => {
        const { api, node } = event;

        if (event.colDef.field == "processQty") {
            let selectQty = 0;

            api.getSelectedRows().map(item => {
                if (isNaN(event.newValue)) {
                    item.processQty = 0;
                } else if (item.packQty < Number(item.processQty)) {
                    item.processQty = item.packQty;
                }
                const qty = Number(item.processQty)
                selectQty += qty ? qty : 0;
            })

            api.redrawRows();
            node.setSelected(true);

            this.props.onChange({ id: 'selectQty', value: selectQty });
        }
    }

    render() {

        const rowClassRules = {
            'highlight_red_background': function (params) {
                if (params.data.barcodeType && params.node.rowPinned != "bottom") {
                    if (params.data.barcodeType == "M") {
                        return true;
                    }
                }
            }
        }

        const { onChange, prodDt, barcode, selectQty, onSearchClick, onSaveClick, onCloseClick, gridApiCallBack } = this.props;

        return (
            <React.Fragment>
                <SearchTemplate
                    searchItem={
                        <React.Fragment>
                            <SDatePicker
                                title={"생산일자"}
                                id={"prodDt"}
                                value={prodDt}
                                onChange={onChange}
                            />
                            <SInput
                                title={"바코드"}
                                id={"barcode"}
                                width={110}
                                value={barcode}
                                onChange={onChange}
                            />
                            <SNumericInput
                                title={"선택수량"}
                                id={"selectQty"}
                                width={80}
                                value={selectQty}
                                onChange={onChange}
                                decimalScale={7}
                                readOnly={true}
                            />
                            <div className='search_item_btn'>
                                <SButton
                                    buttonName={'조회'}
                                    type={'default'}
                                    onClick={onSearchClick} />
                            </div>
                        </React.Fragment>
                    }
                />
                <ContentsMiddleTemplate
                    contentSubTitle={"LOT 정보"}
                    middleItem={
                        <React.Fragment>
                            <SButton
                                buttonName={'저장'}
                                type={'default'}
                                onClick={onSaveClick}
                            />
                            <SButton
                                buttonName={'닫기'}
                                type={'default'}
                                onClick={onCloseClick}
                            />
                        </React.Fragment>
                    }
                />
                <ContentsTemplate
                    id="selectBarcode"
                    contentItem={
                        <SGrid
                            grid={'selectBarcodeGrid'}
                            gridApiCallBack={gridApiCallBack}
                            editable={false}
                            onSelectionChanged={this.handleSelectionChanged}
                            onCellValueChanged={this.handleCellValueChanged}
                            suppressRowClickSelection={true}
                            headerHeight={0}
                            rowClassRules={rowClassRules}
                        />
                    }
                />
            </React.Fragment>
        )
    }
}