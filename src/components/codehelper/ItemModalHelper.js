import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { observable } from 'mobx'
import SearchTemplate from 'components/template/SearchTemplate'
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';
import SGrid from 'components/override/grid/SGrid'
import Alert from 'components/override/alert/Alert';
import GridUtil from 'components/override/grid/utils/GridUtil';
import SInput from 'components/atoms/input/SInput'
import SDatePicker from 'components/override/datepicker/SDatePicker'
import SNumericInput from 'components/override/numericinput/SNumericInput'
import SButton from 'components/atoms/button/SButton'
import HelperRepository from 'components/codehelper/service/HelperRepository'
import SModal from 'components/override/modal/SModal'
import SSelectBox from 'components/atoms/selectbox/SSelectBox'
import CommonStore from 'modules/pages/common/service/CommonStore'
import STreeSelectBox from 'components/override/tree/STreeSelectBox';

export default class ItemModalHelper extends Component {

    @observable helperOption = observable({});

    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false,
            helperOption: {},
        }
    }

    handleHelperChange = (data) => {
        this.helperOption[data.id] = data.value;
    }

    async componentDidMount() {
        this.setData();
    }

    setData = () => {
        this.helperOption = this.state.helperOption;
    }

    static getDerivedStateFromProps = (nextProps, prevState) => {
        if (nextProps.isItemOpen != prevState.isModalOpen) {
            return { isModalOpen: nextProps.isItemOpen }
        }
        if (JSON.stringify(nextProps.helperOption) != JSON.stringify(prevState.helperOption)) {
            return { helperOption: JSON.parse(JSON.stringify(nextProps.helperOption)) }
        }
        return null;
    }

    handleClose = (result) => {
        const { onHelperResult, id, handleChange } = this.props;

        if (onHelperResult) {
            onHelperResult(result);
        }

        if (handleChange) {
            handleChange({ id: id, value: false })
        }
    }

    render() {

        return (
            <SModal
                formId={'order_workorder_erp_partdivide_itemAdd_p'}
                isOpen={this.state.isModalOpen}
                onRequestClose={() => this.handleClose(null)}
                contentLabel={this.props.title}
                width={1030}
                contents={<ItemHelper {...this.props} helperOption={this.helperOption} onHelperResult={this.handleClose} />}
            />
        )
    }
}

ItemModalHelper.propTypes = {
    onHelperResult: PropTypes.func,
    title: PropTypes.string,
    isItemOpen: PropTypes.bool.isRequired,
}

ItemModalHelper.defaultProps = {
    title: "제품추가",
}
@observer
class ItemHelper extends Component {

    @observable itemList = [];

    gridApi = undefined;

    constructor(props) {
        super(props);

    }

    async componentDidMount() {
        await this.handleSearch();
    }

    handleChange = (data) => {
        this[data.id] = data.value;
    }

    handleSearch = async () => {
        const { glclass, item, buId, barcodeYn } = this.props.helperOption;

        const params = {
            item: item,
            glclass: glclass,
            buId: buId,
            barcodeYn: barcodeYn,
        };
        const { data, status } = await HelperRepository.getItem(params);

        if (data.success) {
            this.itemList = data.data;
            this.gridApi.setRowData(this.itemList);
        }
    }

    handleSelect = () => {

        const selectedRows = this.gridApi.getSelectedRows();
        if (selectedRows.length <= 0) {
            Alert.meg("추가할 제품울 선택해주세요.");
            return;
        }

        const { onHelperResult } = this.props;

        if (onHelperResult) {
            onHelperResult(selectedRows);
        }
    }

    handleClose = () => {
        const { onHelperResult } = this.props;

        if (onHelperResult) {
            onHelperResult(null);
        }
    }

    gridApiCallBack = (gridApi) => {
        this.gridApi = gridApi;

        const columnDefs = [
            {
                headerName: "제품타입"
                , field: "glclass"
                , width: 65
                , cellClass: "cell_align_center"
                , valueFormatter: (params) => GridUtil.getCodeNm(params, "MT002")
            },
            { headerName: "제품코드", field: "itemCd", width: 85, cellClass: "cell_align_center" },
            { headerName: "제품명", field: "itemNm", width: 230 },
            {
                headerName: "제품단위"
                , field: "itemUom"
                , width: 65
                , cellClass: "cell_align_center"
            },
            { headerName: "SearchText", field: "searchText", width: 150 },
            { headerName: "규격", field: "spec", width: 150 },
            {
                headerName: "유통기한"
                , field: "expiryDay"
                , width: 65
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
            },
            { headerName: "재고관리여부", field: "invenYn", width: 85, cellClass: "cell_align_center" },
            { headerName: "바코드관리여부", field: "barcodeYn", width: 95, cellClass: "cell_align_center" },
        ];

        this.gridApi.setColumnDefs(columnDefs);
    }

    render() {

        const { mode, buId, buIdReadOnly, glclass, item } = this.props.helperOption;
        const { handleHelperChange } = this.props;

        return (
            <React.Fragment>
                <SearchTemplate searchItem={
                    <React.Fragment>
                        <SSelectBox
                            title={"공장"}
                            id={"buId"}
                            value={buId}
                            onChange={handleHelperChange}
                            addOption={"전체"}
                            disabled={buIdReadOnly}
                            optionGroup={CommonStore.buList}
                            valueMember={"buId"}
                            displayMember={"buNm"}
                        />

                        {mode != "fix" ?
                            <STreeSelectBox
                                title={"제품구분"}
                                id={"glclass"}
                                contentWidth={200}
                                value={glclass}
                                onChange={handleHelperChange}
                                codeGroup={"MT002"}
                            />
                            :
                            null
                        }

                        <SInput
                            title={"제품"}
                            id={"item"}
                            value={item}
                            onChange={handleHelperChange}
                            onEnterKeyDown={this.handleSearch}
                            isFocus={true}
                            contentWidth={130}
                        />

                        <div className='search_item_btn'>
                            <SButton
                                buttonName={'조회'}
                                type={'btnSearch'}
                                onClick={this.handleSearch} />
                        </div>
                    </React.Fragment>
                } />
                <ContentsMiddleTemplate contentSubTitle={"제품"}

                    middleItem={
                        <React.Fragment>
                            <SButton
                                buttonName={'선택'}
                                type={'btnSelect'}
                                onClick={this.handleSelect} />
                            <SButton
                                buttonName={'취소'}
                                type={'default'}
                                onClick={this.handleClose} />
                        </React.Fragment>
                    } />
                <ContentsTemplate
                    id='addLotGrid'
                    contentItem={
                        <SGrid
                            grid={'addLotGrid'}
                            rowData={this.itemList}
                            gridApiCallBack={this.gridApiCallBack}
                            rowDoubleClick={this.handleSelect}
                            editable={false}
                        />
                    }
                />
            </React.Fragment>
        )
    }
}