import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer, inject } from 'mobx-react';
import { observable } from 'mobx'

import SInput from 'components/atoms/input/SInput'
import SSelectBox from 'components/atoms/selectbox/SSelectBox'
import CodeHelper from 'components/codehelper/HoC/CodeHelper'
import ItemRepository from 'modules/pages/system/item/service/ItemRepository';
import SearchTemplate from 'components/template/SearchTemplate'
import GridUtil from 'components/override/grid/utils/GridUtil'
import HelperRepository from './service/HelperRepository';
import CommonStore from 'modules/pages/common/service/CommonStore';

@observer
export default class EmployeeHelper extends Component {

    @observable helperOption = observable({});

    // CodeHelper에 전달할 조회 내역 List
    @observable employeeList = [];

    // // CodeHelper-Modal에 표시할 Grid ColumnDefs
    columnDefs = [
        { headerName: "로그인ID", field: "loginId", width: 100, cellClass: 'cell_align_center' },
        { headerName: "사번", field: "empNo", width: 90, cellClass: 'cell_align_center' },
        { headerName: "이름", field: "loginNm", width: 80, cellClass: 'cell_align_center' },
        { headerName: "부서", field: "deptNm", width: 100 },
        { headerName: "이메일", field: "loginEmail", width: 200 },
        { headerName: "전화번호", field: "loginTel", width: 120 },

    ];
    constructor(props) {
        super(props);

        this.state = {
            helperOption: {},
        }
    }

    componentDidMount() {
        this.setData();
    }

    static getDerivedStateFromProps = (nextProps, prevState) => {
        if (JSON.stringify(nextProps.helperOption) != JSON.stringify(prevState.helperOption)) {
            return { helperOption: JSON.parse(JSON.stringify(nextProps.helperOption)) }
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.helperOption !== this.state.helperOption) {
            this.setData();
        }
    }

    setData = () => {
        this.helperOption = this.state.helperOption;
    }

    handleHelperChange = (data) => {
        this.helperOption[data.id] = data.value;
    }

    // CodeHelper에 전달할 Search 함수
    handleSearch = async () => {

        const { empNo, searchText } = this.helperOption;

        const params = {
            empNo: empNo,
            useYN: 'Y',
            buId: CommonStore.buId,
        };

        const { data, status } = await HelperRepository.getEmployee(params);

        if (status == 200) {
            this.employeeList = data.data;
        }
    }
    render() {

        const { empNo } = this.helperOption;

        return (
            <React.Fragment>

                <CodeHelper
                    {...this.props}
                    columnDefs={this.columnDefs}
                    onSearch={this.handleSearch}
                    onHelperChange={this.handleHelperChange}
                    onOpening={this.setData}
                    itemList={this.employeeList}
                    helperCdNmValue={"empNo"}
                    searchItem={
                        <SearchItem
                            empNo={empNo}
                            onSearch={this.handleSearch}
                            onHelperChange={this.handleHelperChange}
                        />
                    }
                />
            </React.Fragment>
        )
    }
}

// CodeHelper에서의 조회 조건 Component
class SearchItem extends Component {

    render() {

        const { empNo, onSearch, onHelperChange } = this.props;

        return (
            <SearchTemplate searchItem={
                <React.Fragment>
                    <SInput
                        title={"사원"}
                        id={"empNo"}
                        value={empNo}
                        onChange={onHelperChange}
                        onEnterKeyDown={onSearch}
                        isFocus={true}
                    />
                </React.Fragment>
            } />
        )
    }
}