import moment from 'moment';
import 'moment/locale/ko';
import { observer, inject } from 'mobx-react';
import { observable, action } from 'mobx'
import CommonStore from 'modules/pages/common/service/CommonStore';

class OptionModel {

    optionModel = observable({
        cdWidth: 80,
        cdReadOnly: true,
        cdVisible: false,
        cdNmWidth: 100,
        cdNmReadOnly: false,
        cdNmIsFocus: false,

        // searchButton
        searchVisible: true,

        // modal    
        modalWidth: '60%',
        modalHeight: '60%',
        modalTitle: '포장지시',

        isEmptyCondition: false,

        dateType: '01',
        startDate: moment().add(-7, 'days').format('YYYYMMDD'),
        endDate: moment().add(7, 'days').format('YYYYMMDD'),
        woStatus: '',
        item: '',
        lineId: '',
        lineIdReadOnly: true,
        buId: CommonStore.buId,
        buIdReadOnly: true,
        orderNo: '',
    });
}

export default OptionModel;