import { observable } from 'mobx'
import CommonStore from 'modules/pages/common/service/CommonStore'

class OptionModel {

    optionModel = observable({
        cdWidth: 80,
        cdReadOnly: true,
        cdVisible: true,
        cdNmWidth: 100,
        cdNmReadOnly: false,
        cdNmIsFocus: false,
    
        // searchButton
        searchVisible: true,
    
        // modal    
        modalWidth: '50%',
        modalHeight: '50%',
        modalTitle: '제품조회',
    
        isEmptyCondition: false,
    
        glclass: 'S110,S120,S140',
        item: '',
        buId: CommonStore.buId,
        buIdReadOnly: true,
        barcodeYn: '',
    
        // mode
        mode: 'default',
    });
}

export default OptionModel;