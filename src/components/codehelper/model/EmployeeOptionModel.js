import { observable } from 'mobx'

class OptionModel {
    optionModel = observable({
        cdWidth: 80,
        cdReadOnly: true,
        cdVisible: true,
        cdNmWidth: 100,
        cdNmReadOnly: false,
        cdNmIsFocus: false,

        // searchButton
        searchVisible: true,

        // modal
        modalWidth: 1000,
        modalHeight: 800,

        isEmptyCondition: false,

        empNo: '',
    });
};

export default OptionModel;