import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { observable } from 'mobx'
import moment from 'moment';
import 'moment/locale/ko';

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import SNumericInput from 'components/override/numericinput/SNumericInput';
import SInput from 'components/atoms/input/SInput';
import SButton from 'components/atoms/button/SButton';
import SGrid from 'components/override/grid/SGrid';
import GridUtil from 'components/override/grid/utils/GridUtil';
import SModal from 'components/override/modal/SModal';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import Alert from 'components/override/alert/Alert';

import EtcRepository from 'modules/pages/inout/etc/service/EtcRepository';
import HelperRepository from 'components/codehelper/service/HelperRepository';

// 생산정보 Modal
export default class BarcodeAddHelper extends Component {

    constructor(props) {
        super(props);
        this.state = {
            barcodeAddHelperModalIsOpen: false
        }
    }

    // Onload 정보
    static getDerivedStateFromProps = (nextProps, prevState) => {
        if (nextProps.isBarcodeOpen != prevState.barcodeAddHelperModalIsOpen) {
            return { barcodeAddHelperModalIsOpen: nextProps.isBarcodeOpen }
        }
        return null;
    }

    // barcodeAddHelperModalIsOpen ture or false 처리
    handleClose = (result) => {
        const { onHelperResult, id, handleChange } = this.props;

        if (onHelperResult) {
            onHelperResult(result);
        }

        if (handleChange) {
            handleChange({ id: id, value: false })
        }
    }
    render() {
        return (
            <SModal
                formId={"inout_barcode_add_p"}
                isOpen={this.state.barcodeAddHelperModalIsOpen}
                onRequestClose={this.handleClose}
                contentLabel={this.props.title}
                width={800}
                minWidth={800}
                height={400}
                minHeight={400}
                contents={<BarcodeHelper {...this.props} onHelperResult={this.handleClose} />}
            />
        )
    }
}

BarcodeAddHelper.propTypes = {
    onHelperResult: PropTypes.func,
    title: PropTypes.string,
    barcodeHelplerOrderNo: PropTypes.number,
    barcodeHelplerledgerType: PropTypes.string,
    isBarcodeOpen: PropTypes.bool.isRequired,
}

BarcodeAddHelper.defaultProps = {
    title: "바코드추가",
}

@observer
class BarcodeHelper extends Component {

    // Barcode List를 불러오기 위한 변수	
    @observable barcode = '';
    @observable prodDt = moment().format('YYYYMMDD');
    @observable itemNmOrItemCd = '';
    @observable barcodeList = [];
    @observable barcodeGridApi = undefined;

    barcodeHelplerOrderNo = 0;
    barcodeHelplerledgerType = '';

    constructor(props) {
        super(props);
    }

    handleChange = (data) => {
        this[data.id] = data.value;
    }

    //Barcode Info
    // async componentWillMount() {
    // 	const submitData = {
    // 		"orderNo": Number(this.props.orderNo)
    // 	}

    // 	if (this.props.orderNo != null) {
    // 		const { data, status } = await BarcodeHistoryRepository.getOrderInfo(submitData);
    // 		//this.orderDetailModelClick();
    // 		if (status === 200 && data.data != null) {
    // 			const successData = data.data
    // 			this.orderTypeNm = successData.orderTypeNm
    // 			this.itemCd = successData.itemCd
    // 			this.itemNm = successData.itemNm
    // 			this.planStartDtm = successData.planStartDtm
    // 			this.planEndDtm = successData.planEndDtm
    // 			this.orderQty = successData.orderQty
    // 			this.prodQty = successData.prodQty
    // 			this.progress = ((successData.prodQty / successData.orderQty) * 100).toFixed(2) + '%'
    // 		} else {
    // 			Alert.meg('생산정보가 없습니다.')
    // 		}
    // 	}
    // 	this.orderDetailModelClick();
    // }

    //조회버튼
    searchClick = async () => {
        const searchData = {
            barcode: this.barcode
            , prodDt: this.prodDt
            , itemNmOrItemCd: this.itemNmOrItemCd
        }

        const { data, status } = await EtcRepository.getBarcode(searchData);
        if (status === 200) {
            this.barcodeList = data.data;
            this.barcodeGridApi.setRowData(this.barcodeList);
        }
    }

    setBarcodeGridApi = async (gridApi) => {
        this.barcodeGridApi = gridApi;
        const columnDefs = [
            {
                headerName: ""
                , field: "check"
                , width: 29
                , checkboxSelection: true
                , headerCheckboxSelection: true
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "순번"
                , field: "seq"
                , width: 40
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
            },
            {
                headerName: "바코드번호"
                , field: "barcode"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "상태"
                , field: "lotQualityNm"
                , width: 50
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "제품코드"
                , field: "itemCd"
                , width: 90
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "LOT NO"
                , field: "lotNo"
                , width: 80
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "생산일자"
                , field: "prodDt"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "유통기한"
                , field: "expiryDt"
                , width: 90
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "재고량"
                , field: "processQty"
                , width: 60
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
            },
            {
                headerName: "위치"
                , field: "locNm"
                , width: 120
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "처리시간"
                , field: "inputDtm"
                , width: 110
                , cellClass: 'cell_align_center'
                , valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm')
            },
            {
                headerName: "처리자"
                , field: "inputEmpNm"
                , width: 90
                , cellClass: 'cell_align_center'
            }
        ];
        this.barcodeGridApi.setColumnDefs(columnDefs);
    }

    handleSave = async () => {
        const { barcodeHelplerOrderNo, barcodeHelplerledgerType } = this.props;
        const selectedRows = this.barcodeGridApi.getSelectedRows();
        if (selectedRows.length <= 0) {
            Alert.meg("저장할 바코드를 선택해주세요.");
            return;
        }
        // 저장 로직
        let result = '';
        const addData = {
            orderNo: barcodeHelplerOrderNo
            , ledgerType: barcodeHelplerledgerType
        }

        const params = JSON.stringify({ param1: selectedRows, param2: addData });
        const { data, status } = await EtcRepository.saveBarcode(params);
        if (data.success == true) {
            result = true;
        } else {
            Alert.meg("저장에 실패했습니다.");
        }
        const { onHelperResult } = this.props;

        if (onHelperResult) {
            onHelperResult(result);
        }
    }

    handleClose = () => {
        const { onHelperResult } = this.props;

        if (onHelperResult) {
            onHelperResult(null);
        }
    }

    render() {
        return (
            <React.Fragment>
                    <SearchTemplate
                        searchItem={
                            <SearchItem
                                searchClick={this.searchClick}
                                barcode={this.barcode}
                                prodDt={this.prodDt}
                                itemNmOrItemCd={this.itemNmOrItemCd}
                                // orderNo={this.props.orderNo}
                                handleChange={this.handleChange}
                            />} />
                <ContentsMiddleTemplate
                    contentSubTitle={'LOT 정보'}
                    middleItem={
                        <MiddleItem
                            handleSave={this.handleSave}
                            handleClose={this.handleClose}
                        />} />
                <ContentsTemplate id='barcodeHelper'
                    contentItem={
                        <ContentItem
                            setBarcodeGridApi={this.setBarcodeGridApi}
                            barcodeList={this.barcodeList}
                            handleSave={this.handleSave}
                        />} />
            </React.Fragment>
        );
    }
}

//SearchItem
const SearchItem = ({
    searchClick
    , barcode
    , prodDt
    , itemNmOrItemCd
    , handleChange
}) => {
    return (
        <React.Fragment>
            <SInput
                title={"바코드"}
                id={"barcode"}
                value={barcode}
                onChange={handleChange}
                onEnterKeyDown={searchClick}
            />

            <SDatePicker
                title={"생산일자"}
                id="prodDt"
                value={prodDt}
                onChange={handleChange}
            />

            <SInput
                title={"제품"}
                id={"itemNmOrItemCd"}
                value={itemNmOrItemCd}
                onChange={handleChange}
                onEnterKeyDown={searchClick}
            />

            <div className='search_item_btn' style={{ right: "" }}>
                <SButton buttonName={'조회'} onClick={searchClick} type={'btnSearch'} />
            </div>
        </React.Fragment>
    )
}

const MiddleItem = ({
    handleSave
    , handleClose
}) => {
    return (
        <React.Fragment>
            <SButton buttonName={"저장"} onClick={handleSave} type={"btnSave"} />
        </React.Fragment>
    )
}

const ContentItem = ({
    setBarcodeGridApi
    , barcodeList
    , handleSave
}) => {
    return (
        <SGrid
            grid={'barcodeGrid'}
            gridApiCallBack={setBarcodeGridApi}
            rowData={barcodeList}
            rowDoubleClick={handleSave}
            editable={false}
        />
    )
}