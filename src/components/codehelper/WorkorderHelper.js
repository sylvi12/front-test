import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer, inject } from 'mobx-react';
import { observable, toJS } from 'mobx'
import moment from 'moment';
import 'moment/locale/ko';

import SInput from 'components/atoms/input/SInput'
import SSelectBox from 'components/atoms/selectbox/SSelectBox'
import CodeHelper from 'components/codehelper/HoC/CodeHelper'
import WorkorderRepository from 'modules/pages/order/workorder/service/WorkorderRepository';
import HelperRepository from 'components/codehelper/service/HelperRepository'
import SearchTemplate from 'components/template/SearchTemplate'
import GridUtil from 'components/override/grid/utils/GridUtil';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import STreeSelectBox from 'components/override/tree/STreeSelectBox';
import CommonStore from 'modules/pages/common/service/CommonStore'

@observer
export default class WorkorderHelper extends Component {

    @observable helperOption = observable({});

    @observable lineGroup = [];

    // CodeHelper에 전달할 조회 내역 List
    @observable itemList = [];

    // CodeHelper-Modal에 표시할 Grid ColumnDefs
    columnDefs = [
        {
            headerName: "생산라인"
            , children: [
                {
                    headerName: "생산라인"
                    , field: "lineNm"
                    , width: 100
                    , cellClass: 'cell_align_center'
                    , cellRenderer: "rowSpanCellRender"
                    , rowSpan: function (params) {
                        return GridUtil.rowSpanning(params);
                    }
                }
            ]
        },
        {
            headerName: "지시번호"
            , children: [
                {
                    headerName: "지시번호"
                    , field: "orderNo"
                    , width: 55
                    , cellClass: 'cell_align_center'
                }
            ]
        },
        {
            headerName: "상태"
            , children: [
                {
                    headerName: "상태"
                    , field: "woStatusNm"
                    , width: 48
                    , cellClass: 'cell_align_center'
                }
            ]
        },
        {
            headerName: "지시일자"
            , children: [
                {
                    headerName: "지시일자"
                    , field: "planDtm"
                    , width: 95
                    , cellClass: 'cell_align_center'
                }
            ]
        },
        {
            headerName: "생산일자"
            , children: [
                {
                    headerName: "생산일자"
                    , field: "prodDtm"
                    , width: 165
                    , cellClass: 'cell_align_center'
                }
            ]
        },
        {
            headerName: "제품코드"
            , children: [
                {
                    headerName: "제품코드"
                    , field: "itemCd"
                    , width: 80
                    , cellClass: 'cell_align_center'
                }
            ]
        },
        {
            headerName: "제품명"
            , children: [
                {
                    headerName: "제품명"
                    , field: "itemNm"
                    , width: 200
                }
            ]
        },
        {
            headerName: "지시량",
            children: [
                {
                    headerName: "지시량"
                    , field: "orderQty"
                    , width: 70
                    , type: "numericColumn"
                    , valueFormatter: (params) => GridUtil.numberFormatter(params, 0) //GridUtil.numberFormatter
                    , valueParser: GridUtil.numberParser
                },
                {
                    headerName: "지시단위"
                    , field: "orderUom"
                    , width: 40
                    , cellClass: 'cell_align_center'
                }
            ]
        },
        {
            headerName: "지시중량",
            children: [
                {
                    headerName: "지시중량"
                    , field: "orderQty2"
                    , width: 70
                    , type: "numericColumn"
                    , valueFormatter: (params) => GridUtil.numberFormatter(params, 0) //GridUtil.numberFormatter
                    , valueParser: GridUtil.numberParser
                },
                {
                    headerName: "지시중량단위"
                    , field: "orderUom2"
                    , width: 40
                    , cellClass: 'cell_align_center'
                }
            ]
        },
        {
            headerName: "생산량"
            , children: [
                {
                    headerName: "생산량"
                    , field: "prodQty"
                    , width: 65
                    , type: "numericColumn"
                    , valueFormatter: GridUtil.numberFormatter
                }
            ]
        },
        {
            headerName: "관리유형"
            , children: [
                {
                    headerName: "관리유형"
                    , field: "barcodeYN"
                    , width: 55
                    , cellClass: 'cell_align_center'
                    , valueFormatter: (params) => GridUtil.getCodeNm(params, 'SYC009')
                }
            ]
        }
    ];

    constructor(props) {
        super(props);

        this.state = {
            helperOption: {},
        }
    }

    async componentDidMount() {
        this.setData();
        await this.getLine();
    }

    static getDerivedStateFromProps = (nextProps, prevState) => {
        if (JSON.stringify(nextProps.helperOption) != JSON.stringify(prevState.helperOption)) {
            return { helperOption: JSON.parse(JSON.stringify(nextProps.helperOption)) }
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.helperOption !== this.state.helperOption) {
            this.setData();
        }
    }

    setData = () => {
        this.helperOption = this.state.helperOption;
    }

    handleHelperChange = (data) => {
        this.helperOption[data.id] = data.value;
    }

    // CodeHelper에 전달할 Search 함수
    // Modal 내부 search Button에서 클릭했을 경우 check = false
    handleSearch = async () => {

        const { dateType, startDate, endDate, woStatus, item, lineId, orderNo, buId } = this.helperOption;

        const params = {
            "dateType": dateType
            , "lineId": lineId
            , "startDate": startDate
            , "endDate": endDate
            , "woStatus": woStatus
            , "orderNo": orderNo
            , "item": item
            , "buId": buId
        }

        const { data, status } = await HelperRepository.getWorkorder(params);

        if (status == 200) {
            this.itemList = data.data;
        }
    }

    getLine = async () => {
        const params = {
            buId: this.helperOption.buId
        }

        const { data, status } = await WorkorderRepository.getLine(params);
        if (status == 200) {
            this.lineGroup = data.data;
        }
    }

    render() {

        const { buId, buIdReadOnly, dateType, startDate, endDate, woStatus, lineId, item, orderNo, modalTitle } = this.helperOption;

        return (
            <React.Fragment>

                <CodeHelper
                    {...this.props}
                    columnDefs={this.columnDefs}
                    onSearch={this.handleSearch}
                    onHelperChange={this.handleHelperChange}
                    onOpening={this.setData}
                    itemList={this.itemList}
                    helperCdNmValue={"orderNo"}
                    modalTitle={modalTitle}
                    headerHeight={0}
                    searchItem={
                        <SearchItem
                            {...this.props}
                            onSearch={this.handleSearch}
                            onHelperChange={this.handleHelperChange}
                            buId={buId}
                            buIdReadOnly={buIdReadOnly}
                            orderNo={orderNo}
                            dateType={dateType}
                            startDate={startDate}
                            endDate={endDate}
                            woStatus={woStatus}
                            lineId={lineId}
                            orderNo={orderNo}
                            lineGroup={this.lineGroup}
                            item={item}
                        />
                    }
                />
            </React.Fragment>
        )
    }
}

// CodeHelper에서의 조회 조건 Component
class SearchItem extends Component {

    render() {

        const { onHelperChange, buId, buIdReadOnly, dateType, startDate, endDate, woStatus, lineId, lineGroup, item, orderNo, onSearch, helperOption } = this.props;

        return (
            <SearchTemplate searchItem={
                <React.Fragment>

                    <SSelectBox
                        title={"공장"}
                        id={"buId"}
                        value={buId}
                        onChange={onHelperChange}
                        addOption={"전체"}
                        disabled={buIdReadOnly}
                        optionGroup={CommonStore.buList}
                        valueMember={"buId"}
                        displayMember={"buNm"}
                    />

                    <STreeSelectBox
                        title={"지시상태"}
                        id={"woStatus"}
                        value={woStatus}
                        codeGroup={"MFD004"}
                        onChange={onHelperChange}
                        addOption={"전체"}
                    />

                    <SSelectBox
                        id={"dateType"}
                        value={dateType}
                        codeGroup={"SYC003"}
                        onChange={onHelperChange}
                        marginLeft={30}
                        contentWidth={90}
                    />

                    <SDatePicker
                        id={"startDate"}
                        value={startDate}
                        onChange={onHelperChange}
                    />
                    <SDatePicker
                        title={"~"}
                        id={"endDate"}
                        value={endDate}
                        onChange={onHelperChange}
                    />

                    <SInput
                        title={"지시번호"}
                        id={"orderNo"}
                        value={orderNo}
                        onChange={onHelperChange}
                        onEnterKeyDown={onSearch}
                    />

                </React.Fragment>
            } />
        )
    }
}