import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer, inject } from 'mobx-react';
import { observable } from 'mobx'

import ContentsTemplate from 'components/template/ContentsTemplate';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import SIconButton from 'components/atoms/button/SIconButton';
import STitle from 'components/atoms/label/STitle'
import SModal from 'components/override/modal/SModal';
import SGrid from 'components/override/grid/SGrid'
import SInput from 'components/atoms/input/SInput'
import SButton from 'components/atoms/button/SButton'
import Alert from 'components/override/alert/Alert'
import 'components/codehelper/style.css'

@observer
class CodeHelper extends Component {

    @observable isHelperOpen = false;

    @observable cdValue = '';
    @observable cdNmValue = '';

    // Modal-Grid에 표시할 List변수
    @observable codeList = [];

    componentDidMount() {
        // 초기값 지정했을 경우 Store 변수 Update
        const { defaultValue } = this.props;

        if (defaultValue) {
            this._valueUpdate(eval(`defaultValue.${this.props.cdName}`) || "", eval(`defaultValue.${this.props.cdNmName}`) || "");
        }
    }

    componentWillUnmount() {
        if (this.cdValue == '') {
            this.props.onChange({ "id": this.props.selectedItemId, "value": {} });
        }
    }

    componentWillReceiveProps(nextProps) {

        // 각 업무 Helper에서 조회된 itemList가 변경됐을 경우 Modal-Grid에 전달
        // 조회된 값이 하나일 경우 바로 Binding 및 종료
        if (this.props.itemList != nextProps.itemList) {
            this.codeList = nextProps.itemList;

            if (this.codeList && this.codeList.length == 1) {
                const returnValue = this.codeList[0];

                this._valueUpdate(eval(`returnValue.${this.props.cdName}`), eval(`returnValue.${this.props.cdNmName}`))
                this.props.onChange({ "id": this.props.selectedItemId, "value": returnValue });

                this._closeModal(true);
            } else {
                this._openModal();
            }
        }
    }

    _handleChange(data) {

        const { helperCdNmValue, selectedItemId, cdNmName, onChange, helperOption } = this.props;

        this[data.id] = data.value;

        if (data.id == "cdNmValue") {
            // "cdNm" Input에서 값이 변경되면 각 업무 Helper의 기본 "조회요소"(: helperCdNmValue)에도 업데이트
            if (helperCdNmValue) {
                // onHelperChange({ id: helperCdNmValue, value: data.value });
                helperOption[helperCdNmValue] = data.value;
            }

            //"cdNm" Input에서 값이 변경되면 본 Store selectedItem 변수에 현재 cdNm 업데이트
            let selectedItem = {};
            selectedItem[cdNmName] = data.value;
            onChange({ "id": selectedItemId, "value": selectedItem })
        }
    }

    // cd&cdNm Data Update
    _valueUpdate = (cdValue, cdNmValue) => {
        this._handleChange({ "id": 'cdValue', "value": cdValue });
        this._handleChange({ "id": 'cdNmValue', "value": cdNmValue });
    }

    _handleSearch = async (init) => {
        const { helperOption, onSearch, onOpen, onOpening } = this.props;

        // Modal 띄우기 전에 호출할 경우
        if (init) {
            // Store에서 정의한 onOpen Method 호출
            if (onOpen) {
                await onOpen();
            }

            // 각 업무 Helper에서 조회함수 호출 전 onOpening Method 호출
            if (onOpening) {
                onOpening();
            }
        }

        if (helperOption.isEmptyCondition && this.cdNmValue == '') {
            Alert.meg("조회조건을 입력해주세요.");
            return;
        }

        // 각 업무 Helper의 조회함수 호출
        await onSearch();
    }

    _openModal = () => {
        this.isHelperOpen = true;
    }

    // 작업이 완료됐을 때 Client에 Call Back
    // true: 정상 완료
    // false: 작업 취소
    _closeModal = (result) => {

        const { onHelperResult } = this.props;

        if (onHelperResult) {
            onHelperResult(result);
        }

        if (result == false) {
            this._valueUpdate('', '')
            this.props.onChange({ "id": this.props.selectedItemId, "value": {} });
        }

        this.isHelperOpen = false;
    }

    _handleKeyDown = async (event) => {

        if (this.props.helperOption.searchVisible) {
            if (event.key === 'Enter' || (event.key === 'Tab' && this.cdValue == '')) {
                await this._handleSearch(true);
            }
        }
    }

    _handleCdNmChange = (event) => {

        // cdNmValue이 공백이 아닐 때 값을 수정하면 cdValue값 초기화
        if (this.cdNmValue != '') {
            this._handleChange({ "id": 'cdValue', "value": '' });
        }

        this._handleChange({ "id": event.id, "value": event.value });
    }

    render() {

        const { title, helperOption, } = this.props;

        return (
            <React.Fragment>
                <div className='helper'>

                    {title ? (<label className="item_lb">{title}</label>) : (null)}

                    {helperOption.cdVisible ? (
                        <SInput
                            id={'cdValue'}
                            contentWidth={helperOption.cdWidth}
                            readOnly={helperOption.cdReadOnly}
                            value={this.cdValue}
                        />)
                        : (
                            null
                        )
                    }

                    <SInput
                        id={'cdNmValue'}
                        contentWidth={helperOption.cdNmWidth}
                        readOnly={helperOption.cdNmReadOnly}
                        value={this.cdNmValue}
                        onChange={this._handleCdNmChange}
                        onKeyDown={this._handleKeyDown}
                        isFocus={helperOption.cdNmIsFocus}
                    />

                    {helperOption.searchVisible ? (
                        <SIconButton onClick={() => this._handleSearch(true)} />
                    )
                        : (
                            null
                        )
                    }
                </div>

                <SModal
                    id={"codeHelperModal"}
                    isOpen={this.isHelperOpen}
                    contentLabel={helperOption.modalTitle}
                    width={helperOption.modalWidth}
                    height={helperOption.modalHeight}
                    onRequestClose={() => this._closeModal(false)}
                    contents={
                        <CodeHelperModal
                            {...this.props}
                            codeList={this.codeList}
                            _onSearch={this._handleSearch}
                            _valueUpdate={this._valueUpdate}
                            _closeModal={this._closeModal}
                        />}
                />
            </React.Fragment>
        )
    }
}
export default CodeHelper;

@observer
class CodeHelperModal extends Component {

    gridApi = undefined;

    // CodeHelper에서 전달받은 codeList를 grid에 Binding
    componentWillReceiveProps(nextProps) {
        if (this.props.codeList != nextProps.codeList) {
            if (this.gridApi) {
                this.gridApi.setRowData(nextProps.codeList);
            }
        }
    }

    // Grid DoubleClick 시에 1)CodeHelper의 cd-cdNm 변수에 Update, 2) Store변수에 Update
    // 모두 Update 후에 Modal 종료
    onRowDoubleClicked = (event) => {

        this.props._valueUpdate(eval(`event.data.${this.props.cdName}`), eval(`event.data.${this.props.cdNmName}`));
        this.props.onChange({ "id": this.props.selectedItemId, "value": event.data });

        this.props._closeModal(true);
    }

    setGridApi = (gridApi) => {
        this.gridApi = gridApi;
        this.gridApi.setColumnDefs(this.props.columnDefs);
        this.gridApi.setRowData(this.props.codeList);
    }

    render() {
        return (
            <div className='basic'>
                <div>
                    {this.props.searchItem}

                    <ContentsMiddleTemplate middleItem={<SButton buttonName={'조회'} onClick={() => this.props._onSearch(false)} type={'default'} />}/>
                </div>
                <ContentsTemplate id='itemHelperGrid' contentItem={
                    <SGrid
                        grid={"itemHelperGrid"}
                        gridApiCallBack={this.setGridApi}
                        rowDoubleClick={this.onRowDoubleClicked}
                        editable={false}
                        headerHeight={this.props.headerHeight}
                    />}
                />
            </div>
        );
    }
}

CodeHelper.propTypes = {
    selectedItemId: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    defaultValue: PropTypes.object.isRequired, // CodeHelper에서 선택된 항목을 받을 Store변수: 초기값 생성 시에 필요.
}

CodeHelper.defaultProps = {
    // input binding property
    cdName: 'code',
    cdNmName: 'itemNm',
    headerHeight: 26,
}