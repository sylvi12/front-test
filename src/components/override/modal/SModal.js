import React, { Component } from 'react'
import PropTypes from 'prop-types';
import Modal from 'react-modal';
import Draggable from 'react-draggable';
import btn_pop_close from 'style/img/btn_pop_close.png';
import ContentsTemplate from 'components/template/ContentsTemplate';
import 'components/override/modal/style.css';
import { SModalProvider } from 'utils/context/SModalContext';
import { SConsumer } from 'utils/context/SContext'

export default class SModal extends Component {

    constructor(props) {
        super(props);

        this.state = {
            parentWidth: 0,
            parentHeight: 0,
            maxWidth: 0,
            maxHeight: 0,
        }
    }

    componentWillMount() {
        Modal.setAppElement('body');
        this.windowResize();
        window.addEventListener('resize', this.windowResize);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.windowResize);
    }

    windowResize = () => {
        const container = document.getElementById('container');

        this.setState({ parentWidth: container.offsetWidth });
        this.setState({ parentHeight: container.offsetHeight - 44 });
        this.setState({ maxWidth: container.offsetWidth * 0.9 });
        this.setState({ maxHeight: container.offsetHeight * 0.9 })
    }

    render() {

        const { isOpen    // 오픈 여부
            , onRequestClose  // modal 닫기 함수
            , contentLabel  // modal 라벨
            , contents // modal contents
            , onAfterOpen // modal open 후 실행될 함수
            , closeTimeoutMS // modal 닫기 버튼 후 대기 시간 (ms)
            , width
            , height
            , minWidth
            , minHeight
            , id
        } = this.props;

        const modalStyle = {
            overlay: {
                position: 'fixed',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                backgroundColor: 'rgba(0,0,0,0.4)',
                zIndex: 9999,
            },
            content: {
                width: this.state.parentWidth, //container.offsetWidth,
                height: this.state.parentHeight, //container.offsetHeight - 44,
                backgroundColor: 'transparent',
                border: '0px',
                top: 44,
                left: 0,
                padding: '0px',
                overflow: 'visible',
            }
        }

        const widthValue = Number(width.toString().replace(/[^0-9]/g, ""));
        const heightValue = Number(height.toString().replace(/[^0-9]/g, ""));
        let widthText = width.toString().replace(widthValue, '');
        let heightText = height.toString().replace(heightValue, '');
        widthText += widthText.length > 0 ? "" : "px";
        heightText += heightText.length > 0 ? "" : "px";

        return (

            <React.Fragment>
                <SConsumer>
                    {
                        (context) => {
                            return (
                                <SModalProvider formId={this.props.formId} auth={context.auth}>
                                    <Modal
                                        style={modalStyle}
                                        isOpen={isOpen}
                                        onRequestClose={onRequestClose}
                                        contentLabel={contentLabel}
                                        shouldCloseOnOverlayClick={false} // modal 바깥 부분 클릭 시 닫기 기능 없애기 
                                    >
                                        <Draggable bounds="body" handle="strong" cancel="li">
                                            <div className="wrap_pop_up"
                                                style={{
                                                    width: `${widthValue}${widthText}`,
                                                    height: `${heightValue}${heightText}`,
                                                    // minWidth: `${widthValue}${widthText}`,
                                                    // minHeight: `${heightValue}${heightText}`,
                                                    maxWidth: `${this.state.maxWidth}px`,
                                                    maxHeight: `${this.state.maxHeight}px`,
                                                    top: `calc(50% - ${heightValue / 2}${heightText})`,
                                                    left: `calc(50% - ${widthValue / 2}${widthText})`,
                                                    position: 'absolute',
                                                    // border: '1px solid #c5c5c5',
                                                }}
                                            >
                                                <strong className="cursor">
                                                    <div className="pop_panel_head" id={this.props.formId}>
                                                        <div className="pop_panel_tit">{contentLabel}</div>
                                                        <div className="pop_close">
                                                            <li className="no-cursor" onClick={onRequestClose} style={{width:'30px'}}>
                                                                <img alt="" onClick={onRequestClose} src={btn_pop_close}  />
                                                            </li>
                                                        </div>
                                                    </div>
                                                </strong>

                                                <ContentsTemplate id={`modal_body_${id}`} contentItem={
                                                    <div className="pop_panel_body" style={{ overflow: 'visible !important' }}>
                                                        {contents}
                                                    </div>
                                                } />
                                            </div>
                                        </Draggable>
                                    </Modal>
                                </SModalProvider>
                            )
                        }
                    }
                </SConsumer>
            </React.Fragment>
        );
    }
}

SModal.propTypes = {
    minWidth: PropTypes.number,
    minHeight: PropTypes.number,
}

SModal.defaultProps = {
    width: 600,
    height: 800,
    minWidth: 500,
    minHeight: 300,
    id: '',
}