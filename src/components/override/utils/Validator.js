
import validator from 'validator';
import alert from 'components/override/alert/Alert';


// method 사용 가능한 메소드
// https://www.npmjs.com/package/validator
export default class Validator {

    constructor(validations) {
        this.validations = validations;
    }

    validate(thisObject) {
        let validation = this.valid();
        let messageArr = [];

        this.validations.forEach(rule => {

            if (!validation[rule.field].isInvalid) {
                //const field_value = thisObject[rule.field].toString();
                const field_value_tmp = thisObject[rule.field];
                let field_value = '';
                if (field_value_tmp) {
                    field_value = thisObject[rule.field].toString();
                }

                const validation_method =
                    typeof rule.method === 'string' ? validator[rule.method] : rule.method

                //if(validation_method(field_value, thisObject) !== rule.validWhen) {
                if (validation_method(field_value, thisObject)) {
                    validation[rule.field] = { isInvalid: true, message: rule.message }
                    validation.isValid = false;
                    messageArr.push(rule.message);
                }
            }
        });
        if (!validation.isValid) {
            const alterMessage = `필수 항목 [${messageArr.join(",")}]이(가) 입력 되지 않았습니다.`;
            alert.meg(alterMessage);
        }
        return validation;
    }

    valid() {
        const validation = {}

        this.validations.map(rule => {
            validation[rule.field] = { isInvalid: false, message: '' }
        });
        return { isValid: true, ...validation };
    }

}