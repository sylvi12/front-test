import React, { Component } from 'react';
import { Tabs, TabList, Tab, PanelList, Panel } from 'react-tabtab';
import 'components/override/tab/style.css'

export default class SMainTab extends Component {

    index = 0;

    constructor(props) {
        super(props);

        this.state = {
            template: { header: [], content: [] }
        }
    }

    handleEdit = ({ type, index }) => {

        const { onTabEdit } = this.props;

        if (onTabEdit) {
            onTabEdit({ type, index });
        }
    }

    handleTabChange = (index) => {

        const { onTabChange } = this.props;

        this.setState({ activeIndex: index });

        if (onTabChange) {
            onTabChange(index);
        }
    }

    componentWillReceiveProps(nextProps) {

        if (this.props.tabs != nextProps.tabs) {

            if (this.props.tabs.length < nextProps.tabs.length) {

                const newTab = nextProps.tabs[nextProps.tabs.length - 1];
                this.createTab(newTab);

            } else if (this.props.tabs.length > nextProps.tabs.length) {

                let removeTab;
                let removeIndex = 0;

                for (let i = 0; i < this.props.tabs.length; i++) {

                    const origin = this.props.tabs[i];

                    if (!nextProps.tabs.some(next => next.contentUrl == origin.contentUrl)) {
                        removeTab = origin;
                        removeIndex = i;
                        break;
                    }
                }

                this.removeTab(removeTab, removeIndex);
            }
        }
    }

    createTab(tab) {

        const tabTemplate = [];
        const panelTemplate = [];

        tabTemplate.push(<Tab key={`tab-${this.index}`} closable={true}>{tab.title}</Tab>);
        panelTemplate.push(<Panel key={`panel-${this.index}`}>{tab.content}</Panel>);

        // TODO : Url로 Content 생성 this.createContent()
        // if (tab.contentUrl) {
        //     const Component = await this.createContent(tab.contentUrl);
        //     panelTemplate.push(<Panel key={`panel-${this.index}`}>{Component}</Panel>);
        // } else {
        //     panelTemplate.push(<Panel key={`panel-${this.index}`}>{tab.content}</Panel>);
        // }

        this.setState(state => ({
            template: {
                header: state.template.header.concat(tabTemplate)
                , content: state.template.content.concat(panelTemplate)
            }
        }));

        this.index++;
    }

    removeTab(tab, index) {

        this.setState((state) => {

            let { template } = state;

            template.header = [...template.header.slice(0, index), ...template.header.slice(index + 1)];
            template.content = [...template.content.slice(0, index), ...template.content.slice(index + 1)];

            return { template };
        });
    }

    // async createContent(formUrl) {
    //     return await import(`modules/pages${formUrl}`).then((module) => {
    //         const Component = module.default;
    //         return <Component />
    //     });
    // }

    render() {

        const { activeIndex } = this.props;
        const { template } = this.state;

        return (
            <div className='basic'>
                <Tabs onTabEdit={this.handleEdit}
                    onTabChange={this.handleTabChange}
                    activeIndex={activeIndex}> 
                    <TabList>
                        {template.header}
                    </TabList>
                    <PanelList>
                        {template.content}
                    </PanelList>
                </Tabs>
            </div>
        )
    }
}

SMainTab.defaultProps = {
    tabs: [],
}