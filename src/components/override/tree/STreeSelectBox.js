import React, { Component } from 'react'
import TreeSelect, { SHOW_PARENT } from 'rc-tree-select';
import PropTypes from 'prop-types';
import CommonStore from 'modules/pages/common/service/CommonStore'
import 'rc-tree-select/assets/index.css';
import 'components/override/tree/style.css'

export default class STreeSelectBox extends Component {

    state = {
        value: []
        , optionList: []
    };

    async componentWillMount() {

        const { codeGroup, optionGroup } = this.props;

        // codeGroup : 공통코드 가져와서 Binding.
        // optionGroup : Custom List Binding.
        if (codeGroup) {
            await this.getCodeGroup(codeGroup);
        } else {
            this.setList(optionGroup);
        }
    }

    getCodeGroup = async (codeGroup) => {

        const { excludeCode } = this.props;

        await CommonStore.getCodeObject().then(codeObject => {

            let option = codeObject[codeGroup].concat();

            if (excludeCode) {
                const removeItem = excludeCode.split(',');

                removeItem.map(item => {
                    const index = option.findIndex(x => x.cd == item);
                    if (index >= 0) {
                        option.splice(index, 1);
                    }
                });
            }

            this.setList(option);
        });
    }

    setList = (optionList) => {
        const { addOption, valueMember, displayMember } = this.props;

        // if (addOption && !optionList.some(x => x[valueMember] == "")) {
        //     let all = {};
        //     all[valueMember] = "";
        //     all[displayMember] = addOption;

        //     optionList.unshift(all);
        // }

        this.makeMultiObject(optionList);
        // this.setState({ optionList });
    }

    async componentWillReceiveProps(nextProps) {

        if (nextProps.codeGroup) {
            if (this.props.codeGroup != nextProps.codeGroup) {
                await this.getCodeGroup(nextProps.codeGroup);
            }
        } else {
            if (JSON.stringify(this.props.optionGroup) != JSON.stringify(nextProps.optionGroup)) {
                this.setList(nextProps.optionGroup);
            }
        }

        if (JSON.stringify(this.props.value) != JSON.stringify(this.state.value)) {
            if (nextProps.value != '') {
                const values = nextProps.value.split(this.props.splitText);
                this.setState({ value: values });
            }
        }
    }

    makeMultiObject = (codeObject) => {

        const { pIdMember, titleMember, valueMember, value, splitText } = this.props;

        const multiObject = (
            codeObject.map((item => {

                const key = eval(`item.${valueMember}`);
                let pId = eval(`item.${pIdMember}`);
                const title = eval(`item.${titleMember}`);
                const value = eval(`item.${valueMember}`);

                if (!pId) {
                    pId = 0;
                }

                return { key: key, pId: pId, title: title, value: value };
            }))
        )

        this.setState({ optionList: multiObject });

        if (value != '') {
            const values = value.split(splitText);
            // const v = values.length > 0 ? values : undefined;
            // console.log('load', values, v);
            // this.setState({ value: v });
            this.setState({ value: values });
        }
    }

    handleChange = (value) => {

        const v = value.length > 0 ? value : undefined;
        this.setState({ value: v });

        const { id, onChange, onSelectionChange, splitText } = this.props;

        let resultText = '';
        for (let i = 0; i < value.length; i++) {
            resultText += value[i] + splitText;
        }
        resultText = resultText.slice(0, -1);

        if (onChange) {
            const data = { "id": id, "value": resultText };
            onChange(data);
        }

        if (onSelectionChange) {
            onSelectionChange(value);
        }
    }

    render() {

        const { value, optionList } = this.state;
        const { title, titleWidth, contentWidth, disabled } = this.props;

        return (
            <div className="tree_item">
                {title ? (<label className="item_lb" style={{ width: `${titleWidth}px`, textAlign: 'right' }}>{title}</label>) : (null)}
                <TreeSelect
                    style={{ width: `${contentWidth}px` }}
                    treeData={optionList}
                    value={value}
                    onChange={this.handleChange}
                    dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                    treeLine
                    maxTagTextLength={10}
                    // inputValue={null}
                    showSearch={false}
                    treeNodeFilterProp="label"
                    treeCheckable
                    disabled={disabled}
                    // placeholder={<i>{plntName} 태그 그룹...</i>}
                    treeDataSimpleMode={{ id: 'key', rootPId: 0 }}
                // showCheckedStrategy={SHOW_PARENT}
                />
            </div>
        );
    }
}

STreeSelectBox.propTypes = {
    titleWidth: PropTypes.number,
    contentWidth: PropTypes.number,
}

// Default 값 설정
STreeSelectBox.defaultProps = {
    splitText: ',',
    // keyMember: 'cd',
    pIdMember: 'pCd',
    titleMember: 'cdNm',
    valueMember: 'cd',
    titleWidth: 90,
    contentWidth: 110,
    disabled: false,
}