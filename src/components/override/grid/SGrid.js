
import React, { Component } from 'react'
import { AgGridReact } from 'ag-grid-react';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import PropTypes from 'prop-types';

import NumericEditor from "components/override/grid/editor/NumericEditor";
import RowCellTextArea from 'components/override/grid/editor/RowCellTextArea';
import RowCellSelectBox from 'components/override/grid/editor/RowCellSelectBox';
import RowCellDatepicker from 'components/override/grid/editor/RowCellDatepicker';
import RowCellCheckbox from 'components/override/grid/editor/RowCellCheckbox';

import RowSpanCellRender from "components/override/grid/render/RowSpanCellRender";
import RowCellButtonRender from 'components/override/grid/render/RowCellButtonRender';
import RowCellFileLinkRender from 'components/override/grid/render/RowCellFileLinkRender';
import RowCellTextAreaRender from 'components/override/grid/render/RowCellTextAreaRender';
import CustomPinnedRowRenderer from 'components/override/grid/render/CustomPinnedRowRenderer'
import ColumnHeaderRenderer from 'components/override/grid/render/ColumnHeaderRenderer'
import GroupHeaderRenderer from 'components/override/grid/render/GroupHeaderRenderer';
import { SConsumer } from 'utils/context/SContext'
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import 'components/override/grid/style.css'
import 'components/override/grid/customStyle.css'
import 'ag-grid-enterprise';
import { LicenseManager } from "ag-grid-enterprise";
import DailyCellRenderer from 'components/override/grid/render/DailyCellRenderer';
LicenseManager.setLicenseKey("Evaluation_License-_Not_For_Production_Valid_Until_27_October_2019__MTU3MjEzMDgwMDAwMA==5f83b215d012de3ae296c68a215676d1");

@observer
export default class SGrid extends Component {

    constructor(props) {
        super(props);

        this.state = {
            style: { width: '100%', height: '100%' }
            , sizeToFit: this.props.sizeToFit
            , rowDoubleClick: this.props.rowDoubleClick
            , pinnedBottomRowData: []
            , rowBuffer: this.props.rowBuffer
            , gridOptions: {
                suppressRowTransform: true // span 사용자 필요
                , stopEditingWhenGridLosesFocus: true // 그리드 외부 클릭 시 cell 편집 중지
                , enterMovesDownAfterEdit: true // enter 버튼 클릭 시 아래로 이동
                //, localeText: {noRowsToShow: '검색된 데이터가 없습니다.'}
                // , suppressContextMenu: true
                , localeText: { noRowsToShow: ' ' }
                , rowSelection: 'multiple'
                , rowClassRules: this.props.rowClassRules
                , rowHeight: this.props.rowHeight
                , groupHeaderHeight: this.props.groupHeaderHeight
                , headerHeight: this.props.headerHeight
                , enableRangeSelection: this.props.enableRangeSelection
                , isRowSelectable: this.props.isRowSelectable
                , defaultColDef: {
                    resizable: true
                    , filter: this.props.filter // header filter
                    , editable: this.props.editable
                    //, tooltipComponent: "gridTooltip"
                    , sortable: this.props.sortable
                    //, valueSetter: this.nameValueSetter
                    , cellClass: this.getCellClass
                    , menuTabs: []
                }
                , defaultColGroupDef: {
                    headerGroupComponent: 'headerGroupComponent'
                }
                , editType: this.props.editType // edit 시 row 전체 수정 // fullRow, single

                , onRowEditingStarted: function (event) { // row 편집 시작

                }
                , onRowEditingStopped: function (event) { // row 편집 종료

                }
                , onGridSizeChanged: function (event) { // 창 크기 변경 되었을 때 이벤트 
                    if (props.sizeToFit) {
                        event.api.sizeColumnsToFit();
                    }
                }
                , onRowDoubleClicked: function (param) { // Row 더블 클릭 이벤트
                    if (props.rowDoubleClick) {
                        props.rowDoubleClick(param);
                    }
                }
                , getRowClass: function (params) {
                    if (!params.data)
                        return null;
                    if (params.data.totalType == "TOTAL-1" || params.data.TOTAL_TYPE == "TOTAL-1") {
                        return "grid-row-total1";
                    } else if (params.data.totalType == "TOTAL-2" || params.data.TOTAL_TYPE == "TOTAL-2") {
                        return "grid-row-total2";
                    }
                }
                , onCellValueChanged: this.handleCellValueChanged
                , onRowDataChanged: this.handleRowDataChanged
                , onSelectionChanged: this.handleSelectionChanged

                , suppressKeyboardEvent: function (params) {

                    if (params.editing) {
                        return false;
                    }

                    var event = params.event;
                    var key = event.which;
                    let KEY_C = 67;

                    // Ctrl+C 눌렀을 때 셀 하나만 선택되어 있으면 셀만 복사
                    if (key == KEY_C && event.ctrlKey) {
                        const rangeSelections = params.api.getCellRanges();

                        if (rangeSelections.length == 1) {
                            const rangeSelection = rangeSelections[0];
                            if (rangeSelection.startRow == rangeSelection.endRow
                                && rangeSelection.columns.length == 1) {
                                params.api.copySelectedRangeToClipboard();
                                return true;
                            }
                        }
                    }
                    return false;
                }
                , frameworkComponents: {
                    rowSpanCellRender: RowSpanCellRender
                    , numericEditor: NumericEditor
                    , rowCellButtonRender: RowCellButtonRender
                    , rowCellTextArea: RowCellTextArea
                    , rowCellSelectBox: RowCellSelectBox
                    , rowCellDatepicker: RowCellDatepicker
                    , rowCellFileLinkRender: RowCellFileLinkRender
                    , rowCellCheckbox: RowCellCheckbox
                    , rowCellTextAreaRender: RowCellTextAreaRender
                    , customPinnedRowRenderer: CustomPinnedRowRenderer
                    , agColumnHeader: ColumnHeaderRenderer
                    , headerGroupComponent: GroupHeaderRenderer
                    , dailyCellRender: DailyCellRenderer
                }
                , overlayLoadingTemplate: '<span className="ag-overlay-loading-center">잠시만 기다려주세요.</span>' // 로딩 내용
            }
            , excelStyles: [
                {
                    id: "header",
                    alignment: {
                        horizontal: 'Center', vertical: 'Center'
                    },
                    font: {
                        fontName: 'Malgun Gothic',
                        size: 10,
                    },
                    interior: {
                        color: "#CCCCCC",
                        pattern: "Solid"
                    },
                },
                {
                    id: "cell_align_center",
                    alignment: {
                        horizontal: 'Center', vertical: 'Center'
                    },
                    font: {
                        fontName: 'Malgun Gothic',
                        size: 10,
                    },
                },
                {
                    id: "cell_align_left",
                    alignment: {
                        horizontal: 'Left', vertical: 'Center'
                    },
                    font: {
                        fontName: 'Malgun Gothic',
                        size: 10,
                    },
                },
                {
                    id: "cell_align_right",
                    alignment: {
                        horizontal: 'Right', vertical: 'Center'
                    },
                    font: {
                        fontName: 'Malgun Gothic',
                        size: 10,
                    },
                },
                {
                    id: "ag-numeric-cell",
                    alignment: {
                        horizontal: 'Right', vertical: 'Center'
                    },
                    font: {
                        fontName: 'Malgun Gothic',
                        size: 10,
                    },
                },
                {
                    id: "general",
                    alignment: {
                        horizontal: 'Left', vertical: 'Center'
                    },
                    font: {
                        fontName: 'Malgun Gothic',
                        size: 10,
                    },
                },
            ]
        }
    }

    handleCellValueChanged = (event) => {

        this.bottomSummary(event);

        if (event.colDef.type == "numericColumn") {
            event.data[event.colDef.field] = Number(event.newValue);
        }

        if ((event.oldValue != event.newValue)) {
            event.data.edit = true;
            let row = event.api.getDisplayedRowAtIndex(event.rowIndex);
        }

        if (this.props.onCellValueChanged) {
            this.props.onCellValueChanged(event);
        }
    }

    handleRowDataChanged = (event) => {

        const { api } = event;
        // rowSpan의 경우 rowBuffer 설정
        const rows = api.getModel().rowsToDisplay;

        if (rows && rows.length > 0) {

            const { rowBuffer, grid } = this.props;

            const max = Math.max.apply(null, rows.map(item => { return item.customRowSpan ? item.customRowSpan : 0 }));
            const buffer = Math.max(max, rowBuffer)
            if (this.state.rowBuffer != buffer) {
                this.setState({ rowBuffer: buffer })
                console.log('SGrid', grid, 'rowBuffer', buffer)
            }
        }

        this.bottomSummary(event);

        event.api.setSortModel(null);

        // 임의로 포커스를 지정했을 경우 Clear하지 않는다.
        if (!event.api.setFocused) {
            event.api.clearFocusedCell();
        }
        event.api.setFocused = false;

        event.api.clearRangeSelection();
        this.currentRowIndex = undefined;

        if (this.props.onRowDataChanged) {
            this.props.onRowDataChanged(event);
        }
    }

    handleSelectionChanged = (event) => {
        const { onSelectionChanged, suppressRowClickSelection } = this.props;

        if (onSelectionChanged) {
            onSelectionChanged(event);
        }
        if (suppressRowClickSelection) {
            event.api.clearRangeSelection();
        }
    }

    currentRowIndex = undefined;

    handleCellClicked = (event) => {
        const { onCellClicked } = this.props;

        const currentDef = event.colDef;

        // selectCell일 경우에는 한번 눌렀을 때 editing Mode로 변경
        if (currentDef.cellEditor == "agSelectCellEditor") {
            event.api.startEditingCell({ rowIndex: event.rowIndex, colKey: currentDef.field });
        }

        if (onCellClicked) {
            // 같은 rowIndex를 눌렀을 때는 이벤트 발생하지 않음.
            if (event.rowIndex != this.currentRowIndex) {
                onCellClicked(event);
            }
            this.currentRowIndex = event.rowIndex;
        }
    }

    bottomSummary = (event) => {

        const grid = event.api;
        const { pinnedBottomRowData } = this.state;

        if (pinnedBottomRowData && pinnedBottomRowData.length > 0) {
            const bottomData = pinnedBottomRowData[0];
            for (let key in bottomData) {
                let summary = 0;

                if (!isNaN(bottomData[key])) {
                    grid.forEachNode((node) => {
                        const cellValue = eval(`node.data.${key}`);
                        if (!isNaN(cellValue)) {
                            summary += Number(cellValue);
                        }
                    });
                    bottomData[key] = summary;
                }
            }

            grid.setPinnedBottomRowData(pinnedBottomRowData);
        }
    }

    nameValueSetter(params) {
        return true;
    }

    getCellClass = (params) => {

        let cellClass = [];
        if (!this.props.cellReadOnlyColor) {

            cellClass.push('general');

            if (params.colDef.type == 'numericColumn') {
                cellClass.push('ag-numeric-cell');
            }

            if (params.colDef.customCellClass) {
                cellClass.push(params.colDef.customCellClass);
            }

            return cellClass;
        }

        let editable;
        if (typeof params.colDef.editable === "function") {
            editable = params.colDef.editable(params);
        } else {
            editable = params.colDef.editable;
        }

        if (!editable) {

            cellClass.push('general');

            if (params.colDef.type == 'numericColumn') {
                cellClass.push('ag-numeric-cell');
            }

            if (params.colDef.customCellClass) {
                cellClass.push(params.colDef.customCellClass);
            }

            cellClass.push('cell_readonly');

            return cellClass;
        }

        cellClass.push('general');
        return cellClass;
    }

    getContextMenuItems = (params, context) => {

        params.api.clearRangeSelection();
        var result = [
            {
                name: "엑셀저장 (.xlsx)",
                action: () => this.exportExcel(params, context.menuNm),
            },
        ];

        return result;
    }

    exportExcel = (params, menuNm) => {

        const today = new Date().format("yyyyMMddHHmm");
        const excelStyle = {
            fileName: `${today}_${menuNm}`,
            sheetName: `${today}_${menuNm}`,
            rowHeight: 16.5,
            headerRowHeight: 16.5,
        }

        params.api.exportDataAsExcel(excelStyle);
    }

    onGridReady = params => {

        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;

        params.api.setDomLayout(this.props.domLayout); // normal, autoHeight

        this.props.gridApiCallBack(params.api, params.columnApi);
    };

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.pinnedBottomRowData !== prevState.pinnedBottomRowData) {
            return { pinnedBottomRowData: nextProps.pinnedBottomRowData }
        }
        return null;
    }

    render() {
        const { columnDefs, grid, rowData, suppressRowClickSelection, domLayout } = this.props;

        let style = { width: "100%", height: "100%" };
        if (domLayout == "autoHeight") {
            style = {};
        }

        return (
            <SConsumer>
                {
                    (context) => {
                        return (
                            <div id={grid}
                                ref={(div) => { this.gridDiv = div; }}
                                className="ag-theme-balham"
                                style={style}
                            >

                                <div style={this.state.style}>
                                    <AgGridReact
                                        columnDefs={columnDefs}
                                        gridOptions={this.state.gridOptions}
                                        onGridReady={this.onGridReady}
                                        suppressRowTransform={true}
                                        rowData={rowData}
                                        rowBuffer={this.state.rowBuffer}
                                        suppressKeyboardEvent={this.state.suppressKeyboardEvent}
                                        pinnedBottomRowData={this.state.pinnedBottomRowData}
                                        onCellClicked={this.handleCellClicked}
                                        suppressRowClickSelection={suppressRowClickSelection}
                                        getContextMenuItems={(params) => this.getContextMenuItems(params, context)}
                                        excelStyles={this.state.excelStyles}
                                    />
                                </div>
                            </div>
                        )
                    }
                }
            </SConsumer>
        );
    }
}

// Type 체크
SGrid.propTypes = {
    rowHeight: PropTypes.number,
    editType: PropTypes.string
}

// Default 값 설정
SGrid.defaultProps = {
    rowHeight: 26,
    headerHeight: 26,
    groupHeaderHeight: 26,
    editable: true,
    editType: "fullRow",
    sizeToFit: false,
    rowData: [],
    sortable: true,
    filter: false,
    cellReadOnlyColor: false,
    enableRangeSelection: true,
    suppressRowClickSelection: false, //그리드에 체크박스가 있으면 해당 옵션 true 설정
    domLayout: "normal",
    rowBuffer: 20,
}