import React, { Component } from "react";

export default class RowSpanCellRender extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: this.rowSpanCell(),
        };
    }

    rowSpanCell() {
        const { value, eGridCell } = this.props;
        const cellData = value;

        if (value) {
            eGridCell.className = eGridCell.className + ' ag-row-span';
            return cellData;
        } else {
            return null;
        }
    }

    render() {

        const { value } = this.state;
        const { colDef, node, eGridCell, api } = this.props;

        const rows = api.getModel().rowsToDisplay;
        const rowIndex = node.rowIndex;

        if (rows.length > 0) {
            let rowSpan;
            let totalRowHeight = 0;

            if (!colDef.rowSpan) {
                rowSpan = 1;
            } else if (typeof colDef.rowSpan != "function") {
                rowSpan = colDef.rowSpan;
            } else {
                rowSpan = colDef[`customRowSpan_${node.rowIndex}`];
            }

            if (rowSpan && rowSpan != 1) {
                for (let i = rowIndex; i < rowIndex + rowSpan; i++) {
                    if (rows.length <= rowIndex) {
                        continue;
                    }
                    totalRowHeight = totalRowHeight + api.getRowNode(i).rowHeight;
                }

                eGridCell.style.height = `${totalRowHeight}px`; //`${node.rowHeight * rowSpan}px`;
            }
        }

        return (value);
    }
}