import React, { Component } from 'react';
import GridUtil from 'components/override/grid/utils/GridUtil'

export default class CustomPinnedRowRenderer extends Component {
    render() {
        const { style, value, decimalScale } = this.props;

        return (
            <span style={style}>
                {GridUtil.numberFormat(value, decimalScale)}
            </span>
        );
    }
};