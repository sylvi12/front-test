import React, { Component } from 'react';

export default class GroupHeaderRenderer extends Component {

    _isMounted = false;

    constructor(props) {
        super(props);

        this.state = {
            ascSort: false,
            descSort: false,
            editable: false,
        }
    }

    componentDidMount() {
        this._isMounted = true;
        this.props.agGridReact.gridOptions.api.addEventListener('rowDataChanged', this.handleRowDataChanged);
        this.setState({ editable: this.findEditable(this.props.columnGroup.children) })
    }

    componentWillUnmount() {
        // this.props.agGridReact.gridOptions.api.removeEventListener('rowDataChanged', this.handleRowDataChanged);
        this._isMounted = false;
    }

    handleRowDataChanged = (params) => {
        if (this._isMounted == false) {
            return;
        }

        this.setState({ ascSort: false })
        this.setState({ descSort: false })
    }

    handleSortChanged = () => {
        if (this._isMounted == false) {
            return;
        }

        this.setState({ ascSort: false })
        this.setState({ descSort: false })

        const { columnGroup, api } = this.props;

        const colId = columnGroup.children[0].colId;

        if (columnGroup.children[0].isSortAscending()) {
            this.setState({ descSort: true })
            var sort = [{ colId: colId, sort: "desc" }];
            api.setSortModel(api.getSortModel().concat(sort));

        } else if (columnGroup.children[0].isSortDescending()) {
            var sortModel = api.getSortModel();
            var currentIndex = sortModel.findIndex(x => x.colId == colId);
            sortModel.splice(currentIndex, 1);
            api.setSortModel(sortModel);

        } else {
            this.setState({ ascSort: true })
            var sort = [{ colId: colId, sort: "asc" }];
            api.setSortModel(api.getSortModel().concat(sort));
        }
    }

    findEditable = (children) => {
        let editable = false;

        children.map(item => {
            if (item.children && item.children.length > 0) {
                this.findEditable(item.children);
                return;
            }

            if (typeof item.colDef.editable != "function" && editable != true) {
                editable = item.colDef.editable;
            }
        })

        return editable;
    }

    render() {

        let ascClassName;
        let descClassName;

        if (this.state.ascSort) {
            ascClassName = "ag-header-icon ag-sort-order";
        } else {
            ascClassName = "ag-header-icon ag-sort-order ag-hidden";
        }

        if (this.state.descSort) {
            descClassName = "ag-header-icon ag-sort-descending-icon";
        } else {
            descClassName = "ag-header-icon ag-sort-descending-icon ag-hidden";
        }

        return (
            <div className="ag-header-group-cell-label" ref="agContainer" onClick={this.handleSortChanged} style={{ cursor: 'hand' }}>
                {this.state.editable == true ?
                    <div style={{ color: '#F15234' }}>*</div>
                    :
                    null
                }
                <span ref="agLabel" className="ag-header-group-text">
                    {this.props.displayName}
                </span>
                <span ref="eSortAsc" className={ascClassName} aria-hidden={!this.state.ascSort} style={{ width: '10px' }}>
                    <span className="ag-icon ag-icon-asc" unselectable="on">
                    </span>
                </span>
                <span ref="eSortDesc" className={descClassName} aria-hidden={!this.state.descSort} style={{ width: '10px' }}>
                    <span className="ag-icon ag-icon-desc" unselectable="on">
                    </span>
                </span>
            </div>
        )
    }
}