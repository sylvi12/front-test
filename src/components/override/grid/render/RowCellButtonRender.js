import React, { Component } from "react";
import SButton from "components/atoms/button/SButton";

export default class RowCellButtonRender extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { buttonName, onClick } = this.props;

        return <SButton
            buttonName={buttonName}
            onClick={onClick}
            type={'default'}
        />
    }
};