
import React, { Component } from "react";

export default class DailyCellRenderer extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { node, value, eGridCell } = this.props;

        const { rowIndex } = this.props.node;
        const { field } = this.props.colDef;

        let spanCtrl = '';

        if (value && value != "") {

            if (node.data.orderGbn == "WO") {

                const info = JSON.parse(value);
                const prodRate = Number(info['prodRate']);

                const thStyle = {
                    border: '1px solid #086FB5'
                    , textAlign: 'center'
                    , width: '56px'
                }
                const tdStyle = {
                    border: '1px solid #086FB5'
                    , textAlign: 'center'
                }

                spanCtrl = (
                    <table style={{ width: '100%' }}>
                        <tbody>
                            <tr>
                                <th style={thStyle}>지시번호</th>
                                <td style={tdStyle}>{info['orderNo']}</td>
                            </tr>
                            <tr>
                                <th style={thStyle}>{info['itemCd']}</th>
                                <td style={tdStyle}>{info['itemNm']}</td>
                            </tr>
                            <tr>
                                <th style={thStyle}>지시일</th>
                                <td style={tdStyle}>{info['planDtm']}</td>
                            </tr>
                            <tr>
                                <th style={thStyle}>생산일</th>
                                <td style={tdStyle}>{info['prodDtm']}</td>
                            </tr>
                            <tr>
                                <th style={thStyle}>비고</th>
                                <td style={tdStyle}>{info['memo']}</td>
                            </tr>
                            <tr>
                                <th style={thStyle} colSpan='2'>
                                    <div key={`key-${rowIndex}-${field}`} style={{ width: '100%' }}>
                                        <div style={{ width: '100%', position: 'absolute', textAlign: 'center' }}>{`${info['totProd']}`}</div>
                                        <div style={{ width: '100%' }}>
                                            <div style={{ background: 'orange', width: `${prodRate > 100 ? 100 : prodRate}%`, height: '21px', float: 'left' }}></div>
                                            <div style={{ background: '#EAEAEA', width: `${prodRate > 100 ? 0 : 100 - prodRate}%`, height: '21px', float: 'left' }}></div>
                                        </div>
                                    </div>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                )

                eGridCell.style.paddingLeft = '0px';
                eGridCell.style.paddingRight = '0px';

            } else if (node.data.orderGbn == "LOT") {

                const info = JSON.parse(value);

                const thStyle = {
                    border: '1px solid #76AF08'
                    , textAlign: 'center'
                    , width: '56px'
                }
                const tdStyle = {
                    border: '1px solid #76AF08'
                    , textAlign: 'center'
                }

                spanCtrl = (
                    <table style={{ width: '100%' }}>
                        <tbody>
                            <tr>
                                <th style={thStyle}>{'LOT'}</th>
                                <td style={tdStyle}>{info['lotNo']}</td>
                            </tr>
                            <tr>
                                <th style={thStyle}>{'생산일자'}</th>
                                <td style={tdStyle}>{info['prodDt']}</td>
                            </tr>
                            <tr>
                                <th style={thStyle}>{'유통기한'}</th>
                                <td style={tdStyle}>{info['expiryDt']}</td>
                            </tr>
                            <tr>
                                <th style={thStyle}>{'생산수량'}</th>
                                <td style={tdStyle}>{info['prodQty']}</td>
                            </tr>
                        </tbody>
                    </table>
                )

                eGridCell.style.paddingLeft = '0px';
                eGridCell.style.paddingRight = '0px';
            }
        } else {
            spanCtrl = (<span key={`key-${rowIndex}-${field}`}></span>);
        }

        return (
            <div style={{ paddingLeft: '0px', paddingRight: '0px' }}>
                <div>
                    {spanCtrl}
                </div>
            </div>
        );
    }
}