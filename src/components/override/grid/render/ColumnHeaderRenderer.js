import React, { Component } from 'react';

export default class ColumnHeaderRenderer extends Component {

    _isMounted = false;

    constructor(props) {
        super(props);

        this.state = {
            ascSort: false,
            descSort: false,
        }
    }

    componentDidMount() {
        this._isMounted = true;
        this.props.agGridReact.gridOptions.api.addEventListener('rowDataChanged', this.handleRowDataChanged);
    }

    componentWillUnmount() {
        // this.props.agGridReact.gridOptions.api.removeEventListener('rowDataChanged', this.handleRowDataChanged);
        this._isMounted = false;
    }

    handleRowDataChanged = (params) => {
        if (this._isMounted == false) {
            return;
        }
            
        this.setState({ ascSort: false })
        this.setState({ descSort: false })
    }

    handleSortChanged = () => {
        if (this._isMounted == false) {
            return;
        }

        this.setState({ ascSort: false })
        this.setState({ descSort: false })

        const { column, api } = this.props;

        const colId = column.colId;

        if (column.isSortAscending()) {
            this.setState({ descSort: true })
            var sort = [{ colId: colId, sort: "desc" }];
            api.setSortModel(api.getSortModel().concat(sort));

        } else if (column.isSortDescending()) {
            var sortModel = api.getSortModel();
            var currentIndex = sortModel.findIndex(x => x.colId == colId);
            sortModel.splice(currentIndex, 1);
            api.setSortModel(sortModel);

        } else {
            this.setState({ ascSort: true })
            var sort = [{ colId: colId, sort: "asc" }];
            api.setSortModel(api.getSortModel().concat(sort));
        }
    }

    render() {

        const { column } = this.props;

        let editable = false;

        if (typeof column.colDef.editable != "function") {
            editable = column.colDef.editable;
        }

        let ascClassName;
        let descClassName;

        if (this.state.ascSort) {
            ascClassName = "ag-header-icon ag-sort-order";
        } else {
            ascClassName = "ag-header-icon ag-sort-order ag-hidden";
        }

        if (this.state.descSort) {
            descClassName = "ag-header-icon ag-sort-descending-icon";
        } else {
            descClassName = "ag-header-icon ag-sort-descending-icon ag-hidden";
        }

        return (
            <React.Fragment>
                <div className="ag-cell-label-container" role="presentation" onClick={this.handleSortChanged} style={{ cursor: 'hand' }}>
                    <div ref="eLabel" className="ag-header-cell-label" role="presentation" unselectable="on">
                        {editable == true ?
                            <span style={{ color: '#F15234' }}>*</span>
                            :
                            null
                        }
                        <span ref="eText" className="ag-header-cell-text" role="columnheader" unselectable="on">{this.props.displayName}</span>
                        <span ref="eSortAsc" className={ascClassName} aria-hidden={!this.state.ascSort}>
                            <span className="ag-icon ag-icon-asc" unselectable="on">
                            </span>
                        </span>
                        <span ref="eSortDesc" className={descClassName} aria-hidden={!this.state.descSort}>
                            <span className="ag-icon ag-icon-desc" unselectable="on">
                            </span>
                        </span>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
