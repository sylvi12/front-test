// import React, { Component } from "react";
// import FileRepository from '../../modules/file/FileRepository';
// //import API from "../../utils/API";

// export default class RowCellFileLinkRender extends Component {

//     constructor(props) {
//         super(props);

//         this.state = this.createInitialState(props);
//         this.onClick = this.onClick.bind(this);
//     }

//     createInitialState(props) {
//         let title;
//         let key;
//         const value = props.value;
//         if (value) {
//             key = value.split(',')[0]
//             value.split(',')[1] == 'null' ? title = '' : title = value.split(',')[1];
//         }
//         return {
//             title: title,
//             key: key
//         }
//     }

//     onClick() {
//         FileRepository.fileDownload(this.state.key);
//     }

//     render() {
//         return (
//             <span>
//                 <a className="grid-row-cell-link" onClick={this.onClick}>
//                     {this.state.title}
//                 </a>
//             </span>
//         );
//     }
// };