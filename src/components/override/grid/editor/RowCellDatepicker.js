import React, { Component } from "react";
import $ from 'jquery';
import 'jquery-ui/ui/core';
import 'jquery-ui/ui/widgets/datepicker';

export default class RowCellDatepicker extends Component {

    constructor(props) {
        super(props);

        $.datepicker.setDefaults({
            prevText: '이전 달',
            nextText: '다음 달',
            monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
            monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
            dayNames: ['일', '월', '화', '수', '목', '금', '토'],
            dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
            dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
            showMonthAfterYear: true,
            yearSuffix: '년',

        });

        console.log(this.props.value);
        this.state = {
            date: this.props.value
        };

        this.onKeyDown = this.onKeyDown.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        $(this.refs.input).datepicker({
            dateFormat: "yy-mm-dd",
            onSelect: this.onDateChanged.bind(this)
        });
        this.refs.input.addEventListener('keydown', this.onKeyDown);
    }

    componentWillUnmount() {
        $(this.refs.input).datepicker('destroy');
        this.refs.input.removeEventListener('keydown', this.onKeyDown);
    }

    getValue() {
        return this.state.date;
    }

    afterGuiAttached() {
        const eInput = this.refs.input;
        eInput.focus();
    }

    onDateChanged(event) {
        console.log(event);
        this.setState({ date: this.refs.input.value });
    }

    onKeyDown(event) {
        console.log(event);
    }

    handleChange(event) {
        console.log(event);
        //this.setState({value: event.target.value});
    }


    render() {

        return (
            <input ref="input" value={this.state.date} onChange={this.handleChange} />
        );
    }
};