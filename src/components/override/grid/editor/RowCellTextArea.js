import React, { Component } from "react";
import PropTypes from 'prop-types';
export default class RowCellTextArea extends Component {

    constructor(props) {
        super(props);
        this.state = this.createInitialState(props);
        this.onKeyDown = this.onKeyDown.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.onBlur = this.onBlur.bind(this);
    }

    createInitialState(props) {

        return {
            value: props.value
        }
    }

    componentDidMount() {
        this.refs.textarea.addEventListener('keydown', this.onKeyDown);
    }

    componentWillUnmount() {
        this.refs.textarea.removeEventListener('keydown', this.onKeyDown);
    }

    getValue() {
        return this.state.value;
    }

    onKeyDown(event) {
        if (event.keyCode == 13) {
            event.stopPropagation();
            return;
        }
    }

    onBlur() {
        this.props.api.stopEditing();
    }

    handleChange(event) {
        this.setState({ value: event.target.value });
    }

    isPopup() {
        return true;
    }

    render() {

        // 현재 Cell에 크기 맞춤
        const rowHeight = this.props.eGridCell.clientHeight;
        const columnWidth = this.props.eGridCell.clientWidth;

        const style = {
            width: `${columnWidth}px`,
            height: `${rowHeight}px`,
        }

        const { maxLength, rows, cols } = this.props;
        return (
            <div style={style}>
                <textarea
                    ref="textarea"
                    maxLength={maxLength}
                    style={style}
                    value={this.state.value}
                    onChange={this.handleChange}
                    onBlur={this.onBlur}
                ></textarea>
            </div>
        );
    }
};

// Type 체크
RowCellTextArea.propTypes = {

};

// Default 값 설정
RowCellTextArea.defaultProps = {
    maxLength: 200
    , rows: 10
    , cols: 60
}
