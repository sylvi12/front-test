import React, { Component } from 'react'
import PropTypes from 'prop-types';
import SNumericInput from '../numericinput/SNumericInput';
import 'components/override/datepicker/style.css'
import "react-datepicker/dist/react-datepicker.css";

export default class STimePicker extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { title, id, value, titleWidth, contentWidth, readOnly, onChange } = this.props;

        return (
            <React.Fragment>
                <ul className="fl">
                    {title ? (<label className="item_lb">{title}</label>) : (null)}
                    <SNumericInput 
                        id={id}
                        value={value}
                        onChange={onChange}
                        formatType={"time"}
                        titleWidth={titleWidth}
                        contentWidth={contentWidth}
                        readOnly={readOnly}
                    />
                </ul>
            </React.Fragment>
        );
    }
}

// Type 체크
STimePicker.propTypes = {
    id: PropTypes.string,
    title: PropTypes.string,
}

// Default 값 설정
STimePicker.defaultProps = {
    contentWidth: 43,
    titleWidth: 90,
    readOnly: false,
}