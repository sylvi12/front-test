
import React, { Component } from 'react'
import PropTypes from 'prop-types';
import 'components/override/datepicker/style.css'

export default class InputDatePicker extends Component {

    constructor (props) {
        super(props);
    }    

    render() {
        return (
            <input
                id={this.props.id}
                type="text"
                className={this.props.customClassName}
                // className="input_box date"
                onClick={this.props.onClick}
                onChange={this.props.onChange}
                value={this.props.value}
                readOnly={this.props.readOnly}
                style={{ width: `${this.props.width}px`, paddingLeft: '5px', textAlign: 'center' }}
            />
        );
    }
}

// Type 체크
InputDatePicker.propTypes = {
    width: PropTypes.number
}

// Default 값 설정
InputDatePicker.defaultProps = {
    width: 75
}
