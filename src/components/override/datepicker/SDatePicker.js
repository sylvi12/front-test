import React, { Component } from 'react'
import PropTypes from 'prop-types';
import DatePicker, { registerLocale } from 'react-datepicker';
import ko from 'date-fns/locale/ko';
registerLocale('ko', ko)
import ObjectUtility from 'utils/object/ObjectUtility';
import DatePickerIcon from 'components/override/datepicker/DatePickerIcon';
import InputDatePicker from "components/override/datepicker/InputDatePicker";
import 'components/override/datepicker/style.css'
import "react-datepicker/dist/react-datepicker.css";
import { observer } from 'mobx-react';

class SDatePicker extends Component {

    constructor(props) {
        super(props);

        this.state = {
            date: null,
        };
    }

    componentDidMount() {
        const { value } = this.props;
        if (value) {
            this.setState({ date: ObjectUtility.convertToDateFromYYYYMMDD(value) });
        } else {
            this.setState({ date: '' });
            const data = { "id": this.props.id, "value": '' };
            this.props.onChange(data);
        }
    }

    handleChange = (date) => {

        const { id, onChange, onValueChange } = this.props;

        this.setState({ date });
        const dateText = ObjectUtility.convertToYYYYMMDDFromDate(date);
        const data = { "id": id, "value": dateText };
        onChange(data);

        if (onValueChange) {
            onValueChange(dateText);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.value != nextProps.value) {
            if (!nextProps.value || nextProps.value == "") {
                this.setState({ date: '' });
            } else {
                this.setState({ date: ObjectUtility.convertToDateFromYYYYMMDD(nextProps.value) });
            }
        }
    }

    render() {
        const { title, id, dateFormat, contentWidth, titleWidth, readOnly, marginLeft, marginRight } = this.props;
        let css = "";
        let customContentWidth = contentWidth;
        if (readOnly) {
            css = "input_box date readOnly";
        } else {
            css = "input_box date";
            customContentWidth = customContentWidth - 23;  // 버튼크기 제외
        }
        let titleClass = "item_lb";

        if (title && title.indexOf('~') == 0) {
            titleClass += " simple_lb"
        }

        return (
            <React.Fragment>
                <div className="fl" style={{ marginLeft: `${marginLeft}px`, marginRight: `${marginRight}px`, height: '28px' }}>
                    <ul className="fl">
                        {title ? (<label className={titleClass} style={{ width: `${titleWidth}px`, textAlign: 'right' }}>{title}</label>) : (null)}
                        <DatePicker
                            customInput={<InputDatePicker id={id} customClassName={css} width={customContentWidth} readOnly={readOnly} />}
                            selected={this.state.date}
                            onChange={this.handleChange}
                            dateFormat={dateFormat}
                            popperPlacement="center"
                            showMonthYearPicker={dateFormat != "yyyy-MM-dd" ? true : false}
                            locale="ko"
                            readOnly={readOnly}
                        />
                    </ul>
                    {readOnly == false ?
                        <ul className="fl">
                            <DatePicker
                                customInput={<DatePickerIcon />}
                                selected={this.state.date}
                                onChange={this.handleChange}
                                popperPlacement="center"
                                showMonthYearPicker={dateFormat != "yyyy-MM-dd" ? true : false}
                                locale="ko"
                                readOnly={readOnly}
                            />
                        </ul>
                        :
                        null
                    }
                </div>
            </React.Fragment>
        );
    }
}

// Type 체크
SDatePicker.propTypes = {
    id: PropTypes.string,
    // value: PropTypes.string,
    title: PropTypes.string,
    dateFormat: PropTypes.string,
    titleWidth: PropTypes.number,
    contentWidth: PropTypes.number,
}

// Default 값 설정
SDatePicker.defaultProps = {
    dateFormat: "yyyy-MM-dd",
    titleWidth: 90,
    contentWidth: 110,
    readOnly: false,

    marginLeft: 0,
    marginRight: 0,
}

export default SDatePicker;