import React, { Component } from 'react'
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';
import 'components/override/numericinput/style.css';
import Alert from 'components/override/alert/Alert';

//https://www.npmjs.com/package/react-number-format
export default class SNumericInput extends Component {

    constructor(props) {
        super(props);
    }

    timeFormat = (val) => {
        let month = this.limit(val.substring(0, 2), '23');
        let date = this.limit(val.substring(2, 4), '59');

        return month + (date.length ? ':' + date : '');
    }

    limit = (val, max) => {
        if (val.length === 1 && val[0] > max[0]) {
            val = '0' + val;
        }

        if (val.length === 2) {
            // if (Number(val) === 0) {
            //     // val = '01';
            // } else
            if (val > max) {
                val = max;
            }
        }

        return val;
    }

    onValueChange = (values) => {
        const { onChange, id, onValueChange, formatType } = this.props;
        if (values.floatValue < 0) {
            Alert.meg("음수는 입력할 수 없습니다.");
            values = {};
        }

        if (onChange) {

            const data = { id: id, value: values.floatValue ? values.floatValue : null };

            if (formatType == "time") {
                data.value = values.formattedValue;
            }

            onChange(data);
        }

        if (onValueChange) {
            onValueChange(values);
        }
    }

    handleFocus = (event) => {
        this.numericInput.select();
    }

    // static getDerivedStateFromProps = (nextProps, prevState) => {
    //     if (nextProps.value != prevState.value) {
    //         return { value: nextProps.value }
    //     }
    //     return null;
    // }

    render() {

        const { title, thousandSeparator, value, decimalScale, prefix, format, mask, readOnly, contentWidth, titleWidth, formatType } = this.props;
        let css = "";
        if (readOnly) {
            css = "search_box w250 readOnly";
        } else {
            css = "search_box w250";
        }
        return (

            <div className="numeric_input_item">
                {title ? (<label className="item_lb" style={{ width: `${titleWidth}px`, textAlign: 'right' }}>{title}</label>) : (null)}
                <NumberFormat
                    className={css}
                    style={{ width: `${contentWidth}px`, textAlign: formatType == "time" ? 'center' : 'right' }}
                    type={formatType == "time" ? '' : 'search'}
                    onValueChange={this.onValueChange}
                    thousandSeparator={thousandSeparator}
                    value={value}
                    decimalScale={decimalScale}
                    prefix={prefix}
                    format={formatType == "time" ? this.timeFormat : format}
                    mask={mask}
                    readOnly={readOnly}
                    getInputRef = {(el) => this.numericInput = el}
                    onFocus={this.handleFocus}
                />
            </div>

        )
    }
}

SNumericInput.propTypes = {
    decimalScale: PropTypes.number,
    titleWidth: PropTypes.number,
    contentWidth: PropTypes.number,
}

SNumericInput.defaultProps = {
    thousandSeparator: true,
    readOnly: false,
    decimalScale: 0,
    titleWidth: 90,
    contentWidth: 110,
    // format: "#### #### #### ####",
    // mask: "_",
}
