import React, { Component } from 'react'
import button_img_on from 'style/img/btn_on.png';
import button_img_off from 'style/img/btn_off.png';

class SOnOffButton extends Component {

    constructor(props) {
        super(props);

        this.state = {
            onOff: 'on'
        }

        this.onClick = this.onClick.bind(this);
    }

    onClick() {

        const onOffMode = (this.state.onOff == 'on' ? 'off' : 'on');
        this.setState({ onOff: onOffMode });

        if (this.props.onClick) {
            this.props.onClick(onOffMode);
        }
    }

    render() {

        const { title } = this.props;

        return (
            <React.Fragment>
                <div className="search_item">
                    <div className="toggle_warp">
                        <label className="item_lb w60">ON/OFF</label>
                        <label className="toggle_label">
                            <input type="checkbox" />
                            <span className="back">
                                <span className="toggle"></span>
                                <span className="label on">ON</span>
                                <span className="label off">OFF</span>
                            </span>
                        </label>
                    </div>
                </div>
                {/* <ul className='btn_onoff'>
                    <li style={{ lineHeight: '24px' }}>{title}</li>
                    <li style={{ cursor: "pointer" }} onClick={this.onClick} >
                        {this.state.onOff == 'on' ? (
                            <img src={button_img_on} alt="" />
                        ) : (
                                <img src={button_img_off} alt="" />
                            )
                        }
                    </li>
                </ul> */}
            </React.Fragment>
        );
    }
}

export default SOnOffButton;