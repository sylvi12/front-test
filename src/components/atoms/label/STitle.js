
import React, { Component } from 'react'
import PropTypes from 'prop-types';

export default class STitle extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { title } = this.props;
        return (
            <label className="item_lb">{title}</label>
        );
    }
}

STitle.propTypes = {
    title: PropTypes.string.isRequired,
}

STitle.defaultProps = {
    title: '',
}
