import React from 'react';
import PropTypes from 'prop-types';
import 'components/atoms/input/style.css'

class SInput extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { isFocus } = this.props;
        if (isFocus) {
            this.nameInput.focus();
        }
    }

    _handleKeyDown = (event) => {

        if (this.props.onKeyDown) {
            this.props.onKeyDown(event);
        }
    }

    _handleKeyUp = (event) => {

        if (this.props.onKeyUp) {
            this.props.onKeyUp(event);
        }

        if (event.key === 'Enter') {
            if (this.props.onEnterKeyDown) {
                this.props.onEnterKeyDown(event);
            }
        }
    }

    _handleClick = (event) => {

        // this.nameInput.select();

        if (this.props.onClick) {
            this.props.onClick(event);
        }
    }

    _handleChange = (event) => {
        if (this.props.onChange) {
            this.props.onChange({ "id": event.target.id, "value": event.target.value });
        }
    }

    render() {
        const { id, title, value, placeholderValue, readOnly, contentWidth, titleWidth } = this.props;
        let css = "";
        if (readOnly) {
            css = "search_box w250 readOnly";
        } else {
            css = "search_box w250";
        }
        return (
            <div className="input_item">
                {title ? (<label className="item_lb" style={{ width: `${titleWidth}px`, textAlign: 'right' }}>{title}</label>) : (null)}
                <input
                    id={id}
                    type="search"
                    className={css}
                    onChange={this._handleChange}
                    onKeyDown={this._handleKeyDown}
                    onKeyUp={this._handleKeyUp}
                    onClick={this._handleClick}
                    value={value}
                    style={{ width: `${contentWidth}px` }}
                    ref={(input) => { this.nameInput = input; }}
                    placeholder={placeholderValue}
                    readOnly={readOnly}
                />
            </div>
        );
    }
}

export default SInput;

SInput.propTypes = {
    readOnly: PropTypes.bool,
    titleWidth: PropTypes.number,
    contentWidth: PropTypes.number,
}

SInput.defaultProps = {
    readOnly: false,
    titleWidth: 90,
    contentWidth: 110,
}