import React, { Component } from 'react';
import { inject } from 'mobx-react';
import PropTypes from 'prop-types';
import 'components/atoms/checkbox/style.css'

@inject("commonStore")
export default class SCheckBoxGroup extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: '',
            optionList: [],
        }
    }

    async componentDidMount() {
        const { codeGroup, optionGroup, value } = this.props;

        if (codeGroup) {
            await this.getCodeGroup(codeGroup);
        } else {
            this.setState({ optionList: optionGroup });
        }

        // this.setState({ selectedOption: value });
    }

    getCodeGroup = async (codeGroup) => {

        const { commonStore } = this.props;

        await commonStore.getCodeObject().then(codeObject => {

            let option = codeObject[codeGroup].concat();
            this.setState({ optionList: option });
        });
    }

    handleChange = (event) => {
        console.log('handleChange', event.target.id, event.target.value);
        
        // let data = ''
        // if (event.target.checked == true) {
        //     this.setState({ checked: true, value: 'Y' });
        //     data = { "id": this.props.id, "value": 'Y' };
        // }
        // else {
        //     this.setState({ checked: false, value: 'N' });
        //     data = { "id": this.props.id, "value": 'N' };
        // }

        // if (this.props.onChange) {
        //     this.props.onChange(data);
        // }
    }

    handleClick = (event) => {
        console.log('handleClick', event.target.id, event.target.value);
        console.log('bb', this[event.target.id])
        // event.stopPropagation();

        this.setState({
            optionList: optionList.map(
                item => event.target.id === item.id
                    ? { ...item, checked: 'true'  } // 새 객체를 만들어서 기존의 값과 전달받은 data 을 덮어씀
                    : item // 기존의 값을 그대로 유지
            )
        })
    }

    render() {

        const { id, title, valueMember, displayMember } = this.props;
        const { optionList } = this.state;

        const checkBoxList =
            (optionList && optionList.map.length > 0) && optionList.map(item => {

                const optionValue = eval(`item.${valueMember}`);
                const optionDisplay = eval(`item.${displayMember}`);

                return (
                    <div className="check_box" key={optionValue} onClick={this.handleClick}>
                        <input type="checkbox"
                            id={optionValue}
                            value={optionValue}
                            name={optionValue}
                            checked={item.checked}
                            onChange={this.handleChange}
                            
                        />
                        <label
                            htmlFor={id}
                            className="item_lb"
                            style={{ marginTop: "0px" }}
                            id={optionValue}
                            value={optionDisplay}
                            // onClick={this.handleClick}
                        >
                            {optionDisplay}
                        </label>
                    </div>
                )
            }
            );


        return (
            <div className="check_item">
                <div className="check_box_warp">
                    {checkBoxList}
                    {/* <div className="check_box">
                        <input
                            id={id}
                            type="checkbox"
                            checked={this.state.checked}
                            onChange={this.handleChange}
                        />
                        <label htmlFor={id} className="item_lb" style={{ marginTop: "0px" }}>{title}</label>
                    </div> */}
                </div>
            </div>
        );
    }
}

// Type 체크
SCheckBoxGroup.propTypes = {

}

// Default 값 설정
SCheckBoxGroup.defaultProps = {
    valueMember: 'cd',
    displayMember: 'cdNm',
}