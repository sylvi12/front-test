import React from 'react';
import 'components/atoms/spinner/style.css'

const Spinner = () => {
  return (
    <section id="spinnersection" className="section" style={{display : 'none'}}>
      <div className="loader loader-3">
          <div className="dot dot1"></div>
          <div className="dot dot2"></div>
          <div className="dot dot3"></div>
      </div>
    </section>
  );
};
export default Spinner;