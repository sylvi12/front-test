FROM nginx:1.17

ADD dist/  /usr/share/nginx/html
RUN chmod -R 755 /usr/share/nginx/html
COPY server.conf /etc/nginx/conf.d/default.conf