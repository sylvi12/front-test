const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const port = process.env.PORT || 8080;

module.exports = {
    mode: 'development',
    entry: './src/index.js',
    output: {
        path: __dirname + '/dist',
        filename: 'bundle.[hash].js',
        chunkFilename: 'static/js/[name].chunk.js'
    },
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            //modules: true,
                            //camelCase: true,
                            //sourceMap: true
                        }
                    }
                ]
            },
            {
                test: /\.less$/,
                // loader: 'style!css!less!postcss'
                use: [
                    {
                        loader: 'style-loader', // creates style nodes from JS strings
                    },
                    {
                        loader: 'css-loader', // translates CSS into CommonJS
                    },
                    {
                        loader: 'less-loader', // compiles Less to CSS
                    },
                ],
            },
            {
                test: /\.(png|jpg|woff|woff2|eot|ttf|svg|gif|otf)$/,
                exclude: /node_modules/,
                loader: 'file-loader',
                options: {
                    limit: 1024,
                    name: '[name].[ext]',
                    publicPath: './dist/',
                    outputPath: './dist/'
                }
            }
        ]
    },
    devtool: 'inline-source-map',
    plugins: [
        new HtmlWebpackPlugin({
            template: 'public/index.html',
            //favicon: 'public/favicon.ico'
        }),
        new webpack.DefinePlugin({
            WEBPACK_CONFIG_API_URL: JSON.stringify("http://api-wms.samyang.com/")
        })
    ],
    resolve: {
        modules: [path.resolve(__dirname, './src'), 'node_modules'],
        extensions: ['.js', '.json', 'css'],
        alias: {
            components: path.resolve(__dirname, './src/components'),
            modules: path.resolve(__dirname, './src/modules'),
            style: path.resolve(__dirname, './src/style'),
            utils: path.resolve(__dirname, './src/utils'),
        }
    }
};